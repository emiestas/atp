<?php

require_once(__DIR__ . '/../../vendor/autoload.php');

foreach (glob(__DIR__ . '/class/class-*.php') as $file) {
    require_once($file);
}

spl_autoload_register(function ($class) {
    if (strpos($class, '\\') !== false) {
        $separator = '\\';
    } else {
        $separator = '_';
    }

    $expl = explode($separator, $class);
    if ($expl[0] === 'Atp') {
        $path = __DIR__ . '/class/';

        if ($class === '\Atp\Core') {
            $path .= 'class-core.php';
        } else {
            unset($expl[0]);
            $path .= join('/', array_slice($expl, 0, -1)) . '/' . end($expl) . '.php';
        }
        if (is_file($path) === false) {
				$var_dump = debug_backtrace();
                $a = -1;
                $b = count($var_dump);
                while ($a <= $b - 2) {
                    $a++;
                    $ERRORMSG .= 'File:' . $var_dump[$a]['file'] . '; Line: ' . $var_dump[$a]['line'] . '; Function: ' . $var_dump[$a]['function'] . '
';
                }
var_export($ERRORMSG);
var_export($class);
var_export($expl);
			die();
			throw new \Exception('Class ' . $class . ' not found at ' . $path);
        }

        require_once($path);
    }
});

require_once(__DIR__ . '/../atpr/autoload.php');
