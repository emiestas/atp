<?php

// del laiko stokos helperis rasomas be validaciju, apeliuojant i programuotojo tvarkinga koda
class HPhpexcel
{
    private $_curr_cell = null; // pilnas intervalas

    private $_next_cell = null; // neribotas skaicius

    private $_prev_cell = null; // ne mazesnis A

    private $_up_cell = null; // ne mazesnis 1

    private $_down_cell = null; // neribotas skaicius

    private $_letters = null;

    public function __construct()
    {
        $this->_letters = range('A', 'Z');
        $this->SetCurrCell();
    }

    public function GetCurrCell()
    {
        return $this->_curr_cell;
    }

    public function SetCurrCell($cCell = null)
    {
        if (is_null($cCell) === true) {
            $this->_curr_cell = 'A1';
            $this->_next_cell = 'B1';
            $this->_down_cell = 'A2';
            $this->_prev_cell = $this->_up_cell = null;
        } else {
            list($let, $num) = $this->GetSeparateCellLabel($cCell);
//            dump_source($this->GetSeparateCellLabel($cCell));
            $this->_curr_cell = $cCell;
            $this->_next_cell = $this->GetNextLetter($let) . $num;
            $this->_down_cell = $let . (string) ((int) $num + 1);
            if ($let == 'A') {
                $this->_prev_cell = null;
            } else {
                $this->_prev_cell = $this->GetPrevLetter($let) . $num;
            }
            if ((int) $num === 1) {
                $this->_up_cell = null;
            } else {
                $this->_up_cell = $let . (string) ((int) $num - 1);
            }
        }
    }

    public function GetNextCell($cCell = null)
    {
        if (is_null($cCell) === true) {
            if (is_null($this->_curr_cell)) {
                $this->SetCurrCell();
            }
        } else {
            if ($cCell == $this->_curr_cell) {
                // do nothing
            } else {
                $this->SetCurrCell($cCell);
            }
        }
        $setCell = $this->_next_cell;
        $this->SetCurrCell($setCell);
        return $setCell;
    }

    public function GetPrevCell($cCell = null)
    {
        if (is_null($cCell) === true) {
            if (is_null($this->_curr_cell)) {
                $this->SetCurrCell();
            }
        } else {
            if ($cCell == $this->_curr_cell) {
                // do nothing
            } else {
                $this->SetCurrCell($cCell);
            }
        }
        $setCell = $this->_prev_cell;
        $this->SetCurrCell($setCell);
        return $setCell;
    }

    public function GetUpCell($cCell = null)
    {
        if (is_null($cCell) === true) {
            if (is_null($this->_curr_cell)) {
                $this->SetCurrCell();
            }
        } else {
            if ($cCell == $this->_curr_cell) {
                // do nothing
            } else {
                $this->SetCurrCell($cCell);
            }
        }
        $setCell = $this->_up_cell;
        $this->SetCurrCell($setCell);
        return $setCell;
    }

    public function GetDownCell($cCell = null)
    {
        if (is_null($cCell) === true) {
            if (is_null($this->_curr_cell)) {
                $this->SetCurrCell();
            }
        } else {
            if ($cCell == $this->_curr_cell) {
                // do nothing
            } else {
                $this->SetCurrCell($cCell);
            }
        }
        $setCell = $this->_down_cell;
        $this->SetCurrCell($setCell);
        return $setCell;
    }

    public function GetLetters()
    {
        return $this->_letters;
    }

    public function GetCellLetter($cCell = null)
    {
        if (is_null($cCell) === true) {
            if (is_null($this->_curr_cell)) {
                $this->SetCurrCell();
            }
            $cCell = $this->GetCurrCell();
        }
        list($let, $num) = $this->GetSeparateCellLabel($cCell);
        return $let;
    }

    public function GetCellNumber($cCell = null)
    {
        if (is_null($cCell) === true) {
            if (is_null($this->_curr_cell)) {
                $this->SetCurrCell();
            }
            $cCell = $this->GetCurrCell();
        }
        list($let, $num) = $this->GetSeparateCellLabel($cCell);
        return $num;
    }

    public function GetSeparateCellLabel($label)
    {
        $let = $num = '';
        $label = trim($label);
        $strArr = str_split($label);
        foreach ($strArr as $char) {
            if (is_numeric($char)) {
                $num .= $char;
            } else {
                $let .= $char;
            }
        }
        return array($let, $num);
    }

    public function GetNextLetter($str)
    {
        $retLetters = '';
        $lettersCnt = count($this->_letters);
        if (strlen($str) == 1) {
            $key = array_search($str, $this->_letters);
            if ($key < ($lettersCnt - 1)) { // raide ne is array galo
                $retLetters = $this->_letters[$key + 1];
            } elseif ($key == ($lettersCnt - 1)) { // paskutine raide
                $retLetters = 'A' . $this->_letters[0];
            }
        } else {
            $first = substr($str, 0, -1);
            $last = substr($str, -1); // gaunam paskutine raide
            $key = array_search($last, $this->_letters);
            if ($key < ($lettersCnt - 1)) { // raide ne is array galo
                $retLetters = $first . $this->_letters[$key + 1];
            } elseif ($key == ($lettersCnt - 1)) { // paskutine raide
                $first = $this->_letters[array_search($first, $this->_letters) + 1];
                $retLetters = $first . $this->_letters[0];
            }
        }
        return $retLetters;
    }

    public function GetPrevLetter($str)
    {
        if ($str == 'A') {
            return null;
        }
        $retLetters = '';
        $lettersCnt = count($this->_letters);
        if (strlen($str) == 1) {
            $key = array_search($str, $this->_letters);
            if ($key > 0) { // ne pirma abeceles raide
                $retLetters = $this->_letters[$key - 1];
            } else { // pirma raide
                $retLetters = $this->_letters[$lettersCnt - 1];
            }
        } else {
            // assumption - nebus daugiau 2 raidziu ataskaitose
            $first = substr($str, 0, -1);
            $last = substr($str, -1); // gaunam paskutine raide
            $key = array_search($last, $this->_letters);
            if ($key > 0) { // raide ne is array galo
                $retLetters = $first . $this->_letters[$key - 1];
            } else { // paskutine raide
                if ($first == 'A' && $last == 'A') {
                    $retLetters = 'Z';
                } else {
                    // $first != 'A' && $last == 'A'
                    $first = $this->_letters[array_search($first, $this->_letters) - 1];
                    $retLetters = $first . $this->_letters[$lettersCnt - 1];
                }
            }
        }
        return $retLetters;
    }
}
