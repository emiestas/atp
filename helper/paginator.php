<?php

/*
 * PHP Pagination Class
 * @author admin@catchmyfame.com - http://www.catchmyfame.com
 * @version 2.0.0
 * @date October 18, 2011
 * @copyright (c) admin@catchmyfame.com (www.catchmyfame.com)
 * @license CC Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) - http://creativecommons.org/licenses/by-sa/3.0/
 */

class Paginator
{

    var $items_per_page;
    var $items_total;
    var $current_page;
    var $num_pages;
    var $mid_range;
    var $low;
    var $limit;
    var $return;
    var $default_ipp;
    var $querystring;
    var $ipp_array;
    var $url;
    var $url_excludes;
    var $show_btn_all;
    public $limit_end;
    public $limit_start;

    function __construct()
    {
        $_GET['page'] = (!empty($_GET['page'])) ? $_GET['page'] : 1;

        $this->current_page = 1;
        $this->mid_range = 5;
        $this->show_btn_all = false;
        $this->ipp_array = array(10, 25, 50, 100, 'All');
        $this->items_per_page = (!empty($_GET['ipp'])) ? $_GET['ipp'] : $this->default_ipp;
        $this->url_excludes = array('page', 'ipp', 'url_string', 'item_id', 'view',
            'controller', 'action');

        // Nurodo URL'ą
        preg_match('/([^?])*/', $_SERVER['REQUEST_URI'], $m);
        $this->url = $m[0];
    }

    function paginate()
    {
        if (!isset($this->default_ipp))
            $this->default_ipp = 25;
        if ($_GET['ipp'] == 'All') {
            $this->num_pages = 1;
//			$this->items_per_page = $this->default_ipp;
        } else {
            if (!is_numeric($this->items_per_page) || $this->items_per_page <= 0)
                $this->items_per_page = $this->default_ipp;
            $this->num_pages = ceil($this->items_total / $this->items_per_page);
        }
        $this->current_page = (isset($_GET['page'])) ? (int) $_GET['page'] : $this->current_page; // must be numeric > 0
        $prev_page = $this->current_page - 1;
        $next_page = $this->current_page + 1;

        $qry_array = array();
        if ($_GET) {
            $args = explode("&", $_SERVER['QUERY_STRING']);
            foreach ($args as $arg) {
                if (strlen($arg) == 0)
                    continue;
                $keyval = explode("=", $arg);
                $qry_array[$keyval[0]] = $keyval[1];
            }
        }

        if ($_POST) {
            foreach ($_POST as $key => $val) {
                $qry_array[$key] = $val;
            }
        }

        foreach ($qry_array as $key => $val) {
            if (in_array($key, $this->url_excludes))
                unset($qry_array[$key]);
        }
        if (empty($qry_array) === FALSE)
            $this->querystring = '&' . urldecode(http_build_query($qry_array));

        if ($this->num_pages > 10) {
            $this->return = ($this->current_page > 1 And $this->items_total >= 10) ? "<a title=\"Vienu puslapiu atgal\" class=\"paginate\" href=\"$this->url?page=$prev_page&ipp=$this->items_per_page$this->querystring\">" . '<' . "</a> " : "<span title=\"Vienu puslapiu atgal\" class=\"paginate inactive\" href=\"#\">" . '<' . "</span> ";

            $this->start_range = $this->current_page - floor($this->mid_range / 2);
            $this->end_range = $this->current_page + floor($this->mid_range / 2);

            if ($this->start_range <= 0) {
                $this->end_range += abs($this->start_range) + 1;
                $this->start_range = 1;
            }
            if ($this->end_range > $this->num_pages) {
                $this->start_range -= $this->end_range - $this->num_pages;
                $this->end_range = $this->num_pages;
            }
            $this->range = range($this->start_range, $this->end_range);

            for ($i = 1; $i <= $this->num_pages; $i++) {
                if ($this->range[0] > 2 And $i == $this->range[0]) {
                    //$this->return .= " ... ";
                    //		$this->return .= "<a class=\"paginate\" href=\"{$this->url}?page=1&ipp={$this->items_per_page}{$this->querystring}\">1</a> ";
                    $this->return .= '<span class="spacer">&nbsp;&nbsp;&nbsp;</span>'; //text('Site_pagination_mid_page_left_seperator');
                }
                // loop through all pages. if first, last, or in range, display
                if ($i == 1 || $i == $this->num_pages || in_array($i,
                        $this->range)) {
                    //if (in_array($i, $this->range)) {
                    $this->return .= ($i == $this->current_page And $_GET['page'] != 'All') ? "<a title=\"$i puslapis iš $this->num_pages\" class=\"paginate current\" href=\"#\">$i</a> " : "<a class=\"paginate\" title=\"Eiti į $i puslapį iš $this->num_pages\" href=\"$this->url?page=$i&ipp=$this->items_per_page$this->querystring\">$i</a> ";
                }
                if ($this->range[$this->mid_range - 1] < $this->num_pages - 1 And $i == $this->range[$this->mid_range - 1]) {
                    //$this->return .= " ... ";
                    $this->return .= '<span class="spacer">&nbsp;&nbsp;&nbsp;</span>'; //text('Site_pagination_mid_page_right_seperator');
                    //	$this->return .= "<a class=\"paginate\" href=\"{$this->url}?page={$this->num_pages}&ipp={$this->items_per_page}{$this->querystring}\">{$this->num_pages}</a> ";
                }
            }
            $this->return .= (($this->current_page < $this->num_pages And $this->items_total >= 10) And ( $_GET['page'] != 'All') And $this->current_page > 0) ? "<a title=\"Vienu puslapiu į priekį\" class=\"paginate\" href=\"$this->url?page=$next_page&ipp=$this->items_per_page$this->querystring\">" . '>' . "</a>\n" : "<span title=\"Vienu puslapiu į priekį\" class=\"paginate inactive\" href=\"#\">" . '>' . "</span>\n";

            if ($this->show_btn_all)
                $this->return .= ($_GET['page'] == 'All') ? "<a class=\"current\" href=\"#\">" . 'Visi' . "</a> \n" : "<a class=\"paginate\" href=\"$this->url?page=1&ipp=All$this->querystring\">" . 'Visi' . "</a> \n";
        } else {
            for ($i = 1; $i <= $this->num_pages; $i++) {
                $this->return .= ($i == $this->current_page) ? "<a class=\"paginate current\" href=\"#\">$i</a> " : "<a class=\"paginate\" href=\"$this->url?page=$i&ipp=$this->items_per_page$this->querystring\">$i</a> ";
            }

            if ($_GET['ipp'] != 'All' && $this->show_btn_all)
                $this->return .= "<a class=\"paginate\" href=\"$this->url?page=1&ipp=All$this->querystring\">" . 'Visi' . "</a> \n";
        }
        $this->low = ($this->current_page <= 0) ? 0 : ($this->current_page - 1) * $this->items_per_page;
        $this->limit_start = ($this->current_page <= 0) ? 0 : ($this->current_page - 1) * $this->items_per_page;
        if ($this->current_page <= 0)
            $this->items_per_page = 0;
        $this->limit_end = ($this->items_per_page == "All") ? (int) $this->total_items : (int) $this->items_per_page * $this->current_page;
        $this->limit = ($_GET['ipp'] == 'All') ? "" : " LIMIT $this->low,$this->items_per_page";
    }

    function display_items_per_page($redirect = true)
    {
        $items = '';
        if (!isset($_GET[ipp]))
            $this->items_per_page = $this->default_ipp;
        foreach ($this->ipp_array as $ipp_opt) {
            $ipp_opt_text = ($ipp_opt == 'All') ? 'Visi' : $ipp_opt;
            $items .= ($ipp_opt == $this->items_per_page) ? "<option selected value=\"$ipp_opt\">$ipp_opt_text</option>\n" : "<option value=\"$ipp_opt\">$ipp_opt_text</option>\n";
        }
        return "<span class=\"paginate_label on_page\">" . 'Rodyti&nbsp;' . "</span><select class=\"paginate on_page\" " . ($redirect ? "onchange=\"window.location='$this->url?page=1&ipp='+this[this.selectedIndex].value+'$this->querystring';\"" : "") . ">$items</select>\n";
    }

    function display_jump_menu()
    {
        for ($i = 1; $i <= $this->num_pages; $i++) {
            $option .= ($i == $this->current_page) ? "<option value=\"$i\" selected>$i</option>\n" : "<option value=\"$i\">$i</option>\n";
        }
        return "<span class=\"paginate_label show_page\">" . 'Puslapis&nbsp;' . "</span><select class=\"paginate show_page\" onchange=\"window.location='$this->url?page='+this[this.selectedIndex].value+'&ipp=$this->items_per_page$this->querystring';return false\">$option</select>\n";
    }

    function display_selects()
    {
        return "<div class=\"paginator jump_menu\">" . $this->display_jump_menu() . $this->display_items_per_page() . '</div>';
    }

    function display_pages()
    {
        return '<div class="paginator pages">' . $this->return . '<div>';
    }
}
