<?php

if (!defined('MD')) {
    die();
}

class MRLankAtask extends MReport
{
    public function __construct()
    {
        parent::__construct();
        require_once(CLASS_PATH . '3dpart/PHPExcel.php');
    }

    public function Output()
    {
        if ($this->_user->IsLogged() === false) {
            return '';
        }
        $allowUsrTypes = array(USER_TYPE_CLASSHEAD, USER_TYPE_SYSADMIN, USER_TYPE_PRINCIPAL,
            USER_TYPE_SOCIAL,
            USER_TYPE_SPEC_EDUCATOR, USER_TYPE_SPEECH_THERAPIST, USER_TYPE_PSYCHOLOGIST,
            USER_TYPE_MEDICAL, USER_TYPE_LIBRARIAN, USER_TYPE_DEPTHEAD);
        if (!in_array($this->_user->GetUserType(), $allowUsrTypes)) {
            return '';
        }

        // kad uzloadintu kalbos failus
        $_helper_report = Core::Helper('report');

        $reportId = (int) $this->_rq->POST('ReportNormal');
        $classId = (int) $this->_rq->POST('ClassNormal');
        $pupilId = (int) $this->_rq->POST('PupilNormal');
//        $semesterId =   (int) $this->_rq->POST('SemesterNormal');
//        $showCourse =   (int) $this->_rq->POST('ShowCourseNormal', 0);
        $fileType = (int) $this->_rq->POST('FileTypeNormal');
        $schoolId = (int) $this->SchoolId;

        $dFrom = $this->_rq->POST('DateFromNormal', '2013-09-01');
        $dTo = $this->_rq->POST('DateToNormal', date('Y-m-d'));

        if (strtotime($dFrom) === false) {
            $dFrom = '2013-09-01';
        }
        if (strtotime($dTo) === false) {
            $dTo = date('Y-m-d');
        }

        $courseType = (int) $this->_rq->POST('CourseTypeNormal');
        /*
          <option value="0"> -- Pasirinkite -- </option>
          <option value="1">Bendrasis ugdymas</option>
          <option value="2" selected="selected">Bendras ir neformalus ugdymas</option>
          <option value="3">Neformalus ugdymas</option></select>
         */

        if ($classId === 0) {
            Core::Template()->set_var('tempMessage', '<div class="message-error">' . text('report_helper_php_2') . '</div><br />');
            return;
            exit;
        }

        if ($courseType === 0) {
            Core::Template()->set_var('tempMessage', '<div class="message-error">' . text('report_helper_php_39') . '</div><br />');
            return;
            exit;
        }

        $pupils = array(); // mokiniu ID array, nebutinai integeriai
        $pupilsFull = array();
        $pupil_label = text('report_helper_php_40') . '';
        // bendras cfg
        $cfg = array('#' => $this->SchoolId, 'userid' => $this->UserId, 'schid' => $this->SchoolId,
            'clid' => $classId);

        $sql = "SELECT up.USER_NAME, up.USER_SURNAME, c.LABEL FROM ED_USERPASS up
            INNER JOIN SCH#_USER u ON u.GLOBALID = up.GLOBALID
            INNER JOIN SCH#_CLASS c ON c.EDUCATOR = u.ID AND c.ID = :clid
            LIMIT 1";
        $tRes = $this->_db->Query($sql, $cfg);
        list($educator_name, $educator_surname) = $this->_db->GetRowAsList($tRes);

        if ($pupilId === 0) { // visi klases mokiniai
            $sql = "SELECT p.ID, up.USER_NAME, up.USER_SURNAME, p.VALID AS P_VALID, u.VALID AS U_VALID,
                di.DATE AS IS_DATE
                FROM SCH#_PUPIL p
                INNER JOIN SCH#_USER u ON u.ID = p.ID
                INNER JOIN ED_USERPASS up ON up.GLOBALID = u.GLOBALID
                LEFT JOIN SCH#_DIR_ISAK di ON di.PUPILID = p.ID AND di.ISAKID = 2
                WHERE p.CLASS = :clid
                ORDER BY up.USER_SURNAME, up.USER_NAME";
            $res = $this->_db->Query($sql, $cfg);
            while ($row = $this->_db->GetRowAsArray($res)) {
                if ((int) $row['U_VALID'] === 0 || ($row['IS_DATE'] && (strtotime($row['IS_DATE']) < strtotime($dTo)))) {
                    // do nothing - mokinys not valid, arba jam sukurtas isvykimo isakymas
                } else {
                    $pupils[] = (int) $row['ID'];
                    $pupilsFull[(int) $row['ID']] = array(
                        'id' => (int) $row['ID'],
                        'name' => $row['USER_NAME'],
                        'surname' => $row['USER_SURNAME'],
                        'pValid' => (int) $row['P_VALID'],
                        'isakDate' => $row['IS_DATE'] ? $row['IS_DATE'] : null,
                    );
                }
            }
        } else {
            $sql = "SELECT up.USER_NAME, up.USER_SURNAME FROM ED_USERPASS up
                INNER JOIN SCH#_USER u ON u.GLOBALID = up.GLOBALID
                WHERE u.ID = :pid LIMIT 1";
            $res = $this->_db->Query($sql, array('#' => $schoolId, 'pid' => $pupilId));
            list($pupil_name, $pupil_surname) = $this->_db->GetRowAsList($res);
            $pupil_label = $pupil_name . ' ' . $pupil_surname;
            $pupils[] = $pupilId;
            $pupilsFull[$pupilId] = array(
                'id' => $pupilId,
                'name' => $pupil_name,
                'surname' => $pupil_surname,
                'pValid' => 1,
                'isakDate' => null,
            );
        }
        // informacija apie vartotoja sugeneravusi ataskaita
        $sql = "SELECT u.ID, u.GLOBALID, up.USER_NAME, up.USER_SURNAME FROM ED_USERPASS up
            INNER JOIN SCH#_USER u ON u.GLOBALID = up.GLOBALID AND u.ID = :userid LIMIT 1";
        $res = $this->_db->Query($sql, $cfg);
        list($u_id, $u_globalid, $u_name, $u_surname) = $this->_db->GetRowAsList($res);
        // klases info
        $sql = "SELECT ID, LABEL FROM SCH#_CLASS WHERE ID = :clid LIMIT 1";
        $tRes = $this->_db->Query($sql, $cfg);
        list($c_id, $c_label) = $this->_db->GetRowAsList($tRes);
        $c_id = (int) $c_id;

        $helper = Core::Helper('phpexcel'); // vaiksciojimo per cell helpers
        $missedAll = Core::Helper()->GetMissedLessonsByPupils($pupils, $dFrom, $dTo, $courseType); // praleistos mokiniu pamokos
        // PHPExcel klases iskvietimas ir apdorijimas excel failo
        $objPHPExcel = new PHPExcel();

        // * bendri nustatymai BEGIN
        $objPHPExcel->getProperties()->setCreator($u_name . ' ' . $u_surname);
        $objPHPExcel->getProperties()->setLastModifiedBy($u_name . ' ' . $u_surname);
        $objPHPExcel->getProperties()->setTitle(
            text('report_helper_php_41') . '');
        $objPHPExcel->getProperties()->setSubject(
            text('report_helper_php_41') . ': ' . $c_label . ', ' . $pupil_label . '.');
        $objPHPExcel->getProperties()->setDescription(
            text('report_helper_php_41') . ': ' . $c_label . ', ' . $pupil_label . '.');

        $objPHPExcel->setActiveSheetIndex(0);
        $sh1 = $objPHPExcel->getActiveSheet();
        $sh1->setTitle(text('report_helper_php_20') . '');
//        $sh1->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        $pageMargins = $sh1->getPageMargins();
        //in inches (0.5cm)
        $margin = 0.55 / 2.54;
        $margin2 = 0.75 / 2.54;
        $pageMargins->setTop($margin2);
        $pageMargins->setBottom($margin);
        $pageMargins->setLeft($margin);
        $pageMargins->setRight($margin);
        // * bednri nustatymai END

        $headerRow = 5;
        $startRow = 14;
        $startCol = 'C';
        $startColNext = 'D';

        $sh1->mergeCells("A" . $headerRow . ":A" . $startRow);
        $sh1->mergeCells("B" . $headerRow . ":C" . $startRow);
        $sh1->SetCellValue("A" . $headerRow, text('core_nr'));
        $sh1->SetCellValue("B" . $headerRow, text('report_helper_php_24') . '');

        $sh1->SetCellValue("B1", text('report_helper_php_42') . '');
        $sh1->SetCellValue("B2", text('report_exel_php_4') . ': ' . $this->_user->GetSchoolLabel());
        $sh1->SetCellValue("B3", text('core_inner_5') . ': ' . $c_label);
        $sh1->SetCellValue("B4", text('report_exel_php_3') . ': ' . $dFrom . ' - ' . $dTo);


        $lCol = $startCol;
        $pupilRow = 0;
        $all_pupils_cnt = count($pupils);
        // style
        $sh1->getColumnDimension($lCol)->setWidth(10); // C columnas
        // loopinam per mokinius
        $pupilRow = $startRow; // setinam pirmos eilutes nr (-1)
        $pupils_cnt = 0;
        foreach ($pupils as $pid) {
            $pupilRow++;
            $pupils_cnt++;

            $pupilsFull[(int) $pid]['row'] = $pupilRow;
            $sh1->SetCellValue('A' . (string) ($pupilRow), $pupils_cnt);
            $sh1->SetCellValue('B' . (string) ($pupilRow), $pupilsFull[(int) $pid]['surname'] . ' ' . $pupilsFull[(int) $pid]['name']);
            $sh1->mergeCells('B' . (string) ($pupilRow) . ":" . 'C' . (string) ($pupilRow));
            $sh1->getRowDimension($pupilRow)->setRowHeight(13);

            $tempL = $lCol;
            $tempL = $helper->GetNextLetter($tempL);
            $sh1->SetCellValue($tempL . (string) ($pupilRow), $missedAll[$pid]['total']);
            $tempL = $helper->GetNextLetter($tempL);
            $sh1->SetCellValue($tempL . (string) ($pupilRow), $missedAll[$pid]['byIllness']);
            $tempL = $helper->GetNextLetter($tempL);
            $sh1->SetCellValue($tempL . (string) ($pupilRow), $missedAll[$pid]['byOther']);
            $tempL = $helper->GetNextLetter($tempL);
            $sh1->SetCellValue($tempL . (string) ($pupilRow), $missedAll[$pid]['notApproved']);
            $tempL = $helper->GetNextLetter($tempL);
            $sh1->SetCellValue($tempL . (string) ($pupilRow), $missedAll[$pid]['late']);
        }

        // patikrinam ar tikrai yra ka apdoroti
        if (empty($pupils) === false) {
            // teksto pasukimas dalyku pavadinimuose ir kituose uzrasuose kur reikia
            $allLessonRange = $helper->GetNextLetter($startCol) . (string) ($headerRow + 1) . ":" . $tempL . (string) $startRow;
            $sh1->getStyle($allLessonRange)->getAlignment()->setTextRotation(90);

            $infoRow = $pupilRow + 1;
            $lastInfoRow = $pupilRow + 3;

            $start2 = '';
            $tempColL = $lCol;
            for ($i = 6; $i < 11; $i++) {
                $tempColL = $helper->GetNextLetter($tempColL);
                // STYLES
                $sh1->getColumnDimension($tempColL)->setWidth(6);
                $sh1->mergeCells($tempColL . (string) ($headerRow + 1) . ":" . $tempColL . (string) $startRow);
                switch ($i) {
                    case 6:
                        $sh1->SetCellValue($tempColL . (string) ($headerRow + 1), text('report_helper_php_43') . '');
                        $start2 = $tempColL;
                        $sh1->SetCellValue($tempColL . (string) ($infoRow), '=SUM(' . $tempColL . ($startRow + 1) . ":" . $tempColL . ($pupilRow) . ')');
                        $sh1->SetCellValue($helper->GetPrevLetter($tempColL) . (string) ($infoRow + 1), text('report_helper_php_44') . ':');
                        $sh1->SetCellValue($helper->GetPrevLetter($tempColL) . (string) ($infoRow), text('report_helper_php_7') . ':');
                        $sh1->SetCellValue($tempColL . (string) ($infoRow + 1), '=ROUND(SUM(' . $tempColL . ($startRow + 1) . ":" . $tempColL . ($pupilRow) . ')/' . $all_pupils_cnt . ',2)');
                        // style
                        $sh1->getStyle($helper->GetPrevLetter($tempColL) . (string) ($infoRow + 1))
                            ->applyFromArray(Core::Helper()->GetStandartStylesLeftAlignmentArr());
                        $sh1->getStyle($helper->GetPrevLetter($tempColL) . (string) ($infoRow))
                            ->applyFromArray(Core::Helper()->GetStandartStylesLeftAlignmentArr());
                        break;
                    case 7:
                        $sh1->SetCellValue($tempColL . (string) ($headerRow + 1), text('report_helper_php_45') . '');
                        $sh1->SetCellValue($tempColL . (string) ($infoRow), '=SUM(' . $tempColL . ($startRow + 1) . ":" . $tempColL . ($pupilRow) . ')');
                        $sh1->SetCellValue($tempColL . (string) ($infoRow + 1), '=ROUND(SUM(' . $tempColL . ($startRow + 1) . ":" . $tempColL . ($pupilRow) . ')/' . $all_pupils_cnt . ',2)');
                        break;
                    case 8:
                        $sh1->SetCellValue($tempColL . (string) ($headerRow + 1), text('report_helper_php_46') . '');
                        $sh1->SetCellValue($tempColL . (string) ($infoRow), '=SUM(' . $tempColL . ($startRow + 1) . ":" . $tempColL . ($pupilRow) . ')');
                        $sh1->SetCellValue($tempColL . (string) ($infoRow + 1), '=ROUND(SUM(' . $tempColL . ($startRow + 1) . ":" . $tempColL . ($pupilRow) . ')/' . $all_pupils_cnt . ',2)');
                        break;
                    case 9:
                        $sh1->SetCellValue($tempColL . (string) ($headerRow + 1), text('report_helper_php_47') . '');

                        $sh1->SetCellValue($tempColL . (string) ($infoRow), '=SUM(' . $tempColL . ($startRow + 1) . ":" . $tempColL . ($pupilRow) . ')');
                        $sh1->SetCellValue($tempColL . (string) ($infoRow + 1), '=ROUND(SUM(' . $tempColL . ($startRow + 1) . ":" . $tempColL . ($pupilRow) . ')/' . $all_pupils_cnt . ',2)');

                        break;
                    case 10:
                        $sh1->SetCellValue($tempColL . (string) ($headerRow + 1), text('report_helper_php_48') . '');
                        $sh1->SetCellValue($tempColL . (string) ($infoRow), '=SUM(' . $tempColL . ($startRow + 1) . ":" . $tempColL . ($pupilRow) . ')');
                        $sh1->SetCellValue($tempColL . (string) ($infoRow + 1), '=ROUND(SUM(' . $tempColL . ($startRow + 1) . ":" . $tempColL . ($pupilRow) . ')/' . $all_pupils_cnt . ',2)');
                        $sh1->mergeCells($start2 . (string) ($headerRow) . ":" . $tempColL . (string) ($headerRow));
                        $sh1->SetCellValue($start2 . (string) ($headerRow), text('report_helper_php_20') . '');
                        // style
                        $sh1->getStyle($helper->GetNextLetter($lCol) . (string) ($infoRow) . ':' . $tempColL . (string) ($infoRow))
                            ->applyFromArray(Core::Helper()->GetBlackBordersArr());
                        $sh1->getStyle($helper->GetNextLetter($lCol) . (string) ($infoRow) . ':' . $tempColL . (string) ($infoRow))
                            ->getAlignment()->setShrinkToFit(true);
                        $start3Col = $helper->GetPrevLetter($helper->
                                GetPrevLetter($helper->GetPrevLetter($helper->GetPrevLetter($tempColL))));
                        $sh1->getStyle($start3Col . (string) ($infoRow + 1) . ':' . $tempColL . (string) ($infoRow + 1))
                            ->applyFromArray(Core::Helper()->GetBlackBordersArr());
                        $sh1->getStyle($start3Col . (string) ($infoRow + 1) . ':' . $tempColL . (string) ($infoRow + 1))
                            ->applyFromArray(Core::Helper()->GetStandartStylesArr());
                        $sh1->getStyle($start3Col . (string) ($infoRow + 1) . ':' . $tempColL . (string) ($infoRow + 1))
                            ->getAlignment()->setShrinkToFit(true);
                        $sh1->getStyle($helper->GetNextLetter($lCol) . (string) ($infoRow) . ':' . $tempColL . (string) ($infoRow))
                            ->applyFromArray(Core::Helper()->GetStandartStylesArr());
                        break;
                }
            }

            // STYLES
//            $sh1->getColumnDimension($lCol)->setWidth(3);
            $sh1->getColumnDimension('A')->setWidth(3);
            // borderiai
            $sh1->getStyle("A" . (string) $headerRow . ":" . $tempColL . (string) $pupilRow)
                ->applyFromArray(Core::Helper()->GetBlackBordersArr());
            $sh1->getStyle("D" . (string) ($startRow + 1) . ":" . $tempColL . (string) $pupilRow)
                ->applyFromArray(Core::Helper()->GetStandartStylesArr());
            $sh1->getStyle("D" . (string) ($startRow + 1) . ":" . $tempColL . (string) $pupilRow)
                ->getAlignment()->setShrinkToFit(true);

            $sh1->getStyle('B1:B4')->applyFromArray(Core::Helper()->GetHeaderStylesNoWrapArr());
            $sh1->getStyle('A' . $headerRow . ':' . $tempColL . (string) $headerRow)
                ->applyFromArray(array('font' => array('size' => 8, 'bold' => true)));
            $sh1->getStyle('A' . $headerRow . ':' . $tempColL . (string) $headerRow)->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sh1->getStyle('A' . $headerRow . ':' . $tempColL . (string) $headerRow)
                ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sh1->getStyle('A' . (string) ($startRow + 1) . ':B' . $pupilRow)
                ->applyFromArray(Core::Helper()->GetStandartStylesLeftAlignmentArr());
            $sh1->getStyle($startColNext . (string) ($headerRow + 1) . ':' . $tempColL . (string) ($headerRow + 2))
                ->applyFromArray(Core::Helper()->GetStandartStylesLeftAlignmentArrForVertical());
            $sh1->getStyle('A' . (string) ($lastInfoRow + 1) . ':F' . (string) ($lastInfoRow + 1))
                ->applyFromArray(Core::Helper()->GetHeaderStylesNoWrapArr());

            $sh1->getRowDimension($infoRow)->setRowHeight(13);
            $sh1->getRowDimension($infoRow + 1)->setRowHeight(13);
            $sh1->getRowDimension($infoRow + 2)->setRowHeight(13);

            $sh1->SetCellValue('A' . (string) ($lastInfoRow + 1), text('report_helper_php_49') . ": " . $educator_name . ' ' . $educator_surname . ',    ' . text('report_exel_php_20') . ': ');
        }

        // isvalom RAM, pries excelio generavima
        unset($pupils, $subjects, $missedAll, $terms, $pupilsFull);

        $objPHPExcel->setActiveSheetIndex(0);

        // settinam user friendly failo pavadinima
        $file_name = text('report_helper_php_50') . '' . strtolower(unliet($c_label) . '-' . unliet($pupil_label));
        $file_name = str_replace(array(' ', ' (', ') ', ')', '('), array('_', ''), $file_name);
        $valid_chars_arr = range('a', 'z');
        $valid_chars_arr[] = '-';
        $valid_chars_arr[] = '_';
        $f_name_arr = str_split($file_name);
        $new_f_name_arr = array();
        foreach ($f_name_arr as $char) {
            if (in_array($char, $f_name_arr) === true) {
                $new_f_name_arr[] = $char;
            } else {
                $new_f_name_arr[] = '_';
            }
        }
        $baseFileName = '';
        foreach ($new_f_name_arr as $c) {
            $baseFileName .= $c;
        }

        if ($fileType == 1) {
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $baseFileName . '.xlsx"');
            header('Cache-Control: max-age=0');
        } elseif ($fileType == 2) { // kolkas nenaudojamas
            $objWriter = new PHPExcel_Writer_PDF($objPHPExcel);
            header('Content-Type: application/pdf');
            header('Content-Disposition: attachment;filename="' . $baseFileName . '.pdf"');
            header('Cache-Control: max-age=0');
        } else { // po defaultu 2003 exceli generuojam
            $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
            header("Content-Type: application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=\"" . $baseFileName . ".xls\"");
            header("Cache-Control: max-age=0");
        }
        // !!! unsetinti $objPHPExcel kazkurioje vietoje del RAM
        ob_end_clean(); // jei koks buferis uzsilikes butu
        $objWriter->save("php://output");
        unset($objWriter, $objPHPExcel);
    }
}
