<?php

require __DIR__ . '/../vendor/PhpOffice/PhpWord/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::Register();

class PHPWord extends \PhpOffice\PhpWord\PhpWord
{
    public function __construct()
    {
        parent::__construct();
    }
}
