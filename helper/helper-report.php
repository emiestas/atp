<?php

class HReport
{
    public function __construct()
    {
    }

    // Linas: PHPExcel klasei parsiunciam stiliu array baltiems borderiams
    // !!! BUTINAI TURI BUTI INCLUDINTA PHPExcel KALSE
    public function GetWhiteBordersArr()
    {
        return array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => 'ffffff')
                ),
            )
        );
    }

    // Linas: PHPExcel klasei parsiunciam stiliu array juodiems borderiams
    public function GetBlackBordersArr()
    {
        return array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000')
                ),
            )
        );
    }

    // Linas: PHPExcel klasei parsiunciasm stiliu array standartiniams fontams lentelese ir pan
    public function GetStandartStylesArr()
    {
        return array(
            'font' => array(
                'name' => 'Arial',
                'bold' => false,
                'italic' => false,
                'underline' => PHPExcel_Style_Font::UNDERLINE_NONE,
                'strike' => false,
                'size' => 8,
                'color' => array(
                    'rgb' => '000000'
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
//               'wrap'       => true,
            ),
        );
    }

    public function GetStandartStylesSmallArr()
    {
        return array(
            'font' => array(
                'name' => 'Arial',
                'bold' => false,
                'italic' => false,
                'underline' => PHPExcel_Style_Font::UNDERLINE_NONE,
                'strike' => false,
                'size' => 7,
                'color' => array(
                    'rgb' => '000000'
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
//               'wrap'       => true,
            ),
        );
    }

    public function GetStandartStylesLeftAlignmentArr()
    {
        return array(
            'font' => array(
                'name' => 'Arial',
                'bold' => false,
                'italic' => false,
                'underline' => PHPExcel_Style_Font::UNDERLINE_NONE,
                'strike' => false,
                'size' => 8,
                'color' => array(
                    'rgb' => '000000'
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
//               'wrap'       => true,
            ),
        );
    }

    public function GetStandartStylesLeftAlignmentSmallArr()
    {
        return array(
            'font' => array(
                'name' => 'Arial',
                'bold' => false,
                'italic' => false,
                'underline' => PHPExcel_Style_Font::UNDERLINE_NONE,
                'strike' => false,
                'size' => 7,
                'color' => array(
                    'rgb' => '000000'
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
//               'wrap'       => true,
            ),
        );
    }

    public function GetStandartStylesLeftAlignmentArrForVertical()
    {
        return array(
            'font' => array(
                'name' => 'Arial',
                'bold' => false,
                'italic' => false,
                'underline' => PHPExcel_Style_Font::UNDERLINE_NONE,
                'strike' => false,
                'size' => 8,
                'color' => array(
                    'rgb' => '000000'
                )
            ),
        );
    }

    // Linas: PHPExcel klasei parsiunciasm stiliu array header uzrasams
    public function GetHeaderStylesArr()
    {
        return array(
            'font' => array(
                'name' => 'Arial',
                'bold' => false,
                'italic' => false,
                'underline' => PHPExcel_Style_Font::UNDERLINE_NONE,
                'strike' => false,
                'size' => 10,
                'color' => array(
                    'rgb' => '000000'
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap' => true,
            ),
        );
    }

    public function GetHeaderStylesNoWrapArr()
    {
        return array(
            'font' => array(
                'name' => 'Arial',
                'bold' => true,
                'italic' => false,
                'underline' => PHPExcel_Style_Font::UNDERLINE_NONE,
                'strike' => false,
                'size' => 10,
                'color' => array(
                    'rgb' => '000000'
                )
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap' => false,
            ),
        );
    }
}
