<?php

ini_set('display_errors', true);
ini_set('error_reporting', E_ALL);
date_default_timezone_set('Europe/Vilnius');


require 'PhpWord/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::Register();

$phpWord = new \PhpOffice\PhpWord\PhpWord();

$document = $phpWord->loadTemplate('tpl178.docx');
$document->setValue('PAEIT49A1', '');
$document->setValue('PAEIT49A4', '');
$document->setValue('PAEIT49A3', '');
$document->setValue('PAEIT49A2', '');
$document->setValue('PAEIT49AI4', '');
$document->setValue('PAEIT49AI3', '');
$document->setValue('PAEIT11', '');
$document->setValue('PAEIT12', '');
$document->setValue('PAEIT51', '');
$document->setValue('PAEIT56K100K2', '');
$document->setValue('PAEIT56K100K2WSRGTW1', '');
$document->setValue('PAEIT49AI3', '');
$document->setValue('PAEIT57', '');
$document->setValue('PAEIT50K115K8', '');
$document->setValue('PAEIT50K115K6', '');

$document->setImageValue('image1.jpeg', '1.gif');
$document->setImageValue('image2.jpeg', '2.jpg');
$document->setImageValue('image3.jpeg', '3.jpg');
$document->setImageValue('image4.jpeg', '4.jpg');
$document->setImageValue('image5.jpeg', '5.jpg');
$document->setImageValue('image6.jpeg', '1.gif');
$document->setImageValue('image7.jpeg', '2.jpg');
$document->setImageValue('image8.jpeg', '3.jpg');


$name = 'tpl178_gen.docx';
$old_mask = umask(0);
$document->saveAs($name);
umask($old_mask);
die('end');