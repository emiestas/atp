<?php

require_once('../../../inc/mainconf.php');

class atp_update
{
    public $db = null;

    public function __construct()
    {
        $this->db = new dbo();
        $this->db->user = DB_USER;
        $this->db->pass = DB_PASS;
        $this->db->db = DB_DB;

        $this->db->db_connect();
    }

    public function call($function)
    {
        if (method_exists($this, $method = 'up_' . $function) === false) {
            return -1;
        }

        if ($this->{$method}()) {
            return true;
        }
        return false;
    }

    /**
     * TABLE_RELATIONS lentelėja apkeičia dokumento ir tėvinio dokumento laukus
     * @deprecated Nebereikalingas
     * @return boolean
     */
    private function up_swapRelationDocs()
    {
        return true;
        $qry = 'SELECT * FROM ' . PREFIX . 'ATP_TABLE_RELATIONS';
        $result = $this->db->db_query_fast($qry);
        while ($row = $this->db->db_next($result)) {
            list($row['DOCUMENT_PARENT_ID'], $row['TABLE_ID']) = array($row['TABLE_ID'],
                $row['DOCUMENT_PARENT_ID']);
            list($row['COLUMN_PARENT_ID'], $row['COLUMN_ID']) = array($row['COLUMN_ID'],
                $row['COLUMN_PARENT_ID']);
            list($row['COLUMN_PARENT_NAME'], $row['COLUMN_NAME']) = array($row['COLUMN_NAME'],
                $row['COLUMN_PARENT_NAME']);
            $this->db->db_quickupdate(PREFIX . 'ATP_TABLE_RELATIONS', $row, $row['ID'], 'ID');
        }
        return true;
    }

    /**
     * ATP_DOCUMENTS lentelėja nustato STATUS stulpelio reikšmę
     * @deprecated Nebereikalingas
     * @return boolean
     */
    private function up_setDocumnetStatus()
    {
        return true;
//        $atp = prepare_atp();
//        $this->core = new \Atp\Core();
//
//        $documentManager = $this->core->factory->Document();
//
//        $documents = $this->core->logic2->get_document();
//        foreach ($documents as $document) {
//            $arr['STATUS'] = $documentManager->getStatusByFields($document['ID']);
//
//            $this->db->db_quickupdate(PREFIX . 'ATP_DOCUMENTS', $arr, $document['ID'], 'ID');
//        }
//
//        return true;
    }

    /**
     * Asmenis iš ATP_RECORD perkelia į ATP_RECORD_X_WORKER
     * @deprecated Nebereikalingas
     * @return boolean
     */
    private function up_moveRecordPeople()
    {
        return true;

        $qry = 'SELECT A.ID, A.STATUS, B.ID AS WORKER_ID, C.ID AS WORKER_ID_2,
					A.M_PEOPLE_ID, A.M_DEPARTMENT_ID, A.M_PEOPLE_NAME, A.M_PEOPLE_SURNAME,
					A.M_PEOPLE_ID_2, A.M_DEPARTMENT_ID_2, A.M_PEOPLE_NAME_2, A.M_PEOPLE_SURNAME_2
			FROM ' . PREFIX . 'ATP_RECORD A
				LEFT JOIN ' . PREFIX . 'MAIN_WORKERS B ON B.STRUCTURE_ID = A.M_DEPARTMENT_ID AND B.PEOPLE_ID = A.M_PEOPLE_ID
				LEFT JOIN ' . PREFIX . 'MAIN_WORKERS C ON C.STRUCTURE_ID = A.M_DEPARTMENT_ID_2 AND C.PEOPLE_ID = A.M_PEOPLE_ID_2';

        $result = $this->db->db_query_fast($qry);
        $ins = array();
        while ($row = $this->db->db_next($result)) {
            $affixes = array('', '_2');
            foreach ($affixes as $affix) {
                if (empty($row['M_PEOPLE_ID' . $affix]) || empty($row['M_DEPARTMENT_ID' . $affix])) {
                    continue;
                }

                if ($affix === '') {
                    if ($row['STATUS'] === '2') {
                        $type = 'invest';
                        //} elseif($row['STATUS'] === '3') {
                    } else {
                        $type = 'exam';
                    }
                } else {
                    $type = 'next';
                }

                $ins[] = '(' . (int) $row['ID'] . ', ' . (int) $row['WORKER_ID' . $affix] . ', "' . $type . '")';
            }
        }

        if (count($ins)) {
            $qry = 'INSERT INTO ' . PREFIX . 'ATP_RECORD_X_WORKER (RECORD_ID, WORKER_ID, TYPE)
				VALUES ' . join(',', $ins);
            $this->db->db_query_fast($qry);
        }

        return true;
    }

    /**
     * ATP_USER lentelėja nustato WORKER_ID stulpelio reikšmę
     * @deprecated Nebereikalingas
     * @return boolean
     */
    private function up_updateWorkers()
    {
        return true;
        $qry = 'SELECT A.ID, B.ID WORKER_ID
			FROM ' . PREFIX . 'ATP_USER A
				INNER JOIN ' . PREFIX . 'MAIN_WORKERS B
					ON
						B.STRUCTURE_ID = A.M_DEPARTMENT_ID AND
						B.PEOPLE_ID = A.M_PEOPLE_ID';
        $result = $this->db->db_query_fast($qry);
        while ($row = $this->db->db_next($result)) {
            $qry = 'UPDATE ' . PREFIX . 'ATP_USER
				SET
					WORKER_ID = :WORKER_ID
				WHERE
					ID = :ID';
            $this->db->db_query_fast($qry, $row);
        }

        return true;
    }

    /**
     * ATP_CLAUSES lentelėja nustato PENALTY_FIRST stulpelio reikšmę
     * @deprecated Nebereikalingas
     * @return boolean
     */
    private function up_setPenaltyFirst()
    {
        return true;
        $qry = 'UPDATE ' . PREFIX . 'ATP_CLAUSES SET PENALTY_FIRST = PENALTY_MIN / 2 WHERE PENALTY_MIN != 0';
        $this->db->db_query_fast($qry);

        return true;
    }

    /**
     * ATP_CLAUSES lentelėja LABEL nukopijuoja į FIELDS_VALUES
     * @deprecated Nebereikalingas
     * @return boolean
     */
    private function up_copyActLabel()
    {
        return true;

        $qry = 'SELECT ID, LABEL, FIELDS_VALUES
			FROM ' . PREFIX . 'ATP_ACTS';
        $result = $this->db->db_query_fast($qry);
        while ($row = $this->db->db_next($result)) {
            $row['FIELDS_VALUES'] = unserialize($row['FIELDS_VALUES']);
            $row['FIELDS_VALUES'][6] = $row['LABEL'];
            $row['FIELDS_VALUES'] = serialize($row['FIELDS_VALUES']);

            $qry = 'UPDATE ' . PREFIX . 'ATP_ACTS
				SET
					FIELDS_VALUES = :FIELDS_VALUES
				WHERE
					ID = :ID';
            $this->db->db_query_fast($qry, $row);
        }

        return true;
    }

    /**
     * užpildo ATP_RECORD_PAID
     * @deprecated Nebereikalingas
     * @return boolean
     */
    private function up_copyRecordPaid()
    {
        return true;

        $qry = 'SELECT ID, IS_PAID, STATUS
			FROM ' . PREFIX . 'ATP_RECORD';
        $result = $this->db->db_query_fast($qry);
        while ($row = $this->db->db_next($result)) {
            // Jei dokumento tarpinė stadija jau baigta
            if ($row['STATUS'] < 5) {
                continue;
            }

            $arg = array(
                'ID' => (int) $row['ID'],
                'STATUS' => $row['IS_PAID']
            );
            $qry = 'REPLACE INTO ' . PREFIX . 'ATP_RECORD_PAID
					(RECORD_ID, STATUS)
				VALUES
					(:ID, :STATUS)';
            $this->db->db_query_fast($qry, $arg);
        }

        return true;
    }

    private function up_renameRR()
    {
        return true;

        $qry = "ALTER TABLE `" . PREFIX . "ATP_RECORD_RELATIONS` RENAME `" . PREFIX . "ATP_RECORD`";
        $this->db->db_query_fast($qry);

        $qry = "ALTER TABLE `" . PREFIX . "ATP_RECORD_X_WORKER`
			DROP FOREIGN KEY `fk_" . PREFIX . "ATP_RECORD_X_PEOPLE_" . PREFIX . "ATP_RECORD_RELATIONS1`;";
        $this->db->db_query_fast($qry);

        $qry = "ALTER TABLE `" . PREFIX . "ATP_RECORD_X_WORKER`
			DROP INDEX `fk_" . PREFIX . "ATP_RECORD_X_PEOPLE_" . PREFIX . "ATP_RECORD_RELATIONS1_idx`,
			ADD INDEX `fk_" . PREFIX . "ATP_RECORD_X_PEOPLE_" . PREFIX . "ATP_RECORD1_idx` (`RECORD_ID`) COMMENT '';";
        $this->db->db_query_fast($qry);

        $qry = "ALTER TABLE `" . PREFIX . "ATP_RECORD_X_WORKER`
			ADD CONSTRAINT `fk_" . PREFIX . "ATP_RECORD_X_PEOPLE_" . PREFIX . "ATP_RECORD1` FOREIGN KEY (`RECORD_ID`)
				REFERENCES `" . PREFIX . "ATP_RECORD`(`ID`)
					ON DELETE CASCADE
					ON UPDATE NO ACTION;";
        $this->db->db_query_fast($qry);

        return true;
    }

    /**
     * užpildo ATP_ATPR_MESSAGE_WORKER
     * @removed
     * @return boolean
     */
    private function up_sendAtprMessages()
    {
        return true;
        if ($this->db->isTable(PREFIX . 'ATP_ATPR_MESSAGE_WORKER') && $this->db->isEmpty(PREFIX . 'ATP_ATPR_MESSAGE_WORKER')) {
            $this->db->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_ATPR_MESSAGE_WORKER`
                    SELECT
                        NULL                        as ID,
                        1                           as WORKER_ID,
                        0                           as IS_READ,
                        BUSENOS_PASIKEITIMO_DATA    as SEND_DATE,
                        NULL                        as READ_DATE,
                        MESSAGE_ID                  as MESSAGE_ID
                    FROM ' . PREFIX . 'ATP_ATPR_MESSAGE');
        }

        return true;
    }

    /**
     * Prideda ATP būsenas
     * @return boolean
     */
    private function up_addAtprStatuses()
    {
        return true;

        $result = $this->db->db_query_fast('SELECT COUNT(ID) CNT
            FROM ' . PREFIX . 'ATP_STATUS
            WHERE ID IN(64, 65)');
        $data = $this->db->db_num_rows($result);
        if ($data['CNT'] > 0) {
            return false;
        }

        $this->db->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_STATUS
                (ID, LABEL, VALID)
            VALUES
                (64, "Perduotas ATPR", 1),
                (65, "Perduotas pasirašymui", 1)
            ');

        $this->db->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_STATUS_ORDER
                (NR, PARENT_NR, STATUS_ID, DOCUMENT_ID)
            VALUES
                (89, 87, 64, '. \Atp\Document::DOCUMENT_PROTOCOL . '),
                (100, 89, 14, ' . \Atp\Document::DOCUMENT_ACCOMPANYING . '),
                (130, 89, 17, '. \Atp\Document::DOCUMENT_PROTOCOL . '),
                (110, 89, 15, '. \Atp\Document::DOCUMENT_PROTOCOL . '),
                (155, 89, 20, ' . \Atp\Document::DOCUMENT_CASE . '),

                (88, 87, 65, '. \Atp\Document::DOCUMENT_PROTOCOL . '),
                (100, 88, 14, ' . \Atp\Document::DOCUMENT_ACCOMPANYING . '),
                (130, 88, 17, '. \Atp\Document::DOCUMENT_PROTOCOL . '),
                (110, 88, 15, '. \Atp\Document::DOCUMENT_PROTOCOL . '),
                (155, 88, 20, ' . \Atp\Document::DOCUMENT_CASE . '),

                (170, 196, 64, ' . \Atp\Document::DOCUMENT_CASE . '),
                (170, 160, 64, ' . \Atp\Document::DOCUMENT_CASE . '),
                (200, 170, 29, ' . \Atp\Document::DOCUMENT_CASE . ')
            ');

        $this->db->db_query_fast('DELETE
             FROM ' . PREFIX . 'ATP_STATUS_ORDER
             WHERE
                NR = 200 AND
                PARENT_NR IN (160, 196) AND
                STATUS_ID = 29 AND
                DOCUMENT_ID = ' . \Atp\Document::DOCUMENT_CASE . '
             ');

        return true;
    }

    /**
     * išskaido ATP_ACTS.FIELDS_VALUES
     * @return boolean
     */
    private function up_expandActFields()
    {
        return true;

        $result = $this->db->db_query_fast('ALTER TABLE ' . PREFIX . 'ATP_ACTS
            ADD VALUE_6 VARCHAR(5000) NOT NULL AFTER  FIELDS_VALUES,
            ADD VALUE_4 VARCHAR(255) NOT NULL AFTER  FIELDS_VALUES,
            ADD VALUE_3 VARCHAR(255) NOT NULL AFTER  FIELDS_VALUES,
            ADD VALUE_2 VARCHAR(255) NOT NULL AFTER  FIELDS_VALUES,
            ADD VALUE_1 VARCHAR(255) NOT NULL AFTER  FIELDS_VALUES
        ');
        $qry = 'SELECT ID,
            FIELDS_VALUES
            #convert(cast(convert(FIELDS_VALUES using latin1) as binary) using utf8) as FIELDS_VALUES
			FROM ' . PREFIX . 'ATP_ACTS';
        $result = $this->db->db_query_fast($qry);
        while ($row = $this->db->db_next($result)) {
            $fieldsValues = unserialize($row['FIELDS_VALUES']);

            if (empty($fieldsValues)) {
                continue;
            }

            $arg = array(
                'ID' => (int) $row['ID'],
                'VALUE_1' => $fieldsValues[1],
                'VALUE_2' => $fieldsValues[2],
                'VALUE_3' => $fieldsValues[3],
                'VALUE_4' => $fieldsValues[4],
                'VALUE_6' => $fieldsValues[6]
            );
            $qry = 'UPDATE ' . PREFIX . 'ATP_ACTS
                SET
                    VALUE_1 = :VALUE_1,
                    VALUE_2 = :VALUE_2,
                    VALUE_3 = :VALUE_3,
                    VALUE_4 = :VALUE_4,
                    VALUE_6 = :VALUE_6
				WHERE
					ID = :ID';
            $this->db->db_query_fast($qry, $arg);
        }

        return true;
    }

    /**
     * užpildo ATP_FIELD_CONFIGURATION_TYPE
     * @return boolean
     */
    private function up_populateFieldConfigurationTypes()
    {
        return true;

        $this->db->db_query_fast('INSERT INTO `' . PREFIX . 'ATP_FIELD_CONFIGURATION_TYPE`
                (NAME, LABEL)
            VALUES
                ("person_code", "Pažeidėjo asmens kodo laukas"),
                ("person_declared_address", "Pažeidėjo deklaruotos gyvenamosios vietos adresas"),
                ("company_address", "Pažeidėjo adresas (juridinio)"),
                ("person_residential_address", "Pažeidėjo gyvenamosios vietos adresas (kitas adresas)"),
                ("person_name", "Pažeidėjo vardas"),
                ("person_surname", "Pažeidėjo pavardė"),
                ("person_nationality", "Pažeidėjo pilietybė"),
                ("person_gender", "Pažeidėjo lytis"),
                ("person_document", "Pažeidėjo tapatybes dokumentas"),
                ("company_name", "Įmonės pavadinimas"),
                ("violation_date", "Pažeidimo datos laukas"),
                ("violation_time", "Pažeidimo laiko laukas"),
                ("violation_place", "Pažeidimo vietos laukas"),
                ("violation_city", "Pažeidimo miesto laukas"),
                ("violation_place_more", "Pažeidimo vietos papildomos informacijos laukas"),
                ("fill_date", "Surašymo datos laukas"),
                ("witness_name", "Pareigūnas nustatęs ATP (vardas)"),
                ("witness_surname", "Pareigūnas nustatęs ATP (pavardė)"),
                ("witness_institution", "Pareigūnas nustatęs ATP (institucija)"),
                ("decision_date", "Nutarimo priėmimo datos laukas"),
                ("appealed_date", "Apeliacijos data"),
                ("appeal_stc_date", "Išsiuntimo teismui data"),
                ("appeal_ccd_date", "Sprendimo įsiteisėjimo data"),
                ("penalty_sum", "Baudos sumos laukas"),
                ("class_type", "Pažeidimo tipo laukas"),
                ("avilys_date", "Registracijos avilyje datos laukas"),
                ("avilys_number", "Registracijos avilyje numerio laukas"),
                ("examinator", "Nagrinėjantis pareigūnas"),
                ("examinator_cabinet", "Nagrinėjančio pareigūno kabinetas"),
                ("examinator_phone", "Nagrinėjančio pareigūno telefonas"),
                ("examinator_email", "Nagrinėjančio pareigūno el.paštas"),
                ("examinator_institution_name", "Nagrinėjanti institucija"),
                ("examinator_unit", "Nagrinėjantis dalinys"),
                ("examinator_subunit", "Nagrinėjantis padalinys"),
                ("examinator_institution_address", "Nagrinėjančios institucijos adresas"),
                ("examinator_date", "Nagrinėjimo data"),
                ("examinator_start_time", "Nagrinėjimo pradžios laikas"),
                ("examinator_finish_time", "Nagrinėjimo pabaigos laikas")');

        return true;
    }

    /**
     * papildo ATP_FIELD_CONFIGURATION_TYPE
     * @return boolean
     */
    private function up_populateFieldConfigurationTypes2()
    {
        return true;

        $this->db->db_query_fast('INSERT INTO `' . PREFIX . 'ATP_FIELD_CONFIGURATION_TYPE`
                (NAME, LABEL)
            VALUES
                ("nutarimas_sprendimas", "Nutarimas: sprendimas"),
                ("nutarimas_lengvinantiAplinkybe", "Nutarimas: lengvinanti aplinkybė"),
                ("nutarimas_sunkinantiAplinkybe", "Nutarimas: sunkinanti aplinkybė"),
                ("nutarimas_nuobaudosRusis", "Nutarimas: nuobaudos rūšis"),
                ("nutarimas_apskundusiSalis", "Nutarimas: apskundusi šalis"),
                ("nutarimas_apskundimoData", "Nutarimas: apskundimo data"),
                ("nutarimas_apskundusiInstitucija", "Nutarimas: apskundusi institucija");
            ');

        return true;
    }

    /**
     * užpildo ATP_FIELD_CONFIGURATION
     * @return boolean
     */
    private function up_migrateDocumentFieldsConfiguration()
    {
        return true;

        $typeMap = [];
        $columnNames = [];

        $result = $this->db->db_query_fast('
            SELECT
                ID, NAME
			FROM ' . PREFIX . 'ATP_FIELD_CONFIGURATION_TYPE');

        $typeCount = $this->db->db_num_rows($result);
        if ($typeCount) {
            for ($i = 0; $i < $typeCount; $i++) {
                $row = $this->db->db_next($result);

                $typeMap[$row['ID']] = $row;
                $typeMap[$row['NAME']] = $row;
                $columnNames[] = strtoupper($row['NAME']);
            }

            $result = $this->db->db_query_fast('
                SELECT
                    ' . join(',', $columnNames) . '
                FROM ' . PREFIX . 'ATP_DOCUMENTS A');

            $resultData = [];
            while ($row = $this->db->db_next($result)) {
                foreach ($row as $columnName => $fieldId) {
                    if (empty($fieldId)) {
                        continue;
                    }

                    $typeName = strtolower($columnName);
                    if (isset($typeMap[$typeName]) === false) {
                        throw new \Exception('Configuration type "' . $typeName . '" was not found');
                    }

                    $typeId = $typeMap[$typeName]['ID'];

                    $resultData[] = [
                        'FIELD_ID' => $fieldId,
                        'TYPE_ID' => $typeId
                    ];
                }
            }

            $resultData = array_unique($resultData, SORT_REGULAR);

            if (count($resultData)) {
                $db = $this->db;
                $rows = array_map(function($data) use ($db) {
                    return '("' . join('","', array_map([$db, 'db_escape'], $data)) . '")';
                }, $resultData);

                $columns = array_keys(array_pop($resultData));

                $this->db->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_FIELD_CONFIGURATION
                        (' . join(',', $columns) . ')
                    VALUES ' . join(',', $rows));
            }
        }

        return true;
    }

    private function getOldConfigurationFields()
    {
        return [
            'PERSON_CODE',
            'PERSON_ADDRESS',
            'COMPANY_ADDRESS',
            'OTHER_ADDRESS',
            'PERSON_NAME',
            'PERSON_SURNAME',
            'PERSON_NATIONALITY',
            'PERSON_GENDER',
            'PERSON_DOCUMENT',
            'COMPANY_NAME',
            'VIOLATION_DATE',
            'VIOLATION_TIME',
            'VIOLATION_PLACE',
            'VIOLATION_CITY',
            'VIOLATION_PLACE_MORE',
            'FILL_DATE',
            'WITNESS_NAME',
            'WITNESS_SURNAME',
            'WITNESS_INSTITUTION',
            'DECISION_DATE',
            'APPEALED_DATE',
            'APPEAL_STC_DATE',
            'APPEAL_CCD_DATE',
            'PENALTY_SUM',
            'CLASS_TYPE',
            'AVILYS_DATE',
            'AVILYS_NUMBER',
            'EXAMINATOR',
            'EXAMINATOR_CABINET',
            'EXAMINATOR_PHONE',
            'EXAMINATOR_EMAIL',
            'EXAMINATOR_INSTITUTION_NAME',
            'EXAMINATOR_UNIT',
            'EXAMINATOR_SUBUNIT',
            'EXAMINATOR_INSTITUTION_ADDRESS',
            'EXAMINATOR_DATE',
            'EXAMINATOR_START_TIME',
            'EXAMINATOR_FINISH_TIME'
        ];
    }

    /**
     * Pašalina senus konfiguruojamus dokumento laukus.
     * 
     * @return boolean
     */
    private function up_removeOldConfigurationFields()
    {
        return true;

        $backupTable = PREFIX . 'ATP_DOCUMENTS_BACKUP_' . time();
        $this->db->db_query_fast('CREATE TABLE ' . $backupTable . ' LIKE ' . PREFIX . 'ATP_DOCUMENTS');
        $this->db->db_query_fast('INSERT INTO ' . $backupTable . ' SELECT * FROM ' . PREFIX . 'ATP_DOCUMENTS');

        $fieldNames = $this->getOldConfigurationFields();
        foreach ($fieldNames as $fieldName) {
            if ($this->db->isColumn(PREFIX . 'ATP_DOCUMENTS', $fieldName)) {
                $this->db->db_query_fast('
                    ALTER TABLE ' . PREFIX . 'ATP_DOCUMENTS
                        DROP ' . $fieldName . '');
            }
        }

        return true;
    }

    /**
     * Prideda mini dokumento būsenas
     * @return boolean
     */
    private function up_addMiniStatuses()
    {
        return true;

        $result = $this->db->db_query_fast('SELECT COUNT(ID) CNT
            FROM ' . PREFIX . 'ATP_STATUS_ORDER
            WHERE STATUS_ID IN(26, 28)');
        $data = $this->db->db_num_rows($result);
        if ($data['CNT'] > 0) {
            return false;
        }

        $this->db->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_STATUS_ORDER
                (NR, PARENT_NR, STATUS_ID, DOCUMENT_ID)
            VALUES
                (26, 0, 2, 67),
                (28, 26, 4, 67),
                (30, 28, 2, ' . \Atp\Document::DOCUMENT_INFRACTION . ')
            ');

        return true;
    }

    /**
     * Klasifikatorių nested set hierarchija.
     * @return boolean
     */
    private function up_createClassificatorTree()
    {
        return true;

        ini_set('memory_limit', '700M');
        ini_set('max_execution_time', '7200');

        prepare_atp();

        $this->core = new \Atp\Core();

        /* @var $entityManager \Doctrine\ORM\EntityManager */
        $entityManager = $this->core->factory->Doctrine();

        /* @var $repository \Atp\Repository\ClassificatorRepository */
        $repository = $entityManager->getRepository('\Atp\Entity\Classificator');

        // recover tree
        $repository->recover();
        // reordering the tree
//        $repository->reorder(null/* reorder starting from parent */, 'label');
        // it will reorder all tree node left-right values by the title
        $entityManager->flush();

        $entityManager->clear(); // ensures cache clean
//        return $repository->verify();
        return true;
    }

    /**
     * Klasifikatorių gylio perskaičiavimas.
     * @return boolean
     */
    private function up_createClassificatorDepth()
    {
        return true;

        prepare_atp();

        $this->core = new \Atp\Core();

        /* @var $entityManager \Doctrine\ORM\EntityManager */
        $entityManager = $this->core->factory->Doctrine();

        try {
            $s = microtime(true);
            $entityManager->getConnection()->beginTransaction(); // suspend auto-commit

            $query = $entityManager->createQuery('
                SELECT
                    classificator.id, parent.id parentId
                FROM
                    Atp\Entity\Classificator classificator
                        LEFT JOIN classificator.parent parent');

            $classificators = $query->getArrayResult();

            $directChildrenMap = [];
            foreach ($classificators as $classificator) {
                $directChildrenMap[(int) $classificator['parentId']][] = $classificator['id'];
            }

            $addDepth = function(&$mappedElements, $id_field, $parent_id_field, $directChildrenMap, $parent_id = 0, $depth = 0) use(&$addDepth) {
                if (isset($directChildrenMap[$parent_id])) {
                    foreach ($directChildrenMap[$parent_id] as $childId) {
                        $mappedElements[$childId]['depth'] = $depth;

                        $addDepth($mappedElements, $id_field, $parent_id_field, $directChildrenMap, $childId, $depth + 1);
                    }
                }
            };

            $remapedCassificators = [];
            foreach ($classificators as $classificator) {
                $remapedCassificators[$classificator['id']] = $classificator;
            }
            $addDepth($remapedCassificators, 'id', 'parentId', $directChildrenMap);
            $classificators = $remapedCassificators;

            foreach ($classificators as $classificator) {
                $query = $entityManager->createQuery('
                    UPDATE
                        Atp\Entity\Classificator classificator
                    SET
                        classificator.level = :level
                    WHERE
                        classificator.id = :id')
                    ->setParameter('level', $classificator['depth'])
                    ->setParameter('id', $classificator['id']);
                $query->execute();
            }

            $entityManager->getConnection()->commit();

            $e = microtime(true);
            echo 'Executed in: ' . ($e - $s);
        } catch (\Exception $e) {
            $entityManager->getConnection()->rollBack();
            throw $e;
        }

        return true;
    }

    /**
     * Klasifikatorių closure table hierarchija
     * @return boolean
     */
    private function up_createClassificatorClosureTree()
    {
        return true;

        ini_set('memory_limit', '700M'); // might not need so much
        ini_set('max_execution_time', '600'); // 80k records. Localhost: 22s, vilnius: 140s

        prepare_atp();

        $this->core = new \Atp\Core();

        /* @var $entityManager \Doctrine\ORM\EntityManager */
        $entityManager = $this->core->factory->Doctrine();

        try {
            $s = microtime(true);
            $entityManager->getConnection()->beginTransaction(); // suspend auto-commit

            /* @var $repository \Atp\Repository\ClassificatorRepository */
            $query = $entityManager->createQuery('
                SELECT
                    classificator.id, parent.id parentId, classificator.level as depth
                FROM
                    Atp\Entity\Classificator classificator
                        LEFT JOIN classificator.parent parent
                    ');

            $classificators = $query->getArrayResult();

            $directChildrenMap = [];
            foreach ($classificators as $classificator) {
                $directChildrenMap[(int) $classificator['parentId']][] = $classificator['id'];
            }
            $remapedCassificators = [];
            foreach ($classificators as $classificator) {
                $remapedCassificators[$classificator['id']] = $classificator;
            }
            $classificators = $remapedCassificators;

            $getChildren = function($nodeId, &$nodes, &$directChildrenMap) use (&$getChildren) {
                $children = [];
                if (isset($directChildrenMap[$nodeId])) {
                    foreach ($directChildrenMap[$nodeId] as $childId) {
                        $children[] = $childId;

                        foreach ($getChildren($nodes[$childId]['id'], $nodes, $directChildrenMap) as $childId) {
                            $children[] = $childId;
                        }
                    }
                }

                return $children;
            };

            $connection = $entityManager->getConnection();
            foreach ($classificators as $classificator) {
                $connection
                    ->insert('TST_ATP_CLASSIFICATOR_CLOSURE', array(
                        'ancestor' => $classificator['id'],
                        'descendant' => $classificator['id'],
                        'depth' => 0
                ));

                $children = $getChildren($classificator['id'], $remapedCassificators, $directChildrenMap);
                foreach ($children as $childId) {
                    $child = $classificators[$childId];

                    $connection
                        ->insert('TST_ATP_CLASSIFICATOR_CLOSURE', array(
                            'ancestor' => $classificator['id'],
                            'descendant' => $child['id'],
                            'depth' => $child['depth'] - $classificator['depth']
                    ));
                }
            }

            $entityManager->getConnection()->commit();

            $e = microtime(true);
            echo 'Executed in: ' . ($e - $s);
        } catch (\Exception $e) {
            $entityManager->getConnection()->rollBack();
            throw $e;
        }

        return true;
    }

    /**
     * ATP_CLAUSES lentelėja nustato PENALTY_FIRST stulpelio reikšmę (vėl)
     * @deprecated Nebereikalingas
     * @return boolean
     */
    private function up_setPenaltyFirst2()
    {
        return true;
        $qry = 'UPDATE ' . PREFIX . 'ATP_CLAUSES SET PENALTY_FIRST = FLOOR(PENALTY_MIN / 2) WHERE PENALTY_MIN != 0';
        $this->db->db_query_fast($qry);

        return true;
    }

    /**
     * ATP_CLAUSES lentelėja perkelia CLASS_TYPE stulpelio reikšmes
     * @deprecated Nebereikalingas
     * @return boolean
     */
    private function up_migrateClauseClassificators()
    {
        return true;
        $this->db->db_query_fast("CREATE TABLE `" . PREFIX . "ATP_CLAUSE_CLASSIFICATOR_XREF` (
            `ID`                INT UNSIGNED NOT NULL AUTO_INCREMENT,
            `CLAUSE_ID`         INT UNSIGNED NOT NULL,
            `CLASSIFICATOR_ID`  INT UNSIGNED NOT NULL,
            PRIMARY KEY     (`ID`),
            KEY `_CLAUSE_ID`     (`CLAUSE_ID`),
            KEY `_CLASSIFICATOR_ID`     (`CLASSIFICATOR_ID`),
            CONSTRAINT `" . PREFIX . "ATP_CLAUSE_CLASSIFICATOR_XREF_ibfk_1`
                FOREIGN KEY (`CLAUSE_ID`)
                REFERENCES `" . PREFIX . "ATP_CLAUSES` (`ID`)
                ON DELETE CASCADE
                ON UPDATE RESTRICT,
            CONSTRAINT `" . PREFIX . "ATP_CLAUSE_CLASSIFICATOR_XREF_ibfk_2`
                FOREIGN KEY (`CLASSIFICATOR_ID`)
                REFERENCES `" . PREFIX . "ATP_CLASSIFICATORS` (`ID`)
                ON DELETE CASCADE
                ON UPDATE RESTRICT
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        $this->db->db_query_fast('
            INSERT INTO ' . PREFIX . 'ATP_CLAUSE_CLASSIFICATOR_XREF
                (CLAUSE_ID, CLASSIFICATOR_ID)
                SELECT ID, CLASS_TYPE
                FROM
                    ' . PREFIX . 'ATP_CLAUSES
                WHERE
                    CLASS_TYPE != 0
                ');

        return true;
    }




    /**
     * Atnaujina ATP būsenas.
     * Pranešime, Šaukime ir Lydraštyje pridėta būsena "Perduota pasirašymui".
     * Šaukime ir Lydraštyje pridėta būsena "Naujas šaukimas"/"Naujas lydraštis".
     * Pranešime iš Bylos pridėta būsena "Naujas pranešimas".
     * @return boolean
     */
    private function up_updateStatuses()
    {
        return true;

        // Ar egzistuoja seno būsenos
        $result = $this->db->db_query_fast('SELECT COUNT(ID) CNT
            FROM ' . PREFIX . 'ATP_STATUS
            WHERE ID IN(6, 10, 81, 76, 14, 25, 26)');
        $data = $this->db->db_num_rows($result);
        if ($data['CNT'] > 0) {
            return false;
        }

        // Nauja lydraščio būsena
        $this->db->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_STATUS
                (ID, LABEL, VALID)
            VALUES
                (66, "Naujas lydraštis", 1)
            ');

        // Įdedami nauji ryšiai
        $this->db->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_STATUS_ORDER
                (NR, PARENT_NR, STATUS_ID, DOCUMENT_ID)
            VALUES
                (47, 45, 65, '. \Atp\Document::DOCUMENT_REPORT . '),
                (50, 47, 7, '. \Atp\Document::DOCUMENT_REPORT . '),
                (77, 75, 65, '. \Atp\Document::DOCUMENT_SUMMONS . '),
                (80, 77, 10, '. \Atp\Document::DOCUMENT_SUMMONS . '),
                (94, 88, 66, '. \Atp\Document::DOCUMENT_ACCOMPANYING . '),
                (94, 89, 66, '. \Atp\Document::DOCUMENT_ACCOMPANYING . '),
                (94, 90, 66, '. \Atp\Document::DOCUMENT_ACCOMPANYING . '),
                (96, 94, 65, '. \Atp\Document::DOCUMENT_ACCOMPANYING . '),
                (100, 96, 14, '. \Atp\Document::DOCUMENT_ACCOMPANYING . '),
                (174, 155, 43, '. \Atp\Document::DOCUMENT_REPORT . '),
                (178, 174, 65, '. \Atp\Document::DOCUMENT_REPORT . '),
                (180, 178, 21, '. \Atp\Document::DOCUMENT_REPORT . '),
                (190, 178, 22, '. \Atp\Document::DOCUMENT_REPORT . ')
                (45, 40, 43, '. \Atp\Document::DOCUMENT_REPORT . ')
            ');

        // Pašalinami nebereikalingi ryšiai
        $this->db->db_query_fast('DELETE
             FROM ' . PREFIX . 'ATP_STATUS_ORDER
             WHERE
                (
                    NR = 50 AND
                    PARENT_NR = 45 AND
                    STATUS_ID = 7 AND
                    DOCUMENT_ID = ' . \Atp\Document::DOCUMENT_REPORT . '
                ) OR (
                    NR = 80 AND
                    PARENT_NR = 75 AND
                    STATUS_ID = 10 AND
                    DOCUMENT_ID = ' . \Atp\Document::DOCUMENT_SUMMONS . '
                ) OR (
                    NR = 100 AND
                    PARENT_NR IN(88,89,90) AND
                    STATUS_ID = 14 AND
                    DOCUMENT_ID = ' . \Atp\Document::DOCUMENT_ACCOMPANYING . '
                ) OR (
                    NR = 180 AND
                    PARENT_NR = 155 AND
                    STATUS_ID = 21 AND
                    DOCUMENT_ID = ' . \Atp\Document::DOCUMENT_REPORT . '
                ) OR (
                    NR = 190 AND
                    PARENT_NR = 155 AND
                    STATUS_ID = 22 AND
                    DOCUMENT_ID = ' . \Atp\Document::DOCUMENT_REPORT . '
                )
             ');

        return true;
    }
}
ini_set('display_errors', true); 
require_once(__DIR__ . '/../autoload.php');
ini_set('display_errors', true);
header('Contet-Type: text');
if (empty($_GET['update']) === false) {
    $up = new atp_update();
    echo 'Calling "' . $_GET['update'] . '"' . PHP_EOL;
    $result = $up->call($_GET['update']);

    if ($result === true) {
        echo '"' . $_GET['update'] . '" executed' . PHP_EOL;
    } elseif ($result === false) {
        echo '"' . $_GET['update'] . '" failed' . PHP_EOL;
    } else {
        echo '"' . $_GET['update'] . '" not found' . PHP_EOL;
    }
} else {
    echo "No update set";
}

exit;
