<?php

require_once(__DIR__ . '/../../vendor/autoload.php');

foreach (glob(__DIR__ . '/class/class-*.php') as $file) {
    require_once($file);
}

spl_autoload_register(function ($class) {
    if (strpos($class, '\\') !== false) {
        $separator = '\\';
    } else {
        $separator = '_';
    }

    $expl = explode($separator, $class);
    if ($expl[0] === 'Atp') {
        $path = __DIR__ . '/class/';

        if ($class === '\Atp\Core') {
            $path .= 'class-core.php';
        } else {
            unset($expl[0]);
            $path .= join('/', array_slice($expl, 0, -1)) . '/' . end($expl) . '.php';
        }

        if (is_file($path) === false) {
            throw new \Exception('Class ' . $class . ' not found at ' . $path);
        }

        require_once($path);
    }
});

require_once(__DIR__ . '/../atpr/autoload.php');
