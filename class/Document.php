<?php

namespace Atp;

use \Atp\Entity\Document as DocumentEntity;
use \Atp\Entity\FieldConfiguration;
use \Atp\Entity\FieldConfigurationType;
use \Atp\Core;
use \Doctrine\DBAL\Types\Type;

class Document
{
    // Protokolo
    const PROTOCOL_WITH_AN = 87;
    // Byla (senas)
    const PROTOCOL_WITHOUT_AN = 88;

    // Byla (senas)
    const DOCUMENT_CASE_OLD = 9;

    // -
    const DOCUMENT_24 = 24;

    // Protokolas
    const DOCUMENT_PROTOCOL = 36;

    // Pirmas dokumentas
    const DOCUMENT_43 = 43;

    // Antras dokumentas
    const DOCUMENT_44 = 44;

    // Trečia dokumentas
    const DOCUMENT_45 = 45;

    // Šaukimas
    const DOCUMENT_SUMMONS = 48;

    // Tarnybinis pranešimas
    const DOCUMENT_51 = 51;

    // Pažeidimo duomenys
    const DOCUMENT_INFRACTION = 52;

    // Lydraštis
    const DOCUMENT_ACCOMPANYING = 53;

    // Pranešimas
    const DOCUMENT_REPORT = 54;

    // Sprendimas
    const DOCUMENT_55 = 55;

    // SISP pažeidimų duomenys
    const DOCUMENT_INFRACTION_SISP = 56;

    // Byla
    const DOCUMENT_CASE = 57;

    // Protokolas AK
    const DOCUMENT_58 = 58;

    // Nutarimas AK
    const DOCUMENT_59 = 59;

    // Posėdžio protokolas AK
    const DOCUMENT_61 = 61;

    // Lydraštis AK
    const DOCUMENT_62 = 62;

    // Atsiliepimas apylinkei AK
    const DOCUMENT_RESPONSE_AK = 63;

    // Informacija antstolei AK
    const DOCUMENT_64 = 64;

    // Kvietimas AK
    const DOCUMENT_65 = 65;

    // Raštas AK
    const DOCUMENT_66 = 66;

    // Pažeidimo duomenys mini
    const DOCUMENT_INFRACTION_MINI = 67;

    // Skundas
    const DOCUMENT_68 = 68;

    /**
     * @var Core $atp
     */
    private $atp;

    /**
     * @var \Atp\Cache
     */
    private $cache;

    /**
     * @param Core $atp
     */
    public function __construct(Core $atp)
    {
        $this->atp = $atp;
        $this->cache = $this->atp->factory->Cache();
    }

    /**
     * Dokumento informacija
     * @param int $documentId Dokumento ID
     * @param boolean $recache [optiona] Default: false
     * @return \Atp\Entity\Document
     * @throws \Exception
     */
    public function get($documentId, $recache = false)
    {
        /* @var $repository \Atp\Repository\DocumentRepository */
        $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Document');
        return $repository->getById($documentId);
    }

//    public function saveData(\Atp\Entity\DocumentData $documentData)
//    {
//        $data = [
//            'ID' => (int) $documentData->getId(),
//            'RECORD_ID' => $documentData->getNumber(),
//            'RECORD_MAIN_ID' => $documentData->getMainNumber(),
//            'RECORD_VERSION' => (int) $documentData->getVersion(),
//            'RECORD_LAST' => (int) $documentData->getIsLast(),
//            'CREATE_DATE' => $documentData->getCreateDate()->format('Y-m-d H:i:s'),
//        ];
//        $id = $this->core->db_ATP_insert(
//            PREFIX . 'ATP_' . $documentData->getId(), $data, 'replace'
//        );
//
//        return $id;
//    }

    /**
     * @param int $documentId
     * @return boolean
     */
    public function delete($documentId)
    {
        $document = $this->Get($documentId);

        if (empty($document) === false) {
            /* @var $repository \Atp\Repository\DocumentRepository */
            $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Document');
            $repository->removeById($documentId);

            $documentName = $this->atp->_prefix->table . $document->getId();
            $this->delete_table_structure(['ITYPE' => 'DOC', 'ITEM_ID' => (int) $document->getId()]);
            $this->atp->db_query('DELETE
                FROM ' . PREFIX . 'ATP_TABLE_RELATIONS
                WHERE
                    DOCUMENT_ID = ' . $document->getId() . ' OR
                    DOCUMENT_PARENT_ID = ' . $document->getParent()->getId());

            $this->atp->db_query('DROP TABLE IF EXISTS ' . PREFIX . 'ATP_' . $documentName);

            return true;
        }

        return false;
    }

    /**
     * Dokumento nustatymai
     * @param int $documentId Dokumento ID
     * @param boolean $returnMultiple Return multiple fields ids for configuration type. Default: FALSE.
     * @return object
     * @throws \Exception
     */
    public function getOptions($documentId, $returnMultiple = false)
    {
        $cacheName = __METHOD__;
        $data = $this->cache->get($cacheName, serialize(func_get_args()));
		if ($data !== false) {
            return $data;
        }

        $data = $this->atp->logic2->get_document($documentId);

        $exclude = ['ID', 'PARENT_ID', 'LABEL', 'UPDATE_DATE', 'CREATE_DATE', 'VALID'];
        $count = count($exclude);
        for ($i = 0; $i < $count; $i++) {
            unset($data[$exclude[$i]]);
        }

        array_walk($data, function (&$val) {
            if (ctype_digit((string) $val)) {
                $val = (int) $val;
            }
        });

        $configuredFields = $this->getConfiguredFields($documentId);
		foreach ($configuredFields as $configuredField) {
			$typeName = $configuredField->getType()->getName();

            if ($returnMultiple) {
                $data[$typeName][] = (int) $configuredField->getField()->getId();
            } else {
                $data[$typeName] = (int) $configuredField->getField()->getId();
            }
        }

        $data = (object) array_change_key_case($data, CASE_LOWER);
        $this->cache->set($cacheName, serialize(func_get_args()), $data);

        return $data;
    }

    /**
     * Get all configured fields by document and configuration type.
     *
     * @param int $documentId Document ID.
     * @param null|FieldConfigurationType|int|string $type FieldConfigurationType object, entity id or unique name identifier.
     * @return FieldConfiguration[]
     * @throws \InvalidArgumentException
     */
    public function getConfiguredFields($documentId, $type = null)
    {
        $fieldsIds = array_values($this->atp->factory->Field($this->atp)
                ->getFieldsMapByDocument($documentId, 'assoc'));
        if (empty($fieldsIds)) {
            return;
        }

        $queryBuilder = $this->atp->factory->Doctrine()
            ->getRepository('Atp\Entity\FieldConfiguration')
            ->createQueryBuilder('configuration')
            ->select(array('configuration', 'type', 'field'))
            ->innerJoin('configuration.field', 'field')
            ->innerJoin('configuration.type', 'type')
            ->where('field.id IN(:fieldsIds)')
            ->setParameter('fieldsIds', $fieldsIds);
        
        if ($type !== null) {
            if ($type instanceof FieldConfigurationType) {
                $type = $type->getId();
            }

            if (ctype_digit((string) $type)) {
                $queryBuilder->andWhere('type.id = :typeId')
                    ->setParameter('typeId', $type, Type::INTEGER);
            } elseif (is_string($type)) {
                $queryBuilder->andWhere('type.name = :typeName')
                    ->setParameter('typeName', $type, Type::STRING);
            } else {
                throw new \InvalidArgumentException(sprintf(
                    'Expected argument of type "%s", "%s" given', join('","', [FieldConfigurationType::class,
                    'integer', 'string']), is_object($type) ? get_class($type) : gettype($type)
                ));
            }
        }
        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * Get first configured field by document and configuration type.
     *
     * @param int $documentId Document ID.
     * @param FieldConfigurationType|int|string $type FieldConfigurationType object, entity id or unique name identifier.
     * @return FieldConfiguration
     * @throws \InvalidArgumentException
     */
    public function getFirstConfiguredFields($documentId, $type)
    {
        if (empty($type)) {
            throw new \InvalidArgumentException(sprintf(
                'Expected argument of type "%s", "%s" given', join('","', [FieldConfigurationType::class,
                'integer', 'string']), is_object($type) ? get_class($type) : gettype($type)
            ));
        }

        $configurations = $this->getConfiguredFields($documentId, $type);
        if (empty($configurations) === false) {
            return array_shift($configurations);
        }

        return;
    }
//    /**
//     * Get fields structures of document.
//     *
//     * @param int $documentId Document ID.
//     * @return \Atp\Entity\Field[]
//     */
//    public function getFields($documentId)
//    {
//        return $this->atp->factory->Field($this->atp)->getFieldsByDocument($documentId);
//    }
//
//    /**
//     * Get fields structures map of document. $map['{fieldId}'] = '{fieldName}'; and vice versa.
//     *
//     * @param int $documentId Document ID.
//     * @param string $type [optional] Map type. 'assoc' (name as index, id as value) arba 'enum' (id as index, name as value). <b>Default:</b> 'enum'.
//     * @return strin[]|int[]
//     */
//    public function getFieldsMap($documentId, $type = 'enum')
//    {
//        return $this->atp->factory->Field($this->atp)->getFieldsMapByDocument($documentId);
//    }
//
//    /**
//     * Naujo dokumento statusas pagal laukų kompetencijas.
//     *
//     * @todo Nenaudoti. Statusas "nesvarbu" (1) sumaišitas su dokumento įrašo statusu "nutrauktas" (1).
//     * @depricated Nenaudoti. Statusas "nesvarbu" (1) sumaišitas su dokumento įrašo statusu "nutrauktas" (1).
//     * @param int $documentId Dokumento ID
//     * @return int 1 - nesvarbu; 2 - tyrimas; 3 - nagrinėjimas.
//     */
//    public function getStatus($documentId)
//    {
//        $document = $this->get($documentId);
//        if (empty($document) === false) {
//            return $document->getStatus();
//        }
//
//        return false;
//    }

    /**
     * Pagal dokumento pagrindinių laukų tipą (dokumento administravime "TIRIA", "NAGRINĖJA" laukų reikšmes) nustato dokumento stausą.
     *
     * @todo Nenaudoti. Dokumento statusas "nesvarbu" (1) sumaišitas su dokumento įrašo statusu "nutrauktas" (1).
     * @depricated Nenaudoti. Dokumento statusas "nesvarbu" (1) sumaišitas su dokumento įrašo statusu "nutrauktas" (1).
     * @param int $documentId Dokumento ID
     * @return int 1 - visi; 2 - tyrimas; 3 - nagrinėjimas.
     */
    private function getStatusByFields($documentId)
    {
        $fields = $this->atp->logic2->get_fields([
            'ITEM_ID' => $documentId,
            'ITYPE' => 'DOC'
        ]);
        $doc_status = \Atp\Entity\Document::STATUS_NONE;

        if (count($fields)) {
            $arr = [];
            foreach ($fields as &$field) {
                $arr[$field['COMPETENCE']] = $field['COMPETENCE'];
            }

            if ((isset($arr[\Atp\Document\Field::COMPETENCE_INVESTIGATE]) && isset($arr[\Atp\Document\Field::COMPETENCE_EXAMINATE])) || isset($arr[\Atp\Document\Field::COMPETENCE_ALL])) {
                $doc_status = \Atp\Entity\Document::STATUS_BOTH;
            } elseif (isset($arr[\Atp\Document\Field::COMPETENCE_INVESTIGATE])) {
                $doc_status = \Atp\Entity\Document::STATUS_INVEST;
            } elseif (isset($arr[\Atp\Document\Field::COMPETENCE_EXAMINATE])) {
                $doc_status = \Atp\Entity\Document::STATUS_EXAM;
            }
        }

        return $doc_status;
    }

    /**
     * Gauna terminų duomenis
     * @author Justinas Malūkas
     * @param array $params
     * @param int $params['ID'] Įrašo ID
     * @param int $params['DOCUMENT_ID'] Dokumento ID
     * @param int $params['FIELD_ID'] Dokumento lauko ID
     * @param int $params['DAYS'] Dienų skaičius
     * @param int $params['TYPE'] Dienų skaičiavimo tipas
     * @return array
     */
    public function getTerms($params)
    {
        $where = [];
        $arg = [];
        foreach ($params as $key => $value) {
            if (is_int($key) === true) {
                $where[] = ':' . $key;
                $key = '#' . $key . '#';
            } else {
                $key = trim($key, '#');
                $where[] = '`' . $key . '`' . ' = :' . $key;
            }
            $arg[$key] = $value;
        }
        $where = count($where) ? ' WHERE ' . join(' AND ', $where) : '';

        $qry = 'SELECT * FROM ' . PREFIX . 'ATP_DOCUMENT_TERMS ' . $where;
        $result = $this->atp->db_query_fast($qry, $arg);

        $ret = [];
        while ($row = $this->atp->db_next($result)) {
            $ret[] = $row;
        }
        return $ret;
    }

    /**
     * @todo Refactor
     * @return \Atp\Entity\Document
     * @throws \Exception
     */
    public function edit()
    {
        $documentId = (!empty($_POST['table_id'])) ? (int) $_POST['table_id'] : 0;
        $parentDocumentId = (!empty($_POST['table_parent'])) ? (int) $_POST['table_parent'] : null;

        $table_action_edit = (!empty($_POST['table_action_edit']) ? 1 : 0);
        $table_action_disable_edit = (!empty($_POST['table_action_disable_edit']) ? 1 : 0);
        $table_action_end = (!empty($_POST['table_action_end']) ? 1 : 0);
        $table_action_next = (!empty($_POST['table_action_next']) ? 1 : 0);
        $table_status_idle = (!empty($_POST['table_status_idle']) ? 1 : 0);
        $table_status_idle_days = (int) $_POST['table_status_idle_days'];
        $table_status_term = (int) $_POST['table_status_term'];
        $table_status_term_days = (int) $_POST['table_status_term_days'];

//        $fields = (array) $_POST['fields'];
        $new_table_name = (!empty($_POST['new_table_name']) ? $_POST['new_table_name'] : '');
        $table_terms = $_POST['table_terms'];

        $col_order = (!empty($_POST['col_order']) ? $_POST['col_order'] : []);
        $col_label = (!empty($_POST['col_label']) ? $_POST['col_label'] : []);
        $col_default = (!empty($_POST['col_default']) ? $_POST['col_default'] : []);
        $col_default_id = (!empty($_POST['col_default_id']) ? $_POST['col_default_id'] : []);
        $col_configuration = array_filter(array_map(
                function ($value) {
                return array_filter($value);
            }, filter_input(INPUT_POST, 'col_configuration', FILTER_VALIDATE_INT, FILTER_REQUIRE_ARRAY)
        ));
        $col_sort = (!empty($_POST['col_sort']) ? $_POST['col_sort'] : []);
        $col_text = (!empty($_POST['col_text']) ? $_POST['col_text'] : []);
        $col_type = (!empty($_POST['col_type']) ? $_POST['col_type'] : []);
        $col_render = (!empty($_POST['col_render']) ? $_POST['col_render'] : []);
        $col_source = (!empty($_POST['col_source']) ? $_POST['col_source'] : []);
//        $col_value = (!empty($_POST['col_value']) ? $_POST['col_value'] : []);
        $col_ws = (!empty($_POST['col_ws']) ? $_POST['col_ws'] : []);
//        $col_value_type = (!empty($_POST['col_value_type']) ? $_POST['col_value_type'] : []);
//        $col_source_other = (!empty($_POST['col_source_other']) ? $_POST['col_source_other'] : []);
//        $col_value_other = (!empty($_POST['col_value_other']) ? $_POST['col_value_other'] : []);
//        $col_fill = (!empty($_POST['col_fill']) ? $_POST['col_fill'] : []);
        $col_in_list = (!empty($_POST['col_in_list']) ? $_POST['col_in_list'] : []);
        $col_relation = (!empty($_POST['col_relation']) ? $_POST['col_relation'] : []);
        $col_mandatory = (!empty($_POST['col_mandatory']) ? $_POST['col_mandatory'] : []);
        $col_visible = (!empty($_POST['col_visible']) ? $_POST['col_visible'] : []);
        $col_competence = (!empty($_POST['col_competence']) ? $_POST['col_competence'] : []);
        $col_file_cnt = (!empty($_POST['col_file_cnt']) ? $_POST['col_file_cnt'] : []);
        $col_file_size_from = (!empty($_POST['col_file_size_from']) ? $_POST['col_file_size_from'] : []);
        $col_file_size_to = (!empty($_POST['col_file_size_to']) ? $_POST['col_file_size_to'] : []);

        $field_type = $this->atp->logic->get_field_type();

        /* @var $removedFields int[] */
        $removedFields = [];
        /* @var $fieldOrderMap int[] */// Field order to id map.
        $fieldOrderMap = [];

        $sql_types = [];
        $structure_values = [];

        foreach ($col_label as $order => $cols) {
            $col_key = key($cols);
            $col_name = $this->atp->_prefix->column . $order;

            //if($col_sort[$order] == 5) $col_type[$order] = 8;
            //if($col_sort[$order] == 8) $col_type[$order] = 7;
            if ($col_sort[$order] == 9) {
                $col_type[$order] = 8;
            }
            if ($col_sort[$order] == 11) {
                $col_type[$order] = 7;
            }

            if ($col_type[$order] != 18) {
                $col_render[$order] = 0;
            }

            $field = [
                'ITEM_ID' => $documentId,
                'ITYPE' => 'DOC',
                'LABEL' => $cols[$col_key],
                'DEFAULT' => (!empty($col_default[$order]) ? $col_default[$order] : ''),
                'DEFAULT_ID' => (!empty($col_default_id[$order]) ? $col_default_id[$order] : null),
                'NAME' => $this->atp->_prefix->item['DOC'] . $this->atp->_prefix->column . $order,
                'SORT' => (!empty($col_sort[$order]) ? $col_sort[$order] : 0),
                'TYPE' => (!empty($col_type[$order]) ? $col_type[$order] : 0),
                'RENDER' => (!empty($col_render[$order]) ? $col_render[$order] : 0),
                'SOURCE' => (!empty($col_source[$order]) ? $col_source[$order] : 0),
//                'VALUE' => (!empty($col_value[$order]) ? $col_value[$order] : 0),
                'WS' => (!empty($col_ws[$order]) ? $col_ws[$order] : 0),
//                'VALUE_TYPE' => (!empty($col_value_type[$order]) ? $col_value_type[$order] : 0),
//                'OTHER_SOURCE' => (!empty($col_source_other[$order]) ? $col_source_other[$order] : 0),
//                'OTHER_VALUE' => (!empty($col_value_other[$order]) ? $col_value_other[$order] : 0),
//                'FILL' => (!empty($col_fill[$order]) ? 1 : 0),
                'IN_LIST' => (isset($col_in_list[$order]) ? 1 : 0),
                'RELATION' => (!empty($col_relation[$order]) ? $col_relation[$order] : 0),
                'MANDATORY' => (isset($col_mandatory[$order]) ? 1 : 0),
                'VISIBLE' => (isset($col_visible[$order]) ? 1 : 0),
            ];

            $competence = \Atp\Document\Field::COMPETENCE_NONE;
            if (count($col_competence[$order]) === 2) {
                $competence = \Atp\Document\Field::COMPETENCE_ALL;
            } elseif (isset($col_competence[$order][0])) {
                $competence = \Atp\Document\Field::COMPETENCE_INVESTIGATE;
            } elseif (isset($col_competence[$order][1])) {
                $competence = \Atp\Document\Field::COMPETENCE_EXAMINATE;
            }

            $field['COMPETENCE'] = $competence;
            $field['ORD'] = (!empty($col_order[$order])) ? $col_order[$order] : 0;
            $field['TEXT'] = (!empty($col_text[$order])) ? $col_text[$order] : 0;
            $field['UPDATE_DATE'] = date('Y-m-d H:i:s');
            $field['CREATE_DATE'] = date('Y-m-d H:i:s');

            if ($field['SORT'] == 10) {
                //Failas
                $field['FILE_CNT'] = (!empty($col_file_cnt[$order]) ? $col_file_cnt[$order] : 0);
                $field['FILE_SIZE_FROM'] = (!empty($col_file_size_from[$order]) ? $col_file_size_from[$order] : 0);
                $field['FILE_SIZE_TO'] = (!empty($col_file_size_to[$order]) ? $col_file_size_to[$order] : 0);
            }

            if (empty($field['LABEL']) || empty($field['TYPE'])) {
                continue;
            }
            if ($field['SORT'] != 10) {
                $table_structure_col .= "{$col_name} {$field_type[$col_type[$order]]['VALUE']} NOT NULL,";
            }

            $col_id = (!empty($col_key) ? $col_key : 'x' . $order);
            $sql_types[$col_id] = "{$field_type[$col_type[$order]]['VALUE']} NOT NULL";
            $structure_values[$col_id] = $field;

            unset($field, $col_key, $col_name, $order, $cols, $key);
        }

        $parent_in = false;
        if (!empty($documentId) && !empty($parentDocumentId)) {
            $table_path = $this->atp->logic->get_table_path($documentId);
            foreach ($table_path as $path) {
                foreach ($path as $pt) {
                    if ($pt['ID'] == $parentDocumentId || $pt['PARENT_ID'] == $parentDocumentId) {
                        $parent_in = true;
                        break;
                    }
                }
            }
        }
        if (!empty($new_table_name) && empty($documentId)) {
            $qry = $this->atp->db_query('
                SELECT COUNT(ID) as COUNT
                FROM ' . PREFIX . 'ATP_DOCUMENTS
                WHERE
                    LABEL LIKE "' . $this->db_escape($new_table_name) . '"
                LIMIT 1');
            $res = $this->atp->db_next($qry);
            if ($res['COUNT'] == 0) {
                if (!empty($table_structure_col) && !empty($structure_values)) {
                    if ($parent_in) {
                        $parentDocumentId = null;
                        throw new \Exception('"Tėvinė lentelė" priklauso lentelių grandinei.');
                    }


//                    $entityManager = $this->atp->factory->Doctrine();
//                    /* @var $documentRepository \Atp\Repository\DocumentRepository */
//                    $documentRepository = $entityManager->getRepository(DocumentEntity::class);
//                    $document = $documentRepository->getById($document['ID']);
//                    $document->setParent($entityManager->getReference(DocumentEntity::class, $parentDocumentId));
//                    $document->setLabel($new_table_name);
//                    $document->setUpdateDate(new \DateTime('now'));
//                    $document->setCreateDate(new \DateTime('now'));

                    $document['PARENT_ID'] = $parentDocumentId;
                    $document['LABEL'] = $new_table_name;
                    $document['CAN_EDIT'] = 0;
                    $document['DISABLE_EDIT'] = 0;
                    $document['CAN_END'] = 0;
                    $document['CAN_NEXT'] = 0;
                    $document['CAN_IDLE'] = 0;
                    $document['IDLE_DAYS'] = 0;
                    $document['UPDATE_DATE'] = date('Y-m-d H:i:s');
                    $document['CREATE_DATE'] = date('Y-m-d H:i:s');

                    $documentId = $this->atp->db_quickInsert(PREFIX . "ATP_DOCUMENTS", $document);

                    foreach ($structure_values as $field) {
                        $field['ITEM_ID'] = $documentId;
                        $field['ITYPE'] = 'DOC';

                        if ($field['SORT'] == 10) {
                            //Failas
                            $file_values = [
                                'DOCUMENT_ID' => $field['ITEM_ID'],
                                'CNT' => (empty($field['FILE_CNT']) ? 0 : (int) $field['FILE_CNT']),
                                'SIZE_FROM' => (empty($field['FILE_SIZE_FROM']) ? 0 : (int) $field['FILE_SIZE_FROM']),
                                'SIZE_TO' => (empty($field['FILE_SIZE_TO']) ? 0 : (int) $field['FILE_SIZE_TO'])
                            ];
                            unset($field['FILE_CNT'], $field['FILE_SIZE_FROM'], $field['FILE_SIZE_TO']);
                        }

                        $fieldId = $this->atp->db_quickInsert(PREFIX . "ATP_FIELD", $field);

                        $fieldOrderMap[$field['ORD']] = $fieldId;

                        if ($field['SORT'] == 10) {
                            //Failas
                            $file_values['STRUCTURE_ID'] = $fieldId;
                            $this->atp->db_quickInsert(PREFIX . "ATP_FILE_PARAMS", $file_values);
                            unset($file_values);
                        }
                    }

                    $table_name = PREFIX . 'ATP_' . $this->atp->_prefix->table . $documentId;
                    $sql = "CREATE TABLE IF NOT EXISTS " . $table_name . " (
						ID int(11) NOT NULL AUTO_INCREMENT,
						RECORD_ID varchar(25) NOT NULL,
						RECORD_MAIN_ID varchar(25) NOT NULL,
						RECORD_RELATION_ID int(11) NOT NULL,
						RECORD_VERSION int(11) NOT NULL,
						RECORD_LAST tinyint(1) NOT NULL,
						{$table_structure_col}
						CREATE_DATE datetime NOT NULL,
						PRIMARY KEY (ID),
						KEY `RECORD_LAST` (`RECORD_LAST`)
					) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;";
                    $this->atp->db_query($sql);

//                    $this->atp->set_message(
//                        'success', $this->atp->lng('success_table_created') . ' "' . $document['LABEL'] . '"'
//                    );
                } else {
                    throw new \Exception($this->atp->lng('error_occured'));
                }
            } else {
                throw new \Exception($this->atp->lng('error_table_exists') . ' "' . $new_table_name . '"');
            }
        } else {
            $document = $this->atp->logic2->get_document($documentId);
            if (empty($document)) {
                throw new \Exception('Lentelė nerasta');
            }

            if ($parent_in) {
                if ($document['PARENT_ID'] != $parentDocumentId) {
                    $parentDocumentId = null;
                    throw new \Exception('"Tėvinė lentelė" priklauso lentelių grandinei.');
                }
            }

            $table_structure = $this->atp->logic->get_table_fields($documentId);
            foreach ($table_structure as $structure) {
                if (!empty($structure_values[$structure['ID']])) {
                    $new = $structure_values[$structure['ID']];

                    $fieldOrderMap[$new['ORD']] = $structure['ID'];

                    $update = [
                        'label' => $new['LABEL'],
                        'default' => $new['DEFAULT'],
                        'default_id' => $new['DEFAULT_ID'],
                        'type' => $new['TYPE'],
                        'sort' => $new['SORT'],
                        'render' => $new['RENDER'],
                        'source' => $new['SOURCE'],
//                        'value' => $new['VALUE'],
                        'ws' => $new['WS'],
//                        'value_type' => $new['VALUE_TYPE'],
//                        'other_source' => $new['OTHER_SOURCE'],
//                        'other_value' => $new['OTHER_VALUE'],
//                        'fill' => $new['FILL'],
                        'in_list' => $new['IN_LIST'],
                        'relation' => $new['RELATION'],
                        'mandatory' => $new['MANDATORY'],
                        'visible' => $new['VISIBLE'],
                        'competence' => $new['COMPETENCE'],
                        'ord' => $new['ORD'],
                        'update_date' => $new['UPDATE_DATE'],
                        'text' => $new['TEXT']
                    ];

                    if ($structure['SORT'] == 10) { //Failas
                        $file_arg['DOCUMENT_ID'] = $structure['ITEM_ID'];
                        $file_arg['STRUCTURE_ID'] = $structure['ID'];
                        $file_arg['CNT'] = (!empty($new['FILE_CNT']) ? (int) $new['FILE_CNT'] : 0);
                        $file_arg['SIZE_FROM'] = (!empty($new['FILE_SIZE_FROM']) ? (int) $new['FILE_SIZE_FROM'] : 0);
                        $file_arg['SIZE_TO'] = (!empty($new['FILE_SIZE_TO']) ? (int) $new['FILE_SIZE_TO'] : 0);

                        $file_check = $this->atp->db_query_fast('SELECT *
								FROM ' . PREFIX . 'ATP_FILE_PARAMS
								WHERE
									DOCUMENT_ID = :DOCUMENT_ID AND
									STRUCTURE_ID = :STRUCTURE_ID
								LIMIT 1', $file_arg);

                        if ($this->atp->db_select_count($file_check) > 0) {
                            $this->atp->db_query_fast('UPDATE ' . PREFIX . 'ATP_FILE_PARAMS
								SET
									CNT = :CNT,
									SIZE_FROM = :SIZE_FROM,
									SIZE_TO = :SIZE_TO
								WHERE
									DOCUMENT_ID = :DOCUMENT_ID AND
									STRUCTURE_ID = :STRUCTURE_ID
								LIMIT 1', $file_arg);
                        } else {
                            $this->atp->db_quickInsert(PREFIX . "ATP_FILE_PARAMS", $file_arg);
                        }

                        unset($new['FILE_CNT'], $new['FILE_SIZE_FROM'], $new['FILE_SIZE_TO'], $file_arg);
                    }

                    $this->atp->db_query_fast('UPDATE ' . PREFIX . 'ATP_FIELD
						SET
							`LABEL` = :label,
							`DEFAULT` = :default,
							`DEFAULT_ID` = :default_id,
							`TYPE` = :type,
							`SORT` = :sort,
							`RENDER` = :render,
							`SOURCE` = :source,
						#	`VALUE` = :value,
							`WS` = :ws,
						#	`VALUE_TYPE` = :value_type,
						#	`OTHER_SOURCE` = :other_source,
						#	`OTHER_VALUE` = :other_value,
						#	`FILL` = :fill,
							`IN_LIST` = :in_list,
							`RELATION` = :relation,
							`MANDATORY` = :mandatory,
							`VISIBLE` = :visible,
							`COMPETENCE` = :competence,
							`ORD` = :ord,
							`UPDATE_DATE` = :update_date,
							`TEXT` = :text
						WHERE `ID` = ' . (int) $structure['ID'] . '
						LIMIT 1', $update, null, 1);
                    unset($structure_values[$structure['ID']]);
                } else {
                    $removedFields[] = $structure['ID'];
                }

                unset($structure, $new, $update);
            }

            $param['WHERE'] = ['table_id' => $document['ID']];
            $param['SET'] = [
                //'table_id' => $document['ID'],
                'parent_id' => $parentDocumentId,
                'can_edit' => $table_action_edit,
                'disable_edit' => $table_action_disable_edit,
                'can_end' => $table_action_end,
                'can_next' => $table_action_next,
                'can_idle' => $table_status_idle,
                'idle_days' => $table_status_idle_days,
                'can_term' => $table_status_term,
                'term_days' => $table_status_term_days,
                'update_date' => date('Y-m-d H:i:s')
            ];
//            foreach ($fields as $key => $val) {
//                $param['SET'][$key] = $val;
//            }
            foreach ($param['SET'] as $key => $val) {
                $qry_arr[] = strtoupper($key) . ' = :' . $key;
            }
            if (count($qry_arr)) {
                $qry_set = ',' . PHP_EOL . join(',' . PHP_EOL, $qry_arr);
            }
            $qry = 'UPDATE ' . PREFIX . 'ATP_DOCUMENTS
				SET
					PARENT_ID = :parent_id,
					CAN_EDIT = :can_edit,
					CAN_END = :can_end,
					CAN_NEXT = :can_next,
					CAN_IDLE = :can_idle,
					IDLE_DAYS = :idle_days,
					CAN_TERM = :can_term,
					TERM_DAYS = :term_days,
					UPDATE_DATE = :update_date'
                . $qry_set . '
				WHERE ID = :table_id
				LIMIT 1';
            $this->atp->db_query_fast($qry, array_merge($param['WHERE'], $param['SET']));

            $table_name = PREFIX . 'ATP_' . $this->atp->_prefix->table . $documentId;
            foreach ((array) $removedFields as $fieldId) {
                $field = $this->atp->factory->Field()->getField($fieldId);

                $this->atp->db_query("ALTER TABLE " . $table_name . " DROP {$field->name}");
                $this->atp->logic->delete_table_structure(['ID' => $field->id]);
                $this->atp->db_query('
                    DELETE
                    FROM ' . PREFIX . 'ATP_TABLE_RELATIONS
                    WHERE
                        (
                            DOCUMENT_ID = ' . $documentId . ' AND
                            COLUMN_ID = ' . $field->id . '
                        ) OR (
                            DOCUMENT_PARENT_ID = ' . $documentId . ' AND
                            COLUMN_PARENT_ID = ' . $field->id . '
                        )');
            }

            // Redaguojant, jei reikia, atnaujina DB lentelės laukų tipus
            $qry = 'SELECT A.`COLUMN_NAME`, A.`DATA_TYPE`, B.`ID` as FIELD_ID
				FROM
					`information_schema`.`COLUMNS` A
						INNER JOIN ' . PREFIX . 'ATP_FIELD B ON
							B.NAME = A.COLUMN_NAME AND
							B.ITEM_ID = ' . (int) $documentId . ' AND
							B.ITYPE = "DOC"
				WHERE
					A.`TABLE_SCHEMA` = DATABASE() AND
					A.`TABLE_NAME` = "' . $table_name . '" AND
					A.`COLUMN_NAME` LIKE("' . str_replace('_', '\_', $this->atp->_prefix->column) . '%")';
            $result = $this->atp->db_query_fast($qry);
            $count = $this->atp->db_num_rows($result);
            if ($count) {
                for ($i = 0; $i < $count; $i++) {
                    $row = $this->atp->db_next($result);
                    $res = preg_match('/^[a-z]+/i', $sql_types[$row['FIELD_ID']], $type);
                    if ($res) {
                        if ($type[0] === $row['DATA_TYPE']) {
                            continue;
                        }

                        $this->atp->db_query('ALTER TABLE ' . $table_name . '
                            MODIFY COLUMN ' . $row['COLUMN_NAME'] . ' ' . $sql_types[$row['FIELD_ID']]);
                    }
                }
            }
            $this->atp->db_free($result);


            // Paskutinio sukurto lauko numeris
            $last_column_nr = 0;
            $qry = 'SELECT
                    MAX(
                        CAST(
                            REPLACE(`COLUMN_NAME`, "' . $this->atp->_prefix->column . '", "")
                            AS SIGNED
                        )
                    ) as `LAST`
				FROM `information_schema`.`COLUMNS`
				WHERE
					`TABLE_SCHEMA` = DATABASE() AND
					`TABLE_NAME` = "' . $table_name . '" AND
					`COLUMN_NAME` LIKE("' . str_replace('_', '\_', $this->atp->_prefix->column) . '%")
				LIMIT 1';
            $result = $this->atp->db_query_fast($qry);
            if ($this->atp->db_num_rows($result)) {
                $row = $this->atp->db_next($result);
                $last_column_nr = (int) end(explode($this->atp->_prefix->column, $row['LAST']));
            }

            // Prideda naujai sukurtus laukus
            foreach ($structure_values as $key => $field) {
                if ($last_column_nr === 0) {
                    $column_name_after = 'RECORD_LAST';
                } else {
                    $column_name_after = $this->atp->_prefix->item['DOC']
                        . $this->atp->_prefix->column
                        . $last_column_nr;
                }

                if ($field['SORT'] == 10) { //Failas
                    $file_arg['DOCUMENT_ID'] = $field['ITEM_ID'];
                    $file_arg['CNT'] = (!empty($field['FILE_CNT']) ? (int) $field['FILE_CNT'] : 0);
                    $file_arg['SIZE_FROM'] = (!empty($field['FILE_SIZE_FROM']) ? (int) $field['FILE_SIZE_FROM'] : 0);
                    $file_arg['SIZE_TO'] = (!empty($field['FILE_SIZE_TO']) ? (int) $field['FILE_SIZE_TO'] : 0);

                    unset($field['FILE_CNT'], $field['FILE_SIZE_FROM'], $field['FILE_SIZE_TO']);
                }

                $last_column_nr++;
                $column_name = $this->atp->_prefix->item['DOC'] . $this->atp->_prefix->column . $last_column_nr;
                $field['NAME'] = $column_name;

                $this->atp->db_query('ALTER TABLE ' . $table_name . '
                    ADD COLUMN ' . $column_name . ' ' . $sql_types[$key] . ' AFTER ' . $column_name_after);
                $this->atp->db_quickInsert(PREFIX . "ATP_FIELD", $field);

                if ($field['SORT'] == 10) { //Failas
                    $file_arg['STRUCTURE_ID'] = $this->atp->db_last_id();

                    $file_check = $this->atp->db_query_fast('SELECT *
						FROM ' . PREFIX . 'ATP_FILE_PARAMS
						WHERE
							DOCUMENT_ID = :DOCUMENT_ID AND
							STRUCTURE_ID = :STRUCTURE_ID
						LIMIT 1', $file_arg);

                    if ($this->atp->db_select_count($file_check) > 0) {
                        $this->atp->db_query_fast('UPDATE ' . PREFIX . 'ATP_FILE_PARAMS
							SET
								CNT = :CNT,
								SIZE_FROM = :SIZE_FROM,
								SIZE_TO = :SIZE_TO
							WHERE
								DOCUMENT_ID = :DOCUMENT_ID AND
								STRUCTURE_ID = :STRUCTURE_ID
							LIMIT 1', $file_arg);
                    } else {
                        $this->atp->db_quickInsert(PREFIX . "ATP_FILE_PARAMS", $file_arg);
                    }

                    unset($file_arg);
                }
            }

//            $this->atp->set_message(
//                'success', $this->atp->lng('success_table_edited') . ' "' . $document['LABEL'] . '"'
//            );
        }


        $entityManager = $this->atp->factory->Doctrine();
        /* @var $configurationRepository \Atp\Repository\FieldConfigurationRepository */
        $configurationRepository = $entityManager->getRepository(FieldConfiguration::class);
//        $configurationRepository->removeByFields(array_merge(
//            array_values($fieldOrderMap),
//            array_values($removedFields)
//        ));
        $fieldsIds = array_unique(array_merge(
                array_values($fieldOrderMap), array_values($removedFields)
        ));
        if (count($fieldsIds)) {
            $configurations = $configurationRepository->getByFieldId($fieldsIds);
            foreach ($configurations as $configuration) {
                $entityManager->remove($configuration);
            }
        }
        if (empty($col_configuration) === false) {
            foreach ($col_configuration as $orderKey => $configurations) {
                $fieldId = $fieldOrderMap[$orderKey];
                $field = $entityManager->getReference(\Atp\Entity\Field::class, $fieldId);
                foreach ($configurations as $configurationId) {
                    $entity = new FieldConfiguration();
                    $entity->setType($entityManager->getReference(FieldConfigurationType::class, $configurationId));
//                    $entity->setFieldId($fieldId);
                    $entity->setField($field);

                    $entityManager->persist($entity);
                }
            }
        }

        $entityManager->flush();

        /* \Atp\Entity\Document STATUS enumerator */
        $status = $this->getStatusByFields($documentId);
        $this->atp->db_quickupdate(PREFIX . 'ATP_DOCUMENTS', ['STATUS' => $status], $documentId, 'ID');

        $this->atp->db_query_fast('DELETE FROM ' . PREFIX . 'ATP_DOCUMENT_TERMS
            WHERE DOCUMENT_ID = ' . (int) $documentId);

        $ins = [];
        if (count($table_terms['field'])) {
            foreach ($table_terms['field'] as $key => $val) {
                if (empty($fieldOrderMap[$val]) || empty($table_terms['type']) || empty($table_terms['days'])) {
                    continue;
                }

                $ins[] = '('
                    . (int) $documentId . ', '
                    . (int) $fieldOrderMap[$val] . ', '
                    . (int) $table_terms['days'][$key] . ', '
                    . (int) $table_terms['type'][$key]
                    . ')';
            }

            if (count($ins)) {
                $this->atp->db_query_fast('REPLACE INTO
				' . PREFIX . 'ATP_DOCUMENT_TERMS (`DOCUMENT_ID`, `FIELD_ID`, `DAYS`, `TYPE`)
					VALUES ' . join(',', $ins));
            }
        }

        return $this->get($documentId, true);
    }

    /**
     * @todo implement same principle as Atp\Logic::get_table_path()
     * @param type $documentId
     * @throws \Exception
     */
    public function getPathFromDocument($documentId)
    {
        if (empty($documentId)) {
            throw new \Exception('Missing argument $documentId');
        }
    }
}
