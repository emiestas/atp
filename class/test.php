<?php 

class test
{
    private $atp;

    public function __construct()
    {
        require('../../../inc/mainconf.php');
        $this->atp = prepare_atp();
        $this->atp->init();

        ini_set('display_errors', true);
        ini_set('error_reporting', E_ALL);
    }

    public function save()
    {
        $atp = $this->atp;

        // <editor-fold defaultstate="collapsed" desc="Duomenys">
        $rq = '{"m":"5","template_id":"179","record_id":"252393","table_id":"52","table_from_id":"0","col_11":"2014-02-17","col_12":"13:42","col_56":"100","kcol_100_2":"GZA882","kcol_100_1":"191","col_51":"U\u00c5\u00beupio gatv\u00c4\u2014 7ASDASD","col_13":"Vilnius A","col_55":"2015-06-22","col_49":"5","acol_5":"Tekstas","acol_1":"Teis\u00c4\u2014s akto subjektas yra sudas4","acol_2":"Teis\u00c4\u2014s akto numeris","acol_3":"Teis\u00c4\u2014s akto r\u00c5\u00ab\u00c5\u00a1is","acol_4":"2014-02-17","scol_1":"","col_50":"115","kcol_115_8":"Marius Nickus","kcol_115_6":"Pavard\u00c4","kcol_115_7":"Adresas","kcol_115_1":"Telefonas","kcol_115_2":"El. pa\u00c5\u00a1tas ","kcol_115_3":"0","col_66":"0","col_36":"48410120317","col_63":"","col_61":"","col_58":"","col_64":"","col_59":"","col_60":"","col_65":"0","document_status":"2","table_path":"0","save":"I\u0161saugoti"}';
        $_REQUEST = json_decode($rq, true);
        $_REQUEST['ws_field'] = "[{\"1200\":{\"2101\":\"VW\",\"2102\":\"GOLF\",\"2104\":\"\u012eregistruota\",\"2489\":\"2014-09-29\"}},{\"2061\":{\"2080\":\"VW\",\"2081\":\"GOLF\",\"2083\":\"\u012eregistruota\",\"2488\":\"2014-09-29\"}},{\"1200\":{\"2101\":\"VW\",\"2102\":\"GOLF\",\"2104\":\"\u012eregistruota\",\"2489\":\"2014-09-29\"}},{\"2061\":{\"2080\":\"VW\",\"2081\":\"GOLF\",\"2083\":\"\u012eregistruota\",\"2488\":\"2014-09-29\"}},{\"2061\":{\"2080\":\"VW\",\"2081\":\"GOLF\",\"2083\":\"\u012eregistruota\",\"2488\":\"2014-09-29\"}},{\"1158\":{\"1919\":\"GRA\u017dINA\",\"1920\":\"STEPONAVI\u010cI\u016aT\u0116\",\"1921\":\"1984-10-12\",\"1922\":\"Vilnius, Panev\u0117\u017eio 4-1 \",\"2491\":\"UAB \\\"IDAMAS\\\"\",\"2493\":\"DARBUOTOJAS\",\"2495\":\"2008-04-17\",\"2537\":\"48410120317\"}}]";

        $a = -1;
        $fp[++$a] = tmpfile();
        $file_b64 = 'R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs=';
        fwrite($fp[$a], base64_decode($file_b64));
        $data = stream_get_meta_data($fp[$a]);
        $_FILES = array(
            'col_57' => array(
                'tmp_name' => array(
                    $data['uri'],
                    $data['uri']
                ),
                'name' => array(
                    'FILES_failas_57_1.gif',
                    'FILES_failas_57_2.gif'
                ),
                'type' => array(
                    'image/gif',
                    'image/gif'
                ),
                'size' => array(
                    filesize($data['uri']),
                    filesize($data['uri'])
                ),
                'error' => array(
                    0,
                    0
                )
            ),
            'col_62' => array(
                'tmp_name' => array(
                    $data['uri'],
                    $data['uri']
                ),
                'name' => array(
                    'FILES_failas_62_1.gif',
                    'FILES_failas_62_2.gif'
                ),
                'type' => array(
                    'image/gif',
                    'image/gif'
                ),
                'size' => array(
                    filesize($data['uri']),
                    filesize($data['uri'])
                ),
                'error' => array(
                    0,
                    0
                )
            ),
        );
        // </editor-fold>


        $exec = $this->atp->performance->start('Testas');

        $m = new \Atp\Record\Manager($atp);
        $m->ParseFormData();
        $m->SetAction('save');
        //$m->SetAction('up');
        //$m->SetAction('transfer');$m->SetParameter('transferTo', 36);
        $m->init();

        $this->atp->performance->finish($exec);
        $this->atp->performance->echo_exec();
    }
}

$t = new test();
$t->save();
