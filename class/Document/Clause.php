<?php

namespace Atp\Document;

class Clause
{
    /**
     * Pažeidimo tipo tėvinis klasifikatorius (pasirinkimas straipsnio redagavimo formoje)
     * @var int[]
     */
    public $class_type_parent_id;

    /**
     * Straipsnių grupių tėvinis klasifikatorius (pasirinkimas straipsnio redagavimo formoje)
     * @var int
     */
    public $group_parent_id = 267;

    /**
     *
     * @var \Atp\Core
     */
    private $atp;

    /**
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;
        $this->class_type_parent_id = [98, 80068];
    }

    /**
     * @param int $clauseId
     * @return int[]
     */
    public function getClassificators($clauseId)
    {
        $classificators = [];
        if (empty($clauseId) === false) {
            $result = $this->atp->db_query_fast('
                SELECT CLASSIFICATOR_ID
                FROM
                    ' . PREFIX . 'ATP_CLAUSE_CLASSIFICATOR_XREF
                WHERE
                    CLAUSE_ID = :CLAUSE_ID', ['CLAUSE_ID' => $clauseId]);
            $count = $this->atp->db_num_rows($result);
            if ($count) {
                for ($i = 0; $i < $count; $i++) {
                    $row = $this->atp->db_next($result);
                    $classificators[] = $row['CLASSIFICATOR_ID'];
                }
            }
        }

        return $classificators;
    }

    /**
     * Set clause classificators.
     *
     * @param int $clauseId
     * @param int[] $classificatorsIds
     */
    public function setClassificators($clauseId, $classificatorsIds)
    {
        $this->atp->db_query_fast('START TRANSACTION');
        $this->atp->db_query_fast('
            DELETE
            FROM
                ' . PREFIX . 'ATP_CLAUSE_CLASSIFICATOR_XREF
            WHERE
                CLAUSE_ID = :CLAUSE_ID', ['CLAUSE_ID' => $clauseId]);
        if (count($classificatorsIds)) {
            $data = [];
            foreach ($classificatorsIds as $classificatorId) {
                $data[] = [
                    'CLASSIFICATOR_ID' => $classificatorId,
                    'CLAUSE_ID' => $clauseId
                ];
            }
            if (count($data)) {
                $this->atp->db_ATP_insert(PREFIX . 'ATP_CLAUSE_CLASSIFICATOR_XREF', $data);
            }
        }
        $this->atp->db_query_fast('COMMIT');
    }
}
