<?php

namespace Atp\Document\Record;

class Punishment //extends \Atp\Document\Record\PunishmentParent
{
    /**
     * @var \Atp\Core
     */
    private $atp;

    /**
     *
     * @param \Atp\Document\Record $record
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core &$atp)
    {
        $this->atp = &$atp;
//        parent::__construct($this->atp);
    }

    /**
     * Nustato ar baustumas buvo patikrintas
     * @param mixed $record
     * @return boolean
     */
    public function getCheckStatus($record)
    {
        $record = $this->atp->logic2->get_record_relation($record);
        if (empty($record) === true) {
            trigger_error('Record is not set', E_USER_ERROR);
            exit;
        }

        $result = $this->atp->db_query_fast('
            SELECT
                PUNISHMENT_CHECKED CHECKED
			FROM
                ' . PREFIX . 'ATP_RECORD
			WHERE
				ID = :ID AND
				IS_LAST = 1', ['ID' => $record['ID']]);

        if ($this->atp->db_num_rows($result)) {
            $row = $this->atp->db_next($result);

            return (bool) $row['CHECKED'];
        }

        return false;
    }

    /**
     * Nurodo ar baustumas patikrintas
     * @param mixed $record
     * @param boolen $status
     * @return boolean
     */
    public function setCheckStatus($record, $status)
    {
        $record = $this->atp->logic2->get_record_relation($record);
        if (empty($record) === true) {
            trigger_error('Record is not set', E_USER_ERROR);
            exit;
        }

        $this->atp->db_query_fast('
            UPDATE ' . PREFIX . 'ATP_RECORD
            SET
                PUNISHMENT_CHECKED = :PUNISHMENT_CHECKED
            WHERE
                ID = :ID', [
            'PUNISHMENT_CHECKED' => (int) $status,
            'ID' => $record['ID']
        ]);

        return true;
    }

    /**
     * Iškviečia metodą pažeidimų suradimui pagal tipą
     * @param array $data
     * @param string $type Tipas
     * @param array $exclude_records [optional] Dokumentų įrašų numeriai, kuriu nereikia
     * @return type
     */
    public function getRecordsByType($data, $type, $exclude_records = array())
    {
        $result = null;

        $method = 'getRecords' . $type;
		if (method_exists($this, $method) === true) {
            $result = $this->$method($data, $exclude_records);
        }

        return $result;
    }

    /**
     * Pašalinta baustumo tikrinimo metu surastus įrašus
     * @param int $record_id Dokumento įrašo ID
     */
    public function deleteRecords($record_id)
    {
        $this->atp->db_query_fast('
            DELETE
            FROM
                ' . PREFIX . 'ATP_VIOLATIONS
            WHERE
                RECORD_ID = :RECORD_ID', [
            'RECORD_ID' => (int) $record_id]);
    }

    /**
     * Išsaugo baustumo tikrinimo metu surastus įrašus
     * @param int $recordId Dokumento įrašo ID
     * @param array $mappedItems
     * @param int|array $mappedItems[$type][] Dokumento įrašo ID arba įrašo duomenų masyvas.
     */
    public function saveRecords($recordId, $mappedItems)
    {
        $rows = [];
        foreach ($mappedItems as $itemType => $items) {
            foreach ($items as $item) {
                $rows[] = [
                    'RECORD_ID' => $recordId,
                    'VIOLATION_RECORD_ID' => (isset($item['ID']) ? $item['ID'] : $item),
                    'VIOLATION_TYPE' => $itemType
                ];
            }
        }

        if (count($rows)) {
            $this->atp->db_ATP_insert(PREFIX . 'ATP_VIOLATIONS', $rows);
        }
    }

    /**
     * Check whethere valid ATPR violation record is present.
     *
     * @param int $recordId
     * @return bool
     */
    public function hasAtprViolations($recordId)
    {
        $result = $this->atp->db_query_fast('
            SELECT COUNT(ID) CNT
            FROM
                ' . PREFIX . 'ATP_VIOLATIONS
            WHERE
                RECORD_ID = :RECORD_ID AND
                VIOLATION_TYPE = "Atpeir"', ['RECORD_ID' => $recordId]);

        return (bool) $this->atp->db_next($result)['CNT'];
    }

    /**
     * ATPEIR dokumentai.
     * 
     * @param array $data
     * @return null|AtpDetailsType[]
     * @throws \Exception
     */
    protected function getRecordsPAtpeir($data, $_ = null)
    {
        $return = [];
        try {
            $checkCriminalRecord = new \Atp\Atpr\CheckCriminalRecord();
            $return = $checkCriminalRecord->findAtp(
                $checkCriminalRecord->createSearchParameters($data['CODE'], null, null, true)
            );
        } catch (\Exception $e) {
            error_log($e);
            throw $e;
        }

        return $return;
    }
}
