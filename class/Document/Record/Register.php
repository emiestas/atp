<?php

namespace Atp\Document\Record;

throw new \Atp\Exception('Instead of "Atp\\Document\\Record\\Register" use "Atp\\Avilys" service');

use \Avilys\RDODocumentWS as WS;
use \Atp\Document\Record;
use \Atp_Document_Templates_Template as Template;
use \Atp\Core as Core;

class Register
{

    private $atp;

    private $record;

    private $template;

    /**
     * @var type
     */
    public $request;

    /**
     * @var type
     */
    public $response;

    /**
     * @var type
     */
    public $errors;

    public $document_number;

    /**
     * @var type
     */
    private $webservise_url = 'https://edarbuotojas.vilnius.lt/subsystems/webService/index.php';

    /**
     * Ar užregistravus dokumento įrašą jame užpildyti priskirtus registracijos laukus
     * Default: TRUE
     * @var boolean
     */
    public $set_registration_fields = true;

    /**
     * @param \Atp\Document\Record $record
     * @param \Atp_Document_Templates_Template|null $template
     * @param \Atp\Core $atp
     */
    public function __construct(Record $record, Template $template = null, Core $atp)
    {
        $this->atp = $atp;
        $this->record = $record;
        $this->template = $template;
        $this->services = new \Atp_Document_Templates_Services($atp);

        $this->request = null;
        $this->response = null;
        $this->errors = array();
        $this->document_number = null;
    }

    /**
     * @param type $error
     * @param type $write_log
     */
    private function setError($error, $write_log = false)
    {
        $this->errors[] = $error;
        trigger_error($error, E_USER_WARNING);
        if ($write_log === true) {
            $this->atp->manage->writeATPactionLog(array('<br/>' . $error));
        }
    }

    /**
     * Paruošia XML'ą užklausai
     * @return string|boolean
     */
    private function prepareRequest()
    {
        $record = &$this->record->record;
        $record_table = $this->atp->logic2->get_record_table($record['ID']);
        $template = &$this->template->template;
        $service_id = $template->service_id;

        if (empty($service_id) === true) {
            $this->setError('Service is not set');
            return false;
        }


        $xml_arr = array(
            'personCode' => array('@cdata' => $_SESSION['WP']['PEOPLE']['CODE']),
            'serviceId' => array('@cdata' => $service_id),
            'personName' => array('@cdata' => $_SESSION['WP']['PEOPLE']['SHOWS'])
        );


        // Pagal ATP šablono susijeimus su avilio laukais į XML užklausą prideda PVS'o formos laukų kintamuosius
        $qry = 'SELECT A.ID, B.NAME AVILYS_NAME, C.DOCUMENT_ID, C.FIELD_ID, C.FIELD_VARNAME, C.FIELD_VARPATH
			FROM
				' . PREFIX . 'ATP_TMPL_SERVICE_AVILYS_FIELDS_X_VARFIELDS_REL A
					INNER JOIN ' . PREFIX . 'ATP_TMPL_SERVICE_AVILYS_FIELDS B ON B.ID = A.AVILYS_FIELD_ID
					INNER JOIN ' . PREFIX . 'ATP_TMPL_VARFIELDS C ON C.ID = A.TEMPLATE_VAR_ID
			WHERE
				TEMPLATE_ID = :TEMPLATE_ID';
        $result = $this->atp->db_query_fast($qry, array('TEMPLATE_ID' => $template->id));
        $count = $this->atp->db_num_rows($result);
        if ($count) {
            for ($i = 0; $i < $count; $i++) {
                $row = $this->atp->db_next($result);

                if (empty($row['FIELD_ID']) === false) {
//                    $field = $this->atp->logic2->get_fields(array('ID' => $row['FIELD_ID']));
//                    if ($field['ITYPE'] === 'DOC') {
//                        $value = $record_table[$field['NAME']];
//                    } else {
//                        $value = $this->atp->logic->get_extra_field_value($record['ID'], $field['ID']);
//                    }
                    $value = $this->atp->factory->Record()->getFieldValue($record['ID'], $row['FIELD_ID']);
                } else {
                    $field_data = $this->atp->logic->get_field_by_varpath(
                        json_decode($row['FIELD_VARPATH'], true),
                        $template->document_id,
                        $record_table['ID'],
                        $record['ID']
                    );
                    $value = $field_data['VALUE'];
                }

                if (empty($value) === false) {
                    $form_element = $this->services->getFormElementInfoByAvilysName($service_id, $row['AVILYS_NAME']);
                    if (empty($form_element)) {
                        continue;
                    }

                    $xml_arr[$form_element['VNAME']] = array('@cdata' => $value);
                }
            }
        }

        // Šablonas
        $generator = new \Atp\Document\Templates\Generator($this->atp);
        $generator->Make($record['ID'], $template->id);
        $file = $generator->GetFilePath();
        $file = array(
            'filename' => basename($file),
            'realpath' => $file,
        );

        if ($file !== false) {
            $i = 0;

            $xml_arr['fileList']['ITEM' . $i] = array(
                'title' => array('@cdata' => $file['filename']),
                'contentType' => array(
                    '@cdata' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                ),
                'encodedContent' => array('@cdata' => base64_encode(file_get_contents($file['realpath'])))
            );
        }

        require_once($this->atp->config['GLOBAL_REAL_URL'] . 'helper/Array2XML.php');
        $arr2xml = \Array2XML::createXML('actionRequest', $xml_arr);
        $xml = $arr2xml->saveXML();

        return $xml;
    }

    /**
     * Dokumento įrašo registravimas į Avilį
     * @global object $ws
     * @return boolean
     */
    public function register()
    {
        if (empty($this->template)) {
            $this->setError('Template is not set');
            return false;
        }

        $record = &$this->record->record;

        $this->request = $this->prepareRequest();

        if ($this->request === false) {
            $this->setError(
                'Nepavyko dokumento įrašo (' . $record['ID'] . ') registracija kaip paslauga į išorinę sistemą.', true
            );

            return false;
        }


        $this->atp->manage->writeATPactionLog(array(
            '<br/>Registruojamas dokumento įrašas (' . $record['ID'] . ') kaip paslaugą į išorinę sistemą.'
        ));


        // Registruoja dokumento įrašą kaip paslaugą į išorinę sistemą
        /*
          global $ws;
          prepare_ws();
          $ws->openSession();
          $ws->db_connect();
          $this->response = $ws->WS_registerServiceDocument($this->request); */
        $login_data = '<?xml version="1.0" encoding="UTF-8" ?>'
            . '<actionRequest><username>sys</username><password>testinis</password>'
            . '</actionRequest>';

        $this->curl_obj = curl_init();
        curl_setopt($this->curl_obj, CURLOPT_COOKIEJAR, $this->cookie);
        //curl_setopt($this->curl_obj, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($this->curl_obj, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($this->curl_obj, CURLOPT_POST, true);
        curl_setopt($this->curl_obj, CURLOPT_POSTFIELDS, array('xmlRequest' => $login_data));
        curl_setopt($this->curl_obj, CURLOPT_URL, $this->webservise_url . '?' . 'logInUser');
        curl_setopt($this->curl_obj, CURLOPT_HEADER, false);
        curl_setopt($this->curl_obj, CURLOPT_RETURNTRANSFER, true);
        $login_result = curl_exec($this->curl_obj);
        if (strpos($login_result, '<result>true') === false) {
            return false;
        }


        curl_setopt($this->curl_obj, CURLOPT_URL, $this->webservise_url . '?' . 'registerServiceDocument');
        curl_setopt($this->curl_obj, CURLOPT_POSTFIELDS, array('xmlRequest' => $this->request,
            'I' => 'dev'));
        $this->response = curl_exec($this->curl_obj);

        // TODO: atp dokumeno įrašą risti su dhs dokumentu. Iš DHS reikės ATP
        // dokumento įrašo formoje atvaizduoti dokumento registravimo datos ir registro numerio

        $result = $this->parseResponse();

        return $result;
    }

    private function prepareElectronicRequest()
    {
        $record = &$this->record->record;

        $docAttributes = new WS\docAttributes();
        $extraAttributes = new WS\extraAttributes();
        // A773EBE85C211DBBB86001122334455 // templ.RDOINC.BYLA // templ.RDOINC.PRASY
//        $templateParam = (new WS\TemplateParam())->setOid('afd2f29003a911e2a943ebb67d08761d');
        $templateParam = (new WS\TemplateParam())->setOid('c29150209db611e5b1f5a27b12a0020b');

        $param = new WS\DocumentFromTemplateParam($docAttributes, $extraAttributes, $templateParam, $register = false);
        $param->setProject(true);
//        $docAttributes->setIsElectro(true)
//            ->setTitle('Elektroninis dokumentas iš ATP')
//            ->setDraftJournal(new WS\JournalParam('D7BE0D74BCA611D995DA892017285631'))
//            ->setDraftOfficeCase((new WS\OfficeCaseParam())->setOid('4ece3640b78711e08802be19b37f12c6'));
        $docAttributes->addEntry((new WS\entry())->setKey('isElectro')->setValue(true))
            ->addEntry((new WS\entry())->setKey('title')->setValue('Elektroninis dokumentas iš ATP'))
            ->addEntry((new WS\entry())->setKey('draftJournal')->setValue(new WS\JournalParam('D7BE0D74BCA611D995DA892017285631')))
            ->addEntry((new WS\entry())->setKey('draftOfficeCase')->setValue((new WS\OfficeCaseParam())->setOid('4ece3640b78711e08802be19b37f12c6')));

//        $extraAttributes
        $docAttributes
            ->addEntry(
                (new WS\entry())
                ->setKey('personCode')
                ->setValue($_SESSION['WP']['PEOPLE']['CODE'])
            )
            ->addEntry(
                (new WS\entry())
                ->setKey('personName')
                ->setValue($_SESSION['WP']['PEOPLE']['SHOWS'])
        );

        // Failai
        $attachments = null;

        $entityManager = $this->atp->factory->Doctrine();
        /* @var $repository \Atp\Repository\Atpr\AtpRepository */
        $repository = $entityManager->getRepository('\Atp\Entity\Atpr\Atp');
        $atpr = $repository->getByRecordId($record['ID']);
        if ($atpr && $atpr->getRoik()) {
            $filesIds = $repository->getFilesIds($atpr->getRoik(), \Atp\Entity\Atpr\AtpDocument::SOURCE_ATPEIR);
            $fileRepository = $entityManager->getRepository('\Atp\Entity\File');

            foreach($filesIds as $fileId) {
                /* @var $file \Atp\Entity\File */
                $file = $fileRepository->find($fileId);

                $attachments[] = new WS\AttachmentActionParam(
                    (empty($attachments) ? WS\AttachmentType::main : WS\AttachmentType::supp),
                    WS\AttachmentAction::add,
                    $file->getName(),
                    $file->getContent(),
                    $file->getType(),
                    new WS\customFields()
                );
            }
        } else {
            if (empty($this->template) === false) {
                $template = $this->template->template;

                $generator = new \Atp\Document\Templates\Generator($this->atp);
                $generator->Make($record['ID'], $template->id);
                $file = $generator->GetFilePath();

                if ($file !== false) {
                    $attachments[] = new WS\AttachmentActionParam(
                        WS\AttachmentType::main,
                        WS\AttachmentAction::add,
                        basename($file),
                        file_get_contents($file),
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        new WS\customFields()
                    );
                }
            }
        }


        if ($attachments) {
            $param->setBodyAttachment($attachments);
        }

        $parameters = new WS\createDocumentFromTemplate();
        $parameters->setDocumentFromTemplateParam($param);

        return $parameters;
    }

    public function registerElectronic()
    {
        $record = &$this->record->record;

        require_once __DIR__ . '/../../../../avilys/autoload.php';

        $parameters = $this->prepareElectronicRequest();

        $client = new \Avilys\RDODocument();

        $this->atp->manage->writeATPactionLog(array(
            '<br/>Registruojamas dokumento įrašas (' . $record['ID'] . ') kaip '
            . 'elektorinis projektas į išorinę sistemą.'
        ));

        try {
            $result = $client->createDocumentFromTemplate($parameters);
        } catch (\Exception $e) {
            throw new \Exception(
            'Avilyje nepavyko sukurti elektroninio dokumento projekto. Klaida: ' . $e->getMessage()
            );
        }

        $documentInfo = $result->getDocumentInfo();

        if (empty($documentInfo->getOid())) {
            throw new \Exception('Negautas Avilio dokumento oid.');
        }

        $entityManager = $this->atp->factory->Doctrine();

        $avilysDocument = new \Atp\Entity\Avilys\Document(
            $documentInfo->getOid(),
            $documentInfo->getRegistrationNo(),
            $documentInfo->getRegistrationDate(),
            $documentInfo->getDocumentCategory()
        );
        $avilysDocument->setRecord($entityManager->getReference('Atp\Entity\Record', $record['ID']));

        $entityManager->persist($avilysDocument);
        $entityManager->flush();

        $doumentData = $client->getDocument(
            (new WS\getDocument())
                ->setGetDocumentParam(
                    new WS\GetDocumentParam($documentInfo->getOid())
                )
        );
        $this->document_number = $doumentData->getDocument()->getDocAttributes()->getEntryByKey('creationNo')->getValue();

        return true;
    }

    /**
     * Išnagrinėja registracijos užklausos atsaką
     * @return boolean
     */
    private function parseResponse()
    {
        $xml = &$this->response;
        //$xml = '<id>186454</id><registrationNo>AJKsfiA8963</registrationNo>';

        if (preg_match('#<errorCode>(.*?)</errorCode>#i', $xml, $m)) {
            $errorCode = $m[1];
            preg_match('#<errorMessage>(.*?)</errorMessage>#i', $xml, $m);
            $errorMessage = str_replace(array('<![CDATA[', ']]>'), '', $m[1]);

            $this->setError(
                'Klaida registruojant paslaugos dokumentą. errorCode: '
                . $errorCode . '; errorMessage: ' . $errorMessage,
                true
            );
        } elseif (preg_match('#<id>([^<]+)</id>#i', $xml, $m)) {
            $id = $m[1];
            preg_match('#<registrationNo>([^<]+)</registrationNo>#i', $xml, $m);
            $nr = $m[1];
            $this->document_number = $nr;

            $record = $this->record->record;
            $template = $this->template->template;

            if ($this->set_registration_fields === true) {
                $this->setRegistrationFields($this->record);
            }

            // Pažymi, kad dokumento įrašas buvo užregistruotas avilyje
            $qry = 'UPDATE `' . PREFIX . 'ATP_RECORD`
						SET
							`REGISTERED_TEMPLATE_ID` = :TEMPLATE_ID
						WHERE
							ID = :ID AND
							IS_LAST = 1';
            $this->atp->db_query_fast($qry, array(
                'ID' => $record['ID'],
                'TEMPLATE_ID' => $template->id));

            // Atnaujina dokumento įrašo būseną
            $this->atp->logic->update_record_status($record['ID']);

            return true;
        } else {
            $this->setError('Klaida registruojant paslaugos dokumentą.', true);
        }

        return false;
    }

    /**
     * Dokumento įraše išsaugo registravimo avilyje informaciją
     * @param \Atp\Document\Record $record Dokumento įrašas
     * @return boolen TRUE
     */
    public function setRegistrationFields(\Atp\Document\Record &$record = null)
    {
        return $this->atp->factory->Record()->setRegistrationFields(
                $record, $this->document_number, new \DateTime()
        );
    }
}