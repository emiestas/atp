<?php

throw new \Exception('Removed');

namespace Atp\Document\Record;

class PunishmentParent
{
    /**
     * @var \Atp\Core
     */
    private $atp;

    /**
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core &$atp)
    {
        $this->atp = &$atp;
    }

    /**
     * Gauna kokio tipo galiojantį
     * @deprecated removed
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @return string 'PValidInvest' arba 'PValidExam'
     */
    public function getValidRecordMethod($record)
    {
        $record = $this->atp->logic2->get_record_relation($record);

        $worker = $this->atp->logic2->get_record_worker(array(
            'A.RECORD_ID = ' . (int) $record['ID'],
            'A.WORKER_ID = ' . (int) $this->atp->factory->User()->getLoggedPersonDuty()->id), true, null, 'D.ID, A.TYPE'
        );

        if (empty($worker) === false) {
            if ($worker['TYPE'] !== 'invest') {
                return 'ValidExam';
            }
        }

        return 'ValidInvest';
    }

    /**
     * Galiojantys baustumo dokumentai tyrėjui
     * @deprecated removed
     * @param array $data
     * @param array $exclude_records [optional]
     * @return array
     */
    protected function getRecordsPValidInvest($data, $exclude_records = array())
    {
        $exclude_records = (array) $exclude_records;
        $atp = $this->atp->logic2;

        $default = array(
            // Tikrinamas dokumento įrašas
            'RECORD' => null,
            // Tikrinamo dokumento įrašo pažeidimo data
            'VIOLATION_DATE' => null,
            // T.d.į. asmens kodas
            'CODE' => null,
            // T.d.į. straipsnis
            'CLAUSE' => null,
            // T.d.į. automobilio numeris
            'AUTO_NR' => null,
            // Dokumentai ir būsenos, kuriose nustatomas baustumas
            'SOURCE' => array(
                // Protokolas
                \Atp\Document::DOCUMENT_PROTOCOL => array(
                    17, // AN įvykdytas
                ),
                // Nutarimas
                \Atp\Document::DOCUMENT_CASE => array(
                    29, // Priimtas nutarimas
                    27 // Išieškojo antstolis
                )
            ),
            'LIMIT' => array(
                // Protokolas
                //\Atp\Document::DOCUMENT_PROTOCOL => '- 1 year', // skaičiuojant nuo AN įvykdytas
                \Atp\Document::DOCUMENT_PROTOCOL => '- 353 days', // skaičiuojant nuo surašymo datos
                // Nutarimas
                \Atp\Document::DOCUMENT_CASE => '- 1 years - 20 days' // - 385 days
            )
        );
        $data = array_merge($default, $data);
        // Straipsnio informacija
        if (empty($data['CLAUSE']) === false) {
            $data['CLAUSE'] = $atp->get_clause(array('ID' => $data['CLAUSE']));
        }
        // Dok. įrašo informacija
        if (empty($data['RECORD']) === false) {
            $data['RECORD'] = $atp->get_record_relation($data['RECORD']);

            // Einamojo dokumento įrašo pažeidimo data
            $violation_date = $atp->get_record_violation_date($data['RECORD']['ID']);
            if ($violation_date !== null) {
                $data['VIOLATION_DATE'] = strtotime($violation_date);
            }
        }


        // Prie dokumentų priskirti asmens kodo ir surašymo datos laukai
        $documents = $atp->get_document(array('A.ID IN (' . join(',', array_keys($data['SOURCE'])) . ')'), false, null, 'A.ID');

        $result = array();
        foreach ($documents as &$document) {
            $fields = array(
                // Informacija apie asmens kodo lauką
                'CODE' => $atp->get_fields(array('ID' => $this->atp->factory->Document()->getOptions($document['ID'])->person_code), true, null, 'ID, NAME, ITYPE'),
                // Informacija apie surašymo datos lauką
                'DATE' => $atp->get_fields(array('ID' => $this->atp->factory->Document()->getOptions($document['ID'])->fill_date), true, null, 'ID, NAME, ITYPE')
            );
            if (empty($fields['CODE']) === true || empty($fields['DATE']) === true) {
                continue;
            }

            $search = array(
                'CODE' => $data['CODE'],
                'DOCUMENT_ID' => $document['ID'],
                'DOCUMENT_STATUS' => $data['SOURCE'][$document['ID']]
            );

            if (empty($search['CODE'])) {
                continue;
            }

            $records = $this->getRecords($search, $fields['CODE'], $exclude_records);

            if (empty($records)) {
                continue;
            }

            // Filtruoja išrinktus įrašus pagal likusius parametrus ir surenka rastų įrašų RECORD_RELATION.ID
            foreach ($records as $key => &$record) {
                // Įrašo data
//                if ($fields['DATE']['ITYPE'] === 'DOC') {
//                    $row = $atp->get_db(PREFIX . 'ATP_TBL_' . (int) $record['TABLE_TO_ID'], array(
//                        'RECORD_ID' => $record['RECORD_ID'], 'RECORD_LAST' => 1), true, null, false, 'A.ID, A.' . $fields['DATE']['NAME'] . ' as VALUE');
//                    $data['VALUE'] = $row['VALUE'];
//                } else {
//                    $data['VALUE'] = $this->atp->logic->get_extra_field_value($record['ID'], $fields['DATE']['ID']);
//                }
                $data['VALUE'] = $this->atp->factory->Record()->getFieldValue($record['ID'], $fields['DATE']['ID']);

                $record_timestamp = strtotime($data['VALUE']);
                if ($record_timestamp <= 0) {
                    continue;
                }

                // Praleidžia įrašus, kurie įvykdyti po tikrinamo pažeidimo įvykdymo datos + jei įvesta pažeidimo įvykdymo data
                if (empty($data['VIOLATION_DATE']) === false && $record_timestamp > $data['VIOLATION_DATE']) {
                    continue;
                }

                // Praleidžia nebegaliojančius įrašus NUO EINAMOSIOS DATOS (ne nuo tikrinamo dokumento pažeidimo įvykdymo datos)
                if ($record_timestamp < strtotime($data['LIMIT'][$record['TABLE_TO_ID']], strtotime(date('Y-m-d')))) {
                    continue;
                }

                if (empty($data['CLAUSE']) === false) {
                    $clause = $atp->get_record_clause($record['ID']);
                    if (empty($clause) === false && (int) $clause['ID'] !== (int) $data['CLAUSE']['ID']) {
                        continue;
                    }
                }
                // TODO: jei nėra nurodyto (neranda) straipsnio, tai "else continue;" ?

                $result[] = $record['ID'];
            }
        }

        return $result;
    }

    /**
     * Galiojantys baustumo dokumentai nagrinėtojui
     * @deprecated removed
     * @param array $data
     * @param array $exclude_records [optional]
     * @return array
     */
    protected function getRecordsPValidExam($data, $exclude_records = array())
    {
        $exclude_records = (array) $exclude_records;
        $atp = $this->atp->logic2;

        $default = array(
            // Tikrinamas dokumento įrašas
            'RECORD' => null,
            // Tikrinamo dokumento įrašo pažeidimo data
            'VIOLATION_DATE' => null,
            // T.d.į. asmens kodas
            'CODE' => null,
            // T.d.į. straipsnis
            'CLAUSE' => null,
            // T.d.į. automobilio numeris
            'AUTO_NR' => null,
            // Dokumentai ir būsenos, kuriose nustatomas baustumas
            'SOURCE' => array(
                // Protokolas
                \Atp\Document::DOCUMENT_PROTOCOL => array(
                    17, // AN įvykdytas
                ),
                // Nutarimas
                \Atp\Document::DOCUMENT_CASE => array(
                    29, // Priimtas nutarimas
                    27 // Išieškojo antstolis
                )
            ),
            'LIMIT' => array(
                // Protokolas
                //\Atp\Document::DOCUMENT_PROTOCOL => '- 1 year', // skaičiuojant nuo AN įvykdytas
                \Atp\Document::DOCUMENT_PROTOCOL => '- 353 days', // skaičiuojant nuo surašymo datos
                // Nutarimas
                \Atp\Document::DOCUMENT_CASE => '- 1 years - 20 days' // - 385 days
            )
        );
        $data = array_merge($default, $data);
        // Straipsnio informacija
        if (empty($data['CLAUSE']) === false) {
            $data['CLAUSE'] = $atp->get_clause(array('ID' => $data['CLAUSE']));
        }
        // Dok. įrašo informacija
        if (empty($data['RECORD']) === false) {
            $data['RECORD'] = $atp->get_record_relation($data['RECORD']);

            // Einamojo dokumento įrašo pažeidimo data
            $violation_date = $atp->get_record_violation_date($data['RECORD']['ID']);
            if ($violation_date !== null) {
                $data['VIOLATION_DATE'] = strtotime($violation_date);
            }
        }


        // Prie dokumentų priskirti asmens kodo ir surašymo datos laukai
        $documents = $atp->get_document(array('A.ID IN (' . join(',', array_keys($data['SOURCE'])) . ')'), false, null, 'A.ID');

        $result = array();
        foreach ($documents as &$document) {
            $fields = array(
                // Informacija apie asmens kodo lauką
                'CODE' => $atp->get_fields(array('ID' => $personCodeField = $this->atp->factory->Document()->getOptions($document['ID'])->person_code), true, null, 'ID, NAME, ITYPE'),
                // Informacija apie surašymo datos lauką
                'DATE' => $atp->get_fields(array('ID' => $fillDateField = $this->atp->factory->Document()->getOptions($document['ID'])->fill_date), true, null, 'ID, NAME, ITYPE')
            );
            if (empty($fields['CODE']) === true || empty($fields['DATE']) === true) {
                continue;
            }

            $search = array(
                'CODE' => $data['CODE'],
                'DOCUMENT_ID' => $document['ID'],
                'DOCUMENT_STATUS' => $data['SOURCE'][$document['ID']]
            );

            if (empty($search['CODE'])) {
                continue;
            }

            $records = $this->getRecords($search, $fields['CODE'], $exclude_records);

            if (empty($records)) {
                continue;
            }

            // Filtruoja išrinktus įrašus pagal likusius parametrus ir surenka rastų įrašų RECORD_RELATION.ID
            foreach ($records as $key => &$record) {
                // Įrašo data
//                if ($fields['DATE']['ITYPE'] === 'DOC') {
//                    $row = $atp->get_db(PREFIX . 'ATP_TBL_' . (int) $record['TABLE_TO_ID'], array(
//                        'RECORD_ID' => $record['RECORD_ID'], 'RECORD_LAST' => 1), true, null, false, 'A.ID, A.' . $fields['DATE']['NAME'] . ' as VALUE');
//                    $data['VALUE'] = $row['VALUE'];
//                } else {
//                    $data['VALUE'] = $this->atp->logic->get_extra_field_value($record['ID'], $fields['DATE']['ID']);
//                }
                $data['VALUE'] = $this->atp->factory->Record()->getFieldValue($record['ID'], $fields['DATE']['ID']);

                $record_timestamp = strtotime($data['VALUE']);
                if ($record_timestamp <= 0) {
                    continue;
                }

                // Praleidžia įrašus, kurie įvykdyti po tikrinamo pažeidimo įvykdymo datos + jei įvesta pažeidimo įvykdymo data
                if (empty($data['VIOLATION_DATE']) === false && $record_timestamp > $data['VIOLATION_DATE']) {
                    continue;
                }

                // Praleidžia nebegaliojančius įrašus NUO EINAMOSIOS DATOS (ne nuo tikrinamo dokumento pažeidimo įvykdymo datos)
                if ($record_timestamp < strtotime($data['LIMIT'][$record['TABLE_TO_ID']], strtotime(date('Y-m-d')))) {
                    continue;
                }

                // Jei tikrinamam dokumente nurodytas straipsnis, tai turi atitikti rasto dokumento straipsnis arba sutapti jų grupės
                if (empty($data['CLAUSE']) === false) {
                    $clause = $this->atp->logic2->get_record_clause($record['ID']);

                    $valid = false;
                    if (empty($clause) === false && (int) $clause['ID'] === (int) $data['CLAUSE']['ID']) {
                        $valid = true;
                    } elseif (empty($clause['GROUP']) === false && (int) $clause['GROUP'] === (int) $data['CLAUSE']['GROUP']) {
                        $valid = true;
                    }
                    if ($valid === false) {
                        continue;
                    }
                }
                // TODO: jei nėra nurodyto (neranda) straipsnio, tai "else continue;" ?

                $result[] = $record['ID'];
            }
        }

        return $result;
    }

    /**
     * Susiję dokumentai
     * @deprecated removed
     * @param array $data
     * @param array $exclude_records [optional]
     * @return array
     */
    protected function getRecordsPRelated($data, $exclude_records = array())
    {
        $exclude_records = (array) $exclude_records;
        $atp = $this->atp->logic2;

        $default = array(
            // Tikrinamas dokumento įrašas
            'RECORD' => null,
            // Tikrinamo dokumento įrašo pažeidimo data
            'VIOLATION_DATE' => null,
            // T.d.į. asmens kodas
            'CODE' => null,
            // T.d.į. straipsnis
            'CLAUSE' => null,
            // T.d.į. automobilio numeris
            'AUTO_NR' => null,
            // Dokumentai ir būsenos, kuriose nustatomas baustumas
            'SOURCE' => array(
                // Pažeidimo duomenys
                \Atp\Document::DOCUMENT_INFRACTION => array(),
                // Protokolas
                \Atp\Document::DOCUMENT_PROTOCOL => array(
                    41, // Naujas protokolas
                    13, // Surašytas ATP protokolas
                ),
                // Nutarimas
                \Atp\Document::DOCUMENT_CASE => array(
                    20, // Užvesta byla
                    29, // Priimtas nutarimas
                    19 // Nagrinėti
                )
            ),
            'LIMIT' => array()
        );
        $data = array_merge($default, $data);
        // Straipsnio informacija
        if (empty($data['CLAUSE']) === false) {
            $data['CLAUSE'] = $atp->get_clause(array('ID' => $data['CLAUSE']));
        }
        // Dok. įrašo informacija
        if (empty($data['RECORD']) === false) {
            $data['RECORD'] = $atp->get_record_relation($data['RECORD']);

            // Einamojo dokumento įrašo pažeidimo data
            $violation_date = $atp->get_record_violation_date($data['RECORD']['ID']);
            if ($violation_date !== null) {
                $data['VIOLATION_DATE'] = strtotime($violation_date);
            }
        }


        // Prie dokumentų priskirti asmens kodo ir surašymo datos laukai
        $documents = $atp->get_document(array('A.ID IN (' . join(',', array_keys($data['SOURCE'])) . ')'), false, null, 'A.ID');

        $result = array();
        foreach ($documents as &$document) {
            $fields = array(
                // Informacija apie asmens kodo lauką
                'CODE' => $atp->get_fields(array('ID' => $personCodeField = $this->atp->factory->Document()->getOptions($document['ID'])->person_code), true, null, 'ID, NAME, ITYPE'),
                // Informacija apie surašymo datos lauką
                'DATE' => $atp->get_fields(array('ID' => $fillDateField = $this->atp->factory->Document()->getOptions($document['ID'])->fill_date), true, null, 'ID, NAME, ITYPE')
            );
            if (empty($fields['CODE']) === true || empty($fields['DATE']) === true) {
                continue;
            }

            $search = array(
                'CODE' => $data['CODE'],
                'DOCUMENT_ID' => $document['ID'],
                'DOCUMENT_STATUS' => $data['SOURCE'][$document['ID']]
            );

            if (empty($search['CODE'])) {
                continue;
            }

            $records = $this->getRecords($search, $fields['CODE'], $exclude_records);

            if (empty($records)) {
                continue;
            }

            // Filtruoja išrinktus įrašus pagal likusius parametrus ir surenka rastų įrašų RECORD_RELATION.ID
            foreach ($records as $key => &$record) {
                $is_valid = false;

                /* Tinkami įrašai */
                // Jei nenurodytas einamojo pažeidimo auto nr.
                if (empty($auto_nr) === true) {
                    $is_valid = true;
                } else {
                    // Jei rasto įrašo auto nr. sutampa su einamojo pažeidimo auto nr.
                    $record_auto_nr = $atp->get_record_auto_nr($record['ID']);
                    if ($auto_nr === trim($record_auto_nr)) {
                        $is_valid = true;
                    }
                }

                if ($is_valid === true) {
                    $result[] = $record['ID'];
                }
            }
        }

        return $result;
    }

    /**
     * Kiti dokumentai
     * @deprecated removed
     * @param array $data
     * @param array $exclude_records [optional]
     * @return array
     */
    protected function getRecordsPHistory($data, $exclude_records = array())
    {
        $exclude_records = (array) $exclude_records;
        $atp = $this->atp->logic2;

        $default = array(
            // Tikrinamas dokumento įrašas
            'RECORD' => null,
            // Tikrinamo dokumento įrašo pažeidimo data
            'VIOLATION_DATE' => null,
            // T.d.į. asmens kodas
            'CODE' => null,
            // T.d.į. straipsnis
            'CLAUSE' => null,
            // T.d.į. automobilio numeris
            'AUTO_NR' => null,
            // Dokumentai ir būsenos, kuriose nustatomas baustumas
            'SOURCE' => array(),
            'LIMIT' => array()
        );
        $data = array_merge($default, $data);
        // Straipsnio informacija
        if (empty($data['CLAUSE']) === false) {
            $data['CLAUSE'] = $atp->get_clause(array('ID' => $data['CLAUSE']));
        }
        // Dok. įrašo informacija
        if (empty($data['RECORD']) === false) {
            $data['RECORD'] = $atp->get_record_relation($data['RECORD']);

            // Einamojo dokumento įrašo pažeidimo data
            $violation_date = $atp->get_record_violation_date($data['RECORD']['ID']);
            if ($violation_date !== null) {
                $data['VIOLATION_DATE'] = strtotime($violation_date);
            }
        }


        // Prie dokumentų priskirti asmens kodo ir surašymo datos laukai
        $documents = $atp->get_document(null, false, null, 'A.ID');

        $result = array();
        foreach ($documents as &$document) {
            $fields = array(
                // Informacija apie asmens kodo lauką
                'CODE' => $atp->get_fields(array('ID' => $personCodeField = $this->atp->factory->Document()->getOptions($document['ID'])->person_code), true, null, 'ID, NAME, ITYPE')/* ,
                  // Informacija apie surašymo datos lauką
                  'DATE' => $atp->get_fields(array('ID' => $fillDateField = $this->atp->factory->Document()->getOptions($document['ID'])->fill_date), true, null, 'ID, NAME, ITYPE') */
            );
            if (empty($fields['CODE']) === true /* || empty($fields['DATE']) === TRUE */) {
                continue;
            }

            $search = array(
                'CODE' => $data['CODE'],
                'DOCUMENT_ID' => $document['ID'],
                'DOCUMENT_STATUS' => $data['SOURCE'][$document['ID']]
            );

            if (empty($search['CODE'])) {
                continue;
            }

            $records = $this->getRecords($search, $fields['CODE'], $exclude_records);

            if (empty($records)) {
                continue;
            }

            // Filtruoja išrinktus įrašus pagal likusius parametrus ir surenka rastų įrašų RECORD_RELATION.ID
            foreach ($records as $key => &$record) {
                $is_valid = false;

                /* Tinkami įrašai */
                // Jei nenurodytas einamojo pažeidimo auto nr.
                if (empty($auto_nr) === true) {
                    $is_valid = true;
                } else {
                    // Jei rasto įrašo auto nr. sutampa su einamojo pažeidimo auto nr.
                    $record_auto_nr = $atp->get_record_auto_nr($record['ID']);
                    if ($auto_nr === trim($record_auto_nr)) {
                        $is_valid = true;
                    }
                }

                if ($is_valid === true) {
                    $result[] = $record['ID'];
                }
            }
        }

        return $result;
    }

    /**
     * ATPEIR dokumentai
     * @param array $data
     * @return null|AtpDetailsType[]
     */
    protected function getRecordsPAtpeir($data, $_ = null)
    {
        $return = [];
        try {
            $checkCriminalRecord = new \Atp\Atpr\CheckCriminalRecord();
            $return = $checkCriminalRecord->findAtp($checkCriminalRecord->createSearchParameters($data['CODE']));
        } catch (\Exception $e) {
            error_log($e);
            throw $e;
        }

        return $return;
    }

    /**
     * Užklausos duomenys baustumo lentelės pažeidimų išrinkimui
     * @deprecated removed
     * @return array
     */
    private function getQueryData()
    {
        $fields = 'C.*, ( SELECT COUNT(ID) FROM ' . PREFIX . 'ATP_RECORD WHERE RECORD_FROM_ID = C.ID ) NEXT_COUNT';
        return array(
            'MAIN' => array(
                'FIELDS' => $fields,
                'JOINS' => array(
//                    'C' => 'INNER JOIN ' . PREFIX . 'ATP_RECORD C ON C.RECORD_ID = A.RECORD_ID AND C.IS_LAST = 1'
//                    'C' => 'INNER JOIN ' . PREFIX . 'ATP_RECORD C ON C.ID = A.RECORD_ID'
//                    'B' => 'INNER JOIN ' . PREFIX . 'ATP_RECORD_X_RELATION B ON B.RECORD_ID = A.ID AND B.DOCUMENT_ID = :DOCUMENT_ID',
                    'B' => 'INNER JOIN ' . PREFIX . 'ATP_RECORD_X_RELATION B ON B.RECORD_ID = A.ID',
                    'C' => 'INNER JOIN ' . PREFIX . 'ATP_RECORD C ON C.ID = B.RELATION_ID'
                )
            ),
            'EXTRA' => array(
                'FIELDS' => $fields,
                'JOINS' => array(
//                    'B' => 'INNER JOIN ' . PREFIX . 'ATP_TBL_:DOCUMENT_ID B ON B.ID = A.RECORD_ID AND B.RECORD_LAST = 1',
//                    'C' => 'INNER JOIN ' . PREFIX . 'ATP_RECORD C ON C.RECORD_ID = B.RECORD_ID AND C.IS_LAST = 1'
                    'C' => 'INNER JOIN ' . PREFIX . 'ATP_RECORD C ON C.ID = A.RECORD_ID'
                )
            )
        );
    }

    /**
     * Surenka pažeidimus pagal pateiktus parametrus (skirta baustumo lentelei)
     * @deprecated removed
     * @param array $search
     * @param string $search['CODE'] Ieškomas asmens kodas
     * @param array $search['DOCUMENT_ID'] Ieškomas dokumentas
     * @param null|array $search['DOCUMENT_STATUS']  Ieškomo dokumento būsenos
     * @param array $code_field Ieškomo dokumento asmens kodo laukas
     * @param null|array $exclude_records Dokumentų įrašų numeriai, kuriu nereikia
     * @return bool|array Dokumento įrašų masyvas
     */
    private function getRecords(Array $search, Array $code_field, $exclude_records = array())
    {
        $qry_data = $this->getQueryData();

        $param_arr = array();
        $param = array();

        if ($code_field['ITYPE'] === 'DOC') {
            $param[] = '`' . $code_field['NAME'] . '` = "' . $search['CODE'] . '"';
            $param[] = 'RECORD_LAST = 1';
            $param[] = 'B.DOCUMENT_ID = "' . $search['DOCUMENT_ID'] . '"';

            if (empty($search['DOCUMENT_STATUS']) === false) {
                $param[] = 'C.DOC_STATUS IN ("' . join('","', $search['DOCUMENT_STATUS']) . '")';
            }
            if (empty($exclude_records) === false) {
//                $param[] = 'A.RECORD_ID NOT IN ("' . join('","', $exclude_records) . '")';
                $param[] = 'C.RECORD_ID NOT IN ("' . join('","', $exclude_records) . '")';
            }

            $param_arr = array(
                0 => PREFIX . 'ATP_TBL_' . $search['DOCUMENT_ID'],
                5 => $qry_data['MAIN']['FIELDS'],
                6 => $qry_data['MAIN']['JOINS']
            );
        } else {
//            $param['A.TABLE_ID'] = $search['DOCUMENT_ID'];
            $param['A.FIELD_ID'] = (int) $code_field['ID'];
            $param['A.VALUE'] = $search['CODE'];

            if (empty($search['DOCUMENT_STATUS']) === false) {
                $param[] = 'C.DOC_STATUS IN ("' . join('","', $search['DOCUMENT_STATUS']) . '")';
            }
            if (empty($exclude_records) === false) {
//                $param[] = 'A.RECORD_ID NOT IN ("' . join('","', $exclude_records) . '")';
                $param[] = 'C.RECORD_ID NOT IN ("' . join('","', $exclude_records) . '")';
            }

//            $qry_data['EXTRA']['JOINS']['B'] = str_replace(':DOCUMENT_ID', $search['DOCUMENT_ID'], $qry_data['EXTRA']['JOINS']['B']);
            $param_arr = array(
                0 => PREFIX . 'ATP_FIELDS_VALUES',
                5 => $qry_data['EXTRA']['FIELDS'],
                6 => $qry_data['EXTRA']['JOINS']
            );
        }

        // Išrinkti įrašai
        $param_arr[1] = $param;
        $param_arr[2] = false;
        $param_arr[3] = null;
        $param_arr[4] = false;
        ksort($param_arr);

        $records = call_user_func_array(array($this->atp->logic2, 'get_db'), $param_arr);

        foreach ($records as $key => $record) {
            if ((int) $record['NEXT_COUNT'] > 0) {
                unset($records[$key]);
            }
        }

        return $records;
    }
}
