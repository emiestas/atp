<?php

namespace Atp\Document\Record;

class Payment
{
    const STATUS_NULL = 0;

    const STATUS_NOT_PAID = 1;

    const STATUS_PAID = 2;

    const STATUS_FINISH_IDLE = 3;

    /**
     * @var \Atp\Core
     */
    private $atp;

    /**
     * Available payment statuses information.
     * 
     * @var array[]
     */
    private $paymentStatuses;

    /**
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;

        $this->paymentStatuses = [
            \Atp\Document\Record\Payment::STATUS_NULL => [
                'ID' => \Atp\Document\Record\Payment::STATUS_NULL,
                'TAG' => 'null',
                'LABEL' => '-- Būsena --'
            ],
            \Atp\Document\Record\Payment::STATUS_NOT_PAID => [
                'ID' => \Atp\Document\Record\Payment::STATUS_NOT_PAID,
                'TAG' => 'not_paid',
                'LABEL' => 'Neapmokėta'
            ],
            \Atp\Document\Record\Payment::STATUS_PAID => [
                'ID' => \Atp\Document\Record\Payment::STATUS_PAID,
                'TAG' => 'paid',
                'LABEL' => 'Apmokėta'
            ],
            \Atp\Document\Record\Payment::STATUS_FINISH_IDLE => [
                'ID' => \Atp\Document\Record\Payment::STATUS_FINISH_IDLE,
                'TAG' => 'finish_idle',
                'LABEL' => 'Baigti tarpinę būseną'
            ]
        ];
    }

    /**
     * Get available payment statuses information.
     * 
     * @return array[]
     */
    private function getPaymentStatuses()
    {
        return $this->paymentStatuses;
    }

    /**
     * Get payment status information by status ID or TAG.
     * 
     * @param int|string $data Payment status ID or TAG.
     * @return bool|array Payment status information or false.
     */
    public function getPaymentStatus($statusIdOrTag)
    {
        if ($statusIdOrTag === null) {
            $statusIdOrTag = \Atp\Document\Record\Payment::STATUS_NULL;
        }

        if (ctype_digit((string) $statusIdOrTag) === true) {
            $checkColumn = 'ID';
            $statusIdOrTag = (int) $statusIdOrTag;
        } else {
            $checkColumn = 'TAG';
        }

        $options = $this->getPaymentStatuses();
        foreach ($options as &$option) {
            if (isset($option[$checkColumn]) === false) {
                break;
            }

            if ($option[$checkColumn] === $statusIdOrTag) {
                return $option;
            }
        }

        return false;
    }

    /**
     * Get available payments statuses labels.
     *
     * @return string[]
     */
    public function getPaymentStatusesLabels()
    {
        return array_map(function ($arr) {
            return $arr['LABEL'];
        }, $this->getPaymentStatuses());
    }

    /**
     * Ištrina įrašų ir mokėjimų sąryšius
     * @author Justinas Malūkas
     * @param array $params Parametrai
     * @return boolean
     */
    public function removePaymentData($params = array())
    {
        $where = array();
        $arg = array();
        foreach ($params as $key => $value) {
            if (is_int($key) === true) {
                $where[] = ':' . $key;
                $key = '#' . $key . '#';
            } else {
                $key = trim($key, '#');
                $where[] = '`' . $key . '`' . ' = :' . $key;
            }
            $arg[$key] = $value;
        }
        $where = count($where) ? ' WHERE ' . join(' AND ', $where) : '';

        $qry = 'DELETE FROM ' . PREFIX . 'ATP_RECORD_PAYMENTS ' . $where;

        /* */
//		#
        # Vartotojas ištrynė įrašų ir mokėjimų sąryšius.
        #
        $sp = array('<br/>Vartotojas ištrynė įrašų ir mokėjimų sąryšius. RawInfo - ' . json_encode($params));
        $this->atp->manage->writeATPactionLog($sp);
        $this->atp->db_query_fast($qry, $arg);

        return true;
    }

    /**
     * Gauna dokumento įrašui priskirtą baudos mokėjimo būseną
     * @param \Atp\Document\Record $record Dokumento įrašas
     * @return array
     */
    public function GetRecordPaymentOption(\Atp\Document\Record $record)
    {
        $result = $this->atp->db_query_fast('
            SELECT
                PAYMENT_STATUS,
                DATE
			FROM
                ' . PREFIX . 'ATP_RECORD_PAID
			WHERE
                RECORD_ID = :ID', ['ID' => $record->record['ID']]);

        $count = $this->atp->db_num_rows($result);
        if ($count) {
            $data = $this->atp->db_next($result);
            $option = $this->getPaymentStatus($data['PAYMENT_STATUS']);
            if ($option) {
                $option['DATE'] = $data['DATE'];
                return $option;
            }
        }

        return false;
    }

    /**
     * Check whether penalty for record is paid.
     * 
     * @param int $recordId Record ID.
     * @return boolean
     */
    public function isPaid($recordId)
    {
        $result = $this->atp->db_query_fast('
            SELECT
                PAYMENT_STATUS
            FROM
                ' . PREFIX . 'ATP_RECORD_PAID
            WHERE
                RECORD_ID = :ID', ['ID' => $recordId]);

        $count = $this->atp->db_num_rows($result);
        if ($count) {
            $statusId = $this->atp->db_next($result)['PAYMENT_STATUS'];
            $option = $this->getPaymentStatus($statusId);
            if (isset($option['ID']) && $option['ID'] === \Atp\Document\Record\Payment::STATUS_PAID) {
                return true;
            }
        }

        return false;
    }

    /**
     * Nurodo dokumento įrašo baudos mokėjimo būseną
     * @param \Atp\Document\Record $record Dokumento įrašas
     * @param int|string $option Tai ką priima <b>getPaymentStatus()</b> (galimos dokumento mokėjimo būsenos ID arba TAG)
     * @return bool
     */
    public function SetRecordPaymentOption(\Atp\Document\Record $record, $option)
    {
        $option = $this->getPaymentStatus($option);
        if (empty($option)) {
            return false;
        }

        $result = true;
        // Papildomas veiksmas
        if (empty($option['TAG']) === false) {
            $method = 'action_' . $option['TAG'];
            if (method_exists($this, $method) === true) {
                $result = (bool) $this->$method($record);
            }
        }

        if ($result === true) {
            $this->atp->db_query_fast('
                INSERT INTO ' . PREFIX . 'ATP_RECORD_PAID
                    (RECORD_ID, PAYMENT_STATUS)
				VALUES
                    (:RECORD_ID, :PAYMENT_STATUS)
				ON DUPLICATE KEY UPDATE
					PAYMENT_STATUS = VALUES(PAYMENT_STATUS)', [
                'RECORD_ID' => $record->record['ID'],
                'PAYMENT_STATUS' => $option['ID']
            ]);
        }

        return $result;
    }

    /**
     * Veiksmas, kai baudos mokėjimo būsena nustatoma "Nesumokėta".
     * @param \Atp\Document\Record $record Dokumento įrašas
     * @return boolean
     */
    private function action_not_paid(\Atp\Document\Record $record)
    {
        //$this->atp->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD
        //	SET IS_PAID = 0
        //	WHERE ID = :ID', array('ID' => $record->record['ID']));

        return true;
    }

    /**
     * Veiksmas, kai baudos mokėjimo būsena nustatoma "Sumokėta".
     * Nurodoma sena apmokėjimo žyma ir dokumento statusas "Baigta".
     * Pašalinami numatyti nagrinėtojai.
     * Pašalinami sudsieti tvarkaraščio duomenys.
     * @param \Atp\Document\Record $record Dokumento įrašas
     * @return boolean
     */
    private function action_paid(\Atp\Document\Record $record)
    {
        $arg = [
            'RECORD_ID' => $record->record['ID']
        ];

        // Nurodoma sena apmokėjimo žyma ir statusas "Baigta"
        $this->atp->db_query_fast('
            UPDATE ' . PREFIX . 'ATP_RECORD
			SET
				#IS_PAID = 1,
				STATUS = ' . \Atp\Record::STATUS_FINISHED . '
			WHERE ID = :RECORD_ID', $arg);


        // Pašalinami numatyti nagrinėtojai
        $workerManager = new \Atp\Record\Worker($this->atp);
        $workerManager->removeRecordWorker([
            'RECORD_ID=' . (int) $arg['RECORD_ID'],
            'TYPE IN("exam", "next")'
        ]);

        // Pasalinam is tvarkarascio
        // TODO: web-servisas
        $sch = prepare_schedule();
        $events = $sch->logic->getEventsBySoft(['SOFT' => 'atp', 'ITEM' => $record->record['RECORD_ID']]);
        foreach ($events as $event) {
            $sch->logic->disableEvent($event['ID']);
        }

        return true;
    }

    /**
     * Veiksmas, kai baudos mokėjimo būsena nustatoma "Baigti tarpinę būseną".
     * @param \Atp\Document\Record $record Dokumento įrašas
     * @return boolean
     */
    private function action_finish_idle(\Atp\Document\Record $record)
    {
        //$this->atp->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD
        //	SET IS_PAID = NULL
        //	WHERE ID = :ID', array('ID' => $record->record['ID']));
        // TODO: veiksmai su dokumentu
        return true;
    }

    /**
     * Veiksmas, kai nepasirinkta baudos mokėjimo būsena
     * @param \Atp\Document\Record $record Dokumento įrašas
     * @return boolean
     */
    private function action_null(\Atp\Document\Record $record)
    {
        return true;
    }
}
