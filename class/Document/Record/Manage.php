<?php

// TODO: Į čia perkelti saugojimą

class ATP_Document_Record_Manage {

	/**
	 * @var ATP_Core 
	 */
	private $atp;

	/**
	 * @var ATP_Document_Record 
	 */
	private $record;

	/**
	 * Talpina saugojimo informaciją
	 * @var array 
	 */
	private $save;

	/**
	 * Talpina perkėlimo informaciją
	 * @var array 
	 */
	private $transfer;

	/**
	 * Saugojimo rezultato informacija
	 * @var array
	 */
	public $result;

	/**
	 * 
	 * @param ATP_Document_Record $record
	 * @param ATP_Core $atp
	 */
	public function __construct(ATP_Document_Record $record, ATP_Core &$atp) {
		$this->atp = &$atp;
		$this->record = &$record;

		$this->save = array();
		$this->transfer = array();
		$this->result = array();
	}

	//public function Save() {
	// TODO: saugojimas
	// Po išsaugoijimo
	//	$this->save_after();
	//}

	/**
	 * <i>save_after()</i> iškvietimas, kai sugojimas vyksta ne per šią (<i>ATP_Document_Record_Manage</i>) klasę
	 * @return null
	 */
	public function SaveAfter_Walkaround() {
		$this->save_after();
		$this->set_result($result);
		return $this->result;
	}

	// TODO: Save()
	public function Save() {
		die('Save()');
		$result = $this->save_record($document);
		$this->set_result($result);

		return $this->result;
	}

	/**
	 * Dokumento įrašo perkelimas į kitą dokumentą
	 * @param int $document
	 * @return array
	 */
	public function TransferTo($document) {
		$result = $this->transfer_to_document($document);
		$this->set_result($result);

		return $this->result;
	}

	// TODO: Pritaikius pašalinti $redirect iš transferTo, transferToDocument ir create_record
	/**
	 * Surenka saugojimo informaciją
	 * @param bool $result
	 */
	private function set_result($result) {
		$this->atp->get_messages_sess();
		$default = array(
			'result' => (bool) $result,
			'messages' => $this->atp->_message,
			'redirect' => $this->atp->logic->create_record_redirect_url,
			'saved_record_id' => null
		);
		$this->atp->unset_messages_sess();

		$this->result = array_merge($default, (array) $this->result);
	}

	// TODO: save_record()
	private function save_record() {
		die('save_record()');
	}

	/**
	 * Perkelia dokumento įrašą į kitą dokumentą
	 * @param int $document
	 * @return boolean
	 */
	private function transfer_to_document($document) {
		$atp = &$this->atp;
		$record = &$this->record->record;
		$to_document = $atp->logic2->get_document($document);

		if (empty($record['ID']) === TRUE)
			return false;

		$this->transfer = array(
			'record' => $record,
			'to_document' => $to_document);

		$atp->logic->_r_path = TRUE;
		$atp->logic->_r_r_id = $record['ID'];
		$atp->logic->_r_record_id = $record['RECORD_ID'];
		$atp->logic->_r_record_status = $record['STATUS'];

		$atp->logic->_r_table_id = $atp->logic->_r_table_from_id = $record['TABLE_TO_ID'];
		$atp->logic->_r_table_to_id = $to_document['ID'];

		$result = $this->transfer_validate_path();
		if ($result === FALSE) {
			$atp->set_message('error', 'Iš šios dokumento versijos jau buvo sukurtas sekantis dokumentas');
			$this->result['redirect'] = 'index.php?m=5&id=' . $record['ID'];
			return false;
		}

		$result = $this->transfer_values();

		$atp->logic->_r_values = $result['values'];
		$atp->logic->_r_files = $result['files'];

		$new_record_id = $atp->logic->create_record('submit_path');
		if (ctype_digit((string) $new_id) === TRUE) {
			/* $new_record_nr = $to_document['ID'] . 'p' . $new_id;
			  $new_record_table = $this->atp->logic2->get_record_table($new_record_nr, 'C.ID, C.RECORD_ID, A.ID RID');
			  $this->result['saved_record_id'] = (int) $new_record_table['RID']; */
			/* $new_record_relation = $atp->logic2->get_db(PREFIX . 'ATP_RECORD_X_RELATION', array('DOCUMENT_ID' => $atp->logic->_r_table_id,'RECORD_ID' => $new_id), true, null, false, 'ID, RELATION_ID');
			  $this->result['saved_record_id'] = (int) $new_record_relation['RELATION_ID']; */
			$this->result['saved_record_id'] = (int) $new_record_id;

			return true;
		}

		return false;
	}

	/**
	 * Tikrina ar leidžaiama perkelti į nurodytą dokumentą
	 * @return boolean
	 */
	private function transfer_validate_path() {
		$atp = &$this->atp;
		// TODO: TRUE = ($this->transfer['record'] === $this->record->record)
		//$record = &$this->record->record;
		$transfer = &$this->transfer;

		$main_path = array(56, 52, 36, 57);

		// Negalima tos pačios dokumento versijos antrą kartą perduoti į kitą dokumentą pagrindiniame kelyje
		//$current_record = $atp->logic2->get_record_relations(array('RECORD_ID' => $record['RECORD_ID'], 'IS_LAST' => 1), true, null, 'A.*');
		//$current_record = $atp->logic2->get_record_relations(array('ID' => $transfer['record']['ID']), true, null, 'A.*');
		//if (in_array($current_record['TABLE_TO_ID'], $main_path) === TRUE && in_array($transfer['to_document']['ID'], $main_path) === TRUE) {
		if (in_array($transfer['record']['TABLE_TO_ID'], $main_path) === TRUE && in_array($transfer['to_document']['ID'], $main_path) === TRUE) {
			$next_record = $atp->logic2->get_record_relations(array(
				//'RECORD_FROM_ID' => $current_record['ID'],
				'RECORD_FROM_ID' => $transfer['record']['ID'],
				'TABLE_TO_ID' => $transfer['to_document']['ID']), true, null, 'A.*');

			if (empty($next_record) === FALSE)
				return false;
		}

		return true;
	}

	/**
	 * Perkeliamo dokumento reikšmės naujame dokumente
	 * @return array
	 */
	private function transfer_values() {
		$atp = &$this->atp;
		$transfer = &$this->transfer;

		// Naujo dokumento perduoti duomenys
		$values = $atp->logic->table_compatibility(array(
			'RECORD_NR' => $transfer['record']['ID'],
			'FROM_TABLE' => $transfer['record']['TABLE_TO_ID'],
			'TO_TABLE' => $transfer['to_document']['ID']
		));
		$values = array_change_key_case((array) $values, CASE_LOWER);


		// Naujo dokumento perduoti duomenys // Tikslinimas pagal tipus 
		$relations = $atp->logic->get_table_relations($transfer['record']['TABLE_TO_ID'], $transfer['to_document']['ID']);

		foreach ($relations as $relation) {
			$field = $atp->logic2->get_fields(array('ID' => $relation['COLUMN_ID']));

			// Veika // Veikos lauko ryšys perduoda visus žemesnio lygio laukus (laukus, kurie yra saugomi laukų lent.)
			if ($field['TYPE'] === '23') {
				$data = array(
					'FROM' => array(
						'TABLE' => $relation['TABLE_PARENT_ID'],
						'FIELD_ID' => $relation['COLUMN_PARENT_ID'],
						'FIELD_NAME' => $relation['COLUMN_PARENT_NAME'],
						'VALUE' => $values[strtolower($relation['COLUMN_NAME'])]
					),
					'TO' => array(
						'TABLE' => $relation['TABLE_ID'],
						'FIELD_ID' => $relation['COLUMN_ID'],
						'FIELD_NAME' => $relation['COLUMN_NAME']
					)
				);


				$from_record_table = $atp->logic2->get_record_table($transfer['record']['ID'], 'C.*, A.ID RID');
				$from_document = $atp->logic2->get_document($transfer['record']['TABLE_TO_ID']);
				$from_structure = $atp->logic2->get_fields_all(array('ITYPE' => 'DOC', 'ITEM_ID' => $transfer['record']['TABLE_TO_ID']));

				// Gauna pažeidimo datą, pagal kurią atrenkamas teisės akto punktas
				$from_date_field = $from_structure[$from_document['DATE_FIELD']];
				$from_date = date('Y-m-d H:i:s');
				if (empty($from_date_field) === FALSE) {
					if (isset($from_record_table[$from_date_field['NAME']]))
						$from_date = $from_record_table[$from_date_field['NAME']];
					else
						$from_date = $atp->logic->get_extra_field_value($from_record_table['RID'], $from_date_field['ID']);
				}


				// Žemesni veikos laukai
				$params = array(
					'ID' => $data['FROM']['VALUE'],
					'TYPE' => 'DEED',
					'RECORD_DATE' => $from_date
				);
				$deed_children_fields = $atp->logic->get_fields_by_type_and_id($params);

				$new_values = array();
				foreach ($deed_children_fields as $child_field) {
					$new_values[strtolower($child_field['NAME'])] = $atp->logic->get_extra_field_value($from_record_table['RID'], $child_field['ID']);
				}
				$values = array_merge((array) $values, $new_values);
			} else
			// Failas
			if ($field['SORT'] === '10' && $values[strtolower($field['NAME'])] === '1') {
				//$parent_field = $atp->logic2->get_fields(array('ID' => $relation['COLUMN_ID']));
				$parent_field = $atp->logic2->get_fields(array('ID' => $relation['COLUMN_PARENT_ID']));

				$parent_files = $atp->logic2->get_record_files(array(
					'FIELD_ID' => $parent_field['ID'],
					'RECORD_ID' => $transfer['record']['ID']
						), FALSE, null, 'ID, FILE_ID');
				foreach ($parent_files as $file) {
					$files[$field['ID']][] = $file['FILE_ID'];
				}
				unset($parent_files);
			}
		}

		return array(
			'values' => $values,
			'files' => $files
		);
	}

	private function save_after() {
		$atp = &$this->atp;
		$record = &$this->record->record;

		// Būsenai "Nutraukta byla, gražinta tirti"
		$status_obj = new ATP_Document_Record_Status(array('RECORD' => $record['ID']), $atp);
		$result = $status_obj->CheckSingleStatus(63);

		if (empty($result) === FALSE) {
			$result = array_shift($result);
			$result = $status_obj->SetRecordStatus($result['RECORD'], $result['STATUS']);

			if ($result === TRUE) {

				// Suranda paskutinį pažeidimo duomenų dokumento įrašą
				// TODO: naudoti ATP_Document_Record_Path
				$record_path = $atp->logic->get_record_path($record['ID']);
				$filtered_records = array_filter($record_path, function($array) {
					if ((int) $array['TABLE_TO_ID'] !== 52)
						return false;
					return true;
				});

				$count = count($filtered_records);
				if ($count === 0) {
					throw new Exception('Dokumentų kelyje nerastas nei vienas pažeidimo duomenų dokumentas');
				} else
				if ($count === 1) {
					$from_record = array_shift($filtered_records)['ID'];
				} else if ($count > 1) {
					$versions = array_map(function($array) {
						return (int) $array['RECORD_VERSION'];
					}, $filtered_records);
					$from_record = $filtered_records[array_search(max($versions), $versions, true)]['ID'];
				}

				$result = $this->save_new_version(new ATP_Document_Record($from_record, $atp));

				if ($result === TRUE) {
					// Baigiamas bylos dokumentas
					$atp->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD_RELATIONS
						SET
							STATUS = 5
						WHERE
							ID = ' . (int) $record['ID']);
					// Nauja pažeidimo duomenų versija perkeliama į tyrimą
					$atp->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD_RELATIONS
						SET
							STATUS = 2
						WHERE
							ID = ' . (int) $this->result['saved_record_id']);

					$record_table = $atp->logic2->get_record_table($this->result['saved_record_id']);




					$worker = $atp->logic2->get_record_worker(array('A.RECORD_ID = ' . (int) $this->result['saved_record_id'], 'A.TYPE = "invest"'), true);
					$message = array('Nutraukta byla, gražinta tirti.');
					$message[] = 'Sukurtas dokumentas "<a href="' . $atp->config['SITE_URL'] . '/index.php?m=5&id=' . $this->result['saved_record_id'] . '">' . $record_table['RECORD_ID'] . '</a>".';
					$message[] = 'Dokumentas perduotas į "' . $worker['STRUCTURE_NAME'] . '" skyrių<br>' .
							'Atsakingas asmuo: ' . $worker['FIRST_NAME'] . ' ' . $worker['LAST_NAME'];
					$atp->set_message('success', join('<br/>', $message));
				} else {
					// TODO: ???
				}

				$this->set_result($result);
			}
		}
	}

	private function save_new_version(ATP_Document_Record $record) {
		$atp = &$this->atp;
		$record = &$record->record;

		if (empty($record['ID']) === TRUE)
			return false;

		$this->save = array('record' => $record);

		$atp->logic->_r_path = FALSE;
		$atp->logic->_r_r_id = $record['ID'];
		$atp->logic->_r_record_id = $record['RECORD_ID'];
		$atp->logic->_r_record_status = $record['STATUS'];

		$atp->logic->_r_table_id = $atp->logic->_r_table_from_id = $record['TABLE_TO_ID'];
		$atp->logic->_r_table_to_id = $record['TABLE_TO_ID'];

		$result = $this->save_values();

		$atp->logic->_r_values = $result['values'];
		$atp->logic->_r_files = $result['files'];

		$new_record_id = $atp->logic->create_record('save_new');
		if (ctype_digit((string) $new_record_id) === TRUE) {
			$this->result['saved_record_id'] = (int) $new_record_id;
			return true;
		}

		return false;
	}

	private function save_values() {
		$atp = $this->atp;
		$save = $this->save;

		$values = $this->atp->logic2->get_record_values(array('RECORD' => $save['record']['ID'], 'WITH_NAME' => true));
		$values = array_change_key_case($values, CASE_LOWER);

		$files = array();
		$fields = $atp->logic2->get_fields_all(array(
			'ITEM_ID' => $save['record']['TABLE_TO_ID'],
			'ITYPE' => 'DOC'));
		foreach ($fields as &$field) {
			if ($field['SORT'] !== '10')
				continue;

			$files_data = $atp->logic2->get_record_files(array(
				'FIELD_ID' => $field['ID'],
				'RECORD_ID' => $save['record']['ID']
					), FALSE, null, 'ID, FILE_ID');
			foreach ($files_data as &$file) {
				$files[$field['ID']][] = $file['FILE_ID'];
			}
		}

		return array(
			'values' => $values,
			'files' => $files
		);
	}

}
