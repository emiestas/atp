<?php

namespace Atp\Document\Record;

// TODO: update & use
class Path
{

    public function __construct()
    {

    }

    /**
     *
     * @param int $document_id Dokumento ID
     * @param string $record_id Dokumento įrašo ID
     * @return array
     */
    public function get_record_path($document_id, $record_id)
    {
        // Gauna kelyje esančius dokumentų įrašus
        $records = $this->record_path($document_id, $record_id);

        // Surenka papildomus duomenis
        foreach ($records as $key => &$record) {
            $qry = 'SELECT
                    A.ID, A.RECORD_ID, A.TABLE_TO_ID, A.CREATE_DATE, A.STATUS, A.DOC_STATUS,
					C.RECORD_MAIN_ID, C.RECORD_VERSION,
					D.STATUS as DOCUMENT_STATUS
				FROM
					' . PREFIX . 'ATP_RECORD A
						INNER JOIN ' . PREFIX . 'ATP_RECORD_X_RELATION B
							ON
								B.RELATION_ID = A.ID AND
								B.DOCUMENT_ID = :DOCUMENT_ID
						INNER JOIN ' . PREFIX . ':TABLE_NAME C ON
                            C.ID = B.RECORD_ID
						INNER JOIN ' . PREFIX . 'ATP_DOCUMENTS D ON
                            D.ID = A.TABLE_TO_ID
				WHERE
					A.ID = :ID';

            $result = $this->core->db_query_fast($qry, [
                '#TABLE_NAME#' => 'ATP_TBL_' . $record['TABLE_TO_ID'],
                'DOCUMENT_ID' => $record['TABLE_TO_ID'],
                'ID' => $record['ID']]);

            $row = $this->core->db_next($result);

//            $workerType = null;
//            if ((int) $row['DOCUMENT_STATUS'] === \Atp\Entity\Document::STATUS_BOTH) {
//                $workerType = '"invest", "exam"';
//            } elseif ((int) $row['DOCUMENT_STATUS'] === \Atp\Entity\Document::STATUS_INVEST) {
//                $workerType = '"invest"';
//            } elseif ((int) $row['DOCUMENT_STATUS'] === \Atp\Entity\Document::STATUS_EXAM) {
//                $workerType = '"exam"';
//            }
//
//            if (empty($workerType)) {
//                $statuses = [
//                    \Atp\Entity\Document::STATUS_BOTH,
//                    \Atp\Entity\Document::STATUS_INVEST,
//                    \Atp\Entity\Document::STATUS_EXAM,
//                ];
//                throw new \Exception(
//                'Invalid worker type.'
//                . ' Received "' . $row['DOCUMENT_STATUS'] . '".'
//                . ' Expected one of folowing: ' . join(', ', $statuses) . '.'
//                );
//            }
            
            $recordManager = new \Atp\Record($this->atp);
            $worker = $recordManager->getWorker($row['ID']);

            $row['FIRST_NAME'] = $worker['FIRST_NAME'];
            $row['LAST_NAME'] = $worker['LAST_NAME'];

            $record = array_merge($record, (array) $row);
        }

        return $records;
    }

    /**
     * Dokumento įrašo kelyje esantys dokumentų įrašai
     * @param int $document_id Dokumento ID
     * @param string $record_id Dokumento įrašo ID
     * @return array Dokumentų įrašų informacija.<br/>Min.: [ID, TABLE_FROM_ID, TABLE_TO_ID]
     */
    private function record_path($document_id, $record_id)
    {
        // Gauna įrašo pagrindini (pradinį) numerį, kuris nekinta ryšiu susietuose dokumento įrašuose
        $qry = 'SELECT C.RECORD_MAIN_ID
				FROM
					' . PREFIX . 'ATP_RECORD A
						INNER JOIN ' . PREFIX . 'ATP_RECORD_X_RELATION B ON B.RELATION_ID = A.ID AND B.DOCUMENT_ID = :DOCUMENT_ID
						INNER JOIN ' . PREFIX . ':TABLE_NAME C ON C.ID = B.RECORD_ID
				WHERE
					A.ID = :ID';
        $result = $this->core->db_query_fast($qry, [
            '#TABLE_NAME#' => 'ATP_TBL_' . $document_id,
            'DOCUMENT_ID' => $document_id,
            'ID' => $record_id]);
        $row = $this->core->db_next($result);

        // Pradinio dokumento informacija
        $first_record = $this->logic2->get_record_relation($row['RECORD_MAIN_ID']);
        // Nuo pradinio dokumento surenka visus ryšiu susietus dokumento įrašs
        $data = $this->record_path_forward($first_record['ID']);
        $data[$first_record['ID']] = $first_record;

        return $data;
    }

    /**
     * Suranda vėlesnius ryšiu susijusius dokumentų įrašus
     * @param int $record_id Dokumento įrašo ID
     * @return array Dokumentų įrašų informacija.<br/>Min.: [ID, TABLE_FROM_ID, TABLE_TO_ID]
     */
    private function record_path_forward($record_id, &$ret = array())
    {
        $qry = 'SELECT A.ID, A.TABLE_FROM_ID, A.TABLE_TO_ID
					FROM
						' . PREFIX . 'ATP_RECORD A
					WHERE
						A.RECORD_FROM_ID = :RECORD_FROM_ID';
        $result = $this->core->db_query_fast($qry, ['RECORD_FROM_ID' => $record_id]);
        while ($row = $this->core->db_next($result)) {
            if ($this->i > 500) {
                trigger_error('Fatal error: Terminated due to stacked loop', E_USER_ERROR);
            }
            $this->i++;

            $ret[$row['ID']] = $row;

            $this->record_path_forward($row['ID'], $ret);
        }

        return $ret;
    }

    /**
     * Suranda ryšiu susijusius dokumentų įrašus. Pirma suranda visus iki pradinį ir tada nuo pradinio perieško į priekį
     * @deprecated Overkill. Nebenaudojamas ir nenaudoti. Kadangi ieškoma į priekį iki pat galo, rezultatas nebeatitinka pavadinimo.
     * @param int $record_id Dokumento įrašo ID
     * @return array
     */
    private function record_path_backward($record_id, &$ret = array())
    {
        $qry = 'SELECT A.ID, A.RECORD_FROM_ID, A.TABLE_FROM_ID, A.TABLE_TO_ID
			FROM
				' . PREFIX . 'ATP_RECORD A
			WHERE
				A.ID = :ID';
        $result = $this->core->db_query_fast($qry, ['ID' => $record_id]);

        while ($row = $this->core->db_next($result)) {
            if ($this->i > 500) {
                trigger_error('Fatal error: Terminated due to stacked loop', E_USER_ERROR);
            }
            $this->i++;

            $row['parent_id'] = $row['RECORD_FROM_ID'];
            $ret[$row['ID']] = $row;
            if (empty($row['RECORD_FROM_ID'])) {
                continue;
            }

            $this->record_path_backward($row['RECORD_FROM_ID'], $ret);
        }

        // Kai reikia pereit per visus atbuline tvarka rastus dokumentus, nes kiekvienas iš jų gali turėti vaikų // Overkill
        $last = end($ret);
        $this->record_path_forward($last['ID'], $ret);

        return $ret;
    }
}
