<?php

namespace Atp\Document\Record;

class Logics
{

    /**
     *
     * @var \Atp\Entity\Record
     */
    public $record;

    /**
     * @var \Atp\Core
     */
    public $atp;

    /**
     * @param int $recordId
     * @param \Atp\Core $atp
     */
    public function __construct($recordId, $atp)
    {
        $this->atp = $atp;
        $this->record = $this->atp->factory->Record()->getEntity($recordId);
    }

    /**
     * Get classificator id for "Protokolo tipas" field.
     * 
     * @return int
     */
    public function getProtocolClassificator()
    {
        if ($this->record->getDocument()->getId() !== \Atp\Document::DOCUMENT_PROTOCOL) {
            return false;
        }

        $infractionRecord = $this->atp->factory->Record()->getPreviousDocumentFor(
            $this->record->getId(),
            \Atp\Document::DOCUMENT_INFRACTION
        );
        if (!$infractionRecord) {
            return false;
        }
        
        $result = $this->atp->db_query_fast('
            SELECT COUNT(ID) CNT
            FROM
                ' . PREFIX . 'ATP_VIOLATIONS
            WHERE
                VIOLATION_TYPE = "Atpeir" AND
                RECORD_ID = :RECORD_ID
        ', ['RECORD_ID' => $infractionRecord->getId()]);

        $data = $this->atp->db_next($result);

        if ((bool) (int) $data['CNT']) {
            return \Atp\Document::PROTOCOL_WITHOUT_AN;
        }

        return \Atp\Document::PROTOCOL_WITH_AN;
    }

    /**
     * Get first penalty amount.
     * 
     * @return int
     */
    public function getFirstPenalty()
    {
        if ($this->record->getDocument()->getId() !== \Atp\Document::DOCUMENT_PROTOCOL) {
            return false;
        }

        $clause = $this->atp->logic2->get_record_clause($this->record->getId());
        return $clause['PENALTY_FIRST'];
    }
}
