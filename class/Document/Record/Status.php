<?php

namespace Atp\Document\Record;

class Status extends \Atp\Core
{
    /**
     * @var \Atp\Core
     */
    public $core;

    /**
     * @var bool
     */
    public $debug = false;

    /**
     * Dokumento įrašo duomenys kuriame tikrinamos būsenos
     * @var stdClass
     */
    public $record;

    /**
     * Tikrinamo dokumento įrašo tėvinis dokumento įrašas
     * @var stdClass
     */
    public $previous_record;

    /**
     * Parametras nurodantis ar būsenos yra tikrinamos per cronjob'ą
     * @var bool
     */
    public $_cronjob = false;

    /**
     * @param array $arr
     * @param mixed $arr['RECORD'] Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param \Atp\Core $core
     */
    public function __construct($arr, \Atp\Core &$core)
    {
        $this->core = &$core;
        $this->setRecord($arr['RECORD']);
    }

    public function SetDebug($value)
    {
        $this->debug = (bool) $value;
    }

    /**
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @return boolean
     */
    public function SetRecord($record)
    {
        unset($this->record, $this->previous_record);
        $record = $this->core->logic2->get_record_relation($record);
        if (empty($record) === true) {
            trigger_error('Record not found', E_USER_ERROR);
            exit;
        }

        if (empty($record) === false) {
            $this->record = (object) array_change_key_case($record, CASE_LOWER);
            $record = $this->core->logic2->get_record_relation($this->record->record_from_id);
            if (empty($record) === false) {
                $this->previous_record = (object) array_change_key_case($record, CASE_LOWER);
            }

            return true;
        }

        return false;
    }

    /**
     * @param int $status_id Dokumento būsenos ID
     * @return bool|array array('STATUS' => {status_id}, 'RECORD' => {record_id}) arba FALSE, jei būsenos tikrinimo metodas nerastas
     */
    public function CheckSingleStatus($status_id)
    {
        $method = 'status_' . (int) $status_id;
        if (method_exists($this, $method) === true) {
            return $this->parse_status_result($this->$method(), $status_id);
        }

        return false;
    }

    /**
     * Nurodo dokumeno įrašo būsenos ID
     * @param int $record_id Dokumento įrašo ID
     * @param int $status_id Būsenos ID
     * @return boolean
     */
    public function SetRecordStatus($record_id, $status_id)
    {
        if (empty($record_id) === true || ctype_digit((string) $record_id) === false) {
            return false;
        }

        $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD
			SET
				DOC_STATUS = :STATUS
			WHERE
				ID = :RECORD AND
				IS_LAST = 1', array('RECORD' => (int) $record_id, 'STATUS' => (int) $status_id));

        return true;
    }

    /**
     * Suvienodina galimus būsenos (<i>$this->status_???()</i>) metodo rezultatus
     * @param bool|int|string|array $status_result Būsenos metodo rezultatas
     * @param null|int $status_id Tikrintos būsenos ID
     * @return array
     */
    private function parse_status_result($status_result, $status_id)
    {
        $result = array();

        if ($status_result !== false) {
            if (is_array($status_result)) {
                foreach ($status_result as $arr) {
                    // Nurodytas dokumento įrašas ir jo būsena
                    if (is_array($arr)) {
                        $result[] = $arr;
                    }
                    // Nurodytas tik dokumeonto įrašas
                    else {
                        $result[] = array(
                            'STATUS' => $status_id,
                            'RECORD' => $arr
                        );
                    }
                }
            } else {
                $result[] = array(
                    'STATUS' => $status_id,
                    'RECORD' => $status_result
                );
            }
        }

        return $result;
    }

    /**
     *
     * @param null|int $status_id [optional]<p>Dokumento statuso ID</p>
     * @return array
     */
    public function CheckStatus($status_id = null, &$iteration_log = array())
    {
        //<--debug-->
        $this->put_contents('START OF PROCESS' . PHP_EOL . PHP_EOL . PHP_EOL);

        if (isset($iteration_log[(int) $status_id])) {
            $iteration_log[(int) $status_id] ++;
        } else {
            $iteration_log[(int) $status_id] = 1;
        }

        if (in_array(2, $iteration_log, true) === true) {
            trigger_error('Fatal error: Recursive iteration terminated', E_USER_WARNING);
            return;
        }
        // Ieško kokios galimos sekančios dokumento būklės
        $param = array(
            'STATUS_ID' => $status_id === null ? $this->record->doc_status : $status_id,
            'DOCUMENT_ID' => $this->record->table_to_id
        );
        
        //<--debug-->
        $this->put_contents($param);


        if (empty($param['STATUS_ID']) === true && empty($this->previous_record) === false) {
            $param = array(
                'STATUS_ID' => $this->previous_record->doc_status,
                'DOCUMENT_ID' => $this->previous_record->table_to_id
            );
        }
        // dabartinio dokumento būklė
        $order = $this->core->logic2->get_db(PREFIX . 'ATP_STATUS_ORDER', $param, true, null, false, 'A.ID, A.NR, A.STATUS_ID');
        if (empty($order)) {
            $order['NR'] = 0;
            //if ($param['DOCUMENT_ID'] == \Atp\Document::DOCUMENT_SUMMONS) { $order['NR'] = 70; } //Registruojamas "Naujas šaukimas", kurio nera 'ATP_STATUS_ORDER' lentelėje, todėl 'pakišam' jo tvarkos numerį.
            //if ($param['DOCUMENT_ID'] == \Atp\Document::DOCUMENT_REPORT) { $order['NR'] = 40; } //Registruojamas "Naujas praešimas", kurio nera 'ATP_STATUS_ORDER' lentelėje, todėl 'pakišam' jo tvarkos numerį.
        }
        //echo '<pre>'; var_dump($order); echo '</pre><hr>';
        //<--debug-->
        $this->put_contents($order);



        $old_param = $param;
        $param = array(
            'PARENT_NR' => $order['NR']
        );
        // Sekančios galimos būklės
        $next_statuses = $this->core->logic2->get_db(PREFIX . 'ATP_STATUS_ORDER', $param, false, null, false, 'A.ID, A.NR, A.STATUS_ID');
        // Aukštesnio prioriteto esamo dokumento būklės
        $doc_statuses_temp = $this->core->logic2->get_db(PREFIX . 'ATP_STATUS_ORDER', array(
            0 => '`NR` > ' . (int) $order['NR'],
            'DOCUMENT_ID' => $old_param['DOCUMENT_ID']), false, null, false, 'A.ID, A.NR, A.STATUS_ID');
        // Sujungiamos būsenos perrašant tuos pačius ryšius ir tas pačias būkles
        $statuses_temp = array();
        foreach ($doc_statuses_temp as &$doc_status) {
            if (isset($statuses_temp[$doc_status['STATUS_ID']])) {
                continue;
            }
            $statuses_temp[$doc_status['STATUS_ID']] = true;
            $next_statuses[$doc_status['ID']] = $doc_status;
        }

        unset($statuses_temp, $doc_statuses);

        // Būklės išrikiuojamos prioriteto didėjimo tvarka
        usort($next_statuses, function ($a, $b) {
            return (int) $a['NR'] > (int) $b['NR'];
        });

        if ($this->debug) {
            echo '<pre>';
            var_dump(array(
                '$this->record' => $this->record,
                '$this->previous_record' => isset($this->previous_record) ? $this->previous_record : null,
                '$old_param' => $old_param,
                '$order' => $order,
                '$param' => $param,
                '$next_statuses' => $next_statuses
            ));
            echo '</pre>';
        }

        //<--debug-->
        $this->put_contents(array(
            '$this->record' => $this->record,
            '$this->previous_record' => isset($this->previous_record) ? $this->previous_record : null,
            '$old_param' => $old_param,
            '$order' => $order,
            '$param' => $param,
            '$next_statuses' => $next_statuses
        ));


        // Tikrina sekančias būkles
        $res = false;
        $ress = array();
        foreach ($next_statuses as $next) {
            $method = 'status_' . $next['STATUS_ID'];
            if (method_exists($this, $method)) {
                $res = $this->$method();
                if ($this->debug) {
                    var_dump(array(
                        '$res[' . $method . ']' => $res
                    ));
                }
                if ($res === false) {
                    //break; // Nuėmiau, kad tikrintu visus galimus pirmo lygio išsišakojimus
                } //else {
                //$ress[$next['STATUS_ID']] = $res;
                //}

                if (empty($res) === false) {
                    $result2 = $this->checkStatus($next['STATUS_ID'], $iteration_log);
                    if (empty($result2) === false) {
                        return $result2;
                    }
                    break;
                }
                //break;
            }
        }

        //die('status');
        $result = array();
        if ($res !== false) {
            if (is_array($res)) {
                foreach ($res as $arr) {
                    // Nurodytas statusas su dokumento įrašu
                    if (is_array($arr)) {
                        $result[] = $arr;
                    }
                    // Nurodytas tik dokumeonto įrašas
                    else {
                        $result[] = array(
                            'STATUS' => $next['STATUS_ID'],
                            'RECORD' => $arr
                        );
                    }
                }
            } else {
                $result[] = array(
                    'STATUS' => $next['STATUS_ID'],
                    'RECORD' => $res
                );
            }
        }
        //<--debug-->
        $this->put_contents(PHP_EOL . PHP_EOL . 'END OF PROCESS' . PHP_EOL . PHP_EOL . PHP_EOL);
        return $result;
    }

    /**
     * "Naujas protokolas". Kai perduodama is Pazeidimo duomenui I Protokola
     */
    private function status_41()
    {
        if (
            in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_PROTOCOL])
            && (
                $this->record->table_from_id != \Atp\Document::DOCUMENT_PROTOCOL
                || empty($this->record->record_from_id) === true
            )) {
            return $this->record->id;
        }

        return false;
    }

    /**
     * "Naujas šaukimas". Kai sukuriamas dokumentas saukimas
     */
    private function status_42()
    {
//        // Dokumentas "Šaukimas"
//        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_SUMMONS/* , \Atp\Document::DOCUMENT_INFRACTION */]) === false) {
//            return false;
//        }

        // Kuriamas dokumentas "Šaukimas"
        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_SUMMONS/* , \Atp\Document::DOCUMENT_INFRACTION */])) {
//            $return = $this->record->id;
            //if (empty($this->previous_record) === FALSE)
            //	$return = array($return, array('STATUS' => 9, 'RECORD' => $this->previous_record->id));
//
//            return $return;
            return $this->record->id;
        }

        return false;
    }

    /**
     * "Naujas pranešimas". Kai sukuriamas dokumentas pranesimas
     */
    private function status_43()
    {
        //return false;
        // Dokumentas "Pranešimas"
        /* if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_INFRACTION])) {
          // Ieško ar yra sukurtas sekantis dokumentas "Pranešimas" arba "Lydraštis"
          $next_record = $this->core->logic2->get_record_relations(array('RECORD_FROM_ID' => $this->record->id, 'TABLE_TO_ID IN(\Atp\Document::DOCUMENT_REPORT,\Atp\Document::DOCUMENT_ACCOMPANYING)'), true, null, 'A.*');
          if (empty($next_record) === FALSE) {
          //echo 'Ieško ar yra sukurtas sekantis dokumentas "Pranešimas" arba "Lydraštis" - '. $this->record->id;
          }
          } else */
        // Kuriamas dokumentas "Pranešimas" arba "Lydraštis"
        /* if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_REPORT, \Atp\Document::DOCUMENT_ACCOMPANYING))) {
          // Tikrina ar buvęs dokumentas yra iš "Pažeidimo duomenys"
          if (in_array($this->record->table_from_id, [\Atp\Document::DOCUMENT_INFRACTION]) === TRUE) {
          $prev_record = $this->core->logic2->get_record_relations(array('ID' => $this->record->record_from_id), true, null, 'A.*');
          //echo 'Kuriamas dokumentas "Pranešimas" arba "Lydraštis" - '; print_r( array($prev_record['ID'], $this->record->id));
          }
          } */
//        $next_record = $this->core->logic2->get_record_relations([
//            'RECORD_FROM_ID' => $this->record->id,
//            'TABLE_TO_ID IN(' . \Atp\Document::DOCUMENT_REPORT . ',' . \Atp\Document::DOCUMENT_ACCOMPANYING . ')'], true, null, 'A.*');
//
//        //<--debug-->
//        $this->put_contents($next_record);
//
//
//        // echo '<pre> asd '; var_dump($next_record); echo'</pre> asd ';
//        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_REPORT/* , \Atp\Document::DOCUMENT_INFRACTION */)) === false) {
//            return false;
//        }

        // Kuriamas dokumentas "Pranešimas"
        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_REPORT/* , \Atp\Document::DOCUMENT_INFRACTION */])) {
            return [$this->record->id];
        }

        return false;
    }

    /**
     * "Užvesta byla" final. Pseudo statusas atitinkantis "status_20". Nutarimas - Byla - kai uzregistruoja kai išsiunčia pakartotini pranešimą
     */
    private function status_44()
    {
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false || in_array($this->record->doc_status, array(
                19, 20)) === false) {
            return false;
        }
        // Atidejimo laukas
        $qry = 'SELECT
                ID, NAME, LABEL
            FROM
                ' . PREFIX . 'ATP_FIELD
            WHERE
                ITYPE = \'CLASS\' AND
                ITEM_ID = 271 AND
                TYPE = 12'; //atideto nagrinejimo datos lauko isrinkimas
        $result = $this->core->db_query_fast($qry);
        if ($this->core->db_num_rows($result)) {
            $row = $this->core->db_next($result);

            // Tikrina lauko reikšmę
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $row['ID']);
            $value = trim($value);
            if (empty($value) === false && strtotime($value) > 0) {
                return $this->record->id;
            }
        }

        return false;
    }

    /**
     * "Nagrinėti" final. Pseudo statusas atitinkantis "status_19". Nutarimas - kai ateina nagrinėjimo laikas po antro prane6imo siuntimo
     */
    private function status_45()
    {
        if ($this->_cronjob === false) {
            return false;
        }
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false || in_array($this->record->doc_status, array(44)) === false) {
            return false;
        }
        
        // Atidejimo laukas
        $qry = 'SELECT
                ID, NAME, LABEL
            FROM
                ' . PREFIX . 'ATP_FIELD
            WHERE
                ITYPE = \'CLASS\' AND
                ITEM_ID = 271 AND
                TYPE = 12'; //atideto nagrinejimo datos lauko isrinkimas
        $result = $this->core->db_query_fast($qry);
        if ($this->core->db_num_rows($result)) {
            $row = $this->core->db_next($result);

            // Tikrina lauko reikšmę
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $row['ID']);

            $value = trim($value);
            if (empty($value) === false && strtotime($value) < time()) {
                return $this->record->id;
            }
        }

        return false;
    }

    /**
     * "Naujas". Kai sukuriamas naujas dokumentas iš SISP arba vartotojo sukurtas rankiniu būdu
     */
    private function status_2()
    {
        // Dokumentas "Pažeidimo duomenys" arba "SISP pažeidimo duomenys", "Nutarimas", "Pažeidimo duomenys mini"
        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_INFRACTION, \Atp\Document::DOCUMENT_INFRACTION_SISP, \Atp\Document::DOCUMENT_CASE, \Atp\Document::DOCUMENT_INFRACTION_MINI]) === false) {
            return false;
        }

        $boolen = empty($this->record->doc_status) || $this->record->doc_status === '2'; // Jokio statuso
        // Buves dokumentas SISP arba mini su statusu "Paskirtas tiriantis"
        if (empty($this->record->record_from_id) === false && in_array($this->record->table_from_id, [\Atp\Document::DOCUMENT_INFRACTION_SISP, \Atp\Document::DOCUMENT_INFRACTION_MINI])) {
            $previous_record = $this->core->logic2->get_record_relations(array('ID' => $this->record->record_from_id), true, null, 'A.*');
            //$boolen = $boolen || $this->previous_record->doc_status === '4';
            $boolen = $boolen || $previous_record['DOC_STATUS'] === '4';
        } else {
            // Buves dokumentas Pažeidimo duomenys su statusu "Tyrimas baigtas"
            if (empty($this->record->record_from_id) === false && (int) $this->record->table_from_id === \Atp\Document::DOCUMENT_INFRACTION) {
                $previous_record = $this->core->logic2->get_record_relations(array(
                    'ID' => $this->record->record_from_id), true, null, 'A.*');
                //$boolen = $boolen || $this->previous_record->doc_status === '4';
                $boolen = $boolen || $previous_record['DOC_STATUS'] === '4';
            }
        }

        $result = array();
        if ((int) $this->record->table_to_id === \Atp\Document::DOCUMENT_INFRACTION && in_array($this->record->table_from_id, [\Atp\Document::DOCUMENT_INFRACTION_SISP, \Atp\Document::DOCUMENT_INFRACTION_MINI])) {
            //$result[] = array('STATUS' => 4, 'RECORD_NR' => $this->previous_record->record_id);
            //$result[] = array('STATUS' => 4, 'RECORD_NR' => $previous_record['RECORD_ID']);
            $result[] = array('STATUS' => 4, 'RECORD' => $previous_record['ID']);
        }

        if ($boolen) {
            //return $this->record->record_id;
            //$result[] = $this->record->record_id;
            $result[] = $this->record->id;
        }
        if (count($result)) {
            return $result;
        }

        return false;
    }

    /**
     * "Paskirtas tiriantis".
     * SISP pažeidimų duomenys - kai įkeltas iš SISP DB ir perduodas į pažeidimo duomenis.
     * Pažeidimo duomenys mini - kai įkeltas į mini formą ir perduodas į pažeidimo duomenis.
     */
    private function status_4()
    {
        // SISP pažeidimo duomenys, Pažeidimo duomenys mini
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_INFRACTION_SISP, \Atp\Document::DOCUMENT_INFRACTION_MINI))) {
            // Ieško ar yra sukurtas sekantis dokumentas "Pažeidimo duomenyse"
            $next_record = $this->core->logic2->get_record_relations([
                'RECORD_FROM_ID' => $this->record->id,
                'TABLE_TO_ID' => \Atp\Document::DOCUMENT_INFRACTION], true, null, 'A.*');
            if (empty($next_record) === false) {
                return $this->record->id;
            }
        } else {
            // Pažeidimo duomenys
            if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_INFRACTION])) {
                if (in_array($this->record->table_from_id, array(\Atp\Document::DOCUMENT_INFRACTION_SISP, \Atp\Document::DOCUMENT_INFRACTION_MINI)) === true) {
                    $prev_record = $this->core->logic2->get_record_relations(array(
                        'ID' => $this->record->record_from_id), true, null, 'A.*');
                    return array($prev_record['ID'], array('STATUS' => 2, 'RECORD' => $this->record->id));
                }
            }
        }

        return false;
    }

    /**
     * "Nustatytas savininkas/naudotojas, jo tipas (Regitra)". Patikrinta webservis Regitra informacija ir gauti duomenys
     */
    private function status_6()
    {
        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_INFRACTION]) === false) {
            return false;
        }

        // Ieško web-serviso laukų
        $fields = $this->core->logic2->get_fields_all(array('ITEM_ID' => $this->record->table_to_id,
            'ITYPE' => 'DOC'));
        foreach ($fields as &$field) {
            if ($field['WS'] !== '1') {
                continue;
            }

            // Tikrina ar tai Regitros web-serviso laukas
            $ws = $this->core->logic2->get_webservices_fields(array('FIELD_ID' => $field['ID'],
                'WS_NAME' => 'RGT'));
            if (count($ws)) {
                foreach ($ws as $arr) {
                    // Tikrina ar yra bent vienas ne tuščias web-serviso laukas
                    $ws_fields = $this->core->logic2->get_webservices(array('PARENT_FIELD_ID' => $arr['FIELD_ID']));
                    foreach ($ws_fields as &$ws_field) {
                        $value = $this->core->factory->Record()->getFieldValue($this->record->id, $ws_field['FIELD_ID']);
                        if (empty($value) === false) {
                            return $this->record->id;
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * "Surašytas pranešimas (juridinis)". Iš dokumento sukurtas dokumentas - Pranešimas
     */
    private function status_7()
    {
        /* //return false;
          // Kuriamas dokumentas "Pažeidimo duomenys"
          if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_REPORT))) {
          // Ieško ar yra sukurtas sekantis dokumentas "Pranešimas" arba "Lydraštis"
          return $this->record->id;
          } else
          // Kuriamas dokumentas "Pranešimas" arba "Lydraštis"
          if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_INFRACTION, \Atp\Document::DOCUMENT_ACCOMPANYING])) {
          // Tikrina ar buvęs dokumentas yra iš "Pažeidimo duomenys"
          if (in_array($this->record->table_from_id, [\Atp\Document::DOCUMENT_INFRACTION]) === TRUE) {
          $prev_record = $this->core->logic2->get_record_relations(array('ID' => $this->record->record_from_id), true, null, 'A.*');
          return array($prev_record['ID'], $this->record->id);
          }
          }

          return false; */
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_REPORT)) === false) {
            return false;
        }

        // Registracijos numerio laukas
        $documentOptions = $this->core->factory->Document()->getOptions($this->record->table_to_id);
        if(empty($documentOptions->avilys_number) === false) {
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->avilys_number);
            $value = trim($value);
            if (empty($value) === false) {
                return $this->record->id;
            }
        }

        return false;
    }

    /**
     * "Nustatytas subjektas". Patikrinta webservis GYVREG, JAR informacija ir gauti duomenys, pažymėta varnelė kad "Subjektas nustatytas"
     */
    private function status_8()
    {
        if (in_array($this->record->table_to_id, [
                \Atp\Document::DOCUMENT_INFRACTION
            ]) === false) {
            return false;
        }

        // Ieško web-serviso laukų
        $fields = $this->core->logic2->get_fields_all([
            'ITEM_ID' => $this->record->table_to_id,
            'ITYPE' => 'DOC'
        ]);
        foreach ($fields as &$field) {
            if ($field['WS'] !== '1') {
                continue;
            }
            // Tikrina ar tai GRT web-serviso laukas
            $ws = $this->core->logic2->get_webservices_fields([
                'FIELD_ID' => $field['ID'],
                'WS_NAME' => 'GRT'
            ]);
            if (!count($ws)) {
                continue;
            }
            
            foreach ($ws as $arr) {
                // Tikrina ar yra bent vienas ne tuščias web-serviso laukas
                $ws_fields = $this->core->logic2->get_webservices([
                    'PARENT_FIELD_ID' => $arr['FIELD_ID']
                ]);
                foreach ($ws_fields as &$ws_field) {
                    $value = $this->core->factory->Record()
                        ->getFieldValue(
                            $this->record->id,
                            $ws_field['FIELD_ID']
                    );
                    if (empty($value) === false) {
                        return $this->record->id;
                    }
                }
            }
        }

        return false;
    }

    /**
     * "Nustatyta ar buvo baustas". Patikrintas baustumas (patikrinta su senais ATP duomenimis ir gauti duomenys)
     */
    private function status_9()
    {
        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_INFRACTION]) === false) {
            return false;
        }

        $punishment = new \Atp\Document\Record\Punishment($this->core);
        $result = $punishment->getCheckStatus($this->record->id);
        if ($result === true) {
            return $this->record->id;
        }

        return false;
    }

    /**
     * "Surašytas šaukimas (fizinis)". Iš dokumento sukurtas dokumentas - Šaukimas
     */
    private function status_10()
    {
        /* //return false;
          // Kuriamas dokumentas "Pažeidimo duomenys"
          if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_SUMMONS])) {
          // Ieško ar yra sukurtas sekantis dokumentas "Šaukimas" arba "Lydraštis"
          //$next_record = $this->core->logic2->get_record_relations(array('RECORD_FROM_ID' => $this->record->id, 'TABLE_TO_ID IN('.\Atp\Document::DOCUMENT_SUMMONS.','.\Atp\Document::DOCUMENT_ACCOMPANYING.')'), true, null, 'A.*');
          //if (empty($next_record) === FALSE) {
          return $this->record->id;
          //}
          } else
          // Kuriamas dokumentas "Šaukimas" arba "Lydraštis"
          if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_ACCOMPANYING,\Atp\Document::DOCUMENT_INFRACTION])) {
          // Tikrina ar buvęs dokumentas yra iš "Pažeidimo duomenys"
          if (in_array($this->record->table_from_id, [\Atp\Document::DOCUMENT_INFRACTION]) === TRUE) {
          $prev_record = $this->core->logic2->get_record_relations(array('ID' => $this->record->record_from_id), true, null, 'A.*');
          return array($prev_record['ID'], $this->record->id);
          }
          }

          return false; */
        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_SUMMONS]) === false) {
            return false;
        }

        // Registracijos numerio laukas
        $documentOptions = $this->core->factory->Document()->getOptions($this->record->table_to_id);
        if(empty($documentOptions->avilys_number) === false) {
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->avilys_number);
            $value = trim($value);
            if (empty($value) === false) {
                return $this->record->id;
            }
        }

        return false;
    }

    /**
     * "Tyrimas baigtas". Pažeidimo duomenys - kai dokumentas perduotas į Protokolą
     */
    private function status_38()
    {
        // Pažeidimo duomenys
        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_INFRACTION])) {
            // Ieško ar yra sukurtas sekantis dokumentas "Pažeidimo duomenyse"
            $next_record = $this->core->logic2->get_record_relations([
                'RECORD_FROM_ID' => $this->record->id,
                'TABLE_TO_ID' => \Atp\Document::DOCUMENT_PROTOCOL], true, null, 'A.*');
            if (empty($next_record) === false) {
                return $this->record->id;
            }
        } else {
            // Protokolas // TODO: nebereikia? nes Protokole pradinė būseana "Naujas" (2) ir iš jo užregistravus avilyje keliauja į "Surašytas ATP protokolas" (13)
            if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_PROTOCOL])) {
                if (in_array($this->record->table_from_id, [\Atp\Document::DOCUMENT_INFRACTION]) === true) {
                    $prev_record = $this->core->logic2->get_record_relations(array(
                        'ID' => $this->record->record_from_id), true, null, 'A.*');
                    return array($prev_record['ID'], array('STATUS' => 2, 'RECORD' => $this->record->id));
                }
            }
        }

        return false;
    }

    /**
     * "Surašytas ATP protokolas". Protokolas - kai uzregistruoja I Avili ir gauna numeriuka.
     */
    private function status_13()
    {
        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_PROTOCOL]) === false) {
            return false;
        }

        // Registracijos numerio laukas
        $documentOptions = $this->core->factory->Document()->getOptions($this->record->table_to_id);
        if(empty($documentOptions->avilys_number) === false) {
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->avilys_number);
            $value = trim($value);
            if (empty($value) === false) {
                return $this->record->id;
            }
        }

        return false;
    }

    /**
     * "Išsiųstas ATP protokolas". Protokolas - kai sukuriamas Lydraštis?
     * Kai Avilyje užregistruojamas lydraščio dokumentas.
     */
    private function status_14()
    {
        // Kuriamas dokumentas "Protokolas" (Uzkomentuota - nereikalinga protokolui busena. R.K. Lauros nurodymu :) )
        /* if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_PROTOCOL])) {
          // Ieško ar yra sukurtas sekantis dokumentas "Lydraštis"
          $next_record = $this->core->logic2->get_record_relations(array('RECORD_FROM_ID' => $this->record->id, 'TABLE_TO_ID IN('.\Atp\Document::DOCUMENT_ACCOMPANYING.')'), true, null, 'A.*');
          if (empty($next_record) === FALSE) {
          return $this->record->id;
          }
          } else */
//        // Kuriamas dokumentas "Lydraštis"
//        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_ACCOMPANYING])) {
//            // Tikrina ar buvęs dokumentas yra iš "Protokolas"
//            if (in_array($this->record->table_from_id, [\Atp\Document::DOCUMENT_PROTOCOL]) === true) {
////                $prev_record = $this->core->logic2->get_record_relations(array('ID' => $this->record->record_from_id), true, null, 'A.*');
//                return $this->record->id;
//            }
//        }

        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_ACCOMPANYING]) === false) {
            return false;
        }

        $documentOptions = $this->core->factory->Document()->getOptions($this->record->table_to_id);
        if(empty($documentOptions->avilys_number) === false) {
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->avilys_number);
            $value = trim($value);
            if (empty($value) === false) {
                return $this->record->id;
            }
        }
        
        return false;
    }
    /**
     * "Kontroliuoti AN". Tarpinė būsena
     * DECISION MOVED TO OLD *_16 FUNCTION
     * @deprecated
     */
    /* private function status_15() {
      return false;
      if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_PROTOCOL]) === FALSE)
      return false;

      $result = $this->core->db_query_fast('SELECT STATUS
      FROM ' . PREFIX . 'ATP_RECORD
      WHERE
      ID = :ID', array('ID' => $this->record->id));
      if ($this->core->db_num_rows($result)) {
      $row = $this->core->db_next($result);
      if ((int) $row['STATUS'] === 6) {
      return $this->record->id;
      }
      }

      return false;
      } */

    /**
     * "Kontroliuoti AN". Tarpinė būsena( ex "Baigiasi AN įvykdymo terminas".) Tarpinė būsena, iki termino pabaigos liko 1 diena
     */
    private function status_15()
    {
        //return $this->record->id;
        //jeigu statusa tikrina ne crontabas - neleidziam uzdeti.
        if ($this->_cronjob === false) {
            return false;
        }
        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_PROTOCOL]) === false) {
            return false;
        }

        $document = $this->core->logic2->get_document(array('ID' => \Atp\Document::DOCUMENT_PROTOCOL, 'CAN_IDLE' => 1), true, null, 'ID, IDLE_DAYS');

        if (empty($document) === false) {
            $result = $this->core->logic->idle_period_end($this->record->id, time(), $document);
            if ($result === true) {
                return $this->record->id;
            }
        }

        return false;
    }

    /**
     * "AN įvykdytas". Protokolas - kai įvykdytas AN - priskirtas mokėjimas iš VMI patikrinus webservise
     */
    private function status_17()
    {
        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_PROTOCOL]) === false) {
            return false;
        }
        if (in_array($this->record->doc_status, array(13, 14, 15, 16)) === false) {
            return false;
        }

        // Jei bigta arba tarpinė stadija, ir nurodyta, kad apmokėta
        if (in_array((int) $this->record->status, array(\Atp\Record::STATUS_FINISHED, \Atp\Record::STATUS_IDLE), true) && (int) $this->record->is_last === 1) {
            $paymentManager = new \Atp\Document\Record\Payment($this->core);
            $payment_option = $paymentManager->GetRecordPaymentOption(new \Atp\Document\Record($this->record->id, $this->core));
            if (empty($payment_option) === false && $payment_option['ID'] === $paymentManager::STATUS_PAID) {
                return $this->record->id;
            }
        }

        // Pasibaigęs tarpinės stadijos terminas
        $result = $this->core->logic->idle_period_end($this->record->id, null, null, true);
        if ($result === true && (int) $this->record->is_paid === 1) {
            $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD
				SET STATUS = ' . \Atp\Record::STATUS_IDLE . '
				WHERE
					ID = :ID', array('ID' => $this->record->id));
            return $this->record->id;
        }


        return false;
    }

    /**
     * "AN neįvykdytas". Protokolas - kai neįvykdytas AN - nepriskirtas mokėjimas iš VMI patikrinus webservise.
     */
    private function status_18()
    {
        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_PROTOCOL]) === false) {
            return false;
        }
        if (in_array($this->record->doc_status, array(15, 16)) === false) {
            return false;
        }

        // Jei nagrinėjima arba tarpinė stadija, ir nurodyta, kad neapmokėta
        if (in_array((int) $this->record->status, array(\Atp\Record::STATUS_EXAMINING, \Atp\Record::STATUS_IDLE), true) && (int) $this->record->is_last === 1) {
            $paymentManager = new \Atp\Document\Record\Payment($this->core);
            $payment_option = $paymentManager->GetRecordPaymentOption(new \Atp\Document\Record($this->record->id, $this->core));
            if (empty($payment_option) === false && $payment_option['ID'] === $paymentManager::STATUS_NOT_PAID) {
                return $this->record->id;
            }
        }

        // Pasibaigęs tarpinės stadijos terminas
        $result = $this->core->logic->idle_period_end($this->record->id, null, null, true);
        if ($result === true && (int) $this->record->is_paid === 0) {
            $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD
				SET STATUS = ' . \Atp\Record::STATUS_IDLE . '
				WHERE10
					ID = :ID', array('ID' => $this->record->id));
            return $this->record->id;
        }

        return false;
    }
    /**
     * "Nagrinėti". Protokolas po tarpinės būsenos. Protokolas perduotas į Nutarimą
     */
    /* private function status_19() {
      // Protokolas
      if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_PROTOCOL])) {
      // Ieško ar yra sukurtas sekantis dokumentas "Nutarimas"
      $next_record = $this->core->logic2->get_record_relations(array('RECORD_FROM_ID' => $this->record->id, 'TABLE_TO_ID' => \Atp\Document::DOCUMENT_CASE), true, null, 'A.*');
      if (empty($next_record) === FALSE) {
      return $this->record->record_id;
      }
      } else
      // Nutarimas
      if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE))) {
      if (in_array($this->record->table_from_id, [\Atp\Document::DOCUMENT_PROTOCOL]) === TRUE) {
      $prev_record = $this->core->logic2->get_record_relations(array('ID' => $this->record->record_from_id), true, null, 'A.*');
      return array($prev_record['RECORD_ID'], $this->record->record_id);
      }
      }

      return false;
      } */

    /**
     * "Nagrinėti". Nutarimas - kai ateina nagrinėjimo laikas
     */
    private function status_19()
    {
        if ($this->_cronjob === false) {
            return false;
        }
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false || (int) $this->record->doc_status !== 20) {
            return false;
        }

        // Nagrinėjimo data
        $documentOptions = $this->core->factory->Document()->getOptions($this->record->table_to_id);
        if(empty($documentOptions->examinator_date) === false) {
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->examinator_date);
            if (isset($value)) {
                $exam_timestamp = strtotime($value);
                if ($exam_timestamp < time()) {
                    return $this->record->id;
                }
            }
        }

        return false;
    }

    /**
     * "Užvesta byla". Nutarimas - Byla - kai uzregistruoja I Avili ir gauna numeriuka.
     */
    private function status_20()
    {
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false) {
            return false;
        }

        // Registracijos numerio laukas
        $documentOptions = $this->core->factory->Document()->getOptions($this->record->table_to_id);
        if(empty($documentOptions->avilys_number) === false) {
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->avilys_number);
            $value = trim($value);
            if (empty($value) === false) {
                return $this->record->id;
            }
        }

        return false;
    }

    /**
     * "Išsiųstas pranešimas atvykti".<br/>
     * <strike>Nutarimas - kai sukurtas dokumentas - Pranešimas</strike><br/>
     * Pranešimas - kai užregistruotas pranešimas.
     */
    private function status_21()
    {
//        //pazeidimo tipas gauti protokolai
//        // Kuriamas dokumentas "Nutarimas"
//        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE))) {
//            // Ieško ar yra sukurtas sekantis dokumentas "Pranešimas" arba "Lydraštis"
//            $next_record = $this->core->logic2->get_record_relations([
//                'RECORD_FROM_ID' => $this->record->id,
//                'TABLE_TO_ID IN(' . \Atp\Document::DOCUMENT_REPORT . ','.\Atp\Document::DOCUMENT_ACCOMPANYING.')'
//                ], true, null, 'A.ID');
//            if (empty($next_record) === false) {
//                return $this->record->id;
//            }
//        } else {
//            // Kuriamas dokumentas "Pranešimas" arba "Lydraštis"
//            if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_REPORT, \Atp\Document::DOCUMENT_ACCOMPANYING])) {
//                // Tikrina ar buvęs dokumentas yra iš "Nutarimas"
//                if (in_array($this->record->table_from_id, array(\Atp\Document::DOCUMENT_CASE)) === true) {
//                    $prev_record = $this->core->logic2->get_record_relation($this->record->record_from_id);
//                    return array($prev_record['ID'], $this->record->id);
//                }
//            }
//        }
//
//        return false;


        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_REPORT]) === false) {
            return false;
        }

        if (!$this->core->factory->Record()->getPreviousDocumentFor($this->record->id, \Atp\Document::DOCUMENT_CASE)) {
            return false;
        }

        $documentOptions = $this->core->factory->Document()->getOptions($this->record->table_to_id);
        if(empty($documentOptions->avilys_number) === false) {
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->avilys_number);
            $value = trim($value);
            if (empty($value) === false) {
                return $this->record->id;
            }
        }

        return false;
    }

    /**
     * "Išsiųstas pakartotinis pranešimas atvykti".<br/>
     * <strike>Nutarimas - kai sukurtas dokumentas - Pranešimas (kelyje turi būti jau vieną kartą sukurtas dokumentas Pranešimas) </strike><br/>
     * Pranešimas - kai kelyje antrą kartą užregistruotas pranešimas.
     */
    private function status_22()
    {
//        //pazeidimo tipas ne "gauti protokolai"
//        // Kuriamas dokumentas "Nutarimas"
//        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE))) {
//            // Ieško ar yra sukurti sekantys dokumentai "Pranešimas" arba "Lydraštis"s
//            $next_records = $this->core->logic2->get_record_relations(array('RECORD_ID' => $this->record->record_id,
//                'TABLE_TO_ID IN(' . \Atp\Document::DOCUMENT_REPORT . ','.\Atp\Document::DOCUMENT_ACCOMPANYING.')'), false, null, 'A.ID, A.TABLE_TO_ID');
//
//            if (count($next_records) > 1) {
//                // Tikrina ar bent vieno dokumento yra po du įrašus
//                $count = array();
//                foreach ($next_records as $record) {
//                    $count[$record['TABLE_TO_ID']] ++;
//                    if ($count[$record['TABLE_TO_ID']] >= 2) {
//                        return $this->record->id;
//                    }
//                }
//            }
//        } else {
//            // Kuriamas dokumentas "Pranešimas" arba "Lydraštis"
//            if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_REPORT, \Atp\Document::DOCUMENT_ACCOMPANYING))) {
//                // Tikrina ar buvęs dokumentas yra iš "Nutarimas"
//                if (in_array($this->record->table_from_id, array(\Atp\Document::DOCUMENT_CASE)) === true) {
//                    // Ieško ar yra buvusiame dokumente sukurti sekantys dokumentai "Pranešimas" arba "Lydraštis"s
//                    $prev_record = $this->core->logic2->get_record_relation($this->record->record_from_id);
//                    $other_records = $this->core->logic2->get_record_relations(array(
//                        'RECORD_FROM_ID' => $this->record->record_from_id, 'TABLE_FROM_ID' => \Atp\Document::DOCUMENT_CASE), true, null, 'A.*');
//                    if (count($other_records) > 1) {
//                        // Tikrina ar bent vieno dokumento yra po du įrašus
//                        $count = array();
//                        foreach ($other_records as $record) {
//                            $count[$record['TABLE_TO_ID']] ++;
//                            if ($count[$record['TABLE_TO_ID']] >= 2) {
//                                return array($prev_record['ID'], $this->record->id);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        return false;

        if ($this->status_21() !== false) {
            $recordHelper = $this->core->factory->Record();
            $caseRecord = $recordHelper->getPreviousDocumentFor(
                $this->record->id,
                \Atp\Document::DOCUMENT_CASE
            );
            if ($caseRecord) {
                $childDocuments = $this->core->logic2->get_record_relations([
                    'RECORD_FROM_ID' => $caseRecord->getId(),
                    'TABLE_TO_ID' => \Atp\Document::DOCUMENT_REPORT], false, null, 'A.*');
                if (count($childDocuments) >= 2) {
                    $result = array_map(function($record) { return $record['ID']; }, $childDocuments);
                    return $result;
                }
            }
        }

        return false;
    }

    /**
     * "Priimtas nutarimas". Nutarimas - kai užpildytas laukas "Nutarimo priėmimo data"
     * Update: po perdavimo į ATPEIR
     */
    private function status_29()
    {
//        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_CASE]) === false || in_array($this->record->doc_status, [19, 45]) === false) {
//            return false;
//        }
        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_CASE]) === false || in_array($this->record->doc_status, [64]) === false) {
            return false;
        }

        // Nutarimo priėmimo laukas
        $documentOptions = $this->core->factory->Document()->getOptions($this->record->table_to_id);
        if(empty($documentOptions->decision_date) === false) {
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->decision_date);
            $value = trim($value);
            if (empty($value) === false && strtotime($value) > 0) {
                return $this->record->id;
            }
        }

        return false;
    }

    /**
     * "Tikrinti sumokėjima". Nutarimas - atsiranda būsena ta diena, kai sukapsi terminas per kiek laiko turi apmoketi
     */
    private function status_32()
    {
        if (!in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_CASE]) || !in_array($this->record->doc_status, [47,
                48, 52, 58])) {
            return false;
        }

        if ($this->_cronjob === false) {
            return false;
        }

        if (in_array($this->record->doc_status, [47, 48]) === true) {
            // Nutarimo priėmimo laukas
            $documentOptions = $this->core->factory->Document()->getOptions($this->record->table_to_id);
            if(empty($documentOptions->decision_date) === false) {
                $value = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->decision_date);
                $value = trim($value);
                if (empty($value) === false && strtotime($value . ' + 40 day') < time()) {
                    return $this->record->id;
                }
            }
        } elseif (in_array($this->record->doc_status, array(/* 48, */52, 58)) === true) {
            $document = $this->core->logic2->get_document(array('ID' => \Atp\Document::DOCUMENT_CASE));
            $fields_arr = array('APPEAL_CCD_DATE');
            foreach ($fields_arr as $field_name) {
                $optionName = strtolower($field_name);
                $field_id = $this->core->factory->Document()->getOptions($document['ID'])->$optionName;
                $field_values[$field_name] = $this->core->factory->Record()->getFieldValue($this->record->id, $field_id);
            }
            if (empty($field_values[$fields_arr[0]]) === false && strtotime($field_values[$fields_arr[0]] . ' + 40 day') < time()) {
                return $this->record->id;
            }
        }
    }

    /**
     * "Sumokėta". Priskirtas mokėjimas iš VMI, patikrinus webservise
     */
    private function status_33()
    {
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false || in_array($this->record->doc_status, array(
                32)) === false) {
            return false;
        }

        // Baigta arba tarpinė stadija, ir nurodyta, kad apmokėta
        if (in_array((int) $this->record->status, array(\Atp\Record::STATUS_FINISHED, \Atp\Record::STATUS_IDLE), true) && (int) $this->record->is_last === 1) {
            $paymentManager = new \Atp\Document\Record\Payment($this->core);
            $payment_option = $paymentManager->GetRecordPaymentOption(new \Atp\Document\Record($this->record->id, $this->core));
            if (empty($payment_option) === false && $payment_option['ID'] === $paymentManager::STATUS_PAID) {
                return $this->record->id;
            }
        }
        // Pasibaigęs tarpinės stadijos terminas
        $result = $this->core->logic->idle_period_end($this->record->id, null, null, true);
        if ($result === true && (int) $this->record->is_paid === 1) {
            $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD
					SET STATUS = '.\Atp\Record::STATUS_IDLE.'
				WHERE
					ID = :ID', array('ID' => $this->record->id));
            return $this->record->id;
        }


        return false;
    }

    /**
     * "Nesumokėta". Priskirtas mokėjimas iš VMI, patikrinus webservise
     */
    private function status_39()
    {
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false || in_array($this->record->doc_status, array(
                32)) === false) {
            return false;
        }

        // Nagrinėjama arba tarpinė stadija, ir nurodyta, kad neapmokėta
        if (in_array((int) $this->record->status, array(\Atp\Record::STATUS_EXAMINING, \Atp\Record::STATUS_IDLE), true) && (int) $this->record->is_last === 1) {
            $paymentManager = new \Atp\Document\Record\Payment($this->core);
            $payment_option = $paymentManager->GetRecordPaymentOption(new \Atp\Document\Record($this->record->id, $this->core));
            if (empty($payment_option) === false && $payment_option['ID'] === $paymentManager::STATUS_NOT_PAID) {
                return $this->record->id;
            }
        }
        // Pasibaigęs tarpinės stadijos terminas
        $result = $this->core->logic->idle_period_end($this->record->id, null, null, true);
        if ($result === true && (int) $this->record->is_paid === 0) {
            $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD
					SET STATUS = '.\Atp\Record::STATUS_IDLE.'
				WHERE
					ID = :ID', array('ID' => $this->record->id));
            return $this->record->id;
        }

        return false;
    }

    /**
     * "Apskųsta". Nutarimas - jei nutarimo formoje yra įvesta apskundimo data
     */
    private function status_40()
    {
        /* if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === FALSE)
          return false;

          $result = $this->core->db_query_fast('SELECT APPEALED FROM ' . PREFIX . 'ATP_RECORD
          WHERE
          RECORD_ID = :RECORD_NR AND
          IS_LAST = 1', array('RECORD_NR' => $this->record->record_id));
          if ($this->core->db_num_rows($result)) {
          $row = $this->core->db_next($result);
          if ((int) $row['APPEALED'] === 1) {
          return $this->record->record_id;
          }
          }

          return false; */

        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false || in_array($this->record->doc_status, array(
                29)) === false) {
            return false;
        }

        // Apskundimo laukas
        $documentOptions = $this->core->factory->Document()->getOptions($this->record->table_to_id);
        if(empty($documentOptions->appealed_date) === false) {
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->appealed_date);
            $value = trim($value);
            if (empty($value) === false && strtotime($value) > 0) {
                return $this->record->id;
            }
        }

        return false;
    }

    /**
     * "Byla nutraukta" (VMSA).
     */
    private function status_46()
    {
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false || in_array($this->record->doc_status, array(
                29, 40)) === false /* && in_array($this->record->table_from_id, array(\Atp\Document::DOCUMENT_CASE)) === FALSE */) {
            return false;
        }
        //nutarimo duomenys > sprendimas > nutraukti byla
        $qry = 'SELECT ID FROM ' . PREFIX . 'ATP_FIELD where ITYPE = "DOC" AND SOURCE = 92 AND ITEM_ID = ' . \Atp\Document::DOCUMENT_CASE . ' LIMIT 1';
        $result = $this->core->db_query_fast($qry, array('DOCUMENT_ID' => $this->record->table_to_id));
        if ($this->core->db_num_rows($result)) {
            $row = $this->core->db_next($result);

            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $row['ID']);

            $value = trim($value);
            if (empty($value) === false && (int) $value === 183) {
                return $this->record->id;
            }
        }
        return false;
    }

    /**
     * "Įsiteisėjęs nutarimas" po 20 dienų(crontabu)
     */
    private function status_47()
    {
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false || in_array($this->record->doc_status, array(
                29)) === false /* && in_array($this->record->table_from_id, array(\Atp\Document::DOCUMENT_CASE)) === FALSE */) {
            return false;
        }
        //ivesta nutarimo priemimo data + 20d
        if ($this->_cronjob === false) {
            return false;
        }

        // Nutarimo priėmimo laukas
        if(empty($documentOptions->decision_date) === false) {
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->decision_date);
            $value = trim($value);
            if (empty($value) === false && strtotime($value . ' + 20 day') < time()) {
                return $this->record->id;
            }
        }

        return false;
    }

    /**
     * "Nutarimas išnagrinėtoje byloje"
     */
    private function status_48()
    {
        //yra apskusta(40 status) ir ivesta nutarimo data
        if ((in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false && in_array($this->previous_record->table_to_id, array(
                \Atp\Document::DOCUMENT_CASE)) === false) || in_array($this->record->doc_status, array(40)) === false /* && in_array($this->record->table_from_id, array(\Atp\Document::DOCUMENT_CASE)) === FALSE */) {
            return false;
        }

        if(empty($documentOptions->decision_date) === false) {
            $value_old = $this->core->factory->Record()->getFieldValue($this->previous_record->id, $documentOptions->decision_date);
            $value_old = trim($value_old);

            $value_new = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->decision_date);
            $value_new = trim($value_new);

            if ((empty($value_old) === false || empty($value_new) === false) && strtotime($value_new) > strtotime($value_old)) {
                return $this->record->id;
            }
        }


        return false;
    }

    /**
     * "Išsiųsta teismui" po apskundimo
     */
    private function status_49()
    {
        //yra apskusta (40 status) ir ivesta data "issiusta teismui"
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false || in_array($this->record->doc_status, array(
                40)) === false /* && in_array($this->record->table_from_id, array(\Atp\Document::DOCUMENT_CASE)) === FALSE */) {
            return false;
        }
        
        //Issiusta teismui datos laukas
        $document = $this->core->logic2->get_document(array('ID' => \Atp\Document::DOCUMENT_CASE));
        $fields_arr = array('APPEAL_STC_DATE');
        foreach ($fields_arr as $field_name) {
            $optionName = strtolower($field_name);
            $field_id = $this->core->factory->Document()->getOptions($document['ID'])->$optionName;
            $field_values[$field_name] = $this->core->factory->Record()->getFieldValue($this->record->id, $field_id);
        }
        if (empty($field_values[$fields_arr[0]]) === false && strtotime($field_values[$fields_arr[0]]) > 0) {
            return $this->record->id;
        }

        return false;
    }

    /**
     * "Rengiamas atsiliepimas"
     */
    private function status_50()
    {
        //naujas dokumentas "atsiliepimas" ir tevo busena status 49
        $next_record = $this->core->logic2->get_record_relations(array('RECORD_FROM_ID' => $this->record->id,
            'TABLE_TO_ID IN('.\Atp\Document::DOCUMENT_RESPONSE_AK.')'), true, null, 'A.*');

        //<--debug-->
        $this->put_contents($next_record);

        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_RESPONSE_AK/* , \Atp\Document::DOCUMENT_INFRACTION */]) === false || in_array($this->previous_record->doc_status, array(
                49)) === false) {
            return false;
        }

        // Kuriamas dokumentas "Pranešimas"
        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_RESPONSE_AK/* , \Atp\Document::DOCUMENT_INFRACTION */])) {
            return array($this->record->id);
        }


        return false;
    }

    /**
     * "Parengtas atsiliepimas"
     */
    private function status_24()
    {
        //tures "dok nr" ir "data"
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_RESPONSE_AK)) === false || in_array($this->record->doc_status, array(
                50)) === false) {
            return false;
        }

        // Registracijos numerio laukas
        $documentOptions = $this->core->factory->Document()->getOptions($this->record->table_to_id);
        if(empty($documentOptions->avilys_number) === false) {
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->avilys_number);
            $value = trim($value);
            if (empty($value) === false) {
                return $this->record->id;
            }
        }

        return false;
    }

    /**
     * "Rengiamas atsiliepimas į apeliacinį skundą"
     */
    private function status_51()
    {
        //naujas dokumentas "atsiliepimas" ir tevo busena status 62
        $next_record = $this->core->logic2->get_record_relations(array('RECORD_FROM_ID' => $this->record->id,
            'TABLE_TO_ID IN('.\Atp\Document::DOCUMENT_RESPONSE_AK.')'), true, null, 'A.*');

        //<--debug-->
        $this->put_contents($next_record);

        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_RESPONSE_AK/* , \Atp\Document::DOCUMENT_INFRACTION */)) === false || in_array($this->previous_record->doc_status, array(
                62)) === false) {
            return false;
        }

        // Kuriamas dokumentas "Pranešimas"
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_RESPONSE_AK/* , \Atp\Document::DOCUMENT_INFRACTION */))) {
            return array($this->record->id);
        }

        return false;
    }

    /**
     * "Rengiamas atsiliepimas į apeliacinį skundą"
     */
    private function status_26()
    {
        //tures "dok nr" ir "data"
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_RESPONSE_AK)) === false || in_array($this->record->doc_status, array(
                51)) === false) {
            return false;
        }

        // Registracijos numerio laukas
        $documentOptions = $this->core->factory->Document()->getOptions($this->record->table_to_id);
        if(empty($documentOptions->avilys_number) === false) {
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->avilys_number);
            $value = trim($value);
            if (empty($value) === false) {
                return $this->record->id;
            }
        }

        return false;
    }

    /**
     * "Įsiteisėjęs nutarimas" Teismo, po apskundimo
     */
    private function status_52()
    {
        //kai ateina Apskundimo duomenys > "Sprendimo įsiteisėjimo data"
        if ((in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false && in_array($this->previous_record->table_to_id, array(
                \Atp\Document::DOCUMENT_CASE)) === false) || in_array($this->record->doc_status, array(49)) === false /* && in_array($this->record->table_from_id, array(\Atp\Document::DOCUMENT_CASE)) === FALSE */) {
            return false;
        }
        //tikrinam selecta apskundimo nagrinejimo
        $qry = 'SELECT ID, NAME FROM ' . PREFIX . 'ATP_FIELD where ITYPE = \'DOC\' AND SOURCE = 333 AND ITEM_ID = "' . \Atp\Document::DOCUMENT_CASE . '" LIMIT 1';
        $result = $this->core->db_query_fast($qry, array('DOCUMENT_ID' => $this->record->table_to_id));
        if ($this->core->db_num_rows($result)) {
            $row = $this->core->db_next($result);
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $row['ID']);

            $value = trim($value);
            if (empty($value) === false && (int) $value !== 335) {
                $document = $this->core->logic2->get_document(array('ID' => \Atp\Document::DOCUMENT_CASE));
                $fields_arr = array('APPEAL_CCD_DATE');
                foreach ($fields_arr as $field_name) {
                    $optionName = strtolower($field_name);
                    $field_id = $this->core->factory->Document()->getOptions($document['ID'])->$optionName;
                    $field_values[$field_name] = $this->core->factory->Record()->getFieldValue($this->record->id, $field_id);
                }
                if (empty($field_values[$fields_arr[0]]) === false && strtotime($field_values[$fields_arr[0]]) > 0) {
                    return $this->record->id;
                }
            }
        }
        //Sprendimo isiteisejimo datos laukas,271

        return false;
    }

    /**
     * "Byla nutraukta" Teismo sprendimu
     */
    private function status_53()
    {
        if ((in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false && in_array($this->previous_record->table_to_id, array(
                \Atp\Document::DOCUMENT_CASE)) === false) || in_array($this->record->doc_status, array(49)) === false /* && in_array($this->record->table_from_id, array(\Atp\Document::DOCUMENT_CASE)) === FALSE */) {
            return false;
        }
        //status_49 + nutarimo duomenys > sprendimas > nutraukti byla
        $qry = 'SELECT ID, NAME FROM ' . PREFIX . 'ATP_FIELD where ITYPE = \'DOC\' AND SOURCE = 333 AND ITEM_ID = "' . \Atp\Document::DOCUMENT_CASE . '" LIMIT 1';
        $result = $this->core->db_query_fast($qry, array('DOCUMENT_ID' => $this->record->table_to_id));
        if ($this->core->db_num_rows($result)) {
            $row = $this->core->db_next($result);
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $row['ID']);

            $value = trim($value);
            if (empty($value) === false && (int) $value === 335) {
                $document = $this->core->logic2->get_document(array('ID' => \Atp\Document::DOCUMENT_CASE));
                $fields_arr = array('APPEAL_CCD_DATE');
                foreach ($fields_arr as $field_name) {
                    $optionName = strtolower($field_name);
                    $field_id = $this->core->factory->Document()->getOptions($document['ID'])->$optionName;
                    $field_values[$field_name] = $this->core->factory->Record()->getFieldValue($this->record->id, $field_id);
                }
                if (empty($field_values[$fields_arr[0]]) === false && strtotime($field_values[$fields_arr[0]]) > 0) {
                    return $this->record->id;
                }
            }
        }
        return false;
    }

    /**
     * "Byla apskųsta" Antrą kartą, po teismo nutarimo
     */
    private function status_54()
    {
        if ((in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false && in_array($this->previous_record->table_to_id, array(
                \Atp\Document::DOCUMENT_CASE)) === false) || in_array($this->record->doc_status, array(52,
                53)) === false) {
            return false;
        }
        //status 53  + apskundimo duomenys > apskunde > ne surasius institucija + dabartine apskundimo data didesne uz buvusia
        if(empty($documentOptions->appealed_date) === false) {
            $value_old = $this->core->factory->Record()->getFieldValue($this->previous_record->id, $documentOptions->appealed_date);
            $value_old = trim($value_old);

            $value_new = $this->core->factory->Record()->getFieldValue($this->record->id, $documentOptions->appealed_date);
            $value_new = trim($value_new);

            if ((empty($value_old) === false || empty($value_new) === false) && strtotime($value_new) > strtotime($value_old)) {
                return $this->record->id;
            }
        }

        return false;
    }
    /**
     * "Byla apskųsta" Antra kartą - skundžia VMSA.
     */
    /* private function status_55() {
      //status 53  + apskundimo duomenys > apskunde > surasius institucija + dabartine apskundimo data didesne uz buvusia
      return false;
      } */

    /**
     * "Rengiamas apeliacinis skundas"
     */
    private function status_56()
    {
        //naujas dokumentas "skundas" ir tevo busena status 52 arba 53
        return false;
    }

    /**
     * "Pateiktas apeliacinis skundas"
     */
    private function status_25()
    {
        //tures "dok nr" ir "data"



        return false;
    }
    /**
     * "Išsiųsta byla" antrą kartą, VMSA apeliacija
     */
    /* private function status_57() {
      return false;
      }
     */

    /**
     * "Įsiteisėjęs nutarimas" apeliacinio teismo
     */
    private function status_58()
    {
        //status 62  + dabartine isiteisejimo data didesne uz buvusia
        if ((in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false && in_array($this->previous_record->table_to_id, array(
                \Atp\Document::DOCUMENT_CASE)) === false) || in_array($this->record->doc_status, array(62)) === false /* && in_array($this->record->table_from_id, array(\Atp\Document::DOCUMENT_CASE)) === FALSE */) {
            return false;
        }


        $qry = 'SELECT ID, NAME FROM ' . PREFIX . 'ATP_FIELD where ITYPE = \'DOC\' AND SOURCE = 333 AND ITEM_ID = "' . \Atp\Document::DOCUMENT_CASE . '" LIMIT 1';
        $result = $this->core->db_query_fast($qry, array('DOCUMENT_ID' => $this->record->table_to_id));
        if ($this->core->db_num_rows($result)) {
            $row = $this->core->db_next($result);
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $row['ID']);

            $value = trim($value);
            if (empty($value) === false && (int) $value !== 335) {
                $document = $this->core->logic2->get_document(array('ID' => \Atp\Document::DOCUMENT_CASE));
                $fields_arr = array('APPEAL_CCD_DATE');
                foreach ($fields_arr as $field_name) {
                    $optionName = strtolower($field_name);
                    $field_id = $this->core->factory->Document()->getOptions($document['ID'])->$optionName;
                    $field_value_new = $this->core->factory->Record()->getFieldValue($this->record->id, $field_id);
                    $field_value_old = $this->core->factory->Record()->getFieldValue($this->previous_record->id, $field_id);
                    $field_values[$field_name] = array(
                        'NEW' => $field_value_new,
                        'OLD' => $field_value_old
                    );
                }
                if (empty($field_values[$fields_arr[0]]) === false && strtotime($field_values[$fields_arr[0]]['NEW']) > strtotime($field_values[$fields_arr[0]]['OLD'])) {
                    return $this->record->id;
                }
            }
        }
    }

    /**
     * "Byla nutraukta" apeliacinio teismo sprendimu
     */
    private function status_59()
    {
        //status 62  + nutarimo duomenys > sprendimas > nutraukti byla
        if ((in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false && in_array($this->previous_record->table_to_id, array(
                \Atp\Document::DOCUMENT_CASE)) === false) || in_array($this->record->doc_status, array(62)) === false /* && in_array($this->record->table_from_id, array(\Atp\Document::DOCUMENT_CASE)) === FALSE */) {
            return false;
        }
        $qry = 'SELECT ID, NAME FROM ' . PREFIX . 'ATP_FIELD where ITYPE = \'DOC\' AND SOURCE = 333 AND ITEM_ID = "' . \Atp\Document::DOCUMENT_CASE . '" LIMIT 1';
        $result = $this->core->db_query_fast($qry, array('DOCUMENT_ID' => $this->record->table_to_id));
        if ($this->core->db_num_rows($result)) {
            $row = $this->core->db_next($result);
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $row['ID']);

            $value = trim($value);
            if (empty($value) === false && (int) $value === 335) {
                $document = $this->core->logic2->get_document(array('ID' => \Atp\Document::DOCUMENT_CASE));
                $fields_arr = array('APPEAL_CCD_DATE');
                foreach ($fields_arr as $field_name) {
                    $optionName = strtolower($field_name);
                    $field_id = $this->core->factory->Document()->getOptions($document['ID'])->$optionName;
                    $field_value_new = $this->core->factory->Record()->getFieldValue($this->record->id, $field_id);
                    $field_value_old = $this->core->factory->Record()->getFieldValue($this->previous_record->id, $field_id);
                    $field_values[$field_name] = array(
                        'NEW' => $field_value_new,
                        'OLD' => $field_value_old
                    );
                }
                if (empty($field_values[$fields_arr[0]]) === false && strtotime($field_values[$fields_arr[0]]['NEW']) > strtotime($field_values[$fields_arr[0]]['OLD'])) {
                    return $this->record->id;
                }
            }
        }
        //status_49 + nutarimo duomenys > sprendimas > nutraukti byla

        return false;
    }
    /**
     * "Įsiteisėjęs nutarimas" apeliacinio teismo, po VMSA apeliacijos
     */
    /* private function status_60() {
      return false;
      } */

    /**
     * "Išieškojimas negalimas"
     */
    private function status_61()
    {
        //pasirinkimas/klasifikatorius + status 39
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false || in_array($this->record->doc_status, array(
                39)) === false) {
            return false;
        }

        //tikrinam ar pazymeta "perduota antstoliam">taip ir pazymeta "isieskojimas negalimas" > taip
        $qry = 'SELECT ID, NAME FROM ' . PREFIX . 'ATP_FIELD WHERE ITYPE = \'DOC\' AND SOURCE = 241 AND ITEM_ID = "' . \Atp\Document::DOCUMENT_CASE . '" LIMIT 1';
        $result = $this->core->db_query_fast($qry, array('DOCUMENT_ID' => $this->record->table_to_id));
        if ($this->core->db_num_rows($result)) {
            $row = $this->core->db_next($result);
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $row['ID']);

            $value = trim($value);
            if (empty($value) === false && (int) $value === 242) {
                // TODO: WTF???? Laukas, kurio tipas "CLASS" negali tiesiogiai priklausyti 57-tam dokumentui.
                $qry2 = 'SELECT ID, NAME FROM ' . PREFIX . 'ATP_FIELD WHERE ITYPE = \'CLASS\' AND SOURCE = 268 AND ITEM_ID = 57 LIMIT 1';
                $result2 = $this->core->db_query_fast($qry2, array('DOCUMENT_ID' => $this->record->table_to_id));
                if ($this->core->db_num_rows($result2)) {
                    $row2 = $this->core->db_next($result2);
                    $value = $this->core->factory->Record()->getFieldValue($this->record->id, $row['ID']);

                    $value = trim($value);
                    if (empty($value) === false && (int) $value === 269) {
                        return $this->record->id;
                    }
                }
            }
        }
        return false;
    }

    /**
     * "Išieškojo antstolis"
     */
    private function status_27()
    {
        //isieskojimo data + status 39
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false || in_array($this->record->doc_status, array(
                39)) === false) {
            return false;
        }

        //tikrinam ar pazymeta "perduota antstoliam">taip ir pazymeta "isieskojimas negalimas" > taip
        $qry = 'SELECT ID, NAME FROM ' . PREFIX . 'ATP_FIELD WHERE ITYPE = \'DOC\' AND SOURCE = 241 AND ITEM_ID = "' . \Atp\Document::DOCUMENT_CASE . '" LIMIT 1';
        $result = $this->core->db_query_fast($qry, array('DOCUMENT_ID' => $this->record->table_to_id));
        if ($this->core->db_num_rows($result)) {
            $row = $this->core->db_next($result);
            $value = $this->core->factory->Record()->getFieldValue($this->record->id, $row['ID']);

            $value = trim($value);
            if (empty($value) === false && (int) $value === 242) {
                $qry2 = 'SELECT ID, NAME FROM ' . PREFIX . 'ATP_FIELD WHERE ITYPE = \'CLASS\' AND TYPE = 12 AND ITEM_ID = 242 SORT BY ORDER DESC LIMIT 1';
                $result2 = $this->core->db_query_fast($qry2);
                if ($this->core->db_num_rows($result2)) {
                    $row2 = $this->core->db_next($result2);

                    // TODO: tikrai $row, o ne $row2?
//                    $field2 = $this->core->logic2->get_db(PREFIX . 'ATP_TBL_' . \Atp\Document::DOCUMENT_CASE, array(
//                        'ID' => $record_table['ID']), true, null, false, 'A.ID, A.`' . $row['NAME'] . '` as VALUE');
//                    $value = $field2['VALUE'];
                    $value = $this->core->factory->Record()->getFieldValue($this->record->id, $row['ID']);

                    $value = trim($value);
                    if (empty($value) === false && strtotime($value) > 0) {
                        return $this->record->id;
                    }
                }
            }
        }

        return false;
    }

    /**
     * "Išsiųsta byla", po apeliacijos
     */
    private function status_62()
    {
        //yra apskusta (54 status) ir ivesta data "issiusta teismui" ir ji didesne uz ankstesne versija
        if ((in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false && in_array($this->previous_record->table_to_id, array(
                \Atp\Document::DOCUMENT_CASE)) === false) || in_array($this->record->doc_status, array(54)) === false) {
            return false;
        }
        //yra apskusta (54 status) ir ivesta data "issiusta teismui"
        //Issiusta teismui datos laukas

        $document = $this->core->logic2->get_document(array('ID' => \Atp\Document::DOCUMENT_CASE));
        $fields_arr = array('APPEAL_STC_DATE');
        foreach ($fields_arr as $field_name) {
            $optionName = strtolower($field_name);
            $field_id = $this->core->factory->Document()->getOptions($document['ID'])->$optionName;
            $value_old = $this->core->factory->Record()->getFieldValue($this->previous_record->id, $field_id);
            $value_new = $this->core->factory->Record()->getFieldValue($this->record->id, $field_id);

            $field_value = array('OLD' => $value_old, 'NEW' => $value_new);
            $field_values[$field_name] = $field_value;
        }
        if (empty($field_values[$fields_arr[0]]) === false && strtotime($field_values[$fields_arr[0]]['NEW']) > strtotime($field_values[$fields_arr[0]]['OLD'])) {
            return $this->record->id;
        }
        return false;
    }

    /**
     * "Nutraukta byla, gražinta tirti"
     * Bylos dokumente, "Sprendimas po apskundimo" (NAME COL_61, ID: 2630) lauke
     * pasirinktas klasifikatorius "Nutraukti byla, gražinta tirti" (ID: 345)
     */
    private function status_63()
    {
        if (in_array($this->record->table_to_id, array(\Atp\Document::DOCUMENT_CASE)) === false) {
            return false;
        }

        // TODO: Lauko priskyrimas administravime
        $record_table = $this->core->logic2->get_record_table($this->record->id);
        if (empty($record_table['COL_61']) === false && (int) $record_table['COL_61'] === 345) {
            return $this->record->id;
        }

        return false;
    }

    /**
     * "Peduotas ATPR". Protokolas/Byla. Kai dokumentas perduodamas į ATPR.
     */
    private function status_64()
    {
        $forProtocol = in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_PROTOCOL]) && in_array($this->record->doc_status, [41]);
        $forCase = in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_CASE]) && in_array($this->record->doc_status, [19, 45]);

        if ($forProtocol || $forCase) {
            $atprDocumentRepository = $this->core->factory->Doctrine()->getRepository('Atp\Entity\Atpr\Atp');
            $atprDocumentRepository instanceof \Atp\Repository\Atpr\AtpRepository;

            if ($atprDocumentRepository->isByRecordId($this->record->id)) {
                return $this->record->id;
            }
        }

        return false;
    }

    /**
     * "Perduotas pasirašymui".
     * Protokolas - kai Avilyje sukuria el. dokumento projektą.
     * Pranešimas - -"-
     * Šaukimas - -"-
     * Lydraštis - -"-
     */
    private function status_65()
    {
        $validDocuments = [
            \Atp\Document::DOCUMENT_PROTOCOL,
            \Atp\Document::DOCUMENT_REPORT,
            \Atp\Document::DOCUMENT_SUMMONS,
            \Atp\Document::DOCUMENT_ACCOMPANYING,
        ];
        if (in_array($this->record->table_to_id, $validDocuments) === false) {
            return false;
        }

        /* @var $avilysDocumentRepository \Atp\Repository\Avilys\DocumentRepository */
        $avilysDocumentRepository = $this->core->factory->Doctrine()
            ->getRepository('Atp\Entity\Avilys\Document');

        if ($avilysDocumentRepository->isByRecordId($this->record->id)) {
            return $this->record->id;
        }

        return false;
    }

    /**
     * "Naujas lydraštis"
     * Lydraštis - kai sukuriamas.
     * 
     * @return boolean
     */
    private function status_66()
    {
        if (in_array($this->record->table_to_id, [\Atp\Document::DOCUMENT_ACCOMPANYING])) {
            return [$this->record->id];
        }

        return false;
    }

    private function put_contents($contents)
    {
        return;
        if ($debug === true && ($_SERVER['REMOTE_ADDR'] === '195.14.183.214' || ($_SERVER['REMOTE_ADDR'] === '127.0.0.1' && strpos($_SERVER['HTTP_HOST'], 'vilnius.lt') === false))) {
            file_put_contents(dirname(__FILE__) . '/log.txt', PHP_EOL . print_r($contents, true) . PHP_EOL, FILE_APPEND);
        }
    }
    /*
     * 2016.05.09 lenteles statusam

      ---- STATUS ----


      INSERT INTO `TST_ATP_STATUS` (`ID`, `LABEL`, `VALID`) VALUES
      (1, 'Perduotas tyrimui', 0),
      (2, 'Naujas', 1),
      (3, 'Nepaskirtas tiriantis', 0),
      (4, 'Paskirtas tiriantis', 1),
      (5, 'NepradÄ—tas tyrimas', 0),
      (6, 'Nustatytas savininkas/naudotojas, jo tipas (Regitra)', 1),
      (7, 'SuraÅ¡ytas praneÅ¡imas (juridinis)', 1),
      (8, 'Nustatytas subjektas', 1),
      (9, 'Nustatyta ar buvo baustas', 1),
      (10, 'SuraÅ¡ytas Å¡aukimas (fizinis)', 1),
      (11, 'IÅ¡siÅ³stas praneÅ¡imas', 0),
      (12, 'IÅ¡siÅ³stas Å¡aukimas', 0),
      (13, 'SuraÅ¡ytas ATP protokolas', 1),
      (14, 'IÅ¡siÅ³stas ATP protokolas', 1),
      (15, 'Kontroliuoti AN', 1),
      (16, 'Baigiasi AN Ä¯vykdymo terminas', 1),
      (17, 'AN Ä¯vykdytas', 1),
      (18, 'AN neÄ¯vykdytas', 1),
      (19, 'NagrinÄ—ti', 1),
      (20, 'UÅ¾vesta byla', 1),
      (21, 'IÅ¡siÅ³stas praneÅ¡imas atvykti', 1),
      (22, 'IÅ¡siÅ³stas pakartotinis praneÅ¡imas atvykti', 1),
      (23, 'Byla iÅ¡siÅ³sta teismui', 1),
      (24, 'Parengtas atsiliepimas Ä¯ skundÄ…', 1),
      (25, 'Parengtas apeliacinis skundas', 1),
      (26, 'Parengtas atsiliepimas Ä¯ apeliacinÄ¯ skundÄ…', 1),
      (27, 'IÅ¡ieÅ¡kojo antstolis', 1),
      (28, 'Byla baigta (kitu pagrindu)', 1),
      (29, 'Priimtas nutarimas', 1),
      (30, 'Priimti sprendimÄ… del nutarimo', 1),
      (31, 'Priimtas nutarimas iÅ¡nagrinÄ—toje byloje', 1),
      (32, 'Tikrinti sumokÄ—jimÄ…', 1),
      (33, 'SumokÄ—ta', 1),
      (34, 'IÅ¡siÅ³sta', 0),
      (35, 'Baigtas', 1),
      (36, 'Byla nutraukta', 1),
      (38, 'Tyrimas baigtas', 1),
      (39, 'NesumokÄ—ta', 1),
      (40, 'Byla apskÅ³sta', 1),
      (41, 'Naujas protokolas', 1),
      (42, 'Naujas Å¡aukimas', 1),
      (43, 'Naujas praneÅ¡imas', 1),
      (44, 'UÅ¾vesta byla2', 1),
      (45, 'NagrinÄ—ti2', 1),
      (46, 'Byla nutraukta', 1),
      (47, 'IsiteisÄ—jÄ™s nutarimas', 1),
      (48, 'Nutarimas iÅ¡nagrinÄ—toje byloje', 1),
      (49, 'IÅ¡siÅ³sta teismui', 1),
      (50, 'Rengiamas atsiliepimas', 1),
      (51, 'Rengiamas atsiliepimas Ä¯ apeliacinÄ¯ skundÄ…', 1),
      (52, 'Ä®siteisÄ—jÄ™s teismo nutarimas', 1),
      (53, 'Byla nutraukta2', 1),
      (54, 'Byla apskÅ«sta', 1),
      (55, 'Byla apskÅ«sta', 1),
      (56, 'Rengiamas apeliacinis skundas', 1),
      (57, 'IÅ¡siÅ³sta teismui', 1),
      (58, 'Ä®siteisÄ—jÄ™s apeliacinio teismo nutarimas', 1),
      (59, 'Byla nutraukta3', 1),
      (60, 'Ä®siteisÄ—jÄ™s apeliacinio teismo nutarimas', 1),
      (61, 'IÅ¡ieÅ¡kojimas negalimas', 1),
      (62, 'IÅ¡siÅ³sta teismui2', 1),
      (64, "Perduotas ATPR", 1),
      (65, "Perduotas pasiraÅ¡ymui", 1),
      (66, "Naujas LydraÅ¡tis", 1);


      ----- STAUS ORDER -----

      TRUNCATE TABLE `TST_ATP_STATUS_ORDER`;
      INSERT INTO `TST_ATP_STATUS_ORDER` (`ID`, `NR`, `PARENT_NR`, `STATUS_ID`, `DOCUMENT_ID`) VALUES
      (1, 10, 0, 2, 56),
      (2, 20, 10, 4, 56),
      (3, 30, 0, 2, 52),
      (4, 30, 20, 2, 52),
      (5, 40, 30, 6, 52),
      (40, 45, 60, 43, 54),
      #(6, 50, 45, 7, 54),
      (7, 60, 40, 8, 52),
      (9, 70, 60, 9, 52),
      (41, 75, 70, 42, 48),
      #(10, 80, 75, 10, 48),
      (36, 87, 70, 41, 36),
      (13, 90, 87, 13, 36),
      #(14, 100, 90, 14, 53),
      (15, 110, 90, 15, 36),
      (37, 130, 90, 17, 36),
      (45, 130, 110, 17, 36),
      (20, 140, 110, 18, 36),
      (42, 150, 90, 35, 36),
      (22, 150, 130, 35, 36),
      (23, 155, 140, 20, 57),
      (74, 155, 90, 20, 57),
      (24, 160, 155, 19, 57),
      #(25, 180, 155, 21, 54),
      #(26, 190, 155, 22, 54),
      (69, 193, 155, 44, 57),
      (43, 193, 160, 44, 57),
      (44, 196, 193, 45, 57),
      #(27, 200, 160, 29, 57),
      #(28, 200, 196, 29, 57),
      (47, 210, 200, 40, 57),
      (30, 220, 200, 46, 57),
      (48, 220, 210, 46, 57),
      (49, 230, 210, 48, 57),
      (46, 240, 200, 47, 57),
      (34, 250, 210, 49, 57),
      (51, 270, 250, 52, 57),
      (50, 280, 250, 53, 57),
      (53, 310, 270, 54, 57),
      (54, 320, 270, 55, 57),
      (55, 320, 280, 55, 57),
      (56, 330, 310, 62, 57),
      (57, 360, 320, 57, 57),
      (58, 370, 330, 59, 57),
      (59, 370, 360, 59, 57),
      (60, 380, 330, 58, 57),
      (63, 390, 360, 60, 57),
      (33, 400, 240, 32, 57),
      (61, 400, 270, 32, 57),
      (62, 400, 380, 32, 57),
      (64, 400, 390, 32, 57),
      (32, 400, 230, 43, 57),
      (65, 410, 400, 33, 57),
      (66, 420, 400, 39, 57),
      (67, 430, 420, 27, 57),
      (68, 440, 420, 61, 57),
      (89, 87, 64, 36),
      #(100, 89, 14, 53),
      (130, 89, 17, 36),
      (110, 89, 15, 36),
      (155, 89, 20, 57),
      (88, 87, 65, 36),
      #(100, 88, 14, 53),
      (130, 88, 17, 36),
      (110, 88, 15, 36),
      (155, 88, 20, 57),
      (170, 196, 64, 57),
      (170, 160, 64, 57),
      (200, 170, 29, 57),
      (NULL, 47, 45, 65, 54),
      (NULL, 50, 47, 7, 54),
      (NULL, 77, 75, 65, 48),
      (NULL, 80, 77, 10, 48),
      (NULL, 94, 88, 66, 53),
      (NULL, 94, 89, 66, 53),
      (NULL, 94, 90, 66, 53),
      (NULL, 96, 94, 65, 53),
      (NULL, 100, 96, 14, 53),
      (NULL, 174, 155, 43, 54),
      (NULL, 178, 174, 65, 54),
      (NULL, 180, 178, 21, 54),
      (NULL, 190, 178, 22, 54),
      (118, 45, 40, 43, 54)
     ;
     */
}
