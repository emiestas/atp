<?php

//require_once(__DIR__ . '/class-gets.php');
class atp_manage/* extends atp_gets */ {

	public $core;
	public $logic;
	public $logic2;
	public $action;
	public $user;
	public $table;
	public $field_types;
	public $field_type;
	public $table_record;
	public $table_structure;

	//public $new_table;
	//public $webservices;

	public function __construct(&$core) {
		$this->core = &$core;
		$this->logic = &$core->logic;
		$this->logic2 = &$core->logic2;
		$this->action = &$core->action;
	}

	/**
	 * Set'ina klasės kintamąjį
	 * @param string $var Kintamojo pavadinimas
	 * @param string $val Kintamojo reikšmė
	 * @deprecated Nenaudojamas
	 */
	function _set($var, $val) {
		trigger_error('_set() is deprecated', E_USER_DEPRECATED);
		if (is_array($var) === TRUE)
			foreach ($var as $a)
				$this->_set($a, $val->{$a});
		else
			$this->$var = $val;
	}

	public function lng($string) {
		return $this->core->getLangElement($string);
	}

	/**
	 * Dokumentai (Administravimas)
	 * @return string
	 */
	public function tables() {
		$tmpl = $this->core->Template();
		if (!$this->core->user->status['A_TABLE']) {
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
		} else {
			$table_id = (!empty($_REQUEST['table_id']) ? $_REQUEST['table_id'] : 0);
			$parent_id = (!empty($_REQUEST['table_parent']) ? $_REQUEST['table_parent'] : 0);
			if (isset($_GET['a']) && $this->core->user->status['A_TABLE'] > 1) {
				if ($_GET['a'] == 'edit_table')
					$this->logic->edit_table();
				if ($_GET['a'] == 'delete_table')
					$this->logic->delete_table();
			}

			$field_sort = $this->logic->get_field_sort();
			$field_type = $this->logic->get_field_type();
			//$web_services = $this->logic->get_web_services();
			//$web_service_type = $this->logic->get_web_service_type();
			//$classifications = $this->logic->get_classification();
			//$classifications = $this->logic2->get_classificator();
			$classifications = $this->logic2->get_classificator(null, null, null, 'ID, LABEL');
			//$deeds = $this->logic->get_deeds();
			//$acts = $this->logic2->get_acts();

			$tmpl_handler = 'manage_tables_file';
			$tmpl->set_file($tmpl_handler, 'manage_tables.tpl');


			/* $blocks_arr = array(
			  'penalty_field', 'examinator_field', 'examinator_cabinet_field',
			  'examinator_phone_field', 'examinator_email_field', 'examinator_institution_field',
			  'examinator_unit_field', 'examinator_subunit_field', 'examinator_institution_address_field',
			  'examinator_date_field', 'examinator_start_time_field', 'examinator_finish_time_field'
			  );
			  for ($i = 0; $i < count($blocks_arr); $i++) {
			  $blocks[$blocks_arr[$i]] = $tmpl_handler . $blocks_arr[$i];
			  $tmpl->set_block($tmpl_handler, $blocks_arr[$i], $blocks[$blocks_arr[$i]]);
			  } */

			$field_block = $tmpl_handler . 'field_block';
			$tmpl->set_block($tmpl_handler, 'field_block', $field_block);
			$field_type_block = $tmpl_handler . 'field_type';
			$tmpl->set_block($tmpl_handler, 'field_type', $field_type_block);

			$Marker_1 = $tmpl_handler . 'Marker_1';
			$tmpl->set_block($tmpl_handler, 'Marker_1', $Marker_1);
			$Marker_3 = $tmpl_handler . 'Marker_3';
			$tmpl->set_block($tmpl_handler, 'Marker_3', $Marker_3);
			$Marker_5 = $tmpl_handler . 'Marker_5';
			$tmpl->set_block($tmpl_handler, 'Marker_5', $Marker_5);
			$form_block = $tmpl_handler . 'form';
			$tmpl->set_block($tmpl_handler, 'form', $form_block);

			// TODO: lng_table nenaudoajamas
			$tmpl->set_var($this->core->set_lng_vars(array('title', 'table', 'not_chosen', 'save'), array('action_edit_table' => 'index.php?m=1&a=edit_table', 'action_new_table' => 'index.php?m=1&a=new_table')));
			if ($this->core->user->status['A_TABLE'] > 1) {
				$tables = $this->logic2->get_document();

				foreach ($tables as &$table) {
					$tmpl->set_var(array(
						'selected' => ($table_id == $table['ID'] ? '' : ''),
						'table_id' => $table['ID'],
						'table_label' => $table['LABEL']));
					$tmpl->parse($Marker_1, 'Marker_1', true);
					$tmpl->set_var(array(
						'value' => $table['ID'],
						'label' => $table['LABEL']));
					$tmpl->parse($Marker_3, 'Marker_3', true);
				}

				$tmpl->set_var($this->core->set_lng_vars(array('parent_table', 'not_chosen')));
				$tmpl->set_var($this->core->set_lng_vars(array('save', 'chosen_table_structure', 'title', 'not_chosen')));

				foreach ($field_sort as $sort_id => $sort) {

					$tmpl->set_var(array(
						'field_sort_id' => $sort['ID'],
						'field_sort_label' => $sort['LABEL']));
					$tmpl->parse($Marker_5, 'Marker_5', true);
				}
//NAME,SURNAME, COMPANY, ACQ_CONFIRMATION
				$field_types = array(
					'table_person_code_field' => array('LABEL' => 'Pažeidėjo asmens kodo laukas', 'TYPE' => null, 'ELEMENT' => 'PERSON_CODE_FIELD'),
					'fields[person_address]' => array('LABEL' => 'Pažeidėjo adresas(fiz)', 'TYPE' => null, 'ELEMENT' => 'PERSON_ADDRESS'),
					'fields[company_address]' => array('LABEL' => 'Pažeidėjo adresas(jur)', 'TYPE' => null, 'ELEMENT' => 'COMPANY_ADDRESS'),
					'fields[other_address]' => array('LABEL' => 'Pažeidėjo adresas(kitas)', 'TYPE' => null, 'ELEMENT' => 'OTHER_ADDRESS'),
					'fields[person_name]' => array('LABEL' => 'Pažeidėjo vardas', 'TYPE' => null, 'ELEMENT' => 'PERSON_NAME'),
					'fields[person_surname]' => array('LABEL' => 'Pažeidėjo pavardė', 'TYPE' => null, 'ELEMENT' => 'PERSON_SURNAME'),
					'fields[company_name]' => array('LABEL' => 'Įmonės pavadinimas', 'TYPE' => null, 'ELEMENT' => 'COMPANY_NAME'),
					/* ALTER TABLE `TST_ATP_DOCUMENTS` ADD `PERSON_ADDRESS` INT UNSIGNED NOT NULL AFTER `PERSON_CODE_FIELD`;
					  ALTER TABLE `TST_ATP_DOCUMENTS` ADD `COMPANY_ADDRESS` INT UNSIGNED NOT NULL AFTER `PERSON_ADDRESS`;
					  ALTER TABLE `TST_ATP_DOCUMENTS` ADD `OTHER_ADDRESS` INT UNSIGNED NOT NULL AFTER `COMPANY_ADDRESS`;
					  ALTER TABLE `TST_ATP_DOCUMENTS` ADD `PERSON_NAME` INT UNSIGNED NOT NULL AFTER `OTHER_ADDRESS`;
					  ALTER TABLE `TST_ATP_DOCUMENTS` ADD `PERSON_SURNAME` INT UNSIGNED NOT NULL AFTER `PERSON_NAME`;
					  ALTER TABLE `TST_ATP_DOCUMENTS` ADD `COMPANY_NAME` INT UNSIGNED NOT NULL AFTER `PERSON_SURNAME`; */
					'table_date_field' => array('LABEL' => 'Pažeidimo datos laukas', 'TYPE' => null, 'ELEMENT' => 'DATE_FIELD'),
					'table_fill_date_field' => array('LABEL' => 'Surašymo datos laukas', 'TYPE' => null, 'ELEMENT' => 'FILL_DATE_FIELD'),
					'date_of_decision_field' => array('LABEL' => 'Nutarimo priėmimo data', 'TYPE' => null, 'ELEMENT' => 'DATE_OF_DECISION_FIELD'),
					'fields[appealed_date]' => array('LABEL' => 'Apeliacijos data', 'TYPE' => null, 'ELEMENT' => 'APPEALED_DATE'),
					'fields[appeal_stc_date]' => array('LABEL' => 'Išsiuntimo teismui data', 'TYPE' => null, 'ELEMENT' => 'APPEAL_STC_DATE'),
					'fields[appeal_ccd_date]' => array('LABEL' => 'Sprendimo įsiteisėjimo data', 'TYPE' => null, 'ELEMENT' => 'APPEAL_CCD_DATE'),
                    /* ALTER TABLE `TST_ATP_DOCUMENTS` ADD `APPEAL_STC_DATE` INT UNSIGNED NOT NULL AFTER `APPEALED_DATE`; */
					'table_penalty_field' => array('LABEL' => 'Baudos sumos laukas', 'TYPE' => null, 'ELEMENT' => 'PENALTY_FIELD'),
					'fields[class_type]' => array('LABEL' => 'Pažeidimo tipo laukas', 'TYPE' => null, 'ELEMENT' => 'CLASS_TYPE'),
					/* array( */
					'table_avilys_date_field' => array('LABEL' => 'Registracijos data', 'TYPE' => null, 'ELEMENT' => 'AVILYS_DATE_FIELD'),
					'table_avilys_number_field' => array('LABEL' => 'Registracijos numeris', 'TYPE' => null, 'ELEMENT' => 'AVILYS_NUMBER_FIELD')
					/* ) */,
					/* array( */
					'fields[examinator]' => array('LABEL' => 'Nagrinėjantis pareigūnas', 'TYPE' => null, 'ELEMENT' => 'EXAMINATOR'),
					'fields[examinator_cabinet]' => array('LABEL' => 'Nagrinėjančio pareigūno kabinetas', 'TYPE' => null, 'ELEMENT' => 'EXAMINATOR_CABINET'),
					'fields[examinator_phone]' => array('LABEL' => 'Nagrinėjančio pareigūno telefonas', 'TYPE' => null, 'ELEMENT' => 'EXAMINATOR_PHONE'),
					'fields[examinator_email]' => array('LABEL' => 'Nagrinėjančio pareigūno el.paštas', 'TYPE' => null, 'ELEMENT' => 'EXAMINATOR_EMAIL'),
					'fields[examinator_institution_name]' => array('LABEL' => 'Nagrinėjanti institucija', 'TYPE' => null, 'ELEMENT' => 'EXAMINATOR_INSTITUTION_NAME'),
					'fields[examinator_unit]' => array('LABEL' => 'Nagrinėjantis dalinys', 'TYPE' => null, 'ELEMENT' => 'EXAMINATOR_UNIT'),
					'fields[examinator_subunit]' => array('LABEL' => 'Nagrinėjantis padalinys', 'TYPE' => null, 'ELEMENT' => 'EXAMINATOR_SUBUNIT'),
					'fields[examinator_institution_address]' => array('LABEL' => 'Nagrinėjančios institucijos adresas', 'TYPE' => null, 'ELEMENT' => 'EXAMINATOR_INSTITUTION_ADDRESS'),
					'fields[examinator_date]' => array('LABEL' => 'Nagrinėjimo data', 'TYPE' => null, 'ELEMENT' => 'EXAMINATOR_DATE'),
					'fields[examinator_start_time]' => array('LABEL' => 'Nagrinėjimo pradžios laikas', 'TYPE' => null, 'ELEMENT' => 'EXAMINATOR_START_TIME'),
					'fields[examinator_finish_time]' => array('LABEL' => 'Nagrinėjimo pabaigos laikas', 'TYPE' => null, 'ELEMENT' => 'EXAMINATOR_FINISH_TIME')
						/* ) */
				);
                
                $documents = $this->logic2->get_document();
                foreach($documents as $document) {
                    foreach($field_types as $type) {
                        if(isset($document[$type['ELEMENT']]))
                            continue;
                        // alter qry 
                        $qry = 'ALTER TABLE `'.PREFIX.'ATP_DOCUMENTS` ADD `'.$type['ELEMENT'].'` INT UNSIGNED NOT NULL';
                        $this->core->db_query_fast($qry);
                    }
                }

				/*
				  // TODO: kai dokumento redagavimas bus perkeltas į atskirą puslaį
				  $document_id = 52;
				  $all_fields = array();
				  $select_hierarchy = $this->tables_relations_parent_fields($document_id, 'DOC', $data = array(), $all_fields, $null = null);
				  $data = array();
				  $select_hierarchy = array_reverse($select_hierarchy, true);
				  $select_hierarchy[0]['OPT'] = '-- nepasirinkta --';
				  $data['select_hierarchy'] = array_reverse($select_hierarchy, true);
				  $all_fields[0] = array('ID' => 0, 'LABEL' => '-- nepasirinkta --');
				  $data['all_fields'] = &$all_fields;

				  $this->printTree($data['select_hierarchy'], 0, $data['all_fields'], $valid_fields = false, $selected, $opts);
				 */

				foreach ($field_types as $key => $type) {
					/*
					  // TODO: kai dokumento redagavimas bus perkeltas į atskirą puslaį
					  $tmpl->set_var(array(
					  'type_label' => $type['LABEL'],
					  'type_name' => $key));
					  $tmpl->parse($field_type_block, 'field_type', true);
					  $data = array('field_block' => $field_block);
					  $this->tables_classificator_fields_parse(array('PARENT_ID' => 0), $tmpl, $data);
					 */

					$tmpl->set_var(array(
						'type_label' => $type['LABEL'],
						'type_name' => $key,
						//'field_block' => '',
						$field_block => '',
						'options' => '',
					));
					$tmpl->parse($field_type_block, 'field_type', true);
				}
				$tmpl->set_var('field_types', json_encode($field_types));

				//$data = array('penalty_field' => $penalty_field);
				//$data = $blocks;
				//$this->tables_classificator_fields_parse(array('PARENT_ID' => 0), $tmpl, $data);

				$tmpl->set_var($this->core->set_lng_vars(array('not_chosen', 'save'), array('action_delete_table' => 'index.php?m=1&a=delete_table')));
				$tmpl->parse($form_block, 'form');
			} else {
				$tmpl->clean($form_block);
			}

			$tmpl->set_var(array(
				'sys_message' => $this->core->get_messages(),
				'table_id' => $table_id,
				'parent_id' => $parent_id,
				'field_type' => json_encode($field_type),
				'field_sort' => json_encode($field_sort),
				//'web_services' => json_encode($web_services),
				'web_services' => json_encode(array()), // TODO: remove
				//'web_service_type' => json_encode($web_service_type),
				'web_service_type' => json_encode(array()), // TODO: remove
				'classifications' => json_encode($classifications),
				'deeds' => json_encode($deeds),
				'acts' => json_encode($acts),
				'nr' => (empty($_POST['nr']) ? rand(0, 10000) : $_POST['nr']),
				'atp_table_list' => $this->table_list()
			));
		}
		$tmpl->set_var('GLOBAL_SITE_URL', GLOBAL_SITE_URL);
		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');
		return $this->core->atp_content($output);
	}

	/**
	 * Generuoja klasifikatorių laukų pasirinkimą ( Dokumentai (Administravimas))
	 * @param array $param Parametrai
	 * @param object $tmpl Šablono objektas
	 * @param array $data Papildomi duomenys
	 * @param int $depth [optional] Lygis. Default: 0
	 */
	private function tables_classificator_fields_parse($param, &$tmpl, Array &$data, $depth = 0) {
		if (empty($param) === TRUE) {
			$param = array();
		}
		if (empty($param['PARENT_ID']) === TRUE) {
			$tmpl->set_var(array(
				'value' => '',
				'disabled' => ' disabled="true"',
				'label' => str_repeat('&nbsp;', $depth * 3) . 'Klasifikatoriai',
			));
			foreach ($data as $key => $val) {
				$tmpl->parse($val, $key, true);
			}
			//$tmpl->parse($data['penalty_field'], 'penalty_field', true);
			$depth++;
		}
		$classificators = $this->logic2->get_classificator($param, false, 'LABEL DESC');
		foreach ($classificators as &$classificator) {
			$tmpl->set_var(array(
				'value' => '',
				'disabled' => ' disabled="true"',
				'label' => str_repeat('&nbsp;', $depth * 3) . $classificator['LABEL'],
			));
			foreach ($data as $key => $val) {
				$tmpl->parse($val, $key, true);
			}
			//$tmpl->parse($data['penalty_field'], 'penalty_field', true);

			$fields = $this->logic2->get_fields(array('ITEM_ID' => $classificator['ID'], 'ITYPE' => 'CLASS'), false, 'LABEL DESC');
			foreach ($fields as &$field) {
				$tmpl->set_var(array(
					'value' => $field['ID'],
					'disabled' => '',
					'label' => str_repeat('&nbsp;', ($depth + 1) * 3) . $field['LABEL'],
				));
				//$tmpl->parse($data['penalty_field'], 'penalty_field', true);
				foreach ($data as $key => &$val) {
					$tmpl->parse($val, $key, true);
				}
			}

			$this->tables_classificator_fields_parse(array('PARENT_ID' => $classificator['ID']), $tmpl, $data, $depth + 1);
		}
	}

	/**
	 * Dokumentų ryšiai (Administravimas)
	 * @return string
	 */
	public function documents_relations() {
		if (!$this->core->user->status['A_RELATION']) {
			$tmpl = $this->core->Template();
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
			$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
			$output = $tmpl->get($tmpl_handler . '_out');
			return $this->core->atp_content($output);
		}

		$table_id = (!empty($_POST['table_id'])) ? $_POST['table_id'] : 0;
		$table_path_id = (!empty($_POST['table_path_id'])) ? $_POST['table_path_id'] : 0;

		if (isset($_GET['a']) && $this->core->user->status['A_RELATION'] > 1) {
			if ($_GET['a'] == 'edit_table_relations')
				$this->logic->edit_table_relations();
		}

		$tmpl = $this->core->Template();
		$tmpl_handler = 'documents_relations_file';
		$tmpl->set_file($tmpl_handler, 'documents_relations.tpl');

		// Tėvinis dokumentas
		$parent_doc_item = $tmpl_handler . 'parent_doc_item_block';
		$tmpl->set_block($tmpl_handler, 'parent_doc_item', $parent_doc_item);
		// Ryšys su dokumentu
		$doc_item = $tmpl_handler . 'doc_item';
		$tmpl->set_block($tmpl_handler, 'doc_item', $doc_item);

		// Tėvinio dokumento lauko pasirinkimai
		//$parent_doc_row_option = $tmpl_handler . 'parent_doc_row_option';
		//$tmpl->set_block($tmpl_handler, 'parent_doc_row_option', $parent_doc_row_option);
		// Tėvinio dokumento lauko pasirinkimų grupės pradžia
		//$parent_doc_row_option_group_begin = $tmpl_handler . 'parent_doc_row_option_group_begin';
		//$tmpl->set_block($tmpl_handler, 'parent_doc_row_option_group_begin', $parent_doc_row_option_group_begin);
		// Tėvinio dokumento lauko pasirinkimų grupės pabaiga
		//$parent_doc_row_option_group_end = $tmpl_handler . 'parent_doc_row_option_group_end';
		//$tmpl->set_block($tmpl_handler, 'parent_doc_row_option_group_end', $parent_doc_row_option_group_end);
		// Tėvinio dokumento lauko pasirinkimas [visas]
		//$parent_doc_row_option_all = $tmpl_handler . 'parent_doc_row_option_all';
		//$tmpl->set_block($tmpl_handler, 'parent_doc_row_option_all', $parent_doc_row_option_all);
		// Tėvinio dokumento straipsnio/klasifikatoriaus pavadinimas
		$parent_doc_row_parent = $tmpl_handler . 'parent_doc_row_parent';
		$tmpl->set_block($tmpl_handler, 'parent_doc_row_parent', $parent_doc_row_parent);
		// Tėvinio dokumento lauko duomenys
		$parent_doc_data = $tmpl_handler . 'parent_doc_data';
		$tmpl->set_block($tmpl_handler, 'parent_doc_data', $parent_doc_data);
		// Tėvinio dokumento laukas
		$parent_doc_row = $tmpl_handler . 'parent_doc_row';
		$tmpl->set_block($tmpl_handler, 'parent_doc_row', $parent_doc_row);

		// Ryšio su dokumentu straipsnio/klasifikatoriaus pavadinimas
		$doc_row_parent = $tmpl_handler . 'doc_row_parent';
		$tmpl->set_block($tmpl_handler, 'doc_row_parent', $doc_row_parent);
		// Ryšio su dokumentu lauko duomenys
		$doc_data = $tmpl_handler . 'doc_data';
		$tmpl->set_block($tmpl_handler, 'doc_data', $doc_data);
		// Ryšio su dokumentu laukai
		$doc_row = $tmpl_handler . 'doc_row';
		$tmpl->set_block($tmpl_handler, 'doc_row', $doc_row);

		// Saugojimo mygtukas
		$button = $tmpl_handler . 'button';
		$tmpl->set_block($tmpl_handler, 'button', $button);
		// Redagavimo forma
		$form_block = $tmpl_handler . 'form_block';
		$tmpl->set_block($tmpl_handler, 'form_block', $form_block);

		$field_sort = $this->logic->get_field_sort();
		$field_type = $this->logic->get_field_type();

		// Tėvinio dokumento laukai
		$tables = $this->logic2->get_document();
		array_unshift($tables, array('ID' => 0, 'LABEL' => '--nepasirinkta--'));
		foreach ($tables as &$table) {
			$tmpl->set_var(array(
				'id' => $table['ID'],
				'label' => $table['LABEL'],
				'selected' => $table_id == $table['ID'] ? ' selected="selected"' : ''));
			$tmpl->parse($parent_doc_item, 'parent_doc_item', true);
		}

		// Laukai su kuriais gali būti kuriamas ryšys
		if (empty($table_id) === FALSE) {
			$table_path = $this->logic->get_table_path($table_id);
			foreach ($table_path as &$paths) {
				array_unshift($paths, array('ID' => 0, 'LABEL' => '--nepasirinkta--'));
				foreach ($paths as &$path) {
					$tmpl->set_var(array(
						'id' => $path['ID'],
						'label' => $path['LABEL'],
						'selected' => ($table_path_id == $path['ID']) ? ' selected="selected"' : ''));
					$tmpl->parse($doc_item, 'doc_item', true);
				}
			}
		}

		if (!empty($table_id) && !empty($table_path_id)) {
			//$table_relations = $this->logic->get_table_relations($table_id, $table_path_id);
			$table_relations = $this->logic->get_table_relations($table_path_id, $table_id);

			$t_default_relations = $t_relations = array();
			foreach ($table_relations as $relation) {
				//if($relation['DEFAULT'] === NULL && $relation['COLUMN_PARENT_ID'] !== NULL) {
				if (empty($relation['DEFAULT']) === TRUE && empty($relation['COLUMN_PARENT_ID']) === FALSE) {
					$t_relations[$relation['COLUMN_ID']][] = $relation['COLUMN_PARENT_ID'];
					//} else if($relation['DEFAULT'] !== NULL && $relation['COLUMN_PARENT_ID'] === NULL) {
				} else if (empty($relation['DEFAULT']) === FALSE && empty($relation['COLUMN_PARENT_ID']) === TRUE) {
					$t_default_relations[$relation['COLUMN_ID']][] = $relation['DEFAULT'];
				}
			}

			$data = array();
			$data['field_type'] = &$field_type;
			$data['t_relations'] = &$t_relations;
			$data['t_default_relations'] = &$t_default_relations;
			//$data['parent_structure_select'] = &$parent_structure_select;
			$data['parent_doc_row_parent'] = &$parent_doc_row_parent;
			//$data['parent_doc_row_option'] = &$parent_doc_row_option;
			//$data['parent_doc_row_option_group_begin'] = &$parent_doc_row_option_group_begin;
			//$data['parent_doc_row_option_group_end'] = &$parent_doc_row_option_group_end;
			//$data['parent_doc_row_option_all'] = &$parent_doc_row_option_all;

			$data['parent_doc_data'] = &$parent_doc_data;
			$data['parent_doc_row'] = &$parent_doc_row;

			$data['doc_row_parent'] = &$doc_row_parent;
			$data['doc_data'] = &$doc_data;
			$data['doc_row'] = &$doc_row;
			$all_fields = array();
			$select_hierarchy = $this->tables_relations_parent_fields($table_path_id, 'DOC', $data, $all_fields, /* $select_hierarchy, */ $tmpl);
			$select_hierarchy = array_reverse($select_hierarchy, true);

			$select_hierarchy[0]['OPT'] = '-- nepasirinkta --';
			$select_hierarchy = array_reverse($select_hierarchy, true);

			$data['select_hierarchy'] = $select_hierarchy;

			$data['all_fields'] = &$all_fields;
			$data['fields_by_type'] = array();
			foreach ($all_fields as $f) {
				$data['fields_by_type'][$f['TYPE']][] = $f['ID'];
			}
//$s = microtime(true);
			$this->tables_relations_fields($table_id, 'DOC', $data, $tmpl);
//$e = microtime(true);
			// Saugojimo mygtukas
			if ($this->core->user->status['A_RELATION'] > 1) {
				$tmpl->set_var($this->core->set_lng_vars(array('save')));
				$tmpl->parse($button, 'button');
			}

			// Antraštės
			$tmpl->set_var(
					array_merge(
							(array)
							$this->core->set_lng_vars(array(
								'field_name',
								'field_type',
								'parent_fields',
								'column_relations'), array('table_id' => $table_id,
								'table_path_id' => $table_path_id,
								'action_edit_realtions' => 'index.php?m=2&a=edit_table_relations')), (array)
							$this->core->set_lng_vars(array(
								'field_name',
								'field_type',
								'parent_table_sturucture'))
			));
			$tmpl->parse($form_block, 'form_block');
		} else
			$tmpl->clean($form_block);

		$tmpl->set_var(
				array_merge(
						(array)
						$this->core->set_lng_vars(array(
							'title',
							'table_relations',
							'not_chosen'), array(
							'action_choose_table' => 'index.php?m=2')
						), array('sys_message' => $this->core->get_messages())
		));

		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');
		return $this->core->atp_content($output);
	}

	/**
	 * 
	 * @deprecated Nenaudojamas
	 */
	function set_parents(&$h, $a, $bt, &$r = array(), $parents = array()) {
		return null;
		$has_child = false;
		foreach ($h as &$hi) {
			if (isset($hi['CHILDREN'])) {
				$has_child = true;
				if (isset($hi['FIELD'])) {
					$parents[] = $hi['FIELD']['LABEL'];
				}
				if (isset($hi['GROUP'])) {
					$parents[] = $hi['GROUP']['LABEL'];
				}
				foreach ($hi['CHILDREN'] as &$arr) {
					$arr['FIELD']['PARENTS'] = $parents;
				}
				$this->set_parents($hi['CHILDREN'], $a, $bt, $r, $parents);
			}
		}
		return $r;
	}

	private function tables_relations_fields_options($data, &$tmpl = null, $depth = 0, $data2 = array()) {
		return null;

		if (empty($data2) === FALSE)
			$fields = &$data2;
		else
			$fields = &$data['fields_hierarchy'];

		if (empty($data['fields_by_type'][$data['field']['TYPE']]))
			return null;
		$valid_fields = &$data['fields_by_type'][$data['field']['TYPE']];
		foreach ($fields as $field2) {
			if (isset($field2['FIELD']) === TRUE && in_array($field2['FIELD']['ID'], $valid_fields)) {
				$tmpl->set_var(array(
					'selected' => !empty($data['t_relations'][$field['ID']]) && in_array($field2['FIELD']['ID'], $data['t_relations'][$field['ID']]) ? ' selected="selected"' : '',
					'value' => $field2['FIELD']['ID'],
					'label' => $field2['FIELD']['LABEL']
				));
			} else if (isset($field2['GROUP']) === TRUE) {
				$tmpl->set_var(array(
					'label' => $field2['FIELD']['LABEL']
				));
			} else
				return null;

			if (isset($field2['CHILDREN']) === TRUE) {
				$this->tables_relations_fields_options($data, $tmpl, null, $field2['CHILDREN']);
			}
		}
	}

	// TODO: Nebaigta. Reikia, kad nerodytu šakos, jei nėra rodomų tinkamų lapų.
	function printTree2($tree, $depth = 0, $all_fields, $valid_fields, $selected, &$html = null, $is_group = false, $found = false/* , &$good_field = 0 */) {
		if (empty($valid_fields) === TRUE && $valid_fields !== FALSE) {
			return null;
		}

		//$found = false;
		$tabs = str_repeat('&nbsp;', $depth * 3);
		foreach ($tree as $i => $t) {
			if ($is_group === FALSE) {
				if (in_array($i, (array) $valid_fields) || $valid_fields === FALSE) {
					$found = true;
					$sel = (in_array($all_fields[$i]['ID'], (array) $selected) ? ' selected="selected"' : '');
					$html .= sprintf("\t\t<option value='%d' %s class=\"%s\">%s%s</option>\n", $all_fields[$i]['ID'], $sel, 'child level-' . $depth, $tabs, $all_fields[$i]['LABEL']);
				} else
				if (/* $found === TRUE && */ isset($t['GROUP'])) {
					/* $html .= */$parent = sprintf("<option value='-1' class=\"bb %s\" disabled=\"disabled\">%s%s</option>", 'parent level-' . $depth, $tabs, $all_fields[$i]['LABEL']);
					$found = false;
				}
			} else {
				$parent2 = sprintf("<option value='-1' class=\"aa %s\" disabled=\"disabled\">%s%s</option>", 'parent level-' . $depth, $tabs, $t['OPT']);
			}
			$temp = '';
			if (isset($t['GROUP'])) {
				$found = $this->printTree($t['GROUP'], $depth + 1, $all_fields, $valid_fields, $selected, $temp, true, false);

				$temp = $parent . $temp;
			} else if ($t['CHILDREN']) {
				$found = $this->printTree($t['CHILDREN'], $depth + 1, $all_fields, $valid_fields, $selected, $temp);
			}
			if ($found === TRUE && $is_group === TRUE)
				$temp = $parent2 . $temp;

			$html .= sprintf("%s", $temp);
		}

		return $found;
	}

	/**
	 *
	 * @param array $tree
	 * @param int $depth
	 * @param array $all_fields
	 * @param array $valid_fields
	 * @param array $selected
	 * @param string $html
	 * @param boolen $is_group
	 * @return boolean|null
	 */
	function printTree($tree, $depth = 0, $all_fields, $valid_fields, $selected, &$html = null, $is_group = false) {
		return $this->printTree2($tree, $depth, $all_fields, $valid_fields, $selected, $html, $is_group);
		if (empty($valid_fields) === TRUE)
			return null;

		$found = false;
		$tabs = str_repeat('&nbsp;', $depth);
		foreach ($tree as $i => $t) {
			if (in_array($i, $valid_fields) && $is_group === FALSE) {
				$found = true;
				$sel = (in_array($all_fields[$i]['ID'], (array) $selected) ? ' selected="selected"' : '');
				$html .= sprintf("\t\t<option value='%d' %s class=\"%s\">%s%s</option>\n", $all_fields[$i]['ID'], $sel, 'child level-' . $depth, $tabs, $all_fields[$i]['LABEL']);
			} else {
				$html .= sprintf("<option value='-1' class=\"%s\" disabled=\"disabled\">%s%s</option>", 'parent level-' . $depth, $tabs, $t['OPT']);
			}
			$temp = '';

			if (isset($t['GROUP'])) {
				$depth++;
				$j = 0;
				foreach ($t['GROUP'] as $group) {
					if (empty($group['CHILDREN']) === FALSE) {
						$depth++;
						$children = $this->printTree($group['CHILDREN'], $depth, $all_fields, $valid_fields, $selected);
						if ($children === TRUE) {
							$tabs = str_repeat('&nbsp;', $depth);
							$temp .= sprintf("<option value='-1' class=\"%s\" disabled=\"disabled\">%s%s</option>", 'parent level-' . $depth, $tabs, $group['OPT']);
							$depth++;
							$this->printTree($group['CHILDREN'], $depth, $all_fields, $valid_fields, $selected, $temp);
							$depth--;
						}
						$depth--;
						$j++;
					}
				}
				if ($j === 0) {
					$temp = '';
					$depth++;
					$this->printTree($t['GROUP'], $depth, $all_fields, $valid_fields, $selected, $temp, true);
//HERE/				ĘĘĖĮŠŲŪ(	//	$this->printTree($t['GROUP'], $depth, $all_fields, $valid_fields, $selected);
				}
				$depth--;
			}
			/*
			  if(isset($t['CHILDREN'])) {
			  $depth++;
			  $children = $this->printTree($group['CHILDREN'], $depth, $all_fields, $valid_fields, $selected);
			  if ($children === TRUE) {
			  $tabs = str_repeat('&nbsp;', $depth);
			  $temp .= sprintf("<option value='-1' class=\"%s\" disabled=\"disabled\">%s%s</option>", 'parent level-' . $depth, $tabs, $group['OPT']);
			  $depth++;
			  $this->printTree($group['CHILDREN'], $depth, $all_fields, $valid_fields, $selected, $temp);
			  $depth--;
			  }
			  $depth--;
			  $j++;

			  } */
			if (empty($temp) === FALSE) {
				$depth++;
				$tabs = str_repeat('&nbsp;', $depth);
				$html .= sprintf("<option value='-1' class=\"%s\" disabled=\"disabled\">%s%s</option>%s", 'parent level-' . $depth, $tabs, $all_fields[$i]['LABEL'], $temp);
			}
		}
		return $found;
	}

	public function tables_relations_fields($id, $type, $data, &$tmpl = null, $depth = 0) {
		// TODO: ACT, jei reikės (turėtu) Veiką plečiančių laukų
		if (in_array($type, array('DOC', 'CLASS', 'CLAUSE', 'DEED', 'ACT'), true) === FALSE) {
			//if (in_array($type, array_keys($this->core->_prefix->item), true) === FALSE) {
			trigger_error('Unknown item type', E_USER_WARNING);
			return false;
		}

		if (empty($tmpl) === FALSE) {
			if (isset($data['multiplier']))
				$data['multiplier'] = (int) $data['multiplier'];
			else
				$data['multiplier'] = 3;
		}

		// Gauna įrašo laukus
		//$fields = $this->logic->get_extra_fields($id, $type);
		$fields = $this->logic2->get_fields(array('ITYPE' => $type, 'ITEM_ID' => $id), false, '`ORD` ASC');

		foreach ($fields as &$field) {
			if (empty($field) === TRUE)
				continue;

			$selected = array();
			if (isset($data['t_relations'][$field['ID']]))
				$selected = $data['t_relations'][$field['ID']];

			$default_value = null;
			$default_field_html = null;
			if (isset($data['t_default_relations'][$field['ID']])) {
				$default_value = $data['t_default_relations'][$field['ID']][0];
				$default_field_html = $this->get_field_html(array(
					'ID' => $field['ID'],
					'VALUE' => $default_value));
			}

			$valid_fields = $data['fields_by_type'][$field['TYPE']];
			$valid_fields[] = 0;
			$data['all_fields'][0] = array('ID' => 0, 'LABEL' => '-- nepasirinkta --');
			$opts = '';
			$this->printTree($data['select_hierarchy'], 0, $data['all_fields'], $valid_fields, $selected, $opts);
			$tmpl->set_var('options', $opts);

			// Lauko duomenys
			$tmpl->set_var(array(
				'id' => $field['ID'],
				'disabled' => (isset($default_value) ? ' disabled="disabled" style="display: none;"' : ''),
				'active' => (isset($default_value) ? ' active' : ''),
				'default' => (isset($default_field_html) ? $default_field_html : ''),
				'label' => str_repeat('&nbsp;', $data['multiplier'] * $depth) . $field['LABEL'],
				'type' => $data['field_type'][$field['TYPE']]['LABEL'],
				'type_attr' => (empty($field['TYPE']) ? '' : ' type="' . $field['TYPE'] . '"'),
				$data['parent_doc_row_parent'] => ''));
			$tmpl->parse($data['parent_doc_data'], 'parent_doc_data');
			$tmpl->parse($data['parent_doc_row'], 'parent_doc_row', true);

			// Klasifikatorius arba Straipsnis
			if ($field['TYPE'] === '18' || $field['SORT'] === '6') {
				// Klasifikatorius
				if ($field['TYPE'] === '18') {
					// TODO: gauna tik pirmo lygio klasifikatorius, reikia visų?
					//$items = $this->logic->get_classification(null, $field["SOURCE"]);
					$items = $this->logic2->get_classificator(array('PARENT_ID' => $field["SOURCE"]));
					$type = 'CLASS';
					$label_attr = 'LABEL';
				} else
				// Straipsnis
				if ($field['SORT'] === '6') {
					$items = $this->logic2->get_clause();
					$type = 'CLAUSE';
					$label_attr = 'NAME';
				}
				foreach ($items as &$item) {
					$tmpl->set_var(array(
						'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item[$label_attr],
						$data['parent_doc_data'] => ''));
					$tmpl->parse($data['parent_doc_row_parent'], 'parent_doc_row_parent');
					$tmpl->parse($data['parent_doc_row'], 'parent_doc_row', true);
					$this->tables_relations_fields($item['ID'], $type, $data, $tmpl, $depth + 2);
				}
			} else
			/* // Klasifikatorius
			  if ($field['TYPE'] === '18') {
			  // TODO: gauna tik pirmo lygio klasifikatorius, reikia visų?
			  //$items = $this->logic->get_classification(null, $field["SOURCE"]);
			  $items = $this->logic2->get_classificator(array('PARENT_ID' => $field["SOURCE"]));
			  foreach ($items as &$item) {
			  $tmpl->set_var(array(
			  'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['LABEL'],
			  $data['parent_doc_data'] => ''));
			  $tmpl->parse($data['parent_doc_row_parent'], 'parent_doc_row_parent');
			  $tmpl->parse($data['parent_doc_row'], 'parent_doc_row', true);
			  $this->tables_relations_fields($item['ID'], 'CLASS', $data, $tmpl, $depth + 2);
			  }
			  } else
			  // Straipsnis
			  if ($field['SORT'] === '6') {
			  $items = $this->logic2->get_clause();
			  foreach ($items as &$item) {
			  $tmpl->set_var(array(
			  'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['NAME'],
			  $data['parent_doc_data'] => ''));
			  $tmpl->parse($data['parent_doc_row_parent'], 'parent_doc_row_parent');
			  $tmpl->parse($data['parent_doc_row'], 'parent_doc_row', true);
			  $this->tables_relations_fields($item['ID'], 'CLAUSE', $data, $tmpl, $depth + 2);
			  }
			  } else */
			// Veika
			if ($field['TYPE'] === '23') {
				$deed_groups = array(
					0 => array('ID' => 0, 'TYPE' => 'ACT', 'LABEL' => 'Punktas'),
					1 => array('ID' => 0, 'TYPE' => 'CLAUSE', 'LABEL' => 'Straipsnis')
				);

				foreach ($deed_groups as $key => &$item) {

					$tmpl->set_var(array(
						'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['LABEL'],
						$data['parent_doc_data'] => ''));
					$tmpl->parse($data['parent_doc_row_parent'], 'parent_doc_row_parent');
					$tmpl->parse($data['parent_doc_row'], 'parent_doc_row', true);
					$this->tables_relations_fields(0, $item['TYPE'], $data, $tmpl, $depth + 2);
				}
			}
		}
	}

	/**
	 * <b>(Dokumentų šablonas)</b> Spausdina tėvinio dokumento kinatmuosius ir/arba garžina rastus laukus
	 * @author Justinas Malūkas
	 * @param int $id Įrašo ID
	 * @param string $type Įrašo tipas
	 * @param array $data Papildomi duomenys
	 * @param array $all_fields Kintamasis į kurį talpins rastus laukus
	 * @param object $tmpl [optional] Šablonų valdymo objektas
	 * @param int $depth Lauko gylis
	 * @return array Laukų hierarchijos masyvas<br/>
	  <pre>array(
	  array(
	  'FIELD' => array( # Laukas
	  'ID' => '',
	  'LABEL' => '',
	  'TYPE' => ''
	  ),
	  'CHILDREN' => array( # Lauko vaikai
	  array(
	  'FIELD' => array( # Laukas
	  'ID' => '', // lauko ID
	  'LABEL' => '', // lauko pavadinimas
	  'TYPE' => '', // lauko tipo ID
	  ),
	  'CHILDREN' => array( # Laukao vaikai
	 * RECURSION*
	  )
	  ),
	  array(
	  'GROUP' => array( # Grupė
	  'ID' => '', // grupės ID
	  'LABEL' => '', // grupės pavadinimas
	  ),
	  'CHILDREN' => array( # Gupės vaikai
	  array(
	  'FIELD' => array( # Laukas
	  'ID' => '', // lauko ID
	  'LABEL' => '', // lauko pavadinimas
	  'TYPE' => '', // lauko tipo ID
	  ),
	  'CHILDREN' => array( # Lauko vaikai
	 * RECURSION*
	  )
	  ),
	  array(
	  'FIELD' => array( # Laukas
	  'ID' => '', // lauko ID
	  'LABEL' => '', // lauko pavadinimas
	  'TYPE' => '', // lauko tipo ID
	  )
	  )
	  )
	  )
	  )
	  )
	  );</pre>
	 */
	//public function tables_relations_parent_fields($id, $type, $data, &$all_fields, &$tmpl = null, $depth = 0) {
	public function tables_relations_parent_fields($id, $type, &$data, &$all_fields = array(), /* &$select_hierarchy, */ &$tmpl = null, $depth = 0) {
		// TODO: ACT, jei reikės (turėtu) Veiką plečiančių laukų
		if (in_array($type, array_keys($this->core->_prefix->item), true) === FALSE) {
			trigger_error('Unknown item type', E_USER_WARNING);
			return false;
		}

		if (empty($tmpl) === FALSE) {
			$data['multiplier'] = (isset($data['multiplier']) === TRUE ? (int) $data['multiplier'] : 3);
		}

		// Gauna įrašo laukus
		if ($type === 'WS') {
			$fields = array($this->logic2->get_fields(array('ITYPE' => $type, 'ID' => $id)));
		} else
			$fields = $this->logic2->get_fields(array('ITYPE' => $type, 'ITEM_ID' => $id), false, '`ORD` ASC');

		//$i = 0;
		//$fields_arr = array();
		$select_hierarchy = array();
		foreach ($fields as &$field) {
			if (empty($field) === TRUE)
				continue;

			$all_fields[$field['ID']] = $field;
			$select_hierarchy[$field['ID']]['OPT'] = $field['LABEL'];

			// Lauko duomenys
			if (empty($tmpl) === FALSE) {
				$tmpl->set_var(array(
					'label' => str_repeat('&nbsp;', $data['multiplier'] * $depth) . $field['LABEL'],
					'type' => $data['field_type'][$field['TYPE']]['LABEL'],
					$data['doc_row_parent'] => ''));
				$tmpl->parse($data['doc_data'], 'doc_data');
				$tmpl->parse($data['doc_row'], 'doc_row', true);
			}
			// Klasifikatorius
			if ($field['TYPE'] === '18') {
				// TODO: gauna tik pirmo lygio klasifikatorius, reikia visų?
				//$items = $this->logic->get_classification(null, $field["SOURCE"]);
				$items = $this->logic2->get_classificator(array('PARENT_ID' => $field["SOURCE"]));
				$j = 0;
				foreach ($items as &$item) {
					$select_hierarchy[$field['ID']]['GROUP'][$item['ID']]['OPT'] = $item['LABEL'];
					if (empty($tmpl) === FALSE) {
						$tmpl->set_var(array(
							'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['LABEL'],
							$data['doc_data'] => ''));
						$tmpl->parse($data['doc_row_parent'], 'doc_row_parent');
						$tmpl->parse($data['doc_row'], 'doc_row', true);
					}

					$select_hierarchy[$field['ID']]['GROUP'][$item['ID']]['CHILDREN'] = $this->tables_relations_parent_fields($item['ID'], 'CLASS', $data, $all_fields, /* $select_hierarchy, */ $tmpl, $depth + 2);

					$j++;
				}
			} else
			// Straipsnis
			if ($field['SORT'] === '6') {
				$items = $this->logic2->get_clause();
				$j = 0;

				foreach ($items as &$item) {
					$select_hierarchy[$field['ID']]['GROUP'][$item['ID']]['OPT'] = $item['NAME'];

					if (empty($tmpl) === FALSE) {
						$tmpl->set_var(array(
							'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['NAME'],
							$data['doc_data'] => ''));
						$tmpl->parse($data['doc_row_parent'], 'doc_row_parent');
						$tmpl->parse($data['doc_row'], 'doc_row', true);
					}

					$select_hierarchy[$field['ID']]['GROUP'][$item['ID']]['CHILDREN'] = $this->tables_relations_parent_fields($item['ID'], 'CLAUSE', $data, $all_fields, /* $select_hierarchy, */ $tmpl, $depth + 2);

					$j++;
				}
			} else
			// Veika
			if ($field['TYPE'] === '23') {
				$deed_groups = array(
					0 => array('ID' => 0, 'TYPE' => 'ACT', 'LABEL' => 'Punktas'),
					1 => array('ID' => 0, 'TYPE' => 'CLAUSE', 'LABEL' => 'Straipsnis')
				);

				foreach ($deed_groups as $key => &$arr) {
					$select_hierarchy[$field['ID']]['GROUP'][$key]['OPT'] = $arr['LABEL'];
					if (empty($tmpl) === FALSE) {
						$tmpl->set_var(array(
							'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $arr['LABEL'],
							$data['doc_data'] => ''));
						$tmpl->parse($data['doc_row_parent'], 'doc_row_parent');
						$tmpl->parse($data['doc_row'], 'doc_row', true);
					}

					$select_hierarchy[$field['ID']]['GROUP'][$key]['CHILDREN'] = $this->tables_relations_parent_fields(0, $arr['TYPE'], $data, $all_fields, /* $select_hierarchy, */ $tmpl, $depth + 2);
				}
			}
			if ($field['WS'] === '1') {
				$h = &$select_hierarchy[$field['ID']]['GROUP'][$field['ID']];
				//$h = &$select_hierarchy[$field['ID']]['GROUP'][0];
				$h['OPT'] = 'Web-servisai';

				if (empty($tmpl) === FALSE) {
					$tmpl->set_var(array(
						'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . 'Web-servisai',
						$data['doc_data'] => ''));
					$tmpl->parse($data['doc_row_parent'], 'doc_row_parent');
					$tmpl->parse($data['doc_row'], 'doc_row', true);
				}

				$ws = $this->logic2->get_ws();
				$relations = $this->logic2->get_webservices_fields(array('FIELD_ID' => $field['ID']));
				$fields_struct = $this->logic2->get_fields(array('ITYPE' => 'WS', 'ITEM_ID' => $field['ID']), false, '`ORD` ASC');

				foreach ($relations as &$relation) {
					$info = $ws->get_info($relation['WS_NAME']);
					if (empty($info))
						continue;

					$h['GROUP'][$info['name']]['OPT'] = $info['label'];

					if (empty($tmpl) === FALSE) {
						$tmpl->set_var(array(
							'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 2)) . $info['label'],
							$data['doc_data'] => ''));
						$tmpl->parse($data['doc_row_parent'], 'doc_row_parent');
						$tmpl->parse($data['doc_row'], 'doc_row', true);
					}

					$ws_fields = $this->logic2->get_webservices(array('WS_NAME' => $relation['WS_NAME'], 'PARENT_FIELD_ID' => $field['ID']));

					$children = array();
					foreach ($ws_fields as $ws_field) {
						$arr = $fields_struct[$ws_field['FIELD_ID']];
						$ch = $this->tables_relations_parent_fields($arr['ID'], 'WS', $data, $all_fields, $tmpl, $depth + 3);
						foreach ($ch as $k => $c) {
							$h['GROUP'][$info['name']]['CHILDREN'][$k] = $c;
						}
					}
				}
			}

			//$i++;
		}

		return $select_hierarchy;
	}

	/**
	 * Kompetencijų lentelė straipsnių prijungimui prie vartotojo / skyriaus vartotojų
	 * @param int $user_id
	 * @param int $department_id
	 * @return string
	 */
	public function table_user_competence($user_id, $department_id) {
		$html_manage = '';

		if (empty($user_id) === TRUE && empty($department_id) === TRUE)
			return $html_manage;

		$clauses = $clauses = $this->logic2->get_clause();
		$user_clauses = $this->logic->get_user_clauses(array('PEOPLE_ID' => $user_id, 'STRUCTURE_ID' => $department_id));

		$tpl_arr = $this->core->getTemplate('table');
		$columns = array(
			'NAME' => $this->lng('atp_clause_clause'),
			'ADD' => $this->lng('atp_table_user_competence_add')
		);

		// table
		$table = array();
		$table['table_class'] = 'user_competences';
		$html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

		// header
		$header = array('row_html' => '');
		$header['row_class'] = 'table-row table-header';
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);

		foreach ($columns as $col_name => $col) {
			// cell
			$cell = array('cell_html' => '');
			$cell['cell_class'] = 'table-cell cell-' . $col_name;
			$cell['cell_content'] = $col;
			$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
		}
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

		foreach ($clauses as $key => $clause) {

			// row
			$row = array('row_html' => '', 'row_class' => '');
			$row['row_class'][] = 'table-row';
			$row['row_class'] = join(' ', $row['row_class']);
			$html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

			foreach ($columns as $col_name => $col) {
				// cell
				$cell = array('cell_html' => '', 'cell_content' => '');
				$cell['cell_class'] = 'table-cell cell-' . $col_name;
				if ($col_name === 'ADD') {
					$cell['cell_content'] .= '<input type="checkbox" name="clauses[]" value="' . $clause['ID'] . '"'
							. (isset($user_clauses[$clause['ID']]) ? ' checked="checked"' : '') . '/>';
				} else
					$cell['cell_content'] .= $clause[$col_name];
				$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
			}

			$html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
		}

		$html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

		return $html_manage;
	}

	/**
	 * Sugeneruoja teisės aktų lentelę
	 * @return string
	 */
	public function table_acts() {
		$html_manage = '';

		// Surenka klasifikatorius
		$tables = $this->logic2->get_acts();

		// Surenkami papildomi duomenys apie įrašus
		foreach ($tables as $table) {
			$new_tables[$table['ID']] = $table;

			// Surenka galimus veiksmus
			$new_tables[$table['ID']]['ACTIONS'] = array();
			$new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="expand_collapse_block">
					<div class="expand_collapse_img_block">
						<img style="cursor: pointer;" src="' . GLOBAL_SITE_URL . 'images/atp/expand_doc_action.png" width="29" height="29">
					</div>
					<div class="actions_block" style="display: none;">
						<img style="display: block;" src="' . GLOBAL_SITE_URL . 'actions/top_img_actions_buttons.png" width="132" height="8">
						<div class="actions_buttons">';
			$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button addChild"><span>Pridėti vaiką</span></div>';
			$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button remove"><span>Trinti</span></div>';
			// Išjungta
			/* $new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button addFields"><span>Sukurti laukus</span></div>'; */
			$new_tables[$table['ID']]['ACTIONS'][] = '
						</div>
					</div>
				</div>';
			$new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="action_label">'/*
					  '<div class="atp-html-button edit"><span>' . $this->lng('edit') . '</span></div>' */ .
					'<a class="atp-html-button edit dialog" href="index.php?m=120&id=' . $table['ID'] . '&action=editform">' . $this->lng('edit') . '</a>
				</div>';

			$new_tables[$table['ID']]['ACTIONS'] = join(' ', $new_tables[$table['ID']]['ACTIONS']);

			// Gaunamas įrašo lygis
			$new_tables[$table['ID']]['LEVEL'] = $this->logic->get_table_level($tables, $table['ID']);
			// Nurodomas išskleidimo stulpelis, jei laukas turi vaikų
			if ($this->logic->table_has_children($tables, $table['ID'])) {
				$new_tables[$table['ID']]['TOGGLE'] = '<div class="collapse atp-table-list-toggle"></div>';
				$toggle_class = 'collapse';
			} else
				$toggle_class = 'none';

			$new_tables[$table['ID']]['LABEL'] = '<div class="cell-LABEL-container"><div class="' . $toggle_class . ' atp-table-list-toggle"></div><span>' . $new_tables[$table['ID']]['LABEL'] . '</span></div>';
		}
		$tables = &$new_tables;

		// Įrašai surikiuojami pagal hierarchiją (tevas -> vaikai, tevas -> vaikai)
		$tables = $this->logic->sort_children_to_parent($tables, 'ID', 'PARENT_ID');

		$tpl_arr = $this->core->getTemplate('table2');

		// Nurodomi išvedami lentelės stulpeliai
		$columns = array(
			/* 'TOGGLE' => $this->lng('atp_table_toggle'), */
			'NR' => $this->lng('atp_act_number'),
			'LABEL' => $this->lng('atp_act_label'),
			'VALID_FROM' => $this->lng('atp_act_valid_from'),
			'VALID_TILL' => $this->lng('atp_act_valid_till')
		);
		// Tikrinama teisė į veiksmų stulpelio matymą
		if ($this->core->user->status['A_ACT'] > 1)
			$columns['ACTIONS'] = $this->lng('atp_table_actions');

		// table
		$table = array();
		$table['table_class'] = 'acts';
		$html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

		// Lentelės antraštė
		$header = array('row_html' => '');
		$header['row_class'] = 'div-row div-header';
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);
		foreach ($columns as $col_name => $col) {
			// cell
			$cell = array('cell_html' => '');
			$cell['cell_class'] = 'div-cell cell-' . $col_name;
			$cell['cell_content'] = $col;
			$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
		}
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

		// Lentelės įrašų eilutės
		foreach ($tables as $key => $table) {

			// Eilutės duomenys
			$row = array();
			$row['row_html'] = 'id="atp-table-list-item-id-' . $table['ID'] . '"';
			$row['row_class'][] = 'div-row';
			if ($table['LEVEL'])
				$row['row_class'][] = 'child';
			$row['row_class'][] = 'level-' . $table['LEVEL'];
			$row['row_class'][] = 'child-of-' . $table['PARENT_ID'];
			$row['row_class'] = join(' ', $row['row_class']);
			$html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

			// Eilutės stulpeiai
			foreach ($columns as $col_name => $col) {

				// Stulpelio duomenys
				$cell = array('cell_html' => '', 'cell_content' => '');
				if (in_array($col_name, array('VALID_FROM', 'VALID_TILL'))) {
					$table[$col_name] = date('Y-m-d', strtotime($table[$col_name]));
				}
				$cell['cell_class'] = 'div-cell cell-' . $col_name;
				$cell['cell_content'] .= $table[$col_name];
				$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
			}

			$html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
		}

		$html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

		return $html_manage;
	}

	function appendHTML(DOMNode $parent, $source) {
		$dom = new DOMDocument('1.0', 'UTF-8');
		$source = mb_convert_encoding($source, 'HTML-ENTITIES', 'UTF-8');
		@$dom->loadHTML($source);
		foreach ($dom->getElementsByTagName('body')->item(0)->childNodes as $node) {
			$importedNode = $parent->ownerDocument->importNode($node, TRUE);
			$parent->appendChild($importedNode);
		}
	}

	/**
	 * Formuoja dokumento - atp būsenų - būsenų ryšio lentelę
	 * @return string
	 */
	public function table_status_relations() {
		$html_manage = '';

		// Sukurti ryšiai
		$relations = $this->logic2->get_status_relations();

		// Dokumentų informacija
		$documents = $this->logic2->get_document(null, null, 'LABEL ASC');
		// Būsenų informacija
		$status = $this->logic2->get_status(null, null, 'LABEL ASC');
		// ATP būsenų informacija
		$atp_status = $this->logic->get_document_statuses();

		// ATP būsenos abecelės tvarka
		usort($atp_status, array($this->logic, 'sort_atp_status'));

		// Atskato ATP būsenų indeksus
		$atp_status_tmp = array();
		foreach ($atp_status as &$arr) {
			$atp_status_tmp[$arr['ID']] = $arr;
		}
		$atp_status = &$atp_status_tmp;

		// Perfomuoja ryšių masyvą. DOCUMENT_NAME ASC, ATP_SATUS_NAME ASC, STATUS_NAME ASC
		$md_relation = array();
		foreach ($relations as &$relation) {
			$i = array_search($relation['DOCUMENT_ID'], array_keys($documents));
			$j = array_search($relation['ATP_STATUS_ID'], array_keys($atp_status));
			$k = array_search($relation['STATUS_ID'], array_keys($status));

			$md_relation[$i]['ID'] = $relation['DOCUMENT_ID'];
			$md_relation[$i]['LABEL'] = $documents[$relation['DOCUMENT_ID']]['LABEL'];
			$md_relation[$i]['CHILDREN'][$j]['ID'] = $relation['ATP_STATUS_ID'];
			$md_relation[$i]['CHILDREN'][$j]['LABEL'] = $atp_status[$j]['LABEL'];
			$md_relation[$i]['CHILDREN'][$j]['CHILDREN'][$k] = $relation['ID'];
		}
		ksort($md_relation);
		foreach ($md_relation as &$relation) {
			foreach ($relation['CHILDREN'] as &$a) {
				ksort($a['CHILDREN']);
			}
			ksort($relation['CHILDREN']);
		}
		$ordered_relations = array();
		foreach ($md_relation as &$relation) {
			foreach ($relation['CHILDREN'] as &$a) {
				foreach ($a['CHILDREN'] as &$b) {
					$ordered_relations[] = $relations[$b];
				}
			}
		}

		// Lentelės spausdinimas
		$current_document = '';
		$current_atp_status = '';

		$document_row_count = 1;
		$atp_status_row_count = 1;

		$dom = new DOMDocument('1.0', 'UTF-8');

		$table = $dom->createElement('table');
		$table->setAttribute('border', 1);
		$table->setAttribute('id', 'table-table');
		$table->setAttribute('class', 'gridtable');
		$dom->appendChild($table);

		$action = '
			<div class="expand_collapse_block">
				<div class="expand_collapse_img_block">
					<img style="cursor: pointer;" src="' . GLOBAL_SITE_URL . 'images/atp/expand_doc_action.png" width="29" height="29">
				</div>
				<div class="actions_block" style="display: none;">
					<img style="display: block;" src="' . GLOBAL_SITE_URL . 'images/actions/top_img_actions_buttons.png" width="132" height="8">
					<div class="actions_buttons">
						<div class="atp-html-button remove"><span>' . $this->lng('delete') . '</span></div>
					</div>
				</div>
			</div>
			<div class="action_label">
				<a class="atp-html-button edit dialog" href="index.php?m=126&id=[doc_id]&action=editrelation">' . $this->lng('edit') . '</a>
			</div>';

		$this->appendHTML($table, '<tr class="table-row table-header"><td class="table-cell cell-DOCUMENT">Dokumentas</td>' . /* '<td class="table-cell cell-ATP_STATUS">ATP būsena</td>'. */'<td class="table-cell cell-STATUS">Būsena</td><td class="table-cell cell-ACTIONS">Veiksmai</td></tr>');

		foreach ($ordered_relations as $relation) {
			$row = $dom->createElement('tr');
			$row->setAttribute('class', 'table-row');
			$table->appendChild($row);

			if ($current_document !== $relation['DOCUMENT_ID']) {
				$current_document = $relation['DOCUMENT_ID'];
				$document_row_count = 1;

				$doc_col = $dom->createElement('td', $documents[$relation['DOCUMENT_ID']]['LABEL']);
				$doc_col->setAttribute('class', 'table-cell cell-DOCUMENT');
				$action_col = $dom->createElement('td');
				$action_col->setAttribute('class', 'table-cell cell-ACTIONS');
				$action_col->setAttribute('for', $relation['DOCUMENT_ID']);
				$this->appendHTML($action_col, str_replace('[doc_id]', $relation['DOCUMENT_ID'], $action));
				$row->appendChild($doc_col);
			} else {
				$doc_col->setAttribute('rowspan', ++$document_row_count);
				$action_col->setAttribute('rowspan', $document_row_count);
			}
			/*
			  if ($current_atp_status !== $relation['ATP_STATUS_ID']) {
			  $current_atp_status = $relation['ATP_STATUS_ID'];
			  $atp_status_row_count = 1;

			  $atp_col = $dom->createElement('td', $atp_status[$relation['ATP_STATUS_ID']]['LABEL']);
			  $atp_col->setAttribute('class', 'table-cell cell-ATP_STATUS');
			  $row->appendChild($atp_col);
			  } else {
			  $atp_col->setAttribute('rowspan', ++$atp_status_row_count);
			  } */
			$status_col = $dom->createElement('td', $status[$relation['STATUS_ID']]['LABEL']);
			$status_col->setAttribute('class', 'table-cell cell-STATUS');
			$row->appendChild($status_col);

			if ($document_row_count === 1)
				$row->appendChild($action_col);
		}

		return $html_manage = $dom->saveHTML();
	}

	public function table_status_list() {
		$html_manage = '';

		// Surenka klasifikatorius
		$tables = $this->logic2->get_status(null, null, 'LABEL ASC');

		// Surenkami papildomi duomenys apie įrašus
		foreach ($tables as $table) {
			$new_tables[$table['ID']] = $table;

			// Surenka galimus veiksmus
			$new_tables[$table['ID']]['ACTIONS'] = array();
			$new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="expand_collapse_block">
					<div class="expand_collapse_img_block">
						<img style="cursor: pointer;" src="' . GLOBAL_SITE_URL . 'images/atp/expand_doc_action.png" width="29" height="29">
					</div>
					<div class="actions_block" style="display: none;">
						<img style="display: block;" src="' . GLOBAL_SITE_URL . 'images/actions/top_img_actions_buttons.png" width="132" height="8">
						<div class="actions_buttons">';
			$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button remove"><span>Trinti</span></div>';
			$new_tables[$table['ID']]['ACTIONS'][] = '
						</div>
					</div>
				</div>';
			$new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="action_label">'/*
					  '<div class="atp-html-button edit"><span>' . $this->lng('edit') . '</span></div>' */ .
					'<a class="atp-html-button edit dialog" href="index.php?m=126&id=' . $table['ID'] . '&action=editstatus">' . $this->lng('edit') . '</a>
				</div>';

			$new_tables[$table['ID']]['ACTIONS'] = join(' ', $new_tables[$table['ID']]['ACTIONS']);

			// Gaunamas įrašo lygis
			$new_tables[$table['ID']]['LEVEL'] = $this->logic->get_table_level($tables, $table['ID']);
			// Nurodomas išskleidimo stulpelis, jei laukas turi vaikų
			if ($this->logic->table_has_children($tables, $table['ID'])) {
				$new_tables[$table['ID']]['TOGGLE'] = '<div class="collapse atp-table-list-toggle"></div>';
				$toggle_class = 'collapse';
			} else
				$toggle_class = 'none';

			$new_tables[$table['ID']]['LABEL'] = '<div class="cell-LABEL-container"><div class="' . $toggle_class . ' atp-table-list-toggle"></div><span>' . $new_tables[$table['ID']]['LABEL'] . '</span></div>';
		}
		$tables = &$new_tables;

		// Įrašai surikiuojami pagal hierarchiją (tevas -> vaikai, tevas -> vaikai)
		$tables = $this->logic->sort_children_to_parent($tables, 'ID', 'PARENT_ID');

		$tpl_arr = $this->core->getTemplate('table2');

		// Nurodomi išvedami lentelės stulpeliai
		$columns = array(
			'LABEL' => $this->lng('atp_act_label'),
		);
		// Tikrinama teisė į veiksmų stulpelio matymą
		if ($this->core->user->status['A_ACT'] > 1)
			$columns['ACTIONS'] = $this->lng('atp_table_actions');

		// table
		$table = array();
		$table['table_class'] = 'status';
		$html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

		// Lentelės antraštė
		$header = array('row_html' => '');
		$header['row_class'] = 'div-row div-header';
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);
		foreach ($columns as $col_name => $col) {
			// cell
			$cell = array('cell_html' => '');
			$cell['cell_class'] = 'div-cell cell-' . $col_name;
			$cell['cell_content'] = $col;
			$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
		}
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

		// Lentelės įrašų eilutės
		foreach ($tables as $key => $table) {

			// Eilutės duomenys
			$row = array();
			$row['row_html'] = 'id="atp-table-list-item-id-' . $table['ID'] . '"';
			$row['row_class'][] = 'div-row';
			if ($table['LEVEL'])
				$row['row_class'][] = 'child';
			$row['row_class'][] = 'level-' . $table['LEVEL'];
			$row['row_class'][] = 'child-of-' . $table['PARENT_ID'];
			$row['row_class'] = join(' ', $row['row_class']);
			$html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

			// Eilutės stulpeiai
			foreach ($columns as $col_name => $col) {

				// Stulpelio duomenys
				$cell = array('cell_html' => '', 'cell_content' => '');
				$cell['cell_class'] = 'div-cell cell-' . $col_name;
				$cell['cell_content'] .= $table[$col_name];
				$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
			}

			$html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
		}

		$html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

		return $html_manage;
	}

	public function table_search_list($data) {
		// Puslapiavimas
		include_once($this->core->config['REAL_URL'] . 'helper/paginator.php');
		$pg = new Paginator();
		$pg->current_page = empty($_GET['_page']) ? 1 : $_GET['_page'];
		$pg->items_total = count($data);
		$pg->items_per_page = 10;
		$pg->paginate();

		// Lentelės šablonas
		$tpl_arr = $this->core->getTemplate('table');

		// Lentelė
		$html_manage = $this->core->returnHTML(array('table_class' => 'search_fields'), $tpl_arr[0]);

		// Lentelės antraštė
		$html_manage .= $this->core->returnHTML(array('row_html' => '', 'row_class' => 'div-row div-header'), $tpl_arr[1]);

		// Lentelės stulpeliai
		$columns = array(
			'DATE' => $this->lng('atp_search_item_date') . 'Data, Nr.',
			'TYPE' => $this->lng('atp_search_item_type') . 'Tipas',
			'OFFICER' => $this->lng('atp_search_item_officer') . 'Pareigūnas',
			'STATUS' => $this->lng('atp_search_item_status') . 'Būsena',
		);
		foreach ($columns as $col_name => $col) {
			// Lentelės langelis
			$html_manage .= $this->core->returnHTML(array(
				'cell_html' => '',
				'cell_class' => 'div-cell cell-' . $col_name,
				'cell_content' => $col), $tpl_arr[2]);
		}
		$html_manage .= $tpl_arr[3];


		if ($pg->items_total) {
			// Visų dokumentų duomenys
			$documents = $this->logic2->get_document();

			// Lentelės įrašų eilutės
			foreach ($data as $key => $record) {
				// Pagal puslapiavimą apribojamas duomenų kiekis
				if ($key < $pg->limit_start)
					continue;
				else if ($key >= $pg->limit_end)
					break;

				// Įrašo dokumentas
				list($doc_id) = explode('p', $record);
				// Įrašo dokumento duomenys
				$document = &$documents[$doc_id];
				// Įrašo duoemnys
				$record = $this->logic->get_table_record($record, 'tn.*,rr.M_PEOPLE_ID,rr.M_DEPARTMENT_ID,rr.M_PEOPLE_NAME,rr.M_PEOPLE_SURNAME,rr.STATUS, rr.ID RID');

				// Lentelės eilutė
				$row = array();
				$row['row_html'] = '';
				$row['row_class'][] = 'div-row';
				$row['row_class'] = join(' ', $row['row_class']);
				$html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

				// Eilutės stulpeiai
				foreach ($columns as $col_name => $col) {

					// Stulpelio duomenys
					$cell = array(
						'cell_html' => '',
						'cell_content' => '',
						'cell_class' => 'div-cell cell-' . $col_name);
					$url = array(
						'<a href="index.php?m=5&id=' . $record['RID'] . '">',
						'</a>'
					);
					if ($col_name === 'DATE') {
						$cell['cell_content'] .= $url[0] . date('Y-m-d', strtotime($record['CREATE_DATE'])) . $url[1] . '<br/>' . $url[0] . $record['RECORD_ID'] . $url[1];
					} elseif ($col_name === 'TYPE') {
						$cell['cell_content'] .= $url[0] . $document['LABEL'] . $url[1];
					} elseif ($col_name === 'OFFICER') {
						$cell['cell_content'] .= $url[0] . join(' ', array_filter(array($record['M_PEOPLE_NAME'], $record['M_PEOPLE_SURNAME']))) . $url[1];
					} elseif ($col_name === 'STATUS') {
						$status = $this->logic->find_document_status(array('ID' => $record['STATUS']));
						$cell['cell_content'] .= '<div class="status_label"><span class="document_status-' . $status['ID'] . ' document_status-' . $status['SHORT'] . '">' . $url[0] . $status['LABEL'] . $url[1] . '</span></div>';
					}
					$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
				}

				$html_manage .= $tpl_arr[3];
			}
		}

		// Lentelės pabaiga
		$html_manage .= $tpl_arr[4];

		// Puslapiai
		$html_manage .= $pg->display_pages();

		return $html_manage;
	}

	public function table_reports_list() {
		$html_manage = '';
		/*
		  $total = count($data);
		  $per_page = 10;
		  $page = empty($page) ? 1 : $_GET['_page'];

		  include_once($this->core->config['REAL_URL'] . 'helper/paginator.php');
		  $pg = new Paginator();
		  $pg->current_page = $page;
		  $pg->items_total = $total;
		  $pg->items_per_page = $per_page;
		  $pg->paginate(); */

		$tpl_arr = $this->core->getTemplate('table');

		// Nurodomi išvedami lentelės stulpeliai
		$columns = array(
			'NAME' => $this->lng('') . 'Pavadinimas',
			'DATE_FROM' => $this->lng('') . 'Data nuo',
			'DATE_TILL' => $this->lng('') . 'Data iki',
			'GENERATE' => $this->lng('') . 'Generuoti ataskaitą'
		);

		// table
		$table = array();
		$table['table_class'] = 'search_fields';
		$html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

		// Lentelės antraštė
		$header = array('row_html' => '');
		$header['row_class'] = 'div-row div-header';
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);
		foreach ($columns as $col_name => $col) {
			// cell
			$cell = array('cell_html' => '');
			$cell['cell_class'] = 'div-cell cell-' . $col_name;
			$cell['cell_content'] = $col;
			$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
		}
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

		$data = $this->logic2->get_reports(array('PEOPLE_ID' => $this->core->user->id, 'DEPARTMENT_ID' => $this->core->user->department->id));
		if (count($data)) {

			$dparams = array(
				'm' => 19,
				'action' => 'download'
			);
			// Lentelės įrašų eilutės
			foreach ($data as $key => $report) {
				$dparams['id'] = $report['ID'];
				$durl = 'index.php?' . http_build_query($dparams);

				// Eilutės duomenys
				$row = array();
				$row['row_html'] = '';
				$row['row_class'][] = 'div-row';
				$row['row_class'] = join(' ', $row['row_class']);
				$html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);
				// Eilutės stulpeiai
				foreach ($columns as $col_name => $col) {

					// Stulpelio duomenys
					$cell = array('cell_html' => '', 'cell_content' => '');
					$cell['cell_class'] = 'div-cell cell-' . $col_name;
					if ($col_name === 'DATE_FROM') {
						$cell['cell_content'] .= '<input type="text" class="atp-date from atp-input">';
					} else if ($col_name === 'DATE_TILL') {
						$cell['cell_content'] .= '<input type="text" class="atp-date till atp-input">';
					} elseif ($col_name === 'GENERATE') {
						/* $cell['cell_content'] .=
						  '<span class="icon xls"><img src="' . GLOBAL_SITE_URL . 'images/icons/xls.gif" alt="XLS" title="XLS"/></span>
						  <span class="icon doc"><img src="' . GLOBAL_SITE_URL . 'images/icons/doc.gif" alt="DOC" title="DOC"/></span>
						  <span class="icon pdf"><img src="' . GLOBAL_SITE_URL . 'images/icons/pdf.gif" alt="PDF" title="PDF"/></span>'; */
						$cell['cell_content'] .=
								'<span class="icon" title="Generuoti CVS failą"><a href="' . $durl . '&type=xml&from=&till="><div class="xls"></div></a></span>
								<span class="icon" title="Generuoti DOC failą"><a href="' . $durl . '&type=doc&from=&till="><div class="doc"></div></a></span>
								<span class="icon" title="Generuoti PDF failą"><a href="' . $durl . '&type=pdf&from=&till="><div class="pdf"></div></a></span>';
					} else {
						$cell['cell_content'] .= $report[$col_name];
					}
					$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
				}

				$html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
			}
		}

		$html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);
		//$html_manage .=  $pg->display_pages();

		return $html_manage;
	}

	/**
	 * Sugeneruoja paieškos laukų lentelę
	 * @return string
	 */
	public function table_fields() {
		$html_manage = '';

		// Surenka klasifikatorius
		$tables = $this->logic2->get_search_field(array('VALID' => 1), false, null, 'ID, NAME LABEL');

		// Surenkami papildomi duomenys apie įrašus
		foreach ($tables as $table) {
			$new_tables[$table['ID']] = $table;

			// Surenka galimus veiksmus
			$new_tables[$table['ID']]['ACTIONS'] = array();
			$new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="expand_collapse_block">
					<div class="expand_collapse_img_block">
						<img style="cursor: pointer;" src="' . GLOBAL_SITE_URL . 'images/atp/expand_doc_action.png" width="29" height="29">
					</div>
					<div class="actions_block" style="display: none;">
						<img style="display: block;" src="' . GLOBAL_SITE_URL . 'images/actions/top_img_actions_buttons.png" width="132" height="8">
						<div class="actions_buttons">';
			$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button remove"><span>Trinti</span></div>';
			$new_tables[$table['ID']]['ACTIONS'][] = '
						</div>
					</div>
				</div>';
			$new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="action_label">'/*
					  '<div class="atp-html-button edit"><span>' . $this->lng('edit') . '</span></div>' */ .
					'<a class="atp-html-button edit dialog" href="index.php?m=125&id=' . $table['ID'] . '&action=editform">' . $this->lng('edit') . '</a>
				</div>';

			$new_tables[$table['ID']]['ACTIONS'] = join(' ', $new_tables[$table['ID']]['ACTIONS']);

			// Gaunamas įrašo lygis
			//$new_tables[$table['ID']]['LEVEL'] = $this->logic->get_table_level($tables, $table['ID']);
			// Nurodomas išskleidimo stulpelis, jei laukas turi vaikų
			//if ($this->logic->table_has_children($tables, $table['ID'])) {
			//	$new_tables[$table['ID']]['TOGGLE'] = '<div class="collapse atp-table-list-toggle"></div>';
			//	$toggle_class = 'collapse';
			//} else
			$toggle_class = 'none';

			$new_tables[$table['ID']]['LABEL'] = '<div class="cell-LABEL-container"><div class="' . $toggle_class . ' atp-table-list-toggle"></div><span>' . $new_tables[$table['ID']]['LABEL'] . '</span></div>';
		}
		$tables = &$new_tables;

		// Įrašai surikiuojami pagal hierarchiją (tevas -> vaikai, tevas -> vaikai)
		//$tables = $this->logic->sort_children_to_parent($tables, 'ID', 'PARENT_ID');

		$tpl_arr = $this->core->getTemplate('table');

		// Nurodomi išvedami lentelės stulpeliai
		$columns = array(
			/* 'TOGGLE' => $this->lng('atp_table_toggle'), */
			'LABEL' => $this->lng('atp_act_label')
		);
		// Tikrinama teisė į veiksmų stulpelio matymą
		if ($this->core->user->status['A_TABLE'] > 1)
			$columns['ACTIONS'] = $this->lng('atp_table_actions');

		// table
		$table = array();
		$table['table_class'] = 'search_fields';
		$html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

		// Lentelės antraštė
		$header = array('row_html' => '');
		$header['row_class'] = 'div-row div-header';
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);
		foreach ($columns as $col_name => $col) {
			// cell
			$cell = array('cell_html' => '');
			$cell['cell_class'] = 'div-cell cell-' . $col_name;
			$cell['cell_content'] = $col;
			$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
		}
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

		if (count($tables))
		// Lentelės įrašų eilutės
			foreach ($tables as $key => $table) {

				// Eilutės duomenys
				$row = array();
				$row['row_html'] = 'id="atp-table-list-item-id-' . $table['ID'] . '"';
				$row['row_class'][] = 'div-row';
				//if ($table['LEVEL'])
				//	$row['row_class'][] = 'child';
				//$row['row_class'][] = 'level-' . $table['LEVEL'];
				//$row['row_class'][] = 'child-of-' . $table['PARENT_ID'];
				$row['row_class'] = join(' ', $row['row_class']);
				$html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

				// Eilutės stulpeiai
				foreach ($columns as $col_name => $col) {

					// Stulpelio duomenys
					$cell = array('cell_html' => '', 'cell_content' => '');
					$cell['cell_class'] = 'div-cell cell-' . $col_name;
					$cell['cell_content'] .= $table[$col_name];
					$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
				}

				$html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
			}

		$html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

		return $html_manage;
	}

	/**
	 * Sugeneruoja veikų lentelę
	 * @return string
	 */
	public function table_deeds() {
		$html_manage = '';

		// Surenka klasifikatorius
		$tables = $this->logic->get_deeds();

		// Surenkami papildomi duomenys apie įrašus
		foreach ($tables as $table) {
			$new_tables[$table['ID']] = $table;

			// Surenka galimus veiksmus
			$new_tables[$table['ID']]['ACTIONS'] = array();
			$new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="expand_collapse_block">
					<div class="expand_collapse_img_block">
						<img style="cursor: pointer;" src="' . GLOBAL_SITE_URL . 'images/atp/expand_doc_action.png" width="29" height="29">
					</div>
					<div class="actions_block" style="display: none;">
						<img style="display: block;" src="' . GLOBAL_SITE_URL . 'images/actions/top_img_actions_buttons.png" width="132" height="8">
						<div class="actions_buttons">';
			$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button addChild"><span>Pridėti vaiką</span></div>';
			$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button remove"><span>Trinti</span></div>';
			$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button addFields"><span>Sukurti laukus</span></div>';
			$new_tables[$table['ID']]['ACTIONS'][] = '
						</div>
					</div>
				</div>';
			$new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="action_label">
					<div class="atp-html-button edit"><span>' . $this->lng('edit') . '</span></div>
				</div>';
			$new_tables[$table['ID']]['ACTIONS'] = join(' ', $new_tables[$table['ID']]['ACTIONS']);

			// Gaunamas įrašo lygis
			$new_tables[$table['ID']]['LEVEL'] = $this->logic->get_table_level($tables, $table['ID']);
			// Nurodomas išskleidimo stulpelis, jei laukas turi vaikų
			if ($this->logic->table_has_children($tables, $table['ID'])) {
				$new_tables[$table['ID']]['TOGGLE'] = '<div class="collapse atp-table-list-toggle"></div>';
				$toggle_class = 'collapse';
			} else
				$toggle_class = 'none';

			$new_tables[$table['ID']]['LABEL'] = '<div class="cell-LABEL-container"><div class="' . $toggle_class . ' atp-table-list-toggle"></div><span>' . $new_tables[$table['ID']]['LABEL'] . '</span></div>';
		}
		$tables = &$new_tables;

		// Įrašai surikiuojami pagal hierarchiją (tevas -> vaikai, tevas -> vaikai)
		$tables = $this->logic->sort_children_to_parent($tables, 'ID', 'PARENT_ID');

		$tpl_arr = $this->core->getTemplate('table2');

		// Nurodomi išvedami lentelės stulpeliai
		$columns = array(
			/* 'TOGGLE' => $this->lng('atp_table_toggle'), */
			'LABEL' => $this->lng('atp_deed_label')
		);
		// Tikrinama teisė į veiksmų stulpelio matymą
		if ($this->core->user->status['A_DEED'] > 1)
			$columns['ACTIONS'] = $this->lng('atp_table_actions');

		// table
		$table = array();
		$table['table_class'] = 'deeds';
		$html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

		// Lentelės antraštė
		$header = array('row_html' => '');
		$header['row_class'] = 'div-row div-header';
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);
		foreach ($columns as $col_name => $col) {
			// cell
			$cell = array('cell_html' => '');
			$cell['cell_class'] = 'div-cell cell-' . $col_name;
			$cell['cell_content'] = $col;
			$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
		}
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

		// Lentelės įrašų eilutės
		foreach ($tables as $key => $table) {

			// Eilutės duomenys
			$row = array();
			$row['row_html'] = 'id="atp-table-list-item-id-' . $table['ID'] . '"';
			$row['row_class'][] = 'div-row';
			if ($table['LEVEL'])
				$row['row_class'][] = 'child';
			$row['row_class'][] = 'level-' . $table['LEVEL'];
			$row['row_class'][] = 'child-of-' . $table['PARENT_ID'];
			$row['row_class'] = join(' ', $row['row_class']);
			$html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

			// Eilutės stulpeiai
			foreach ($columns as $col_name => $col) {

				// Stulpelio duomenys
				$cell = array('cell_html' => '', 'cell_content' => '');
				if (in_array($col_name, array('VALID_FROM', 'VALID_TILL'))) {
					$table[$col_name] = date('Y-m-d', strtotime($table[$col_name]));
				}
				$cell['cell_class'] = 'div-cell cell-' . $col_name;
				$cell['cell_content'] .= $table[$col_name];
				$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
			}

			$html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
		}

		$html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

		return $html_manage;
	}

	/**
	 * Sugeneruoja klasifikatorių lentelę
	 * @return string
	 */
	public function table_classifications() {
		$html_manage = '';

		// Surenka klasifikatorius
		//$tables = $this->logic->get_classification(null, null, array('ORDER' => 'PARENT_ID, LABEL'));
		$tables = $this->logic2->get_classificator(null, false, 'PARENT_ID, LABEL');

		// Surenkami papildomi duomenys apie įrašus
		$new_tables = array();
		foreach ($tables as $table) {
			$new_tables[$table['ID']] = $table;

			// Surenka galimus veiksmus
			$new_tables[$table['ID']]['ACTIONS'] = array();
			// Tikrina ar vartotojas turi teisę matyti mygtukus
			if ($this->core->user->status['A_CLASSIFICATION'] > 1) {
				$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button edit"><span>' . $this->lng('edit') . '</span></div>';
				$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button addChild"><span>Pridėti vaiką</span></div>';
				$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button remove"><span>Trinti</span></div>';
				$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button addFields"><span>Sukurti laukus</span></div>';
			}
			$new_tables[$table['ID']]['ACTIONS'] = join(' ', $new_tables[$table['ID']]['ACTIONS']);

			// Gaunamas įrašo lygis
			$new_tables[$table['ID']]['LEVEL'] = $this->logic->get_table_level($tables, $table['ID']);

			// Nurodomas išskleidimo stulpelis, jei laukas turi vaikų
			if ($this->logic->table_has_children($tables, $table['ID'])) {
				$new_tables[$table['ID']]['TOGGLE'] = '<div class="collapse atp-table-list-toggle"></div>';
			}
		}
		$tables = &$new_tables;

		// Įrašai surikiuojami pagal hierarchiją (tevas -> vaikai, tevas -> vaikai)
		$tables = $this->logic->sort_children_to_parent($tables, 'ID', 'PARENT_ID');

		$tpl_arr = $this->core->getTemplate('table');

		// Nurodomi išvedami lentelės stulpeliai
		$columns = array(
			'TOGGLE' => $this->lng('atp_table_toggle'),
			'LABEL' => $this->lng('atp_classificator_label')
		);
		// Tikrinama teisė į veiksmų stulpelio matymą
		if ($this->core->user->status['A_CLASSIFICATION'] > 1)
			$columns['ACTIONS'] = $this->lng('atp_table_actions');

		// table
		$table = array();
		$table['table_class'] = 'classifications';
		$html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

		// Lentelės antraštė
		$header = array('row_html' => '');
		$header['row_class'] = 'table-row table-header';
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);
		foreach ($columns as $col_name => $col) {
			// cell
			$cell = array('cell_html' => '');
			$cell['cell_class'] = 'table-cell cell-' . $col_name;
			$cell['cell_content'] = $col;
			$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
		}
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

		// Lentelės įrašų eilutės
		foreach ($tables as $key => $table) {

			// Eilutės duomenys
			$row = array();
			$row['row_html'] = 'id="atp-table-list-item-id-' . $table['ID'] . '"';
			$row['row_class'][] = 'table-row';
			if ($table['LEVEL'])
				$row['row_class'][] = 'child';
			$row['row_class'][] = 'level-' . $table['LEVEL'];
			$row['row_class'][] = 'child-of-' . $table['PARENT_ID'];
			$row['row_class'] = join(' ', $row['row_class']);
			$html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

			// Eilutės stulpeiai
			foreach ($columns as $col_name => $col) {

				// Stulpelio duomenys
				$cell = array('cell_html' => '', 'cell_content' => '');
				if ($col_name === 'LABEL' && $table['LEVEL'] > 0) {
					$cell['cell_content'] .= '<div class="level-img level-' . $table['LEVEL'] . '"><img src="' . GLOBAL_SITE_URL . 'images/dok_rodykle_extended_left.png" alt="">' .
							str_repeat('<img src="' . GLOBAL_SITE_URL . 'images/dok_rodykle_extended_middle.png" alt="">', $table['LEVEL']) .
							'<img src="' . GLOBAL_SITE_URL . 'images/dok_rodykle_extended_right.png" alt=""></div>';
				}
				$cell['cell_class'] = 'table-cell cell-' . $col_name;
				$cell['cell_content'] .= $table[$col_name];
				$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
			}

			$html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
		}

		$html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

		return $html_manage;
	}

	/**
	 * Klasifikatorių puslapis
	 * @return string
	 */
	public function classifications() {
		$tmpl = $this->core->Template();

		// Tikrina ar vartotojas turi teisę matyti puslapį
		if (!$this->core->user->status['A_CLASSIFICATION']) {
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
		} else {
			$tmpl_handler = 'classifications_file';
			$tmpl->set_file($tmpl_handler, 'classifications.tpl');

			$new_block = $tmpl_handler . 'new';
			$tmpl->set_block($tmpl_handler, 'new', $new_block);

			// Naujo klasifikatoriaus kūrimo bloko išvedimas
			// Tikrina ar vartotojas turi teisę kurti naują klasifikatorių
			if ($this->core->user->status['A_CLASSIFICATION'] > 1)
				$tmpl->parse($new_block, 'new');
			else
				$tmpl->clean($new_block);

			// Klasifikatorių lentelė
			$tmpl->set_var('classifications_table', $this->table_classifications());
		}

		$tmpl->set_var(array(
			'GLOBAL_SITE_URL' => GLOBAL_SITE_URL,
			'nr' => (empty($_POST['nr']) ? rand(0, 10000) : $_POST['nr'])
		));
		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');

		return $this->core->atp_content($output);
	}

	/**
	 * Teisės aktų punktai (ex. Teisės aktai)
	 * @return string
	 */
	public function acts() {
		$tmpl = $this->core->Template();
		// Tikrina ar vartotojas turi teisę matyti puslapį
		if (!$this->core->user->status['A_ACT']) {
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
		} else {

//$this->core->_load_files['js'][] = 'jquery.form.min.js';
			$this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
			$this->core->_load_files['js'][] = 'jquery.validationEngine.js';
			$this->core->_load_files['css'][] = 'validationEngine.jquery.css';

			$tmpl_handler = 'acts_file';
			$tmpl->set_file($tmpl_handler, 'acts.tpl');

			$new_block = $tmpl_handler . 'new';
			$tmpl->set_block($tmpl_handler, 'new', $new_block);

			// Naujo teisės akto kūrimo bloko išvedimas
			// Tikrina ar vartotojas turi teisę kurti naują teisės aktą
			if ($this->core->user->status['A_ACT'] > 1)
				$tmpl->parse($new_block, 'new');
			else
				$tmpl->clean($new_block);

			$tmpl->set_var(array(
				'sys_message' => $this->core->get_messages(),
				'acts_table' => $this->table_acts()
			));
		}

		$tmpl->set_var('GLOBAL_SITE_URL', GLOBAL_SITE_URL);
		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');

		return $this->core->atp_content($output);
	}

	/**
	 * Dokumentų būsenos
	 * @return string
	 */
	public function status() {
		$tmpl = $this->core->Template();
		// Tikrina ar vartotojas turi teisę matyti puslapį
		if (!$this->core->user->status['A_STATUS']) {
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
		} else {

//$this->core->_load_files['js'][] = 'jquery.form.min.js';
			$this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
			$this->core->_load_files['js'][] = 'jquery.validationEngine.js';
			$this->core->_load_files['css'][] = 'validationEngine.jquery.css';

			$tmpl_handler = 'status_file';
			$tmpl->set_file($tmpl_handler, 'status.tpl');

			$new_block = $tmpl_handler . 'new';
			$tmpl->set_block($tmpl_handler, 'new', $new_block);

			// Naujo teisės akto kūrimo bloko išvedimas
			// Tikrina ar vartotojas turi teisę kurti naują teisės aktą
			if ($this->core->user->status['A_STATUS'] > 1)
				$tmpl->parse($new_block, 'new');
			else
				$tmpl->clean($new_block);

			$tmpl->set_var(array(
				'sys_message' => $this->core->get_messages(),
				'table' => $this->table_status_relations()
			));
		}

		$tmpl->set_var('GLOBAL_SITE_URL', GLOBAL_SITE_URL);
		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');

		return $this->core->atp_content($output);
	}

	function status_list() {
		$tmpl = $this->core->Template();
		// Tikrina ar vartotojas turi teisę matyti puslapį
		if (!$this->core->user->status['A_STATUS']) {
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
		} else {

//$this->core->_load_files['js'][] = 'jquery.form.min.js';
			$this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
			$this->core->_load_files['js'][] = 'jquery.validationEngine.js';
			$this->core->_load_files['css'][] = 'validationEngine.jquery.css';

			$tmpl_handler = 'status_list_file';
			$tmpl->set_file($tmpl_handler, 'status_list.tpl');

			$new_block = $tmpl_handler . 'new';
			$tmpl->set_block($tmpl_handler, 'new', $new_block);

			// Naujo teisės akto kūrimo bloko išvedimas
			// Tikrina ar vartotojas turi teisę kurti naują teisės aktą
			if ($this->core->user->status['A_STATUS'] > 1)
				$tmpl->parse($new_block, 'new');
			else
				$tmpl->clean($new_block);

			$tmpl->set_var(array(
				'sys_message' => $this->core->get_messages(),
				'table' => $this->table_status_list()
			));
		}

		$tmpl->set_var('GLOBAL_SITE_URL', GLOBAL_SITE_URL);
		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');

		return $this->core->atp_content($output);
	}

	/**
	 * Dokumento būsenos redagavimo forma
	 * @author Justinas Malūkas
	 * @return string
	 */
	public function status_editstatus() {
		$tmpl = $this->core->Template();
		if (!$this->core->user->status['A_STATUS']) {
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
			$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
			$output = $tmpl->get($tmpl_handler . '_out');
			return $this->core->atp_content($output);
		}

		if ($this->core->content_type !== 2) {
			$this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
			$this->core->_load_files['js'][] = 'jquery.validationEngine.js';
			$this->core->_load_files['css'][] = 'validationEngine.jquery.css';
		}

		$tmpl_handler = 'status_editstatus_file';
		$tmpl->set_file($tmpl_handler, 'status_editstatus.tpl');

		// Formos blokas
		$form_block = $tmpl_handler . 'form';
		$tmpl->set_block($tmpl_handler, 'form', $form_block);

		$vars = array();
		$m = (int) $this->core->extractVariable('m');

		$id = (int) $this->core->extractVariable('id');
		//$parent_id = (int) $this->core->extractVariable('parent');

		$vars['sys_message'] = $this->core->get_messages();
		$vars['save'] = 'Išsaugoti';

		$data = array();
		if (empty($id) === FALSE) {
			$data = $this->logic2->get_status(array('ID' => $id));
		}

		//
		if (empty($data) === TRUE) {
			//$vars['url'] = 'index.php?m=' . $m . '&parent=' . $parent_id . '&action=save';
			$vars['url'] = 'index.php?m=' . $m . '&parent=0&action=savestatus';
			$vars['title'] = 'Nauja būsena';
			$vars['label'] = '';
		} else {
			$vars['url'] = 'index.php?m=' . $m . '&id=' . $id . '&action=savestatus';
			$vars['title'] = 'Būsena: ' . $data['LABEL'];

			$vars['label'] = htmlspecialchars($data['LABEL']);
		}

		$tmpl->set_var($vars);

		if ($this->core->user->status['A_STATUS'] > 1) {
			$tmpl->parse($form_block, 'form');
		} else {
			$tmpl->clean($form_block);
		}

		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');

		return $this->core->atp_content($output);
	}

	public function status_editrelation() {
		$tmpl = $this->core->Template();
		if (!$this->core->user->status['A_STATUS']) {
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
			$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
			$output = $tmpl->get($tmpl_handler . '_out');
			return $this->core->atp_content($output);
		}

		if ($this->core->content_type !== 2) {
			$this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
			$this->core->_load_files['js'][] = 'jquery.validationEngine.js';
			$this->core->_load_files['css'][] = 'validationEngine.jquery.css';
		}

		$tmpl_handler = 'status_editrelation_file';
		$tmpl->set_file($tmpl_handler, 'status_editrelation.tpl');

		// Ryšio laukas
		$relation_block = $tmpl_handler . 'relation';
		$tmpl->set_block($tmpl_handler, 'relation', $relation_block);

		// Dokumento pasirinkimas
		$document_option_block = $tmpl_handler . 'document_option';
		$tmpl->set_block($tmpl_handler, 'document_option', $document_option_block);
		// Dokumento laukas
		$document_block = $tmpl_handler . 'document';
		$tmpl->set_block($tmpl_handler, 'document', $document_block);

		// ATP būsena
		$atp_status_option_block = $tmpl_handler . 'atp_status_option';
		$tmpl->set_block($tmpl_handler, 'atp_status_option', $atp_status_option_block);
		// Būsena
		$status_option_block = $tmpl_handler . 'status_option';
		$tmpl->set_block($tmpl_handler, 'status_option', $status_option_block);
		// Naujo ryšio kūrimas
		$relation_new_block = $tmpl_handler . 'relation_new';
		$tmpl->set_block($tmpl_handler, 'relation_new', $relation_new_block);

		// Formos blokas
		$form_block = $tmpl_handler . 'form';
		$tmpl->set_block($tmpl_handler, 'form', $form_block);


		$vars = array();
		$m = (int) $this->core->extractVariable('m');
		$id = (int) $this->core->extractVariable('id');


		$vars['sys_message'] = $this->core->get_messages();
		$vars['save'] = 'Išsaugoti';


		// Surenka dumenis apie ryšius
		$data = array();
		if (empty($id) === FALSE) {
			$data = $this->logic2->get_status_relations(array('DOCUMENT_ID' => $id));
		}

		if (empty($data) === TRUE) {
			$vars['title'] = 'Naujas ryšys';
			$vars['url'] = 'index.php?m=' . $m . '&action=saverelation';

			// Dokumentai
			$documents = $this->logic2->get_document(null, false, 'PARENT_ID, LABEL');
			$documents = $this->logic->sort_children_to_parent($documents, 'ID', 'PARENT_ID');

			$tmpl->set_var(array(
				'value' => null,
				'label' => $this->lng('not_chosen')
			));
			$tmpl->parse($document_option_block, 'document_option');
			foreach ($documents as &$document) {
				$tmpl->set_var(array(
					'value' => $document['ID'],
					'label' => str_repeat('&nbsp;', $document['depth'] * 3) . $document['LABEL']
				));
				$tmpl->parse($document_option_block, 'document_option', true);
			}
			$tmpl->parse($document_block, 'document');
		} else {
			$document = $this->logic2->get_document(array('ID' => $id));
			$vars['title'] = 'Ryšys dokumentui: ' . $document['LABEL'];
			$vars['url'] = 'index.php?m=' . $m . '&id=' . $id . '&action=saverelation';

			$tmpl->clean($document_block);
		}

		/*
		 * ATP būsenos pasirinkimas
		 */
		$tmpl->set_var(array(
			'value' => 0,
			'label' => $this->lng('not_chosen')
		));
		$tmpl->parse($atp_status_option_block, 'atp_status_option');
		$atp_status = $this->logic->get_document_statuses();

		$atp_status_sorted = $atp_status;
		usort($atp_status_sorted, array($this->logic, 'sort_atp_status'));

		foreach ($atp_status_sorted as &$status) {
			$tmpl->set_var(array(
				'value' => $status['ID'],
				'label' => $status['LABEL']
			));
			$tmpl->parse($atp_status_option_block, 'atp_status_option', true);
		}


		/*
		 * Būsenos pasirinkimas
		 */
		$tmpl->set_var(array(
			'value' => 0,
			'label' => $this->lng('not_chosen')
		));
		$tmpl->parse($status_option_block, 'status_option');
		$statuses = $this->logic2->get_status(null, null, 'LABEL ASC');
		foreach ($statuses as &$status) {
			$tmpl->set_var(array(
				'value' => $status['ID'],
				'label' => $status['LABEL']
			));
			$tmpl->parse($status_option_block, 'status_option', true);
		}

		$tmpl->parse($relation_new_block, 'relation_new');


		// Dokumentų, klasifikatorių, webservisų informacija
		$items_names['DOC'] = $this->logic2->get_document(null, false, null, 'ID, LABEL, PARENT_ID');
		$items_names['CLASS'] = $this->logic2->get_classificator(null, false, null, 'ID, LABEL, PARENT_ID');

		/*
		 * Priskirtų laukų išvedimas
		 */
		$tmpl->set_var(array(
			'class' => ' sample',
			'realtion_str' => '',
			'atp_status' => '',
			'status' => ''
		));
		$tmpl->parse($relation_block, 'relation');
		if (empty($data) === FALSE) {

			foreach ($data as &$arr) {
				$arr['ATP_STATUS_NAME'] = $atp_status[$arr['ATP_STATUS_ID']]['LABEL'];
				$arr['STATUS_NAME'] = $statuses[$arr['STATUS_ID']]['LABEL'];
			}

			usort($data, array($this->logic, 'sort_atp_status_relation'));

			foreach ($data as &$arr) {
				$tmpl->set_var(array(
					'class' => '',
					'realtion_str' => $arr['ATP_STATUS_ID'] . '-' . $arr['STATUS_ID'],
					'atp_status' => $arr['ATP_STATUS_NAME'],
					'status' => $arr['STATUS_NAME']
				));
				$tmpl->parse($relation_block, 'relation', true);
			}
		}

		$tmpl->set_var($vars);

		if ($this->core->user->status['A_STATUS'] > 1) {
			$tmpl->parse($form_block, 'form');
		} else {
			$tmpl->clean($form_block);
		}

		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');

		return $this->core->atp_content($output);
	}

	/**
	 * Veikos
	 * @return string
	 */
	public function deeds() {
		$tmpl = $this->core->Template();
		// Tikrina ar vartotojas turi teisę matyti puslapį
		if (!$this->core->user->status['A_DEED']) {
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
		} else {

//$this->core->_load_files['js'][] = 'jquery.form.min.js';
			$this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
			$this->core->_load_files['js'][] = 'jquery.validationEngine.js';
			$this->core->_load_files['css'][] = 'validationEngine.jquery.css';
			$tmpl_handler = 'deeds_file';
			$tmpl->set_file($tmpl_handler, 'deeds.tpl');

			$new_block = $tmpl_handler . 'new';
			$tmpl->set_block($tmpl_handler, 'new', $new_block);

			// Naujo teisės akto kūrimo bloko išvedimas
			// Tikrina ar vartotojas turi teisę kurti naują teisės aktą
			if ($this->core->user->status['A_DEED'] > 1)
				$tmpl->parse($new_block, 'new');
			else
				$tmpl->clean($new_block);
			$tmpl->set_var(array(
				'sys_message' => $this->core->get_messages(),
				'deeds_table' => $this->table_deeds()
			));
		}

		$tmpl->set_var('GLOBAL_SITE_URL', GLOBAL_SITE_URL);
		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');

		return $this->core->atp_content($output);
	}

	/**
	 * Dokumentas (Dokumentų sąrašas)
	 * @return string HTML'as
	 */
	public function table_records() {
		$this->logic->_r_table_id = (int) $_GET['table_id'];
		if ($this->logic->privileges('table_records') === FALSE) {
			$tmpl = $this->core->Template();
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
			$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
			$output = $tmpl->get($tmpl_handler . '_out');
			return $this->core->atp_content($output);
		}

		//$table = $this->logic->get_table($this->logic->_r_table_id);
		$table = $this->logic2->get_document($this->logic->_r_table_id);

		$tmpl = $this->core->Template();
		$tmpl_handler = 'table_records_file';
		$tmpl->set_file($tmpl_handler, 'table_records.tpl');

		$button_new = $tmpl_handler . 'button_new';
		$tmpl->set_block($tmpl_handler, 'button_new', $button_new);

		$tmpl->set_var(array(
			'title' => $table['LABEL'],
			//'table_id' => $this->logic->_r_table_id,
			'sys_message' => $this->core->get_messages(),
			//'action_exit' => 'index.php?m=4&table_id=' . $this->logic->_r_table_id, // Popup'o dalis
			'action_create_new' => 'index.php?m=5&table_id=' . $this->logic->_r_table_id,
			'table_record' => $this->table_record($table),
			'GLOBAL_SITE_URL' => GLOBAL_SITE_URL
		));
		if ($this->logic->privileges('table_records_button_new') === TRUE)
			$tmpl->parse($button_new, 'button_new');
		else
			$tmpl->clean($button_new);


		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');
		return $this->core->atp_content($output);
	}

	/**
	 * Mokėjimų atnaujinimas (Dokumentų sąrašas)
	 * @param int $table_id Dokumento ID
	 * @param mixed $record Tai ką valgo <b>atp_logic2::get_record_relation</b>
	 * @return string HTML'as
	 */
	function record_payments_form($table_id, $record) {
		$record = $this->logic2->get_record_relation($record);
		if (empty($record) === TRUE) {
			trigger_error('Record not found', E_USER_ERROR);
			exit;
		}

		if ($this->logic->privileges('table_record_update', array('table_id' => $record['TABLE_TO_ID'], 'record' => &$record)) === FALSE) {
			$tmpl = $this->core->Template();
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
			$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
			$output = $tmpl->get($tmpl_handler . '_out');
			return $this->core->atp_content($output);
		}

		if ($_GET['action'] === 'submit') {
			$this->logic->record_payments_save();
		}

		$record_table = $this->logic2->get_record_table($record);
		$document = $this->logic2->get_document($record['TABLE_TO_ID']);
		//$doc_structure = $this->logic->get_table_fields($document['ID']);
		$doc_structure = $this->logic2->get_fields_all(array('ITEM_ID' => $document['ID'], 'ITYPE' => 'DOC'));


		$tmpl = $this->core->Template();
		$tmpl_handler = 'table_reocord_payments_file';
		$tmpl->set_file($tmpl_handler, 'table_reocord_payments.tpl');

		// Mokėjimo lauko papildoma informacija
		$extra_block = $tmpl_handler . 'extra';
		$tmpl->set_block($tmpl_handler, 'extra', $extra_block);
		// Mokėjimo informacijos laukas
		$payment_field_block = $tmpl_handler . 'payment_field';
		$tmpl->set_block($tmpl_handler, 'payment_field', $payment_field_block);
		// Mokėjimo informacija
		$payment_block = $tmpl_handler . 'payment';
		$tmpl->set_block($tmpl_handler, 'payment', $payment_block);
		// Baudos suma
		$penalty_block = $tmpl_handler . 'penalty';
		$tmpl->set_block($tmpl_handler, 'penalty', $penalty_block);
		// Mokėjimų forma
		$form_block = $tmpl_handler . 'form';
		$tmpl->set_block($tmpl_handler, 'form', $form_block);


		// Dokumento įrašo mokėjimai
		$payments = $this->logic2->get_payments(array('DOCUMENT_ID' => $document['ID'], 'RECORD_ID' => $record['RECORD_ID']));
		$payments_tmp = array();
		foreach ($payments as &$tmp) {
			$payments_tmp[$tmp['PAYMENT_ID']] = $tmp;
		}
		$payments = &$payments_tmp;


		// Gauna nustatytos baudos sumą
		$penalty_sum = null;
		if (empty($doc_structure[$document['PENALTY_FIELD']]) === FALSE) {
			$penalty_sum = $this->logic->get_extra_field_value($record['ID'], $document['PENALTY_FIELD']);
		}
		// Išveda nustatytos baudos sumą
		if (empty($penalty_sum) === FALSE) {
			$tmpl->set_var('penalty_sum', $penalty_sum);
			$tmpl->parse($penalty_block, 'penalty', false);
		} else {
			$tmpl->clean($penalty_block);
		}


		// Iš VMI registro gauna atliktų mokėjimų informaciją
		$data = array('registry' => 'vmi');
		foreach (array('SEARCH_CODE' => $document['PERSON_CODE_FIELD'], 'SEARCH_FROM' => $document['FILL_DATE_FIELD']) as $key => $field) {
			if (empty($field) === TRUE)
				continue;

			if (isset($doc_structure[$field])) {
				if (isset($record_table[$doc_structure[$field]['NAME']])) {
					$data[$key] = $record_table[$doc_structure[$field]['NAME']];
				}
			}
		}
		
		//$data = array('registry' => 'vmi', 'SEARCH_CODE' => 38512110322, 'SEARCH_FROM' => '2013-01-01 01:00:00');
		$found_payments = $this->core->getRDBWsData('getVMISearchResult', $data);
		$fp_count = count($found_payments);


		// Gauna sistemoje priskirtas apmokėtų mokėjimų sumas
		$found_payments_ids = array();
		for ($i = 0; $i < $fp_count; $i++) {
			$found_payments_ids[] = $found_payments[$i]['ID'];
		}
		$paid = array();
		if (count($found_payments_ids)) {
			$get_paid_payments = $this->logic2->get_payments(array(0 => 'PAYMENT_ID IN(' . join(',', $found_payments_ids) . ')'));
			foreach ($get_paid_payments as &$paid_payment) {
				$paid[$paid_payment['PAYMENT_ID']] += $paid_payment['PAID'];
			}
		}


		// Formoje nereikalingi laukai
		$exclude = array(
			'ID',
			'DOK_NUMERIS',
			'DOK_DATA',
			'SAVIVALDYBES_ID',
			'OP_TIPAS',
			'SUMA_BV',
			'VALIUTA',
			'IRS_UPD_DATA',
			'IRS_INS_DATA',
		);

		// Generuoja mokėjimo informacijos bloką
		for ($i = 0; $i < $fp_count; $i++) {
			$j = 0;
			// Generuoja mokėjimo informacijos lauko bloką
			foreach ($found_payments[$i] as $key => &$field) {
				if (in_array($key, $exclude, true) === TRUE)
					continue;

				$tmpl->set_var(array(
					'class' => $key,
					'label' => $this->lng('wmi_field_' . $key) . ':',
					'value' => htmlspecialchars($field)
				));

				// Papildoma informacija
				if ($key === 'SUMA') {
					$tmpl->set_var('extra', '<span class="paid" default="' . $paid[$found_payments[$i]['ID']] . '">&nbsp;(-' . $paid[$found_payments[$i]['ID']] . ')</span>');
					$tmpl->parse($extra_block, 'extra');
				} else {
					$tmpl->set_var($extra_block, '');
				}

				$tmpl->parse($payment_field_block, 'payment_field', $j);
				$j++;
			}
			$tmpl->set_var(array(
				'id' => $found_payments[$i]['ID'],
				'dvalue' => isset($payments[$found_payments[$i]['ID']]) ? $payments[$found_payments[$i]['ID']]['PAID'] : '0',
				'value' => isset($payments[$found_payments[$i]['ID']]) ? $payments[$found_payments[$i]['ID']]['PAID'] : '',
				'disabled' => isset($payments[$found_payments[$i]['ID']]) ? '' : ' disabled="disabled"',
				'checked' => isset($payments[$found_payments[$i]['ID']]) ? ' checked="checked"' : ''
			));

			$tmpl->parse($payment_block, 'payment', $i);
		}

		$tmpl->set_var(array(
			'url' => 'index.php?m=17&action=submit&id=' . $record['ID'],
			'save' => $this->lng('save')
		));

		$tmpl->set_var('checked', ($record['IS_PAID'] == 1 ? ' checked="checked"' : ''));
		$tmpl->parse($form_block, 'form');

		$tmpl->set_var(array(
			'title' => 'Dokumento "' . $record['RECORD_ID'] . '" mokėjimai',
			'sys_message' => $this->core->get_messages()
		));

		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');
		return $this->core->atp_content($output);
	}

	/**
	 * Dokumento įrašų sąrašas
	 * @param array $document Dokumento duomenys
	 * @return null
	 */
	public function table_record($document) {
		if (empty($document['ID']) === TRUE) {
			trigger_error('Table ID is empty', E_USER_WARNING);
			return null;
		}

		$html = '';


		// Puslapiavimas
		include_once($this->core->config['REAL_URL'] . 'helper/paginator.php');
		$pg = new Paginator();
		$pg->current_page = empty($_GET['_page']) ? 1 : $_GET['_page'];
		$pg->items_per_page = 20;
		// Viso įrašų
		$count = $this->logic->get_worker_records(array(
			'WORKER_ID' => $this->core->user->worker['ID'],
			'DOCUMENT_ID' => $document['ID']
				), true);
		$pg->items_total = $count;
		$pg->paginate();

		// Pagal puslapiavimą apribojamas duomenų kiekis
		$start = $pg->items_total - $pg->limit_end;
		$limit = $pg->items_per_page;
		if ($start < 0) {
			$limit = $limit + $start;
			$start = 0;
		}
		// Puslapio dokumentai
		$records = $this->logic->get_worker_records(array(
			'WORKER_ID' => $this->core->user->worker['ID'],
			'DOCUMENT_ID' => $document['ID'],
			'START' => $start,
			'OFFSET' => $limit,
			'ORDER' => 'CREATE_DATE DESC'
		));


		$terms = $this->logic->get_records_terms_by_table_id($document['ID'], $document);

		$privileges_data = array('table_id' => $document['ID']);

		//$i = -1;
		$new_records = array();

		//$records = array_reverse($records);
		foreach ($records as $record) {
			$record = $this->logic2->get_record_relation($record);
			if (empty($record) === TRUE)
				continue;

			$record = current($this->logic->get_table_records(array('INCLUDE_OWNER' => false, 'RECORD_NR' => $record['ID'])));

			$new_records[$record['ID']] = $record;
			$temp = &$new_records[$record['ID']];

			$privileges_data['record'] = &$record;

			//$temp['TERM'] = $this->logic->get_record_term($record['RECORD_ID'], $document);
			$temp['TERM'] = $terms[$record['RECORD_ID']];
			$temp['ACTIONS'] = array();
			$temp['ACTIONS'][] = '<div class="expand_collapse_block"><div class="expand_collapse_img_block"><img style="cursor: pointer;" src="' . GLOBAL_SITE_URL . 'images/atp/expand_doc_action.png" width="29" height="29"></div><div class="actions_block" style="display: none;"><img style="display: block;" src="' . GLOBAL_SITE_URL . 'images/actions/top_img_actions_buttons.png" width="132" height="8"><div class="actions_buttons">';

			if ($this->logic->privileges('table_record_button_view', $privileges_data) === TRUE)
				$temp['ACTIONS'][] = '<a alt="Peržiūrėti" title="Peržiūrėti" id="atp-record-' . $record['RECORD_ID'] . '" class="atp-record-edit atp-html-button edit" href="index.php?m=5&id=' . $record['RECORD_ID'] . '">Peržiūrėti</a>';

			if ($this->logic->privileges('table_record_button_update', $privileges_data) === TRUE)
				$temp['ACTIONS'][] = '<a alt="Atnaujinti" title="Atnaujinti" class="atp-html-button update dialog" href="index.php?m=17&id=' . $record['RECORD_ID'] . '">Atnaujinti</a>';

			$temp['ACTIONS'][] = '<a alt="Kelias" title="Kelias" class="atp-html-button road" href="index.php?m=6&table_id=' . $document['ID'] . '&record_id=' . $record['RECORD_ID'] . '">Kelias</a>';

			if ($this->logic->privileges('table_record_button_delete', $privileges_data) === TRUE)
				$temp['ACTIONS'][] = '<a alt="Trinti" title="Trinti" class="atp-html-button remove" href="index.php?m=98&table_id=' . $document['ID'] . '&record_id=' . $record['RECORD_ID'] . '&delete=1">Trinti</a>';

			$temp['ACTIONS'][] = '</div></div></div>';
			/*
			  // Dokumento įrašo būklė
			  $status = $this->logic->find_document_status(array('ID' => $record['STATUS']));
			  $status2 = $this->logic2->get_status(array('ID' => $record['DOC_STATUS']));
			  if (empty($status2)) {
			  $status_name = '----';
			  } else {
			  $status_name = $status2['LABEL'];
			  }
			  $temp['ACTIONS'][] = '<div class="status_label"><span class="document_status-' . $status['ID'] . ' document_status-' . $status['SHORT'] . '">' . $status_name . '</span></div>'; */


			// Paskutinio dokumento įrašo kelyje būklė
			// Gaunamas RECORD_RELATIONS.ID
			$qry = 'SELECT B.ID
				FROM
					' . PREFIX . 'ATP_RECORD_X_RELATION A
						INNER JOIN ' . PREFIX . 'ATP_RECORD_RELATIONS B ON B.ID = A.RELATION_ID
				WHERE 
					A.RECORD_ID = :RECORD_ID AND
					A.DOCUMENT_ID = :DOCUMENT_ID
				LIMIT 1';
			$result = $this->core->db_query_fast($qry, array(
				'RECORD_ID' => $record['ID'],
				'DOCUMENT_ID' => $document['ID']
			));
			$record_relation = $this->core->db_next($result);

			$last_record = $this->logic->record_path_last($record_relation['ID']);
			$last_status = $this->logic->find_document_status(array('ID' => $last_record['STATUS']));
			$last_status2 = $this->logic2->get_status(array('ID' => $last_record['DOC_STATUS']));
			if (empty($last_status2)) {
				$last_status_name = '----';
			} else {
				$last_status_name = $last_status2['LABEL'];
			}
			//$temp['STATUS'] = '<div class="status_label last"><span class="document_status-' . $last_status['ID'] . ' document_status-' . $last_status['SHORT'] . '">' . $last_status_name . '</span></div>';
			$temp['STATUS'] = $last_status_name;


			$temp['ACTIONS'] = join(' ', $temp['ACTIONS']);
		}

		$records = &$new_records;


		$template = 'table';
		switch ($template) {
			case 'table':
				$css_pre = 'table';
				break;
			case 'table2':
				$css_pre = 'div';
				break;
			default:
				$css_pre = 'table';
		}
		$tpl_arr = $this->core->getTemplate($template);

		//$table_structure = $this->logic->get_table_fields($document['ID'], array('VISIBILITY' => TRUE));
		$table_structure = $this->logic2->get_fields_all(array('ITEM_ID' => $document['ID'], 'ITYPE' => 'DOC'));
		if (is_array($table_structure)) {
			foreach ($table_structure as $key => $item) {
				if ($item['VISIBILITY'] == 0)
					unset($table_structure[$key]);
			}
		}

		$columns = array('RECORD_ID' => 'ATP ID');
		//$table_structure_order = array();
		$table_structure_by_col_name = array();
		//$order_offset = 0;
		$extra_columns = array();
		foreach ($table_structure as $structure) {
			//$table_structure_order[((int) $structure['ORD'] + (int) $order_offset)] = $structure;
			$table_structure_by_col_name[$structure['NAME']] = $structure;
			$columns[$structure['NAME']] = $structure['LABEL'];
			if ((int) $structure['TYPE'] === 23) {
				//$order_offset++;
				$extra_columns['CLAUSE'] = $structure['NAME'] . '_clause';
				$columns[$extra_columns['CLAUSE']] = 'Straipsnis';
				$table_structure_by_col_name[$extra_columns['CLAUSE']] = array(
					'SORT' => 6
				);
			}
		}

		if (empty($document['CAN_TERM']) === FALSE)
			$columns['TERM'] = 'TERMINAS'; //$this->lng('atp_table_actions');

		$columns['STATUS'] = $this->lng('atp_table_status');
		$columns['ACTIONS'] = $this->lng('atp_table_actions');

		// table
		$table = array();
		$table['table_class'] = $css_pre . ' gridtable';
		$html .= $this->core->returnHTML($table, $tpl_arr[0]);

		// header
		$header = array('row_html' => '');
		$header['row_class'] = $css_pre . '-row ' . $css_pre . '-header';
		$html .= $this->core->returnHTML($header, $tpl_arr[1]);

		foreach ($columns as $col_name => $col) {
			// cell
			$cell = array('cell_html' => '');
			$cell['cell_class'] = $css_pre . '-cell cell-' . $col_name;
			/* if ($col_name === 'ACTIONS')
			  //$cell['cell_html'] = 'style="width: 235px;"';
			  $cell['cell_html'] = 'style="width: 132px;"'; */

			$cell['cell_content'] = $col;
			$html .= $this->core->returnHTML($cell, $tpl_arr[2]);
		}
		$html .= $this->core->returnHTML($header, $tpl_arr[3]);

		foreach ($records as $key => &$record) {
			// row
			$row = array('row_html' => '', 'row_class' => $css_pre . '-row');
			$html .= $this->core->returnHTML($row, $tpl_arr[1]);

			$regId = $record['RECORD_ID'];

			foreach ($columns as $col_name => $col) {
				// cell
				$cell = array('cell_html' => '', 'cell_content' => '');

				$empty = false;
				// Checkbox'as
				if ($table_structure_by_col_name[$col_name]['SORT'] == 5) {
					$record[$col_name] = implode(', ', json_decode(stripslashes($record[$col_name])));
				} else
				// Veika
				if ($table_structure_by_col_name[$col_name]['TYPE'] == 23) {
					if (empty($record[$col_name]) === FALSE) {
						$deed = $this->logic->get_deed(array('ID' => $record[$col_name]), true);
						if (empty($deed) === FALSE) {
							$record[$col_name] = $deed['LABEL'];

							// Nurodomas veikos straipsnis
							$item = $this->logic2->get_record_clause($record['RID'], array('DEED' => $deed['ID']));
							if (empty($item) === FALSE) {
								$record[$extra_columns['CLAUSE']] = $item['ID'];
							}
							unset($item);
						}
					}
					if (empty($deed) === TRUE || empty($record[$col_name]) === TRUE)
						$empty = true;

					unset($deed);
				} else
				// Datos ir laiko tipas
				if ($table_structure_by_col_name[$col_name]['SORT'] == 7) {
					$timestamp = strtotime($record[$col_name]);

					// Laikas
					if ($table_structure_by_col_name[$col_name]['TYPE'] == 13) {
						$record[$col_name] = date('H:i', $timestamp);
					} else
					// Data
					if ($table_structure_by_col_name[$col_name]['TYPE'] == 14) {
						$record[$col_name] = date('Y-m-d H:i', $timestamp);
					}
				} else
				// Klasifikatorius
				if ((int) $table_structure_by_col_name[$col_name]['TYPE'] === 18) {
					if (empty($record[$col_name]) === FALSE) {
						$item = $this->logic2->get_classificator(array('ID' => (int) $record[$col_name]));
						if (empty($item) === FALSE)
							$record[$col_name] = $item['LABEL'];
					}
					if (empty($item) === TRUE || empty($record[$col_name]) === TRUE)
						$empty = true;

					unset($item);
				} else
				// Straipsnis
				if ((int) $table_structure_by_col_name[$col_name]['SORT'] === 6) {
					if (empty($record[$col_name]) === FALSE) {
						$item = $this->logic2->get_clause(array('ID' => (int) $record[$col_name]));
						if (empty($item) === FALSE)
							$record[$col_name] = $item['NAME'];
					}
					if (empty($item) === TRUE || empty($record[$col_name]) === TRUE)
						$empty = true;

					unset($item);
				}

				if ($empty === TRUE) {
					$record[$col_name] = '';
				}

				// TODO: kas čia vyksta?
				//if (!empty($table_structure_by_col_name[$structure['NAME']]['FIELD_RELATION'])) {
				if (!empty($table_structure_by_col_name[$col_name]['FIELD_RELATION'])) {
					//$field_select = $this->logic->get_field_select($table_structure_by_col_name[$structure['NAME']]['FIELD_RELATION']);
					$field_select = $this->logic->get_field_select($table_structure_by_col_name[$col_name]['FIELD_RELATION']);
					foreach ($field_select as $select) {
						if ($select['ID'] == $record[$col_name]) {
							$record[$col_name] = $select['NAME'];
							break;
						}
					}
				}

				// Jei tai dokumento įrašo numerio stulpelis, ant jo reikšmės formuojama redagavimo nuoroda
				if ($col_name === 'RECORD_ID') {
					$record[$col_name] = $record[$col_name];
				} else
				// Jei nėra teisingos reikšmės, ją pakeičia į ""
				if (empty($record[$col_name]) === TRUE)
					$record[$col_name] = '';
				else
				// Jei tai ne ACTION stulpelis ir jei reikia, trumpinama išvedama informacija
				if ($col_name !== 'ACTIONS') {
					$max_length = 35;
					if (mb_strlen($record[$col_name], 'UTF-8') > $max_length) {
						$shortened = shortHtml($record[$col_name], $max_length, '...', false, true);
						$record[$col_name] = '<span title="' . htmlspecialchars($record[$col_name]) . '">' . $shortened . '</span>';
					}
				}

				if ($col_name !== 'ACTIONS' && empty($record[$col_name]) === FALSE)
					$record[$col_name] = '<a href="index.php?m=5&id=' . $regId . '">' . $record[$col_name] . '</a>';




				$cell['cell_class'] = $css_pre . '-cell cell-' . $col_name;
				$cell['cell_content'] = $record[$col_name];
				$html .= $this->core->returnHTML($cell, $tpl_arr[2]);
			}

			$html .= $this->core->returnHTML($row, $tpl_arr[3]);
		}

		$html .= $this->core->returnHTML($table, $tpl_arr[4]);

		// Puslapiai
		$html .= $pg->display_pages();

		return $html;
	}

	/**
	 * Dokumento įrašo kūrimo / redagavimo forma
	 * @version beta
	 * @author Justinas Malūkas
	 * @return mixed
	 */
	public function table_record_edit2() {

		/**
		 * Duomenys
		 */
		// Dokumento ID
		$param['table_id'] = (int) $_REQUEST['table_id'];
		// Įrašo ID
		$param['record_id'] = $record_id = empty($_REQUEST['record_id']) ? 0 : $_REQUEST['record_id'];
		// Dokumento informacija
		$param['table'] = $this->logic2->get_document($param['table_id']);
		// Dokumento laukų informacija
		$param['structure'] = $this->logic->get_table_fields($param['table_id']);
		// Įrašo informacija
		//$param['record'] = $this->logic->get_table_records($param['table_id'], $param['record_id']);
		$param['record'] = $this->logic->get_table_records(array(
			'DOC_ID' => $param['table_id'],
			'RECORD_NR' => $param['record_id']
		));
		$param['record'] = array_shift($param['record']);
		// Galimas dokumento kelias
		$param['path'] = $this->logic->get_table_path($param['table_id']);
		// Surašančio vartotojo informacija
		$param['user'] = $this->logic->get_m_user($this->core->user->id, $this->core->user->department->id);


		$tmpl = $this->core->Template();
		$tmpl_handler = 'table_record_edit2_file';
		$tmpl->set_file($tmpl_handler, 'table_record_edit2.tpl');

		// Šablono pasirinkiams
		$template_block = $tmpl_handler . 'template';
		$tmpl->set_block($tmpl_handler, 'template', $template_block);
		// Šablono atsisiuntimas
		$create_document = $tmpl_handler . 'create_document';
		$tmpl->set_block($tmpl_handler, 'create_document', $create_document);
		// Sekančio dokumento pasirinkimas
		$path_block = $tmpl_handler . 'path';
		$tmpl->set_block($tmpl_handler, 'path', $path_block);
		// Sekančio dokumento kūrimas
		$create_new = $tmpl_handler . 'create_new';
		$tmpl->set_block($tmpl_handler, 'create_new', $create_new);

		/**
		 * Mygtukai
		 */
		// Saugoti
		$button_save = $tmpl_handler . 'button_save';
		$tmpl->set_block($tmpl_handler, 'button_save', $button_save);
		// Saugoti naują versiją
		$button_save_new = $tmpl_handler . 'button_save_new';
		$tmpl->set_block($tmpl_handler, 'button_save_new', $button_save_new);
		// Baigti tyrimą
		$button_finish_investigation = $tmpl_handler . 'button_finish_investigation';
		$tmpl->set_block($tmpl_handler, 'button_finish_investigation', $button_finish_investigation);
		// Baigti narginėtimą
		$button_finish_examination = $tmpl_handler . 'button_finish_examination';
		$tmpl->set_block($tmpl_handler, 'button_finish_examination', $button_finish_examination);
		// Baigti procesą
		$button_finish_proccess = $tmpl_handler . 'button_finish_proccess';
		$tmpl->set_block($tmpl_handler, 'button_finish_proccess', $button_finish_proccess);
		// Baigti tarpinę būseną
		$button_finish_idle = $tmpl_handler . 'button_finish_idle';
		$tmpl->set_block($tmpl_handler, 'button_finish_idle', $button_finish_idle);


		// Adresas dokumento atsisiuntimui
		$document_link = 'index.php?' . http_build_query(array('m' => 9, 'table_id' => $param['table_id'], 'record_id' => $param['record_id']));

		// Duomenys teisių tikrinimui
		$privileges_data = array(
			'table_id' => $param['table_id'],
			'user_id' => $param['user']['ID'],
			'department_id' => $param['user']['STRUCTURE_ID'],
			'table' => $param['table'],
			'record' => $param['record']);

		/**
		 * Blokas dokumento spausdinimui
		 */
		if (empty($param['record_id']) === FALSE) {
			//$templates = $this->logic->get_table_template($param['table_id']);
			$templates = $this->logic2->get_template(array('TABLE_ID' => $param['table_id']));
			foreach ($templates as $template) {
				$tmpl->set_var(array(
					'template_value' => $template['ID'],
					'template_label' => $template['LABEL']));
				$tmpl->parse($template_block, 'template', true);
			}

			$template = reset($templates);
			$tmpl->set_var('document_link', $document_link . http_build_query(array('template_id' => $template['ID'])));
			$tmpl->parse($create_document, 'create_document');
		}
		if (empty($templates) === TRUE)
			$tmpl->clean($create_document);


		/**
		 * Blokas sekančio dokumento kūrimui
		 */
		$create_next = false;
		if ((in_array($param['record']['STATUS'], array(4)) === TRUE || ( $param['table']['CAN_NEXT'] === '1' && in_array($param['record']['STATUS'], array(2, 3, 4)) === TRUE)
				) && empty($param['record_id']) === FALSE && empty($param['path']) === FALSE) {

			// Performuoja masyvą iš 3 į 2 dimensijų
			$param['path_singular'] = array($param['table']['ID'] => $param['table']);
			foreach ($param['path'] as &$path) {
				foreach ($path as &$document) {
					$param['path_singular'][$document['ID']] = $document;
				}
			}
			// Perrikiuoja kelio masyvą
			$param['path_singular'] = $this->logic->sort_children_to_parent($param['path_singular'], 'ID', 'PARENT_ID');

			// Spausdina pasirinkimus
			foreach ($param['path_singular'] as $key => &$path) {
				$opt_extra = null;

				// Išjungiami pasirinkimai
				if ($this->logic->privileges('table_record_edit_button_submit_path', array_merge($privileges_data, array('table_id' => $path['ID']))) === FALSE)
					$opt_extra = ' disabled="disabled"';
				// TODO: ne visose naršyklės matosi stiliai
				if ($path['ID'] == $param['table']['ID']) {
					$opt_extra = ' disabled="disabled" class="current" style="font-weight: bold!important;"';
				}

				$tmpl->set_var(array(
					'value' => ($opt_extra === NULL ? $path['ID'] : ''),
					'label' => str_repeat('&nbsp;', (int) $path['depth'] * 3) . htmlspecialchars($path['LABEL']),
					'opt_extra' => $opt_extra));
				$tmpl->parse($path_block, 'path', $key);

				if ($opt_extra === NULL) {
					$create_next = true;
				}
			}
			$tmpl->set_var('create', $this->lng('create'));

			if ($create_next === FALSE)
				trigger_error('User "' . $param['user']['SHOWS'] . '" (' . $param['user']['ID'] . ') does not have rights to create next document from "' . $param['record_id'] . '" document', E_USER_WARNING);
		}
		if ($create_next === TRUE) {
			$tmpl->parse($create_new, 'create_new');
		} else {
			$tmpl->clean($create_new);
			if ($this->logic->check_privileges('EDIT_RECORD', $privileges_data) === TRUE)
				$tmpl->set_var(array(
					'table_id' => $param['table_id'],
					'record_id' => $param['record_id']));
		}

		// Veiksmų mygtukai
		if ($this->logic->privileges('table_record_edit_button_save', $privileges_data) === TRUE)
			$tmpl->parse($button_save, 'button_save');
		else
			$tmpl->clean($button_save);
		if ($this->logic->privileges('table_record_edit_button_save_new', $privileges_data) === TRUE)
			$tmpl->parse($button_save_new, 'button_save_new');
		else
			$tmpl->clean($button_save_new);
		if ($this->logic->privileges('table_record_edit_button_finish_investigation', $privileges_data) === TRUE)
			$tmpl->parse($button_finish_investigation, 'button_finish_investigation');
		else
			$tmpl->clean($button_finish_investigation);
		if ($this->logic->privileges('table_record_edit_button_finish_examination', $privileges_data) === TRUE)
			$tmpl->parse($button_finish_examination, 'button_finish_examination');
		else
			$tmpl->clean($button_finish_examination);
		if ($this->logic->privileges('table_record_edit_button_finish_proccess', $privileges_data) === TRUE)
			$tmpl->parse($button_finish_proccess, 'button_finish_proccess');
		else
			$tmpl->clean($button_finish_proccess);
		if ($this->logic->privileges('table_record_edit_button_finish_idle', $privileges_data) === TRUE)
			$tmpl->parse($button_finish_idle, 'button_finish_idle');
		else
			$tmpl->clean($button_finish_idle);


		// Dokumento įrašo pranešimas
		$doc_message = null;
		if (in_array($param['record']['STATUS'], array(3, 4, 5)) === TRUE && empty($param['table']['CAN_IDLE']) === FALSE) {
			$message_tpl = '<div class="atp-doc-message"><div class="atp-doc-message-{$type}">{$message} (patikrinta: {$date}; apmokėta: {$paid_date})</div><div class="separator"></div></div>';
			$to = ((bool) $param['record']['IS_PAID'] === TRUE ? array('success', 'Įvykdytas AN') : array('error', 'Neįvykdytas AN'));
			$to[3] = $param['record']['CREATE_DATE'];
			$to[4] = $param['record']['CREATE_DATE'];
			$doc_message = str_replace(array('{$type}', '{$message}', '{$date}', '{$paid_date}'), $to, $message_tpl);
		}

		// Suranda pažeidimo datos lauką
		$date_field_name = '';
		if (isset($param['structure'][$param['table']["DATE_FIELD"]])) {
			$date_field_name = strtolower($param['structure'][$param['table']["DATE_FIELD"]]['NAME']);
		}

		// Parametras laukų išrinkimui
		//$param['fields_param'] = array('ITYPE' => 'DOC', 'ITEM_ID' => $param['table_id']);
		// Parametras lauko [name] atributui
		//$param['prefix'] = 'T' . $param['table_id'];

		$tmpl->set_var(array(
			'title' => $param['table']['LABEL'] . ' - ' . $param['record_id'],
			'sys_message' => $this->core->get_messages(),
			'doc_message' => $doc_message,
			'user_surname' => $param['user']['LAST_NAME'],
			'user_name' => $param['user']['FIRST_NAME'],
			'user_department' => $param['user']['DEPARTMENT_TITLE'],
			'document_link' => $document_link . '&template_id=',
			'action_edit' => 'index.php?m=55555',
			'record_id' => $param['record_id'],
			'table_id' => $param['table_id'],
			'table_from_id' => $this->logic->_r_table_from_id, // TODO: kaip ir niekur nenaudojamas, nebent išsaugojus nenaudoti header Location, o tieiogiai užkrauti dokumento formą į kurią puvo persiųstas įrašas
			//'template_html' => $this->record_form($param),
			'template_html' => $this->record_form_parse(array('ITYPE' => 'DOC', 'ITEM_ID' => $param['table_id']), $param['record_id'], $param),
			'js_folder' => JS_URL,
			'str_webservice' => '', // TODO: ?
			'col_webservice' => '', // TODO: ?
			'date_field_name' => $date_field_name,
			'GLOBAL_SITE_URL' => GLOBAL_SITE_URL
		));

		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');
		return $this->core->atp_content($output);
	}

	public function record_form_parse($data, $record_id, &$param = array()) {
		if (empty($param) === TRUE) {
			// Dokumento ID
			$param['table_id'] = (int) $data['ITEM_ID'];
			// Įrašo ID
			$param['record_id'] = empty($record_id) ? 0 : $record_id;
			// Dokumento informacija
			$param['table'] = $this->logic2->get_document($param['table_id']);
			// Dokumento laukų informacija
			$param['structure'] = $this->logic->get_table_fields($param['table_id']);
			// Įrašo informacija
			//$param['record'] = $this->logic->get_table_records($param['table_id'], $param['record_id']);
			$param['record'] = $this->logic->get_table_records(array(
				'DOC_ID' => $param['table_id'],
				'RECORD_NR' => $param['record_id']
			));
			$param['record'] = array_shift($param['record']);
			// Galimas dokumento kelias
			$param['path'] = $this->logic->get_table_path($param['table_id']);
			// Surašančio vartotojo informacija
			$param['user'] = $this->logic->get_m_user($this->core->user->id, $this->core->user->department->id);
		}

		// Parametras laukų išrinkimui
		$param['fields_param'] = $data;

		// Parametras lauko [name] atributui
		switch ($data['ITYPE']) {
			case 'DOC':
				$param['prefix'] = '';
				break;
			case 'CLASS':
				$param['prefix'] = $param['prefix'] . 'K' . $data['ITEM_ID'];
				break;
		}

		return $this->record_form($param);
	}

	/**
	 * Suformuoja dokumento įrašo formos HTML'ą
	 * @author Justinas Malūkas
	 * @param array $param Duomenys
	 * @return string
	 */
	private function record_form(&$param) {
		$fields = $this->logic2->get_fields($param['fields_param'], false, '`ORD` ASC');

		$ret = null;
		if (empty($fields) === FALSE) {
			//$param['sort'] =  $this->logic->get_field_sort();
			$param['types'] = $this->logic->get_field_type();

			$ret = $this->record_fields($fields, $param);
		}

		return $ret;
	}

	/**
	 * Suformuoja dokumento įrašo laukų HTML'ą
	 * @author Justinas Malūkas
	 * @param array $fields Laukų informacija
	 * @param array $param Duomenys
	 * @param string $result [optional] Laukų HTML'as
	 * @return string
	 */
	private function record_fields(&$fields, &$param, &$result = null) {
		$field = current($fields);
		if ($field !== FALSE) {

			$field_html = $this->record_field($field, $param);
			$result .= $field_html;

			next($fields);
			return $this->record_fields($fields, $param, $result);
		}
		return $result;
	}

	/**
	 * Suformuoja dokumento įrašo lauko vaikų HTML'ą
	 * @author Justinas Malūkas
	 * @param array $param Duomenys
	 * @param array $param_tmp Duomenys vaikų formavimui
	 * @return string
	 */
	private function record_field_children($param, $param_tmp) {
		$param = array_merge($param, $param_tmp);
		return '<div class="children">' . $this->record_form($param_tmp) . '</div>';
	}

	/**
	 * Suformuoja dokumento įrašo lauko HTML'ą
	 * @authors Justinas Malūkas, Martynas Boika
	 * @param array $field Lauko informacija
	 * @param array $param Duomenys
	 * @return string
	 */
	private function record_field(&$field, &$param) {
		if (empty($field))
			return;

		$record_id = $param['record_id'];
		$table = $param['table'];
		$table_record = $param['record'];
		$value = isset($param['record'][$field['NAME']]) ? $param['record'][$field['NAME']] : '';
		$result = null;

		$expl = explode('_', $field['NAME']);
		$param['prefix'] = $prefix = $param['prefix'] . 'F' . end($expl);
		$name = 'field[' . $prefix . ']';
		// Šablono laukai
		$tpl = array(
			'filename' => '', // Lauko šablonas
			'title' => $field['LABEL'], // Lauko pavadinimas
			'id' => '', // [id] atributas
			'rows' => '', // [rows] atributas
			'type' => 'text', // input[type] atributas
			'class' => '', // [class] atributas
			'onkeypress' => '', // [onkeypress] atributas
			'max_length' => '', // [maxlength] atributas
			'note_class' => '', // Failui
			'note_text' => '', // Failui
			//'name' => strtolower($field['NAME']), // [name] atributas
			'name' => strtoupper($name), // [name] atributas
			'value' => $value, // Lauko reikšmė
			'disabled' => '', // [disabled] atributas
			'unallowed' => false // [unallowed] atributas
		);
		$tpl_html_data = array(); // Papildomi šablono kintamieji
		// Įrašo statusas pagal nutylėjimą
		if (empty($table_record) === TRUE) {
			$table_record['STATUS'] = 2;
		}


		// Nustato ar laukas yra neredaguojamas
		if (// Jei įrašo statusas nėra nei 2, nei 3 (t.y. nei tyriamas, nei nagrinėjamas)
				in_array($table_record['STATUS'], array(2, 3)) === FALSE // TODO: kaip su redagavimu?
				// Jei lauko statusas neatitinka įrašo statuso ir lauko statusas nėra 1 (t.y. tirimui ir nagrinėjimui)
				|| ( $field['COMPETENCE'] != $table_record['STATUS'] && $field['COMPETENCE'] != 1 )
				// Jei dokumento negalima redaguoti ir vartotojas nėra dokumento tyrėjas arba nagrinėtojas
				|| ( empty($table['CAN_EDIT']) === TRUE && $this->logic->check_privileges(array('OR' => array('INVESTIGATE_RECORD', 'EXAMINATE_RECORD')), array('table_id' => $table['ID'], 'user_id' => $param['user']['ID'], 'department_id' => $param['user']['STRUCTURE_ID'])) === FALSE )
		) {
			$tpl['disabled'] = ' disabled="disabled"';
			$tpl['unallowed'] = true;
		}

		/*
		 * Lauko informacija pagal rūšį ir tipą
		 */
		// Skaičius
		if ($field['SORT'] == 1 && $field['TYPE'] != 6) {
			$tpl['filename'] = 'input';

			$tpl['type'] = 'text';
			$tpl['onkeypress'] = ' onkeypress="return isNumberKey(event)"';

			$max_length = array(1 => 3, 2 => 5, 3 => 7, 4 => 11, 5 => 19);
		} else
		// Skaičius su kableliu
		if ($field['SORT'] == 1 && $field['TYPE'] == 6) {
			$tpl['filename'] = 'input';

			$tpl['type'] = 'text';
			$tpl['onkeypress'] = ' onkeypress="return isFloatNumberKey(event)"';
			$tpl['value'] = empty($value) === FALSE ? $value : '0.00';
		} else
		// Tekstinis tipas
		if ($field['SORT'] == 2) {
			// Smulkus tekstas
			if ($field['TYPE'] == 7)
				$tpl['filename'] = 'input';
			else
				$tpl['filename'] = 'textarea';

			$rows = array(8 => 2, 9 => 4, 10 => 6);
			$max_length = array(7 => 256, 8 => 65000, 9 => 16000000, 10 => 4000000000);
		} else
		// Veika
		if ($field['SORT'] == 4 && $field['TYPE'] === '23') {
			$tpl['filename'] = 'select';

			$data = $this->logic->get_deed_search_values();
			if (empty($field['DEFAULT']) === FALSE && empty($field['DEFAULT_ID']) === FALSE && empty($value) === TRUE)
				$value = $field['DEFAULT_ID'];

			$str_val = 0;
			foreach ($data as $arr) {
				$selected = '';
				// WTF? SELECT reikšmė pažymima pagal pavadinima, kai DB saugoma value atributo reikšmė
				//if ($value == $arr['LABEL']) {
				if ($value == $arr['ID']) {
					$selected = ' selected="selected"';
					$str_val = $arr['VALUE'];
				}
				$tpl_html_data[] = array('value' => $arr['VALUE'], 'selected' => $selected, 'label' => $arr['LABEL']);
			}
			$value = $str_val;
		} else
		// Klasifikatorius
		if ($field['SORT'] == 4) {
			$tpl['filename'] = 'select';

			//$classifications = $this->logic->get_classification(null, $field['SOURCE']);
			$classifications = $this->logic2->get_classificator(array('PARENT_ID' => $field['SOURCE']));
			if (empty($field['DEFAULT']) === FALSE && empty($field['DEFAULT_ID']) === FALSE && empty($value) === TRUE)
				$value = $field['DEFAULT_ID'];

			$str_val = 0;
			foreach ($classifications as $classification) {
				$selected = '';
				// WTF? SELECT reikšmė pažymima pagal pavadinima, kai DB saugoma value atributo reikšmė
				//if ($value == $classification['LABEL']) {
				if ($value == $classification['ID']) {
					$selected = ' selected="selected"';
					$str_val = $classification['ID'];
				}
				$tpl_html_data[] = array('value' => $classification['ID'], 'selected' => $selected, 'label' => $classification['LABEL']);
			}
			$value = $str_val;

			// Parametras vaikų išrinkimui
			//$param_field = array(
			//	'fields_param' => array('ITYPE' => 'CLASS', 'ITEM_ID' => $value),
			//	'prefix' => $param['prefix'] . 'C' . $value
			//);
			//$tpl['children'] = $this->record_field_children($param, $param_field);
			$tpl['children'] = $this->record_form_parse(array('ITYPE' => 'CLASS', 'ITEM_ID' => $value), $param['record_id'], $param);
		} else
		// Checkbox ir radio
		if ($field['SORT'] == 5 || $field['SORT'] == 8) {
			$tpl['filename'] = 'pick';

			$pk_type = '';
			if ($field['SORT'] == 5) {
				$pk_type = 'checkbox';
				$tpl['name'] = $tpl['name'] . '[]';
				$value = json_decode(stripslashes($value));
			}

			if ($field['SORT'] == 8) {
				$pk_type = 'radio';
				$value = array($value);
			}

			//$str_val = 0;
			//$classifications = $this->logic->get_classification(null, $field['SOURCE']);
			$classifications = $this->logic2->get_classificator(array('PARENT_ID' => $field['SOURCE']));
			foreach ($classifications as $classification) {
				$pk_checked = '';
				//if (in_array($classification['LABEL'], $value)) {
				if (in_array($classification['ID'], $value)) {
					$pk_checked = ' checked="checked"';
					//$str_val = $classification['ID'];
					$value = (int) $classification['ID'];
				}

				$tpl_html_data[] = array(
					'pk_type' => $pk_type,
					'pk_id' => $classification['ID'],
					'pk_label' => $classification['LABEL'],
					'pk_class' => '',
					'pk_name' => $tpl['name'],
					'pk_checked' => $pk_checked,
					'pk_value' => $classification['ID']);
			}

			//$value = $str_val;
		} else
		// Straipsnis
		if ($field['SORT'] == 6) {
			$tpl['filename'] = 'select';

			// Vartotojo straipsniai
			$user_clauses = $this->logic->get_user_clauses(array('PEOPLE_ID' => $param['user']['ID'], 'STRUCTURE_ID' => $param['user']['STRUCTURE_ID']));

			// Reikšmė pagal nutylėjimą
			if (empty($field['DEFAULT']) === FALSE && empty($field['DEFAULT_ID']) === FALSE && empty($value) === TRUE)
				$value = $field['DEFAULT_ID'];

			//$str_val = 0;
			foreach ($user_clauses as $clause) {
				$selected = '';
				if ($value == $clause['ID']) {
					$selected = ' selected="selected"';
					//$str_val = $clause['ID'];
					$value = (int) $clause['ID'];
				}
				$tpl_html_data[] = array(
					'value' => $clause['ID'],
					'selected' => $selected,
					'label' => $clause['NAME']);
			}
			//$value = (int) $str_val;
		} else
		// Datos ir laiko laukai
		if ($field['SORT'] == 7) {
			$tpl['filename'] = 'input';

			$is_empty = false;
			if (empty($value) === TRUE)
				$is_empty = true;

			$value = strtotime($value);
			$max_length = array(11 => 4, 12 => 10, 13 => 8, 14 => 19);

			// Metai
			if ($field['TYPE'] == 11) {
				$tpl['class'] .= ' atp-date-year';
				$value = date('Y', $value);
			} else
			// Data
			if ($field['TYPE'] == 12) {
				$tpl['class'] .= ' atp-date';
				$value = date('Y-m-d', $value);
			} else
			// Laikas
			if ($field['TYPE'] == 13) {
				$tpl['class'] .= ' atp-time';
				$value = date('H:i:s', $value);
			} else
			// Data ir laikas
			if ($field['TYPE'] == 14) {
				$tpl['class'] .= ' atp-datetime';
				$value = date('Y-m-d H:i:s', $value);
			}

			if ($is_empty === TRUE)
				$value = '';

			$tpl['value'] = $value;
		} else
		// Antraštė
		if ($field['SORT'] == 9) {
			$tpl['filename'] = 'text';
			$tpl['title'] = $tpl['title'];
			$tpl['value'] = $field['TEXT'];
		} else
		// Failas
		if ($field['SORT'] == 10) {
			$tpl['filename'] = 'input';

			$tpl['type'] = 'file';
			$tpl['name'] = $tpl['name'] . '[]';
			$tpl['class'] = '-file' . $tpl['class'];

			$file_params = $this->logic->get_file_params(array('TABLE_ID' => $field['ITEM_ID'], 'STRUCTURE_ID' => $field['ID'], 'LIMIT' => 1));
			if (!empty($file_params)) {
				$file_params = array_shift($file_params);

				$tpl['note_class'] = ' atp-note-show';
				$tpl['note_text'] = 'Failų kiekis: <span class="atp-file-cnt">' . $file_params['CNT'] . '</span>, dydis: ' . $file_params['SIZE_FROM'] . ' - ' . $file_params['SIZE_TO'] . ' Mb';
			}
		}

		// Šablono generavimas
		if (!empty($tpl['filename'])) {
			$tmpl = $this->core->getTemplate('template_' . $tpl['filename']);

			// [row] atributo formavimas
			if (!empty($rows[$field['TYPE']]))
				$tpl['rows'] = ' rows="' . $rows[$field['TYPE']] . '"';
			// [maxlength] atributo formavimas
			if (!empty($max_length[$field['TYPE']]))
				$tpl['max_length'] = ' maxlength="' . $max_length[$field['TYPE']] . '"';
			// [unallowed] atributo formavimas
			if ($tpl['unallowed'] === TRUE)
				$tpl['disabled'] .= ' unallowed="unallowed"';

			// Jei ne veika/klasifikatorius, checkbox, straipsnis, radio button
			if (in_array($field['SORT'], array(4, 5, 6, 8)) === FALSE) {
				if (isset($tpl['value']) === TRUE && empty($tpl['value']) === TRUE && empty($field['DEFAULT']) === FALSE)
					$tpl['value'] = $field['DEFAULT'];
				$result = $this->core->returnHTML($tpl, $tmpl[0]);
			} else {
				$tpl['lng_not_chosen'] = $this->lng('not_chosen');
				$result = $this->core->returnHTML($tpl, $tmpl[0]);
				foreach ($tpl_html_data as $template_dt) {
					foreach ($template_dt as $val => $dt)
						$tpl[$val] = $dt;

					$result .= $this->core->returnHTML($tpl, $tmpl[1]);
				}
				$result .= $this->core->returnHTML($tpl, $tmpl[2]);
			}
		}
		return $result;
	}

	public function table_record_edit() {
		// Jei pasitaikytu request'as su "record_id" parametru
		if (empty($_REQUEST['record_id']) === FALSE && empty($_REQUEST['id']) === TRUE) {
			$_REQUEST['id'] = $_REQUEST['record_id'];
			unset($_REQUEST['record_id']);
		}

		// Jei nepateikas ID, tuomet tai naujo įrašo kūrimo forma. Gaunamas / kuriamas laikinas dokumento įrašas
		$is_temp_record = false;
		if (empty($_REQUEST['id']) === TRUE) {
			if (empty($_REQUEST['table_id']) === TRUE) {
				trigger_error('Document ID is not passed', E_USER_ERROR);
			}

			$record_nr = $this->logic->create_record_empty(array('DOCUMENT_ID' => $_REQUEST['table_id']));
			if (empty($record_nr) === FALSE) {
				$_REQUEST['id'] = $record_nr;
			}
			$is_temp_record = true;
		}

		$record = $this->logic2->get_record_relation($_REQUEST['id']);
		if (empty($record) === TRUE) {
			trigger_error('Record not found', E_USER_ERROR);
			exit;
		}

		$this->logic->_r_r_id = $record['ID']; // Jei ką, jis visada set'intas
		$this->logic->_r_record_id = empty($record['RECORD_ID']) ? 0 : $record['RECORD_ID']; // perziurai / redagavimui
		$this->logic->_r_table_id = (int) $record['TABLE_TO_ID'];
		//$this->logic->_r_table_to_id = empty($_REQUEST['table_path']) ? $this->logic->_r_table_id : $_REQUEST['table_path'];
		$this->logic->_r_table_to_id = $this->logic->_r_table_id;

		// Dokumento duomenys
		//$this->table = $this->logic->get_table($this->logic->_r_table_id);
		$this->table = $this->logic2->get_document($this->logic->_r_table_id);

		/*
		  // Vartotojas turi turėti galimybę peržiurėti dokumento įrašą neturėdamas teisės į jo redagavimą
		  // Tikrina ar vartotojas turi teisę redaguoti įrašą
		  if ($this->logic->privileges('table_record_edit') === FALSE) {
		  $this->core->set_message('error', 'Priskirtos vartotojo teisės nesuteikia redagavimo teisės.');
		  header('Location: ' . GLOBAL_SITE_URL . '/subsystems/atp/index.php?m=4&table_id=' . $this->logic->_r_table_id);
		  exit;
		  } */

		// Tipo "Automatinis saugojimas" prieš veiksmą
		if (empty($_POST) === FALSE) {
			// Jei tai naujas dokumentas
			//if (empty($this->logic->_r_record_id) === TRUE) {
			if ($is_temp_record === TRUE) {
				// KAM? Jei vykdoma gauti / kurti laikiną dokumento įrašą ($is_temp_record yra TRUE), tai laikinas įrašas egzistuoja
				/*
				  // Tikrina ar dokumentui yra sukurtas laikinas dokumentas
				  $qry = 'SELECT A.ID, B.RECORD_ID as RECORD_NR
				  FROM ' . PREFIX . 'ATP_RECORD_UNSAVED A
				  INNER JOIN ' . PREFIX . 'ATP_RECORD_RELATIONS B
				  ON
				  B.ID = A.RECORD_ID AND
				  B.TABLE_TO_ID = :DOCUMENT_ID AND
				  B.IS_LAST = 1
				  WHERE
				  A.WORKER_ID = :WORKER_ID';
				  $qry_arg = array(
				  'WORKER_ID' => $this->core->user->worker['ID'],
				  'DOCUMENT_ID' => $this->logic->_r_table_id
				  );
				  $result = $this->core->db_query_fast($qry, $qry_arg);
				  $count = mysql_num_rows($result); */
				//if (isset($_POST['submit_path']) && $count) {
				if (isset($_POST['submit_path'])) {
					// TODO: failai i post kitur
					foreach ($_FILES as $key => $val) {
						$_POST[$key] = '1';
					}
					$this->logic->_r_values = $_POST;
					/* 	$files_fields = $this->logic2->get_fields_all(array(
					  'ITEM_ID' => $this->logic->_r_table_id,
					  'ITYPE' => 'DOC',
					  'SORT' => 10));
					  $table_record = $this->logic->get_table_records($this->logic->_r_table_id, $this->logic->_r_record_id, null, null, true);
					  $table_record = array_shift($table_record);
					  foreach ($files_fields as $file_field) {
					  $saved_files = $this->logic2->get_record_files(array(
					  'FIELD_ID' => $file_field['ID'],
					  'TABLE_ID' => $this->logic->_r_table_id,
					  'RECORD_ID' => $table_record['ID']), FALSE, null, 'ID, FILE_ID');
					  foreach ($saved_files as $saved_file) {
					  $this->logic->_r_files[$file_field['ID']][] = $saved_file['FILE_ID'];
					  }
					  }
					  unset($table_record, $file_field, $saved_files, $saved_file);
					 */
					$this->logic->create_record('save', 'return_id');
				} else
				//if (isset($_POST['save_new']) && $count) {
				if (isset($_POST['save_new'])) {
					unset($_POST['save_new']);
					$_POST['save'] = 'Išsaugoti';
				}
			} else {
				if (isset($_POST['submit_path'])) {
					foreach ($_FILES as $key => $val) {
						$_POST[$key] = '1';
					}

					$this->logic->_r_values = $_POST;
					$this->logic->create_record('save', 'return_id');
				} else if (isset($_POST['view_document']) || isset($_POST['register_document']) || isset($_POST['register_case'])) {
					foreach ($_FILES as $key => $val) {
						$_POST[$key] = '1';
					}

					$this->logic->_r_values = $_POST;
					$this->logic->create_record('save', 'return_id');
					$this->core->unset_messages_sess();
					unset($this->logic->_r_values);

					// Jei dokumentas užregistruotas, tai POST'u neperduodamas "template_id"
					if (empty($_POST['template_id']) === TRUE) {
						/* $document_record = current((array) $this->logic->get_table_records($this->logic->_r_table_id, $this->logic->_r_record_id, null, null, true));
						  //$document_record = $this->logic2->get_record_relation($this->logic->_r_record_id); */
						$_POST['template_id'] = $record['REGISTERED_TEMPLATE_ID'];
					}

					if (isset($_POST['view_document'])) {
						$url = 'index.php?m=9&'
								//. '&record_id=' . $this->logic->_r_record_id
								. '&id=' . $this->logic->_r_r_id
								. '&template_id=' . $_POST['template_id'];
					} else if (isset($_POST['register_document'])) {
						$url = 'index.php?m=128'
								. '&table_id=' . $this->logic->_r_table_id
								//. '&record_id=' . $this->logic->_r_record_id
								. '&id=' . $this->logic->_r_r_id
								. '&template_id=' . $_POST['template_id'];
					} else if (isset($_POST['register_case'])) {
						$url = 'index.php?m=138'
								. '&table_id=' . $this->logic->_r_table_id
								. '&id=' . $this->logic->_r_r_id
								. '&template_id=' . $_POST['template_id'];
					}

					header('Location: ' . $url);
					exit;
				}
			}
			$this->core->unset_messages_sess();
			unset($this->logic->_r_values);
		}

		// ?
		$table_record = array();
		if (empty($this->logic->_r_r_id) === FALSE) {
			// Visų užpildytų laukų reikšmės
			$shit = current($this->logic->get_table_records(array(
						'DOC_ID' => $this->logic->_r_table_id,
						'RECORD_NR' => $this->logic->_r_r_id,
						'START' => null,
						'OFFSET' => null,
						'WITH_EXTRA' => true,
						'INCLUDE_OWNER' => false
			))); // TODO: pakeisti // get_table_records cia tik del STATUS
			$this->table_record = $this->logic2->get_record_values(array(
				'RECORD' => $this->logic->_r_r_id,
				'WITH_NAME' => true,
			));
			$this->table_record['STATUS'] = $shit['STATUS'];
			$this->table_record['RID'] = $shit['RID'];
		}

		if (!empty($_POST)) {
			$action = null;
			if (isset($_POST['save']) && $this->logic->privileges('table_record_edit_save') === TRUE) {
				$action = 'save';
			} elseif (isset($_POST['save_new']) && $this->logic->privileges('table_record_edit_save_new') === TRUE) {

				$fields = $this->logic2->get_fields_all(array(
					'ITEM_ID' => $this->logic->_r_table_id,
					'ITYPE' => 'DOC'));

				// TODO: RECORD_ID | į RECORD_ID paduoti RECORD_RELATION.ID
				foreach ($fields as &$field) {
					if ($field['SORT'] === '10') {
						$files = $this->logic2->get_record_files(array(
							'FIELD_ID' => $field['ID'],
							/* 'TABLE_ID' => $this->logic->_r_table_id,
							  'RECORD_ID' => $this->table_record['ID'] */
							'RECORD_ID' => $this->logic->_r_r_id
								), FALSE, null, 'ID, FILE_ID');
						foreach ($files as $file) {
							$this->logic->_r_files[$field['ID']][] = $file['FILE_ID'];
						}
					}
				}

				$action = 'save_new';
			} elseif (isset($_POST['finish_investigation']) && $this->logic->privileges('table_record_edit_finish_investigation') === TRUE) {
				$action = 'finish_investigation';
			} elseif (isset($_POST['finish_idle']) && $this->logic->privileges('table_record_edit_finish_idle') === TRUE) {
				$action = 'finish_idle';
			} elseif (isset($_POST['finish_examination']) && $this->logic->privileges('table_record_edit_finish_examination') === TRUE) {
				$action = 'finish_examination';
			} elseif (isset($_POST['finish_proccess']) && $this->logic->privileges('table_record_edit_finish_proccess') === TRUE) {
				$action = 'finish_proccess';
			} elseif (isset($_POST['submit_path'])) {
				if ($this->logic->privileges('table_record_edit_submit_path', array('table_id' => $_REQUEST['table_path'])) === TRUE) {

					$this->logic->_r_table_to_id = $_REQUEST['table_path'];

					//<editor-fold defaultstate="collapsed" desc="Nebenaudojamas laukų riekšmių surinkimas">
					if (11 === 123456789) {
						exit('Nebenaudojamas laukų riekšmių surinkimas');
						// Negalima tos pačios dokumento versijos antrą kartą perduoti į kitą dokumentą pagrindiniame kelyje
						$current_record = $this->core->logic2->get_record_relations(array('RECORD_ID' => $this->logic->_r_record_id, 'IS_LAST' => 1), true, null, 'A.*');
						$main_doc_path = array(56, 52, 36, 57);
						if (in_array($current_record['TABLE_TO_ID'], $main_doc_path) === TRUE && in_array($this->logic->_r_table_to_id, $main_doc_path) === TRUE) {
							$next_record = $this->core->logic2->get_record_relations(array('RECORD_FROM_ID' => $current_record['ID'], 'TABLE_TO_ID' => $this->logic->_r_table_to_id), true, null, 'A.*');

							if (empty($next_record) === FALSE) {
								$this->core->set_message('error', 'Iš šios dokumento versijos jau buvo sukurtas sekantis dokumentas');
								$url = 'index.php?m=5&table_id=' . $this->logic->_r_table_id . '&record_id=' . $this->logic->_r_record_id;
								header('Location: ' . $url);
								exit;
							}
						}


						$new_values = $this->logic->table_compatibility(array(
							'RECORD_NR' => $current_record['ID'],
							'FROM_TABLE' => $this->logic->_r_table_id,
							'TO_TABLE' => $this->logic->_r_table_to_id));

						$new_values = array_change_key_case((array) $new_values, CASE_LOWER);
						$this->logic->_r_values = $new_values;

						$relations = $this->logic->get_table_relations($this->logic->_r_table_id, $this->logic->_r_table_to_id);

						foreach ($relations as $relation) {
							//$field = $this->logic2->get_fields(array('ID' => $relation['COLUMN_PARENT_ID']));
							$field = $this->logic2->get_fields(array('ID' => $relation['COLUMN_ID']));
							// Veika // Veikos lauko ryšys perduoda visus žemesnio lygio laukus (laukus, kurie yra saugomi laukų lent.)
							if ($field['TYPE'] === '23') {
								$data = array(
									'FROM' => array(
										//'TABLE' => $relation['TABLE_ID'],
										'TABLE' => $relation['TABLE_PARENT_ID'],
										//'FIELD_ID' => $relation['COLUMN_ID'],
										'FIELD_ID' => $relation['COLUMN_PARENT_ID'],
										//'FIELD_NAME' => $relation['COLUMN_NAME'],
										'FIELD_NAME' => $relation['COLUMN_PARENT_NAME'],
										//'VALUE' => $this->logic->_r_values[strtolower($relation['COLUMN_PARENT_NAME'])]
										'VALUE' => $this->logic->_r_values[strtolower($relation['COLUMN_NAME'])]
									),
									'TO' => array(
										//'TABLE' => $relation['TABLE_PARENT_ID'],
										'TABLE' => $relation['TABLE_ID'],
										//'FIELD_ID' => $relation['COLUMN_PARENT_ID'],
										'FIELD_ID' => $relation['COLUMN_ID'],
										//'FIELD_NAME' => $relation['COLUMN_PARENT_NAME']
										'FIELD_NAME' => $relation['COLUMN_NAME']
									)
								);

								// Gauna įrašo datą, pagal kurią atrenkamas teisės aktas
								$from_table = $this->logic2->get_document($_POST['table_id']);
								$from_structures = $this->logic->get_extra_fields($this->logic->_r_table_id, 'DOC');
								$from_date_field = $from_structures[$from_table["DATE_FIELD"]];
								//$from_record_table = $this->logic->get_table_record($this->logic->_r_record_id, 'C.*, B.ID RID');
								$from_record_table = $this->logic2->get_record_table($this->logic->_r_record_id, 'C.*, A.ID RID');
								$from_date = date('Y-m-d H:i:s');
								if (empty($from_date_field) === FALSE) {
									if (isset($from_record_table[$from_date_field['NAME']]))
										$from_date = $from_record_table[$from_date_field['NAME']];
									else
									//$from_date = $this->logic->get_extra_field_value($this->logic->_r_table_id, $from_record_table['ID'], $from_date_field['ID']);
									//$from_date = $this->logic->get_extra_field_value($this->logic->_r_table_id, $from_record_table['RECORD_ID'], $from_date_field['ID']);
										$from_date = $this->logic->get_extra_field_value($from_record_table['RID'], $from_date_field['ID']);
								}

								$params = array(
									'ID' => $data['FROM']['VALUE'],
									'TYPE' => 'DEED',
									'RECORD_DATE' => $from_date);

								// Žemesni veikos laukai
								$deed_children_fields = $this->logic->get_fields_by_type_and_id($params);

								$new_values = array();
								foreach ($deed_children_fields as $child_field) {
									//$new_values[strtolower($child_field['NAME'])] = $this->logic->get_extra_field_value($this->logic->_r_table_id, $from_record_table['ID'], $child_field['ID']);
									// TODO: [done] Perduodant dokumento įrašo nr. negauna scol_1 ir acol_5
									//$new_values[strtolower($child_field['NAME'])] = $this->logic->get_extra_field_value($this->logic->_r_table_id, $from_record_table['RECORD_ID'], $child_field['ID']);
									$new_values[strtolower($child_field['NAME'])] = $this->logic->get_extra_field_value($from_record_table['RID'], $child_field['ID']);
								}
								$this->logic->_r_values = array_merge((array) $this->logic->_r_values, $new_values);
							} else
							// Failas
							if ($field['SORT'] === '10' && $this->logic->_r_values[strtolower($field['NAME'])] === '1') {
								//$parent_field = $this->logic2->get_fields(array('ID' => $relation['COLUMN_ID']));
								$parent_field = $this->logic2->get_fields(array('ID' => $relation['COLUMN_PARENT_ID']));

								$parent_files = $this->logic2->get_record_files(array(
									'FIELD_ID' => $parent_field['ID'],
									//'TABLE_ID' => $this->logic->_r_table_id,
									//'RECORD_ID' => $this->table_record['ID']
									'RECORD_ID' => $this->logic->_r_r_id
										), FALSE, null, 'ID, FILE_ID');
								foreach ($parent_files as $file) {
									$this->logic->_r_files[$field['ID']][] = $file['FILE_ID'];
								}
								unset($parent_files);
							}
						}

						$this->logic->_r_path = TRUE;
						$action = 'submit_path';
					}//</editor-fold>

					$record = new ATP_Document_Record($this->logic->_r_r_id, $this->core);
					$manager = new ATP_Document_Record_Manage($record, $this->core);
					$manager->transferTo($this->logic->_r_table_to_id);
					$this->core->set_messages($manager->result['messages']);
					header('Location: ' . $manager->result['redirect']);
					exit;
				} else {
					$this->core->set_message('error', 'Neturite teisės kurti naujo dokumento');
					$url = 'index.php?m=5&table_id=' . $this->logic->_r_table_id . '&id=' . $this->logic->_r_r_id;
				}
			} else {
				$this->core->set_message('error', 'Pasirinktas veiksmas draudžimas');
				$url = 'index.php?m=4&table_id=' . $this->logic->_r_table_id;
			}

			if ($action !== NULL) {
				if ($action !== 'submit_path')
					$this->logic->_r_values = $_POST;
				else {
					//$this->logic->_r_values = array_merge
				}

				$this->logic->create_record($action);
//die('after create');
			} else {
				header('Location: ' . $url);
			}
			exit;
		}

		$this->field_sort = $this->logic->get_field_sort();
		$this->field_type = $this->logic->get_field_type();
		$this->table_structure = $this->logic->get_table_fields($this->logic->_r_table_id);

		// Surenka dokumento laukų sąryšius (lauko sąryšis su teviniu lauku)
		$this->table_column_relation = array();
		foreach ($this->table_structure as $structure) {
			if (empty($structure['RELATION']) === FALSE)
				$this->table_column_relation[$structure['ID']] = array('COLUMN_ID' => $structure['ID'], 'COLUMN_PARENT_ID' => $structure['RELATION']);
		}

		// Pagal laukų sąryšius suformuoja laukų hierarchiją
		foreach ($this->table_structure as $structure) {
			$relation_status = $this->logic->get_column_relation_status($structure['ID'], $this->table_column_relation);
			if ($relation_status == 1)
				$column_relations[$structure['ID']] = $this->logic->get_column_relation_order($structure['ID'], $this->table_column_relation);
			elseif ($relation_status == 2)
				$column_relations[$structure['ID']] = array();
		}

		// Suformuoja įvedimo laukų HTML'ą
		$template_html = $this->view_column_order($column_relations);

		$tmpl = $this->core->Template();
		$tmpl_handler = 'table_record_edit_file';
		$tmpl->set_file($tmpl_handler, 'table_record_edit.tpl');

		$Marker_2 = $tmpl_handler . 'Marker_2';
		$tmpl->set_block($tmpl_handler, 'Marker_2', $Marker_2);
		$select_document_template_block = $tmpl_handler . 'select_document_template';
		$tmpl->set_block($tmpl_handler, 'select_document_template', $select_document_template_block);
		$button_register_document_block = $tmpl_handler . 'button_register_document';
		$tmpl->set_block($tmpl_handler, 'button_register_document', $button_register_document_block);
		$button_register_case_block = $tmpl_handler . 'button_register_case';
		$tmpl->set_block($tmpl_handler, 'button_register_case', $button_register_case_block);
		$button_register_post_data_block = $tmpl_handler . 'button_get_RN';
		$tmpl->set_block($tmpl_handler, 'button_get_RN', $button_register_post_data_block);
		$create_document = $tmpl_handler . 'create_document';
		$tmpl->set_block($tmpl_handler, 'create_document', $create_document);
		$Marker_10 = $tmpl_handler . 'Marker_10';
		$tmpl->set_block($tmpl_handler, 'Marker_10', $Marker_10);
		$create_new = $tmpl_handler . 'create_new';
		$tmpl->set_block($tmpl_handler, 'create_new', $create_new);

		// Įrašo ir tiriančio asmens duomenų laukas
		$doc_author_field = $tmpl_handler . 'doc_author_field';
		$tmpl->set_block($tmpl_handler, 'doc_author_field', $doc_author_field);
		// Įrašo ir tiriančio asmens duomenys
		$doc_author = $tmpl_handler . 'doc_author';
		$tmpl->set_block($tmpl_handler, 'doc_author', $doc_author);

		// Dokumento būsenos pasirinkimas
		$doc_status_option = $tmpl_handler . 'doc_status_option';
		$tmpl->set_block($tmpl_handler, 'doc_status_option', $doc_status_option);
		// Dokumento būsenos blokas
		$doc_status = $tmpl_handler . 'doc_status_block';
		$tmpl->set_block($tmpl_handler, 'doc_status_block', $doc_status);

		$this->core->_load_files['js'][] = 'jquery.form.min.js';


		// Formos mygtukai
		$buttons = array(
			'button_vmi', // "Tikrinti su VMI"
			/**/ 'buttons_group', // Apglėbia 'button_vmi'
			'button_save', // "Išsaugoti"
			'button_save_new', // "Išsaugoti naują"
			'button_finish_investigation', // "Baigti tyrimą"
			'button_finish_examination', // "Baigti nagrinėjimą"
			'button_finish_proccess', // "Baigti procesą"
			'button_finish_idle' // "Baigti tarpinę stadiją"
		);
		foreach ($buttons as $button) {
			${$button} = $tmpl_handler . $button;
			$tmpl->set_block($tmpl_handler, $button, ${$button});
		}

		// Blokas dokumento spausdinimui / registravimui
		if (empty($this->logic->_r_r_id) === FALSE) {

			// Iš viso dokumento šablonų
			$templates_count = count($this->core->logic2->get_template(array('TABLE_ID' => $record_obj->record['TABLE_TO_ID'])));
			// Dokumento šablonai atitinkantys sąlygas
			$templates = $this->logic2->get_template_valid($this->logic->_r_r_id);

			// Jei yra priskirtų šablonų
			if (empty($templates_count) === FALSE) {

				$current_template = array(); // Registruoto šablono informacija
				// Išvedamas šablonų sąrašas arba registruotas šablonas
				foreach ($templates as $template) {
					if ((int) $record['REGISTERED_TEMPLATE_ID'] !== 0) {
						if ((int) $template['ID'] === (int) $record['REGISTERED_TEMPLATE_ID']) {
							$tmpl->set_var(array(
								'template_value' => $template['ID'],
								'template_label' => $template['LABEL']));
							$tmpl->parse($Marker_2, 'Marker_2');
							$current_template = $template;
							break;
						}
					} else {
						$tmpl->set_var(array(
							'template_value' => $template['ID'],
							'template_label' => $template['LABEL']));
						$tmpl->parse($Marker_2, 'Marker_2', true);
					}
				}

				$attributes = array();
				//if ((int) $record['REGISTERED_TEMPLATE_ID'] === 0) {
				//$current_template = reset($templates); // TODO: [done] kam? toliau nebepanaudojamas // pašalinta
				//} else {
				// Jei dokumentas registruotas, tai neleidžia keisti šablono pasirinkimo
				if (empty($record['REGISTERED_TEMPLATE_ID']) === FALSE) {
					$attributes[] = 'disabled="disabled"';
					$attributes[] = ' title="' . htmlspecialchars($current_template['LABEL']) . '"';
				}
				$tmpl->set_var('attributes', join(' ', $attributes));
				$tmpl->parse($select_document_template_block, 'select_document_template');


				// Nustato ar mygtukas "Užregistruoti bylą" yra matomas // Matomas "Protokolo" dokumente, nagrinėjimo stadijoje, nagrinėjančiam pareigūnui
				$button_register_case_valid = false;
				if ((int) $record['STATUS'] === 3 && (int) $this->table['ID'] === 36) {

					$workers = $this->logic2->get_record_worker(array('A.RECORD_ID' => $record['ID'],
						'A.WORKER_ID' => (int) $this->core->user->worker['ID'],
						'A.TYPE IN("exam", "next")'));

					if (count($workers))
						$button_register_case_valid = true;
				}

				//$button_register_post_data_block - RN info blokas
				$button_register_post_data_valid = false;
				if (in_array($record['TABLE_TO_ID'], array(53,48, 54/* DOKUMENTO RUSIS,57,36 */))) { //)/* && in_array($this->core->user->worker["STRUCTURE_ID"], array(1/*SKYRIAUS ID,57,54,48,36*/))*/ ) {
					$_POST['record'] = $record['ID'];
					$RN = new ATP_Post_RN($this->core);
					$rn_code = $RN->get_code();
					$rn_free_code = '';
					$get_new_button = '<input class="atp-button get_RN_code" name="get_RN" type="button" value="gauti RN kodą" />';
					$rn_checkbox = '';//$rn_checkbox = '<input type="checkbox" id="ACQ_CONFIRMATION" name="ACQ_CONFIRMATION" value="1">&nbsp;Su&nbsp;patvirtinimu.';
					if (empty($rn_code) === false && empty($RN->data['POST_CODE']) === false) {
						$rn_checkbox ='';//$rn_checkbox = ($RN->data['ACQ_CONFIRMATION'] == 1 ? ', su įteikimo patvirtinimu.' : ', be įteikimo patvirtinimo.');
						$get_new_button = 'RN kodas: ' . $rn_code . '';
						$rn_free_code = '<br>' . '<input class="atp-button free_RN_code" name="free_RN_code" type="button" value="atlaisvinti RN kodą" />';
					}
					$rn_info = $get_new_button . $rn_checkbox . $rn_free_code;
					$tmpl->set_var(array('RN_FIELD_DATA' => $rn_info));
					$button_register_post_data_valid = true;
				}

				// Mygtukas "Užregistruoti dokumentą" // Jei matomas "Užregistruoti bylą", tai šis slepiamas
				if ((int) $record['REGISTERED_TEMPLATE_ID'] === 0 && $button_register_case_valid === FALSE) {
					$tmpl->parse($button_register_document_block, 'button_register_document');
				} else {
					$tmpl->set_var($button_register_document_block, '');
				}
				// Mygtukas "Užregistruoti bylą"
				if ($button_register_case_valid === TRUE) {
					$tmpl->parse($button_register_case_block, 'button_register_case');
				} else {
					$tmpl->set_var($button_register_case_block, '');
				}
				// Mygtukai "Gauti RN" Patikrinti |RN
				if ($button_register_post_data_valid === TRUE) {
					$tmpl->parse($button_register_post_data_block, 'button_get_RN');
				} else {
					$tmpl->set_var($button_register_post_data_block, '');
				}


				$tmpl->parse($create_document, 'create_document');
			}
		}
		if (empty($templates_count) === TRUE)
			$tmpl->clean($create_document);


		// Kai dokumentas yra sukurtas, išvesti kūrėjo informaciją
		if (empty($this->logic->_r_r_id) === FALSE) {
			$fields = $this->logic->record_worker_info_to_fields($this->logic->_r_r_id/* , (int) $this->table['STATUS'] */);
			$parse_fields = array('TICOL_0', 'TICOL_1', 'TICOL_2', 'TICOL_3');
			foreach ($fields as &$field) {
				if (empty($field['VALUE']) === TRUE || in_array($field['NAME'], $parse_fields) === FALSE) {
					continue;
				}
				$tmpl->set_var(array(
					'title' => $field['LABEL'],
					'value' => $field['VALUE']
				));
				$tmpl->parse($doc_author_field, 'doc_author_field', true);
			}
			$tmpl->parse($doc_author, 'doc_author');
		}


		//$statuses = $this->logic2->get_status_relations(array('DOCUMENT_ID' => $this->logic->_r_table_id, 'ATP_STATUS_ID' => $record['STATUS']));
		$statuses = $this->logic2->get_status();
		$tmpl->set_var(array(
			'selected' => ($selected ? ' selected="selected"' : ''),
			'value' => 0,
			'label' => $this->lng('not_chosen')
		));
		$tmpl->parse($doc_status_option, 'doc_status_option');
		foreach ($statuses as &$status) {
			$stat = $this->logic2->get_status(array('ID' => $status['ID']));
			$tmpl->set_var(array(
				'selected' => ($record['DOC_STATUS'] === $stat['ID'] ? ' selected="selected"' : ''),
				'value' => $stat['ID'],
				'label' => $stat['LABEL']
			));
			$tmpl->parse($doc_status_option, 'doc_status_option', true);
		}

		if ($this->logic->_r_table_id == 57 || $_SERVER['REMOTE_ADDR'] === '127.0.0.1')
			$tmpl->parse($doc_status, 'doc_status_block');

		$create_next = false;
		if (in_array($record['STATUS'], array(4)) === TRUE || ($this->table['CAN_NEXT'] === '1' && in_array($record['STATUS'], range(1, 4)) === TRUE)) {

			if ($this->logic->_r_table_id == $this->logic->_r_table_to_id && !empty($this->logic->_r_r_id)) {
				$table_path = $this->logic->get_table_path($this->logic->_r_table_id);
				if (!empty($table_path)) {
					$table_path_tmp = array($this->table['ID'] => $this->table);
					foreach ($table_path as $type => $path) {
						foreach ($path as $table) {
							$table_path_tmp[$table['ID']] = $table;
						}
					}

					$table_path = $this->logic->sort_children_to_parent($table_path_tmp, 'ID', 'PARENT_ID');


					foreach ($table_path as $key => $path) {
						$opt_extra = null;
						if ($this->logic->privileges('table_record_edit_button_submit_path', array('table_id' => $path['ID'])) === FALSE)
							$opt_extra = ' disabled="disabled"';
						if ($path['ID'] == $this->table['ID']) {
							$opt_extra = ' disabled="disabled" class="current" style="font-weight: bold!important;"';
						}

						$tmpl->set_var(array(
							'value' => $opt_extra === NULL ? $path['ID'] : '',
							'label' => repeatString('&nbsp;&nbsp;&nbsp;', (int) $path['depth']) . $path['LABEL'],
							'opt_extra' => $opt_extra));
						$tmpl->parse($Marker_10, 'Marker_10', $key);

						if ($opt_extra === NULL) {
							$create_next = true;
						}
					}

					$tmpl->set_var($this->core->set_lng_vars(array('create')));

					// TODO: šalint?
					if ($create_next === FALSE) {
						trigger_error('User "' . $this->core->user->id . '" does not have privileges to create next document from "' . $this->logic->_r_record_id . '" document', E_USER_WARNING);
					}
				}
			}
		}

		if ($create_next === TRUE) {
			$tmpl->parse($create_new, 'create_new');
		} else {
			$tmpl->clean($create_new);

			// TODO: nereikia sios salygos isimti is else?
			if ($this->logic->check_privileges('EDIT_RECORD', array('table_id' => $this->logic->_r_table_id, 'user_id' => $this->core->user->id, 'department_id' => $this->core->user->department->id)) === TRUE) {
				$tmpl->set_var(array(
					'table_id' => $this->logic->_r_table_id,
					'record_id' => $this->logic->_r_record_id));
			}
		}


		// Veiksmų mygtukai
		if ($this->logic->privileges('table_record_edit_button_save') === TRUE) {
			$tmpl->parse($button_save, 'button_save');
		} else
			$tmpl->clean($button_save);
		if ($this->logic->privileges('table_record_edit_button_save_new') === TRUE)
			$tmpl->parse($button_save_new, 'button_save_new');
		else
			$tmpl->clean($button_save_new);
		if ($this->logic->privileges('table_record_edit_button_finish_investigation') === TRUE)
			$tmpl->parse($button_finish_investigation, 'button_finish_investigation');
		else
			$tmpl->clean($button_finish_investigation);
		if ($this->logic->privileges('table_record_edit_button_finish_examination') === TRUE)
			$tmpl->parse($button_finish_examination, 'button_finish_examination');
		else
			$tmpl->clean($button_finish_examination);
		if ($this->logic->privileges('table_record_edit_button_finish_proccess') === TRUE)
			$tmpl->parse($button_finish_proccess, 'button_finish_proccess');
		else
			$tmpl->clean($button_finish_proccess);

		if ($this->logic->privileges('table_record_edit_button_finish_idle') === TRUE)
			$tmpl->parse($button_finish_idle, 'button_finish_idle');
		else
			$tmpl->clean($button_finish_idle);

		$tmpl->parse($button_vmi, 'button_vmi');
		$tmpl->parse($buttons_group, 'buttons_group');

		// Dokumento įrašo pranešimas
		$doc_message = null;
		if (in_array($record['STATUS'], range(3, 5)) === TRUE && empty($this->table['CAN_IDLE']) === FALSE) {
			$record_paid = $this->logic2->get_db(PREFIX . 'ATP_RECORD_PAID', array('RECORD_ID' => $record['ID']), true, null, false);

			// Jei apmokėta
			if ((int) $record_paid['STATUS'] === 1) {
				$message_tpl = '<div class="atp-doc-message"><div class="atp-doc-message-{$type}">{$message} ({$date}{$paid_date})</div><div class="separator"></div></div>';

				// AN būsena
				$to = ((bool) $record['IS_PAID'] === TRUE ? array('success', 'Įvykdytas AN') : array('error', 'Neįvykdytas AN'));

				// Patikrinimo data
				$to[2] = 'patikrinta: ' . $record_paid['CREATE_DATE'];

				// Apmokėjimo data
				$payment_date = null;
				// TODO: RECORD_ID | Vietoje RECORD_ID perduoti RECORD_RELATIONS.ID | Ar tikrai?
				$payments = $this->logic2->get_payments(array('DOCUMENT_ID' => $this->logic->_r_table_id, 'RECORD_ID' => $this->logic->_r_record_id));
				foreach ($payments as &$payment) {
					$found_payments = $this->core->getRDBWsData('getVMISearchResult', array('registry' => 'vmi', 'SEARCH_ID' => $payment['PAYMENT_ID']));
					$payment_date = max($payment_date, strtotime($found_payments[0]['OPERACIJOS_DATA']));
				}
				if (empty($payment_date) === FALSE) {
					$to[3] = '; apmokėta: ' . date('Y-m-d', $payment_date);
				}


				$doc_message = str_replace(array('{$type}', '{$message}', '{$date}', '{$paid_date}'), $to, $message_tpl);
			}
		}


		// Dokumento administravime nurodyti formos įvedimo laukų pavadinimai
		$all_fields = $this->logic2->get_fields_all(array('ITYPE' => 'DOC', 'ITEM_ID' => $this->table['ID']));
		$form_fields = array(
			'date_field' => 'DATE_FIELD',
			'date_of_decision_field' => 'DATE_OF_DECISION_FIELD',
			'penalty_field' => 'PENALTY_FIELD',
			'class_type_field' => 'CLASS_TYPE',
			'examinator_date_field' => 'EXAMINATOR_DATE',
			'examinator_start_time_field' => 'EXAMINATOR_START_TIME',
			'examinator_finish_time_field' => 'EXAMINATOR_FINISH_TIME',
			'table_person_code_field' => 'PERSON_CODE_FIELD'
		);
		foreach ($form_fields as $key => $column) {
			$form_fields[$key] = strtolower($all_fields[$this->table[$column]]['NAME']);
		}
		$tmpl->set_var('form_fields', json_encode($form_fields));


		if ($_GET['id'] === NULL) {
			$currentCatIdArray = $_GET["table_id"];
		} else {
			$currentRecId = $_GET['id'];
			$currentCatIdArray = explode("p", $currentRecId);
			$currentCatIdArray = $currentCatIdArray[0];
		}

		$tmpl->set_var(array(
			'title' => $this->table['LABEL'] . ' - ' . $this->logic->_r_record_id,
			'advanced_class' => $currentCatIdArray,
			'sys_message' => $this->core->get_messages(),
			'doc_message' => $doc_message,
			'action_edit' => 'index.php?m=5',
			'record_id' => $this->logic->_r_r_id,
			'table_id' => $this->logic->_r_table_id,
			'table_from_id' => $this->logic->_r_table_from_id,
			'template_html' => $template_html,
			'path_table' => $this->record_path_table($this->logic->_r_r_id),
			'js_folder' => JS_URL,
			'GLOBAL_SITE_URL' => GLOBAL_SITE_URL
		));

		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');
		return $this->core->atp_content($output);
	}

	/**
	 * Pagal laukų hierarchiją formuoja HTML'ą
	 * @authors Martynas Boika, Justinas Malūkas
	 * @param array $relations Laukų hierarchija.<br><b>array('Tevo ID' =><br>&nbsp;&nbsp;array('Vaiko 1 ID' => array(), 'Vaiko 2 ID' => array()),<br>'Lauko ID' => array())</b>
	 * @return string HTML'as
	 */
	public function view_column_order($relations, $params = array()) {
		$html = array('all' => '', 'field' => '', 'children' => '', 'classificator_fields' => '',
			'data' => array(
				'CLASS' => array(
					'id' => 'atp-column-classificator-',
					'field_class' => 'atp-column-classificator-fields fields'
				),
				'DEED' => array(
					'id' => 'atp-column-deed-',
					'field_class' => 'atp-column-deed-fields fields'
				),
				'CLAUSE' => array(
					'id' => 'atp-column-clause-',
					'field_class' => 'atp-column-clause-fields fields'
				))
		);

		if (empty($params['ID']) === TRUE) {

			// Veikos informacijai gauti per AJAX'ą
			if ($params['TYPE'] === 'DEED' && $_REQUEST['action'] === 'deedrecordfields' && $_GET['m'] === '121') {
				$template_html = '';
				// Duomenys reikalingi $this->manage->view_column_order();
				$record = $this->logic2->get_record_relation($_POST['record_id']);
				//$this->record_id = $record['RECORD_ID'];
				$this->table = $document = $this->logic2->get_document($_POST['table_id']); // reikalingas self::view_column_structure()
				//$this->field_sort = $this->logic->get_field_sort();
				$this->field_type = $this->logic->get_field_type(); // reikalingas self::view_column_structure()
				$params = array('ID' => $_POST['deed_id'], 'TYPE' => 'DEED');

				// Dokumento data
				if (empty($_POST['date'])) {
					// Jei nėra įvestos pažeidimo datos, neišvesti veikos informacijos
					$params['RECORD_DATE'] = date('Y-m-d H:i:s');
					//return null;
				} else {
					$params['RECORD_DATE'] = $_POST['date'];
				}

				$this->table_structure = $this->logic->get_fields_by_type_and_id($params);
				//$params2 = array('ITEM_ID' => $params['deed_id'], 'ITYPE' => 'DEED');
				//$this->table_structure = $this->logic2->get_fields_all($params2);

				if (empty($record) === FALSE) {

					$params['RECORD_ID'] = $record['RECORD_ID'];
					//$table_record = $this->logic->get_table_records($document['ID'], $record['RECORD_ID']);
					$table_record = $this->logic->get_table_records(array(
						'DOC_ID' => $document['table_id'],
						'RECORD_NR' => $record['RECORD_ID']
					));
					$table_record = array_shift($table_record);
					foreach ($table_record as $key => $val) {
						if (strpos($key, 'COL_') !== FALSE) {
							unset($table_record[$key]);
						}
					}
					/* $this->table_record = $this->logic->get_fields_values_by_type_and_id($params);
					  $this->table_record = array_merge((array) $table_record, (array) $this->table_record); */
					$this->table_record = $this->logic2->get_record_values(array('RECORD' => $record, 'WITH_NAME' => true));
					$this->table_record = array_merge($record, (array) $this->table_record);
				}
				/*
				 *  Surenka veikos laukus
				 */
				// Veikos informacija
				$deed = $this->logic->get_deed(array('ID' => $_POST['deed_id']));
				// Veiką praplečia
				$ddata = $this->logic->get_deed_extend_data(array('DEED_ID' => $deed['ID']));

				// Surenka veikos praplečiančių elementų ID
				$act_ids = array();
				$clause_ids = array();
				foreach ($ddata as $arr) {
					if ($arr['ITYPE'] === 'ACT')
						$act_ids[$arr['ID']] = $arr['ITEM_ID'];
					else if ($arr['ITYPE'] === 'CLAUSE')
						$clause_ids[$arr['ID']] = $arr['ITEM_ID'];
				}
// TODO: veika savo laukų nebeturi
				/*
				  // Veikos laukai
				  $fields = (array) $this->logic->get_table_fields_w_extra2r($deed['ID'], array('ITYPE' => 'DEED'));
				  $relations = array();
				  foreach ($fields as $val) {
				  $relations[$val['ID']] = array();
				  }
				  // Suformuoja įvedimo laukų HTML'ą
				  $template_html .= $this->view_column_order($relations);
				 */
				// Teisės akto laukai
				if (count($act_ids)) {
					$data = $this->logic2->get_act(array(
						0 => '`ID` IN(' . join(',', $act_ids) . ')',
						1 => '"' . $params['RECORD_DATE'] . '"' . ' > `VALID_FROM` AND "' . $params['RECORD_DATE'] . '" <= `VALID_TILL`'
					));
					if (count($data)) {
						foreach ($data as &$arr) {
							//$fields = (array) $this->logic->get_table_fields_w_extra2r($arr['ID'], array('ITYPE' => 'ACT'));
							//$fields = $this->logic2->get_fields(array('ITYPE' => 'ACT', 'ITEM_ID' => $arr['ID']));
							//$this->table_structure = $fields;
							$field_info = $this->get_info_fields_html(array('ID' => $arr['ID'], 'TYPE' => 'ACT'));



							// Papildomi laukai
							$fields = $this->logic2->get_act_default_fields_structure();
							$fields = sort_by($fields, 'ORD', 'asc');
							// Laukų reikšmės
							//$fields_values = $this->logic2->get_fields(array('ITYPE' => 'ACT', 'ITEM_ID' => $arr['ID']));

							$relations = array();
							foreach ($fields as $val) {
								$relations[$val['ID']] = array();
							}
							// Suformuoja įvedimo laukų HTML'ą
							$fields = $this->view_column_order($relations);

							// Gauna straipsnio lauką, kurio pavadinimas yra "acol_5" ("Tekstas") ir jį pašalina
							$parent_html = '';
							$dom = new DOMDocument('1.0', 'UTF-8');
							$html = mb_convert_encoding($fields, 'HTML-ENTITIES', 'UTF-8');
							$dom->loadHTML($html);
							// Pakeitimas pateiktame HTML'e
							$xpath = new DOMXPath($dom);
							$body = $xpath->query("//body")->item(0);
							$tags = $xpath->query("//textarea[@name='acol_5']");

							if ($tags->length) {
								$tag = $tags->item(0);

								$parent_node = $tag->parentNode;
								if (version_compare(phpversion(), '5.3.6', '>=') === TRUE) {
									$parent_html = $dom->saveHTML($parent_node);
								} else {
									$p_dom = new DOMDocument('1.0', 'UTF-8');
									$p_node = $p_dom->importNode($parent_node, true);
									$p_dom->appendChild($p_node);
									$parent_html = $p_dom->saveHTML();
								}

								$body->removeChild($parent_node);
								$html = $dom->saveHTML();

								$preg = array(
									'pattern' => '~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i',
									'replacement' => ''
								);
								$fields = preg_replace($preg['pattern'], $preg['replacement'], $html);
							}

							// "acol_5" prideda priekyje
							$fields = $parent_html . $field_info . $fields;
							$template_html .= $fields;
							break; // only one
						}
					}
				}

				// Straipsnio laukai
				if (count($clause_ids)) {
					//$data = $this->logic2->get_clause(array(
					//	0 => '`ID` IN(' . join(',', $clause_ids) . ')'
					//));
					$data = $this->logic2->get_clause(array(
						0 => '`ID` IN(' . join(',', $clause_ids) . ')',
						1 => '"' . $params['RECORD_DATE'] . '"' . ' > `VALID_FROM` AND "' . $params['RECORD_DATE'] . '" <= `VALID_TILL`'
					));
					if (count($data)) {
						foreach ($data as &$arr) {
							//$fields = (array) $this->logic->get_table_fields_w_extra2r($arr['ID'], array('ITYPE' => 'CLAUSE'));
							$fields = $this->logic2->get_fields(array('ITYPE' => 'CLAUSE', 'ITEM_ID' => $arr['ID']));
							$field_info = $this->get_info_fields_html(array('ID' => $arr['ID'], 'TYPE' => 'CLAUSE'));

							$relations = array();
							foreach ($fields as $val) {
								$relations[$val['ID']] = array();
							}

							// Suformuoja įvedimo laukų HTML'ą
							$template_html .= $field_info . $this->view_column_order($relations);
							break; // only one
						}
					}
				}

				return $template_html;
			}

			$structure = $this->table_structure;
		} else {
			$structure = $this->logic->get_extra_fields($params['ID'], $params['TYPE']);
		}


		foreach ($relations as $field_id => $children) {
			// Lauko HTML'as
			$html['field'] .= $this->view_column_structure($field_id);

			if ((int) $structure[$field_id]['VISIBLE'] === 0) {
				$html['field'] = str_replace('atp-table-fill', 'atp-table-fill hidden', $html['field']);
			}

			// Jei turi vaikų, gauna vaikų HTML'ą
			if (empty($children) === FALSE)
				$html['children'] .= $this->view_column_order($children);

			$type = $id = $field_class = '';
			$classes = $fields = array();
			// Jei yra klasifikatorius, surenka klasifikatoriaus laukus
			if (in_array($structure[$field_id]['SORT'], array(4, 6)) || $structure[$field_id]['WS'] === '1') {
				// Pagal tevinį ID suranda visų vaikų laukus
				if ($structure[$field_id]['TYPE'] == 18) {
					$type = 'CLASS';
					$classes[] = 'atp-column-classificator';
					$classes[] = 'group';
					$fields = $this->logic->get_extra_children_fields_by_parent($structure[$field_id]['SOURCE'], $type);
				} else if ($structure[$field_id]['TYPE'] == 23) {
					$type = 'DEED';
					$classes[] = 'atp-column-deed';
					$classes[] = 'group';
					// Veika neturi laukų
					//$fields = $this->logic->get_extra_children_fields_by_parent($structure[$field_id]['SOURCE'], $type);
					$html['extra_fields'] = '<div id="atp-column-deed-' . $structure[$field_id]['ID'] . '" class="atp-column-deed-fields"></div>';
				} else if ($structure[$field_id]['TYPE'] == 19) {
					$type = 'CLAUSE';
					$classes[] = 'atp-column-clause';
					$classes[] = 'group';
					//$fields = $this->logic->get_all_extra_fields_by_type($type, 'A.ORD ASC');

					$fields = $this->logic2->get_fields(array('ITYPE' => 'CLAUSE'));

					$fields_tmp = array();
					$clauses = $this->logic2->get_clause();
					foreach ($clauses as &$clause) {
						$fields_tmp[$clause['ID']] = $fields;
					}
					$fields = $fields_tmp; // Don't make reference!!!
				}

				// Webserviso laukas
				if ($structure[$field_id]['WS'] === '1') {
					$type = 'WS';
					$classes[] = 'atp-column-ws';
					$classes[] = 'group';
					$html['extra_fields'] .= '<div id="atp-column-ws-' . $structure[$field_id]['ID'] . '" class="atp-column-ws-fields"></div>';
				}

				if (empty($fields) === FALSE) {
					foreach ($fields as $field_id_2 => $children_2) {
						if ($type === 'CLAUSE') {
							$html['field_info'] = $this->get_info_fields_html(array('ID' => $field_id_2, 'TYPE' => $type));
						}
						// Pagal laukų sąryšius suformuoja laukų hierarchiją
						$table_column_relation = array();
						foreach ($children_2 as $arr) {
							if (empty($arr['RELATION']) === FALSE)
								$table_column_relation[$arr['ID']] = array('COLUMN_ID' => $arr['ID'], 'COLUMN_PARENT_ID' => $arr['RELATION']);
						}
						$column_relations = array();
						foreach ($children_2 as $arr) {
							$relation_status = $this->logic->get_column_relation_status($arr['ID'], $table_column_relation);
							if ($relation_status == 1)
								$column_relations[$arr['ID']] = $this->logic->get_column_relation_order($arr['ID'], $table_column_relation);
							elseif ($relation_status == 2)
								$column_relations[$arr['ID']] = array();
						}

						// TODO: pažymėta su PHP, o ne JS'u
						$html['extra_fields'] .= '<div id="' . $html['data'][$type]['id'] . $field_id_2 . '" class="' . $html['data'][$type]['field_class'] . '">'
								. $html['field_info']
								//. $this->view_column_order($column_relations, $field_id_2, $type)
								. $this->view_column_order($column_relations, array('ID' => $field_id_2, 'TYPE' => $type))
								. '</div>';
					}
				}
			}

			// Gautą HTML'ą prijungia prie rezultato
			if (empty($html['children']) === FALSE || empty($html['extra_fields']) === FALSE) {
				if (empty($html['children']) === FALSE) {
					$classes[] = 'atp-column-parent';
					$classes[] = 'group';
				}

				if ($type === 'WS' && empty($_GET['record_id']) === FALSE) {
					$ws = $this->logic2->get_ws();
					// Laukų struktūra į kuruos ateina informacija
					$fields = $this->logic2->get_fields(array('ITYPE' => 'WS', 'ITEM_ID' => $field_id), false, '`ORD` ASC');

					$html_arr = array();
					$values_arr = array();
					// Sąryšiai tarp web-serviso laukų ir dokumento laukų į kuriuos keliaus informacija
					$ws_relations = $this->logic2->get_webservices(array('PARENT_FIELD_ID' => $field_id));
					foreach ($ws_relations as $relation) {
						// Web-serviso lauko informaciją
						$info = $ws->get_field($relation['WS_NAME'], array('ID' => $relation['WS_FIELD_ID']));
						if (empty($info) === TRUE)
							continue;

						// Lauko informacija, į kurį keliaus informacija
						$field = $fields[$relation['FIELD_ID']];
						if (empty($field) === TRUE)
							continue;

						//$value = $this->logic->get_extra_field_value($_GET['table_id'], end(explode('p', $_GET['record_id'])), $field['ID']);
						$value = $this->logic->get_extra_field_value($_GET['table_id'], $_GET['record_id'], $field['ID']);
						if (empty($value) === TRUE)
							continue;

						$html_arr[$field['ORD']] = '<div class="atp-table-fill info"><div class="atp-rec-title">' . $field['LABEL'] . ':</div><span>' . $value . '</span></div>';
						$values_arr[$field['ID']] = $value;
					}

					$str = join('', $html_arr) . '<script>ws.push({' . $field_id . ': ' . (count($values_arr) ? json_encode($values_arr) : '[]') . '});</script>';
					$html['extra_fields'] = str_replace('class="atp-column-ws-fields">', 'class="atp-column-ws-fields">' . $str, $html['extra_fields']);
				}

				$classes = array_unique($classes);
				// TODO; sukelti į šablonus
				$contains = '';
				if (empty($cont) === TRUE) {
					$contains .= '<div id="atp-column' . $field_id . '" class="' . join(' ', $classes) . '">' . $html['field'];
					if (empty($html['extra_fields']) === FALSE) {
						$contains .= $html['extra_fields'];
					}
					if (empty($html['children']) === FALSE)
						$contains .= '<div id="atp-column-parent-' . $field_id . '" class="atp-column-child fields">' . $html['children'] . '</div>';
				}
				$contains .= '</div>';
				$html['all'] .= $contains;
			} else {
				$html['all'] .= $html['field'];
			}

			unset($html['field'], $html['children'], $html['extra_fields'], $html['field_info']);
		}

		return $html['all'];
	}

	/**
	 * Informacinių laukų HTML'as
	 * @param array $params
	 * @param int $params['ID'] Elemento ID
	 * @param string $params['TYPE'] CLAUSE / ACT
	 * @return string HTML'as
	 */
	public function get_info_fields_html($params) {
		$data = $this->logic->get_info_fields($params);
		$ret = null;
		if (empty($data) === FALSE)
			foreach ($data['header'] as $key => $val) {
				if (empty($data['value'][$key]) === TRUE)
					continue;
				$ret .= '<div class="atp-table-fill info field-' . $data['column'][$key] . '"><div class="atp-rec-title">' . $val . ':</div><span class="value" value="' . htmlspecialchars($data['data'][$key]) . '">' . $data['value'][$key] . '</span></div>';
			}

		return $ret;
	}

	/**
	 * Sugeneruoja dokumento lauko HTML'ą
	 * @author Martynas Boika
	 * @param int $structure_id
	 * @return string HTML'as
	 */
	public function view_column_structure($structure_id) {
		//$record_id = $this->record_id;
		$table = $this->table;
		$field_type = $this->field_type;
		//$table_structure = $this->table_structure;
		$table_record = $this->table_record;
		//$user = $this->core->user;
		$structure = $this->table_structure[$structure_id];



		if (empty($structure) === TRUE) {

			// Pakeistas keitus teisės aktus į teisės aktų punktus
			//$structure = $this->logic->get_extra_field($structure_id);
			$structure = $this->logic2->get_fields(array('ID' => $structure_id));
			if (empty($structure))
				unset($structure);
		}


		$structure_field_type = $field_type[$structure['TYPE']]['VALUE'];

		//$template_hidden_html = '';
		$template_name = '';
		$template_data = array(
			'title' => $structure['LABEL'],
			'name' => strtolower($structure['NAME']),
			'html' => '',
			'readonly' => '',
			'disabled' => '',
			'unallowed' => false,
			'v' => '',
			'no_check' => false
		);

		//if ((empty($table['CAN_EDIT']) === TRUE && empty($table_record['RECORD_VERSION']) === TRUE) || $table_record['STATUS'] != $structure['COMPETENCE'])
		if (empty($table_record) === TRUE) {
			$table_record['STATUS'] = 2;
		}

		// TODO: tai yra ir prie pačio saugojimo
		// Nurodoma, kada laukai neredaguojami
		// Jei dokumeno įrašo statusas nera tyriama arba redaguojama
		// Jei dokumeno lauko statusas neatitinka dokumento įrašo statusui ir dokumento lauko bei dokumento statusas nėra 1
		// Jei dokumento negalima redaguoti ir prisijungęs vartotojas nėra nei tyrėjas, nei redaguotojas

		if (in_array($table_record['STATUS'], array(1, 2, 3)) === FALSE // TODO: kaip su redagavimu?
				|| (
				$structure['COMPETENCE'] != $table_record['STATUS'] && $structure['COMPETENCE'] != 1 && $table_record['STATUS'] != 1
				) || (
				empty($table['CAN_EDIT']) === TRUE && $this->logic->check_privileges(array('OR' => array('INVESTIGATE_RECORD', 'EXAMINATE_RECORD')), array('table_id' => $table['ID'], 'user_id' => $this->core->user->id, 'department_id' => $this->core->user->department->id)) === FALSE
				)
		) {
			$template_data['disabled'] = ' disabled="disabled"';
			$template_data['unallowed'] = true;
		}

		//$structure_value = isset($table_record[$structure['NAME']]) ? $table_record[$structure['NAME']] : '';
		$structure_value = isset($table_record[$structure['NAME']]) ? $table_record[$structure['NAME']] : NULL;

		$template_data['id'] = '';
		$template_data['rows'] = '';
		$template_data['type'] = 'text';
		$template_data['value'] = $structure_value;
		$template_data['class'] = '';
		$template_data['rec_class'] = '';
		$template_data['onkeypress'] = '';
		$template_data['max_length'] = '';
		$template_data['note_class'] = '';
		$template_data['note_text'] = '';
		$template_html_data = array();

		// TROL LOGIKOS
		$logics_fiels = array(1124, 1126);
		if (in_array($structure['ID'], $logics_fiels)) {
			$logics = new ATP_Document_Record_Logics($this->logic->_r_r_id, $this->core);

			//Protokolas: Protokolo tipas
			if ($structure['ID'] == 1124) {
				$structure_value = $logics->get_protocol_classificator();
				$template_data['disabled'] = ' disabled="disabled"';
				$template_data['class'] = ' reenable';
			} else
			// Protokolas: AN baudos suma
			if ($structure['ID'] == 1126) {
				if (empty($structure_value) === TRUE)
					$structure_value = $logics->get_first_penalty();
			}
			$template_data['value'] = $structure_value;
		}


		// Skaičius
		if ($structure['SORT'] == 1 && $structure['TYPE'] != 6) {
			$template_name = 'input';

			$template_data['type'] = 'text';
			$template_data['onkeypress'] = ' onkeypress="return isNumberKey(event)"';

			$max_length = array(1 => 3, 2 => 5, 3 => 7, 4 => 11, 5 => 19);
		} else
		// Skaičius su kableliu
		if ($structure['SORT'] == 1 && $structure['TYPE'] == 6) {
			$template_name = 'input';

			$template_data['type'] = 'text';
			$template_data['onkeypress'] = ' onkeypress="return isFloatNumberKey(event)"';
			$template_data['value'] = empty($structure_value) === FALSE ? $structure_value : '0.00';
		} else
		// Tekstinis tipas
		if ($structure['SORT'] == 2) {
			// Smulkus tekstas
			if ($structure['TYPE'] == 7)
				$template_name = 'input';
			else
				$template_name = 'textarea';

			$rows = array(8 => 2, 9 => 4, 10 => 6);
			$max_length = array(7 => 256, 8 => 65000, 9 => 16000000, 10 => 4000000000);
		} else
		// Veika
		if ($structure['SORT'] == 4 && $structure['TYPE'] === '23') {
			$template_name = 'select';

			$data = $this->logic->get_deed_search_values();
			if (empty($structure['DEFAULT']) === FALSE && empty($structure['DEFAULT_ID']) === FALSE && empty($structure_value) === TRUE)
				$structure_value = $structure['DEFAULT_ID'];

			$str_val = 0;
			foreach ($data as $arr) {
				$selected = '';
				// WTF? SELECT reikšmė pažymima pagal pavadinima, kai DB saugoma value atributo reikšmė
				//if ($structure_value == $arr['LABEL']) {
				if ($structure_value == $arr['ID'] && $arr['TYPE'] === 'DEED') {
					$selected = ' selected="selected"';
					$str_val = $arr['VALUE'];
				}
				$template_html_data[] = array('value' => $arr['VALUE'], 'selected' => $selected, 'label' => $arr['LABEL']);
			}
			$structure_value = $str_val;
		} else
		// Klasifikatorius
		if ($structure['SORT'] == 4) {
			$template_name = 'select';

			//$classifications = $this->logic->get_classification(null, $structure['SOURCE']);
			$classifications = $this->logic2->get_classificator(array('PARENT_ID' => $structure['SOURCE']));
			if (empty($structure['DEFAULT']) === FALSE && empty($structure['DEFAULT_ID']) === FALSE && empty($structure_value) === TRUE)
				$structure_value = $structure['DEFAULT_ID'];

			$str_val = 0;
			foreach ($classifications as $classification) {
				$selected = '';
				// WTF? SELECT reikšmė pažymima pagal pavadinima, kai DB saugoma value atributo reikšmė
				//if ($structure_value == $classification['LABEL']) {
				if ($structure_value == $classification['ID']) {
					$selected = ' selected="selected"';
					$str_val = $classification['ID'];
				}
				$template_html_data[] = array('value' => $classification['ID'], 'selected' => $selected, 'label' => $classification['LABEL']);
			}
			$structure_value = $str_val;
		} else
		// Checkbox ir radio
		if ($structure['SORT'] == 5 || $structure['SORT'] == 8) {
			$template_name = 'pick';

			$pk_type = '';
			if ($structure['SORT'] == 5) {
				$pk_type = 'checkbox';
				$template_data['name'] = $template_data['name'] . '[]';
				$structure_value = json_decode(stripslashes($structure_value));
			}

			if ($structure['SORT'] == 8) {
				$pk_type = 'radio';
				$structure_value = array($structure_value);
			}

			$str_val = 0;
			//$classifications = $this->logic->get_classification(null, $structure['SOURCE']);
			$classifications = $this->logic2->get_classificator(array('PARENT_ID' => $structure['SOURCE']));
			foreach ($classifications as $classification) {
				$pk_checked = '';
				//if (in_array($classification['LABEL'], $structure_value)) {
				if (is_array($structure_value) && in_array($classification['ID'], $structure_value)) {
					$pk_checked = ' checked="checked"';
					$str_val = $classification['ID'];
				}

				$template_html_data[] = array(
					'pk_type' => $pk_type,
					'pk_id' => $classification['ID'],
					'pk_label' => $classification['LABEL'],
					'pk_class' => '',
					'pk_name' => $template_data['name'],
					'pk_checked' => $pk_checked,
					'pk_value' => $classification['ID']);
			}

			$structure_value = $str_val;
		}

		// Straipsnis
		if ($structure['SORT'] == 6) {

			$template_name = 'select';

			$user_clauses = $this->logic->get_user_clauses($this->core->user->worker['ID']);

			if (empty($structure['DEFAULT']) === FALSE && empty($structure['DEFAULT_ID']) === FALSE && empty($structure_value) === TRUE)
				$structure_value = $structure['DEFAULT_ID'];

			$str_val = 0;
			foreach ($user_clauses as $clause) {
				$selected = '';
				//if ($structure_value == $clause['NAME']) {
				if ($structure_value == $clause['ID']) {
					$selected = ' selected="selected"';
					$str_val = $clause['ID'];
				}
				$template_html_data[] = array('value' => $clause['ID'], 'selected' => $selected, 'label' => $clause['NAME']);
			}
			$structure_value = $str_val;
		}

		// Datos ir laiko laukai
		if ($structure['SORT'] == 7) {
			$template_name = 'input';

			$is_empty = false;

			// Jeigu tuščia, data ar laikas prasideda nuliais
			if (empty($structure_value) === TRUE || preg_match('/^([0]{4}.*|[0]{2}:[0]{2}(:[0]{2})?)$/', $structure_value) === 1 || strtotime($structure_value) <= 0)
				$is_empty = true;

			$timestamp = strtotime($structure_value);
			if ($structure['TYPE'] == 11) {
				$max_length = array(11 => 4);
				$template_data['class'] .= ' atp-date-year';
				$time_string = date('Y', $timestamp);
			}
			if ($structure['TYPE'] == 12) {
				$max_length = array(12 => 10);
				$template_data['class'] .= ' atp-date';
				$time_string = date('Y-m-d', $timestamp);
			}
			if ($structure['TYPE'] == 13) {
				$max_length = array(13 => 8);
				$template_data['class'] .= ' atp-time';
				//$time_string = date('H:i:s', $timestamp);
				$time_string = date('H:i', $timestamp);
			}
			if ($structure['TYPE'] == 14) {
				$max_length = array(14 => 19);
				$template_data['class'] .= ' atp-datetime';
				//$time_string = date('Y-m-d H:i:s', $timestamp);
				$time_string = date('Y-m-d H:i', $timestamp);
			}

			if ($structure_value === NULL) {
				$structure_value = NULL;
			} else if ($is_empty === TRUE)
			//$structure_value = '';
			/**
			 * @TODO: [done] Netinka, kadangi radus NULL reikšmę įkeliama reikšmė pagal nutylėjimą
			  $structure_value = NULL;
			 */
				$structure_value = '';
			else
				$structure_value = $time_string;

			$template_data['value'] = $structure_value;
		}

		// Antraštė
		if ($structure['SORT'] == 9) {
			$template_name = 'text';
			$template_data['title'] = $template_data['title'];
			$template_data['value'] = $structure['TEXT'];
		}

		// Failas
		if ($structure['SORT'] == 10) {
			$template_name = 'files';

			$template_data['type'] = 'file';
			$template_data['name'] = $template_data['name'] . '[]';
			$template_data['class'] = '-file' . $template_data['class'];

			// Failo lauko nustatymai
			$file_params = $this->logic->get_file_params(array(
				'TABLE_ID' => $structure['ITEM_ID'],
				'STRUCTURE_ID' => $structure['ID'],
				'LIMIT' => 1));
			if (!empty($file_params)) {
				$file_params = array_shift($file_params);

				$template_data['note_class'] = ' atp-note-show';
				$template_data['note_text'] = 'Failų kiekis: <span class="atp-file-cnt">' . $file_params['CNT'] . '</span>, '
						. 'dydis: ' . $file_params['SIZE_FROM'] . ' - ' . $file_params['SIZE_TO'] . ' Mb';
			}

			// TODO: RECORD_ID | į RECORD_ID turi ateiti RECORD_RELATION.ID
			// Dokumento įrašo lauko ir failų ryšiai
			$files = $this->logic2->get_record_files(array(
				/* 'TABLE_ID' => $table['ID'],
				  'RECORD_ID' => $table_record['ID'], */
				'RECORD_ID' => $this->logic->_r_r_id,
				'FIELD_ID' => $structure['ID']));

			// TODO: atskirti failo išvedimą pagal failo tipą
			$file_list = '';
			if (empty($files) === FALSE) {
				foreach ($files as &$file) {
					// Failo informacija
					$file_info = $this->logic2->get_files(array('ID' => $file['FILE_ID']));

					if (/* strpos($file_info['TYPE'], 'image') === 0 && */$structure['TYPE'] === '20') {
						// Sugeneruoja thumbnail'ą
						$path = 'files/tmp/';
						$thumb_name = $this->action->create_thumbnail($this->core->config['REAL_URL'] . $file_info['PATH'], $this->core->config['REAL_URL'] . $path);
						$thumb_path = $this->core->config['SITE_URL'] . $path . $thumb_name;
						// Failo išvedimas
						$file_list .= '<li for="' . $file['ID'] . '">'
								. '<a href="' . $this->core->config['SITE_URL'] . $file_info['PATH'] . '" target="_blank">'
								. '<img class="photo" style="background: url(' . $thumb_path . ') center center no-repeat;"/>'
								. $file_info['NAME']
								. '</a>'
								. '</li>';
					} else if ($structure['TYPE'] === '21') {
						$file_list .= '<li for="' . $file['ID'] . '">'
								. '<a href="' . $this->core->config['SITE_URL'] . $file_info['PATH'] . '" target="_blank">'
								. $file_info['NAME']
								. '</a>'
								. '</li>';
					}
				}
			}

			if (empty($file_list) === FALSE) {
				if ($structure['TYPE'] === '20') {
					$file_list = '<div class="file-list"><ul id="photos_b" class="photo-list fl">' . $file_list . '</ul></div>';
				} else if ($structure['TYPE'] === '21') {
					$file_list = '<div class="file-list"><ul class="file-list">' . $file_list . '</ul></div>';
				}
			}
			$template_data['file_list'] = &$file_list;
		}

		// Web-serviso laukui 
		if ((int) $structure['WS'] === 1) {
			// Jei įrašas importuotas iš VMSA
			if ($this->logic2->is_VMSA_record((int) $table_record['RID']) === TRUE) {
				$template_data['no_check'] = true;
			} else {
				// Pridedamas mygtukas tikrinimui
				$template_data['html'] .= '<div class="check ws">tikrinti</div>';
			}
		}
		// Pažeidėjo asmens kodo laukui pridedamas mygtukas baustumo tikrinimui
		if ((int) $structure['ID'] === (int) $this->table['PERSON_CODE_FIELD']) {
			$template_data['html'] .= '<div class="check punishment">tikrinti baustumą</div>';
		}

		if (!empty($rows[$structure['TYPE']]))
			$template_data['rows'] = ' rows="' . $rows[$structure['TYPE']] . '"';

		if (!empty($max_length[$structure['TYPE']]))
			$template_data['max_length'] = ' maxlength="' . $max_length[$structure['TYPE']] . '"';

		if (!empty($template_name)) {
			$template = $this->core->getTemplate('template_' . $template_name);

			if ($template_data['unallowed'] === TRUE)
				$template_data['disabled'] .= ' unallowed="unallowed"';
			if ($template_data['no_check'] === TRUE)
				$template_data['no_check'] = ' no_check="no_check"';
			if (in_array($structure['SORT'], array(4, 5, 6, 8)) === FALSE) {
				/* if (isset($template_data['value']) === TRUE && empty($template_data['value']) === TRUE && empty($structure['DEFAULT']) === FALSE) { */
				// Pagal nutylėjimą papildomiems laukams $template_data['value'] yra NULL (pagrindiniams laukams įvairiai, t.y. "" arba 0), o isset(NULL) yra FALSE
				if (empty($template_data['value']) === TRUE && empty($structure['DEFAULT']) === FALSE) {

					// Jei datos ir laiko tipas
					if ($structure['SORT'] == 7 && strtotime($structure['DEFAULT']) <= 0) {
						$structure['DEFAULT'] = '';
					}


					/* Sako, kad to nereikia:
					 * Kad vietoje papildomo lauko tuščio string reikšmės neįdėtu reikšmės pagal nutylėjimą
					 */
					//if ($template_data['value'] !== NULL && $structure['ITYPE'] !== 'DOC') { } else
					$template_data['value'] = $structure['DEFAULT'];
				}

				$template_html = $this->core->returnHTML($template_data, $template[0]);
			} else {
				$template_data['lng_not_chosen'] = $this->lng('not_chosen');
				$template_html = $this->core->returnHTML($template_data, $template[0]);
				foreach ($template_html_data as $template_dt) {
					foreach ($template_dt as $val => $dt)
						$template_data[$val] = $dt;

					$template_html .= $this->core->returnHTML($template_data, $template[1]);
				}
				$template_html .= $template[2];
			}

			//if($template_data['disabled'])
			//    $template_hidden_html .= '<input name="'.$template_data['name'].'" type="hidden" value="'.$structure_value.'" />';
		}

		// Kad DOMDocument nesisvaidytu pranešimais apie besidubliuojančius ID
		$template_html = str_replace(' id=""', '', $template_html);
		//return $template_hidden_html . $template_html;
		return $template_html;
	}

	/**
	 * Dokumento įrašo kelias
	 * @return string HTML'as
	 */
	public function record_path() {
		$table_id = (!empty($_REQUEST['table_id'])) ? $_REQUEST['table_id'] : 0;
		$record_nr = (!empty($_REQUEST['record_id'])) ? $_REQUEST['record_id'] : 0;

		$table = $this->logic2->get_document(array('ID' => $table_id), true, null, 'ID, LABEL');

		$tpl_arr = $this->core->getTemplate('record_path');
		$html_data['title'] = $table['LABEL'] . ' - ' . $record_nr;
		$html_data['table'] = $this->record_path_table($record_nr);

		$html = $this->core->returnHTML($html_data, $tpl_arr[0]);

		return $this->core->atp_content($html);
	}

	/**
	 * 
	 * @param mixed $record Tai ką valgo <b>atp_logic2::get_record_relation</b>
	 * @param int $table_id
	 * @return boolean
	 */
	public function record_path_table($record) {
		if (empty($record))
			return FALSE;

		$tables = $this->logic2->get_document(null, FALSE, null, 'ID, LABEL');
		$record = $this->logic2->get_record_relation($record);
		$record_path = $this->logic->get_record_path($record);

		$tmpl = $this->core->Template();
		$tmpl_handler = 'record_path_table_file';
		$tmpl->set_file($tmpl_handler, 'record_path_table.tpl');

		$row_block = $tmpl_handler . 'row_block';
		$tmpl->set_block($tmpl_handler, 'row_block', $row_block);

		$record_path = sort_by($record_path, 'CREATE_DATE', 'asc');

		foreach ($record_path as $path) {
			$atp_status = $this->logic->find_document_status(array('ID' => $path['STATUS']));
			$status = $this->logic2->get_status(array('ID' => $path['DOC_STATUS']));

			if ($path['TABLE_TO_ID'] == 53) {
				$_POST['record'] = $path['ID'];
				$RN = new ATP_Post_RN($this->core);
				$ret = $RN->check_rn_state();

				if ($ret !== false) {
					$post_state = $ret;
				}
			} else {
				$post_state = '';
			}
			$arr = array(
				'record_id' => $path['ID'],
				'record_nr' => $path['RECORD_ID'],
				'record_table' => $tables[$path['TABLE_TO_ID']]['LABEL'],
				'record_version' => $path['RECORD_VERSION'],
				'record_person' => join(' ', array_filter(array($path["FIRST_NAME"], $path["LAST_NAME"]))),
				'record_atp_status' => $atp_status['LABEL'],
				'record_status' => $status['LABEL'],
				'record_post_status' => $post_state, //'Nusiustas :)',//$status['LABEL'],
				'record_created' => $path['CREATE_DATE'],
				'record_current_path' => ($path['ID'] === $record['ID'] ? ' class="atp-current-path"' : '')
			);
			$tmpl->set_var($arr);
			$tmpl->parse($row_block, 'row_block', true);
		}

		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');
		return $this->core->atp_content($output, 2);
	}

	/**
	 * Administravimas (Vartotojų tesiės)
	 * @return string
	 */
	public function user_administration() {
		$tmpl = $this->core->Template();
		if (!$this->core->user->status['A_COMPETENCE']) {
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
		} else {
			if (isset($_POST['submit_status']) && $this->core->user->status['A_COMPETENCE'] > 1) {
				$user_id = (!empty($_POST['user_admin_id'])) ? $_POST['user_admin_id'] : 0;
				$department_id = (!empty($_POST['user_department_id'])) ? $_POST['user_department_id'] : 0;
				$this->logic->edit_status();
				$params = array(
					'm' => $_GET['m'],
					'department_id' => $department_id,
					'user_id' => $user_id
				);
				header('Location: index.php?' . http_build_query($params));
				exit;
			} else {
				$user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
				$department_id = (!empty($_REQUEST['department_id'])) ? $_REQUEST['department_id'] : 0;
			}

			$tmpl_handler = 'user_administration_file';
			$tmpl->set_file($tmpl_handler, 'user_administration.tpl');

			$submit_block = $tmpl_handler . 'submit';
			$tmpl->set_block($tmpl_handler, 'submit', $submit_block);
			$table_block = $tmpl_handler . 'table';
			$tmpl->set_block($tmpl_handler, 'table', $table_block);

			$user_data = null;
			if (!empty($user_id))
				$user_data = $this->logic->get_m_user($user_id, $department_id);

			$department = $this->logic->get_m_department($department_id);

			$arr = array(
				'sys_message' => $this->core->get_messages(),
				'user_id' => $user_data['ID'],
				'user_name' => $user_data['SHOWS'],
				'department_id' => $department['ID'],
				'department_label' => $department['SHOWS'],
				'm' => $_GET['m'],
				'GLOBAL_SITE_URL' => GLOBAL_SITE_URL,
			);

			if (empty($user_id) === FALSE || empty($department_id) === FALSE) {
				$tmpl->set_var('table_user_administration', $this->table_user_administration($user_id, $department_id));
				$tmpl->parse($table_block, 'table');
			} else {
				$tmpl->clean($table_block);
			}

			if ($this->core->user->status['A_COMPETENCE'] > 1)
				$tmpl->parse($submit_block, 'submit');
			else
				$tmpl->clean($submit_block);

			$tmpl->set_var($arr);
		}
		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');
		return $this->core->atp_content($output);
	}

	/**
	 * Kompetencijos (Administravimas)
	 * @return type
	 */
	public function user_competences() {
		$tmpl = $this->core->Template();
		if (!$this->core->user->status['A_COMPETENCE']) {
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
		} else {
			if (isset($_POST['submit_competence']) && $this->core->user->status['A_COMPETENCE'] > 1) {
				$user_id = (!empty($_POST['user_admin_id'])) ? $_POST['user_admin_id'] : 0;
				$department_id = (!empty($_POST['user_department_id'])) ? $_POST['user_department_id'] : 0;
				$this->logic->edit_competence();
			} else {
				$user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
				$department_id = (!empty($_REQUEST['department_id'])) ? $_REQUEST['department_id'] : 0;
			}

			$tmpl = $this->core->Template();
			$tmpl_handler = 'user_competences_file';
			$tmpl->set_file($tmpl_handler, 'user_competences.tpl');

			$submit_block = $tmpl_handler . 'submit';
			$tmpl->set_block($tmpl_handler, 'submit', $submit_block);
			$table_block = $tmpl_handler . 'table';
			$tmpl->set_block($tmpl_handler, 'table', $table_block);

			$user_data = null;
			if (!empty($user_id))
				$user_data = $this->logic->get_m_user($user_id, $department_id);

			$department = $this->logic->get_m_department($department_id);

			$arr = array(
				'sys_message' => $this->core->get_messages(),
				'user_id' => $user_data['ID'],
				'user_name' => $user_data['SHOWS'],
				'department_id' => $department['ID'],
				'department_label' => $department['SHOWS'],
				'm' => $_GET['m'],
				'GLOBAL_SITE_URL' => GLOBAL_SITE_URL,
			);
			if (empty($user_id) === FALSE || empty($department_id) === FALSE) {
				$tmpl->set_var('table_user_competence', $this->table_user_competence($user_id, $department_id));
				$tmpl->parse($table_block, 'table');
			} else {
				$tmpl->clean($table_block);
			}
			if ($this->core->user->status['A_COMPETENCE'] > 1)
				$tmpl->parse($submit_block, 'submit');
			else
				$tmpl->clean($submit_block);

			$tmpl->set_var($arr);
		}
		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');
		return $this->core->atp_content($output);
	}

	/**
	 * Dokumentų teisės (Administravimas)
	 * @return type
	 */
	public function user_rights() {
		$tmpl = $this->core->Template();

		if (!$this->core->user->status['A_COMPETENCE']) {
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
		} else {
			if (isset($_POST['submit_privileges']) && $this->core->user->status['A_COMPETENCE'] > 1) {
				$user_id = (!empty($_POST['user_admin_id'])) ? $_POST['user_admin_id'] : 0;
				$department_id = (!empty($_POST['user_department_id'])) ? $_POST['user_department_id'] : 0;
				$this->logic->edit_rights();
			} else {
				$user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
				$department_id = (!empty($_REQUEST['department_id'])) ? $_REQUEST['department_id'] : 0;
			}

			$tmpl = $this->core->Template();
			$tmpl_handler = 'user_rights_file';
			$tmpl->set_file($tmpl_handler, 'user_rights.tpl');

			$Marker_24 = $tmpl_handler . '_Marker_24';
			$tmpl->set_block($tmpl_handler, 'Marker_24', $Marker_24);
			$submit_block = $tmpl_handler . 'submit';
			$tmpl->set_block($tmpl_handler, 'submit', $submit_block);
			$table_block = $tmpl_handler . 'table';
			$tmpl->set_block($tmpl_handler, 'table', $table_block);

			$user_data = null;
			if (!empty($user_id))
				$user_data = $this->logic->get_m_user($user_id, $department_id);

			$department = $this->logic->get_m_department($department_id);

			$tables = $this->logic2->get_document();


			//TODO: kas čia?
			$default_user = FALSE;
			if ($user_id === 'x') {//default user
				$user_id = 0;
				$default_user = TRUE;
			}

			if (((!empty($user_id) || $default_user) && !empty($department_id)) || !empty($department_id)) {
				$priveleges_data = array('user_id' => $user_id, 'department_id' => $department_id);
				foreach ($tables as &$table) {
					$priveleges_data['table_id'] = $table['ID'];
					$arr = array(
						'label' => $table['LABEL'],
						'table_id' => $table['ID'],
						'view_record' => $this->logic->check_privileges('VIEW_RECORD', $priveleges_data) ? ' checked="checked"' : '',
						'edit_record' => $this->logic->check_privileges('EDIT_RECORD', $priveleges_data) ? ' checked="checked"' : '',
						'delete_record' => $this->logic->check_privileges('DELETE_RECORD', $priveleges_data) ? ' checked="checked"' : '',
						'investigate_record' => $this->logic->check_privileges('INVESTIGATE_RECORD', $priveleges_data) ? ' checked="checked"' : '',
						'examinate_record' => $this->logic->check_privileges('EXAMINATE_RECORD', $priveleges_data) ? ' checked="checked"' : '',
						'end_record' => $this->logic->check_privileges('END_RECORD', $priveleges_data) ? ' checked="checked"' : ''
					);
					$tmpl->set_var($arr);
					$tmpl->parse($Marker_24, 'Marker_24', true);
				}

				if ($this->core->user->status['A_COMPETENCE'] > 1)
					$tmpl->parse($submit_block, 'submit');
				else
					$tmpl->clean($submit_block);

				$tmpl->parse($table_block, 'table');
			} else {
				$tmpl->clean($table_block);
			}


			$arr = array(
				'sys_message' => $this->core->get_messages(),
				'user_id' => $user_data['ID'],
				'user_name' => $user_data['SHOWS'],
				'department_id' => $department['ID'],
				'department_label' => $department['SHOWS'],
				'm' => $_GET['m'],
				'GLOBAL_SITE_URL' => GLOBAL_SITE_URL,
			);

			$tmpl->set_var($arr);
		}
		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');
		return $this->core->atp_content($output);
	}

	/**
	 * Dokumentai (Administravimas) puslapio dokumentų lentelė
	 * @return string HTML'as
	 */
	public function table_user_administration($user_id, $department_id) {
		$html_manage = '';

		$tpl_arr = $this->core->getTemplate('table');
		$user_status = $this->logic->get_user_status($user_id, $department_id);
		$columns = array(
			'right' => '',
			/* 'none' => $this->lng('atp_user_administration_none'),
			  'see' => $this->lng('atp_user_administration_see'), */
			'edit' => $this->lng('atp_user_administration_edit')
		);
		$rows = array(
			'view_table' => 'A_TABLE',
			'view_relation' => 'A_RELATION',
			'view_status' => 'A_STATUS',
			'view_document' => 'A_DOCUMENT',
			'view_classification' => 'A_CLASSIFICATION',
			'view_act' => 'A_ACT',
			'view_deed' => 'A_DEED',
			'view_competence' => 'A_COMPETENCE',
			'view_clause' => 'A_CLAUSE'/* ,
				  'view_department' => 'A_DEPARTMENT' */);

		// table
		$clause = array();
		$clause['table_class'] = 'user_administration';
		$html_manage .= $this->core->returnHTML($clause, $tpl_arr[0]);

		// header
		$header = array('row_html' => '');
		$header['row_class'] = 'table-row table-header';
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);

		foreach ($columns as $col_name => $col) {
			// cell
			$cell = array('cell_html' => '');
			$cell['cell_class'] = 'table-cell cell-' . $col_name;
			$cell['cell_content'] = $col;
			$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
		}
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

		foreach ($rows as $key => $row) {

			// row
			$row_var = array('row_html' => '', 'row_class' => '');
			$html_manage .= $this->core->returnHTML($row_var, $tpl_arr[1]);

			foreach ($columns as $col_name => $col) {
				// cell
				$cell = array('cell_html' => '', 'cell_content' => '');
				$cell['cell_class'] = 'table-cell cell-' . $col_name;
				switch ($col_name) {
					case 'right':
						//$clause[$col_name] = $this->lng('atp_user_administration_right_' . $row);
						$clause[$col_name] = $this->lng('menu_' . $row);
						break;
					/* case 'none':
					  $clause[$col_name] = '<input type="radio" name="' . $key . '" value="0"' . ($user_status[$row] === '0' || $user_status === FALSE ? ' checked="checked"' : '') . '/>';
					  break;
					  case 'see':
					  $clause[$col_name] = '<input type="radio" name="' . $key . '" value="1"' . ($user_status[$row] === '1' ? ' checked="checked"' : '') . '/>';
					  break;
					 */case 'edit':
						//$clause[$col_name] = '<input type="radio" name="' . $key . '" value="2"' . ($user_status[$row] === '2' ? ' checked="checked"' : '') . '/>';
						$clause[$col_name] = '<input type="checkbox" name="' . $key . '" value="2"' . ($user_status[$row] === '2' ? ' checked="checked"' : '') . '/>';
						break;
				}

				$cell['cell_content'] .= $clause[$col_name];
				$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
			}

			$html_manage .= $this->core->returnHTML($row_var, $tpl_arr[3]);
		}
		$html_manage .= $this->core->returnHTML($clause, $tpl_arr[4]);

		return $html_manage;
	}

	/**
	 * Administravimas (Administravimas) puslapių teisių lentelė
	 * @return string HTML'as
	 */
	public function table_list() {

		$html_manage = '';
		$tables = $this->logic2->get_document(null, false, 'PARENT_ID, LABEL');

		$new_tables = array();
		foreach ($tables as $table) {
			$new_tables[$table['ID']] = $table;

			$new_tables[$table['ID']]['ACTIONS'] = array();
			if ($this->core->user->status['A_TABLE'] > 1) {
				$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button edit" id="atp-table-id-' . $table['ID'] . '"><span>' . $this->lng('edit') . '</span></div>';
				$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button remove" id="atp-table-id-' . $table['ID'] . '"><span>Trinti</span></div>';
			}
			$new_tables[$table['ID']]['ACTIONS'] = join(' ', $new_tables[$table['ID']]['ACTIONS']);

			$new_tables[$table['ID']]['LEVEL'] = $this->logic->get_table_level($tables, $table['ID']);

			if ($this->logic->table_has_children($tables, $table['ID']))
				$toggle_class = 'collapse';
			else
				$toggle_class = 'none';

			$new_tables[$table['ID']]['LABEL'] = '<div class="cell-LABEL-container"><div class="' . $toggle_class . ' atp-table-list-toggle"></div><span>' . $new_tables[$table['ID']]['LABEL'] . '</span></div>';
		}

		$tables = &$new_tables;

		$tables = $this->logic->sort_children_to_parent($tables, 'ID', 'PARENT_ID');

		$tpl_arr = $this->core->getTemplate('table2');
		$columns = array(
			'LABEL' => $this->lng('atp_table_label'),
			'CREATE_DATE' => $this->lng('atp_table_create_date'),
			'UPDATE_DATE' => $this->lng('atp_table_update_date')
		);
		if ($this->core->user->status['A_TABLE'] > 1)
			$columns['ACTIONS'] = $this->lng('atp_table_actions');

		// table
		$table = array();
		$table['table_class'] = 'div';
		$html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

		// header
		$header = array('row_html' => '');
		$header['row_class'] = 'div-row div-header';
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);

		foreach ($columns as $col_name => $col) {
			// cell
			$cell = array('cell_html' => '');
			$cell['cell_class'] = 'div-cell cell-' . $col_name;
			$cell['cell_content'] = $col;
			$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
		}
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

		foreach ($tables as $key => $table) {

			// row
			$row = array();
			$row['row_html'] = 'id="atp-table-list-item-id-' . $table['ID'] . '"';
			$row['row_class'][] = 'div-row';
			if ($table['LEVEL'])
				$row['row_class'][] = 'child';
			$row['row_class'][] = 'level-' . $table['LEVEL'];
			$row['row_class'][] = 'child-of-' . $table['PARENT_ID'];
			$row['row_class'] = join(' ', $row['row_class']);
			$html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

			foreach ($columns as $col_name => $col) {
				// cell
				$cell = array('cell_html' => '', 'cell_content' => '');
				$cell['cell_class'] = 'div-cell cell-' . $col_name;
				$cell['cell_content'] .= $table[$col_name];
				$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
			}

			$html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
		}

		$html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

		return $html_manage;
	}

	/**
	 * Dokumentų šablonai (Administravimas)
	 * @return string HTML'as
	 */
	public function documents() {
		if (!$this->core->user->status['A_DOCUMENT']) {
			$tmpl = $this->core->Template();
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
			$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
			$output = $tmpl->get($tmpl_handler . '_out');
			return $this->core->atp_content($output);
		}

		$table_id = (!empty($_REQUEST['table_id'])) ? $_REQUEST['table_id'] : 0;

		if (!empty($table_id) && $this->core->user->status['A_DOCUMENT'] > 1) {
			if (isset($_POST['submit_file']) && !empty($_POST['template_name'])) {
				$file_uploaded = FALSE;

				$template_label = $_POST['template_name'];
				$wp_last_id = $this->core->db_quickInsert(PREFIX . 'ATP_TABLE_TEMPLATE', array('TABLE_ID' => $table_id, 'LABEL' => $template_label));

				$this->core->set_message('success', $this->lng('success_created'));
				$template_id = mysql_insert_id();

				if (empty($template_id) && !empty($wp_last_id))
					$template_id = $wp_last_id;

				if (isset($_POST['submit_file'])) {
					$goodExtensions = array('doc', 'docx');
					$error = '';

					$uploaddir = $this->core->config['REAL_URL'] . 'files/templates/';
					if (is_dir($uploaddir) === FALSE) {
						$oldmask = umask(0);
						mkdir($uploaddir, 0777, true);
						umask($oldmask);
					}

					$name = $_FILES['filename']['name'];
					$min_filesize = 10; //bytes

					$stem = 'tpl' . $template_id;
					$extension = pathinfo($name, PATHINFO_EXTENSION);

					if (!in_array($extension, $goodExtensions))
						$error .= 'Extension not allowed<br>';

					if (filesize($_FILES['filename']['tmp_name']) < $min_filesize)
						$error .= 'File size too small<br>' . "\n";

					$filename = $stem . '.' . $extension;
					$uploadfile = $uploaddir . $filename;

					if (move_uploaded_file($_FILES['filename']['tmp_name'], $uploadfile))
						$file_uploaded = TRUE;

					$this->core->db_quickupdate(PREFIX . 'ATP_TABLE_TEMPLATE', array('FILE' => $filename), $template_id);
				}
				if (!$file_uploaded)
					$this->core->db_query("DELETE FROM " . PREFIX . "ATP_TABLE_TEMPLATE WHERE ID={$template_id} LIMIT 1");
			} elseif (!empty($_GET['delete'])) {
				if (!empty($_GET['template_id'])) {
					$template_id = $_GET['template_id'];
					$this->core->db_query("DELETE FROM " . PREFIX . "ATP_TABLE_TEMPLATE WHERE ID={$_GET['template_id']} AND TABLE_ID={$table_id} LIMIT 1");
					// TODO: trint failą iš failų sistemos?
				}
			} elseif (!empty($_GET['download'])) {
				if (!empty($_GET['template_id'])) {
					$template_id = $_GET['template_id'];
					$this->get_raw_document();
					exit;
				}
			} else if (!empty($_POST['submit_services'])) {
				foreach ($_POST['service_id'] as $template_id => $service_id) {
					$qry = 'UPDATE ' . PREFIX . 'ATP_TABLE_TEMPLATE SET SERVICE_ID = :service_id WHERE ID = :id';
					$this->core->db_query_fast($qry, array('id' => $template_id, 'service_id' => $service_id));
				}
			}
		}

		$tmpl = $this->core->Template();
		$tmpl_handler = 'documents_file';
		$tmpl->set_file($tmpl_handler, 'documents.tpl');

		// Dokumentas iš dokumentų pasirinkimo
		$document_block = $tmpl_handler . 'document_block';
		$tmpl->set_block($tmpl_handler, 'document_block', $document_block);

		$data = array();

		// Dokumento šablono kintamojo eilutė
		$data['var_row_block'] = $var_row_block = $tmpl_handler . 'var_row_block';
		$tmpl->set_block($tmpl_handler, 'var_row_block', $var_row_block);
		// Naujo šablono įkėlimo blokas
		$new_template = $tmpl_handler . 'new_template';
		$tmpl->set_block($tmpl_handler, 'new_template', $new_template);
		// Šablono trinimas
		$template_delete = $tmpl_handler . 'template_delete';
		$tmpl->set_block($tmpl_handler, 'template_delete', $template_delete);
		// Šablono atsisiuntimo blokas
		$template_download = $tmpl_handler . 'template_download';
		$tmpl->set_block($tmpl_handler, 'template_download', $template_download);
		// Šablonų konteineris
		$templates_block = $tmpl_handler . 'templates';
		$tmpl->set_block($tmpl_handler, 'templates', $templates_block);
		// Pranešimas, kad nėra šablonų
		$no_template = $tmpl_handler . 'no_template';
		$tmpl->set_block($tmpl_handler, 'no_template', $no_template);
		// Dokumento informacijos blokas
		$document_info_block = $tmpl_handler . 'document_info_block';
		$tmpl->set_block($tmpl_handler, 'document_info_block', $document_info_block);

		$tmpl->set_var(array(
			'title' => 'Dokumentai',
			'sys_message' => $this->core->get_messages(),
			'GLOBAL_SITE_URL' => GLOBAL_SITE_URL
		));

		$tables = $this->logic2->get_document();
		if (count($tables) === 0)
			$tmpl->clean($document_block);
		else
			foreach ($tables as $table) {
				$tmpl->set_var(array(
					'table_id' => $table['ID'],
					'table_label' => $table['LABEL'],
					'selected' => $table['ID'] == $table_id ? ' selected="selected"' : ''));
				$tmpl->parse($document_block, 'document_block', true);
			}

		if (empty($tables[$table_id]) === FALSE) {
			$data['field_type'] = $this->logic->get_field_type();

			if ($this->core->user->status['A_DOCUMENT'] > 1) {
				$tmpl->set_var(array(
					'table_id' => $table_id));
				$tmpl->parse($new_template, 'new_template');
			} else {
				$tmpl->clean($new_template);
			}
			
			$table_template = $this->logic2->get_template(array('TABLE_ID' => $table_id));
			if (empty($table_template) === FALSE) {
				foreach ($table_template as $template) {

					if (empty($template['SERVICE_ID']) === FALSE)
						$service = $this->logic2->get_external_service(array('ID' => $template['SERVICE_ID']), true, false, 'ID, SHOWS');

					$tmpl_data = array(
						'table_id' => $table_id,
						'template_id' => $template['ID'],
						'template_label' => $template['LABEL'],
						'service_id' => $service['ID'],
						'service_name' => $service['SHOWS']
					);

					if ($this->core->user->status['A_DOCUMENT'] > 1) {
						$tmpl->set_var($tmpl_data);
						$tmpl->parse($template_delete, 'template_delete');
					} else
						$tmpl->clean($template_delete);
					$tmpl->set_var($tmpl_data);
					$tmpl->parse($template_download, 'template_download', true);
				}

				$tmpl->set_var(array(
				));
				$tmpl->parse($templates_block, 'templates');
				//$tmpl->clean('no_template');
				$tmpl->set_var($no_template, '');
			} else {
				//$tmpl->clean($template_download);
				$tmpl->clean($templates_block);
				$tmpl->parse($no_template, 'no_template');
			}

			$data['table_name'] = $tables[$table_id]['LABEL'];
			$this->document_fields($table_id, 'DOC', $data, $tmpl);
			$tmpl->parse($document_info_block, 'document_info_block');
		} else
			$tmpl->clean($document_info_block);

		$tmpl->clean($no_template);
		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');
		return $this->core->atp_content($output);
	}

	/**
	 * <b>(Dokumentų šablonas)</b> Spausdina dokumento kinatmuosius ir/arba garžina rastus laukus
	 * @author Justinas Malūkas
	 * @param int $id Įrašo ID
	 * @param string $type Įrašo tipas
	 * @param array $data Papildomi duomenys
	 * @param object $tmpl [optional] Šablonų valdymo objektas
	 * @param int $depth Lauko gylis
	 * @param array $all_fields Visis rasti laukai
	 * @param array $nr ID masyvas unikaliam kintamųjų pavadinimų generavimui
	 * @return array Visi rasti laukai
	 */
	public function document_fields($id, $type, $data, &$tmpl = null, $depth = 0, &$all_fields = array(), &$nr = array()) {
		if (in_array($type, array_keys($this->core->_prefix->item), true) === FALSE) {
			trigger_error('Unknown item type', E_USER_WARNING);
			return false;
		}
		if (empty($data['prefix']) && empty($data['table_name']) === FALSE) {
			$data['prefix'] = $this->parse_document_template_variable_prefix_from_string($data['table_name']);
		}

		if (empty($tmpl) === FALSE) {
			if (isset($data['multiplier']))
				$data['multiplier'] = (int) $data['multiplier'];
			else
				$data['multiplier'] = 6;
		}

		// Gauna įrašo laukus
		/* if ($type === 'DEED') {
		  // TODO: Veikų laukai vienodi
		  $fields = array(
		  array('NAME' => 'DCOL_1', 'LABEL' => 'Punktas', 'TYPE' => '7'),
		  array('NAME' => 'DCOL_2', 'LABEL' => 'Tekstas', 'TYPE' => '8')
		  );
		  } else */
		if ($type === 'ACT') {
			
			//$info = $this->logic->get_info_fields(array('ID' => $arr['ID'], 'TYPE' => 'ACT'));
			$info = $this->logic->get_info_fields(array('TYPE' => 'ACT'));
			foreach ($info['header'] as $key => $arr) {
				$fields[] = array('NAME' => 'AICOL_' . $key, 'LABEL' => $arr, 'TYPE' => '7', 'SORT' => '2');
			}
			$fields = array_merge($fields, $this->logic2->get_fields(array('ITYPE' => $type), false, 'ORD ASC'));
		} else
		if ($type === 'CLAUSE') {
			//$info = $this->logic->get_info_fields(array('ID' => $arr['ID'], 'TYPE' => 'CLAUSE'));
			$info = $this->logic->get_info_fields(array('TYPE' => 'CLAUSE'));
			foreach ($info['header'] as $key => $arr) {
				$fields[] = array('NAME' => 'SICOL_' . $key, 'LABEL' => $arr, 'TYPE' => '7', 'SORT' => '2');
			}
			$fields = array_merge($fields, $this->logic2->get_fields(array('ITYPE' => $type), false, 'ORD ASC'));
		} else {
			$fields = $this->logic->get_extra_fields($id, $type);
		}

		if ($type === 'WS') {
			$fields2 = array($this->logic2->get_fields(array('ID' => $id, 'ITYPE' => $type), false));
		} else {
		  #$fields = $this->logic->get_extra_fields($id, $type); }
		  $fields2 = array(); }

		$fields = array_merge((array) $fields, (array) $fields2);

		if ($type === 'DOC') {
			$record_person_fields = $this->logic->record_worker_info_to_fields(null);
			$fields = array_merge($fields, $record_person_fields);
			$record_post_fields = $this->logic->record_post_info_to_fields(null);
			$fields = array_merge($fields, $record_post_fields);
		}

		foreach ($fields as &$field) {
			if (empty($field) === TRUE)
				continue;

			// Suformuoja kintamojo pavadinimą
			$expl = explode('_', $field['NAME']);
			$expl[1] = end($expl);
			$expl[0] = str_replace('COL', '', $expl[0]);
			if (empty($expl[0]) === TRUE)
				$expl[0] = 'T'; //$this->core->_prefix->item['DOC'];
			$nr[$depth] = $expl[0] . $expl[1];
			$name = $data['prefix'] . join('', $nr);

			// Kintamasis
			$field['VAR_NAME'] = '${' . $name . '}';

			// Spausdinama kintamojo informacija
			$this->document_fields_field_parser(array(
				'NAME' => str_repeat('&nbsp;', $data['multiplier'] * $depth) . $field['VAR_NAME'],
				'LABEL' => $field['LABEL'],
				'TYPE' => $data['field_type'][$field['TYPE']]['LABEL'],
					), $data, $tmpl);

			$field['depth'] = $depth;
			
			
			if (isset($data['options']) && $data['options'] === TRUE) {
				if($data['options_value_use_name']) {
					$value = $name;
				} else {
					$value = $field['ID'];
				}
				
				$all_fields[] = '<option value="' . $value . '" for="' . $field['ID'] . '" class="sort-' . $field['SORT'] . ' type-' . $field['TYPE'] . ' level' . (int) $depth . '">' . (str_repeat('&nbsp;', $data['multiplier'] * $depth) . $field['LABEL']) . '</option>';
			}
			if(isset($field['SORT']) === false) {
				var_dump($field);die;
			}
			// Tikrinamas kintamojo tipas nustatyti tolimesnius veiksmus
			// Klasifikatorius kintamasis
			if ($field['TYPE'] === '18') {
				// TODO: gauna tik pirmo lygio klasifikatorius, gal reikia visų?
				//$items = $this->logic->get_classification(null, $field["SOURCE"]);
				$items = $this->logic2->get_classificator(array('PARENT_ID' => $field['SOURCE']));
				foreach ($items as &$item) {
					// Spausdina klasifikatoriaus pavadinimą
					$this->document_fields_field_parser(array(
						'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['LABEL']
							), $data, $tmpl);

					if (isset($data['options']) && $data['options'] === TRUE) {
						$all_fields[] = '<option value="-1" disabled="disabled" class="parent level' . (int) ($depth + 1) . '">' . (str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['LABEL']) . '</option>';
					}
					// Gilesnių laukų spausdinimui
					$nr[$depth + 1] = $this->core->_prefix->item['CLASS'] . $item['ID'];
					$this->document_fields($item['ID'], 'CLASS', $data, $tmpl, $depth + 2, $all_fields, $nr);
					unset($nr[$depth + 1]);
				}
			} else
			// Straipsnio kintamasis
			if ($field['SORT'] === '6') {
				/* $items = $this->logic2->get_clause();
				  foreach ($items as &$item) {
				  // Spausdina straipsnio pavadinimą
				  $this->document_fields_field_parser(array(
				  'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['NAME']
				  ), $data, $tmpl);

				  if ($data['options'] === TRUE) {
				  $all_fields[] = '<option value="-1" disabled="disabled" class="parent level' . (int) ($depth + 1) . '">' . (str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['NAME']) . '</option>';
				  }
				  // Gilesnių laukų spausdinimui
				  $nr[$depth + 1] = $this->core->_prefix->item['CLAUSE'] . $item['ID'];
				  $this->document_fields($item['ID'], 'CLAUSE', $data, $tmpl, $depth + 2, $all_fields, $nr);
				  unset($nr[$depth + 1]);
				  } */
				$this->document_fields_field_parser(array(
					'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . 'Straipsnis'
						), $data, $tmpl);
				$this->document_fields($item['ID'], 'CLAUSE', $data, $tmpl, $depth + 2, $all_fields, $nr);
			} else
			// Veika, teisės aktas ir straipsniai
			if ($field['TYPE'] === '23') {
				// Gilesnių veikos laukų spausdinimui
				//$nr[$depth + 2] = $this->core->_prefix->item['DEED'];
				//$this->document_fields(null, 'DEED', $data, $tmpl, $depth + 1, $all_fields, $nr);
				// Gilesnių teisės akto laukų spausdinimui
				$this->document_fields_field_parser(array(
					'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . 'Teisės akto punktas',
					'LABEL' => '',
					'TYPE' => '',
						), $data, $tmpl);
				$this->document_fields(null, 'ACT', $data, $tmpl, $depth + 2, $all_fields, $nr);
				// Gilesnių teisės akto laukų spausdinimui
				$this->document_fields_field_parser(array(
					'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . 'Straipsnis',
					'LABEL' => '',
					'TYPE' => '',
						), $data, $tmpl);

				$this->document_fields(null, 'CLAUSE', $data, $tmpl, $depth + 2, $all_fields, $nr);
				//unset($nr[$depth + 2]);
				/*

				  // Surenka visus straipsnius priskirtus veikoms
				  $extends = $this->logic->get_deed_extend_data(array('A.ITYPE = "CLAUSE"'));
				  // Surenka unikalius straipsnius
				  $clauses = array();
				  foreach ($extends as &$extend) {
				  if ($extend['ITYPE'] !== 'CLAUSE')
				  continue;
				  $clauses[$extend['ITEM_ID']] = $extend;
				  }
				  $extends = &$clauses;

				  foreach ($extends as &$item) {
				  // Spausdina straipsnio pavadinimą
				  $this->document_fields_field_parser(array(
				  'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['INAME']
				  ), $data, $tmpl);

				  if ($data['options'] === TRUE) {
				  $all_fields[] = '<option value="-1" disabled="disabled" class="parent level' . (int) ($depth + 1) . '">' . (str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['INAME']) . '</option>';
				  }
				  // Gilesnių laukų spausdinimui
				  $nr[$depth + 1] = $this->core->_prefix->item['DEED'] . $this->core->_prefix->item['CLAUSE'] . $item['ITEM_ID'];
				  $this->document_fields($item['ITEM_ID'], 'CLAUSE', $data, $tmpl, $depth + 2, $all_fields, $nr);
				  unset($nr[$depth + 1]);
				  }
				 */
				//unset($nr[$depth]);
			}
			// Web-servisas
			if (isset($field['WS']) && $field['WS'] === '1') {
				$this->document_fields_field_parser(array(
					'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . 'Web-servisai',
					'LABEL' => '',
					'TYPE' => '',
						), $data, $tmpl);

				$ws = $this->logic2->get_ws();
				$relations = $this->logic2->get_webservices_fields(array('FIELD_ID' => $field['ID']));
				$fields_struct = $this->logic2->get_fields(array('ITYPE' => 'WS', 'ITEM_ID' => $field['ID']), false, '`ORD` ASC');

				foreach ($relations as &$relation) {
					$info = $ws->get_info($relation['WS_NAME']);
					if (empty($info))
						continue;

					$this->document_fields_field_parser(array(
						'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 2)) . $info['label'],
						'LABEL' => '',
						'TYPE' => '',
							), $data, $tmpl);

					$nr[$depth + 1] = 'WS' . $info['name'];

					$ws_fields = $this->logic2->get_webservices(array('WS_NAME' => $relation['WS_NAME'], 'PARENT_FIELD_ID' => $field['ID']));
					foreach ($ws_fields as &$ws_field) {
						$arr = &$fields_struct[$ws_field['FIELD_ID']];
						if (empty($arr))
							continue;
						$this->document_fields($arr['ID'], 'WS', $data, $tmpl, $depth + 3, $all_fields, $nr);
					}

					unset($nr[$depth + 1]);
				}
			}

			unset($nr[$depth]);

			if (isset($data['options']) === FALSE || $data['options'] !== TRUE)
				$all_fields[] = $field;
		}

		if ($type === 'DOC') {
			
		}
		return $all_fields;
	}

	/**
	 * <b>(Dokumentų šablonas)</b> Spausdina lauko informaciją
	 * @param array $data
	 * @param array $extradata
	 * @param object $tmpl
	 * @return boolean
	 */
	private function document_fields_field_parser($data, &$extradata, &$tmpl) {
		if (empty($tmpl) === TRUE)
			return false;
		
		$default = array(
			'NAME' => null, 
			'LABEL' => null,
			'TYPE' => null
		);
		$data = array_merge($default, (array) $data);
		
		$tmpl->set_var(array(
			'field_varname' => $data['NAME'],
			'field_label' => $data['LABEL'],
			'field_type' => $data['TYPE']));
		
		$tmpl->parse($extradata['var_row_block'], 'var_row_block', true);
		return true;
	}

	/**
	 * Dokumento šablono atsisiuntimas
	 */
	public function get_raw_document() {
		$result = $this->core->db_query('SELECT * FROM ' . PREFIX . 'ATP_TABLE_TEMPLATE WHERE `ID` = ' . (int) $_GET['template_id']);
		$row = $this->core->db_next($result);
		if (empty($row))
			return false;

		$expl = explode('.', $row['FILE']);
		$extension = '.' . end($expl);

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . basename($row['LABEL'] . $extension) . '"');
		header('Content-Transfer-Encoding: binary');
		header('Connection: Keep-Alive');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		//header('Content-Length: ' . filesize($file));
		readfile('files/templates/' . $row['FILE']);
	}

	/**
	 * Iš dokumento pavadinimo sugeneruoja dokumento šablono kintamojo priešdėlį
	 * @param string $str Dokumento pavadinimas
	 * @return string Dokumento priešdelis
	 */
	public function parse_document_template_variable_prefix_from_string($str) {
		return strtoupper(substr(preg_replace('/[^\da-z]/i', '', $str), 0, 4));
	}

	/**
	 * Sugeneruoja dokumento įrašo šabloną ir paduoda atsisiųsti
	 * @authors Martynas Boika, Justinas Malūkas
	 */
	public function get_document() {
		$param['id'] = empty($_GET['id']) ? 0 : $_GET['id'];
		$param['template_id'] = (int) $_GET['template_id'];

		$record = $this->logic2->get_record_relation($param['id']);
		if (empty($record) === TRUE) {
			trigger_error('Record not found', E_USER_ERROR);
			exit;
		}

		$param['record'] = $record['ID'];

		if (empty($param['template_id'])) {
			echo 'Missing parameters';
			trigger_error('Missing parameters', E_USER_WARNING);
			header('Refresh: 5; URL=' . $_SERVER['HTTP_REFERER']);
			exit();
		}
		$data = $this->action->generate_document($param);

		// Dokumentą pateikia atsiisiuntimui
		if (is_file($data['realpath']) === TRUE) {
			header('Content-Type: application/docx; charset=utf-8');
			header('Content-Description: File Transfer');
			header("Content-Disposition: attachment; filename={$data['filename']}");
			header('Pragma: no-cache');

			readfile($data['realpath']);
		} else {
			echo 'File not found';
			header('Refresh: 5; URL=' . $_SERVER['HTTP_REFERER']);
			exit();
		}
	}

	/**
	 * Straipsnių puslapis
	 * @return string
	 */
	public function clause() {
		if (!$this->core->user->status['A_CLAUSE']) {
			$tmpl = $this->core->Template();
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
			$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
			$output = $tmpl->get($tmpl_handler . '_out');
			return $this->core->atp_content($output);
		}

		$tmpl = $this->core->Template();
		$tmpl_handler = 'clause_file';
		$tmpl->set_file($tmpl_handler, 'clause.tpl');

		$new_block = $tmpl_handler . 'new';
		$tmpl->set_block($tmpl_handler, 'new', $new_block);

		$tmpl->set_var(array(
			'GLOBAL_SITE_URL' => GLOBAL_SITE_URL,
			'sys_message' => $this->core->get_messages(),
			'clause_list' => $this->table_clauses()
		));

		if ($this->core->user->status['A_CLAUSE'] > 1)
			$tmpl->parse($new_block, 'new');
		else
			$tmpl->clean($new_block);

		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');

		return $this->core->atp_content($output);
	}

	public function table_clauses() {
		$html_manage = '';

		// Surenka klasifikatorius
		$tables = $this->logic2->get_clause();

		// Surenkami papildomi duomenys apie įrašus
		foreach ($tables as $table) {
			$new_tables[$table['ID']] = $table;

			// Surenka galimus veiksmus
			$new_tables[$table['ID']]['ACTIONS'] = array();
			$new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="expand_collapse_block">
					<div class="expand_collapse_img_block">
						<img style="cursor: pointer;" src="' . GLOBAL_SITE_URL . 'images/atp/expand_doc_action.png" width="29" height="29">
					</div>
					<div class="actions_block" style="display: none;">
						<img style="display: block;" src="' . GLOBAL_SITE_URL . 'images/actions/top_img_actions_buttons.png" width="132" height="8">
						<div class="actions_buttons">';
			$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button addChild" onClick="window.location=\'index.php?m=10&id=' . $table['ID'] . '&action=sub\';"><span>Pridėti vaiką</span></div>';
			$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button remove" id="atp-table-id-' . $table['ID'] . '"><span>Trinti</span></div>';
//			$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button addFields"><span>Sukurti laukus</span></div>';
			$new_tables[$table['ID']]['ACTIONS'][] = '
						</div>
					</div>
				</div>';
			$new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="action_label">' .
					/* <div class="atp-html-button edit" id="atp-table-id-' . $table['ID'] . '" onClick="window.location=\'index.php?m=11&clause_id=' . $table['ID'] . '&edit=1\';"><span>' . $this->lng('edit') . '</span></div> */
					'<a class="atp-html-button edit dialog" href="index.php?m=10&id=' . $table['ID'] . '&action=editform">' . $this->lng('edit') . '</a>
				</div>';
			$new_tables[$table['ID']]['ACTIONS'] = join(' ', $new_tables[$table['ID']]['ACTIONS']);

			// Gaunamas įrašo lygis
			$new_tables[$table['ID']]['LEVEL'] = $this->logic->get_table_level($tables, $table['ID']);

			// Nurodomas išskleidimo stulpelis, jei laukas turi vaikų
			if ($this->logic->table_has_children($tables, $table['ID'])) {
				$new_tables[$table['ID']]['TOGGLE'] = '<div class="collapse atp-table-list-toggle"></div>';
				$toggle_class = 'collapse';
			} else
				$toggle_class = 'none';

			$new_tables[$table['ID']]['CLAUSE'] = '<div class="cell-CLAUSE-container"><div class="' . $toggle_class . ' atp-table-list-toggle"></div><span>' . $new_tables[$table['ID']]['CLAUSE'] . ' str.' . '</span></div>';
		}
		$tables = &$new_tables;

		// Įrašai surikiuojami pagal hierarchiją (tevas -> vaikai, tevas -> vaikai)
		$tables = $this->logic->sort_children_to_parent($tables, 'ID', 'PARENT_ID');

		$tpl_arr = $this->core->getTemplate('table2');

		// Nurodomi išvedami lentelės stulpeliai
		$columns = array(
			//'TOGGLE' => $this->lng('atp_table_toggle'),
			'CLAUSE' => $this->lng('atp_clause_clause'),
			'SECTION' => $this->lng('atp_clause_section'),
			'PARAGRAPH' => $this->lng('atp_clause_paragraph'),
			'SUBPARAGRAPH' => $this->lng('atp_clause_subparagraph'),
			'LABEL' => $this->lng('atp_clause_label'),
			'PENALTY_MIN' => $this->lng('atp_clause_penalty_min'),
			'PENALTY_MAX' => $this->lng('atp_clause_penalty_max'),
			'NOTICE' => $this->lng('atp_clause_notice'),
			'NOTICE_LABEL' => $this->lng('atp_clause_notice_label'),
			'TERM' => $this->lng('atp_clause_term'),
			'INVESTIGATION_CNT' => $this->lng('atp_clause_investigation_cnt'),
			'EXAMINATION_CNT' => $this->lng('atp_clause_examination_cnt')
		);
		// Tikrinama teisė į veiksmų stulpelio matymą
		if ($this->core->user->status['A_CLAUSE'] > 1)
			$columns['ACTIONS'] = $this->lng('atp_clause_actions');

		// table
		$table = array();
		$table['table_class'] = 'div';
		$html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

		// Lentelės antraštė
		$header = array('row_html' => '');
		$header['row_class'] = 'div-row div-header';
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);
		foreach ($columns as $col_name => $col) {
			// cell
			$cell = array('cell_html' => '');
			$cell['cell_class'] = 'div-cell cell-' . $col_name;
			$cell['cell_content'] = $col;
			$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
		}
		$html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

		// Lentelės įrašų eilutės
		foreach ($tables as $key => $table) {

			// Eilutės duomenys
			$row = array();
			$row['row_html'] = 'id="atp-table-list-item-id-' . $table['ID'] . '"';
			$row['row_class'][] = 'div-row';
			if ($table['LEVEL'])
				$row['row_class'][] = 'child';
			$row['row_class'][] = 'level-' . $table['LEVEL'];
			$row['row_class'][] = 'child-of-' . $table['PARENT_ID'];
			$row['row_class'] = join(' ', $row['row_class']);
			$html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

			$investigation_cnt = 0;
			$examination_cnt = 0;
			/* $clauses_competence = $this->logic->get_clause_competence(array('CLAUSE_ID' => $table['ID']));
			  foreach ((array) $clauses_competence as $competence) {
			  if ($competence['TYPE'] == 1)
			  $investigation_cnt++;
			  elseif ($competence['TYPE'] == 2)
			  $examination_cnt++;
			  } */
			$table['INVESTIGATION_CNT'] = $investigation_cnt;
			$table['EXAMINATION_CNT'] = $examination_cnt;

			// Eilutės stulpeiai
			foreach ($columns as $col_name => $col) {

				// Stulpelio duomenys
				$cell = array('cell_html' => '', 'cell_content' => '');
				$cell['cell_class'] = 'div-cell cell-' . $col_name;
				switch ($col_name) {
					//	case 'CLAUSE':
					//		//$table[$col_name] = $table[$col_name] . ' str.';
					//		$table[$col_name] = '<div class="cell-LABEL-container"><div class="' . $toggle_class . ' atp-table-list-toggle"></div><span>' . $table[$col_name] . ' str.' . '</span></div>';
					//		break;
					case 'SECTION':
						$table[$col_name] = (!empty($table[$col_name]) ? $table[$col_name] . ' d.' : '');
						break;
					case 'PARAGRAPH':
						$table[$col_name] = (!empty($table['PARAGRAPH']) ? $table['PARAGRAPH'] . ' p.' : '');
						break;
					case 'SUBPARAGRAPH':
						$table[$col_name] = (!empty($table['SUBPARAGRAPH']) ? $table['SUBPARAGRAPH'] : '');
						break;
					case 'NOTICE':
						$table[$col_name] = ($table['NOTICE'] ? 'Galima skirti' : 'Negalima skirti');
						break;
				}

				$cell['cell_content'] .= $table[$col_name];
				$html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
			}

			$html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
		}

		$html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

		return $html_manage;
	}

	/*
	  public function departments() {
	  if (!$this->core->user->status['A_DEPARTMENT'])
	  return FALSE;

	  if (isset($_GET['action']) && $this->core->user->status['A_DEPARTMENT'] > 1)
	  $this->logic->edit_department();

	  $tpl_arr = $this->core->getTemplate('departments');

	  $html_data['sys_message'] = $this->core->get_messages();
	  $html_manage = $this->core->returnHTML($html_data, $tpl_arr[0]);

	  $m_departments[490] = $this->logic->get_m_department_list(490);
	  $m_departments[189] = $this->logic->get_m_department_list(189);
	  $m_departments = array_merge($m_departments[490], $m_departments[189]);

	  $cnt = 0;
	  foreach ($m_departments as $m_department) {
	  $department = $this->logic->get_department($m_department['ID'], TRUE);
	  $department_action = (!empty($department) ? 'remove' : 'add');
	  $department_class = (!empty($department) ? 'selected' : 'select');
	  $html_manage .= $this->core->returnHTML(array('cnt' => ++$cnt, 'department_link' => 'index.php?m=10&action=' . $department_action . '&main_structure_id=' . $m_department['ID'], 'department_class' => 'atp-department-' . $department_class, 'department_level' => str_repeat('----', $m_department['LEVEL']), 'department_label' => $m_department['SHOWS']), $tpl_arr[1]);
	  }
	  $html_manage .= $tpl_arr[2];

	  return $this->core->atp_content($html_manage);
	  } */

	/**
	 * Teisės akto redagavimo forma
	 * @author Justinas Malūkas
	 * @param boolen $ajax Nurodo ar formuoti pilną puslapio HTML'ą
	 * @return string
	 */
	public function act_editform($ajax = false) {
		$tmpl = $this->core->Template();
		if (!$this->core->user->status['A_ACT']) {
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
			$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
			$output = $tmpl->get($tmpl_handler . '_out');
			return $this->core->atp_content($output);
		}


		if ($this->core->content_type !== 2) {
			$this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
			$this->core->_load_files['js'][] = 'jquery.validationEngine.js';
			$this->core->_load_files['css'][] = 'validationEngine.jquery.css';
		}

		$tmpl_handler = 'act_editform_file';
		$tmpl->set_file($tmpl_handler, 'act_editform.tpl');

		// Papildomo lauko blokas
		$field_block = $tmpl_handler . 'field';
		$tmpl->set_block($tmpl_handler, 'field', $field_block);
		// Formos blokas
		$form_block = $tmpl_handler . 'form';
		$tmpl->set_block($tmpl_handler, 'form', $form_block);

		$vars = array();
		$m = (int) $this->core->extractVariable('m');

		$id = (int) $this->core->extractVariable('id');
		$parent_id = (int) $this->core->extractVariable('parent');

		$vars['sys_message'] = $this->core->get_messages();
		$vars['save'] = 'Išsaugoti';

		$data = array();
		if (empty($id) === FALSE) {
			$data = $this->logic2->get_act(array('ID' => $id));
		}

		//
		if (empty($data) === TRUE) {
			$vars['url'] = 'index.php?m=' . $m . '&parent=' . $parent_id . '&action=save';
			//$vars['title'] = 'Naujas teisės aktas';
			$vars['title'] = 'Naujas punktas';
			$vars['act_id'] = $vars['act_name'] = $vars['number'] = $vars['name'] = $vars['valid_from'] = $vars['valid_till'] = '';
		} else {
			$vars['url'] = 'index.php?m=' . $m . '&id=' . $id . '&action=save';
			//$vars['title'] = 'Teisės aktas: ' . $data['LABEL'];
			$vars['title'] = 'Punktas: ' . $data['LABEL'];

			//$vars['name'] = htmlspecialchars($data['LABEL']);
			$vars['number'] = $data['NR'];
			$vars['valid_from'] = $data['VALID_FROM'];
			$vars['valid_till'] = $data['VALID_TILL'];

			// Klasifikatoriaus (Teisės akto) laukas
			$classificator = $this->logic2->get_classificator(array('ID' => $data['CLASSIFICATOR']));
			$vars['act_id'] = $classificator['ID'];
			$vars['act_name'] = htmlspecialchars($classificator['LABEL']);
		}


		// Papildomi laukai
		$fields = $this->logic2->get_act_default_fields_structure();
		$fields = sort_by($fields, 'ORD', 'asc');
		// Laukų reikšmės
		$fields_values = $this->logic2->get_fields(array('ITYPE' => 'ACT', 'ITEM_ID' => $data['ID']));

		foreach ($fields as &$field) {
			$info = &$fields_values[$field['ID']];
			$tmpl->set_var(array(
				'id' => $field['ID'],
				'name' => $field['LABEL'],
				'value' => htmlspecialchars($info['DEFAULT'])
			));
			$tmpl->parse($field_block, 'field', true);
		}


		$vars['atp_act_number'] = $this->lng('atp_act_number');
		//$vars['atp_act_label'] = $this->lng('atp_act_label');
		$vars['atp_act_classificator'] = $this->lng('atp_act_classificator');
		$tmpl->set_var($vars);

		if ($this->core->user->status['A_ACT'] > 1) {
			$tmpl->parse($form_block, 'form');
		} else {
			$tmpl->clean($form_block);
		}

		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');

		return $this->core->atp_content($output);
	}

	/**
	 * Veikos redagavimo forma
	 * @param boolen $ajax Nurodo ar formuoti pilną puslapio HTML'ą
	 * @return string
	 */
	public function deed_editform($ajax = false) {
		$tmpl = $this->core->Template();
		if (!$this->core->user->status['A_DEED']) {
			$tmpl_handler = 'home_file';
			$tmpl->set_file($tmpl_handler, 'home.tpl');
			$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
			$output = $tmpl->get($tmpl_handler . '_out');
			if ($ajax === TRUE)
				return $output;
			return $this->core->atp_content($output);
		}

		$tmpl_handler = 'deed_editform_file';
		$tmpl->set_file($tmpl_handler, 'deed_editform.tpl');

		// Straipnsių sąrašas
		$clauses_list = $tmpl_handler . 'clauses_list';
		$tmpl->set_block($tmpl_handler, 'clauses_list', $clauses_list);
		// Teisės aktų sąrašas
		$acts_list = $tmpl_handler . 'acts_list';
		$tmpl->set_block($tmpl_handler, 'acts_list', $acts_list);
		// Forma
		$form_block = $tmpl_handler . 'form';
		$tmpl->set_block($tmpl_handler, 'form', $form_block);

		$vars = array();
		$m = (int) $this->core->extractVariable('m');

		if ($ajax === TRUE)
			$vars['tmpl_class'] = ' class="dialog"';

		$id = (int) $this->core->extractVariable('id');
		$parent_id = (int) $this->core->extractVariable('parent');
		$vars['sys_message'] = $this->core->get_messages();
		$vars['GLOBAL_SITE_URL'] = GLOBAL_SITE_URL;
		$vars['save'] = 'Išsaugoti';

		$tmpl->set_var(array(
			'clause_id' => 0,
			'clause_name' => '',
			'class' => 'sample'));
		$tmpl->parse($clauses_list, 'clauses_list');
		$tmpl->set_var(array(
			'act_id' => 0,
			'act_name' => '',
			'class' => 'sample'));
		$tmpl->parse($acts_list, 'acts_list');

		$data = array();
		if (empty($id) === FALSE) {
			$data = $this->logic->get_deed(array('ID' => $id));
		}

		$i = $j = 0;
		if (empty($data) === TRUE) {
			$vars['url'] = 'index.php?m=' . $m . '&parent=' . $parent_id . '&action=save';
			$vars['title'] = 'Naujas teisės aktas';
			$vars['name']/* = $vars['valid_from'] = $vars['valid_till'] */ = null;
		} else {
			$vars['url'] = 'index.php?m=' . $m . '&id=' . $id . '&action=save';
			$vars['title'] = 'Veika: ' . $data['LABEL'];
			$vars['name'] = htmlspecialchars($data['LABEL']);

			$extends = $this->logic->get_deed_extend_data(array('DEED_ID' => $id));
			foreach ($extends as $extend) {
				if ($extend['ITYPE'] === 'CLAUSE') {
					$tmpl->set_var(array(
						'clause_id' => $extend['ITEM_ID'],
						'clause_name' => $extend['INAME'],
						'class' => ''));
					$tmpl->parse($clauses_list, 'clauses_list', true);
					$i++;
				} else if ($extend['ITYPE'] === 'ACT') {
					$tmpl->set_var(array(
						'act_id' => $extend['ITEM_ID'],
						'act_name' => $extend['INAME'],
						'class' => ''));
					$tmpl->parse($acts_list, 'acts_list', true);
					$j++;
				}
			}
		}

		if ($this->core->user->status['A_DEED'] > 1) {
			$tmpl->parse($form_block, 'form');
		} else {
			$tmpl->clean($form_block);
		}

		$tmpl->set_var($vars);

		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');

		if ($ajax === TRUE)
			return $output;
		return $this->core->atp_content($output);
	}

	//http://www.vilnius.lt/pvstest/subsystems/atp/index.php?m=11&clause_id=59&edit=1
	//http://wp1.dev//subsystems/atp/index.php?m=11&clause_id=42&edit=1
	/**
	 * Straipsnio redagavimo forma
	 * @return string
	 */
	public function clause_editform() {
		$tmpl = $this->core->Template();
		if (!$this->core->user->status['A_CLAUSE']) {
			$tmpl->handler = 'home_file';
			$tmpl->set_file($tmpl->handler, 'home.tpl');
			$tmpl->parse($tmpl->handler . '_out', $tmpl->handler);
			$output = $tmpl->get($tmpl->handler . '_out');
			return $this->core->atp_content($output);
		}

		if ($this->core->content_type !== 2) {
			$this->core->_load_files['js'][] = 'jquery.autogrow-textarea.js';
			$this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
			$this->core->_load_files['js'][] = 'jquery.validationEngine.js';
			$this->core->_load_files['css'][] = 'validationEngine.jquery.css';
		}

		$tmpl->handler = 'clause_editform_file';
		$tmpl->set_file($tmpl->handler, 'clause_editform.tpl');

		$handler = $tmpl->set_multiblock($tmpl->handler, array(
			array(
				'field', // Papildomo lauko blokas
				'clause_type_option', // Pažeidimo tipo pasirinkimas
				'clause_group_option' // Straipsnio grupės pasirinkimas
			),
			'form' // Formos blokas
		));

		$m = (int) $this->core->extractVariable('m');


		$id = (int) $this->core->extractVariable('id');
		$parent = (int) $this->core->extractVariable('parent');



		if ($this->core->user->status['A_CLAUSE'] > 1) {
			if (isset($_POST['submit']))
				$this->action->clause();
		}

		$vars = array(
			'm' => $m,
			'GLOBAL_SITE_URL' => GLOBAL_SITE_URL,
			'sys_message' => $this->core->get_messages(),
			'save' => 'Išsaugoti',
			'hidden_fields' => ''
		);

		$vars['clause_codex_label'] = $this->lng('atp_clause_codex');
		$vars['clause_codex'] = '';
		$vars['e_clause_clause'] = '';
		$vars['e_clause_section'] = '';
		$vars['e_clause_paragraph'] = '';
		$vars['e_clause_subparagraph'] = '';
		$vars['e_clause_label'] = '';
		$vars['e_clause_deed'] = '';
		$vars['e_clause_penalty_min'] = '';
		$vars['e_clause_penalty_max'] = '';
		$vars['e_clause_penalty_first'] = '';
		$vars['e_clause_notice'] = '';
		$vars['e_clause_notice_label'] = '';
		$vars['e_clause_term'] = '';
		$vars['e_clause_create_section'] = '';
		$vars['e_clause_create_paragraph'] = '';
		$vars['e_clause_create_subparagraph'] = '';
		$vars['e_show_clause_section'] = '';
		$vars['e_show_clause_paragraph'] = '';
		$vars['e_show_clause_subparagraph'] = '';
		$vars['e_clause_class_type'] = '0';
		$vars['e_clause_group'] = '0';

		$data = array();
		if (empty($id) === FALSE) {
			$data = $this->logic2->get_clause(array('ID' => $id), true);
		}

		if (empty($data) === TRUE) {
			$vars['url'] = 'index.php?m=' . $m . '&id=' . $parent . '&action=save';
			$vars['title'] = 'Naujas straipsnis';
			$vars['name']/* = $vars['valid_from'] = $vars['valid_till'] */ = null;
		} else {
			$vars['url'] = 'index.php?m=' . $m . '&id=' . $id . '&action=save';
			$vars['title'] = 'Straipsnis: ' . $data['NAME'];
			$vars['name'] = htmlspecialchars($data['LABEL']);

			foreach ($data as $key => $value) {
				if ($key == 'NOTICE')
					$value = ($value ? ' checked="checked"' : '');
				elseif ($key == 'VALID_FROM') {
					if (empty($value) === FALSE)
						$value = date('Y-m-d', strtotime($value));
				} elseif ($key == 'VALID_TILL') {
					if (empty($value) === FALSE)
						$value = date('Y-m-d', strtotime($value));
				}

				$vars['e_clause_' . strtolower($key)] = htmlspecialchars($value);
			}
			$vars['clause_codex'] = htmlspecialchars($data['CODEX']);
		}

		if (empty($_GET['id']) === FALSE && $_GET['action'] === 'sub') {
			$clauses = $this->logic->search_clauses(array(
				'clause' => $vars['e_clause_clause'],
				'section' => $vars['e_clause_section'],
				'paragraph' => $vars['e_clause_paragraph'],
				'subparagraph' => $vars['e_clause_subparagraph']));
			$max = array();
			if (count($clauses)) {
				for ($i = 0; $i < count($clauses); $i++) {
					$max['section'] = max($max['section'], $clauses[$i]['SECTION']);
					$max['paragraph'] = max($max['paragraph'], $clauses[$i]['PARAGRAPH']);
					$max['subparagraph'] = max($max['subparagraph'], $clauses[$i]['SUBPARAGRAPH']);
				}
			}
		}
		$create_new = true;
		if (empty($data['SUBPARAGRAPH'])) {
			$vars['e_show_clause_subparagraph'] = ' style="display: none;"';
		} else {
			$vars['e_show_clause_subparagraph'] = '';
			$create_new = false;
		}
		if (empty($data['PARAGRAPH'])) {
			$vars['e_show_clause_paragraph'] = ' style="display: none;"';
			$vars['e_clause_create_subparagraph'] = '';
		} else {
			$vars['e_show_clause_paragraph'] = '';
			if ($create_new === TRUE) {
				if ($_GET['action'] === 'sub') {
					$vars['e_show_clause_subparagraph'] = '';
					$vars['e_clause_subparagraph'] = $max['subparagraph'] + 1;
				} else {
					$vars['e_clause_create_subparagraph'] = '<a href="index.php?m=' . $m . '&id=' . $id . '&action=sub">Sukurti papunktį</a>';
				}
				$create_new = false;
			}
		}
		if (empty($data['SECTION'])) {
			$vars['e_show_clause_section'] = ' style="display: none;"';
			$vars['e_clause_create_paragraph'] = '';
		} else {
			$vars['e_show_clause_section'] = '';
			if ($create_new === TRUE) {
				if ($_GET['action'] === 'sub') {
					$vars['e_show_clause_paragraph'] = '';
					$vars['e_clause_paragraph'] = $max['paragraph'] + 1;
				} else {
					$vars['e_clause_create_paragraph'] = '<a href="index.php?m=' . $m . '&id=' . $id . '&action=sub">Sukurti punktą</a>';
				}
				$create_new = false;
			}
		}
		if (empty($data['CLAUSE'])) {
			$vars['e_clause_create_section'] = '';
		} else {
			if ($create_new === TRUE) {
				if ($_GET['action'] === 'sub') {
					$vars['e_show_clause_section'] = '';
					$vars['e_clause_section'] = $max['section'] + 1;
				} else {
					$vars['e_clause_create_section'] = '<a href="index.php?m=' . $m . '&id=' . $id . '&action=sub">Sukurti dalį</a>';
				}
				$create_new = false;
			}
		}

		if (empty($id) === FALSE && $_GET['action'] === 'sub') {
			$vars['e_submit_title'] = 'Sukurti';
			$vars['e_title'] = 'Naujas straipsnis';
			$pass_keys = array('CLAUSE', 'SECTION', 'PARAGRAPH', 'SUBPARAGRAPH');
			foreach ($data as $key => $value) {
				if ($key == 'NOTICE')
					$value = ($value ? ' checked="checked"' : '');
				if (in_array($key, $pass_keys))
					continue;
				$vars['e_clause_' . strtolower($key)] = null;
			}
			$vars['hidden_fields'] = '<input type="hidden" name="parent_id" value="' . $id . '">';
		}

		// Papildomi laukai
		$fields = $this->logic2->get_fields(array('ITYPE' => 'CLAUSE', 'ITEM_ID' => $data['ID']));
		foreach ($fields as &$field) {
			$tmpl->set_var(array(
				'id' => $field['ID'],
				'name' => $field['LABEL'],
				'value' => htmlspecialchars($field['DEFAULT'])
			));
			$tmpl->parse($handler['field'], 'field', true);
		}

		
		$clause = new ATP_Document_Clause();

		// Pažeidimo tipo pasirinkiams
		$classificators = $this->logic2->get_classificator(array('PARENT_ID' => $clause->class_type_parent_id), null, null, 'ID, LABEL');
		foreach ($classificators as &$classificator) {
			$tmpl->set_var(array(
				'value' => $classificator['ID'],
				'label' => $classificator['LABEL'],
				'option_attr' => ((int) $classificator['ID'] === (int) $vars['e_clause_class_type'] ? ' selected="selected"' : '')
			));
			$tmpl->parse($handler['clause_type_option'], 'clause_type_option', true);
		}
		
		// Straipsnio grupės pasirinkiams
		$classificators = $this->logic2->get_classificator(array('PARENT_ID' => $clause->group_parent_id), null, null, 'ID, LABEL');
		foreach ($classificators as &$classificator) {
			$tmpl->set_var(array(
				'value' => $classificator['ID'],
				'label' => $classificator['LABEL'],
				'option_attr' => ((int) $classificator['ID'] === (int) $vars['e_clause_group'] ? ' selected="selected"' : '')
			));
			$tmpl->parse($handler['clause_group_option'], 'clause_group_option', true);
		}


		if ($this->core->user->status['A_CLAUSE'] > 1) {
			$tmpl->parse($handler['form'], 'form');
		} else {
			$tmpl->clean($handler['form']);
		}

		$tmpl->set_var($vars);

// <editor-fold defaultstate="collapsed" desc="Ryšių kolkas nebereikia (Jau ląąąbai seniai nebereikia)">
		/*
		  $clause_relations = array();
		  if (empty($clause_id) === FALSE) {
		  $clause_relations = $this->logic->get_clause_relation(array('CLAUSE_ID' => $clause_id));
		  }
		  $Marker_12 = $tmpl->handler . '_Marker_12';
		  $tmpl->set_block($tmpl->handler, 'Marker_12', $Marker_12);
		  $Marker_14 = $tmpl->handler . '_Marker_14';
		  $tmpl->set_block($tmpl->handler, 'Marker_14', $Marker_14);
		  $Marker_16 = $tmpl->handler . '_Marker_16';
		  $tmpl->set_block($tmpl->handler, 'Marker_16', $Marker_16);
		  $Marker_11 = $tmpl->handler . '_Marker_11';
		  $tmpl->set_block($tmpl->handler, 'Marker_11', $Marker_11);

		  $tables = $this->logic2->get_document();
		  $clause_structures = $this->logic->get_clause_structures();
		  $loop = count($clause_relations) ? count($clause_relations) : 1;

		  for ($i = 0; $i < $loop; $i++) {

		  // Dokumentas
		  $selected = false;
		  $current_table = null;
		  $j = 0;
		  foreach ($tables as $table_id => $table) {
		  $select = false;
		  if ($selected === false) {
		  foreach ($clause_relations as $relation_id => $relation) {
		  if ($relation['TABLE_ID'] == $table_id && $relation['SELECTED'] !== true) {
		  $selected = $select = true;
		  $clause_relations[$relation_id]['SELECTED'] = true;
		  $current_table = $table_id;
		  break;
		  }
		  }
		  }

		  $tmpl->set_var(array(
		  'table_id_12' => $table['ID'],
		  'table_label_12' => $table['LABEL'],
		  'table_selected_12' => $select ? 'selected="selected"' : ''));
		  $tmpl->parse($Marker_12, 'Marker_12', $j);
		  $j++;
		  }

		  // Dokumento laukas
		  if (empty($current_table) === FALSE) {
		  $table_structure = $this->logic->get_table_fields($current_table);
		  $selected = false;
		  $j = 0;
		  foreach ($table_structure as $col_id => $col) {
		  $select = false;
		  if ($selected === false) {
		  foreach ($clause_relations as $relation_id => $relation) {
		  if ($relation['TABLE_ID'] == $current_table && $relation['TABLE_STRUCTURE_ID'] == $col_id && $relation['SELECTED_STRUCTURE'] !== true) {
		  $selected = $select = true;
		  $clause_relations[$relation_id]['SELECTED_STRUCTURE'] = true;
		  $current_field = $clause_relations[$relation_id]['TABLE_STRUCTURE_ID'];
		  break;
		  }
		  }
		  }
		  $tmpl->set_var(array(
		  'column_id_14' => $col['ID'],
		  'column_label_14' => $col['LABEL'],
		  'column_selected_14' => $select ? 'selected="selected"' : ''));
		  $tmpl->parse($Marker_14, 'Marker_14', $j);
		  $j++;
		  }
		  }

		  // Straipsnio laukas (?)
		  $selected = false;
		  $j = 0;
		  foreach ($clause_structures as $key => $clause) {
		  $select = false;
		  if ($selected === false) {
		  foreach ($clause_relations as $relation_id => $relation) {
		  if ($relation['TABLE_ID'] == $current_table && $relation['CLAUSE_STRUCTURE_ID'] == $clause['ID'] && $relation['TABLE_STRUCTURE_ID'] == $current_field && $relation['SELECTED_CLAUSE'] !== true) {
		  $selected = $select = true;
		  $clause_relations[$relation_id]['SELECTED_CLAUSE'] = true;
		  break;
		  }
		  }
		  }
		  $tmpl->set_var(array(
		  'clause_id_16' => $clause['ID'],
		  'clause_label_16' => $clause['LABEL'],
		  'clause_selected_16' => $select ? 'selected="selected"' : ''));
		  $tmpl->parse($Marker_16, 'Marker_16', $j);
		  $j++;
		  }

		  $tmpl->parse($Marker_11, 'Marker_11', true);
		  }
		 */// </editor-fold>

		$tmpl->parse($tmpl->handler . '_out', $tmpl->handler);
		$output = $tmpl->get($tmpl->handler . '_out');

		return $this->core->atp_content($output);
	}

	/**
	 * Paieškas
	 * @author Justinas Malūkas
	 * @return string
	 */
	function search() {
		$tmpl = $this->core->Template();

		$tmpl_handler = 'searchfile';
		$tmpl->set_file($tmpl_handler, 'search.tpl');

		// Paieškos lauko reikšmė
		$field_input_block = $tmpl_handler . 'field_input';
		$tmpl->set_block($tmpl_handler, 'field_input', $field_input_block);

		// Paieškos lauko checkbox'as
		$field_checkbox_block = $tmpl_handler . 'field_checkbox';
		$tmpl->set_block($tmpl_handler, 'field_checkbox', $field_checkbox_block);
		//$tmpl->parse($field_checkbox_block, 'field_checkbox');
		// Paieškos lauko blokas
		$field_block = $tmpl_handler . 'field';
		$tmpl->set_block($tmpl_handler, 'field', $field_block);

		// Paieškos forma
		$form_block = $tmpl_handler . 'form';
		$tmpl->set_block($tmpl_handler, 'form', $form_block);

		// Paieškos rezultatų sąrašas
		$list_block = $tmpl_handler . 'list';
		$tmpl->set_block($tmpl_handler, 'list', $list_block);

		// Paieškos rezultatų sąrašas
		$reports_block = $tmpl_handler . 'reports';
		$tmpl->set_block($tmpl_handler, 'reports', $reports_block);

		$m = $this->core->extractVariable('m');

		$vars['GLOBAL_SITE_URL'] = GLOBAL_SITE_URL;
		$vars['sys_message'] = $this->core->get_messages();

		$fdata = &$_REQUEST;

		if ($_GET['type'] === 'advanced' && isset($fdata['field']) === FALSE) {
			$vars['reports'] = $vars['list'] = '';
			$vars['title'] = 'Išplėstinė dokumentų paieška';
			$vars['action'] = 'index.php?m=' . $m . '&type=advanced';


			//require_once($this->core->config['REAL_URL'] . 'helper/slug.php');
			$slug = new Slug();


			// Sukurti galiojantys paieškos laukai
			$fields = $this->logic2->get_search_field(array('VALID' => 1), false, null, 'ID, NAME');


			// Spauzdinami paieškos laukai
			foreach ($fields as $field) {
				$name = 'field-' . $slug->makeSlugs($field['NAME']);
				// Lauko reikšmės įvedimas
				$tmpl->set_var(array(
					'class' => '', //$name,
					'type' => 'text',
					'name' => 'field[' . $field['ID'] . ']',
					'value' => '',
					'extra' => '',
					$field_checkbox_block => ''
				));
				$tmpl->parse($field_input_block, 'field_input');
				// Lauko pavadinimas
				$tmpl->set_var(array(
					'class' => $name,
					'label' => $field['NAME']
				));
				$tmpl->parse($field_block, 'field', true);
			}


			// Saugojimo kaip "Ataskaita" laukai
			$name = 'field-as-report';
			// Lauko pasirinkimas
			//$tmpl->set_var(array(
			//	'name' => 'field[as_report]',
			//	'checked' => ''
			//));
			$tmpl->parse($field_checkbox_block, 'field_checkbox');
			// Lauko reikšmės įvedimas
			$tmpl->set_var(array(
				'class' => '', //$name,
				'type' => 'text',
				'name' => 'field[as_report]',
				'value' => '',
				'extra' => 'disabled="disabled"'
			));
			$tmpl->parse($field_input_block, 'field_input');
			// Lauko pavadinimas
			$tmpl->set_var(array(
				'class' => $name,
				'label' => 'Išsaugoti kaip ataskaitą'
			));
			$tmpl->parse($field_block, 'field', true);

			$tmpl->parse($form_block, 'form');


			$tmpl->set_var($vars);
		} elseif ($_GET['type'] === 'reports') {
			$vars['form'] = $vars['list'] = '';
			$vars['title'] = 'Ataskaitos';
			$tmpl->set_var('table_reports_list', $this->table_reports_list());
			$tmpl->parse($reports_block, 'reports');

			$tmpl->set_var($vars);
		} elseif (empty($fdata['field']) === FALSE) {
			$vars['form'] = $vars['reports'] = '';
			$vars['title'] = 'Dokumentų paieška';

			$data = $this->action->search_fields($fdata, 'search');
			$tmpl->set_var('table_search_list', $this->table_search_list($data));
			$tmpl->parse($list_block, 'list');

			$tmpl->set_var($vars);
		} else {
			header('Location: index.php?m=18&type=advanced');
			exit;
		}

		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');

		return $this->core->atp_content($output);
	}

	/**
	 * Paieškos laukai
	 * @author Justinas Malūkas
	 * @return string
	 */
	function search_fields() {
		$tmpl = $this->core->Template();

		$tmpl_handler = 'search_fields_file';
		$tmpl->set_file($tmpl_handler, 'search_fields.tpl');

		// Naujo peiškos lauko kūrimo mygtukas
		$new_block = $tmpl_handler . 'new';
		$tmpl->set_block($tmpl_handler, 'new', $new_block);
		$tmpl->parse($new_block, 'new');

		$vars['fields_table'] = $this->table_fields();
		$vars['GLOBAL_SITE_URL'] = GLOBAL_SITE_URL;
		$vars['sys_message'] = $this->core->get_messages();
		$vars['save'] = 'Išsaugoti';

		$tmpl->set_var($vars);

		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');

		return $this->core->atp_content($output);
	}

	/**
	 * Paieškos lauko redagavimo forma
	 * @author Justinas Malūkas
	 * @return string
	 */
	public function search_fields_editform() {
		$tmpl = $this->core->Template();
		/* 	if (!$this->core->user->status['A_TABLE']) {
		  $tmpl_handler = 'home_file';
		  $tmpl->set_file($tmpl_handler, 'home.tpl');
		  $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		  $output = $tmpl->get($tmpl_handler . '_out');
		  return $this->core->atp_content($output);
		  } */


		if ($this->core->content_type !== 2) {
			$this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
			$this->core->_load_files['js'][] = 'jquery.validationEngine.js';
			$this->core->_load_files['css'][] = 'validationEngine.jquery.css';
		}

		$tmpl_handler = 'search_field_editform_file';
		$tmpl->set_file($tmpl_handler, 'search_field_editform.tpl');

		// Papildomo lauko blokas
		$form_field_block = $tmpl_handler . 'form_field';
		$tmpl->set_block($tmpl_handler, 'form_field', $form_field_block);

		// Naujo lauko saltinio tipas
		$field_source_type_option_block = $tmpl_handler . 'field_source_type_option';
		$tmpl->set_block($tmpl_handler, 'field_source_type_option', $field_source_type_option_block);
		// Naujo lauko prijungimas
		$form_field_new_block = $tmpl_handler . 'form_field_new';
		$tmpl->set_block($tmpl_handler, 'form_field_new', $form_field_new_block);

		// Lauko tipo pasirinkimai
		$field_type_option_block = $tmpl_handler . 'field_type_option';
		$tmpl->set_block($tmpl_handler, 'field_type_option', $field_type_option_block);

		// Formos blokas
		$form_block = $tmpl_handler . 'form';
		$tmpl->set_block($tmpl_handler, 'form', $form_block);


		$vars = array();
		$m = (int) $this->core->extractVariable('m');
		$id = (int) $this->core->extractVariable('id');
		//$parent_id = (int) $this->core->extractVariable('parent');


		$vars['sys_message'] = $this->core->get_messages();
		$vars['save'] = 'Išsaugoti';


		// Surenka dumenis apie paieškos lauką
		$info = array();
		if (empty($id) === FALSE) {
			$info = $this->logic2->get_search_field(array('ID' => $id));
		}

		if (empty($info) === TRUE) {
			$vars['title'] = 'Naujas paieškos laukas';
			$vars['url'] = 'index.php?m=' . $m . '&action=save';
			$vars['name'] = '';
		} else {
			$vars['title'] = 'Laukas: ' . $info['NAME'];
			$vars['url'] = 'index.php?m=' . $m . '&id=' . $id . '&action=save';
			$vars['name'] = htmlspecialchars($info['NAME']);

			// Priskirti laukai
			$field_fields = $this->logic2->get_search_field_form_fields(array('SEARCH_FIELD_ID' => $info['ID']), false, null, 'ID, FIELD_ID');
			$ids_arr = array();
			foreach ($field_fields as &$arr) {
				$ids_arr[] = $arr['FIELD_ID'];
			}
			$fields = array();
			if (count($ids_arr))
				$fields = $this->logic2->get_fields(array(0 => 'ID IN(' . join(',', $ids_arr) . ')'));
			unset($field_fields, $ids_arr);

			// Priskirti informaciniai laukai
			$ifields = array();
			$field_fields = $this->logic2->get_search_field_form_ifields(array('SEARCH_FIELD_ID' => $info['ID']), false, null);
			$act_info = $this->logic->get_info_fields(array('TYPE' => 'ACT'));
			$clause_info = $this->logic->get_info_fields(array('TYPE' => 'CLAUSE'));
			foreach ($field_fields as $ifield) {
				if ($ifield['ITYPE'] === 'ACT') {
					$itype = 'IACT';
					$name = 'AICOL_';
					$label = $act_info['header'][$ifield['FIELD_ID']];
				} else if ($ifield['ITYPE'] === 'CLAUSE') {
					$itype = 'ICLAUSE';
					$name = 'SICOL_';
					$label = $clause_info['header'][$ifield['FIELD_ID']];
				}
				$ifields[] = array('ID' => $ifield['FIELD_ID'], 'ITYPE' => $itype, 'NAME' => $name . $ifield['FIELD_ID'], 'LABEL' => $label, 'TYPE' => '7');
			}
			unset($field_fields);
		}


		/*
		 *  Paieškos lauko tipo pasirinkimas
		 */
		$field_types = $this->logic->get_field_type();
		$allowed_types = array(8, 12, 13, 14);

		$tmpl->set_var(array(
			'value' => 0,
			'label' => $this->lng('not_chosen'),
			'selected' => ''
		));
		$tmpl->parse($field_type_option_block, 'field_type_option');

		foreach ($field_types as &$val) {
			if (in_array($val['ID'], $allowed_types) === FALSE)
				continue;

			$tmpl->set_var(array(
				'value' => $val['ID'],
				'label' => $val['LABEL'],
				'selected' => ((int) $val['ID'] === (int) $info['TYPE'] ? ' selected="selected"' : '')
			));
			$tmpl->parse($field_type_option_block, 'field_type_option', true);
		}


		/*
		 * Priskirto lauko šaltinio tipo pasirinkimas
		 */
		$source_types = array(
			0 => $this->lng('not_chosen'),
			'DOC' => 'Dokumentas',
			'CLASS' => 'Klasifikatorius',
			'CLAUSE' => 'Straipsnis',
			'ICLAUSE' => 'Straipsnis',
			'ACT' => 'Tesiės akto punktas',
			'IACT' => 'Tesiės akto punktas',
			'WS' => 'Web-servisas'
		);
		foreach ($source_types as $val => &$label) {
			if (in_array($val, array('WS', 'IACT', 'ICLAUSE'), true)) {
				continue;
			}
			$tmpl->set_var(array(
				'value' => $val,
				'label' => $label
			));
			$tmpl->parse($field_source_type_option_block, 'field_source_type_option', true);
		}
		$tmpl->parse($form_field_new_block, 'form_field_new');


		// Dokumentų, klasifikatorių, webservisų informacija
		$items_names['DOC'] = $this->logic2->get_document(null, false, null, 'ID, LABEL, PARENT_ID');
		$items_names['CLASS'] = $this->logic2->get_classificator(null, false, null, 'ID, LABEL, PARENT_ID');
		$ws = $this->logic2->get_ws();

		/*
		 * Priskirtų laukų išvedimas
		 */
		$tmpl->set_var(array(
			'class' => ' sample',
			'field_id' => '',
			'field_source_type' => '',
			'field_source' => '',
			'field_name' => ''
		));
		$tmpl->parse($form_field_block, 'form_field');
		if (empty($fields) === FALSE || empty($ifields) === FALSE) {
			foreach (array(0 => $fields, 'I' => $ifields) as $key => $fields) {
				foreach ($fields as &$field) {

					// Pagal dokumento tipą nustatomas šaltinio pavadinimas
					$item_name = array();
					switch ($field['ITYPE']) {
						case 'DOC':
						case 'CLASS':
							$item_name[] = $items_names[$field['ITYPE']][$field['ITEM_ID']]['LABEL'];
							break;
						case 'WS':
							$parent_field_info = $this->logic2->get_fields(array('ID' => $field['ITEM_ID']));
							switch ($parent_field_info['ITYPE']) {
								case 'DOC':
									$item_info = $this->logic2->get_document(array('ID' => $parent_field_info['ITEM_ID']), true, null, 'ID, LABEL');
									$item_name[] = $source_types['DOC'] . ': ' . $item_info['LABEL'];
									break;
								case 'CLASS':
									$item_info = $this->logic2->get_classificator(array('ID' => $parent_field_info['ITEM_ID']), true, null, 'ID, LABEL');
									$item_name[] = $source_types['CLASS'] . ': ' . $item_info['LABEL'];
									break;
								default:
									$item_name[] = 'Unknown: ???';
							}
							$item_name[] = $parent_field_info['LABEL'];
							break;
						default:
							$item_name[] = '-------';
					}

					$type = '';
					if ($key === 'I') {
						$type = substr($field['NAME'], 0, 2);
					}

					$tmpl->set_var(array(
						'class' => '',
						'field_id' => $type . $field['ID'],
						'field_source_type' => $source_types[$field['ITYPE']],
						'field_source' => join(' > ', $item_name),
						'field_name' => $field['LABEL']
					));
					$tmpl->parse($form_field_block, 'form_field', true);
				}
			}
		}

		/*
		 * Galimų laukų pasirinkimų hierarchija
		 */
		$fields_data = array();
		// Surenka dokumentų ir klasifikatorių laukus
		$tfields = $this->logic2->get_fields(array(0 => 'ITYPE IN ("DOC", "CLASS")'), false, null, 'ID, LABEL, ITYPE, ITEM_ID, WS');

		// Straipsnių laukai
		$clause_info = $this->logic->get_info_fields(array('TYPE' => 'CLAUSE'));
		$ifields = array();
		foreach ($clause_info['header'] as $key => $arr) {
			$ifields[] = array('ID' => 'SI' . $key, 'ITYPE' => 'ICLAUSE', 'NAME' => 'SICOL_' . $key, 'LABEL' => $arr, 'TYPE' => '7');
		}
		$tfields2['CLAUSE'] = array_merge($ifields, $this->logic2->get_fields(array('ITYPE' => 'CLAUSE'), false, null, 'ID, LABEL, ITYPE, ITEM_ID, WS'));
		foreach ($tfields2['CLAUSE'] as $f) {
			$fields_data['CLAUSE']['FIELDS'][$f['ID']]['NAME'] = $f['LABEL'];
		}

		// Teisės aktų punktų laukai
		$act_info = $this->logic->get_info_fields(array('TYPE' => 'ACT'));
		$ifields = array();
		foreach ($act_info['header'] as $key => $arr) {
			$ifields[] = array('ID' => 'AI' . $key, 'ITYPE' => 'IACT', 'NAME' => 'AICOL_' . $key, 'LABEL' => $arr, 'TYPE' => '7');
		}
		$tfields2['ACT'] = array_merge($ifields, $this->logic2->get_fields(array('ITYPE' => 'ACT'), false, null, 'ID, LABEL, ITYPE, ITEM_ID, WS'));
		foreach ($tfields2['ACT'] as $f) {
			$fields_data['ACT']['FIELDS'][$f['ID']]['NAME'] = $f['LABEL'];
		}

		foreach ($tfields as $tfid => &$tfield) {
			$tfid = $tfield['ID'];
			// Šaltinio informacija
			$tfield_info = $items_names[$tfield['ITYPE']][$tfield['ITEM_ID']];
			// Šaltinio pavadinimas
			$tfield_name = $tfield_info['LABEL'];

			if (empty($tfield_name))
				continue;

			$ref = &$fields_data[$tfield['ITYPE']];

			// Surenka šaltinio tėvinius šaltinius
			$parents = array();
			if (empty($tfield_info['PARENT_ID']) === FALSE) {
				$fff = $tfield_info;
				$i = 0;
				while (empty($fff['PARENT_ID']) === FALSE) {
					$i++;
					if ($i > 50) {
						trigger_error('To many loops', E_USER_WARNING);
						break;
					}
					$parents[] = $fff['PARENT_ID'];
					$fff = $items_names[$tfield['ITYPE']][$fff['PARENT_ID']];
				}
			}
			// Į masyvą hierarchiškai sudeda rėvinius šaltinius
			$parents = array_reverse($parents);
			foreach ($parents as $parent) {
				$ref['GROUP'][$parent]['NAME'] = $items_names[$tfield['ITYPE']][$parent]['LABEL'];
				$ref = &$ref['GROUP'][$parent];
			}

			$g_ref = &$ref['GROUP'][$tfield['ITEM_ID']];
			// Pagrindinio šaltinio pavadinimas
			$g_ref['NAME'] = $tfield_name;
			// Šaltinio laukai
			$g_ref['FIELDS'][$tfid]['NAME'] = $tfield['LABEL'];

			// Surenka web-serviso lauko laukus
			if ($tfield['WS'] === '1') {
				$g_ref['FIELDS'][$tfid]['WS'] = array();
				// Web-serviso laukui sukurti laukai
				$ws_fields = $this->logic2->get_webservices(array('PARENT_FIELD_ID' => $tfid));

				foreach ($ws_fields as $id => &$wsfield) {
					$wsfinfo = $this->logic2->get_fields(array('ID' => $wsfield['FIELD_ID']), true, null, 'ID, LABEL');
					$wsinfo = $ws->get_info($wsfield['WS_NAME']);
					$g_ref['FIELDS'][$tfid]['WS'][$wsfield['WS_NAME']]['NAME'] = $wsinfo['label'];
					$g_ref['FIELDS'][$tfid]['WS'][$wsfield['WS_NAME']]['FIELDS'][$wsfinfo['ID']]['NAME'] = $wsfinfo['LABEL'];
				}
			}
		}

		$fields_data = json_encode($fields_data);
		$vars['fields_data'] = &$fields_data;

		$tmpl->set_var($vars);

		if ($this->core->user->status['A_TABLE'] > 1) {
			$tmpl->parse($form_block, 'form');
		} else {
			$tmpl->clean($form_block);
		}

		$tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
		$output = $tmpl->get($tmpl_handler . '_out');

		return $this->core->atp_content($output);
	}

	// TODO: Straipsnio laukams turi leisti pasirinkti staipsnius, kalasifikatorių - klasifikatorius, veikos - veikas ir pan.?
	/**
	 * Suformuoja formos lauką (koreguota view_column_structure() kopija. Daryta "Dokumnetų ryšiai" puslapiui, kai yra nurodoma reikšmę pagal nutylėjimą, pagal lauko tipą suformuojamas įvesties laukas)
	 * @param array $arr Parametrai
	 * @param int $arr['ID'] [optional] Formos lauko ID
	 * @param int $arr['FIELD'] [optional] Formos lauko informacija
	 * @param int $arr['NAME'] [optional] Nurodoma kaip suformuojamas formos lauko vardas
	 * @param int $arr['NAME_TMPL'] [optional] Nurodoma kaip suformuojamas formos lauko vardas
	 * @param int $arr['VALUE'] [optional] Formos lauko reikšmė
	 * @param array $param TODO: kad atitiktu $this->manage->record_field(&$field, &$param)
	 */
	function get_field_html($arr, &$param = null) {
		// Vienas parametras privalomas
		if (isset($arr['ID']) === FALSE && isset($arr['FIELD']) === FALSE) {
			trigger_error('Field is not set', E_USER_WARNING);
			return false;
		}

		// Gauna lauko informaciją
		if (empty($arr['FIELD']) === FALSE) {
			$field = &$arr['FIELD'];
		} else if (empty($arr['ID']) === FALSE) {
			$field = $this->logic2->get_fields(array('ID' => $arr['ID']));
		}
		if (empty($field)) {
			trigger_error('Field not found', E_USER_WARNING);
			return false;
		}


		if (isset($arr['VALUE']) === TRUE) {
			$value = &$arr['VALUE'];
		} else {
			$record_id = $param['record_id'];
			$table = $param['table'];
			$table_record = $param['record'];
			$value = isset($param['record'][$field['NAME']]) ? $param['record'][$field['NAME']] : '';
		}

		$field['LABEL'] = '';
//-----ČIA BAIGIAU
		// Lauko "name" atributo reikšmė
		switch ($arr['NAME']) {
			case 'PREFIX';
				$index = $param['prefix'] = $param['prefix'] . 'F' . end(explode('_', $field['NAME']));
				break;
			case 'ID':
			default:
				$index = $field['ID'];
		}
		if (strpos((string) $arr['NAME_TMPL'], '{$name}') === FALSE) {
			$arr['NAME_TMPL'] = 'field[{$name}]';
		}
		$name = str_replace('{$name}', $index, $arr['NAME_TMPL']);

		// Šablono laukai
		$tpl = array(
			'filename' => '', // Lauko šablonas
			'title' => $field['LABEL'], // Lauko pavadinimas
			'id' => '', // [id] atributas
			'rows' => '', // [rows] atributas
			'type' => 'text', // input[type] atributas
			'class' => '', // [class] atributas
			'onkeypress' => '', // [onkeypress] atributas
			'max_length' => '', // [maxlength] atributas
			'note_class' => '', // Failui
			'note_text' => '', // Failui
			//'name' => strtolower($field['NAME']), // [name] atributas
			'name' => strtoupper($name), // [name] atributas
			'value' => $value, // Lauko reikšmė
			'disabled' => '', // [disabled] atributas
			'readonly' => '',
			'children' => '',
			'unallowed' => false // [unallowed] atributas
		);

		$tpl_html_data = array(); // Papildomi šablono kintamieji
		$result = null; // Galutinis HTML'as

		/*
		 * Lauko informacija pagal rūšį ir tipą
		 */
		// Skaičius
		if ($field['SORT'] == 1 && $field['TYPE'] != 6) {
			$tpl['filename'] = 'input';

			$tpl['type'] = 'text';
			$tpl['onkeypress'] = ' onkeypress="return isNumberKey(event)"';

			$max_length = array(1 => 3, 2 => 5, 3 => 7, 4 => 11, 5 => 19);
		} else
		// Skaičius su kableliu
		if ($field['SORT'] == 1 && $field['TYPE'] == 6) {
			$tpl['filename'] = 'input';

			$tpl['type'] = 'text';
			$tpl['onkeypress'] = ' onkeypress="return isFloatNumberKey(event)"';
			$tpl['value'] = empty($value) === FALSE ? $value : '0.00';
		} else
		// Tekstinis tipas
		if ($field['SORT'] == 2) {
			// Smulkus tekstas
			if ($field['TYPE'] == 7)
				$tpl['filename'] = 'input';
			else
				$tpl['filename'] = 'textarea';

			$rows = array(8 => 2, 9 => 4, 10 => 6);
			$max_length = array(7 => 256, 8 => 65000, 9 => 16000000, 10 => 4000000000);
		} else
		// Veika
		if ($field['SORT'] == 4 && $field['TYPE'] === '23') {
			$tpl['filename'] = 'select';

			$data = $this->logic->get_deed_search_values();
			if (empty($field['DEFAULT']) === FALSE && empty($field['DEFAULT_ID']) === FALSE && empty($value) === TRUE)
				$value = $field['DEFAULT_ID'];

			$str_val = 0;
			foreach ($data as $arr) {
				$selected = '';
				if ($value == $arr['ID']) {
					$selected = ' selected="selected"';
					$str_val = $arr['VALUE'];
				}
				$tpl_html_data[] = array('value' => $arr['VALUE'], 'selected' => $selected, 'label' => $arr['LABEL']);
			}
			$value = $str_val;
		} else
		// Klasifikatorius
		if ($field['SORT'] == 4) {
			$tpl['filename'] = 'select';

			$classifications = $this->logic2->get_classificator(array('PARENT_ID' => $field['SOURCE']));
			if (empty($field['DEFAULT']) === FALSE && empty($field['DEFAULT_ID']) === FALSE && empty($value) === TRUE)
				$value = $field['DEFAULT_ID'];

			$str_val = 0;
			foreach ($classifications as $classification) {
				$selected = '';
				if ($value == $classification['ID']) {
					$selected = ' selected="selected"';
					$str_val = $classification['ID'];
				}
				$tpl_html_data[] = array('value' => $classification['ID'], 'selected' => $selected, 'label' => $classification['LABEL']);
			}
			$value = $str_val;
		} else
		// Checkbox ir radio
		if ($field['SORT'] == 5 || $field['SORT'] == 8) {
			$tpl['filename'] = 'pick';

			$pk_type = '';
			if ($field['SORT'] == 5) {
				$pk_type = 'checkbox';
				$tpl['name'] = $tpl['name'] . '[]';
				$value = json_decode(stripslashes($value));
			}

			if ($field['SORT'] == 8) {
				$pk_type = 'radio';
				$value = array($value);
			}
			$classifications = $this->logic2->get_classificator(array('PARENT_ID' => $field['SOURCE']));
			foreach ($classifications as $classification) {
				$pk_checked = '';
				//if (in_array($classification['LABEL'], $value)) {
				if (in_array($classification['ID'], $value)) {
					$pk_checked = ' checked="checked"';
					$value = (int) $classification['ID'];
				}

				$tpl_html_data[] = array(
					'pk_type' => $pk_type,
					'pk_id' => $classification['ID'],
					'pk_label' => $classification['LABEL'],
					'pk_class' => '',
					'pk_name' => $tpl['name'],
					'pk_checked' => $pk_checked,
					'pk_value' => $classification['ID']);
			}
		} else
		// Straipsnis
		if ($field['SORT'] == 6) {
			$tpl['filename'] = 'select';

			// Vartotojo straipsniai
			$user_clauses = $this->logic->get_user_clauses(array('PEOPLE_ID' => $param['user']['ID'], 'STRUCTURE_ID' => $param['user']['STRUCTURE_ID']));

			// Reikšmė pagal nutylėjimą
			if (empty($field['DEFAULT']) === FALSE && empty($field['DEFAULT_ID']) === FALSE && empty($value) === TRUE)
				$value = $field['DEFAULT_ID'];

			//$str_val = 0;
			foreach ($user_clauses as $clause) {
				$selected = '';
				if ($value == $clause['ID']) {
					$selected = ' selected="selected"';
					//$str_val = $clause['ID'];
					$value = (int) $clause['ID'];
				}
				$tpl_html_data[] = array(
					'value' => $clause['ID'],
					'selected' => $selected,
					'label' => $clause['NAME']);
			}
			//$value = (int) $str_val;
		} else
		// Datos ir laiko laukai
		if ($field['SORT'] == 7) {
			$tpl['filename'] = 'input';

			$is_empty = false;
			if (empty($value) === TRUE)
				$is_empty = true;

			$value = strtotime($value);
			$max_length = array(11 => 4, 12 => 10, 13 => 8, 14 => 19);

			// Metai
			if ($field['TYPE'] == 11) {
				$tpl['class'] .= ' atp-date-year';
				$value = date('Y', $value);
			} else
			// Data
			if ($field['TYPE'] == 12) {
				$tpl['class'] .= ' atp-date';
				$value = date('Y-m-d', $value);
			} else
			// Laikas
			if ($field['TYPE'] == 13) {
				$tpl['class'] .= ' atp-time';
				$value = date('H:i', $value);
			} else
			// Data ir laikas
			if ($field['TYPE'] == 14) {
				$tpl['class'] .= ' atp-datetime';
				$value = date('Y-m-d H:i', $value);
			}

			if ($is_empty === TRUE)
				$value = '';

			$tpl['value'] = $value;
		} else
		// Antraštė
		if ($field['SORT'] == 9) {
			$tpl['filename'] = 'text';
			$tpl['title'] = $tpl['title'];
			$tpl['value'] = $field['TEXT'];
		} else
		// Failas
		if ($field['SORT'] == 10) {
			$tpl['filename'] = 'input';

			$tpl['type'] = 'file';
			$tpl['name'] = $tpl['name'] . '[]';
			$tpl['class'] = '-file' . $tpl['class'];

			$file_params = $this->logic->get_file_params(array(
				'TABLE_ID' => $field['ITEM_ID'],
				'STRUCTURE_ID' => $field['ID'],
				'LIMIT' => 1));
			if (!empty($file_params)) {
				$file_params = array_shift($file_params);

				$tpl['note_class'] = ' atp-note-show';
				$tpl['note_text'] = 'Failų kiekis: <span class="atp-file-cnt">' . $file_params['CNT'] . '</span>, dydis: ' . $file_params['SIZE_FROM'] . ' - ' . $file_params['SIZE_TO'] . ' Mb';
			}
			/*
			  // Dokumento įrašo lauko ir failų ryšiai
			  $files = $this->logic2->get_record_files(array(
			  'TABLE_ID' => $table['ID'],
			  'RECORD_ID' => $table_record['ID'],
			  'FIELD_ID' => $structure['ID']));

			  // TODO: atskirti failo išvedimą pagal failo tipą
			  $file_list = '';
			  if (empty($files) === FALSE) {
			  foreach ($files as &$file) {
			  // Failo informacija
			  $file_info = $this->logic2->get_files(array('ID' => $file['FILE_ID']));

			  if (// strpos($file_info['TYPE'], 'image') === 0 &&
			  $structure['TYPE'] === '20') {
			  // Sugeneruoja thumbnail'ą
			  $path = 'files/tmp/';
			  $thumb_name = $this->action->create_thumbnail($this->core->config['REAL_URL'] . $file_info['PATH'], $this->core->config['REAL_URL'] . $path);
			  $thumb_path = $this->core->config['SITE_URL'] . $path . $thumb_name;
			  // Failo išvedimas
			  $file_list .= '<li for="' . $file['ID'] . '">'
			  . '<a href="' . $this->core->config['SITE_URL'] . $file_info['PATH'] . '" target="_blank">'
			  . '<img class="photo" style="background: url(' . $thumb_path . ') center center no-repeat;"/>'
			  . $file_info['NAME']
			  . '</a>'
			  . '</li>';
			  } else if ($structure['TYPE'] === '21') {
			  $file_list .= '<li for="' . $file['ID'] . '">'
			  . '<a href="' . $this->core->config['SITE_URL'] . $file_info['PATH'] . '" target="_blank">'
			  . $file_info['NAME']
			  . '</a>'
			  . '</li>';
			  }
			  }
			  }

			  if (empty($file_list) === FALSE) {
			  if ($structure['TYPE'] === '20') {
			  $file_list = '<div class="file-list"><ul id="photos_b" class="photo-list fl">' . $file_list . '</ul></div>';
			  } else if ($structure['TYPE'] === '21') {
			  $file_list = '<div class="file-list"><ul class="file-list">' . $file_list . '</ul></div>';
			  }
			  }
			  $template_data['file_list'] = &$file_list;
			 */
		}

		// Šablono generavimas
		if (!empty($tpl['filename'])) {
			$tmpl = $this->core->getTemplate('template2_' . $tpl['filename']);

			// [row] atributo formavimas
			if (!empty($rows[$field['TYPE']]))
				$tpl['rows'] = ' rows="' . $rows[$field['TYPE']] . '"';
			// [maxlength] atributo formavimas
			if (!empty($max_length[$field['TYPE']]))
				$tpl['max_length'] = ' maxlength="' . $max_length[$field['TYPE']] . '"';
			// [unallowed] atributo formavimas
			if ($tpl['unallowed'] === TRUE)
				$tpl['disabled'] .= ' unallowed="unallowed"';

			// Jei ne veika/klasifikatorius, checkbox, straipsnis, radio button
			if (in_array($field['SORT'], array(4, 5, 6, 8)) === FALSE) {
				if (isset($tpl['value']) === TRUE && empty($tpl['value']) === TRUE && empty($field['DEFAULT']) === FALSE)
					$tpl['value'] = $field['DEFAULT'];
				$result = $this->core->returnHTML($tpl, $tmpl[0]);
			} else {
				$tpl['lng_not_chosen'] = $this->lng('not_chosen');
				$result = $this->core->returnHTML($tpl, $tmpl[0]);
				foreach ($tpl_html_data as $template_dt) {
					foreach ($template_dt as $val => $dt)
						$tpl[$val] = $dt;

					$result .= $this->core->returnHTML($tpl, $tmpl[1]);
				}
				$result .= $this->core->returnHTML($tpl, $tmpl[2]);
			}
		}

		return $result;
	}

	public function register_case() {
		$record_id = $_GET['id'];
		$template_id = $_GET['template_id'];

		$record = new ATP_Document_Record($record_id, $this->core);
		$template = new ATP_Document_Templates_Template($this->core, $template_id);
		$register = new ATP_Document_Record_Register($record, $template, $this->core);
		//$register->mark_record_as_registered = false;
		$register->set_registration_fields = false;
		$result = $register->register();

		if ($result === TRUE) {
			// TODO: Perduoti dokumentą į nutarimą, užsideda būseana "Užvesta byla"
			// Dokumentas perduodamas į nutarimą
			$manager = new ATP_Document_Record_Manage($record, $this->core);
			$manager->transferTo(57);

			$new_record = new ATP_Document_Record($manager->result['saved_record_id'], $this->core);

			$result = $register->set_registration_fields($new_record);

			$this->core->set_messages($manager->result['messages']);
			header('Location: ' . $manager->result['redirect']);
		} else {
			die('Klaida registruojant dokumentą');
		}


		//$this->register_document();
	}

	public function register_document() {
		$record_id = $_GET['id'];
		$template_id = $_GET['template_id'];

		$record_obj = new ATP_Document_Record($record_id, $this->core);
		$template_obj = new ATP_Document_Templates_Template($this->core, $template_id);
		$register_obj = new ATP_Document_Record_Register($record_obj, $template_obj, $this->core);

		$result = $register_obj->register();
		if ($result === TRUE) {
			$this->core->set_message('success', 'Dokumentas užregistruotas. Dokumento numers: ' . $register_obj->document_number);
		} else {
			$this->core->set_message('error', 'Nepavyko užregistruoti dokumento');
			if ($_COOKIE['I'] === 'dev') {
				var_dump($result->errors);
				die('end');
			}
		}

		header('Location: index.php?m=5&id=' . $record_obj->record['ID']);
		exit;

		/* $record = $this->logic2->get_record_relation($record_id);
		  $result = $this->action->WS_register_document($record, $template_id);
		  //$result = '<id>186454</id><registrationNo>AJKsfiA8963</registrationNo>';

		  if (preg_match('#<errorCode>([^<]+)</errorCode>#i', $result, $m)) {
		  $error_code = $m[1];
		  preg_match('#<errorMessage>([^<]+)</errorMessage>#i', $result, $m);
		  $error_message = $m[1];

		  trigger_error('Klaida registruojant paslaugos dokumentą. errorCode: ' . $error_code . '; errorMessage: ' . $error_message, E_USER_WARNING);
		  $this->core->set_message('error', 'Klaida (' . $error_code . '): Nepavyko užregistruoti dokumento');
		  } else
		  if (preg_match('#<id>([^<]+)</id>#i', $result, $m)) {
		  $id = $m[1];
		  preg_match('#<registrationNo>([^<]+)</registrationNo>#i', $result, $m);
		  $nr = $m[1];


		  $document = $this->logic2->get_document(array('ID' => $record['TABLE_TO_ID']));
		  $record_table = $this->logic2->get_record_table($record['ID']);

		  if (empty($document) === FALSE) {
		  $fields = array(
		  'nr' => $document['AVILYS_NUMBER_FIELD'],
		  'date' => $document['AVILYS_DATE_FIELD']
		  );


		  $up_doc = array();
		  $up_extra = array();
		  foreach ($fields as $type => $field_id) {
		  if (empty($field_id))
		  continue;

		  $field = $this->logic2->get_fields(array('ID' => $field_id));

		  if ($type === 'nr') {
		  $value = $nr;
		  } else if ($type === 'date') {
		  $value = date('Y-m-d H:i:s');
		  }
		  if ($field['ITYPE'] === 'DOC') {
		  $up_doc[$field['NAME']] = $value;
		  } else {
		  $up_extra[$field_id] = $value;
		  }
		  }


		  $param = array(
		  '#TABLE_ID#' => $document['ID'],
		  'TABLE_ID' => $document['ID'],
		  'ID' => $record_table['ID']);
		  if (empty($up_doc) === FALSE) {
		  foreach ($up_doc as $field_name => $val) {
		  $qry = 'UPDATE `' . PREFIX . 'ATP_TBL_:TABLE_ID`
		  SET
		  `:FIELD_NAME` = :VALUE
		  WHERE
		  ID = :ID';
		  $param['#FIELD_NAME#'] = $field_name;
		  $param['VALUE'] = $val;
		  $this->core->db_query_fast($qry, $param);
		  }
		  }
		  // TODO: [DONE] $record_id turi buti ne įrašo numeris, o įrašo ID dokumento lentelėje
		  if (empty($up_extra) === FALSE) {
		  //trigger_error('Wrong record ID', E_USER_ERROR);

		  foreach ($up_doc as $field_id => $val) {
		  $qry = 'REPLACE INTO `' . PREFIX . 'ATP_FIELDS_VALUES` (`FIELD_ID`, `TABLE_ID`, `RECORD_ID`, `VALUE`) VALUES
		  (:FIELD_ID, :TABLE_ID, :ID, :VALUE)';
		  $param['FIELD_ID'] = $field_id;
		  $param['VALUE'] = $val;
		  $this->core->db_query_fast($qry, $param);
		  }
		  }


		  // Pažymi, kad dokumento įrašas buvo užregistruotas avilyje
		  $qry = 'UPDATE `' . PREFIX . 'ATP_RECORD_RELATIONS`
		  SET
		  `REGISTERED_TEMPLATE_ID` = :TEMPLATE_ID
		  WHERE
		  ID = :ID AND
		  IS_LAST = 1';
		  $this->core->db_query_fast($qry, array(
		  'ID' => $record['ID'],
		  'TEMPLATE_ID' => $template_id));

		  // Atnaujina dokumento įrašo būseną
		  $this->logic->update_record_status($record['ID']);
		  }
		  #
		  # Užregistravo dokumentą. Dokumento numers: ' . $nr
		  #
		  //$sp[] = '<br/>Užregistravo dokumentą. Dokumento numers: ' . $nr;
		  //$this->writeATPactionLog($sp);

		  $this->core->set_message('success', 'Dokumentas užregistruotas. Dokumento numers: ' . $nr);
		  } else {
		  $this->core->set_message('error', 'Nepavyko užregistruoti dokumento');
		  }

		  header('Location: index.php?m=5&id=' . $record['ID']);
		  exit; */
	}

	public function writeATPactionLog(array $sp) {
		if (empty($_SESSION['wp_log_name']) === FALSE) {
			$employer_arr['short'] = $_SESSION['wp_log_name'];
		} else {
			$employer_arr = $this->core->getPEopleData($this->core->user->id);
		}

		// Webartnerio logas
		$wp_log = new WP_Log();
		$wp_log->writeAction(join('', $sp), $employer_arr['short'], 'ATP');
	}

	/**
	 * @deprecated nenaudojamas
	 * @return type
	 */
	function login() {
		if (!empty($_GET['user_id'])) {
			$_SESSION['user_id'] = $_GET['user_id'];
			$this->core->user->id = $_SESSION['user_id'];

			header('Location: ' . GLOBAL_SITE_URL . '/subsystems/atp/index.php');
			exit;
		}

		$tpl_arr = $this->core->getTemplate('login');

		$html_data['sys_message'] = $this->core->get_messages();
		$html_manage = $this->core->returnHTML($html_data, $tpl_arr[0]);
		
		$result = $this->core->db_query("SELECT B.ID, B.FIRST_NAME, B.LAST_NAME, C.SHOWS STRUCTURE_NAME
			FROM " . PREFIX . "MAIN_WORKERS A
			INNER JOIN ". PREFIX. "MAIN_PEOPLE B ON B.ID = A.PEOPLE_ID
			INNER JOIN ".PREFIX."MAIN_STRUCTURE C ON C.ID = A.STRUCTURE_ID");
			$user_privileges[$res['TABLE_ID']] = $res;
		$count = mysql_num_rows($result);
		for($i = 0; $i < $count; $i++) {
			$row = $this->core->db_next($result);
			$html_manage .= $this->core->returnHTML(array(
				'user_login' => 'index.php?m=99&user_id=' . $row['ID'],
				'user_surname' => $row['LAST_NAME'],
				'user_name' => $row['FIRST_NAME'],
				'user_department' => $row['STRUCTURE_NAME']
					), $tpl_arr[1]);
		}

		$html_manage .= $tpl_arr[2];

		return $this->core->atp_content($html_manage);
	}

}
