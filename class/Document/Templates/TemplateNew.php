<?php

namespace Atp\Document\Templates;

use \Atp\Document\Templates\Template\IHandler;

//use \Atp_Document_Templates_Template;
// TODO: naudoti kešavimą
/**
 * Dokumento šablonų valdymas
 */
class TemplateNew
{
    /**
     * Handler'ių (IHandler) talpykla
     * @var array
     */
    private $handlers = [];

    /**
     * Įjungto handler'io pavadinimas
     * @var string
     */
    private $current;

    /**
     * @var \Atp\Cache
     */
    private $cache;

    /**
     * @var \Atp\Core
     */
    private $atp;

    /**
     *
     * @param \Atp\Core $atp
     * @param IHandler $handler [optional]<p>Vykdytojo objektas. <b>Default:</b> NULL</p>
     */
    public function __construct(\Atp\Core $atp, IHandler $handler = null)
    {
        $this->atp = $atp;
        $this->cache = $this->atp->factory->Cache();

        $this->SetHandler($handler);
    }

    /**
     * Vykdytojo objekto klasės pavadinimas
     * @param IHandler $handler Vykdytojo objektas
     * @return string
     */
    private function get_handler_name(IHandler $handler)
    {
        return get_class($handler);
    }

    /**
     * Nustato naudojamą vykdytoją
     * @param string|IHandler $handler Vykdytojo klasės pavadinimas, jei vykdytojas jau yra pridėtas, arba vykdytojo objektas
     * @return boolean
     */
    public function SetHandler($handler)
    {
        if (empty($handler) === true) {
            return false;
        }

        if (is_object($handler) === true) {
            if ($handler instanceof IHandler === false) {
                return false;
            }

            $name = $this->get_handler_name($handler);
            $this->PushHandler($handler);
        } else {
            $name = $handler;
            $handler = $this->GetHandler($name);
            if ($handler === false) {
                return false;
            }
        }

        $this->current = $name;

        return true;
    }

    /**
     * Gauna pridėta vykdytojo objektą
     * @param string $name Vykdytojo clasės pavadinimas
     * @return boolean
     */
    public function GetHandler($name)
    {
        if (empty($name) === true || isset($this->handlers[$name]) === false) {
            return false;
        }

        return $this->handlers[$name];
    }

    /**
     * Nustatyto vykdytojo objektas
     * @return bool|IHandler
     */
    public function GetCurrentHandler()
    {
        return $this->GetHandler($this->current);
    }

    /**
     * Prideda vykdytoją
     * @param IHandler $handler Vykdytojo klasės objektas
     * @return boolean
     */
    public function PushHandler(IHandler $handler)
    {
        $name = $this->get_handler_name($handler);
        if (isset($this->handlers[$name]) === false) {
            $this->handlers[$name] = $handler;
        }

        return true;
    }

    /**
     * Šablono saugojimas.
     *
     * Po išsaugojimo setinamas $param->id (šablono id).
     *
     * @param \stdClass $param
     * @param int $param->document_id Dokumento ID
     * @param string $param->label Pavadinimas
     * @param \stdClass $handler_data Handler'iui paduodami duomenys
     * @return bool
     * @throws Exception
     */
    public function Save(\stdClass $param, \stdClass $handler_data)
    {
        $handler = $this->GetCurrentHandler();
        if ($handler === false) {
            throw new \Exception('Nenustatytas handler\'is');
        }

        // Sukuriamas DB įrašas
        $param->id = $this->atp->db_quickInsert(PREFIX . 'ATP_TABLE_TEMPLATE', array(
            'DOCUMENT_ID' => $param->document_id,
            'LABEL' => $param->label
        ));

        $handler_data->id = $param->id;
        $result = $handler->save($handler_data);

        if ($result === true) {
            $this->atp->db_quickupdate(
                PREFIX . 'ATP_TABLE_TEMPLATE',
                array('HANDLER' => $handler->getName()),
                $param->id
            );
        } else {
            $this->atp->db_query_fast('DELETE FROM ' . PREFIX . 'ATP_TABLE_TEMPLATE WHERE ID = :ID', array(
                'ID' => $param->id));
        }

        return $result;
    }

    /**
     * Šablono pašalinimas
     * @param int $id Šablono ID
     * @return bool
     * @throws Exception
     */
    public function Delete($id)
    {
        $handler = $this->GetCurrentHandler();
        if ($handler === false) {
            throw new \Exception('Nenustatytas handler\'is');
        }

        $result = $handler->delete($id);

        if ($result === true) {
            $param = array('ID' => $id);

            // Pašalinamas ryšys tarp skripto ir failo
            $this->atp->db_query_fast('DELETE FROM ' . PREFIX . 'ATP_TABLE_TEMPLATE WHERE ID = :ID', $param);

            // Tikrina ar ištrinta
            $result = $this->atp->db_query_fast('SELECT COUNT(ID) CNT
                FROM ' . PREFIX . 'ATP_TABLE_TEMPLATE
                WHERE ID = :ID', $param);
            $result = (bool) (int) $this->atp->db_next($result)['CNT'];
        }

        return $result;
    }

    /**
     * Gauna šablono informacija
     * @param array $params Parametrų masyvas
     * @return array Masyvas talpinantis parametrų masyvą
     */
    public function GetTemplates($param, $handler_data = false)
    {
        $rows = $this->atp->logic2->get_db(
            PREFIX . 'ATP_TABLE_TEMPLATE', $param, false, null, false, null, [], false
        );
        if (empty($rows)) {
            return [];
        }

        // Jei ieškoma pagal ID, tai gaunamas parametrų masyvas, nors ir paduodamas FALSE
        if (isset($rows[0]) === false) {
            $rows = [$rows];
        }

        if ($handler_data === true) {
            foreach ($rows as &$row) {
                $handler_name = 'Atp\Document\Templates\Template\\' . $row['HANDLER'];
                $result = $this->PushHandler(new $handler_name($this->atp));
                if ($result === true) {
                    $handler = $this->GetHandler($handler_name);
                    $row['HANDLER_DATA'] = $handler->GetData($row['ID']);
                }
            }
        }

        return $rows;
    }

    public function GenerateTemplate($record_id, $template_id)
    {
        $record = $this->atp->logic2->get_record_relation($record_id);
        if (empty($record) === true) {
            throw new \Exception('Nerastas dokumento įrašas "' . $record_id . '"');
        }        

        $template = $this->GetTemplates(array('ID' => $template_id))[0];
        $handler_name = 'Atp\Document\Templates\Template\\' . $template['HANDLER'];
		$result = $this->PushHandler(new $handler_name($this->atp));
        if ($result === false) {
            throw new \Exception('Nenustatytas handler\'is');
        }

        $handler = $this->GetHandler($handler_name);
		$file_path = $handler->GenerateTemplate($record_id, $template);
        
        $adminJournal = $this->atp->factory->AdminJournal();
        $adminJournal->addRecord(
            $adminJournal->getCreateRecord(
                'Dokumento įrašo failą iš šablono'
                . ' (Įrašas "' . $record['RECORD_ID'] . '";'
                . ' Šablonas "' . $template['LABEL'] . '").'
            )
        );
        

        return $file_path;
    }
}
