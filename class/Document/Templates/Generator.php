<?php

namespace Atp\Document\Templates;

/**
 * Šablono generavimas
 */
class Generator
{
    private $atp;

    private $file;

    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;
    }

    public function Make($record_id, $template_id)
    {
        $template = new TemplateNew($this->atp);
        $this->file = $template->GenerateTemplate($record_id, $template_id);
    }

    public function GetFilePath()
    {
        return $this->file;
    }

    /**
     * Pateikia sugeneruotą failą atsisiuntimui
     * @throws \Exception
     */
    public function Download()
    {
        if (is_file($this->file) === true) {
            $mime = (new \finfo(FILEINFO_MIME_TYPE))->file($this->file);

            header('Content-Description: File Transfer');
            //header('Content-Type: ' . $mime . '; charset=utf-8');
            header('Content-Type: application/octet-stream');
            //header('Content-Disposition: attachment; filename="' . pathinfo($this->file, PATHINFO_BASENAME) . '"');
            header('Content-Disposition: attachment; filename=' . basename($this->file));
            header('Expires: 0');
            //header('Pragma: no-cache');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($this->file));
            ob_clean();
            readfile($this->file);
            exit;
        } else {
            throw new \Exception('Generated file not found');
        }
    }
}
