<?php

//namespace Atp\Document\Templates;

/**
 * Gauti informacijai apie dokumento šabloną
 */
class Atp_Document_Templates_Template
{
    /**
     * @var \Atp\Core
     */
    private $atp;

    /**
     * Šablono informacija
     * @var object
     */
    public $template;

    /**
     * @var Atp\Document\Templates\TemplateNew
     */
    private $newTemplateManager;

    /**
     *
     * @param \Atp\Core $db
     * @param int $id [optional]
     */
    public function __construct(\Atp\Core &$atp, $id = null)
    {
        $this->atp = &$atp;
        $this->newTemplateManager = new \Atp\Document\Templates\TemplateNew($this->atp);
        if ($id !== null) {
            $this->set($id);
        }
    }

    /**
     * Gauna set'intą šabloną
     * @param int $id [optional] Šablono ID
     * @return object
     */
    public function Get($id = null)
    {
        if ($id !== null) {
            $this->set($id);
        }

        return $this->template;
    }

    /**
     * Set'ina šabloną
     * @param int $id Šablono ID
     * @return boolean
     */
    private function set($id)
    {
        $templates = $this->newTemplateManager->GetTemplates(array('ID' => $id), true);
        if (count($templates)) {
            $data = $templates[0];
            $data['FILE'] = $data['HANDLER_DATA']['FILE'];
            $this->template = (object) array(
                    'id' => $data['ID'],
                    'label' => $data['LABEL'],
                    'filename' => $data['FILE'],
                    'filepath' => 'subsystems/atp/files/templates/' . $data['FILE'],
                    'service_id' => $data['SERVICE_ID'],
                    'document_id' => $data['DOCUMENT_ID']);

            return true;
        }

        return false;
    }

    /**
     * Gauna šablonus, kurie atitinka šablono sąlygas
     * @param \Atp\Document\Record $recordManager Dokumento įrašo duomenys
     * @return array
     */
    public function GetAvailable(\Atp\Document\Record $recordManager)
    {
        $templates = $this->newTemplateManager->GetTemplates(array(
            'DOCUMENT_ID' => $recordManager->record['TABLE_TO_ID']
        ));

        // Pašalina dokumento šablonus, kurie neatitinka nustatytų sąlygų
        if (empty($templates) === false) {
            foreach ($templates as $key => $template) {
                $this->set($template['ID']);
                $available = $this->is_available_for($recordManager);
                if ($available === false) {
                    unset($templates[$key]);
                }
            }
        }

        return $templates;
    }

    /**
     * Patikrina ar dokumento įrašas tenkina set'into dokumento šabloną sąlygas
     * @param \Atp\Document\Record $recordManager Dokumento įrašo duomenys
     * @return boolen
     */
    private function is_available_for(\Atp\Document\Record &$recordManager)
    {
        if (empty($this->template)) {
            throw new \Exception('Template must be set by set(), get() or constructor');
        }

        $conditionManager = new \Atp_Document_Templates_Template_Condition(
            $this, $this->atp
        );
        $actionManager = new \Atp_Document_Templates_Template_Condition_Action(
            $this, $this->atp
        );

        $result = true;

        $conditions = $conditionManager->get_by_template();
        foreach ($conditions as &$condtition) {
            if (empty($condtition->action_id) === true) {
                continue;
            }

            $actionManager->set($condtition->action_id);

            $parameters = $conditionManager->get_parameters($condtition->id);
            $result = $actionManager->check($parameters, $recordManager);
            if ($result === false) {
                break;
            }
        }

        return $result;
    }
}
