<?php

namespace Atp\Document\Templates;

/**
 * HTML generavimas
 */
class Browse
{
    /**
     * @var \Atp\Core
     */
    private $atp;

    /**
     * Šablono objjektas
     * @var object
     */
    private $tmpl;

    /**
     * Duomenys apie reikalaujamą puslapį
     * @var \stdClass
     */
    private $browse;

    /**
     * @var $_FILES
     */
    private $files;

    /**
     * @var $_REQUEST
     */
    private $request;

    /**
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;
        $this->tmpl = $this->atp->factory->Template();

        $this->_parse_request();

        //if ($_SERVER['REMOTE_ADDR'] !== '127.0.0.1') {
        //	throw new \Exception('STOP');
        //}
    }

    public function init()
    {
        if ($this->_authorization() === false) {
            //throw new \Exception('Neturite teisės matyti šio puslapio', 401);
            return $this->atp->printForbidden();
        }

        if (in_array($this->browse->view, array('page', 'ajax', 'action'))) {
            $result = $this->_get_method($this->browse->method, $this->browse->view);

            if ($this->browse->view === 'page') {
                return $result;
            } elseif ($this->browse->view === 'ajax') {
                echo $result;
                exit;
            } elseif ($this->browse->view === 'action') {
                header('Location: ' . $result);
                exit;
            }
        }
    }

    /**
     * Apdoroja http užklausos duomenis
     */
    private function _parse_request()
    {
        $this->request = (object) $_REQUEST;
        $this->files = $_FILES;

        $browse = new \stdClass();
        $request = &$this->request;

        if (isset($request->view)) {
            $browse->view = $request->view;
        } else {
            $browse->view = 'page';
        }

        if (isset($request->method)) {
            $browse->method = $request->method;
        }


        if ($browse->view === 'page' && empty($browse->method)) {
            $browse->method = 'AdminMain';
        }

        $this->browse = $browse;
    }

    /**
     * Gauna kviečiamo metodo rezultatą
     * @param string $method
     * @param string $prefix
     * @return mixed
     * @throws \Exception
     */
    private function _get_method($method, $prefix)
    {
        $method = $prefix . $method;
        if (method_exists($this, $method)) {
            $html = $this->$method();
        } else {
            throw new \Exception('Metodas nerastas', 404);
        }

        return $html;
    }

    /**
     * Tikrina autorizaciją
     * @return boolean
     */
    private function _authorization()
    {
        $browse = &$this->browse;

        if ($browse->view === 'page') {
            if ($browse->method === 'AdminMain') {
                if (!$this->atp->factory->User()->getLoggedPersonDutyStatus('A_DOCUMENT')) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Sugeneruoja adresą
     * @param type $arr
     * @param type $type [optional] Default: 1
     * @return string
     */
    private function make_url($arr, $type = 1)
    {
        if ($type === 1) {
            $url = 'index.php?';
        } else {
            $url = $this->atp->config['SUBSYSTEM_URL'] . '?';
        }

        if (isset($arr['m']) === false) {
            $url .= 'm=' . $this->request->m;
        } else {
            $url .= 'm=' . $arr['m'];
            unset($arr['m']);
        }

        $qry = http_build_query($arr);
        if (empty($qry) === false) {
            $url .= '&' . $qry;
        }

        return $url;
    }

    /**
     * Dokumento šablono redagavimo forma
     * @return type
     */
    private function PageEditTemplate()
    {
        $template = new \Atp_Document_Templates_Template($this->atp, $this->request->id);
        $obj = new \Atp_Document_Templates_Template_Form($template, $this->atp);

        return $obj->printForm();
    }

    /**
     * Paslaugos išsaugojimas (dokumento šablono redagavimo forma)
     * @return type
     */
    private function AjaxSaveService()
    {
        $template = new \Atp_Document_Templates_Template($this->atp, $this->request->template);
        $obj = new \Atp_Document_Templates_Template_Form($template, $this->atp);
        $result = $obj->saveService();

        return json_encode(array('result' => $result));
    }

    /**
     * Sąlygos išsaugojimas (dokumento šablono redagavimo forma)
     * @return type
     */
    private function AjaxSaveCondition()
    {
        $template = new \Atp_Document_Templates_Template($this->atp, $this->request->template);
        $obj = new \Atp_Document_Templates_Template_Form($template, $this->atp);

        return $obj->printAjax();
    }

    /**
     * Sąklygos pašalinimas (dokumento šablono redagavimo forma)
     * @return type
     */
    private function AjaxRemoveCondition()
    {
        $template = new \Atp_Document_Templates_Template($this->atp, $this->request->template);
        $condition = new \Atp_Document_Templates_Template_Condition($template, $this->atp);
        $condition->delete($_POST['condition']);

        return;
    }

    /**
     * Avilio laukų ryšio su dokumento laukais HTML lentelė (dokumento šablono redagavimo forma)
     * @return type
     */
    private function AjaxAvilysFieldsTable()
    {
        $templateManager = new \Atp_Document_Templates_Template($this->atp, $this->request->template);
        $obj = new \Atp_Document_Templates_Template_Form($templateManager, $this->atp);
        $template = $templateManager->get();
        $result = $obj->getServiceFieldsTable($template->service_id);

        return json_encode(array('result' => (bool) $result, 'html' => $result));
    }

    /**
     * Avilio lauko ryšio su dokumento lauku išsaugojimas (dokumento šablono redagavimo forma)
     */
    private function AjaxSaveAvilysField()
    {
        $templateManager = new \Atp_Document_Templates_Template($this->atp, $this->request->template);
        //$servicesManager =Atp_Document_Templates_Services($this->atp); // $this->request->service
        $obj = new \Atp_Document_Templates_Template_Form($templateManager, $this->atp);
        //$template = $templateManager->get();

        $param = array(
            'service' => $this->request->service,
            'field' => $this->request->field,
            'avilys_field' => $this->request->avilys_field
        );
        $result = $obj->saveServiceField($param);

        return json_encode(array('result' => (bool) $result, 'html' => $result));
    }

    /**
     * Šablono trinimas
     * @return string
     */
    private function ActionDeleteTemplate()
    {
        $result = $this->atp->db_query_fast('SELECT DOCUMENT_ID
			FROM ' . PREFIX . 'ATP_TABLE_TEMPLATE
			WHERE ID = :ID', array('ID' => (int) $this->request->id));

        $count = $this->atp->db_num_rows($result);
        if ($count) {


            $template = new \Atp_Document_Templates_Template($this->atp, $this->request->id);
            $templateData = $template->get();
            $adminJournal = $this->atp->factory->AdminJournal();
            $adminJournal->addRecord(
                $adminJournal->getDeleteRecord(
                    'Administravime, dokumento šabloną'
                    . ' (Dokumentas "' . $this->atp->factory->Document()->Get($templateData->document_id)->getLabel() . '";'
                    . ' Šablonas "' . $templateData->label . '").'
                )
            );


            $row = $this->atp->db_next($result);
            $this->atp->db_query_fast('DELETE FROM ' . PREFIX . 'ATP_TABLE_TEMPLATE WHERE ID = :ID', array(
                'ID' => (int) $this->request->id));
            // TODO: trint failą iš failų sistemos?
        }

        return $this->make_url(array('view' => 'page', 'method' => 'AdminMain', 'id' => $row['DOCUMENT_ID']));
    }

    /**
     * Šablono atsisiuntimas
     * @throws \Exception
     */
    private function ActionDownloadTemplate()
    {
        $template = (new TemplateNew($this->atp))->GetTemplates(array('ID' => $this->request->id), true)[0];

        if ($template['HANDLER'] !== 'FileHandler' || empty($template['HANDLER_DATA']['FILE'])) {
            throw new \Exception('Šablonas nerastas');
            return false;
        }

        // TODO: paduoti handlinti failų klasei?

        $file = $this->atp->config['REAL_URL'] . 'files/templates/' . $template['HANDLER_DATA']['FILE'];
        if (is_file($file) === false) {
            throw new \Exception('Šablonas nerastas');
        }

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($template['LABEL'] . '.' . end(explode('.', $template['HANDLER_DATA']['FILE']))) . '"');
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        //header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;
    }

    /**
     * Šablono pridėjimas
     * @return string
     * @throws \Exception
     */
    private function ActionAdminMain()
    {
        $atp = &$this->atp;

        $template = new TemplateNew($this->atp);

        $handler_data = new \stdClass();
        if ($this->request->template_type === 'script') {
            $result = $template->SetHandler(new Template\ScriptHandler($this->atp));
            if ($result) {
                $handler_data->script = $this->request->script;
            }
        } elseif ($this->request->template_type === 'file') {
            $result = $template->SetHandler(new Template\FileHandler($this->atp));
            if ($result) {
                $handler_data->file = $this->files['file'];
            }
        }

        // Jei pavyko nurodyti vykdytoją
        if ($result === true) {
            // Vykdomas saugojimas
            $templateData = (object) array(
                    'document_id' => $this->request->id,
                    'label' => $this->request->template_name
            );
            $result = $template->Save($templateData, $handler_data);
        }

        if ($result !== true) {
            /*
             * TODO: Susirinkti klaidas iš TemplateNew, kuris susirenka klaidas iš handler'io.
             * Tam reikėtu apsirašyti klasę ar metodous pranešimų valdymui
             */
            throw new \Exception('Nepavyko išsaugoti');
        } else {
            $atp->set_message('success', $atp->lng('success_created'));


            $template = new \Atp_Document_Templates_Template($this->atp, $templateData->id);
            $templateData = $template->get();
            $adminJournal = $this->atp->factory->AdminJournal();
            $adminJournal->addRecord(
                $adminJournal->getCreateRecord(
                    'Administravime, dokumento šabloną'
                    . ' (Dokumentas "' . $this->atp->factory->Document()->Get($templateData->document_id)->getLabel() . '";'
                    . ' Šablonas "' . $templateData->label . '").'
                )
            );

            
        }

        return $this->make_url(array('view' => 'page', 'method' => 'AdminMain', 'id' => $this->request->id));
    }

    /**
     * Dokumentų šablonai (Administravimas)
     * @return string
     */
    private function PageAdminMain()
    {
        $document_id = (!empty($this->request->id)) ? $this->request->id : 0;
        $documents = $this->atp->logic2->get_document();

        // Šablonas
        $tmpl = &$this->tmpl;
        $tmpl->handle = 'documents_file';
        $tmpl->set_file($tmpl->handle, 'documents.tpl');

        // Šablono blokai
        $handles = $tmpl->set_multiblock($tmpl->handle, array(
            array(
                'var_row_block', // Dokumento šablono kintamojo eilutė
                'no_template', // Pranešimas, kad nėra šablonų
                array(
                    array(
                        'template_edit', // Šablono trinimas
                        'template_download', // Šablono pavadinimas su atsisiuntimo linku
                        'template_no_download'), // Šablono pavadinimas be atsisiuntimo linko
                    'template'), // Šablono blokas
                'templates', // Šablonų konteineris
                array(
                    'script_block'), // Skripto pasirinkimas
                'new_template'), // Naujo šablono įkėlimo blokas
            'document_info_block', // Dokumento informacijos blokas
            'document_block'// Dokumentas iš dokumentų pasirinkimo
        ));


        $tmpl->set_var(array(
            'title' => 'Dokumentų šablonai',
            'sys_message' => $this->atp->get_messages(),
            'GLOBAL_SITE_URL' => $this->atp->config['GLOBAL_SITE_URL']
        ));


        // Dokumento pasirinkimas
        $tmpl->set_var(array(
            'value' => '0',
            'label' => '-- nepasirinkta --',
            'selected' => ''
        ));
        $tmpl->parse($handles['document_block'], 'document_block');
        if (count($documents)) {
            foreach ($documents as &$document) {
                $tmpl->set_var(array(
                    'value' => $document['ID'],
                    'label' => $document['LABEL'],
                    'selected' => $document['ID'] == $document_id ? ' selected="selected"' : ''));
                $tmpl->parse($handles['document_block'], 'document_block', true);
            }
        }


        // Jei yra pasirinktas dokumentas
        if (empty($documents[$document_id]) === false) {


            $adminJournal = $this->atp->factory->AdminJournal();
            $adminJournal->addRecord(
                $adminJournal->getVisitRecord(
                    'Administravime, dokumentų šablonai'
                    . ' (Dokumentas "' . $this->atp->factory->Document()->Get($document_id)->getLabel() . '").'
                )
            );


            // Naujo šablono pridėjimo blokas
            if ($this->atp->factory->User()->getLoggedPersonDutyStatus('A_DOCUMENT') > 1) {
                $tmpl->set_var(array(
                    'form_url' => $this->make_url(array('id' => $document_id, 'method' => 'AdminMain',
                        'view' => 'action'))
                ));
                $tmpl->parse($handles['new_template'], 'new_template');


                // Skripto pasirinkimas
                $scripts = (new Template\ScriptHandler($this->atp))->get_scripts();

                $tmpl->set_var(array(
                    'value' => '0',
                    'label' => '-- nepasirinkta --',
                    'selected' => ''
                ));
                $tmpl->parse($handles['script_block'], 'script_block');
                if (count($scripts)) {
                    foreach ($scripts as &$script) {
                        $tmpl->set_var(array(
                            'value' => $script->name,
                            'label' => $script->label
                        ));
                        $tmpl->parse($handles['script_block'], 'script_block', true);
                    }
                }


                $tmpl->parse($handles['new_template'], 'new_template');
            } else {
                $tmpl->set_var($handles['new_template'], '');
            }

            // Visi pridėti šablonai
            $templates = (new TemplateNew($this->atp))->GetTemplates(array('DOCUMENT_ID' => $document_id));
            if (empty($templates) === false) {
                foreach ($templates as $template) {
                    $set_var = array(
                        'label' => $template['LABEL']
                    );

                    // Tikrina redagavimo teisę // TODO: kitaip?
                    if ($this->atp->factory->User()->getLoggedPersonDutyStatus('A_DOCUMENT') > 1) {
                        $set_var['edit_url'] = $this->make_url(array('id' => $template['ID'],
                            'method' => 'EditTemplate', 'view' => 'page'));
                        $set_var['delete_url'] = $this->make_url(array('id' => $template['ID'],
                            'method' => 'DeleteTemplate', 'view' => 'action'));
                        $set_var['download_url'] = $this->make_url(array('id' => $template['ID'],
                            'method' => 'DownloadTemplate', 'view' => 'action'));
                    } else {
                        $tmpl->clean('template_edit');
                    }


                    // TODO: reikėtu, kad valdytu pats handler'is
                    if ($template['HANDLER'] === 'FileHandler') {
                        $show = 'template_download';
                        $set_var['download_url'] = $this->make_url(array('id' => $template['ID'],
                            'method' => 'DownloadTemplate', 'view' => 'action'));
                        $set_var[$handles['template_no_download']] = '';
                    } else {
                        $show = 'template_no_download';
                        $set_var[$handles['template_download']] = '';
                    }


                    $tmpl->set_var($set_var);
                    $tmpl->parse($handles[$show], $show);
                    $tmpl->parse($handles['template_edit'], 'template_edit');

                    $tmpl->parse($handles['template'], 'template', true);
                }

                $tmpl->parse($handles['templates'], 'templates');
                $tmpl->set_var($handles['no_template'], '');
            } else {
                $tmpl->parse($handles['no_template'], 'no_template');
                $tmpl->set_var($handles['templates'], '');
            }

            // TODO: Pakeisti kintamųjų lentelės generavimą
            // Dokumento kintamieji
            $data = array(
                'var_row_block' => $handles['var_row_block'],
                'field_type' => $this->atp->logic->get_field_type(),
                'table_name' => $documents[$document_id]['LABEL']
            );
            $this->atp->manage->document_fields($document_id, 'DOC', $data, $tmpl);

            $tmpl->parse($handles['document_info_block'], 'document_info_block');
        } else {
            $tmpl->set_var($handles['document_info_block'], '');
        }

        // TODO: Yra tikimybė, kad ankščiau blokas buvo išparsintas
        $tmpl->set_var($handles['no_template'], '');

        $tmpl->parse($tmpl->handle . '_out', $tmpl->handle);
        $output = $tmpl->get($tmpl->handle . '_out');

        // TODO: Iškelti. PageEditTemplate kviečiaAtp_Document_Templates_Template_Form::printForm(), kuriame jis taip pat naudojamas
        return $this->atp->atp_content($output);
    }
}
