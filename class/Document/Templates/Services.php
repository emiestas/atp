<?php

class Atp_Document_Templates_Services
{
    /**
     * @var \Atp\Core
     */
    private $atp;

    private $wp_service;

    /**
     *
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core &$atp)
    {
        $this->atp = &$atp;

        global $srv;
        prepare_service();
        $srv->openSession();
        $srv->getConfig('service');
        $srv->getAllLangElements('service');
        $this->wp_service = &$srv;
    }

    /**
     * Priskiria paslaugą prie dokumento šablono
     * @param int $template_id
     * @param int $service_id Paslaugos ID
     * @return boolean
     */
    public function addToTemplate($template_id, $service_id)
    {
        $arg = array(
            'ID' => (int) $template_id,
            'SERVICE_ID' => (int) $service_id
        );

        // Jei keičiasi servisas, ištrinami susieti avilio laukai
        $qry = 'SELECT COUNT(SERVICE_ID) CNT
			FROM ' . PREFIX . 'ATP_TABLE_TEMPLATE
			WHERE
				SERVICE_ID = :SERVICE_ID
				AND ID = :ID';
        $result = $this->atp->db_query_fast($qry, $arg);
        $row = $this->atp->db_next($result);
        if ((int) $row['CNT'] === 0) {
            $qry = 'DELETE
				FROM ' . PREFIX . 'ATP_TMPL_SERVICE_AVILYS_FIELDS_X_VARFIELDS_REL
				WHERE TEMPLATE_ID = :ID';
            $result = $this->atp->db_query_fast($qry, $arg);
        }

        $qry = 'UPDATE ' . PREFIX . 'ATP_TABLE_TEMPLATE SET SERVICE_ID = :SERVICE_ID WHERE ID = :ID';
        $this->atp->db_query_fast($qry, $arg);
        return true;
    }

    /**
     * Paslaugos informacija
     * @param int $service_id
     * @param boolean $get_form_elements
     * @return boolean|stdClass
     */
    public function getInfo($service_id, $get_form_elements = false)
    {
        $row = $this->atp->logic2->get_db(PREFIX . 'PASLAUGOS', array('ID' => $service_id), true, null, false, 'ID, SHOWS');

        if (empty($row)) {
            return false;
        }

        $return = array(
            'id' => $row['ID'],
            'label' => $row['SHOWS']
        );
        if ($get_form_elements === true) {
            $return['form'] = $this->getFormElementsInfo($service_id);
        }

        return (object) $return;
    }

    /**
     * Paslaugos informacija pagal dokumento šabloną
     * @param int $template_id
     * @return boolean|stdClass
     */
    public function getFromTemplate($template_id)
    {
        $qry = 'SELECT SERVICE_ID FROM ' . PREFIX . 'ATP_TABLE_TEMPLATE WHERE ID = :ID';
        $result = $this->atp->db_query_fast($qry, array('ID' => (int) $template_id));
        $row = $this->atp->db_next($result);
        if (empty($row)) {
            return false;
        }

        return $this->getInfo((int) $row['SERVICE_ID']);
    }

    /**
     * Gauna paslaugos formos elementų informaciją
     * @param int $service_id Paslaugos ID
     * @return array
     */
    public function getFormElementsInfo($service_id)
    {
        return $this->wp_service->getXMLFormElements($service_id);
        /*
          $result = array();

          $xml = $this->getServiceXML($service_id);
          if ($xml === FALSE)
          return $result;

          require_once($this->atp->config['GLOBAL_REAL_URL'] . 'helper/XML2Array.php');
          $array = XML2Array::createArray($xml);

          if(isset($array['root']['FORMELEMENT']) === FALSE)
          return $result;

          $result = $array['root']['FORMELEMENT'];
          if (empty($result) === FALSE && isset($result[0]) === FALSE)
          $result = array($result);

          $result = $this->removeCData($result);
          return $result;
         */
    }

//    // TODO: extend'inti Array2XML klasę
//    /**
//     * Iš masyvo pašalina "<i>@cdata</i>" indeksą
//     * @param array $array
//     * @deprecated Nebenaudojamas
//     * @return array
//     */
//    private function removeCData(&$array)
//    {
//        trigger_error('removeCData is deprecated', E_USER_DEPRECATED);
//        if (is_array($array) === false) {
//            return $array;
//        }
//
//        if (isset($array['@cdata'])) {
//            $array = $array['@cdata'];
//        } else {
//            $keys = array_keys($array);
//            for ($i = 0; isset($keys[$i]); $i++) {
//                $array[$keys[$i]] = $this->removeCData($array[$keys[$i]]);
//            }
//        }
//
//        return $array;
//    }

    /**
     * Gauna prie paslaugos formos laukų priskirtus avilio laukų pavadinimus
     * @param int $service_id Paslaugos ID
     * @return array
     */
    public function getAvilysNames($service_id)
    {
        $result = array();

        $form_elements = $this->getFormElementsInfo($service_id);
        if (is_array($form_elements) && count($form_elements)) {
            foreach ($form_elements as &$arr) {
                if (isset($arr['AVILYS_FIELD_NAME']) === false) {
                    continue;
                }

                //$value = current((array) $arr['AVILYS_FIELD_NAME']);
                $value = $arr['AVILYS_FIELD_NAME'];

                if (empty($value) === false) {
                    $result[] = $value;
                }
            }
        }

        return $result;
    }

    /**
     * Gauna formos elemento informaciją pagal paslaugą ir avilio lauko pavadinimą
     * @param int $service_id
     * @param string $avilys_name
     * @return bool|array
     */
    public function getFormElementInfoByAvilysName($service_id, $avilys_name)
    {
        // TODO: pridėtas failo laukas
        $qry = 'SELECT A.*, B.NAME AVILYS_NAME, C.NAME AVILYS_FIELD_NAME
			FROM
				' . LOCALPREFIX . 'PASLAUGOS_ORDER_FORM_FIELDS A
					LEFT JOIN ' . LOCALPREFIX . 'PASLAUGOS_FOREIGN_FIELDS B ON
						B.FIELD_ID = A.ID AND
						B.TYPE = "avilysAuto"
					LEFT JOIN ' . LOCALPREFIX . 'PASLAUGOS_FOREIGN_FIELDS C ON
						C.FIELD_ID = A.ID AND
						C.TYPE = "avilys"
			WHERE
				A.PASLAUGOS_ID = :ID AND
				C.NAME = :AVILYS_NAME';

        $result = $this->atp->db_query_fast($qry, array('ID' => (int) $service_id,
            'AVILYS_NAME' => $avilys_name));
        $count = $this->atp->db_num_rows($result);
        if ($count) {
            return $this->atp->db_next($result);
        }

        return false;
        /*
          $form_elements = $this->getFormElementsInfo($service_id);
          if (empty($form_elements))
          return false;


          $keys = array_keys($form_elements);
          for ($i = 0; isset($form_elements[$keys[$i]]); $i++) {
          if (empty($form_elements[$keys[$i]]['AVILYS_FIELD_NAME']) === FALSE && $form_elements[$keys[$i]]['AVILYS_FIELD_NAME'] === $avilys_name) {
          $result = $form_elements[$keys[$i]];
          break;
          }
          }

          return $result;
         */
    }

//    /**
//     * Gauna paslaugos formos laukų informacijos XML'ą
//     * @overload Originalus metodas yra <i>tools::getServiceXML()</i> iš <i>subsystems/adm/tools/class-serviceforms.php</i>
//     * @deprecated Nebenaudojamas
//     * @param int $service_id
//     * @return string
//     */
//    private function getServiceXML($service_id)
//    {
//        trigger_error('getServiceXML is deprecated', E_USER_DEPRECATED);
//        $file = $this->atp->config['GLOBAL_REAL_URL'] . 'subsystems/service/xmlforms/xmlform_' . $service_id . '.xml';
//        if (is_file($file)) {
//            return file_get_contents($file);
//        }
//
//        return false;
//    }
}
