<?php

namespace Atp\Document\Templates\Template;

/**
 * Šablono handler'io pradinė klasė
 */
abstract class AbstractHandler implements IHandler
{
    /**
     * Šablono saugojimas
     * @param \stdClass $param
     * @param int $param->id Šablono ID
     * @return bool
     */
    abstract public function Save(\stdClass $param);

    /**
     * Šablono pašalinimas
     * @param int $id Šablono ID
     * @return bool
     */
    abstract public function Delete($id);

    /**
     * Handler'io duomenys
     * @param int $id Šablono ID
     * @return array
     */
    abstract public function GetData($id);

    /**
     * Sugeneruoją dokumeno įrašo dokumentą ir gražina kelią iki jo.
     * @param int $record_id Dokumento įrašo ID
     * @param array $template_data Šablono duomenys
     * @return string Sugeneruoto failo kelias serveryje
     */
    abstract public function GenerateTemplate($record_id, $template_data);

    /**
     * Handler'io pavadinimas
     * @return string
     */
    public function GetName()
    {
        return array_pop(explode('\\', get_called_class()));
    }
}
