<?php

namespace Atp\Document\Templates\Template\Script;

class PunismentScript extends AbstractScript
{
    protected $label = 'Baustumo pažyma';

    /**
     * @var \Atp\Core
     */
    private $atp;

    /**
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;
    }

    public function GenerateTemplate($record_id)
    {
        $data = $this->getData($record_id);
        return $this->buildDocument($data);
    }

    private function getData($record_id)
    {
        $atp = &$this->atp;
        $violation_document_id = \Atp\Document::DOCUMENT_INFRACTION; // Pažeidimo duomenų dokumento ID // TODO: Hardcode
        $documents = $atp->logic2->get_document();
        $document = $documents[$violation_document_id];

        $record = $atp->logic2->get_record_relation($record_id);

        // Jei tai nėra pažeidimo duomenų dokumentas, tai kelyje ieškoma jo aukščiausios versijos
        if ((int) $record['TABLE_TO_ID'] !== (int) $document['ID']) {
            // Suranda paskutinį pažeidimo duomenų dokumento įrašą
            // TODO: naudoti \Atp\Document\Record\Path
            $record_path = $atp->logic->get_record_path($record['ID']);
            $filtered_records = array_filter($record_path, function ($array) use ($document) {
                if ((int) $array['TABLE_TO_ID'] !== (int) $document['ID']) {
                    return false;
                }
                return true;
            });

            $count = count($filtered_records);
            if ($count === 0) {
                throw new \Exception('Dokumentų kelyje nerastas nei vienas pažeidimo duomenų dokumentas');
            }

            $versions = array_map(function ($array) {
                return (int) $array['RECORD_VERSION'];
            }, $filtered_records);
            $main_record = (int) $filtered_records[array_search(max($versions), $versions, true)]['ID'];
        } else {
            $main_record = (int) $record['ID'];
        }

        // Pažeidimo duomenų dokumento įrašas
        $main_record = $atp->logic2->get_record_relation($main_record);

        // Straipsnis
        $clause = $atp->logic2->get_record_clause($main_record);
        // Asmens kodas
//        $field = $atp->logic2->get_fields(array('ID' => $document['PERSON_CODE']));
//        $field = $atp->logic2->get_record_table($main_record['ID'], 'C.`ID`, C.`' . $field['NAME'] . '` `VALUE`');
//        $code = $field['VALUE'];
        $code = $this->atp->factory->Record()->getFieldValue($main_record['ID'], $this->atp->factory->Document()->getOptions($document['ID'])->person_code);

        $param = array(
            // Tikrinamas dokumento įrašas
            'RECORD' => $main_record['ID'],
            // T.d.į. asmens kodas
            'CODE' => $code,
            // T.d.į. straipsnis
            'CLAUSE' => empty($clause) ? null : $clause['ID'],
            // T.d.į. automobilio numeris
            'AUTO_NR' => $atp->logic2->get_record_auto_nr($main_record['ID'])
        );

        // Dokumentų įrašų numeriai, kuriuose neieškoti baustumo
        $exclude_records = $atp->logic->get_record_path($main_record['ID']);
        array_walk($exclude_records, function (&$val) {
            $val = $val['RECORD_ID'];
        });
        $exclude_records = array_values($exclude_records);
        // Galiojantys baustumo įrašai
        $punishment = new \Atp\Document\Record\Punishment($atp);
        /* @var $punishment_records \Atpr\Kis\AtpDetailsType[] */
        $punishment_records = $punishment->getRecordsByType($param, 'P' . 'Atpeir', $exclude_records);

        //$investigator = $this->logic2->get

        $invest = $atp->logic->record_worker_info_to_fields($main_record['ID']);

        // Surenka dokumentų įrašų informaciją šablono generavimui
        $data = array(
            'PERSON' => array(
                'CODE' => $code,
                'NAME' => $atp->logic2->get_record_person_name($main_record['ID']),
                'ADDRESS' => $atp->logic2->get_record_person_address($main_record['ID'])
            ),
            'INVESTIGATOR' => array(
                'NAME' => join(' ', array($invest[4]['VALUE'], $invest[5]['VALUE'])),
                'POSITION' => $invest[6]['VALUE'],
                'STRUCTURE' => array(
                    $invest[8]['VALUE'], $invest[9]['VALUE'], $invest[10]['VALUE']
                )
            ),
            'PUNISHMENTS' => array()
        );


        /* @var $record \Atpr\Kis\AtpDetailsType */
        foreach ($punishment_records as $record) {
            $record = $atp->logic2->get_record_relation($record);

            // Visos laukų reikšmės
            $rv = new \Atp\Record\Values($this->atp);
            $values = $rv->getValuesByRecord($record['ID'], null, true);

            // Straipsnis
            $clause = $atp->logic2->get_record_clause($record['ID']);
            // Teisės akto informacija
            $act = $atp->logic2->get_record_act($record['ID']);
            // "Teisės aktas" // t.y. Teisės aktui priskirto klasifikatoriaus pavadinimas
            $classificator = $atp->logic2->get_classificator(array('ID' => $act['CLASSIFICATOR']));
            $act_label = $classificator['LABEL'];

            // Mokėjimai
            $payments = array();
            // Surenka mokėjimų informaciją
            $path_record_ids = array_map(function ($array) {
                return (int) $array['ID'];
            }, $atp->logic->get_record_path($record['ID']));

            if (count($path_record_ids)) {
                $all_payments = $atp->logic2->get_payments(array('RECORD_ID IN (' . join(',', $path_record_ids) . ')'));
                foreach ($all_payments as $payment) {
                    $qry = 'SELECT OPERACIJOS_DATA DATE, MOKA_MM_KODAS PAYER_CODE, MOKA_MM_PAVADINIMAS PAYER_NAME
						FROM ' . PREFIX . 'ATP_PAYMENTS_DATA
						WHERE ID = :ID';
                    $result = $atp->db_query_fast($qry, array('ID' => $payment['PAYMENT_ID']));
                    if ($atp->db_num_rows($result)) {
                        $row = $atp->db_next($result);
                        $row['PAID'] = $payment['PAID'];

                        $payments[] = $row;
                    }
                }
            }


            $tmp = array(
                'CLAUSE_NAME' => $clause['NAME'],
                'ACT_LABEL' => $act_label,
                'ACT_NR' => $act['NR'],
                'ACT_TYPE' => $values['ACOL_3'],
                'ACT_TEXT' => $values['ACOL_5'],
                'AUTO_NR' => $atp->logic2->get_record_auto_nr($record['ID']),
                'PAYMENTS' => $payments
            );

            // Pažeidimo data, laikas ir vieta
            $fields_rel = array(
                'VIOLATION_DATE' => $this->atp->factory->Document()->getOptions($document['ID'])->violation_date, // Pažeidimo data
                'VIOLATION_TIME' => $this->atp->factory->Document()->getOptions($document['ID'])->violation_time, // Pažeidimo laikas
                'VIOLATION_PLACE' => $this->atp->factory->Document()->getOptions($document['ID'])->violation_place// Pažeidimo vieta
            );
            $fields_arr = $atp->logic2->get_fields(array('ID IN (' . join(',', $fields_rel) . ')'));
            foreach ($fields_rel as $key => $val) {
                if (empty($fields_arr[$val])) {
                    continue;
                }

                $tmp[$key] = $values[$fields_arr[$val]['NAME']];
            }


            $data['PUNISHMENTS'][] = $tmp;
        }

        return $data;
    }

    private function buildDocument($data)
    {
        require_once($this->atp->config['REAL_URL'] . 'helper/PHPWord.php');
        $phpWord = $this->word = new \PHPWord();

        $phpWord->setDefaultFontName('Times New Roman');
        $phpWord->setDefaultFontSize(12);


        $cmToTwip = function ($cm) {
            return \PhpOffice\PhpWord\Shared\Font::centimeterSizeToTwips($cm);
        };
        $section = $phpWord->addSection(array(
            'orientation' => 'landscape',
            'marginTop' => $cmToTwip(2),
            'marginLeft' => $cmToTwip(2.54),
            'marginRight' => $cmToTwip(2.54),
            'marginBottom' => $cmToTwip(1)
        ));

        // Stiliai
        $phpWord->addParagraphStyle('pTitle', array('align' => 'center'));
        $phpWord->addFontStyle('fTitle', array('bold' => true));
        $phpWord->addParagraphStyle('pTitleDate', array('align' => 'center'));
        //$phpWord->addTableStyle('tTable', array('width' => 100));
        $tTable = array('width' => 50 * 100, 'unit' => 'pct');
        $phpWord->addFontStyle('fTableHead', array('bold' => true));
        $cell_width = array(25, 75);
        $phpWord->addParagraphStyle('pEmptyUnderline', array('align' => 'center'));
        $phpWord->addFontStyle('fSubTitle', array('bold' => true));
        $phpWord->addParagraphStyle('pSubTableHead', array('spaceBefore' => 6));




        // Title
        $section->addText('PAŽYMA', 'fTitle', 'pTitle');
        $section->addText('APIE TAIKYTAS ATP PRIEMONES, UŽVESTAS BYLAS IR ĮMOKAS', 'fTitle', 'pTitle');
        $section->addTextBreak(1);
        $section->addText(date('Y-m-d'), null, 'pTitleDate');
        $section->addTextBreak(1);

        // Asmens informacija
        $table = $section->addTable($tTable);

        $row = $table->addRow();
        $row->addCell($cell_width[0])->addText('Asmens vardas, pavardė:', 'fTableHead');
        $row->addCell($cell_width[1])->addText($data['PERSON']['NAME']);
        $row = $table->addRow();

        $row->addCell($cell_width[0])->addText('Adresas kodas / gimimo data:', 'fTableHead');
        $row->addCell($cell_width[1])->addText($data['PERSON']['CODE']);

        $row = $table->addRow();
        $row->addCell($cell_width[0])->addText('Adresas:', 'fTableHead');
        $row->addCell($cell_width[1])->addText($data['PERSON']['ADDRESS']);


        // Kažkas
        $section->addTextBreak(1);
        $section->addText('________________', null, 'pEmptyUnderline');
        $section->addTextBreak(1);
        $section->addText('Taikytų ATP priemonių, užvestų bylų ir įmokų ataskaita:', 'fSubTitle');
        $section->addTextBreak(3);
        $section->addText('________________', null, 'pEmptyUnderline');
        $section->addTextBreak(1);


        // Pažeidimai
        $count = count($data['PUNISHMENTS']);
        if ($count) {
            $space = '        ';
            for ($i = 0; $i < $count; $i++) {
                $tmp = $data['PUNISHMENTS'][$i];

                $table = $section->addTable($tTable);
                // <editor-fold defaultstate="collapsed" desc="Seni dokumento laukai">
                /*
                  $row = $table->addRow();
                  $row->addCell($cell_width[0])->addText('Pažeidimo identifikacinis numeris:', 'fTableHead');
                  $row->addCell($cell_width[1])->addText('old');

                  $row = $table->addRow();
                  $row->addCell($cell_width[0])->addText('Pranešimo Nr. (jei yra):', 'fTableHead');
                  $row->addCell($cell_width[1])->addText('old');


                  $row = $table->addRow();
                  $row->addCell($cell_width[0])->addText('Pažeidimo data, laikas, vieta:', 'fTableHead');
                  $row->addCell($cell_width[1])->addText('old');


                  $row = $table->addRow();
                  $row->addCell($cell_width[0])->addText('Pažeistas teisės aktas, LR ATPK straipsnis, dalis:', 'fTableHead');
                  $row->addCell($cell_width[1])->addText('old');


                  $row = $table->addRow();
                  $row->addCell($cell_width[0])->addText('Pažeidimo esmė:', 'fTableHead');
                  $row->addCell($cell_width[1])->addText('old');
                 */// </editor-fold>
                //$row = $table->addRow();
                //$row->addCell($cell_width[0])->addText('Pažeidimo data:', 'fTableHead');
                //$row->addCell($cell_width[1])->addText($tmp['VIOLATION_DATE']);
                //$row = $table->addRow();
                //$row->addCell($cell_width[0])->addText('Pažeidimo laikas:', 'fTableHead');
                //$row->addCell($cell_width[1])->addText($tmp['VIOLATION_TIME']);
                //$row = $table->addRow();
                //$row->addCell($cell_width[0])->addText('Pažeidimo vieta:', 'fTableHead');
                //$row->addCell($cell_width[1])->addText($tmp['VIOLATION_PLACE']);

                $row = $table->addRow();
                $row->addCell($cell_width[0])->addText('Pažeidimas:', 'fTableHead');
                $row->addCell($cell_width[1])->addText(join(' ', array_filter(array(
                    $tmp['VIOLATION_DATE'],
                    $tmp['VIOLATION_TIME'],
                    $tmp['VIOLATION_PLACE']
                ))));

                $row = $table->addRow();
                $row->addCell($cell_width[0])->addText('Straipsnis:', 'fTableHead');
                $row->addCell($cell_width[1])->addText($tmp['CLAUSE_NAME']);

                $row = $table->addRow();
                $row->addCell($cell_width[0])->addText('Teisės aktas:', 'fTableHead');
                $row->addCell($cell_width[1])->addText($tmp['ACT_LABEL']);

                $row = $table->addRow();
                $row->addCell($cell_width[0])->addText('Teisės akto numeris:', 'fTableHead');
                $row->addCell($cell_width[1])->addText($tmp['ACT_NR']);

                $row = $table->addRow();
                $row->addCell($cell_width[0])->addText('Teisės akto rūšis:', 'fTableHead');
                $row->addCell($cell_width[1])->addText($tmp['ACT_TYPE']);

                $row = $table->addRow();
                $row->addCell($cell_width[0])->addText('Tekstas:', 'fTableHead');
                $row->addCell($cell_width[1])->addText($tmp['ACT_TEXT']);

                if (empty($tmp['AUTO_NR']) === false) {
                    $row = $table->addRow();
                    $row->addCell($cell_width[0])->addText('Automobilio numeris:', 'fTableHead');
                    $row->addCell($cell_width[1])->addText($tmp['AUTO_NR']);
                }

                $m_count = count($tmp['PAYMENTS']);
                if ($m_count) {
                    $row = $table->addRow();
                    $cell = $row->addCell(null, array('gridSpan' => 2));
                    $cell->addText('Įmokos:', 'fTableHead');

                    for ($j = 0; $j < $m_count; $j++) {
                        $payment = $tmp['PAYMENTS'][$j];

                        if ($j !== 0) {
                            $table->addRow()->addCell(null, array('gridSpan' => 2));
                        }

                        $row = $table->addRow();
                        $row->addCell()->addText($space . 'Data:', null, 'pSubTableHead');
                        $row->addCell()->addText($payment['DATE']);

                        $row = $table->addRow();
                        $row->addCell()->addText($space . 'Mokėtojas:', null, 'pSubTableHead');
                        $row->addCell()->addText($payment['PAYER_NAME'] . ' (a.k. ' . $payment['PAYER_CODE'] . ')');

                        $row = $table->addRow();
                        $row->addCell()->addText($space . 'Sumokėta suma:', null, 'pSubTableHead');
                        $row->addCell()->addText($payment['PAID']);
                    }
                }
            }
        }

        $section->addText('________________', null, 'pEmptyUnderline');
        $section->addTextBreak(1);
        //$section->addText('Tikrino Vilniaus miesto savivaldybės administracijos Saugaus miesto departamento Administracinių pažeidimų tyrimo skyriaus vyriausioji specialistė Kristina Kazarinienė');
        $section->addText('Tikrino ' . $data['INVESTIGATOR']['NAME'] . ', ' . $data['INVESTIGATOR']['POSITION'] . ', ' . join(', ', array_filter($data['INVESTIGATOR']['STRUCTURE'])));

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $old_mask = umask(0);
        $tmpfname = tempnam(sys_get_temp_dir(), 'scriptDoc_');
        $objWriter->save($tmpfname);
        umask($old_mask);

        return $tmpfname;
    }
}
