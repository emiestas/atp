<?php

namespace Atp\Document\Templates\Template\Script;

interface IScript
{
    public function GetName();

    public function GetLabel();

    public function GenerateTemplate($record_id);
}
