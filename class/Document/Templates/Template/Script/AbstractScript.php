<?php

namespace Atp\Document\Templates\Template\Script;

abstract class AbstractScript implements IScript
{
    protected $label;

    public function GetName()
    {
        return array_pop(explode('\\', get_called_class()));
    }

    public function GetLabel()
    {
        return $this->label;
    }

    abstract public function GenerateTemplate($record_id);
}
