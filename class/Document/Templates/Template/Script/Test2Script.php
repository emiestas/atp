<?php

namespace Atp\Document\Templates\Template\Script;

//class Test2Script extends AbstractScript {
class Test2Script
{
    protected $label = 'Kita pažyma';

    /**
     * @var \Atp\Core
     */
    private $atp;

    /**
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;
    }

    public function GenerateTemplate($record_id)
    {
        die('Test2Script::GenerateTemplate');
    }
}
