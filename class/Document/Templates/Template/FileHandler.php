<?php

namespace Atp\Document\Templates\Template;

/**
 * Šablono iš failo handler'is
 */
class FileHandler extends AbstractHandler
{
    /**
     * Šablonų talpykla
     * @var string
     */
    private $files_root = 'files/templates/';

    /**
     * @var \Atp\Core
     */
    private $atp;

    /**
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;

        // TODO: Paduoti tvarkytis Fail'ų handleriui
        // Sukuriamas katalogas failo talpinimui
        $this->global_files_root = $this->atp->config['REAL_URL'] . $this->files_root;
        if (is_dir($this->global_files_root) === false) {
            $oldmask = umask(0);
            mkdir($this->global_files_root, 0777, true);
            umask($oldmask);
        }
    }

    /**
     * Šablono saugojimas
     * @param \stdClass  $param
     * @param int $param->id Šablono ID
     * @param array $param->file $_FILES masyvas be HTML elemento vardo
     * @return bool
     */
    public function Save(\stdClass $param)
    {
        if (empty($param->file)) {
            return false;
            //throw new \Exception('Nerastas įkeliamas failas');
        }

        $file = $param->file;

        // Tikrinama ar įkeltas failas
        if (empty($file['error']) === false) {
            return false;
            //throw new \Exception('Nerastas įkeliamas failas');
        }

        // Tikrinama ar leidžiamas failo tipas
        $extension = pathinfo($file['name'], PATHINFO_EXTENSION);
        if (in_array($extension, array('doc', 'docx')) === false) {
            return false;
            //throw new \Exception('Extension not allowed');
        }

        $filename = 'tpl' . $param->id . '.' . $extension;

        // Bandoma perkelti failą į talpyklą, nepavykus pašalinami išsaugoti duomenys
        $result = rename($file['tmp_name'], $this->global_files_root . $filename);
        if ($result === true) {
            // Sukuriamas ryšys tarp šablono ir failo
            $qry = 'INSERT INTO ' . PREFIX . 'ATP_TMPL_X_FILE (TMPL_ID, FILE) VALUES (:ID, :FILE)';
            $this->atp->db_query_fast($qry, array(
                'ID' => $param->id,
                'FILE' => $filename
            ));
        }

        return $result;
    }

    /**
     * Šablono pašalinimas
     * @param int $id Šablono ID
     * @return bool
     */
    public function Delete($id)
    {
        $param = array('ID' => $id);

        // Pašalinamas ryšys tarp skripto ir failo
        $this->atp->db_query_fast('DELETE FROM ' . PREFIX . 'ATP_TMPL_X_FILE WHERE TMPL_ID = :ID', $param);

        // Tikrina ar ištrinta
        $result = $this->atp->db_query_fast('SELECT COUNT(ID) CNT FROM ' . PREFIX . 'ATP_TMPL_X_FILE WHERE TMPL_ID = :ID', $param);
        return (bool) (int) $this->atp->db_next($result)['CNT'];
    }

    /**
     * Handler'io duomenys
     * @param int $id Šablono ID
     * @return array
     */
    public function GetData($id)
    {
        $data = $this->atp->logic2->get_db(PREFIX . 'ATP_TMPL_X_FILE', array('TMPL_ID' => $id), true, null, false, null, array(), false);
        unset($data['ID'], $data['TMPL_ID']);

        return $data;
    }

    /**
     * Bando pakeisti lietuvišką žodį į kitą linksnį
     * @param string $string žodis ar frazė
     * @param string $action linksnis, dafault - "from" (kilmininkas).
     * @return string pakeistas žodis ar frazė
     */


	function makeStringLTStructure($string, $action = 'from') {
		$array = array();
		$array['normal'][0] = 'DEPARTAMENTAS';
		$array['from'][0] = 'DEPARTAMENTO';
		$array['normal'][1] = 'SKYRIUS';
		$array['from'][1] = 'SKYRIAUS';
		$array['normal'][2] = 'POSKYRIS';
		$array['from'][2] = 'POSKYRIO';
		$array['normal'][3] = 'TARNYBA';
		$array['from'][3] = 'TARNYBOS';
		$array['normal'][4] = 'SAVIVALDYBĖ';
		$array['from'][4] = 'SAVIVALDYBĖS';
		$array['normal'][5] = 'TARYBA';
		$array['from'][5] = 'TARYBOS';
		$array['normal'][6] = 'ADMINISTRACIJA';
		$array['from'][6] = 'ADMINISTRACIJOS';
		$array['normal'][7] = 'KOMISIJA';
		$array['from'][7] = 'KOMISIJOS';

		$myString = $string;
		foreach ($array['normal'] as $key => $val) {
			$myString = str_replace($array['normal'][$key], $array[$action][$key], $myString);
			$myString = str_replace(strtolower($array['normal'][$key]), strtolower($array[$action][$key]), $myString);
		}
		if (empty($myString) === FALSE) {
			return $myString;
		} else {
			return $string;
		}
	}

    /**
     * Sugeneruoją dokumeno įrašo dokumentą ir gražina kelią iki jo.
     * @param int $record_id Dokumento įrašo ID
     * @param array $template_data Šablono duomenys
     * @return string Sugeneruoto failo kelias serveryje
     * @throws \Exception
     */
    public function GenerateTemplate($record_id, $template_data)
    {

		$ltStructArrFrom = array('${PRANTI8}', '${PRANTI9}', '${PRANTI10}');
		
		$atp = &$this->atp;

        $data = $this->getData($template_data['ID']);
        $template = (object) array(
                'id' => $template_data['ID'],
                'label' => $template_data['LABEL'],
                'filename' => $data['FILE'],
                'document_id' => $template_data['DOCUMENT_ID'],
        );

        // Doukemento informacija
        $document = $atp->logic2->get_document($template->document_id);

        // Dokumento įrašo informacija
        $record = $atp->logic2->get_record_relation($record_id);
        $record_table = $atp->logic2->get_record_table($record['ID'], 'C.ID');

		$ROIK = false;
		$roik_qur = "SELECT ROIK, RECORD_ID FROM ".PREFIX."ATP_ATPR_ATP WHERE RECORD_ID = '".$record['ID']."'";
		$roik_col = $atp->db_query_fast($roik_qur);
		$roik_res = $atp->db_next($roik_col);
		if (empty($roik_res['ROIK']) === FALSE) {
			$ROIK = $roik_res['ROIK'];
		}


        $path = array();
        $path['real']['template'] = $this->global_files_root . $data['FILE'];
        if (is_file($path['real']['template']) === false) {
            throw new \Exception('Documents template file "' . pathinfo($path['real']['template'], PATHINFO_BASENAME) . '" was not found.');
        }

        // Paduoda dokumento šabloną redagavimui
        require_once($atp->config['REAL_URL'] . 'helper/PHPWord.php');
        $PHPWord = new \PHPWord();
        $doc = $PHPWord->loadTemplate($path['real']['template']);
        $next_file_var = 0;

        // Sudeda laukų reikšmes į šabloną
        $data = array('table_name' => $document['LABEL']);
        $all_fields = $atp->manage->document_fields($document['ID'], 'DOC', $data);
		foreach ($all_fields as &$field) {
            $varpath = $atp->logic->parse_variable($field['VAR_NAME']);
			$info = $atp->logic->get_field_by_varpath($varpath, $document['ID'], $record_table['ID'], $record['ID']);
            if (in_array($info['INFO']['TYPE'], array(23, 19, 18)) === true) {
                // Veika
                if ($info['INFO']['TYPE'] === '23') {
                    $arr = $atp->logic->get_deed(array('ID' => $info['VALUE']));
                    $info['VALUE'] = $arr['LABEL'];
                } else {
                    // Straipsnis
                    if ($info['INFO']['TYPE'] === '19') {
                        $arr = $atp->logic2->get_clause(array('ID' => $info['VALUE']));
                        $info['VALUE'] = $arr['NAME'];
                    } else {
                        // Klasifikatorius
                        if ($info['INFO']['TYPE'] === '18') {
                            $arr = $atp->logic2->get_classificator(array('ID' => $info['VALUE']));
                            $info['VALUE'] = $arr['LABEL'];
                        }
                    }
                }
            } else {
                // Datos ir laiko laukai
                if ($info['INFO']['SORT'] == 7) {
                    //var_dump($structure_value, strtotime($structure_value), date('Y-m-d H:i:s', NULL));
                    $is_empty = false;

                    // Jeigu tuščia, data ar laikas prasideda nuliais
                    if (empty($info['VALUE']) === true || preg_match('/^([0]{4}.*|[0]{2}:[0]{2}(:[0]{2})?)$/', $info['VALUE']) === 1 || strtotime($info['VALUE']) <= 0) {
                        $is_empty = true;
                    }

                    $timestamp = strtotime($info['VALUE']);
                    if ($info['INFO']['TYPE'] == 11) {
                        $time_string = date('Y', $timestamp);
                    } elseif ($info['INFO']['TYPE'] == 12) {
                        $time_string = date('Y-m-d', $timestamp);
                    } elseif ($info['INFO']['TYPE'] == 13) {
                        $time_string = date('H:i', $timestamp);
                    } elseif ($info['INFO']['TYPE'] == 14) {
                        ;
                        $time_string = date('Y-m-d H:i', $timestamp);
                    }

                    if ($info['VALUE'] === null) {
                        $info['VALUE'] = null;
                    } elseif ($is_empty === true) {
                        /**
                         * @TODO: [done] Netinka, kadangi radus NULL reikšmę įkeliama reikšmė pagal nutylėjimą
                          $structure_value = NULL;
                         */
                        $info['VALUE'] = '';
                    } else {
                        $info['VALUE'] = $time_string;
                    }
                } elseif ((int) $info['INFO']['SORT'] === 10) {
                    if ((int) $info['INFO']['TYPE'] === 20) {
                        if ((int) $info['VALUE'] === 1) {
                            $file_vars = null;
                            if ((int) $document['ID'] === \Atp\Document::DOCUMENT_INFRACTION) {
                                $file_vars = array(
                                    'image1.jpeg',
                                    'image2.jpeg',
                                    'image3.jpeg',
                                    'image4.jpeg',
                                    'image5.jpeg',
                                    'image6.jpeg',
                                    'image7.jpeg',
                                    'image8.jpeg',
                                    'image9.jpeg',
                                    'image10.jpeg'
                                );
                            }
                            if (empty($file_vars) === false) {
                                $files = $atp->logic2->get_record_files(array(
                                    'RECORD_ID' => $record['ID'],
                                    'FIELD_ID' => $info['INFO']['ID']));

                                if (count($files) > 0) {
                                    foreach ($files as $file) {
                                        if ($next_file_var > count($file_vars) - 1) {
                                            continue;
                                        }

                                        $file = $atp->logic2->get_files(array('ID' => $file['FILE_ID']));
                                        $file_path = $atp->_config['REAL_URL'] . $file['PATH'];
                                        if (is_file($file_path)) {
                                            $doc->setImageValue($file_vars[$next_file_var], $file_path);
                                            $next_file_var++;
                                        }
                                    }
                                }
                                $info['VALUE'] = '';
                            }
                        }
                    }
                }
            }

            $value = $info['VALUE'];

			if (is_file($info['VALUE']) === false) {
				if (strpos($field['VAR_NAME'], 'TI8') !== FALSE OR strpos($field['VAR_NAME'], 'TI9') !== FALSE OR strpos($field['VAR_NAME'], 'TI10') !== FALSE) {
					$from_value = $this->makeStringLTStructure($value, 'from');
					$doc->setValue(str_replace(array('$', '{', '}'), '', $field['VAR_NAME'].'_FROM'), $from_value);
//					$tmparrmy[str_replace(array('$', '{', '}'), '', $field['VAR_NAME'].'_FROM')] = $from_value;
				}
//				$tmparrmy[str_replace(array('$', '{', '}'), '', $field['VAR_NAME'])] = $value;
				$doc->setValue(str_replace(array('$', '{', '}'), '', $field['VAR_NAME']), $value);
			} else {
                $doc->setImageValue('image2.gif', $value);
            }
        }
		if (empty($ROIK) === FALSE) {
			$doc->setValue(str_replace(array('$', '{', '}'), '', '{$ROIK}'), $ROIK);
		}
		//for($i = $next_file_var; $i < count($file_vars); $i++) { $doc->removeImage($file_vars[$i]); }
        // Naujo failo WP katalogas
        $path['atp']['dir'] = 'files/documents/' . date('Y/m/d') . '/' . $record['ID'] . '/';
        $path['wp']['dir'] = 'subsystems/atp/' . $path['atp']['dir'];
        $path['real']['dir'] = $atp->config['GLOBAL_REAL_URL'] . $path['wp']['dir'];
        // Jei neegzistuoja - sukuria
        if (is_dir($path['real']['dir']) === false) {
            $oldumask = umask(0);
            mkdir($path['real']['dir'], 0777, true);
            umask($oldumask);
        }

        // Šablono plėtinys
        $extension = pathinfo($template->filename, PATHINFO_EXTENSION);

        // Naujo failo pavadinimas
        $slug = new \Slug('0123456789-_()[].');
        $i = 0;
        do {
            if ($i > 50) {
                $new_name = md5(time() . rand(0, 10000)) . '.' . $extension;
                break;
            }
            $i++;

            $new_name = $template->label . '_[' . substr(md5(microtime()), 0, 5) . ']';
            //$new_name = $template->label . '_[' . $record['RECORD_ID'] . '-' . $record['ID'] . '][' . date('YmdHis') . '][' . rand(0, 100) . ']';
            ////$new_name = $template->label . '_[' . $record['RECORD_ID'] . '-' . $record['ID'] . '][' . date('YmdHis') . '][' . rand(0, 100) . ']';
            $new_name = $slug->makeSlugs($new_name) . '.' . $extension;
        } while (is_file($path['real']['dir'] . $new_name) === true);

        $path['atp']['file'] = $path['atp']['dir'] . $new_name;
        $path['wp']['file'] = $path['wp']['dir'] . $new_name;
        $path['real']['file'] = $path['real']['dir'] . $new_name;

        // Išsaugo dokumentą
        $oldumask = umask(0);
        // TODO: saugoti tarp laikinų failų? // $new_file_path = $doc->Save();
        $doc->saveAs($path['real']['file']);
        umask($oldumask);


        // Išsaugo sugeneruoto failo informaciją
        $file_row = array(
            'TYPE' => (new \finfo(FILEINFO_MIME_TYPE))->file($path['real']['file']),
            'SIZE' => filesize($path['real']['file']),
            'NAME' => $new_name,
            'PATH' => $path['atp']['file']
        );
        $fiel_new_id = $atp->db_quickInsert(PREFIX . 'ATP_FILES', $file_row);
        $document_info_row = array(
            'FILE_ID' => $fiel_new_id,
            'RECORD_ID' => (int) $record['ID'],
            'TEMPLATE_ID' => (int) $template->id
        );
        $atp->db_quickInsert(PREFIX . 'ATP_RECORD_DOCUMENTS', $document_info_row);


        /* $r = array(
          'filename' => $new_name,
          'sitepath' => $atp->config['GLOBAL_SITE_URL'] . $path['wp']['file'],
          'realpath' => $path['real']['file']
          );
          return $r; */
        return $path['real']['file'];
    }
}
