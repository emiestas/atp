<?php

namespace Atp\Document\Templates\Template;

/**
 * Šablono iš skripto handler'is
 */
class ScriptHandler extends AbstractHandler
{
    /**
     * Sukurti skriptai
     * @var array
     */
    private $scripts = null;

    /**
     * @var \Atp\Core
     */
    private $atp;

    /**
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;
    }

    /**
     * Šablono saugojimas
     * @param \stdClass $param
     * @param int $param->id Šablono ID
     * @param string $param->script Skripto ID
     * @return bool
     */
    public function Save(\stdClass $param)
    {
        if (empty($param->script)) {
            return false;
            //throw new \Exception('Nenurodytas skriptas');
        }

        $script = $this->get_script($param->script);
        if ($script === false) {
            return false;
            //throw new \Exception('Nerastas skriptas');
        }

        // Sukuriamas ryšys tarp skripto ir failo
        $qry = 'INSERT INTO ' . PREFIX . 'ATP_TMPL_X_SCRIPT (TMPL_ID, SCRIPT_NAME) VALUES (:ID, :SCRIPT_NAME)';
        $this->atp->db_query_fast($qry, array(
            'ID' => $param->id,
            'SCRIPT_NAME' => $script->name
        ));

        return true;
    }

    /**
     * Šablono pašalinimas
     * @param int $id Šablono ID
     * @return bool
     */
    public function Delete($id)
    {
        $param = array('ID' => $id);

        // Pašalinamas ryšys tarp skripto ir failo
        $this->atp->db_query_fast('DELETE FROM ' . PREFIX . 'ATP_TMPL_X_SCRIPT WHERE TMPL_ID = :ID', $param);

        // Tikrina ar ištrinta
        $result = $this->atp->db_query_fast('SELECT COUNT(ID) CNT FROM ' . PREFIX . 'ATP_TMPL_X_SCRIPT WHERE TMPL_ID = :ID', $param);
        return (bool) (int) $this->atp->db_next($result)['CNT'];
    }

    /**
     * Handler'io duomenys
     * @param int $id Šablono ID
     * @return array
     */
    public function GetData($id)
    {
        $data = $this->atp->logic2->get_db(PREFIX . 'ATP_TMPL_X_SCRIPT', array('TMPL_ID' => $id), true, null, false, null, array(), false);
        unset($data['ID'], $data['TMPL_ID']);

        return $data;
    }

    /**
     * Sugeneruoją dokumeno įrašo dokumentą ir gražina kelią iki jo.
     * @param int $record_id Dokumento įrašo ID
     * @param array $template_data Šablono duomenys
     * @return string Sugeneruoto failo kelias serveryje
     * @throws \Exception
     */
    public function GenerateTemplate($record_id, $template_data)
    {
        $data = $this->getData($template_data['ID']);
        $script = $this->get_script($data['SCRIPT_NAME']);

        $file = $script->object->GenerateTemplate($record_id);

        if (is_file($file) === false) {
            throw new \Exception('Nepavyko sugeneruoti failo');
        }


        // Naujo failo WP katalogas
        $path['atp']['dir'] = 'files/documents/' . date('Y/m/d') . '/' . $record_id . '/';
        $path['wp']['dir'] = 'subsystems/atp/' . $path['atp']['dir'];
        $path['real']['dir'] = $this->atp->config['GLOBAL_REAL_URL'] . $path['wp']['dir'];
        // Jei neegzistuoja - sukuria
        if (is_dir($path['real']['dir']) === false) {
            $oldumask = umask(0);
            mkdir($path['real']['dir'], 0777, true);
            umask($oldumask);
        }

        // Šablono plėtinys
        $extension = 'docx';

        // Naujo failo pavadinimas
        $slug = new \Slug('0123456789-_()[].');
        $i = 0;
        do {
            if ($i > 50) {
                $new_name = md5(time() . rand(0, 10000)) . '.' . $extension;
                break;
            }
            $i++;

            $new_name = $template_data['LABEL'] . '_[' . substr(md5(microtime()), 0, 5) . ']';
            $new_name = $slug->makeSlugs($new_name) . '.' . $extension;
        } while (is_file($path['real']['dir'] . $new_name) === true);

        $path['atp']['file'] = $path['atp']['dir'] . $new_name;
        $path['wp']['file'] = $path['wp']['dir'] . $new_name;
        $path['real']['file'] = $path['real']['dir'] . $new_name;

        // Išsaugo dokumentą
        $oldumask = umask(0);
        rename($file, $path['real']['file']);
        umask($oldumask);


        // Išsaugo sugeneruoto failo informaciją
        $file_row = array(
            'TYPE' => (new \finfo(FILEINFO_MIME_TYPE))->file($path['real']['file']),
            'SIZE' => filesize($path['real']['file']),
            'NAME' => $new_name,
            'PATH' => $path['atp']['file']
        );
        $fiel_new_id = $this->atp->db_quickInsert(PREFIX . 'ATP_FILES', $file_row);
        $document_info_row = array(
            'FILE_ID' => $fiel_new_id,
            'RECORD_ID' => (int) $record_id,
            'TEMPLATE_ID' => (int) $template_data['ID']
        );
        $this->atp->db_quickInsert(PREFIX . 'ATP_RECORD_DOCUMENTS', $document_info_row);

        return $path['real']['file'];
    }

    /**
     * Sukurti skriptai
     * @return array
     */
    public function get_scripts()
    {
        if ((bool) $this->scripts === true) {
            return $this->scripts;
        }

        $this->scripts = new \stdClass();

        // Užkraunami visi skriptų failai
        foreach (glob(dirname(__FILE__) . '/Script/*.php') as $file) {
            require_once($file);
        }

        // Nustatomas skriptų namespace
        $script_namespase = (new \ReflectionClass(__CLASS__))->getNamespaceName() . '\Script';

        // Tikrinama ar tarp užkrautų klasių yra šablono skriptų klasių
        $classes = get_declared_classes();
        foreach ($classes as $class) {
            $reflect = new \ReflectionClass($class);
            $namespace = $reflect->getNamespaceName();
            if (strpos($namespace, $script_namespase) !== 0 || $reflect->isAbstract()) {
                continue;
            }

            if ($reflect->implementsInterface($script_namespase . '\IScript') === false) {
                continue;
            }

            // Gaunam skripto kalsės informacija
            $script = $reflect->newInstance($this->atp);
            $name = $script->GetName();
            $this->scripts->$name = (object) array(
                    'name' => $script->GetName(),
                    'label' => $script->GetLabel(),
                    'object' => $script
            );
        }

        return $this->scripts;
    }

    /**
     * Suranda skriptą pagal <i>ID</i> arba <i>NAME</i>
     * @note: Tokio kaip skripto <i>ID</i> nebėra, palieku jei kada bus sugalvoja sukurtų skriptų informaciją sudėti į DB
     * @param int|string $id Identifikatorius (Skripto <i>ID</i> arba <i>NAME</i>)
     * @return boolean|array
     */
    public function get_script($id)
    {
        $scripts = $this->get_scripts();

        $count = count($scripts);
        if ($count) {
            if (ctype_digit((string) $id) === true) {
                $id = (int) $id;
            }

            foreach ($scripts as &$script) {
                if ((int) $script->id === $id || $script->name === $id) {
                    return $script;
                }
            }
        }

        return false;
    }
}
