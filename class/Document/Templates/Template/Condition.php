<?php

/**
 * Gauti informacijai apie dokumento šablono sąlygas
 */
class Atp_Document_Templates_Template_Condition
{
    /**
     * @var dbo
     */
    private $db;

    /**
     * @var \Atp\Core
     */
    private $core;

    /**
     * Šablono informacija
     * @var Atp_Document_Templates_Template
     */
    private $template;

    /**
     * Šablono sąlygos
     * @var $conditions
     */
    public $conditions;

    /**
     *
     * @param Atp_Document_Templates_Template $template
     * @param \Atp\Core $core
     * @param dbo $db
     */
    public function __construct(Atp_Document_Templates_Template &$template, \Atp\Core &$core, dbo &$db = null)
    {
        $this->template = &$template;
        $this->core = &$core;
        if ($db === null) {
            $this->db = &$this->core;
        } else {
            $this->db = &$db;
        }
    }

    /**
     *
     * @param int $condition_id
     * @return boolean|object
     */
    public function get($condition_id)
    {
        $qry = 'SELECT A.*
			FROM
				' . PREFIX . 'ATP_TMPL_CONDITION A
			WHERE
				A.ID = :ID';
        $result = $this->db->db_query_fast($qry, array('ID' => $condition_id));
        $count = $this->db->db_num_rows($result);

        if ($count) {
            $row = $this->db->db_next($result);

            return $this->conditions[$row['ID']] = (object) array(
                    'id' => $row['ID'],
                    'action_id' => $row['ACTION_ID'],
            );
        }

        return false;
    }

    /**
     *
     * @return array Objektų masyvas
     */
    public function get_by_template()
    {
        $qry = 'SELECT B.*
			FROM
				' . PREFIX . 'ATP_TMPL_X_TMPL_CONDITION A
					INNER JOIN ' . PREFIX . 'ATP_TMPL_CONDITION B ON B.ID = A.CONDITION_ID
			WHERE
				A.TMPL_ID = :ID';
        $template = $this->template->get();
        $result = $this->db->db_query_fast($qry, array('ID' => $template->id));
        $count = $this->db->db_num_rows($result);

        $conditions = array();
        for ($i = 0; $i < $count; $i++) {
            $row = $this->db->db_next($result);

            $conditions[$row['ID']] = (object) array(
                    'id' => $row['ID'],
                    'action_id' => $row['ACTION_ID'],
            );
        }

        return $this->conditions = &$conditions;
    }

    /**
     *
     * @param type $condition_id
     * @return type
     */
    public function get_parameters($condition_id)
    {
        $qry = 'SELECT C.*
			FROM
				' . PREFIX . 'ATP_TMPL_CONDITION A
					INNER JOIN ' . PREFIX . 'ATP_TMPL_CONDITION_X_CONDITION_PRAM B ON B.CONDITION_ID = A.ID
					INNER JOIN ' . PREFIX . 'ATP_TMPL_CONDITION_PARAM C ON C.ID = B.PARAMETER_ID
			WHERE
				A.ID = :CONDITION_ID';

        $template = $this->template->get();
        $result = $this->db->db_query_fast($qry, array(
            'CONDITION_ID' => $condition_id
        ));
        $count = $this->db->db_num_rows($result);

        $conditions = array();
        for ($i = 0; $i < $count; $i++) {
            $row = $this->db->db_next($result);
            $parameter[] = (object) array(
                    'id' => $row['ACTION_PARAMETER_ID'],
                    'type' => $row['COMPARISON_TYPE'],
                    'value' => $row['VALUE'],
            );
        }

        return $parameter;
    }

    public function delete($condition_id)
    {
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_DOCUMENT')) {
            trigger_error('Neturite teisės redaguoti šablonų');
            return false;
        }

        $template = $this->template->get();
        
        $adminJournal = $this->core->factory->AdminJournal();
        $adminJournal->addRecord(
            $adminJournal->getDeleteRecord(
                'Administravime, dokumento šablono redagavimo formoje, sąlygą'
                . ' (Dokumentas "' . $this->core->factory->Document()->Get($template->document_id)->getLabel() . '";'
                . ' Šablonas "' . $template->label . '";'
                . ' Sąlygos parametrai: ' . json_encode($this->get_parameters($condition_id)) . ')'
            )
        );


        $qry = 'DELETE A, B, C, D
			FROM
				' . PREFIX . 'ATP_TMPL_CONDITION A
					LEFT JOIN ' . PREFIX . 'ATP_TMPL_X_TMPL_CONDITION B ON B.CONDITION_ID = A.ID
					LEFT JOIN ' . PREFIX . 'ATP_TMPL_CONDITION_X_CONDITION_PRAM C ON C.CONDITION_ID = A.ID
					LEFT JOIN ' . PREFIX . 'ATP_TMPL_CONDITION_PARAM D ON D.ID = C.PARAMETER_ID
			WHERE A.ID = :ID';

        $this->db->db_query_fast($qry, array('ID' => $condition_id));

        return true;
    }
}
