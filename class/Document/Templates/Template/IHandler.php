<?php

namespace Atp\Document\Templates\Template;

/**
 * Šablono handler'io interfeisas
 */
interface IHandler
{
    /**
     * Šablono saugojimas
     * @param \stdClass $param
     * @param int $param->id Šablono ID
     * @return bool
     */
    public function Save(\stdClass $param);

    /**
     * Šablono pašalinimas
     * @param int $id Šablono ID
     * @return bool
     */
    public function Delete($id);

    /**
     * Handler'io duomenys
     * @param int $id Šablono ID
     * @return array
     */
    public function GetData($id);

    /**
     * Sugeneruoją dokumeno įrašo dokumentą ir gražina kelią iki jo.
     * @param int $record_id Dokumento įrašo ID
     * @param array $template_data Šablono duomenys
     * @return string Sugeneruoto failo kelias serveryje
     */
    public function GenerateTemplate($record_id, $template_data);

    /**
     * Handler'io pavadinimas
     * @return string
     */
    public function GetName();
}
