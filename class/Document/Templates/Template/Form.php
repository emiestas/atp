<?php

/**
 * Šablono informacijos forma (Dokumentų šablonai)
 */
class Atp_Document_Templates_Template_Form
{
    /**
     * Šablono informacija
     * @var Atp_Document_Templates_Template_Info
     */
    private $template;

    /**
     *
     * @var \Atp\Core
     */
    private $core;

    /**
     *
     * @param Atp_Document_Templates_Template $template
     * @param \Atp\Core $core
     */
    public function __construct(Atp_Document_Templates_Template &$template, \Atp\Core &$core)
    {
        $this->template = &$template;
        $this->core = &$core;
    }

    /**
     * Šablono informacijos forma (Dokumentų šablonai)
     * @return string HTML
     */
    public function printForm()
    {
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_DOCUMENT')) {
            return $this->core->printForbidden();
        } elseif (isset($_POST['submit']) === true) {
            $this->saveForm();
            exit;
        }

        $template = $this->template->get();

        // Dokumento informacija
//        $document = $this->core->logic2->get_document($template->document_id);
        $document = $this->core->factory->Document()->Get($template->document_id);


        $adminJournal = $this->core->factory->AdminJournal();
        $adminJournal->addRecord(
            $adminJournal->getVisitRecord(
                'Administravime, dokumento šablono redagavimo forma'
                . ' (Dokumentas "' . $document->getLabel() . '";'
                . ' Šablonas "' . $template->label . '").'
            )
        );


        $tmpl = $this->core->factory->Template();
        $tmpl->handle = 'documents_template_form_file';
        $tmpl->set_file($tmpl->handle, 'documents_template_form.tpl');


        //$this->core->_load_files['js'][] = 'jquery.cascadingdropdown.js';
        // Blokai
        $handles = $tmpl->set_multiblock($tmpl->handle, array(
            array(
                array(
                    array(
                        'condition_field_option'),
                    'select',
                    'html_block'),
                'condition'),
            'form' // Forma
        ));

        // Priskirta paslauga
        $servicesManager = new \Atp_Document_Templates_Services($this->core);
        $service = $servicesManager->getFromTemplate($template->id);
        $tmpl->set_var(array(
            'service_id' => $service->id,
            'service_name' => $service->label,
            'service_fields_table' => $this->getServiceFieldsTable($service->id)
        ));

        //$servicesManager->updateTemplateService($this->template);
        // Veiksmai
        $actionsManager = new \Atp_Document_Templates_Template_Condition_Action($this->template, $this->core);
        $actions = $actionsManager->get_actions();


        // Sąlygos
        $conditionManager = new \Atp_Document_Templates_Template_Condition($this->template, $this->core);
        $conditions = $conditionManager->get_by_template();


        // Pavyzdys JS'ui // Sąlygos tipo pasirinkimas
        $tmpl->set_var(array(
            'class' => ' action',
            'value' => '',
            'label' => '-- ' . 'Sąlygos tipas' . ' --',
            'attr' => ''
        ));
        $tmpl->parse($handles['condition_field_option'], 'condition_field_option', false);
        foreach ($actions as $action) {
            $tmpl->set_var(array(
                'value' => $action['ID'],
                'label' => $action['LABEL'],
                'attr' => (int) $action['ID'] === (int) $condition->action_id ? ' selected="selected"' : ''
            ));
            $tmpl->parse($handles['condition_field_option'], 'condition_field_option', true);
        }
        $tmpl->set_var(array(
            'id' => '{js@condition_index}',
            'i' => 'TYPE',
            'title' => 'Sąlygos tipas',
            //'select_attr' => ' disabled="disabled"',
            'select_attr' => '',
            $handles['html_block'] => ''
        ));
        $tmpl->parse($handles['select'], 'select', false);
        $tmpl->set_var('append_condition_class', ' sample');
        $tmpl->parse($handles['condition'], 'condition', true);

        // Spausdinamos sąlygos
        if (count($conditions)) {
            foreach ($conditions as &$condition) {
                // Sąlygos tipo pasirinkimas
                $tmpl->set_var(array(
                    'class' => ' action',
                    'value' => '',
                    'label' => '-- ' . 'Sąlygos tipas' . ' --',
                    'attr' => ''
                ));
                $tmpl->parse($handles['condition_field_option'], 'condition_field_option', false);
                foreach ($actions as $action) {
                    $tmpl->set_var(array(
                        'value' => $action['ID'],
                        'label' => $action['LABEL'],
                        'attr' => (int) $action['ID'] === (int) $condition->action_id ? ' selected="selected"' : ''
                    ));
                    $tmpl->parse($handles['condition_field_option'], 'condition_field_option', true);
                }
                $tmpl->set_var(array(
                    'id' => $condition->id,
                    'i' => 'TYPE',
                    'title' => 'Sąlygos tipas',
                    'select_attr' => '',
                    $handles['html_block'] => ''
                ));
                $tmpl->parse($handles['select'], 'select', false);

                if (empty($condition->action_id) === false) {
                    // Sąlygos veiksmo parametrai
                    $parameters = $actionsManager->get_parameters($condition->action_id);
                    // Sąlygos veiksmo parametrų reikšmės
                    $parameters_values = $conditionManager->get_parameters($condition->id);
                    $actionsManager->set_values($parameters_values);

                    // Sąlygos veiksmo parametrų laukai
                    $append = false;
                    foreach ($parameters as &$parameter) {
                        $html = $actionsManager->parse_parameter_field($parameter['ID'], $condition->id);

                        $tmpl->set_var('html', $html);
                        $tmpl->parse($handles['html_block'], 'html_block', $append);
                        $append = true;
                    }
                }

                $tmpl->set_var('append_condition_class', '');
                $tmpl->parse($handles['condition'], 'condition', true);
            }
        } else {
            $tmpl->set_var(array(
                'append_condition_class' => '',
                $handles['html_block'] => '',
                $handles['select'] => ''
            ));
            $tmpl->parse($handles['condition'], 'condition', true);
        }

        // Pagrindiniai puslapio duomenys
        $tmpl->set_var(array(
            'title' => $document->getLabel() . ': ' . $template->label,
            'sys_message' => $this->core->get_messages(),
            'url' => 'index.php?m=131&id=' . $template->id,
            'template_id' => $template->id,
            'save' => $this->core->lng('save'),
        ));
        $tmpl->parse($handles['form'], 'form');


        $tmpl->parse($tmpl->handle . '_out', $tmpl->handle);
        $output = $tmpl->get($tmpl->handle . '_out');
        return $this->core->atp_content($output);
    }

    /**
     *
     * @return string
     */
    public function printAjax()
    {
        $rq = array(
            'template_id' => $_POST['template'],
            'condition_id' => $_POST['condition'],
            'action_id' => $_POST['action'],
            'parameters' => $_POST['parameters']
        );

        $parameters_values = array();
        if (count($rq['parameters'])) {
            foreach ($rq['parameters'] as $key => &$val) {
                $parameters_values[] = (object) array(
                        'id' => $key,
                        'type' => 'IS',
                        'value' => $val,
                );
            }
        }

        // Šablono informacija
        $template = $this->template->get();

        // Sąlygos informacija
        $conditionManager = new \Atp_Document_Templates_Template_Condition($this->template, $this->core);
        $condition = $conditionManager->get($rq['condition_id']);

        if ($condition !== false) {
            // Priskiriamas veiksmas
            $actionsManager = new \Atp_Document_Templates_Template_Condition_Action($this->template, $this->core);
            $result = $actionsManager->set($rq['action_id']);

            if ($result !== false) {
                // Veiksmo parametrai
                $parameters = $actionsManager->get_parameters();
                $actionsManager->set_values($parameters_values);


                $tmpl = $this->core->factory->Template();
                $tmpl->handle = 'documents_template_form_file';
                $tmpl->set_file($tmpl->handle, 'documents_template_form.tpl');


                // Parametro blokas
                $handles = $tmpl->set_multiblock($tmpl->handle, array('html_block'));


                // Sąlygos veiksmo parametrų laukai
                $append = false;
                foreach ($parameters as &$parameter) {
                    $no_need = false;
                    foreach ($parameters_values as $parameter_value) {
                        if ($parameter_value->id === $parameter['ID']) {
                            $no_need = true;
                            break;
                        }
                    }
                    if ($no_need === true) {
                        continue;
                    }

                    $html = $actionsManager->parse_parameter_field($parameter['ID'], $condition->id);

                    $tmpl->set_var('html', $html);
                    $tmpl->parse($handles['html_block'], 'html_block', $append);
                    $append = true;
                }

                $output = $tmpl->get($handles['html_block']);

                $output = $this->core->atp_content(trim($output));
            }
        } else {
            $new_id = $this->saveChanges($rq, $this->template);
            return json_encode(array('new_id' => $new_id));
        }

        $this->saveChanges($rq, $this->template, $conditionManager, $actionsManager);

        return json_encode(array('html' => $output));
    }

    /**
     *
     * @param array $rq
     * @param Atp_Document_Templates_Template $template
     * @param Atp_Document_Templates_Template_Condition $conditionManager [optional]
     * @param Atp_Document_Templates_Template_Condition_Action $actionsManager [optional]
     */
    private function saveChanges($rq, Atp_Document_Templates_Template &$template, Atp_Document_Templates_Template_Condition &$conditionManager = null, Atp_Document_Templates_Template_Condition_Action &$actionsManager = null)
    {

        // Šablono informacija
        $template = $template->get();


        $adminJournal = $this->core->factory->AdminJournal();
        $adminJournal->addRecord(
            $adminJournal->getSaveRecord(
                'Administravime, dokumento šablono redagavimo formoją'
                . ' (Dokumentas "' . $this->core->factory->Document()->Get($template->document_id)->getLabel() . '";'
                . ' Šablonas "' . $template->label . '")'
            )
        );


        // Jei tai nauja sąlyga, sukurti tuščią sąlygą
        if (empty($rq['condition_id']) === true) {
            $this->core->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_TMPL_CONDITION
						(`ACTION_ID`) VALUES (0)');
            $new_id = $this->core->db_last_id();
            $arg = array(
                'TMPL_ID' => $template->id,
                'CONDITION_ID' => $new_id
            );
            $this->core->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_TMPL_X_TMPL_CONDITION
						(`TMPL_ID`, `CONDITION_ID`) VALUES (:TMPL_ID, :CONDITION_ID)', $arg);

            return $new_id;
        } else {

            // Sąlygos informacija
            $condition = $conditionManager->get($rq['condition_id']);


            // Jei keičiasi veiksmas
            if ((int) $condition->action_id != (int) $rq['action_id']) {
                $arg = array(
                    'CONDITION_ID' => $condition->id,
                    'ACTION_ID' => $rq['action_id']
                );

                // Pašalinami seni parametrai
                $this->core->db_query_fast('DELETE A, B
				FROM
					' . PREFIX . 'ATP_TMPL_CONDITION_X_CONDITION_PRAM A
						LEFT JOIN ' . PREFIX . 'ATP_TMPL_CONDITION_PARAM B ON B.ID = A.PARAMETER_ID
				WHERE
					A.CONDITION_ID = :CONDITION_ID', $arg);

                // Atnaujiamas veiksmo ID
                $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_TMPL_CONDITION
				SET ACTION_ID = :ACTION_ID
				WHERE
					ID = :CONDITION_ID', $arg);
            } else {
                $arg = array(
                    'CONDITION_ID' => $condition->id
                );

                // Pašalinami seni parametrai
                $this->core->db_query_fast('DELETE A, B
				FROM
					' . PREFIX . 'ATP_TMPL_CONDITION_X_CONDITION_PRAM A
						LEFT JOIN ' . PREFIX . 'ATP_TMPL_CONDITION_PARAM B ON B.ID = A.PARAMETER_ID
				WHERE
					A.CONDITION_ID = :CONDITION_ID', array('CONDITION_ID' => $condition->id));

                // Sukuriami nauji parametrai
                $new_ids = array();
                foreach ($actionsManager->values[$rq['action_id']] as &$value) {
                    $this->core->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_TMPL_CONDITION_PARAM
						(`COMPARISON_TYPE`, `ACTION_PARAMETER_ID`, `VALUE`)
					VALUES
						(:COMPARISON_TYPE, :ACTION_PARAMETER_ID, :VALUE)', array(
                        'COMPARISON_TYPE' => $value->type,
                        'ACTION_PARAMETER_ID' => $value->id,
                        'VALUE' => $value->value
                    ));
                    $new_ids[] = $this->core->db_last_id();
                }

                // Sukuriami naujų parametrų ryšiai
                if (count($new_ids)) {
                    $values = array();
                    foreach ($new_ids as &$new_id) {
                        $values[] = '(' . (int) $condition->id . ', ' . (int) $new_id . ')';
                    }
                    $this->core->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_TMPL_CONDITION_X_CONDITION_PRAM
						(`CONDITION_ID`, `PARAMETER_ID`)
					VALUES ' . join(',', $values));
                }
            }
        }
    }

    public function saveService()
    {
        if (empty($_POST['service'])) {
            throw new \Exception('Service ID is not set');
        }

        $template = $this->template->get();


        $adminJournal = $this->core->factory->AdminJournal();
        $adminJournal->addRecord(
            $adminJournal->getSaveRecord(
                'Administravime, dokumento šablono redagavimo formoje, paslaugą'
                . ' (Dokumentas "' . $this->core->factory->Document()->Get($template->document_id)->getLabel() . '";'
                . ' Šablonas "' . $template->label . '";'
                . ' Paslauga "' . (int) $_POST['service'] . '").'
            )
        );
        

        $services = new \Atp_Document_Templates_Services($this->core);
        $services->addToTemplate($template->id, (int) $_POST['service']);

        return true;
    }

    public function saveServiceField($param)
    {
        if (isset($param['service']) === false || isset($param['field']) === false || isset($param['avilys_field']) === false) {
            return false;
        }

        $template = $this->template->get();


        $adminJournal = $this->core->factory->AdminJournal();
        $adminJournal->addRecord(
            $adminJournal->getSaveRecord(
                'Administravime, dokumento šablono redagavimo formoje, paslaugos laukų ryšiai'
                . ' (Dokumentas "' . $this->core->factory->Document()->Get($template->document_id)->getLabel() . '";'
                . ' Šablonas "' . $template->label . '").'
            )
        );
        

        $arg = array(
            'TEMPALTE_ID' => (int) $template->id,
            'DOCUMENT_ID' => (int) $template->document_id,
            'SERVICE_ID' => (int) $param['service'],
            'FIELD_ID' => null,
            'FIELD_VARNAME' => $param['field'],
            'FIELD_VARPATH' => null,
            'AVILYS_FIELD_ID' => null,
            'AVILYS_FIELD_NAME' => $param['avilys_field']
        );


        // Avilio lauko ID ATP sistemoje
        $qry = 'SELECT ID FROM ' . PREFIX . 'ATP_TMPL_SERVICE_AVILYS_FIELDS WHERE NAME = :AVILYS_FIELD_NAME';
        $result = $this->core->db_query_fast($qry, $arg);
        $count = $this->core->db_num_rows($result);
        if ($count) {
            $row = $this->core->db_next($result);
            $arg['AVILYS_FIELD_ID'] = $row['ID'];
        } else {
            $this->core->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_TMPL_SERVICE_AVILYS_FIELDS (`NAME`) VALUES (:AVILYS_FIELD_NAME)', $arg);
            $arg['AVILYS_FIELD_ID'] = $this->core->db_last_id();
        }


        // Šablono lauko ID ATP sistemoje
        $qry = 'SELECT ID FROM ' . PREFIX . 'ATP_TMPL_VARFIELDS WHERE FIELD_VARNAME = :FIELD_VARNAME AND DOCUMENT_ID = :DOCUMENT_ID';
        $result = $this->core->db_query_fast($qry, $arg);
        $count = $this->core->db_num_rows($result);
        if ($count) {
            $row = $this->core->db_next($result);
            $arg['FIELD_ID'] = $row['ID'];
        } else {
            $varpath = $this->core->logic->parse_variable($arg['FIELD_VARNAME']);
            $field = $this->core->logic->get_field_info_by_varpath($varpath, $template->document_id);

            $arg['FIELD_ID'] = $field['ID'];
            $arg['FIELD_VARPATH'] = json_encode($varpath);

            $qry = 'INSERT INTO ' . PREFIX . 'ATP_TMPL_VARFIELDS (`DOCUMENT_ID`, `FIELD_ID`, `FIELD_VARNAME`, `FIELD_VARPATH`)
				VALUES
					(:DOCUMENT_ID, :FIELD_ID, :FIELD_VARNAME, :FIELD_VARPATH)';
            $this->core->db_query_fast($qry, $arg);
            $arg['FIELD_ID'] = $this->core->db_last_id();
        }

        if (empty($arg['AVILYS_FIELD_ID']) || empty($arg['FIELD_ID'])) {
            return false;
        }

        $qry = 'SELECT COUNT(ID) CNT
			FROM ' . PREFIX . 'ATP_TMPL_SERVICE_AVILYS_FIELDS_X_VARFIELDS_REL
			WHERE
				TEMPLATE_ID = :TEMPALTE_ID AND
				AVILYS_FIELD_ID = :AVILYS_FIELD_ID AND
				TEMPLATE_VAR_ID = :FIELD_ID';
        $result = $this->core->db_query_fast($qry, $arg);
        $row = $this->core->db_next($result);

        if ((int) $row['CNT'] === 0) {
            $qry = 'INSERT INTO ' . PREFIX . 'ATP_TMPL_SERVICE_AVILYS_FIELDS_X_VARFIELDS_REL (`AVILYS_FIELD_ID`, `TEMPLATE_VAR_ID`, `TEMPLATE_ID`)
					VALUES
						(:AVILYS_FIELD_ID, :FIELD_ID, :TEMPALTE_ID)
					ON DUPLICATE KEY UPDATE
						`AVILYS_FIELD_ID` = VALUES(`AVILYS_FIELD_ID`),
						`TEMPLATE_VAR_ID` = VALUES(`TEMPLATE_VAR_ID`),
						`TEMPLATE_ID` = VALUES(`TEMPLATE_ID`)';
            $result = $this->core->db_query_fast($qry, $arg);

            if (empty($this->core->db_last_id())) {
                return false;
            }
        }

        return true;
    }

    public function getServiceFieldsTable($service_id)
    {
        // Lentelės šablonas
        $tpl_arr = $this->core->getTemplate('table');

        // Lentelė
        $html = $this->core->returnHTML(array('table_class' => 'fields_table'), $tpl_arr[0]);

        // Lentelės antraštė
        $html .= $this->core->returnHTML(array('row_html' => '', 'row_class' => 'div-row div-header'), $tpl_arr[1]);

        // Lentelės stulpeliai
        $columns = array(
            'AVILYS_FIELD' => 'Avilio laukas',
            'DOC_FIELD' => 'Dokumento laukas'
        );
        foreach ($columns as $col_name => $col) {
            // Lentelės langelis
            $html .= $this->core->returnHTML(array(
                'cell_html' => '',
                'cell_class' => 'div-cell cell-' . $col_name,
                'cell_content' => $col), $tpl_arr[2]);
        }
        $html .= $tpl_arr[3];

        // Laukų pasirinkiams
        $template = $this->template->get();

        $document = $this->core->logic2->get_document($template->document_id);
        $data = array(
            'table_name' => $document['LABEL'],
            'options' => true,
            'options_value_use_name' => true
        );
        $all_fields = $this->core->manage->document_fields($document['ID'], 'DOC', $data);

        $pattern = array(
            'level' => '/class=\".*?level([0-9]+)\"/i',
            'value' => '/value=\"(.*?)\"/i',
            'label' => '#<option.*?>(.*?)</option>#i',
            'replace_from' => '#<option(.*?)>(.*?)</option>#',
            'replace_to' => '<option$1>[space]$2</option>'
        );
        foreach ($all_fields as &$option) {
            if (strpos($option, 'level') === false) {
                continue;
            }

            if (preg_match($pattern['level'], $option, $matches)) {
                $level = (int) $matches[1];
                if (preg_match($pattern['label'], $option, $matches)) {
                    $label = $matches[1];
                    $replace_to = str_replace('[space]', str_repeat('&nbsp;', $level * 2), $pattern['replace_to']);
                    $option = preg_replace($pattern['replace_from'], $replace_to, $option, -1, $count);
                }
            }
        }
        /* foreach ($all_fields as $key => $option) {
          $new_key = $key;
          if (preg_match($pattern['value'], $option, $matches)) {
          if(empty($matches[1]) === FALSE && is_numeric((string) $matches[1]) === FALSE) {
          $new_key = $matches[1];
          }
          }

          //$new_option = $option;
          if (strpos($option, 'level') !== FALSE) {
          if (preg_match($pattern['level'], $option, $matches)) {
          $level = (int) $matches[1];
          if (preg_match($pattern['label'], $option, $matches)) {
          $label = $matches[1];
          $replace_to = str_replace('[space]', str_repeat('&nbsp;', $level * 2), $pattern['replace_to']);

          $new_option = preg_replace($pattern['replace_from'], $replace_to, $option, -1, $count);
          }
          }
          }

          unset($all_fields[$key]);
          $all_fields[$new_key] = $new_option;
          } */
        array_unshift($all_fields, '<option value="">-- nepasirinkta --</option>');
        $options = join('', $all_fields);

        // Avilio laukai
        $servicesManager = new \Atp_Document_Templates_Services($this->core);
        $avilys_field_names = $servicesManager->getAvilysNames($service_id);

        $qry = 'SELECT B.NAME AVILYS_NAME, C.FIELD_VARNAME
			FROM
				' . PREFIX . 'ATP_TMPL_SERVICE_AVILYS_FIELDS_X_VARFIELDS_REL A
					INNER JOIN ' . PREFIX . 'ATP_TMPL_SERVICE_AVILYS_FIELDS B ON B.ID = A.AVILYS_FIELD_ID
					INNER JOIN ' . PREFIX . 'ATP_TMPL_VARFIELDS C ON C.ID = A.TEMPLATE_VAR_ID
			WHERE
				TEMPLATE_ID = :TEMPLATE_ID';
        $result = $this->core->db_query_fast($qry, array('TEMPLATE_ID' => $template->id));
        $count = $this->core->db_num_rows($result);
        $varnames_by_avilys_names = array();
        if ($count) {
            for ($i = 0; $i < $count; $i++) {
                $row = $this->core->db_next($result);
                $varnames_by_avilys_names[$row['AVILYS_NAME']] = $row['FIELD_VARNAME'];
            }
        }

        $data = array();
        foreach ($avilys_field_names as $key => $field) {
            $data[$key] = array(
                'AVILYS_FIELD' => $field,
                'DOC_FIELD' => $varnames_by_avilys_names[$field]
            );
        }

        // Lentelės įrašų eilutės
        foreach ($data as $key => $record) {
            // Lentelės eilutė
            $row = array();
            $row['row_html'] = '';
            $row['row_class'][] = 'div-row';
            $row['row_class'] = join(' ', $row['row_class']);
            $html .= $this->core->returnHTML($row, $tpl_arr[1]);

            // Eilutės stulpeiai
            foreach ($columns as $col_name => $col) {
                // Stulpelio duomenys
                $cell = array(
                    'cell_html' => '',
                    'cell_content' => $record[$col_name],
                    'cell_class' => 'div-cell cell-' . $col_name);

                if ($col_name === 'DOC_FIELD') {
                    $replace_from = 'value="' . $record['DOC_FIELD'] . '"';
                    $options_selected = str_replace($replace_from, $replace_from . ' selected="selected"', $options);
                    $cell['cell_content'] = '<select name="' . htmlspecialchars($record['AVILYS_FIELD']) . '">' . $options_selected . '</select>';
                    /* $options_selected = $all_fields;
                      $options_selected[$record['DOC_FIELD']] = str_replace('<option ', ' <option selected="selected"', $options_selected[$record['DOC_FIELD']]);
                      $cell['cell_content'] = '<select name="' . htmlspecialchars($record['AVILYS_FIELD']) . '">' . join('', $options_selected) . '</select>'; */
                }

                $html .= $this->core->returnHTML($cell, $tpl_arr[2]);
            }

            $html .= $tpl_arr[3];
        }

        // Lentelės pabaiga
        $html .= $tpl_arr[4];

        return $html;
    }
}

/*
CREATE TABLE IF NOT EXISTS `TST_ATP_TMPL_SERVICE_AVILYS_FIELDS` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) NOT NULL COMMENT 'Avilio lauko kintamasis',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Avilio laukų kintamieji';
CREATE TABLE IF NOT EXISTS `TST_ATP_TMPL_VARFIELDS` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DOCUMENT_ID` int(10) unsigned NOT NULL COMMENT 'Dokumento ID',
  `FIELD_ID` int(10) unsigned DEFAULT NULL COMMENT 'Lauko ID',
  `FIELD_VARNAME` varchar(100) NOT NULL COMMENT 'Šablono kintamojo pavadinimas',
  `FIELD_VARPATH` varchar(300) NOT NULL COMMENT 'Šablono kintamojo kelias',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Šablono kintamojo informacija';
CREATE TABLE IF NOT EXISTS `TST_ATP_TMPL_SERVICE_AVILYS_FIELDS_X_VARFIELDS_REL` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AVILYS_FIELD_ID` int(10) unsigned NOT NULL COMMENT 'ATP_TMPL_SERVICE_AVILYS_FIELDS.ID',
  `TEMPLATE_ID` int(10) unsigned NOT NULL,
  `TEMPLATE_VAR_ID` int(10) unsigned NOT NULL COMMENT 'ATP_TMPL_VARFIELDS.ID',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `AVILYS_FIELD_ID` (`AVILYS_FIELD_ID`,`TEMPLATE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Avilio laukų ryšia su dokumentų šablonų kintamaisias';
 */
