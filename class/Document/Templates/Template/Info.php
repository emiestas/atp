<?php

/**
 * Dokumento šablono informacijos talpinimui
 */
class Atp_Document_Templates_Template_Info
{
    public $id; // ID

    public $label; // Pavadinimas

    public $filename; // Šablono failo pavadinimas

    public $filepath; // Šablono failo kelis nuo WP katalogo

    public $document_id; // Šablono dokumento ID

    public $service_id; // Priskirtos paslaugos ID

    public $conditions = array(); // Šablono sąlygos

    /**
     *
     * @param Atp_Document_Templates_Template|Atp_Document_Templates_Template_Condition $object
     */

    public function __construct($object)
    {
        return $this->add($object);
    }

    public function add($object)
    {
        if ($object instanceof Atp_Document_Templates_Template) {
            $from = $object->template;
            $to = &$this;
            $exclude = array('conditions');

            foreach ($from as $key => $value) {
                if (array_key_exists($key, $to) && in_array($key, $exclude, true) === false) {
                    $to->$key = $value;
                }
            }
        } elseif ($object instanceof Atp_Document_Templates_Template_Condition) {
            $from = $object->conditions;
            $to = &$this->conditions;

            foreach ($from as $key => $value) {
                $to->$key = $value;
            }
        }

//        var_dump($this);
        return;
    }
}
