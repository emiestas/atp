<?php

class Atp_Document_Templates_Template_Condition_Action
{
    /**
     * Veiksmų klasių objektai
     * @var array
     */
    public $actions;

    /**
     * Einamojo veiksmo ID
     * @var int
     */
    public $current_id;

    /**
     * Galimi veiksmų parametrai
     * @var array
     */
    public $parameters;

    /**
     * Veiksmų parametrų reikšmės
     * @var array
     */
    public $values;

    /**
     * ATP core perdavimui į veiksmo klasę
     * @var \Atp\Core
     */
    public $core;

    /**
     * Šablono informacija perdavimui į veiksmo klasę
     * @var Atp_Document_Templates_Template
     */
    protected $template;

    /**
     *
     * @param Atp_Document_Templates_Template $template
     * @param \Atp\Core $core
     */
    public function __construct(Atp_Document_Templates_Template &$template, \Atp\Core &$core)
    {
        $this->core = &$core;
        $this->template = &$template;

        $this->action = null;
        $this->parameters = null;
        $this->values = null;
    }

    /**
     * Sukuria veiksmo objektą
     * @param int $action_id Veiksmo ID
     * @return boolean
     */
    public function set($action_id)
    {
        if (empty($action_id) === false) {
            $this->actions[$action_id] = null;

            $actions = $this->get_actions();

            foreach ($actions as &$action) {
                if ((int) $action['ID'] === (int) $action_id) {
                    $class = 'Atp_Document_Templates_Template_Condition_Action_' . $action['CLASS'];
                    $this->actions[$action_id] = new $class($this);
                    $this->current_id = $action_id;
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Gauna einamąjį arba nurodytą veiksmo objektą
     * @param int $action_id [optional] Veiksmo ID
     * @return boolen|array
     */
    public function get($action_id = null)
    {
        if (empty($action_id)) {
            $action_id = $this->current_id;
        }

        $result = $this->set($action_id);
        if ($result === false) {
            trigger_error('Action is not set', E_USER_ERROR);
            return;
        }

        return $this->actions[$action_id];
    }

    /**
     * Veiksmų sąrašas
     * @return array
     */
    public function get_actions()
    {
        $actions = array(
            array('ID' => 1, 'CLASS' => 'Classificator', 'LABEL' => 'Klasifikatorius'),
            array('ID' => 2, 'CLASS' => 'Clause', 'LABEL' => 'Straipsnis'),
            array('ID' => 3, 'CLASS' => 'Modified', 'LABEL' => 'Pasikeitė laukas'),
            array('ID' => 4, 'CLASS' => 'FromDocument', 'LABEL' => 'Sukurtas iš (dokumento)'),
            array('ID' => 5, 'CLASS' => 'WorkerStructure', 'LABEL' => 'Surašantis padalinys'),
            array('ID' => 6, 'CLASS' => 'Status', 'LABEL' => 'Būsena')
        );

        return $actions;
    }

    /**
     * Gauna veiksmo parametrų laukus
     * @param int $action_id [optional] Veiksmo ID
     * @return array
     */
    public function get_parameters($action_id = null)
    {
        if (empty($action_id)) {
            $action_id = $this->current_id;
        }

        $action = $this->get($action_id);
        if ($action === false) {
            trigger_error('Action is not set', E_USER_ERROR);
            return;
        }

        $this->parameters[$action_id] = $action->get_parameters();

        return $this->parameters[$action_id];
    }

    /**
     * Sąlygos parametrų reikšmes perduoda į veiksmo klasę
     * @param object $values
     */
    public function set_values($values, $action_id = null)
    {
        if (empty($action_id)) {
            $action_id = $this->current_id;
        }

        $action = $this->get($action_id);
        if ($action === false) {
            trigger_error('Action is not set', E_USER_ERROR);
            return;
        }

        $this->values[$action_id] = $values;
        $action->set_values($values);
    }

    /**
     *
     * @param type $parameters
     * @param \Atp\Document\Record $recordManager
     * @return null|boolen
     */
    public function check($parameters, \Atp\Document\Record $recordManager)
    {
        $action = $this->get();
        if ($action === false) {
            trigger_error('Action is not set', E_USER_ERROR);
            return;
        }

        if (count($parameters)) {
            $temp = array();
            foreach ($parameters as &$parameter) {
                $temp[$parameter->id] = $parameter;
            }
            $parameters = &$temp;
        }

        return $action->check($parameters, $recordManager);
    }

    /**
     *
     * @param int $parameter_id Veiksmo parametro ID
     * @param int $condition_index Parametro indeksas (Sąlygos ID)
     * @return boolean|string
     */
    public function parse_parameter_field($parameter_id, $condition_index)
    {
        if (empty($this->actions[$this->current_id]) || empty($this->parameters[$this->current_id])) {
            return false;
        }

        // Veiksmas
        $action = &$this->actions[$this->current_id];

        // Ieško parametro, kurį reikia išvesti
        $parameters = &$this->parameters[$this->current_id];
        $found = false;
        foreach ($parameters as &$parameter) {
            if ((int) $parameter['ID'] === (int) $parameter_id) {
                $found = true;
                break;
            }
        }
        if ($found === false) {
            return false;
        }

        // Ieško ar yra nurodyta parametro reikšmė
        $values = &$this->values[$this->current_id];
        $value = null;
        if (count($values)) {
            foreach ($values as &$val) {
                if ((int) $val->id === (int) $parameter_id) {
                    $value = $val;
                    break;
                }
            }
        }

        // Generuoja HTML'ą
        $html = '';
        switch ($parameter['TYPE']) {
            case 'SELECT':

                $attributes_arr = array('class' => array('atp-select', 'param', 'param[' . $parameter['ID'] . ']'));

                if (empty($parameter['PARENT_ID']) === false) {
                    $attributes_arr['class'][] = 'required[' . $parameter['PARENT_ID'] . ']';
                }

                $options = $action->{$parameter['SOURCE']}();
                foreach ($options as &$option) {
                    $attr = '';

                    if ($option['disabled'] === 'disabled') {
                        $attr .= ' disabled="disabled"';
                    }

                    if ((string) $value->value === (string) $option['value']) {
                        $attr .= ' selected="selected"';
                    }


                    $option = '<option value="' . $option['value'] . '"' . $attr . '>' . $option['label'] . '</option>';
                }

                $attributes_string = '';
                foreach ($attributes_arr as $attribute => $arr) {
                    $attributes_string .= ' ' . $attribute . '="' . join(' ', $arr) . '"';
                }

                if (empty($condition_index) === true) {
                    $condition_index = '{js@condition_index}';
                }

                $html = '<select name="condition[' . $condition_index . '][PARAM][' . $parameter['ID'] . ']"' . $attributes_string . '>' . join('', $options) . '</select>';

                break;
        }

        return $html;
    }
}
