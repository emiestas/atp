<?php

class Atp_Document_Templates_Template_Condition_Action_Clause extends Atp_Document_Templates_Template_Condition_Action
{
    public $core;

    public $template;

    public $values;

    public function __construct(Atp_Document_Templates_Template_Condition_Action &$action)
    {
        $this->template = &$action->template;
        $this->core = &$action->core;
    }

    public function set_values($values)
    {
        $this->values = $values;
    }

    public function get_parameters()
    {
        return array(
            array('ID' => 1, 'LABEL' => 'Straipsnis', 'TYPE' => 'SELECT', 'SOURCE' => 'clause_list')
        );
    }

    public function clause_list()
    {
        $result = array();

        $i = 0;
        $result[$i++] = array(
            'value' => '',
            'label' => '-- ' . 'Straipsnis' . ' --'
        );


        $items = $this->core->logic2->get_clause(null, null, 'NAME', 'ID, NAME');
        foreach ($items as &$item) {
            $result[$i++] = array(
                'value' => $item['ID'],
                'label' => $item['NAME']
            );
        }

        return $result;
    }

    public function check($parameters, \Atp\Document\Record &$recordManager)
    {
        if (empty($parameters[1]->value)) {
            return false;
        }

        $clause = $this->core->logic2->get_record_clause($recordManager->record);

        if ((int) $clause['ID'] === (int) $parameters[1]->value) {
            return true;
        }

        return false;
    }
}
