<?php

class Atp_Document_Templates_Template_Condition_Action_WorkerStructure extends Atp_Document_Templates_Template_Condition_Action
{
    public $core;

    public $template;

    public $values;

    public function __construct(Atp_Document_Templates_Template_Condition_Action &$action)
    {
        $this->template = &$action->template;
        $this->core = &$action->core;
    }

    public function set_values($values)
    {
        $this->values = $values;
    }

    public function get_parameters()
    {
        return array(
            array('ID' => 1, 'LABEL' => 'Organizacinė struktūra', 'TYPE' => 'SELECT',
                'SOURCE' => 'structure_list')
        );
    }

    public function structure_list()
    {
        $result = array();

        $i = 0;
        $result[$i++] = array(
            'value' => '',
            'label' => '-- ' . 'Organizacinės struktūros vienetas' . ' --'
        );

        $structure = $this->core->logic2->get_db(PREFIX . 'MAIN_STRUCTURE', null, null, null, false);
        $items = $this->core->logic->sort_children_to_parent($structure, 'ID', 'PARENT_ID');

        foreach ($items as &$item) {
            $result[$i++] = array(
                'value' => $item['ID'],
                'label' => str_repeat('&nbsp;', $item['depth'] * 3) . $item['SHOWS']
            );
        }

        return $result;
    }

    public function check($parameters, \Atp\Document\Record &$recordManager)
    {
        if (empty($parameters[1]->value)) {
            return false;
        }

        $param = array(
            'F.ID = "' . $recordManager->record['ID'] . '"');
                // TODO: Kai statusas "nutrauktas" ?
        if ((int) $recordManager->record['STATUS'] === \Atp\Record::STATUS_INVESTIGATING || (int) $recordManager->record['STATUS'] === \Atp\Record::STATUS_DISCONTINUED) {
            $workerType = \Atp\Record\Worker::TYPE_INVESTIGATOR;
            $param[] = 'A.TYPE = "' . $workerType . '"';
        } else {
            // jei nagrinėjimo dokumentas, ieško nagrinėtojo
            if ((int) $recordManager->record['STATUS'] === \Atp\Record::STATUS_EXAMINING) {
                // TODO: "next" nereikėtu, jei baigaint tarpinę būseną "next" darbuotojas taptu "exam" (t.y. numatytas nagrinėtojas taptu nagrinėjančiu)
                $workerTypes = [\Atp\Record\Worker::TYPE_EXAMINATOR, \Atp\Record\Worker::TYPE_NEXT];
                $param[] = 'A.TYPE IN("' . join('","', $workerTypes) . '")';
            }
        }

        $worker = $this->core->logic2->get_record_worker($param, true, null, 'F.ID, D.STRUCTURE_ID');

        if ((int) $worker['STRUCTURE_ID'] === (int) $parameters[1]->value) {
            return true;
        }

        return false;
    }
}
