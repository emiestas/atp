<?php

class Atp_Document_Templates_Template_Condition_Action_FromDocument extends Atp_Document_Templates_Template_Condition_Action implements Atp_Document_Templates_Template_Condition_Action_IAction
{
    public $core;

    public $template;

    public $values;

    public function __construct(Atp_Document_Templates_Template_Condition_Action &$action)
    {
        $this->template = &$action->template;
        $this->core = &$action->core;
    }

    public function set_values($values)
    {
        $this->values = $values;
    }

    public function get_parameters()
    {
        return array(
            array('ID' => 1, 'LABEL' => 'Dokumentas', 'TYPE' => 'SELECT', 'SOURCE' => 'document_list')
        );
    }

    public function document_list()
    {
        $items = $this->core->logic2->get_document(null, null, 'LABEL', 'ID, LABEL');

        $result = array();

        $i = 0;
        $result[$i++] = array(
            'value' => '',
            'label' => '-- ' . 'Dokumentas' . ' --'
        );

        foreach ($items as &$item) {
            $result[$i++] = array(
                'value' => $item['ID'],
                'label' => $item['LABEL']
            );
        }

        return $result;
    }

    public function check($parameters, \Atp\Document\Record &$recordManager)
    {
        if (empty($parameters[1]->value)) {
            return false;
        }

        $path = $this->core->logic->get_record_path($recordManager->record);
        $path = sort_by($path, 'CREATE_DATE', 'asc');

        // Suranda enamąjį įrašą
        while (key($path) !== null && (int) key($path) !== (int) $recordManager->record['ID']) {
            next($path);
        }

        // Praleidžia to pačio įrašo skirtingas versijas
        $current = current($path);
        $previous = prev($path);
        while ($current['RECORD_ID'] === $previous['RECORD_ID']) {
            $previous = prev($path);
        }

        if ((int) $previous['TABLE_TO_ID'] === (int) $parameters[1]->value) {
            return true;
        }

        return false;
    }
}
