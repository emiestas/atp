<?php

class Atp_Document_Templates_Template_Condition_Action_Classificator extends Atp_Document_Templates_Template_Condition_Action
{
    public $core;

    public $template;

    public $values;

    /**
     * @var \Atp\Record\Values
     */
    public $rv;

    public function __construct(Atp_Document_Templates_Template_Condition_Action &$action)
    {
        $this->template = &$action->template;
        $this->core = &$action->core;
        $this->rv = new \Atp\Record\Values($this->core);
    }

    public function set_values($values)
    {
        $this->values = $values;
    }

    public function get_parameters()
    {
        return array(
            array('ID' => 1, 'LABEL' => 'Klasifikatoriaus laukas', 'TYPE' => 'SELECT',
                'SOURCE' => 'classificator_field'),
            array('ID' => 2, 'LABEL' => 'Klasifikatoriaus reikšmė', 'TYPE' => 'SELECT',
                'SOURCE' => 'classificator_value', 'PARENT_ID' => 1)
        );
    }

    public function classificator_field()
    {
        $template = $this->template->get();

        $all_fields = array();
        $select_hierarchy = $this->core->manage->tables_relations_parent_fields($template->document_id, 'DOC', $data = array(), $all_fields, $null = null);
        $data = array();
        $select_hierarchy = array_reverse($select_hierarchy, true);
        $select_hierarchy[0]['OPT'] = '-- Klasifikatoriaus laukas --';
        $data['select_hierarchy'] = array_reverse($select_hierarchy, true);
        $all_fields[0] = array('ID' => 0, 'LABEL' => '-- Klasifikatoriaus laukas --');
        $data['all_fields'] = &$all_fields;
        $this->core->manage->printTree($data['select_hierarchy'], 0, $data['all_fields'], $valid_fields = false, $selected = null, $options);

        $matches = null;
        $returnValue = preg_match_all('#<option([^>]*)>([^</]*)</?option>#i', $options, $matches);

        $result = array();
        if ($returnValue) {
            for ($i = 0; $i < $returnValue; $i++) {
                $ref = &$result[$i];
                $attributes_ref = &$matches[1][$i];

                preg_match('#value=["\']?([^"\']*)#i', $attributes_ref, $m);
                $field = $this->core->logic2->get_fields(array('ID' => $m[1]));

                $ref['value'] = $m[1];

                preg_match('#disabled=["\']?([^"\']*)#i', $attributes_ref, $m);
                $ref['disabled'] = $m[1];

                $ref['label'] = $matches[2][$i];

                // Išjungiami visi laukai, kurie nėra klasifikatoriai ir kurių reikšmė nėra "0"
                if ($field['TYPE'] !== '18' && $ref['value'] !== '0') {
                    $ref['disabled'] = 'disabled';
                }
                if ($ref['value'] === '0') {
                    $ref['value'] = '';
                }
            }
        }

        return $result;
    }

    public function classificator_value()
    {
        if (count($this->values)) {
            foreach ($this->values as &$value) {
                if ((int) $value->id === 1) {
                    $field_id = $value->value;
                }
            }

            $field = $this->core->logic2->get_fields(array('ID' => $field_id));

            $items = array();
            if (empty($field['SOURCE']) === false) {
                $items = $this->core->logic2->get_classificator(array('PARENT_ID' => $field['SOURCE']));
            }
        }
        $result = array();

        $i = 0;
        $result[$i++] = array(
            'value' => '',
            'label' => '-- ' . 'Klasifikatorius' . ' --'
        );

        if (count($items)) {
            foreach ($items as &$item) {
                $result[$i++] = array(
                    'value' => $item['ID'],
                    'label' => $item['LABEL']
                );
            }
        }

        return $result;
    }

    public function check($parameters, \Atp\Document\Record &$recordManager)
    {
        if (empty($parameters[1]->value) || empty($parameters[2]->value)) {
            return false;
        }

//        $field = $this->core->logic2->get_fields(array('ID' => $parameters[1]->value));
//        $values = $this->rv->getValuesByRecord($recordManager->record['ID']);
//
//        $field_value = $values[$field['ID']];
//        if ((int) $field_value === (int) $parameters[2]->value) {
//            return true;
//        }

        $recordId = $recordManager->record['ID'];
        $fieldId = $parameters[1]->value;
        if((int) $this->rv->getValue($recordId, $fieldId) === (int) $parameters[2]->value) {
            return true;
        }

        return false;
    }
}
