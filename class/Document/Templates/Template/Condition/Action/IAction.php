<?php

interface Atp_Document_Templates_Template_Condition_Action_IAction
{
    public function __construct(Atp_Document_Templates_Template_Condition_Action &$action);

    public function set_values($values);

    public function get_parameters();

    public function check($parameters, \Atp\Document\Record &$recordManager);
}
