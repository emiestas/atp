<?php

class Atp_Document_Templates_Template_Condition_Action_Status extends Atp_Document_Templates_Template_Condition_Action
{
    public $core;

    public $template;

    public $values;

    public function __construct(Atp_Document_Templates_Template_Condition_Action &$action)
    {
        $this->template = &$action->template;
        $this->core = &$action->core;
    }

    public function set_values($values)
    {
        $this->values = $values;
    }

    public function get_parameters()
    {
        return array(
            array('ID' => 1, 'LABEL' => 'Būsena', 'TYPE' => 'SELECT', 'SOURCE' => 'status_list')
        );
    }

    public function status_list()
    {
        $result = array();

        $i = 0;
        $result[$i++] = array(
            'value' => '',
            'label' => '-- ' . 'Būsena' . ' --'
        );

        $items = $this->core->logic2->get_status();

        foreach ($items as &$item) {
            $result[$i++] = array(
                'value' => $item['ID'],
                'label' => $item['LABEL']
            );
        }

        return $result;
    }

    public function check($parameters, \Atp\Document\Record &$recordManager)
    {
        if (empty($parameters[1]->value)) {
            return false;
        }

        if ((int) $recordManager->record['DOC_STATUS'] === (int) $parameters[1]->value) {
            return true;
        }

        return false;
    }
}
