<?php

class Atp_Document_Templates_Template_Condition_Action_Modified extends Atp_Document_Templates_Template_Condition_Action
{
    public $core;

    public $template;

    public $values;

    /**
     * @var \Atp\Record\Values
     */
    public $rv;

    public function __construct(Atp_Document_Templates_Template_Condition_Action &$action)
    {
        $this->template = &$action->template;
        $this->core = &$action->core;
        $this->rv = new \Atp\Record\Values($this->core);
    }

    public function set_values($values)
    {
        $this->values = $values;
    }

    public function get_parameters()
    {
        return array(
            array('ID' => 1, 'LABEL' => 'Laukas', 'TYPE' => 'SELECT', 'SOURCE' => 'fields_list')
        );
    }

    public function fields_list()
    {
        $template = $this->template->get();

        $all_fields = array();
        $select_hierarchy = $this->core->manage->tables_relations_parent_fields($template->document_id, 'DOC', $data = array(), $all_fields, $null = null);
        $data = array();
        $select_hierarchy = array_reverse($select_hierarchy, true);
        $select_hierarchy[0]['OPT'] = '-- Laukas --';
        $data['select_hierarchy'] = array_reverse($select_hierarchy, true);
        $all_fields[0] = array('ID' => 0, 'LABEL' => '-- Laukas --');
        $data['all_fields'] = &$all_fields;
        $this->core->manage->printTree($data['select_hierarchy'], 0, $data['all_fields'], $valid_fields = false, $selected = null, $options);
        $matches = null;
        $returnValue = preg_match_all('#<option([^>]*)>([^</]*)</?option>#i', $options, $matches);

        $result = array();
        if ($returnValue) {
            for ($i = 0; $i < $returnValue; $i++) {
                $ref = &$result[$i];
                $attributes_ref = &$matches[1][$i];

                preg_match('#value=["\']?([^"\']*)#i', $attributes_ref, $m);
                $field = $this->core->logic2->get_fields(array('ID' => $m[1]));

                $ref['value'] = $m[1];

                preg_match('#disabled=["\']?([^"\']*)#i', $attributes_ref, $m);
                $ref['disabled'] = $m[1];

                $ref['label'] = $matches[2][$i];

                if ($ref['value'] === '0') {
                    $ref['value'] = '';
                }
            }
        }

        return $result;
    }

    public function check($parameters, \Atp\Document\Record &$recordManager)
    {
        if (empty($parameters[1]->value)) {
            return false;
        }

        $path = $this->core->logic->get_record_path($recordManager->record);
        $path = sort_by($path, 'CREATE_DATE', 'asc');

        // Suranda enamąjį įrašą
        while (key($path) !== null && (int) key($path) !== (int) $recordManager->record['ID']) {
            next($path);
        }

        // Praleidžia to pačio įrašo skirtingas versijas
        $current = current($path);
        $previous = prev($path);

        // Jei nėra ankstesnės dokumento versijos
        if ($current['RECORD_ID'] !== $previous['RECORD_ID']) {
            return true;
        }

        // Tikrinamas laukas
        $field = $this->core->logic2->get_fields(array('ID' => $parameters[1]->value));

        // Dabartinio dokumento įrašo lauko reikšmė
        $values = $this->rv->getValuesByRecord($current['ID']);
        $current_value = $values[$field['ID']];

        // Buvusio dokumento įrašo lauko reikšmė
        $values = $this->rv->getValuesByRecord($previous['ID']);
        $previous_value = $values[$field['ID']];

        // Jei nesutampa
        if ($current_value !== $previous_value) {
            return true;
        }

        return false;
    }
}
