<?php

interface ATPWS
{
    /**
     * Informacija apie web-servisą
     */
    public function info();
    /**
     * Informacija apie parametrus, kuriuos web-servisas siunčia
     */
    /* function params_info(); */

    /**
     * Parametrai, kuriuos web-servisas gali siųsti
     */
    public function params();

    /**
     * Web-servisu gaunamų laukų informacija
     */
    public function fields();

    /**
     * Siunčiamų parametrų formavimas
     */
    public function set_params($params, $type = null);

    /**
     * Siunčiam užklausa
     */
    public function init();

    /**
     * Web-servisu gauto rezultato performavimas
     */
    public function result();
}

/*
 * Example:
  $ws = new atp_ws($this);
  $ws->set_webservice('vmi'); // Or by ID // $ws->set_webservice(1);
  $ws->get_fields();
  $ws->set_parameters(array('POST' => array('CODE' => '49005071259')));
  $ws->initialize();
  $response = $ws->get_result();
 */

class atp_ws
{
    private $atp;

    private $ws; // Loaded web-services

    private $webservice; // Current web-service

    private $log_info;

    /**
     *
     * @param \Atp\Core $atp
     * @param int/string $name Web-serviso vardas arba ID
     */
    public function __construct(\Atp\Core $atp, $name = null)
    {
        $this->atp = $atp;
        $this->load($name);

        if ($name !== null) {
            $this->set_webservice($name);
        }
    }

    /**
     * Sukuria web-servisų objektus.
     * @param string $ws_name [optional] Konkretaus web-serviso pavadinimas
     */
    private function load($ws_name = null)
    {
        if ($ws_name !== null) {
            $ws_name = strtoupper($ws_name);
        }

        foreach (glob($this->atp->config['REAL_URL'] . 'class/Document/WebService/*_ws.php') as $path) {
            $name = strtoupper(str_replace('_ws.php', '', basename($path)));
            if ($ws_name !== null && $ws_name !== $name) {
                continue;
            }

            if (isset($this->ws[$name]) === false) {
                require_once($path);
                $this->ws[$name] = new $name;
            }
        }
    }

    /**
     * Gauna visų web-servisų informaciją.
     * @return method
     */
    public function get_infos()
    {
        return $this->get_info(0);
    }

    /**
     * Gauna web-serviso informaciją.
     * @param int/string $name [optional] Web-serviso pavadinimas arba ID
     * @return boolean/array <b>FALSE</b> - jei web-servisas neužkrautas arba neegzistuoja
     * @example set_webservice('vmi'); get_info(); // Predefined web-service
     * @example get_info('vmi'); // By web-service name
     * @removed_example get_info(1); // By web-service ID
     * @example get_info(0); // All web-services
     */
    public function get_info($name = null)
    {
        if ($name === 0) {
            $ret = array();
            foreach ($this->ws as $name => &$obj) {
                $ret[$name] = $obj->info();
            }
            return $ret;
        }

        if ($name === null && is_object($this->webservice) == true) {
            return $this->webservice->info();
        }

        if (ctype_digit((string) $name) === true) {
            $webservices = $this->get_infos();
            foreach ($webservices as &$webservice) {
                if ($webservice['id'] != $name) {
                    continue;
                }

                $name = $webservice['name'];
                break;
            }
        }

        $name = strtoupper($name);
        if (isset($this->ws[$name])) {
            return $this->ws[$name]->info();
        }

        return false;
    }

    /**
     * Gauna visų web-servisų laukus.
     * @return array
     */
    public function get_webservices_fields()
    {
        $ret = array();
        foreach ($this->ws as $name => &$obj) {
            $ret[$name] = $this->get_fields($name);
        }
        return $ret;
    }

    /**
     * Gauna web-serviso laukus
     * @param int/string $name [optional] Web-serviso pavadinimas arba ID
     * @return boolean/array <b>FALSE</b> - jei web-servisas neužkrautas arba neegzistuoja
     * @example set_webservice('vmi'); get_fields(); // Predefined web-service
     * @example get_fields('vmi'); // By web-service name
     * @example get_fields(1); // By web-service ID
     */
    public function get_fields($name = null)
    {
        if ($name === null && is_object($this->webservice) === true) {
            return $this->webservice->fields();
        }
        if (ctype_digit((string) $name) === true) {
            $info = $this->get_info($name);
            if ($info !== false) {
                $name = $info['name'];
            }
        }

        $name = strtoupper($name);
        if (isset($this->ws[$name]) === true) {
            return $this->ws[$name]->fields();
        }

        return false;
    }

    /**
     * Gauna web-serviso lauko informaciją
     * @param string/int $name Web-serviso pavadinimas arba ID
     * @param array $param Paieškos parametrai
     * @return boolean/array Lauko informacijos masyvas arba <b>FALSE</b>
     * @example get_field('vmi', array('ID' => 1));
     * @example get_field('vmi', array('NAME' => 'WCOL_1'));
     * @example get_field(1, array('ID' => 1));
     */
    public function get_field($name, $param = array())
    {
        if (empty($name) === true) {
            return false;
        }

        $fields = $this->get_fields($name);
        if (empty($fields) === false) {
            foreach ($fields as &$field) {
                if (isset($param['ID']) === true) {
                    if ((int) $field['ID'] === (int) $param['ID']) {
                        return $field;
                    }
                } elseif (isset($param['NAME']) === true) {
                    if ($field['NAME'] === strtoupper($param['NAME'])) {
                        return $field;
                    }
                } elseif (isset($param['TAG']) === true) {
                    if ($field['TAG'] === strtoupper($param['TAG'])) {
                        return $field;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Gauna visų web-servisų parametrų laukus.
     * @return array
     */
    public function get_webservices_params()
    {
        $ret = array();
        foreach ($this->ws as $name => &$obj) {
            $ret[$name] = $this->get_params($name);
        }
        return $ret;
    }

    /**
     * Gauna web-serviso parametrų laukus
     * @param int/string $name [optional] Web-serviso pavadinimas arba ID
     * @return boolean/array <b>FALSE</b> - jei web-servisas neužkrautas arba neegzistuoja
     * @example set_webservice('vmi'); get_params(); // Predefined web-service
     * @example get_params('vmi'); // By web-service name
     * @example get_params(1); // By web-service ID
     */
    public function get_params($name = null)
    {
        if ($name === null && is_object($this->webservice) === true) {
            return $this->webservice->fields();
        }
        if (ctype_digit((string) $name) === true) {
            $info = $this->get_info($name);
            if ($info !== false) {
                $name = $info['name'];
            }
        }

        $name = strtoupper($name);
        if (isset($this->ws[$name]) === true) {
            return $this->ws[$name]->params();
        }

        return false;
    }

    /**
     * Gauna web-serviso parametro lauką
     * @param string/int $name Web-serviso pavadinimas arba ID
     * @param array $param Paieškos parametrai
     * @return boolean/array Lauko informacijos masyvas arba <b>FALSE</b>
     * @example get_param('vmi', array('ID' => 1));
     * @example get_param('vmi', array('NAME' => 'WCOL_1'));
     * @example get_param(1, array('ID' => 1));
     */
    public function get_param($name, $param = array())
    {
        if (empty($name) === true) {
            return false;
        }

        $ret = array();

        $params = $this->get_params($name);
        if (empty($params) === false) {
            foreach ($params as &$arr) {
                if (isset($arr['ID']) === true) {
                    if ((int) $param['ID'] === (int) $arr['ID']) {
                        return $arr;
                    }
                } elseif (isset($param['TAG']) === true) {
                    if (in_array($param['TAG'], $arr['TAGS']) === true) {
                        $ret[$arr['ID']] = $arr;
                    }
                }
            }
            if (count($ret)) {
                return $ret;
            }
        }
        return false;
    }

    /**
     * Nustato naudojamą web-servisą
     * @param string/int $name Web-serviso vardas arba ID
     * @return boolean <b>TRUE</b> jei web-servisas nurodytas, kitaip <b>FALSE</b>
     * @example set_webservice('vmi'); // By web-service name
     * @example set_webservice(1); // By web-service ID
     */
    public function set_webservice($name)
    {
        if (isset($name) && ctype_digit((string) $name) === true) {
            $info = $this->get_info($name);
            if ($info !== false) {
                $name = $info['name'];
            }
        }

        $name = strtoupper($name);
        if (isset($this->ws[$name]) === true) {
            $this->webservice = $this->ws[$name];
            return true;
        }
        return false;
    }

    /**
     * Nurado web-serviso parametrus
     * @param array $params Parametrai.
     * @param string $type Parametrų perdavimo tipas
     * @example set_webservice('vmi'); set_parameters(array('page' => 1), 'GET');
     * @example set_webservice('vmi'); set_parameters(array('page' => 1)); // By default type is POST
     * @example set_webservice('vmi'); set_parameters(array('GET' => array('page' => 1)));
     */
    public function set_parameters($params)
    {
        if (is_object($this->webservice) === false) {
            return false;
        }

        $ws_info = $this->webservice->info();

        if (func_num_args() > 1) {
            $par = func_get_arg(1);
            $this->log_info = 'Registras "' . $ws_info['label'] . '".<br/>'
                . ' Siunčiamos užklausos parametrai - ' . urldecode(http_build_query($params, '', ', ')) . '.';
            return $this->webservice->set_params($params, $par);
        }
        if (empty($params['POST']) === false) {
            $this->log_info = 'Registras "' . $ws_info['label'] . '".<br/>'
                . ' Siunčiamos užklausos parametrai - ' . urldecode(http_build_query($params['POST'], '', ', ')) . '.';
        } elseif (empty($params['GET']) === false) {
            $this->log_info = 'Registras "' . $ws_info['label'] . '".<br/>'
                . ' Siunčiamos užklausos parametrai - ' . urldecode(http_build_query($params['GET'], '', ', ')) . '.';
        }

        return $this->webservice->set_params($params);
    }

    public function initialize()
    {
        if (is_object($this->webservice) === false) {
            return false;
        }

        return $this->webservice->init();
    }

    public function get_result()
    {
        if (is_object($this->webservice) === false) {
            return false;
        }

        $res = $this->webservice->result();
        if (empty($res) === false) {
            $this->log_info .= '</br>Užklausa sėkminga.';
        } else {
            $this->log_info .= '</br>Užklausa nesėkminga.';
        }

//        $this->atp->manage->writeATPactionLog(array($this->log_info));
        $adminJournal = $this->atp->factory->AdminJournal();
        $adminJournal->addRecord(
            $adminJournal->getMessageRecord($this->log_info)
        );

        return $res;
    }

    /**
     * Gauna web-serviso pranešimą
     * @return boolean
     */
    public function get_messages()
    {
        if (is_object($this->webservice) === false) {
            return false;
        }

        if (method_exists($this->webservice, 'get_messages')) {
            return $this->webservice->get_messages();
        }

        return null;
    }

    public function getRDBWsData($method, $data, $type = 'array')
    {
        prepare_rdb();
        require_once($this->atp->config['GLOBAL_REAL_URL'] . 'subsystems/rdb/tools/class-rdbws.php');
        $data['username'] = sha1('rdbNRTws');
        $data['password'] = sha1('T53==+oje+tsycv3Z!2-nQbwt*R==');
        $ws = new tools_ws(strtolower($method), $data, $type);
        $ws->init();
        $res = $ws->get_result();
        if (isset($res['data'])) {
            return $res['data'];
        }
        return false;
    }

    /**
     * @param string $logType
     * @param mixed $logData Data to log
     * @param string $logFile [optional] Log file path
     * @return string Log file path
     */
    protected function log($logType, $logData, $logFile = null)
    {
        if (empty($logFile)) {
            $dir = __DIR__ . '/../../../../../logs/wslogs/atp/' . $logType . '/' . date('/Y/m/d/');
            if (is_dir($dir) === false) {
                $oldMask = umask(0);
                mkdir($dir, 0777, true);
                umask($oldMask);
            }
            $logFile = $dir . '/' . date('Y-m-d H-i-s') . '-'
                . preg_replace('#^0\.([0-9]+)\s([0-9]+)$#', '$1', microtime())
                . '.json';
        }

        if (is_array($logData)) {
            $logData = json_encode($logData) . "\r\n";
        }

        $oldMask = umask(0);
        file_put_contents($logFile, $logData, FILE_APPEND);
        umask($oldMask);

        return $logFile;
    }
}
