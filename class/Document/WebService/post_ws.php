<?php

class POST extends atp_ws implements ATPWS
{
    //private $ws_id = 3;
    private $ws_name = 'POST';

    private $ws_label = 'Paštas';

    private $ws_url = 'https://edarbuotojas.vilnius.lt/subsystems/rdb/ws.php?method=getPOSTCodeSearchResult'; //'http://wp.dev/subsystems/rdb/ws.php?method=getPOSTCodeSearchResult';//

    private $ws_params;

    private $ws_result;

    private $ws_messages;

    public function POST()
    {
        $this->ws_params['POST']['datatype'] = 'json';
        $this->ws_params['POST']['registry'] = 'postrt';
        $this->ws_params['POST']['username'] = '7bb08e54dfb43275087a2400451bf96be726e5b1'; //sha1('rdbNRTws');
        $this->ws_params['POST']['password'] = '93addf8424e87104d33dc33428c7e6a74f03ce2b'; //sha1('T53==+oje+tsycv3Z!2-nQbwt*R==');
        //$this->ws_params['POST']['SEARCH_FROM'] = $this->ws_params['POST']['SEARCH_TILL'] = date('Y-m-d H:i:s');
    }

    public function info()
    {
        return array(
            //'id' => $this->ws_id,
            'name' => $this->ws_name,
            'label' => $this->ws_label,
            'path' => __FILE__
        );
    }

    public function params()
    {
        $data = array(
            array('ID' => 1, 'LABEL' => 'Vietovės pavadinimas', 'TAGS' => array(
                    'SEARCH_LOCALITY')),
            array('ID' => 2, 'LABEL' => 'Gatvės pavadinimas', 'TAGS' => array('SEARCH_STREET')),
            array('ID' => 3, 'LABEL' => 'Namo numeris', 'TAGS' => array('SEARCH_HOUSE_NO'))
        );


        $ret = array();
        for ($i = 1; $i <= count($data); $i++) {
            $ret[$i] = $data[$i - 1];
        }
        return $ret;
    }

    public function fields()
    {
        $fields = array(
            'ID' => 0, 'ITEM_ID' => null, 'ITYPE' => 'WS',
            'LABEL' => null, 'NAME' => null, 'SORT' => null,
            'TYPE' => null, 'SOURCE' => '0', /*'VALUE' => '',
            'VALUE_TYPE' => '0', 'OTHER_SOURCE' => '0', 'OTHER_VALUE' => '0',
            'FILL' => '0',*/ 'IN_LIST' => '0', 'RELATION' => '0',
            'MANDATORY' => '0', 'VISIBLE' => '0', 'COMPETENCE' => \Atp\Document\Field::COMPETENCE_ALL,
            'ORD' => '0', 'TEXT' => '', 'DEFAULT' => '',
            'DEFAULT_ID' => '', 'UPDATE_DATE' => '2014-01-01 00:00:00', 'CREATE_DATE' => '2014-01-01 00:00:00');

        $data = array(
            array('ID' => 1, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Įrašo ID', 'TAG' => 'ID'),
            array('ID' => 2, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Aptarnaujančio pašto kodas',
                'TAG' => 'POST_OFFICE_CODE'),
            array('ID' => 3, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Pašto kodas',
                'TAG' => 'POST_CODE'),
            array('ID' => 4, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Savivaldybė',
                'TAG' => 'MUNICIPALITY'),
            array('ID' => 5, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Pašto pavadinimas',
                'TAG' => 'POST_OFFICE'),
            array('ID' => 6, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Vietovės pavadinimas',
                'TAG' => 'LOCALITY'),
            array('ID' => 7, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Gatvės pavadinimas',
                'TAG' => 'STREET'),
            array('ID' => 8, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Namo numeris',
                'TAG' => 'HOUSE_NO')
        );

        $ret = array();
        for ($i = 1; $i <= count($data); $i++) {
            $data[$i - 1]['NAME'] = 'WCOL_' . $data[$i]['ID'];
            $ret[$i] = array_merge($fields, $data[$i - 1]);
        }
        return $ret;
    }

    public function set_params($params, $type = null)
    {
        if (func_num_args() > 1) {
            $this->ws_params[$type] = array_merge((array) $this->ws_params[$type], (array) $params);
        } else {
            foreach ($params as $type => $param) {
                $this->ws_params[$type] = array_merge((array) $this->ws_params[$type], (array) $param);
            }
        }

        return true;
    }

    public function init()
    {
        $logFile = $this->log(__CLASS__, '');

        $cr = curl_init();
        curl_setopt($cr, CURLOPT_URL, $this->ws_url);
        curl_setopt($cr, CURLOPT_HEADER, 0);
        curl_setopt($cr, CURLOPT_POST, 1);
        curl_setopt($cr, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cr, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($cr, CURLOPT_POSTFIELDS, http_build_query($this->ws_params['POST']));

        $logParameters = $this->ws_params['POST'];
        $logParameters['username'] = ':)p';
        $logParameters['password'] = ':)p';
        $this->log(__CLASS__, "REQUEST:\r\n" . json_encode($logParameters) . "\r\n", $logFile);

        $response = curl_exec($cr);

        $this->log(__CLASS__, "RESPONSE:\r\n" . $response . "\r\n", $logFile);
        $this->log(__CLASS__, "INFO:\r\n" . json_encode(curl_getinfo($cr)) . "\r\n", $logFile);

        if (curl_errno($cr)) {
            $this->wp_error = curl_error($cr);
            $this->log(__CLASS__, "ERROR:\r\n" . $this->wp_error . "\r\n", $logFile);
        } else {
            $this->wp_result = $response;

            $this->log(__CLASS__, "RESULT:\r\n" . json_encode($this->result()) . "\r\n", $logFile);
        }

        curl_close($cr);

        return true;
    }

    public function result()
    {
        return $this->wp_result;
    }
}
