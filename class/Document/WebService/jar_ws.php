<?php

class JAR extends atp_ws implements ATPWS
{
    //private $ws_id = 2;
    private $ws_name = 'JAR';

    private $ws_label = 'Juridinių asmenų registras';

    private $ws_url = 'https://edarbuotojas.vilnius.lt/subsystems/rdb/ws.php?method=getRCRTSearchResult';

    private $ws_params;

    private $ws_result;

    private $ws_messages;

    //private $tables;

    public function JAR()
    {
        $this->ws_params['POST']['datatype'] = 'json';
        $this->ws_params['POST']['registry'] = 'rcrt';
        $this->ws_params['POST']['rcrt_registry'] = 'jar';
        $this->ws_params['POST']['username'] = '7bb08e54dfb43275087a2400451bf96be726e5b1'; //sha1('rdbNRTws');
        $this->ws_params['POST']['password'] = '93addf8424e87104d33dc33428c7e6a74f03ce2b'; //sha1('T53==+oje+tsycv3Z!2-nQbwt*R==');
    }

    public function info()
    {
        return array(
            //'id' => $this->ws_id,
            'name' => $this->ws_name,
            'label' => $this->ws_label,
            'path' => __FILE__
        );
    }

    public function params()
    {
        $data = array(
            array('ID' => 1, 'LABEL' => 'OBJ_KODAS', 'TAGS' => array('SEARCH_CODE')),
            array('ID' => 2, 'LABEL' => 'OBJ_PAV', 'TAGS' => array('SEARCH_NAME'))
        );

        $ret = array();
        for ($i = 1; $i <= count($data); $i++) {
            $ret[$i] = $data[$i - 1];
        }
        return $ret;
    }

    public function fields()
    {
        $fields = array(
            'ID' => 0, 'ITEM_ID' => null, 'ITYPE' => 'WS',
            'LABEL' => null, 'NAME' => null, 'SORT' => null,
            'TYPE' => null, 'SOURCE' => '0', /*'VALUE' => '',
            'VALUE_TYPE' => '0', 'OTHER_SOURCE' => '0', 'OTHER_VALUE' => '0',
            'FILL' => '0',*/ 'IN_LIST' => '0', 'RELATION' => '0',
            'MANDATORY' => '0', 'VISIBLE' => '0', 'COMPETENCE' => \Atp\Document\Field::COMPETENCE_ALL,
            'ORD' => '0', 'TEXT' => '', 'DEFAULT' => '',
            'DEFAULT_ID' => '', 'UPDATE_DATE' => '2014-01-01 00:00:00', 'CREATE_DATE' => '2014-01-01 00:00:00');

        $data = array(
            array('ID' => 1, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'OBJ_ID', 'TAG' => 'OBJ_ID'),
            array('ID' => 2, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'OBJ_KODAS', 'TAG' => 'OBJ_KODAS'),
            array('ID' => 3, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'OBJ_REJESTRO_KODAS',
                'TAG' => 'OBJ_REJESTRO_KODAS'),
            array('ID' => 4, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'OBJ_PAV', 'TAG' => 'OBJ_PAV'),
            array('ID' => 5, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'FORM_KODAS', 'TAG' => 'FORM_KODAS'),
            array('ID' => 6, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'JST_DATA_NUO',
                'TAG' => 'JST_DATA_NUO'),
            array('ID' => 7, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'JST_IGIJIMO_DATA',
                'TAG' => 'JST_IGIJIMO_DATA'),
            array('ID' => 8, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'JAD_TEKSTAS',
                'TAG' => 'JAD_TEKSTAS'),
            array('ID' => 9, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'OBJ_REG_DATA',
                'TAG' => 'OBJ_REG_DATA'),
            array('ID' => 10, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VER_DATA_NUO',
                'TAG' => 'VER_DATA_NUO'),
            array('ID' => 11, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VER_VERSIJA',
                'TAG' => 'VER_VERSIJA'),
            array('ID' => 12, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'DBUK_KODAS',
                'TAG' => 'DBUK_KODAS'),
            array('ID' => 13, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'TARN_NR', 'TAG' => 'TARN_NR'),
            array('ID' => 14, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'TARN_PAV_I',
                'TAG' => 'TARN_PAV_I'),
            array('ID' => 15, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'FAV_PAVARDE',
                'TAG' => 'FAV_PAVARDE'),
            array('ID' => 16, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'FAV_VARDAS',
                'TAG' => 'FAV_VARDAS'),
            array('ID' => 17, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'FIZ_ID', 'TAG' => 'FIZ_ID'),
            array('ID' => 18, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'FIZ_KODAS', 'TAG' => 'FIZ_KODAS'),
            array('ID' => 19, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'TATR_KODAS_10',
                'TAG' => 'TATR_KODAS_10'),
            array('ID' => 20, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'TATR_KODAS_20',
                'TAG' => 'TATR_KODAS_20'),
            array('ID' => 21, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'TATR_KODAS_30',
                'TAG' => 'TATR_KODAS_30'),
            array('ID' => 22, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'TATR_KODAS_40',
                'TAG' => 'TATR_KODAS_40'),
            array('ID' => 23, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'TATR_KODAS_50',
                'TAG' => 'TATR_KODAS_50')
        );

        $ret = array();
        for ($i = 1; $i <= count($data); $i++) {
            $data[$i - 1]['NAME'] = 'WCOL_' . $data[$i]['ID'];
            $ret[$i] = array_merge($fields, $data[$i - 1]);
        }
        return $ret;
    }

    public function set_params($params, $type = null)
    {
        if (func_num_args() > 1) {
            $this->ws_params[$type] = array_merge((array) $this->ws_params[$type], (array) $params);
        } else {
            foreach ($params as $type => $param) {
                $this->ws_params[$type] = array_merge((array) $this->ws_params[$type], (array) $param);
            }
        }

        return true;
    }

    public function init()
    {
        $logFile = $this->log(__CLASS__, '');

        $cr = curl_init();
        curl_setopt($cr, CURLOPT_URL, $this->ws_url);
        curl_setopt($cr, CURLOPT_HEADER, 0);
        curl_setopt($cr, CURLOPT_POST, 1);
        curl_setopt($cr, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cr, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($cr, CURLOPT_POSTFIELDS, http_build_query($this->ws_params['POST']));

        $logParameters = $this->ws_params['POST'];
        $logParameters['username'] = ':)p';
        $logParameters['password'] = ':)p';
        $this->log(__CLASS__, "REQUEST:\r\n" . json_encode($logParameters) . "\r\n", $logFile);

        $response = curl_exec($cr);

        $this->log(__CLASS__, "RESPONSE:\r\n" . $response . "\r\n", $logFile);
        $this->log(__CLASS__, "INFO:\r\n" . json_encode(curl_getinfo($cr)) . "\r\n", $logFile);

        if (curl_errno($cr)) {
            $this->wp_error = curl_error($cr);
            $this->log(__CLASS__, "ERROR:\r\n" . $this->wp_error . "\r\n", $logFile);
        } else {
            $this->wp_result = $response;

            $this->log(__CLASS__, "RESULT:\r\n" . json_encode($this->result()) . "\r\n", $logFile);
        }

        curl_close($cr);

        return true;
    }

    public function result()
    {
        $arr = json_decode($this->wp_result, true);
        if (count($arr['data']) === 1) {
            return current($arr['data']);
        }
        return array();
    }
}
