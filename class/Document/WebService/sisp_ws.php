<?php

class SISP extends atp_ws implements ATPWS
{
    private $tables;

    private $ws_name = 'SISP';

    private $ws_label = 'SĮ "Susisiekimo paslaugos"';

    private $ws_url = '';

    private $ws_params;

    private $ws_result;

    private $ws_messages;

    public function SISP()
    {
        $this->tables = new \stdClass();
        $this->tables = (object) array(
                'tows' => PREFIX . 'ATP_SISP_TOWS',
                'pics' => PREFIX . 'ATP_SISP_TOWPICTURES',
                'dict' => PREFIX . 'ATP_SISP_DICTIONARIES',
                'users' => PREFIX . 'ATP_SISP_USERS',
                'zones' => PREFIX . 'ATP_SISP_ZONES'
        );
    }

    public function info()
    {
        return array(
            'name' => $this->ws_name,
            'label' => $this->ws_label,
            'path' => __FILE__
        );
    }

    public function params()
    {
        $data = array(
            array('ID' => 1, 'LABEL' => 'TowDocNum', 'TAGS' => array('TowDocNum')),
            array('ID' => 2, 'LABEL' => 'PlateNumber', 'TAGS' => array('PlateNumber'))
        );

        $ret = array();
        for ($i = 1; $i <= count($data); $i++) {
            $ret[$i] = $data[$i - 1];
        }
        return $ret;
    }

    public function fields()
    {
        $fields = array(
            'ID' => 0, 'ITEM_ID' => null, 'ITYPE' => 'WS',
            'LABEL' => null, 'NAME' => null, 'SORT' => null,
            'TYPE' => null, 'SOURCE' => '0', /*'VALUE' => '',
            'VALUE_TYPE' => '0', 'OTHER_SOURCE' => '0', 'OTHER_VALUE' => '0',
            'FILL' => '0',*/ 'IN_LIST' => '0', 'RELATION' => '0',
            'MANDATORY' => '0', 'VISIBLE' => '0', 'COMPETENCE' => \Atp\Document\Field::COMPETENCE_ALL,
            'ORD' => '0', 'TEXT' => '', 'DEFAULT' => '',
            'DEFAULT_ID' => '', 'UPDATE_DATE' => '2014-01-01 00:00:00', 'CREATE_DATE' => '2014-01-01 00:00:00');

        $data = array(
            array('ID' => 1, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'id', 'TAG' => 'id'),
            array('ID' => 2, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'TowDocNum', 'TAG' => 'TowDocNum'),
            array('ID' => 3, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'didTowStatus',
                'TAG' => 'didTowStatus'),
            array('ID' => 4, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'RegisterTime',
                'TAG' => 'RegisterTime'),
            array('ID' => 5, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'ContactTime',
                'TAG' => 'ContactTime'),
            array('ID' => 6, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'ReleaseTime',
                'TAG' => 'ReleaseTime'),
            array('ID' => 7, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'PlateNumber',
                'TAG' => 'PlateNumber'),
            array('ID' => 8, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'didCarModel',
                'TAG' => 'didCarModel'),
            array('ID' => 9, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'AllowRelease',
                'TAG' => 'AllowRelease'),
            array('ID' => 10, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'didAddressCity',
                'TAG' => 'didAddressCity'),
            array('ID' => 11, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'AddressStreet',
                'TAG' => 'AddressStreet'),
            array('ID' => 12, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'AddressNumber',
                'TAG' => 'AddressNumber'),
            array('ID' => 13, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'didTowTruck',
                'TAG' => 'didTowTruck'),
            array('ID' => 14, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'didTowDriver',
                'TAG' => 'didTowDriver'),
            array('ID' => 15, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Fine', 'TAG' => 'Fine'),
            array('ID' => 16, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'OwnerName', 'TAG' => 'OwnerName'),
            array('ID' => 17, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'ScannedDocPath',
                'TAG' => 'ScannedDocPath'),
            array('ID' => 18, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'idLastChangeUser',
                'TAG' => 'idLastChangeUser'),
            array('ID' => 19, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'IsStolen', 'TAG' => 'IsStolen'),
            array('ID' => 20, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'StolenExplanation',
                'TAG' => 'StolenExplanation'),
            array('ID' => 21, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'CarComment',
                'TAG' => 'CarComment'),
            array('ID' => 22, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'FaultCode1',
                'TAG' => 'FaultCode1'),
            array('ID' => 23, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'FaultCode2',
                'TAG' => 'FaultCode2'),
            array('ID' => 24, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Coordinates',
                'TAG' => 'Coordinates'),
            array('ID' => 25, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'txtSelectedFaultCode',
                'TAG' => 'txtSelectedFaultCode'),
            array('ID' => 26, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'PrasymasDocPath',
                'TAG' => 'PrasymasDocPath'),
            array('ID' => 27, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'AtsakymasDocPath',
                'TAG' => 'AtsakymasDocPath'),
            array('ID' => 28, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'CoordinatesLat',
                'TAG' => 'CoordinatesLat'),
            array('ID' => 29, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'CoordinatesLon',
                'TAG' => 'CoordinatesLon'),
            array('ID' => 30, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'ZoneId', 'TAG' => 'ZoneId'),
            array('ID' => 31, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Zone', 'TAG' => 'Zone')
        );

        $ret = array();
        for ($i = 1; $i <= count($data); $i++) {
            $data[$i - 1]['NAME'] = 'WCOL_' . $data[$i]['ID'];
            $ret[$i] = array_merge($fields, $data[$i - 1]);
        }
        return $ret;
    }

    public function set_params($params, $type = null)
    {
        if (func_num_args() > 1) {
            $this->ws_params[$type] = array_merge((array) $this->ws_params[$type], (array) $params);
        } else {
            foreach ($params as $type => $param) {
                $this->ws_params[$type] = array_merge((array) $this->ws_params[$type], (array) $param);
            }
        }

        return true;
    }

    public function init()
    {
        $logFile = $this->log(__CLASS__, '');

        $cr = curl_init();
        curl_setopt($cr, CURLOPT_URL, $this->ws_url);
        curl_setopt($cr, CURLOPT_HEADER, 0);
        curl_setopt($cr, CURLOPT_POST, 1);
        curl_setopt($cr, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cr, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($cr, CURLOPT_POSTFIELDS, $this->ws_params['POST']);

        $logParameters = $this->ws_params['POST'];
        $logParameters['username'] = ':)p';
        $logParameters['password'] = ':)p';
        $this->log(__CLASS__, "REQUEST:\r\n" . json_encode($logParameters) . "\r\n", $logFile);

        $response = curl_exec($cr);

        $this->log(__CLASS__, "RESPONSE:\r\n" . $response . "\r\n", $logFile);
        $this->log(__CLASS__, "INFO:\r\n" . json_encode(curl_getinfo($cr)) . "\r\n", $logFile);

        if (curl_errno($cr)) {
            $this->wp_error = curl_error($cr);
            $this->log(__CLASS__, "ERROR:\r\n" . $this->wp_error . "\r\n", $logFile);
        } else {
            $this->wp_result = $response;

            $this->log(__CLASS__, "RESULT:\r\n" . json_encode($this->result()) . "\r\n", $logFile);
        }

        curl_close($cr);

        return true;
    }

    public function result()
    {
        $arr = json_decode($this->wp_result, true);
        if (count($arr['data']) === 1) {
            return current($arr['data']);
        }
        return array();
    }

    private function getIdsByPlate($plate_nr)
    {
        $plate_nr = str_replace(' ', '', $plate_nr);
        if (empty($plate_nr)) {
            return false;
        }

        $qry = 'SELECT id ID FROM ' . $this->tables->tows . ' A WHERE A.PlateNumber = :plate AND A.didTowStatus = 4';
        $result = $this->core->db_query_fast($qry, array('plate' => $plate_nr));

        $rows_count = $this->core->db_num_rows($result);
        if ($rows_count === 0) {
            return null;
        }

        $rows = null;
        for ($i = 0; $i < $rows_count; $i++) {
            $row = $this->core->db_next($result);
            $rows[] = $row['ID'];
        }

        return $rows;
    }

    private function getTows($id)
    {
        if (empty($id)) {
            return false;
        }

        $qry = 'SELECT
				A.id `ID`,
				A.TowDocNum `DOC_ID`,
				A.RegisterTime `TIME`,
				A.PlateNumber `PLATE_NUMBER`,
				A.didCarModel `MODEL_ID`,
				B.Text `MODEL_NAME`,
				C.Text `BRAND_NAME`,
			#	A.didTowDriver `INSPECTOR_ID`,
				D.FullName `INSPECTOR_NAME`,
				CONCAT(A.AddressStreet, " ", A.AddressNumber) `ADDRESS`,
				A.txtSelectedFaultCode `PARAGRAPH`,
			#	E.Text `PARAGRAPH2`,
				A.CoordinatesLat `LATITUDE`,
				A.CoordinatesLon `LONGITUDE`,
				(
				SELECT GROUP_CONCAT(tbl.paths SEPARATOR ";")
				FROM
					(SELECT idTows `id`, CONCAT(Path, ",", ThumbnailPath) `paths`
						FROM
							' . $this->tables->pics . ' A
								INNER JOIN ' . $this->tables->tows . ' B ON B.id = A.idTows AND B.id = :id
					) as tbl
				) `PHOTOS`,
				A.ScannedDocPath `PHOTO_PAPER`,
				A.PrasymasDocPath `REQUEST`,
				A.AtsakymasDocPath `ANSWER`,
				A.CarComment `COMMENT`,
				F.Zone `ZONE`,
				F.ZoneId `ZONE_ID`
			FROM
				' . $this->tables->tows . ' A
					LEFT JOIN ' . $this->tables->zones . ' F ON F.ZoneId = A.ZoneId
					LEFT JOIN ' . $this->tables->dict . ' B ON B.Id = A.didCarModel AND B.Name = "CarModel"
					LEFT JOIN ' . $this->tables->dict . ' C ON C.Id = B.didParent AND C.Name = B.ParentName
					LEFT JOIN ' . $this->tables->users . ' D ON D.ID = A.didTowDriver
			#		LEFT JOIN ' . $this->tables->dict . ' E ON E.Id = A.FaultCode1 AND E.Name = "InspectFault"
			WHERE
				A.id = :id
			LIMIT 1';

        $result = $this->core->db_query_fast($qry, array('id' => $id));

        $rows = null;
        $rows_count = $this->core->db_num_rows($result);
        if ($rows_count) {
            for ($i = 0; $i < $rows_count; $i++) {
                $rows[$i] = $this->core->db_next($result);

                $arr = null;
                if (empty($rows[$i]['PHOTOS']) === false) {
                    $photos = explode(';', $rows[$i]['PHOTOS']);
                    foreach ($photos as $photo) {
                        $tmp = array();
                        list($tmp['PHOTO'], $tmp['THUMB']) = explode(',', $photo);
                        $arr[] = array_filter($tmp);
                    }
                    $arr = array_filter($arr);
                }
                $rows[$i]['PHOTOS'] = $arr;
            }
        }

        return $rows;
    }
}

/*
// Patikrinti ar pažeidimas egizistuoja
SELECT COUNT(*) 'cnt' FROM Tows A WHERE A.PlateNumber = 'TOYOTA' AND A.didTowStatus = 4;

// Gauna pažeidimų duomenis pagal automobilio numerį
SET group_concat_max_len = 2048;
SELECT
    A.id 'ID',
    A.TowDocNum 'DOC_ID',
    A.RegisterTime 'TIME',
    A.PlateNumber 'PLATE_NUMBER',
    A.didCarModel 'MODEL_ID',
    B.Text 'MODEL_NAME',
    C.Text 'BRAND_NAME',
#	A.didTowDriver 'INSPECTOR_ID',
    D.FullName 'INSPECTOR_NAME',
    CONCAT (A.AddressStreet, ' ', A.AddressNumber) 'ADDRESS',
    A.txtSelectedFaultCode 'PARAGRAPH',
#	E.Text 'PARAGRAPH2',
# TODO: Reikia išrinkt nuotraukas kiekvienam pažeidimui atskirai
    (
    SELECT GROUP_CONCAT(tbl.paths SEPARATOR ';')
    FROM
        (SELECT idTows 'id', CONCAT(Path, ',', ThumbnailPath) 'paths'
            FROM
                TowPictures A
                    INNER JOIN Tows B ON B.id = A.idTows/* AND B.id = 164281*\/ AND B.PlateNumber = 'TOYOTA' AND B.didTowStatus = 4
        ) as tbl
    ) 'PHOTOS',
    A.ScannedDocPath 'PHOTO_PAPER',
    A.CarComment 'COMMENT'
FROM
    Tows A
        LEFT JOIN Dictionaries B ON B.Id = A.didCarModel AND B.Name = "CarModel"
        LEFT JOIN Dictionaries C ON C.Id = B.didParent AND C.Name = B.ParentName
        LEFT JOIN Users D ON D.ID = A.didTowDriver
#		LEFT JOIN Dictionaries E ON E.Id = A.FaultCode1 AND E.Name = "InspectFault"
WHERE
    /*A.id = 164281 AND*\/
    A.PlateNumber = 'TOYOTA' AND
    A.didTowStatus = 4;

---------------------------------------------
// Patikrinti ar pažeidimas egizistuoja
// TODO: patikrinimo nereikia, sekanti užklausa pakankamai greitai išrenka duomenis pažeidimų duomenis ir nenustačius konkretaus įrašo
#SELECT COUNT(*) 'cnt' FROM Tows A WHERE A.PlateNumber = 'TOYOTA' AND A.didTowStatus = 4;

// Gauna pradinius pažeidimų duomenis
SELECT
    A.id 'ID',
    A.TowDocNum 'DOC_ID',
    A.RegisterTime 'TIME',
    A.PlateNumber 'PLATE_NUMBER',
    A.didCarModel 'MODEL_ID',
    B.Text 'MODEL_NAME',
    C.Text 'BRAND_NAME',
#	A.didTowDriver 'INSPECTOR_ID',
    D.FullName 'INSPECTOR_NAME',
    CONCAT (A.AddressStreet, ' ', A.AddressNumber) 'ADDRESS',
    A.txtSelectedFaultCode 'PARAGRAPH',
#	E.Text 'PARAGRAPH2',
    A.ScannedDocPath 'PHOTO_PAPER',
    A.CarComment 'COMMENT'
FROM
    Tows A
        LEFT JOIN Dictionaries B ON B.Id = A.didCarModel AND B.Name = "CarModel"
        LEFT JOIN Dictionaries C ON C.Id = B.didParent AND C.Name = B.ParentName
        LEFT JOIN Users D ON D.ID = A.didTowDriver
#		LEFT JOIN Dictionaries E ON E.Id = A.FaultCode1 AND E.Name = "InspectFault"
WHERE
    /*A.id = 164281 AND*\/
    A.PlateNumber = 'TOYOTA' AND
    A.didTowStatus = 4;

// Gauna nuotraukas iš pradinių duomenų perduodant pažeidimo ID
SELECT GROUP_CONCAT(tbl.paths SEPARATOR ';')
FROM
    (SELECT idTows 'id', CONCAT(Path, ',', ThumbnailPath) 'paths'
        FROM
            TowPictures A
                INNER JOIN Tows B ON B.id = A.idTows AND B.id = 440
    ) as tbl

---------------------------------------------

SELECT idTows, COUNT( * ) cnt
FROM
    TowsLog
WHERE
    didTowStatus = 4
GROUP BY idTows
ORDER BY cnt DESC*/
