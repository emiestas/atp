<?php

class SDR extends atp_ws implements ATPWS
{
    //private $ws_id = 3;
    private $ws_name = 'SDR';

    private $ws_label = 'Sodra';

    private $ws_url = 'https://edarbuotojas.vilnius.lt/subsystems/rdb/ws.php?method=getSDRRTSearchResult';

    private $ws_params;

    private $ws_result;

    private $ws_messages;

    public function SDR()
    {
        $this->ws_params['POST']['datatype'] = 'json';
        $this->ws_params['POST']['registry'] = 'sdrrt';
        $this->ws_params['POST']['username'] = '7bb08e54dfb43275087a2400451bf96be726e5b1'; //sha1('rdbNRTws');
        $this->ws_params['POST']['password'] = '93addf8424e87104d33dc33428c7e6a74f03ce2b'; //sha1('T53==+oje+tsycv3Z!2-nQbwt*R==');
        $this->ws_params['POST']['SEARCH_FROM'] = $this->ws_params['POST']['SEARCH_TILL'] = date('Y-m-d H:i:s');
    }

    public function info()
    {
        return array(
            //'id' => $this->ws_id,
            'name' => $this->ws_name,
            'label' => $this->ws_label,
            'path' => __FILE__
        );
    }

    public function params()
    {
        $data = array(
            array('ID' => 1, 'LABEL' => 'Asmens kodas', 'TAGS' => array('SEARCH_CODE')),
            array('ID' => 2, 'LABEL' => 'Vardas', 'TAGS' => array('SEARCH_FNAME')),
            array('ID' => 3, 'LABEL' => 'Pavardė', 'TAGS' => array('SEARCH_LNAME')),
            array('ID' => 4, 'LABEL' => 'Gimimo data', 'TAGS' => array('SEARCH_BDATE')),
            array('ID' => 5, 'LABEL' => 'Sodros knygutės serija', 'TAGS' => array(
                    'SEARCH_SDS')),
            array('ID' => 6, 'LABEL' => 'Sodros knygutės numeris', 'TAGS' => array(
                    'SEARCH_SDN'))/* ,
              array('ID' => 7, 'LABEL' => 'Periodas nuo', 'TAGS' => array('SEARCH_FROM')),
              array('ID' => 8, 'LABEL' => 'Periodas iki', 'TAGS' => array('SEARCH_TILL')) */
        );


        $ret = array();
        for ($i = 1; $i <= count($data); $i++) {
            $ret[$i] = $data[$i - 1];
        }
        return $ret;
    }

    public function fields()
    {
        $fields = array(
            'ID' => 0, 'ITEM_ID' => null, 'ITYPE' => 'WS',
            'LABEL' => null, 'NAME' => null, 'SORT' => null,
            'TYPE' => null, 'SOURCE' => '0', /*'VALUE' => '',
            'VALUE_TYPE' => '0', 'OTHER_SOURCE' => '0', 'OTHER_VALUE' => '0',
            'FILL' => '0',*/ 'IN_LIST' => '0', 'RELATION' => '0',
            'MANDATORY' => '0', 'VISIBLE' => '0', 'COMPETENCE' => \Atp\Document\Field::COMPETENCE_ALL,
            'ORD' => '0', 'TEXT' => '', 'DEFAULT' => '',
            'DEFAULT_ID' => '', 'UPDATE_DATE' => '2014-01-01 00:00:00', 'CREATE_DATE' => '2014-01-01 00:00:00');

        $data = array(
            array('ID' => 1, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Asmens kodas',
                'TAG' => 'ASM_KODAS'),
            array('ID' => 2, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Vardas', 'TAG' => 'VARDAS'),
            array('ID' => 3, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Pavardė', 'TAG' => 'PAVARDE'),
            array('ID' => 4, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Gimimo data',
                'TAG' => 'GIM_DATA'),
            array('ID' => 5, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Sodros knygutės serija',
                'TAG' => 'SDS'),
            array('ID' => 6, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Sodros knygutės numeris',
                'TAG' => 'SDN'),
            array('ID' => 7, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Darbovietės kodas',
                'TAG' => 'DAB_DR_KODAS'),
            array('ID' => 8, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Darbovietės pavadinimas',
                'TAG' => 'DAB_DR_PAVADINIMAS'),
            array('ID' => 9, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Dirba nuo', 'TAG' => 'DAB_DR_NUO'),
            array('ID' => 10, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Dirba iki', 'TAG' => 'DAB_DR_IKI'),
            array('ID' => 11, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Pareigos', 'TAG' => 'DAB_ASM_TIPAS'),
        );

        $ret = array();
        for ($i = 1; $i <= count($data); $i++) {
            $data[$i - 1]['NAME'] = 'WCOL_' . $data[$i]['ID'];
            $ret[$i] = array_merge($fields, $data[$i - 1]);
        }
        return $ret;
    }

    public function set_params($params, $type = null)
    {
        if (func_num_args() > 1) {
            $this->ws_params[$type] = array_merge((array) $this->ws_params[$type], (array) $params);
        } else {
            foreach ($params as $type => $param) {
                $this->ws_params[$type] = array_merge((array) $this->ws_params[$type], (array) $param);
            }
        }

        return true;
    }

    public function init()
    {
        $logFile = $this->log(__CLASS__, '');

        $cr = curl_init();
        curl_setopt($cr, CURLOPT_URL, $this->ws_url);
        curl_setopt($cr, CURLOPT_HEADER, 0);
        curl_setopt($cr, CURLOPT_POST, 1);
        curl_setopt($cr, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cr, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($cr, CURLOPT_POSTFIELDS, http_build_query($this->ws_params['POST']));

        $logParameters = $this->ws_params['POST'];
        $logParameters['username'] = ':)p';
        $logParameters['password'] = ':)p';
        $this->log(__CLASS__, "REQUEST:\r\n" . json_encode($logParameters) . "\r\n", $logFile);

        $response = curl_exec($cr);

        $this->log(__CLASS__, "RESPONSE:\r\n" . $response . "\r\n", $logFile);
        $this->log(__CLASS__, "INFO:\r\n" . json_encode(curl_getinfo($cr)) . "\r\n", $logFile);

        if (curl_errno($cr)) {
            $this->wp_error = curl_error($cr);
            $this->log(__CLASS__, "ERROR:\r\n" . $this->wp_error . "\r\n", $logFile);
        } else {
            $this->wp_result = $response;

            $this->log(__CLASS__, "RESULT:\r\n" . json_encode($this->result()) . "\r\n", $logFile);
        }

        curl_close($cr);

        return true;
    }

    public function result()
    {
        $arr = json_decode($this->wp_result, true);
        if (count($arr['data']) === 1) {
            return $this->reformat_result(current($arr['data']));
        }
        return array();
    }

    private function reformat_result_sort_by_dr_iki_desc($a, $b)
    {
        return strtotime($a["dr_iki"]) < strtotime($b["dr_iki"]);
    }

    private function reformat_result(&$result, &$arr = array(), &$prefix = '', $depth = 0)
    {
        if (is_array($result) === true) {
            foreach ($result as $key => $val) {
                if ($key === 'laikotarpiai') {
                    if (empty($val)) {
                        continue;
                    }

                    if (isset($val['ID'])) {
                        $val = array($val);
                    }

                    $current_company_key = null;
                    foreach ($val as $company_key => &$laikotarpis) {
                        if ($laikotarpis['dr_iki'] === null) {
                            $current_company_key = $company_key;
                            break;
                        }
                    }

                    // Jei šiuo metu niekur nedirba, randa darboovietę iš kurios paskutinį kartą išėjo
                    if ($current_company_key === null) {
                        if (count($val) > 1) {
                            usort($val, array($this, 'reformat_result_sort_by_dr_iki_desc'));
                        }
                        $laikotarpis = $val[0];
                    }

                    $this->{__FUNCTION__}($laikotarpis, $arr, $prefix = 'DAB_', $depth + 1);
                } else {
                    if ($val === null) {
                        continue;
                    }

                    $arr[strtoupper($prefix . $key)] = $val;
                }
            }
        }

        return $arr;
    }
}
