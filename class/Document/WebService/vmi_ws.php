<?php

class VMI extends atp_ws implements ATPWS
{
    //private $ws_id = 1;
    private $ws_name = 'VMI';

    private $ws_label = 'Valstybinė mokesčių inspekcija';

    private $ws_url = 'https://edarbuotojas.vilnius.lt/subsystems/rdb/ws.php?method=getVMISearchResult';

    private $ws_params;

    private $ws_result;

    private $ws_messages;

    public function VMI()
    {
        if ($_SERVER['REMOTE_ADDR'] === '127.0.0.1') {
            $this->ws_url = 'http://wp.dev/subsystems/rdb/ws.php?method=getVMISearchResult';
        }
        $this->ws_params['POST']['datatype'] = 'json';
        $this->ws_params['POST']['registry'] = 'vmi';
        $this->ws_params['POST']['username'] = '7bb08e54dfb43275087a2400451bf96be726e5b1'; //sha1('rdbNRTws');
        $this->ws_params['POST']['password'] = '93addf8424e87104d33dc33428c7e6a74f03ce2b'; //sha1('T53==+oje+tsycv3Z!2-nQbwt*R==');
    }

    public function info()
    {
        return array(
            //'id' => $this->ws_id,
            'name' => $this->ws_name,
            'label' => $this->ws_label,
            'path' => __FILE__
        );
    }

    public function params()
    {
        $data = array(
            array('ID' => 1, 'LABEL' => 'Identifikavimo numeris', 'TAGS' => array(
                    'SEARCH_ID')),
            array('ID' => 2, 'LABEL' => 'Asmens kodas', 'TAGS' => array('SEARCH_CODE')),
            array('ID' => 3, 'LABEL' => 'Vardas', 'TAGS' => array('SEARCH_FNAME')),
            array('ID' => 4, 'LABEL' => 'Pavardė', 'TAGS' => array('SEARCH_LNAME')),
            array('ID' => 5, 'LABEL' => 'Operacijos data nuo', 'TAGS' => array('SEARCH_FROM')),
            array('ID' => 6, 'LABEL' => 'Operacijos data iki', 'TAGS' => array('SEARCH_TILL'))
        );

        $ret = array();
        for ($i = 1; $i <= count($data); $i++) {
            $ret[$i] = $data[$i - 1];
        }
        return $ret;
    }

    public function fields()
    {
        $fields = array(
            'ID' => 0, 'ITEM_ID' => null, 'ITYPE' => 'WS',
            'LABEL' => null, 'NAME' => null, 'SORT' => null,
            'TYPE' => null, 'SOURCE' => '0', /*'VALUE' => '',
            'VALUE_TYPE' => '0', 'OTHER_SOURCE' => '0', 'OTHER_VALUE' => '0',
            'FILL' => '0',*/ 'IN_LIST' => '0', 'RELATION' => '0',
            'MANDATORY' => '0', 'VISIBLE' => '0', 'COMPETENCE' => \Atp\Document\Field::COMPETENCE_ALL,
            'ORD' => '0', 'TEXT' => '', 'DEFAULT' => '',
            'DEFAULT_ID' => '', 'UPDATE_DATE' => '2014-01-01 00:00:00', 'CREATE_DATE' => '2014-01-01 00:00:00');

        $data = array(
            array('ID' => 1, 'SORT' => 2, 'TYPE' => 7, 'LABEL' => 'Identifikavimo numeris',
                'TAG' => 'ID'),
            array('ID' => 2, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Dokumento numeris',
                'TAG' => 'DOK_NUMERIS'),
            array('ID' => 3, 'SORT' => 7, 'TYPE' => 12, 'LABEL' => 'Dokumento data',
                'TAG' => 'DOK_DATA'),
            array('ID' => 4, 'SORT' => 7, 'TYPE' => 12, 'LABEL' => 'Operacijos data',
                'TAG' => 'OPERACIJOS_DATA'),
            array('ID' => 5, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Mokėtojo kodas',
                'TAG' => 'MOKA_MM_KODAS'),
            array('ID' => 6, 'SORT' => 2, 'TYPE' => 7, 'LABEL' => 'Mokėtojas', 'TAG' => 'MOKA_MM_PAVADINIMAS'),
            array('ID' => 7, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Mokama už (kodas)',
                'TAG' => 'MOKESCIO_MOKETOJAI_KODAS'),
            array('ID' => 8, 'SORT' => 2, 'TYPE' => 7, 'LABEL' => 'Mokama už', 'TAG' => 'MOKESCIO_MOKETOJAI_PAV'),
            array('ID' => 9, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Įmokos kodas',
                'TAG' => 'IMOKOS_KODAS'),
            array('ID' => 10, 'SORT' => 1, 'TYPE' => 3, 'LABEL' => 'Mokėjimo paskirtis',
                'TAG' => 'MOKEJIMO_PASKIRTIS'),
            array('ID' => 11, 'SORT' => 1, 'TYPE' => 2, 'LABEL' => 'Valiuta', 'TAG' => 'VALIUTA'),
            array('ID' => 12, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Suma', 'TAG' => 'SUMA'),
            array('ID' => 13, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'SUMA_BV', 'TAG' => 'SUMA_BV'),
            array('ID' => 14, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Savivaldybės ID',
                'TAG' => 'SAVIVALDYBES_ID'),
            array('ID' => 15, 'SORT' => 7, 'TYPE' => 14, 'LABEL' => 'Dokumento atnaujinimo data',
                'TAG' => 'IRS_UPD_DATA'),
            array('ID' => 16, 'SORT' => 7, 'TYPE' => 14, 'LABEL' => 'Dokumento įkėlimo data',
                'TAG' => 'IRS_INS_DATA'),
            array('ID' => 17, 'SORT' => 1, 'TYPE' => 2, 'LABEL' => 'Operacijos tipas',
                'TAG' => 'OP_TIPAS')
        );

        $ret = array();
        for ($i = 1; $i <= count($data); $i++) {
            $data[$i - 1]['NAME'] = 'WCOL_' . $data[$i]['ID'];
            $ret[$i] = array_merge($fields, $data[$i - 1]);
        }
        return $ret;
    }

    public function set_params($params, $type = null)
    {
        if (func_num_args() > 1) {
            $this->ws_params[$type] = array_merge((array) $this->ws_params[$type], (array) $params);
        } else {
            foreach ($params as $type => $param) {
                $this->ws_params[$type] = array_merge((array) $this->ws_params[$type], (array) $param);
            }
        }

        return true;
    }

    public function init()
    {
        $logFile = $this->log(__CLASS__, '');

        $cr = curl_init();
        curl_setopt($cr, CURLOPT_URL, $this->ws_url);
        curl_setopt($cr, CURLOPT_HEADER, 0);
        curl_setopt($cr, CURLOPT_POST, 1);
        curl_setopt($cr, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cr, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($cr, CURLOPT_POSTFIELDS, http_build_query($this->ws_params['POST']));

        $logParameters = $this->ws_params['POST'];
        $logParameters['username'] = ':)p';
        $logParameters['password'] = ':)p';
        $this->log(__CLASS__, "REQUEST:\r\n" . json_encode($logParameters) . "\r\n", $logFile);

        $response = curl_exec($cr);

        $this->log(__CLASS__, "RESPONSE:\r\n" . $response . "\r\n", $logFile);
        $this->log(__CLASS__, "INFO:\r\n" . json_encode(curl_getinfo($cr)) . "\r\n", $logFile);

        if (curl_errno($cr)) {
            $this->wp_error = curl_error($cr);
            $this->log(__CLASS__, "ERROR:\r\n" . $this->wp_error . "\r\n", $logFile);
        } else {
            $this->wp_result = $response;

            $this->log(__CLASS__, "RESULT:\r\n" . json_encode($this->result()) . "\r\n", $logFile);
        }

        curl_close($cr);

        return true;
    }

    public function result()
    {
        $arr = json_decode($this->wp_result, true);
        if (count($arr['data']) === 1) {
            return current($arr['data']);
        }
        return array();
    }
}
