<?php

class RGT extends atp_ws implements ATPWS
{
    private $ws_name = 'RGT';

    private $ws_label = 'Regitra';

    private $ws_url = 'https://edarbuotojas.vilnius.lt/subsystems/rdb/ws.php?method=getRGTRRTSearchResult';

    private $ws_params;

    private $ws_result;

    private $ws_messages;

    private $slug;

    public function RGT()
    {
        $this->ws_params['POST']['datatype'] = 'json';
        $this->ws_params['POST']['registry'] = 'rgtrrt';
        $this->ws_params['POST']['username'] = '7bb08e54dfb43275087a2400451bf96be726e5b1'; //sha1('rdbNRTws');
        $this->ws_params['POST']['password'] = '93addf8424e87104d33dc33428c7e6a74f03ce2b'; //sha1('T53==+oje+tsycv3Z!2-nQbwt*R==');
    }

    public function info()
    {
        return array(
            //'id' => $this->ws_id,
            'name' => $this->ws_name,
            'label' => $this->ws_label,
            'path' => __FILE__
        );
    }

    public function params()
    {
        $data = array(
            array('ID' => 1, 'LABEL' => 'Valstybinis numeris', 'TAGS' => array('SEARCH_CODE'))
        );

        $ret = array();
        for ($i = 1; $i <= count($data); $i++) {
            $ret[$i] = $data[$i - 1];
        }
        return $ret;
    }

    public function fields()
    {
        $fields = array(
            'ID' => 0, 'ITEM_ID' => null, 'ITYPE' => 'WS',
            'LABEL' => null, 'NAME' => null, 'SORT' => null,
            'TYPE' => null, 'SOURCE' => '0', 'VALUE' => '',
            'VALUE_TYPE' => '0', 'OTHER_SOURCE' => '0', 'OTHER_VALUE' => '0',
            'FILL' => '0', 'VISIBILITY' => '0', 'RELATION' => '0',
            'MANDATORY' => '0', 'VISIBLE' => '0', 'COMPETENCE' => \Atp\Document\Field::COMPETENCE_ALL,
            'ORD' => '0', 'TEXT' => '', 'DEFAULT' => '',
            'DEFAULT_ID' => '', 'UPDATE_DATE' => '2014-01-01 00:00:00', 'CREATE_DATE' => '2014-01-01 00:00:00');

        $data = array(
            //array('ID' => 1, 'SORT' => 1, 'TYPE' => 1,'LABEL' => 'Valstybinis numeris', 'TAG' => 'VNZ'),
            array('ID' => 2, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės unikalus identifikacinis numeris duomenų bazėje - TPId',
                'TAG' => 'TP_DATA_KTPR_TP_ID'),
            array('ID' => 3, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės fizinio identifikavimo numeris (kėbulo numeris) - VIN kodas',
                'TAG' => 'TP_DATA_KTPR_VIN_KODAS'),
            array('ID' => 4, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Leidimo eksplotuoti numeris',
                'TAG' => 'TP_DATA_KTPR_LEN'),
            array('ID' => 5, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Šalies kodas',
                'TAG' => 'TP_DATA_KTPR_SALIS'),
            array('ID' => 6, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės žymėjimas',
                'TAG' => 'TP_DATA_KTPR_ZYMEJIMO_KODAS'),
            array('ID' => 7, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės gamintojas',
                'TAG' => 'TP_DATA_KTPR_GAMINTOJAS'),
            array('ID' => 8, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės markė',
                'TAG' => 'TP_DATA_KTPR_MARKE'),
            array('ID' => 9, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės komercinis pavadinimas',
                'TAG' => 'TP_DATA_KTPR_KOMERCINIS_PAV'),
            array('ID' => 10, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės modelis',
                'TAG' => 'TP_DATA_KTPR_MODELIS'),
            array('ID' => 11, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės tipo arba kategorijos ir klasės kodas',
                'TAG' => 'TP_DATA_KTPR_KATEGORIJA_KLASE'),
            array('ID' => 12, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės spalva',
                'TAG' => 'TP_DATA_KTPR_SPALVA'),
            array('ID' => 13, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės variklio numeris',
                'TAG' => 'TP_DATA_KTPR_VARIKLIO_NR'),
            array('ID' => 14, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės variklio darbinis tūris, kub. cm',
                'TAG' => 'TP_DATA_KTPR_VARIKLIO_TURIS'),
            array('ID' => 15, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės variklio galia, kW',
                'TAG' => 'TP_DATA_KTPR_VARIKLIO_GALIA'),
            array('ID' => 16, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės variklio pagrinidiniai degalai',
                'TAG' => 'TP_DATA_KTPR_PAGR_DEGALAI'),
            array('ID' => 17, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės variklio alternatyvūs degalai',
                'TAG' => 'TP_DATA_KTPR_ALTERN_DEGALAI'),
            array('ID' => 18, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės sėdimų vietų skaičius',
                'TAG' => 'TP_DATA_KTPR_SEDIMU_VT_SK'),
            array('ID' => 19, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės stovimų vietų skaičius',
                'TAG' => 'TP_DATA_KTPR_STOVIMU_VT_SK'),
            array('ID' => 20, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės leistinas maksimalus svoris, kg',
                'TAG' => 'TP_DATA_KTPR_LEISTINAS_MAKS_SVORIS'),
            array('ID' => 21, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės nuosavas svoris',
                'TAG' => 'TP_DATA_KTPR_NUOSAVAS_SVORIS'),
            array('ID' => 22, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Puspriekabei leistino maksimalaus svorio ir puspriekabės vilkiko balniniam sujungimui tenkančio visiškai pakrautos puspriekabės svorio dalies suma, kg', 'TAG' => 'TP_DATA_KTPR_SVORIS_PUSPRIEKABEI'),
            array('ID' => 23, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Translporto priemonės gamybos metai',
                'TAG' => 'TP_DATA_KTPR_GAMYBOS_METAI'),
            array('ID' => 24, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VZN ID', 'TAG' => 'REG_DATA_VNZ_INFO_KTPR_VNZ_ID'),
            array('ID' => 25, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Valstybinio numerio ženklo būsena',
                'TAG' => 'REG_DATA_VNZ_INFO_KTPR_VNZ_BUSENA'),
            array('ID' => 26, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Su valstybiniu numeriu susijęs registracijos padalinys',
                'TAG' => 'REG_DATA_VNZ_INFO_KTPR_VNZ_PADALINYS'),
            array('ID' => 27, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Valstybinis numeris',
                'TAG' => 'REG_DATA_VNZ_INFO_KTPR_VNZ'),
            array('ID' => 28, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Valstybinio numerio ženklo tipas',
                'TAG' => 'REG_DATA_VNZ_INFO_KTPR_VNZ_TIPAS'),
            array('ID' => 29, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'REG_DATA_MAIN_INFO_KTPR_REG_ID',
                'TAG' => 'REG_DATA_MAIN_INFO_KTPR_REG_ID'),
            array('ID' => 30, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės registracijos statusas (būsena)',
                'TAG' => 'REG_DATA_MAIN_INFO_KTPR_REGISTRAVIMO_BUSENA'),
            array('ID' => 31, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės registracijos statuso (būsenos) papildymas',
                'TAG' => 'REG_DATA_MAIN_INFO_KTPR_DALYVAVIMAS_EISME'),
            array('ID' => 32, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės pirmos registracijos data',
                'TAG' => 'REG_DATA_MAIN_INFO_KTPR_PIRMOS_REG_DATA'),
            array('ID' => 33, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Transporto priemonės pirmos registracijos data Lietuvoje',
                'TAG' => 'REG_DATA_MAIN_INFO_KTPR_PIRMOS_REG_DATA_LT'),
            array('ID' => 34, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Su transporto priemone paskutinės vykdytos operacijos data',
                'TAG' => 'REG_DATA_MAIN_INFO_KTPR_PASKUTINES_OP_DATA'),
            array('ID' => 35, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Transporto priemonės paskutinės vykdytos operacijos būsena (statusas)',
                'TAG' => 'REG_DATA_MAIN_INFO_KTPR_PASKUTINES_OP_BUSENA'),
            array('ID' => 36, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Transporto priemonės registracijos galiojimo data (tik per EUCATIS)',
                'TAG' => 'REG_DATA_MAIN_INFO_KTPR_REGISTRC_GALIOJ_DATA'),
            array('ID' => 37, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'REG_DATA_MAIN_INFO_KTPR_REG_APLINKYBE',
                'TAG' => 'REG_DATA_MAIN_INFO_KTPR_REG_APLINKYBE'),
            /**/ // BEGIN Kartojasi pagal dokumento buseną
            /* 	array('ID' => 36, 'SORT' => 1, 'TYPE' => 1,
              'LABEL' => 'Transporto priemonės registracijos dokumento (naudotojo pažymėjimo) numeris',
              'TAG' => 'REG_DATA_DOK_INFO_ISDUOTAS_KTPR_REG_DOK_NR'),
              array('ID' => 37, 'SORT' => 1, 'TYPE' => 1,
              'LABEL' => 'Registracijos dokumento tipas',
              'TAG' => 'REG_DATA_DOK_INFO_ISDUOTAS_KTPR_REG_DOK_TIPAS'),
              array('ID' => 38, 'SORT' => 1, 'TYPE' => 1,
              'LABEL' => 'Registracijos dokumento spausdinimo data',
              'TAG' => 'REG_DATA_DOK_INFO_ISDUOTAS_KTPR_REG_DOK_DATA'),
              array('ID' => 39, 'SORT' => 1, 'TYPE' => 1,
              'LABEL' => 'Registracijos dokumento būsena (statusas)',
              'TAG' => 'REG_DATA_DOK_INFO_ISDUOTAS_KTPR_REG_DOK_BUSENA'),
              array('ID' => 40, 'SORT' => 1, 'TYPE' => 1,
              'LABEL' => 'Registracijos dokumento galiojimo data',
              'TAG' => 'REG_DATA_DOK_INFO_ISDUOTAS_KTPR_REG_DOK_GALIOJ_DATA'),
              //array('ID' => 41, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Su registracijos dokumentu susijęs padalinys','TAG' => 'REG_DATA_DOK_INFO_ISDUOTAS_KTPR_REG_DOK_PADAL'),
             */
            /**/ // END Kartojasi pagal dokumento buseną

            /**/ // BEGIN Kartojasi pagal asmens rolę (savininkas, naudotojas, pirkėjas) ir asmens tipą (fizinis, juridinis)
            array('ID' => 44, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Savininko adreso eilutė',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_VIETOVE_GATVE_EILUTE'),
            array('ID' => 45, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Savininko vietovės kodas',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_VIETOVE'),
            array('ID' => 46, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Savininko gatvės kodas',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_GATVE'),
            array('ID' => 47, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Savininko namo numeris',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_NAMO_NR'),
            array('ID' => 48, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Savininko buto numeris',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_BUTO_NR'),
            array('ID' => 49, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Su transporto priemone susijusio asmens rodlė ("A" - savininkas)',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_ASMENS_ROLE'),
            array('ID' => 50, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Data nuo kada asmuo tapo susijęs su transporto priemone (savininko)',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_DATA_NUO'),
            array('ID' => 51, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Savininko šalies kodas',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_SALIES_KODAS'),
            array('ID' => 52, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Savininko struktūrizuotas adresas',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_ADRESAS_STRUKT'),
            array('ID' => 55, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Naudotojo adreso eilutė',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_VIETOVE_GATVE_EILUTE'),
            array('ID' => 56, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Naudotojo vietovės kodas',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_VIETOVE'),
            array('ID' => 57, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Naudotojo gatvės kodas',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_GATVE'),
            array('ID' => 58, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Naudotojo namo numeris',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_NAMO_NR'),
            array('ID' => 59, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Naudotojo buto numeris',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_BUTO_NR'),
            array('ID' => 60, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Su transporto priemone susijusio asmens rodlė ("N" - naudotojas)',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_ASMENS_ROLE'),
            array('ID' => 61, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Data nuo kada asmuo tapo susijęs su transporto priemone (naudotojo)',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_DATA_NUO'),
            array('ID' => 62, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Naudotojo šalies kodas',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_SALIES_KODAS'),
            array('ID' => 63, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Naudotojo struktūrizuotas adresas',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_ADRESAS_STRUKT'),
            array('ID' => 72, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Pirkėjo adreso eilutė',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKEJAS_KTPR_VIETOVE_GATVE_EILUTE'),
            array('ID' => 73, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Pirkėjo vietovės kodas',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKEJAS_KTPR_VIETOVE'),
            array('ID' => 74, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Pirkėjo gatvės kodas',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKEJAS_KTPR_GATVE'),
            array('ID' => 75, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Pirkėjo namo numeris',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKEJAS_KTPR_NAMO_NR'),
            array('ID' => 76, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Pirkėjo buto numeris',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKEJAS_KTPR_BUTO_NR'),
            array('ID' => 77, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Su transporto priemone susijusio asmens rodlė ("PIRKĖJAS")',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKEJAS_KTPR_ASMENS_ROLE'),
            array('ID' => 78, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Data nuo kada asmuo tapo susijęs su transporto priemone (pirkėjo)',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKEJAS_KTPR_DATA_NUO'),
            array('ID' => 79, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Pirkėjo šalies kodas',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKEJAS_KTPR_SALIES_KODAS'),
            array('ID' => 80, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Pirkėjo struktūrizuotas adresas',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKEJAS_KTPR_ADRESAS_STRUKT'),
            // Juridinis savininkas --------------
            array('ID' => 42, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Juridinio savininko asmens kodas',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_IMONES_KODAS'),
            array('ID' => 43, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Juridinio savininko asmens pavadinimas',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_IMONES_PAV'),
            // Juridinis naudotojas --------------
            array('ID' => 53, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Juridinio naudotojo asmens kodas',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_IMONES_KODAS'),
            array('ID' => 54, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Juridinio naudotojo asmens pavadinimas',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_IMONES_PAV'),
            // Juridinis pirkėjas --------------
            array('ID' => 53, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Juridinio pirkėjo asmens kodas',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKEJAS_KTPR_IMONES_KODAS'),
            array('ID' => 54, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Juridinio pirkėjo asmens pavadinimas',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKEJAS_N_KTPR_IMONES_PAV'),
            // Fizinis savininkas --------------
            array('ID' => 64, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Fizinio savininko asmens kodas',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_ASMENS_KODAS'),
            array('ID' => 65, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Fizinio savininko asmens vardas',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_ASMENS_VARDAS'),
            array('ID' => 66, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Fizinio savininko asmens pavardė',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_ASMENS_PAVARDE'),
            array('ID' => 67, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Fizinio savininko asmens vardas ir pavardė',
                'TAG' => 'REG_DATA_PERS_INFO_A_KTPR_ASMENS_VARDAS_PAVARDE'),
            // Fizinis naudotojas --------------
            array('ID' => 68, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Fizinio naudotojo asmens kodas',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_ASMENS_KODAS'),
            array('ID' => 69, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Fizinio naudotojo asmens vardas',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_ASMENS_VARDAS'),
            array('ID' => 70, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Fizinio naudotojo asmens pavardė',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_ASMENS_PAVARDE'),
            array('ID' => 71, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Fizinio naudotojo asmens vardas ir pavardė',
                'TAG' => 'REG_DATA_PERS_INFO_N_KTPR_ASMENS_VARDAS_PAVARDE'),
            // Fizinis pirkėjas --------------
            array('ID' => 81, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Fizinio pirkėjo asmens kodas',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKĖJAS_KTPR_ASMENS_KODAS'),
            array('ID' => 82, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Fizinio pirkėjo asmens vardas',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKĖJAS_KTPR_ASMENS_VARDAS'),
            array('ID' => 83, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Fizinio pirkėjo asmens pavardė',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKĖJAS_KTPR_ASMENS_PAVARDE'),
            array('ID' => 84, 'SORT' => 1, 'TYPE' => 1,
                'LABEL' => 'Fizinio pirkėjo asmens vardas ir pavardė',
                'TAG' => 'REG_DATA_PERS_INFO_PIRKĖJAS_KTPR_ASMENS_VARDAS_PAVARDE')
            /**/ // END Kartojasi pagal asmens rolę (savininkas, naudotojas) ir asmens tipą (fizinis, juridinis)
        );

        $ret = array();
        /* for ($i = 1; $i <= count($data); $i++) {
          //$data[$i - 1]['NAME'] = 'WCOL_' . $data[$i]['ID'];
          $data[$i - 1]['NAME'] = 'WCOL_' . $data[$i]['ID'];
          $ret[$i] = array_merge($fields, $data[$i - 1]);
          } */
        foreach ($data as &$arr) {
            $arr['NAME'] = 'WCOL_' . $arr['ID'];
            $ret[$arr['ID']] = array_merge($fields, $arr);
        }
        return $ret;
    }

    public function set_params($params, $type = null)
    {
        if (func_num_args() > 1) {
            $this->ws_params[$type] = array_merge((array) $this->ws_params[$type], (array) $params);
        } else {
            foreach ($params as $type => $param) {
                $this->ws_params[$type] = array_merge((array) $this->ws_params[$type], (array) $param);
            }
        }

        return true;
    }

    public function init()
    {
        $logFile = $this->log(__CLASS__, '');

        $cr = curl_init();
        curl_setopt($cr, CURLOPT_URL, $this->ws_url);
        curl_setopt($cr, CURLOPT_HEADER, 0);
        curl_setopt($cr, CURLOPT_POST, 1);
        curl_setopt($cr, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cr, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($cr, CURLOPT_POSTFIELDS, http_build_query($this->ws_params['POST']));

        $logParameters = $this->ws_params['POST'];
        $logParameters['username'] = ':)p';
        $logParameters['password'] = ':)p';
        $this->log(__CLASS__, "REQUEST:\r\n" . json_encode($logParameters) . "\r\n", $logFile);

        $response = curl_exec($cr);

        $this->log(__CLASS__, "RESPONSE:\r\n" . $response . "\r\n", $logFile);
        $this->log(__CLASS__, "INFO:\r\n" . json_encode(curl_getinfo($cr)) . "\r\n", $logFile);

        if (curl_errno($cr)) {
            $this->wp_error = curl_error($cr);
            $this->log(__CLASS__, "ERROR:\r\n" . $this->wp_error . "\r\n", $logFile);
        } else {
            $this->wp_result = $response;

            $this->log(__CLASS__, "RESULT:\r\n" . json_encode($this->result()) . "\r\n", $logFile);
        }

        curl_close($cr);

        return true;
    }

    public function result()
    {
        $arr = json_decode($this->wp_result, true);
        if (isset($arr['error'])) {
            if (isset($arr['error_message'])) {
                $this->set_message($arr['error_message'], 'error');
            } else {
                $this->set_message('Registras gražino klaidą', 'error');
            }
            return [];
        }

        $count = count($arr['data']['RESULTS']);
        if ($count === 1) {
            return $this->reformat_result(current($arr['data']['RESULTS']));
        } elseif($count > 1) {
            $this->set_message('Per daug rezultatų', 'notice');
        } else {
            $this->set_message('Duomenų nerasta', 'error');
        }

        return array();
    }

    private function reformat_result(&$result, &$arr = array(), &$prefix = array(), $depth = 0)
    {
        if (is_array($result) === true) {
            $this->slug = new \Slug();
            foreach ($result as $key => $val) {
                if (is_array($val) === true) {
                    if (in_array('PERS_INFO_', $prefix, true) === true) {
                        $prefix[$depth] = strtoupper($this->slug->makeSlugs($val['KTPR_ASMENS_ROLE'])) . '_';
                    } elseif (in_array('DOK_INFO_', $prefix, true) === true) {
                        $prefix[$depth] = strtoupper($this->slug->makeSlugs($val['KTPR_REG_DOK_BUSENA'])) . '_';
                    } else {
                        $prefix[$depth] = $key . '_';
                    }

                    $this->{__FUNCTION__}($val, $arr, $prefix, $depth + 1);
                    if (isset($prefix[$depth])) {
                        unset($prefix[$depth]);
                    }
                } else {
                    $new_key = join('', (array) $prefix) . $key;
                    if($new_key === 'REG_DATA_MAIN_INFO_KTPR_REGISTRAVIMO_BUSENA') {
                        switch($val) {
                            case 'R':
                                $val = 'Registruota';
                                break;
                            case 'D':
                                $val = 'Deklaruota';
                                break;
                            case 'Į':
                                $val = 'Įregistruota';
                                break;
                            case 'I':
                                $val = 'Išregistruota';
                                break;
                            case 'N':
                                $val = 'Neįregistruota';
                                break;
                        }
                    }
                    if ($new_key === 'REG_DATA_MAIN_INFO_KTPR_DALYVAVIMAS_EISME') {
                        switch ($val) {
                            case 'L':
                                $val = 'Leidžiama';
                                break;
                            case 'D':
                                $val = 'Draudžiama';
                                break;
                        }
                    }
                    $arr[$new_key] = $val;
                }
            }
        }// else
        //	$arr[join('', (array) $prefix) . $key] = $val;

        if(empty($arr['TP_DATA_KTPR_MARKE']) === false) {
            if ($arr['TP_DATA_KTPR_MARKE'] === 'VW') {
                $arr['TP_DATA_KTPR_MARKE'] = 'VOLKSWAGEN';
            }
        }

        return $arr;
    }

    public function set_message($message, $type = 'notice')
    {
        $this->ws_messages[] = array('message' => $message, 'type' => $type);
        return true;
    }

    public function get_messages()
    {
        return $this->ws_messages;
    }
}
