<?php

class GRT extends atp_ws implements ATPWS
{
    //private $ws_id = 3;
    private $ws_name = 'GRT';

    private $ws_label = 'Gyventojų registras';

    private $ws_url = 'https://edarbuotojas.vilnius.lt/subsystems/rdb/ws.php?method=getGRTSearchResult';

    private $ws_params;

    private $ws_result;

    private $ws_messages;

    public function GRT()
    {
        $this->ws_params['POST']['datatype'] = 'json';
        $this->ws_params['POST']['registry'] = 'grt';
        $this->ws_params['POST']['rcrt_registry'] = 'jar';
        $this->ws_params['POST']['username'] = '7bb08e54dfb43275087a2400451bf96be726e5b1'; //sha1('rdbNRTws');
        $this->ws_params['POST']['password'] = '93addf8424e87104d33dc33428c7e6a74f03ce2b'; //sha1('T53==+oje+tsycv3Z!2-nQbwt*R==');
    }

    public function info()
    {
        return array(
            //'id' => $this->ws_id,
            'name' => $this->ws_name,
            'label' => $this->ws_label,
            'path' => __FILE__
        );
    }

    public function params()
    {
        $data = array(
            array('ID' => 1, 'LABEL' => 'Asmens kodas', 'TAGS' => array('SEARCH_CODE')),
            array('ID' => 2, 'LABEL' => 'Vardas', 'TAGS' => array('SEARCH_FNAME')),
            array('ID' => 3, 'LABEL' => 'Pavardė', 'TAGS' => array('SEARCH_NAME')),
            array('ID' => 4, 'LABEL' => 'Adresas', 'TAGS' => array('SEARCH_ADDR')),
            array('ID' => 5, 'LABEL' => 'Gimimo data', 'TAGS' => array('SEARCH_BIRTH_DATE'))
        );

        $ret = array();
        for ($i = 1; $i <= count($data); $i++) {
            $ret[$i] = $data[$i - 1];
        }
        return $ret;
    }

    public function fields()
    {
        $fields = array(
            'ID' => 0, 'ITEM_ID' => null, 'ITYPE' => 'WS',
            'LABEL' => null, 'NAME' => null, 'SORT' => null,
            'TYPE' => null, 'SOURCE' => '0', /*'VALUE' => '',
            'VALUE_TYPE' => '0', 'OTHER_SOURCE' => '0', 'OTHER_VALUE' => '0',
            'FILL' => '0',*/ 'IN_LIST' => '0', 'RELATION' => '0',
            'MANDATORY' => '0', 'VISIBLE' => '0', 'COMPETENCE' => \Atp\Document\Field::COMPETENCE_ALL,
            'ORD' => '0', 'TEXT' => '', 'DEFAULT' => '',
            'DEFAULT_ID' => '', 'UPDATE_DATE' => '2014-01-01 00:00:00', 'CREATE_DATE' => '2014-01-01 00:00:00');

        $data = array(
            array('ID' => 1, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Identifikatorius',
                'TAG' => 'ID'),
            array('ID' => 2, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Asmens kodas',
                'TAG' => 'ASM_KODAS'),
            array('ID' => 3, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Vardas', 'TAG' => 'VARDAS'),
            array('ID' => 4, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Pavardė', 'TAG' => 'PAVARDE'),
            array('ID' => 5, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Lytis', 'TAG' => 'LYTIS'),
            array('ID' => 6, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Gimimo data',
                'TAG' => 'GIMIMO_DATA'),
            array('ID' => 7, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Gimė valstybėje',
                'TAG' => 'GIMIMO_VALST'),
            array('ID' => 8, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Adresas', 'TAG' => 'ADRESAS'/* , 'PARAM' => 'SEARCH_ADDR' */),
            array('ID' => 9, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'TER_REJ_KODAS',
                'TAG' => 'TER_REJ_KODAS'),
            array('ID' => 10, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Gatvės kodas',
                'TAG' => 'GATV_K'),
            array('ID' => 11, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Korpusas', 'TAG' => 'KORPUSAS'),
            array('ID' => 12, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Namas', 'TAG' => 'NAMAS'),
            array('ID' => 13, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Butas', 'TAG' => 'BUTAS'),
            array('ID' => 14, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'DATA_NUO', 'TAG' => 'DATA_NUO'),
            array('ID' => 15, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Šeimyninė padetis',
                'TAG' => 'SEIM_PADET'),
            array('ID' => 16, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Sutuoktinio (-ės) asmens kodas',
                'TAG' => 'SUT_ASM_KODAS'),
            array('ID' => 17, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_1_TIPAS',
                'TAG' => 'VAI_1_TIPAS'),
            array('ID' => 18, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_2_TIPAS',
                'TAG' => 'VAI_2_TIPAS'),
            array('ID' => 19, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_3_TIPAS',
                'TAG' => 'VAI_3_TIPAS'),
            array('ID' => 20, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_4_TIPAS',
                'TAG' => 'VAI_4_TIPAS'),
            array('ID' => 21, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_5_TIPAS',
                'TAG' => 'VAI_5_TIPAS'),
            array('ID' => 22, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_6_TIPAS',
                'TAG' => 'VAI_6_TIPAS'),
            array('ID' => 23, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_7_TIPAS',
                'TAG' => 'VAI_7_TIPAS'),
            array('ID' => 24, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_8_TIPAS',
                'TAG' => 'VAI_8_TIPAS'),
            array('ID' => 25, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_9_TIPAS',
                'TAG' => 'VAI_9_TIPAS'),
            array('ID' => 26, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_10_TIPAS',
                'TAG' => 'VAI_10_TIPAS'),
            array('ID' => 27, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_1_ASM_KODAS',
                'TAG' => 'VAI_1_ASM_KODAS'),
            array('ID' => 28, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_2_ASM_KODAS',
                'TAG' => 'VAI_2_ASM_KODAS'),
            array('ID' => 29, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_3_ASM_KODAS',
                'TAG' => 'VAI_3_ASM_KODAS'),
            array('ID' => 30, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_4_ASM_KODAS',
                'TAG' => 'VAI_4_ASM_KODAS'),
            array('ID' => 31, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_5_ASM_KODAS',
                'TAG' => 'VAI_5_ASM_KODAS'),
            array('ID' => 32, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_6_ASM_KODAS',
                'TAG' => 'VAI_6_ASM_KODAS'),
            array('ID' => 33, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_7_ASM_KODAS',
                'TAG' => 'VAI_7_ASM_KODAS'),
            array('ID' => 34, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_8_ASM_KODAS',
                'TAG' => 'VAI_8_ASM_KODAS'),
            array('ID' => 35, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_9_ASM_KODAS',
                'TAG' => 'VAI_9_ASM_KODAS'),
            array('ID' => 36, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'VAI_10_ASM_KODAS',
                'TAG' => 'VAI_10_ASM_KODAS'),
            array('ID' => 37, 'SORT' => 1, 'TYPE' => 1, 'LABEL' => 'Viso vaikų',
                'TAG' => 'VISO_VAIKU')
        );

        $ret = array();
        for ($i = 1; $i <= count($data); $i++) {
            $data[$i - 1]['NAME'] = 'WCOL_' . $data[$i]['ID'];
            $ret[$i] = array_merge($fields, $data[$i - 1]);
        }
        return $ret;
    }

    public function set_params($params, $type = null)
    {
        if (func_num_args() > 1) {
            $this->ws_params[$type] = array_merge((array) $this->ws_params[$type], (array) $params);
        } else {
            foreach ($params as $type => $param) {
                $this->ws_params[$type] = array_merge((array) $this->ws_params[$type], (array) $param);
            }
        }

        return true;
    }

    public function init()
    {
        $logFile = $this->log(__CLASS__, '');

        $cr = curl_init();
        curl_setopt($cr, CURLOPT_URL, $this->ws_url);
        curl_setopt($cr, CURLOPT_HEADER, 0);
        curl_setopt($cr, CURLOPT_POST, 1);
        curl_setopt($cr, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cr, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($cr, CURLOPT_POSTFIELDS, http_build_query($this->ws_params['POST']));

        $logParameters = $this->ws_params['POST'];
        $logParameters['username'] = ':)p';
        $logParameters['password'] = ':)p';
        $this->log(__CLASS__, "REQUEST:\r\n" . json_encode($logParameters) . "\r\n", $logFile);

        $response = curl_exec($cr);

        $this->log(__CLASS__, "RESPONSE:\r\n" . $response . "\r\n", $logFile);
        $this->log(__CLASS__, "INFO:\r\n" . json_encode(curl_getinfo($cr)) . "\r\n", $logFile);

        if (curl_errno($cr)) {
            $this->wp_error = curl_error($cr);
            $this->log(__CLASS__, "ERROR:\r\n" . $this->wp_error . "\r\n", $logFile);
        } else {
            $this->wp_result = $response;

            $this->log(__CLASS__, "RESULT:\r\n" . json_encode($this->result()) . "\r\n", $logFile);
        }

        curl_close($cr);

        return true;
    }
    
    public function result()
    {
        $arr = json_decode($this->wp_result, true);
		$count = count($arr['data']);
		$value = $_POST['value'];
		$addMsg = false;
		if ((is_numeric($value) != TRUE) AND (strlen($value) != 11 OR strlen($value) != 9)) {
//          $addMsg = 'Neatitinka įmonės arba asmens kodo standarto.';
		}
		if ($count === 1) {
            return current($arr['data']);
        } elseif($count > 1) {
            $this->set_message($addMsg. 'Per daug rezultatų', 'error');
        } else {
            $this->set_message($addMsg. 'GRT duomenų nerasta', 'error');
        }

        return array();
    }

    public function set_message($message, $type = 'notice')
    {
        $this->ws_messages[] = array('message' => $message, 'type' => $type);
        return true;
    }

    public function get_messages()
    {
        return $this->ws_messages;
    }
}
