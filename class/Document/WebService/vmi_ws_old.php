<?php

class WMI
{
    private $url;

    private $payment_codes;

    private $vmi_data;

    public $vmi_vars;

    public $structure;

    public function __construct()
    {
        $this->url = 'http://vmi.vilnius.vilnius.lt/ws.php';
        $this->payment_codes = array(55813, 58413, 58613, 57213);
        $this->vmi_vars = array(
            array('ID' => 1, 'NAME' => 'COL_1', 'TAG' => 'ID', 'LABEL' => 'Identifikavimo numeris',
                'TYPE' => 1),
            array('ID' => 2, 'NAME' => 'COL_2', 'TAG' => 'DOK_NUMERIS', 'LABEL' => 'Dokumento numeris',
                'TYPE' => 1),
            array('ID' => 3, 'NAME' => 'COL_3', 'TAG' => 'DOK_DATA', 'LABEL' => 'Dokumento data',
                'TYPE' => 12),
            array('ID' => 4, 'NAME' => 'COL_4', 'TAG' => 'OPERACIJOS_DATA', 'LABEL' => 'Operacijos data',
                'TYPE' => 12),
            array('ID' => 5, 'NAME' => 'COL_5', 'TAG' => 'MOKA_MM_KODAS', 'LABEL' => 'Mokėtojo kodas',
                'TYPE' => 1),
            array('ID' => 6, 'NAME' => 'COL_6', 'TAG' => 'MOKA_MM_PAVADINIMAS', 'LABEL' => 'Mokėtojas',
                'TYPE' => 7),
            array('ID' => 7, 'NAME' => 'COL_7', 'TAG' => 'MOKESCIO_MOKETOJAI_KODAS',
                'LABEL' => 'Mokama už (kodas)', 'TYPE' => 1),
            array('ID' => 8, 'NAME' => 'COL_8', 'TAG' => 'MOKESCIO_MOKETOJAI_PAV',
                'LABEL' => 'Mokama už', 'TYPE' => 7),
            array('ID' => 9, 'NAME' => 'COL_9', 'TAG' => 'IMOKOS_KODAS', 'LABEL' => 'Įmokos kodas',
                'TYPE' => 1),
            array('ID' => 10, 'NAME' => 'COL_10', 'TAG' => 'MOKEJIMO_PASKIRTIS',
                'LABEL' => 'Mokėjimo paskirtis', 'TYPE' => 3),
            array('ID' => 11, 'NAME' => 'COL_11', 'TAG' => 'VALIUTA', 'LABEL' => 'Valiuta',
                'TYPE' => 2),
            array('ID' => 12, 'NAME' => 'COL_12', 'TAG' => 'SUMA', 'LABEL' => 'Suma',
                'TYPE' => 1),
            array('ID' => 13, 'NAME' => 'COL_13', 'TAG' => 'SUMA_BV', 'LABEL' => 'SUMA_BV',
                'TYPE' => 1),
            array('ID' => 14, 'NAME' => 'COL_14', 'TAG' => 'SAVIVALDYBES_ID', 'LABEL' => 'Savivaldybės ID',
                'TYPE' => 1),
            array('ID' => 15, 'NAME' => 'COL_15', 'TAG' => 'IRS_UPD_DATA', 'LABEL' => 'Dokumento atnaujinimo data',
                'TYPE' => 5),
            array('ID' => 16, 'NAME' => 'COL_16', 'TAG' => 'IRS_INS_DATA', 'LABEL' => 'Dokumento įkėlimo data',
                'TYPE' => 5),
            array('ID' => 17, 'NAME' => 'COL_17', 'TAG' => 'OP_TIPAS', 'LABEL' => 'Operacijos tipas',
                'TYPE' => 2)
        );
    }

    public function get_web_service()
    {
        return $this->vmi_vars;
    }

    public function get($structure, $data)
    {
        $this->structure = $structure;

        $where = '';
        $payment_code = '';
        foreach ($this->payment_codes as $code) {
            if (!empty($payment_code)) {
                $payment_code .= " OR ";
            }
            $payment_code .= "IMOKOS_KODAS={$code}";
        }

        if (!empty($data)) {
            $data['COL_6'] = strtoupper($data['COL_6']);
            $where .= (!empty($data['COL_1'])) ? " AND ID={$data['COL_1']}" : '';
            $where .= (!empty($data['COL_2'])) ? " AND DOK_NUMERIS={$data['COL_2']}" : '';
            $where .= (!empty($data['COL_3'])) ? " AND TO_DATE(TO_CHAR(DOK_DATA,'YYYY-MM-DD'),'YYYY-MM-DD') == TO_DATE('{$data['COL_3']}','YYYY-MM-DD')" : '';
            $where .= (!empty($data['COL_5'])) ? " AND MOKA_MM_KODAS={$data['COL_5']}" : '';
            $where .= (!empty($data['COL_6'])) ? " AND UPPER(MOKA_MM_PAVADINIMAS) LIKE('%{$data['COL_6']}%')" : '';
            $where .= (!empty($data['COL_7'])) ? " AND SAVIVALDYBES_ID={$data['COL_7']}" : '';
            $where .= (!empty($data['COL_8'])) ? " AND IMOKOS_KODAS={$data['COL_8']}" : '';
            $where .= (!empty($data['COL_9'])) ? " AND SUMA={$data['COL_9']}" : '';
            $where .= (!empty($data['COL_10'])) ? " AND VALIUTA='{$data['COL_10']}'" : '';
            $where .= (!empty($data['COL_11'])) ? " AND MOKEJIMO_PASKIRTIS='{$data['COL_11']}'" : '';
            $where .= (!empty($data['COL_12'])) ? " AND IRS_UPD_DATA='{$data['COL_12']}'" : '';
            $where .= (!empty($data['COL_13'])) ? " AND IRS_INS_DATA='{$data['COL_13']}'" : '';
        }

        if (!empty($payment_code) && !empty($where)) {
            $postvars = "q=SELECT * FROM IMOKOS WHERE ({$payment_code}) {$where} ORDER BY OPERACIJOS_DATA ASC";

            $ch = curl_init($this->url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $this->vmi_data = curl_exec($ch);

            preg_match_all('/<row>(.*?)<\/row>/is', $this->vmi_data, $matches);
            $this->vmi_data = $matches[1];

            if (!empty($this->vmi_data)) {
                return $this->get_vars();
            }
        }
        return false;
    }

    public function get_vars()
    {
        $vmi_data = array();
        foreach ($this->vmi_data as $data) {
            $vmi_data = $this->get_tag_values($data);
        }

        return $vmi_data;
    }

    public function get_tag_values($data)
    {
        $data_arr = array();
        foreach ($this->vmi_vars as $vars) {
            preg_match('/<' . $vars['TAG'] . '>(.*?)<\/' . $vars['TAG'] . '>/is', $data, $match);
            $data_arr[] = array('NAME' => strtolower($this->structure[$vars['ID']]),
                'VALUE' => $match[1]);
        }

        return $data_arr;
    }
}
