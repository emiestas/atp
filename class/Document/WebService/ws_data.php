<?php

class WebServiceData
{
    private $name = '';

    private $label = '';

    private $url = '';

    private $params = array();

    private $result = array();

    private $messages = array();

    public function get($param, $value)
    {
        if (isset($this->{$param})) {
            return $this->{$param};
        }

        return false;
    }

    public function set($param, $value)
    {
        if (isset($this->{$param})) {
            $this->{$param} = $value;
            return true;
        }

        return false;
    }

    public function add($param, $value)
    {
        if (isset($this->{$param}) && is_array($this->{$param})) {
            $this->{$param}[] = $value;
            return true;
        }

        return false;
    }
}
