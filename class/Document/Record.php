<?php

namespace Atp\Document;

// TODO: Klasę dokumento informacijos gavimui

class Record
{
    /**
     *
     * @var \Atp\Core
     */
    private $core;

    public $record;

    /**
     *
     * @param mixed $param <p>Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param \Atp\Core $core
     */
    public function __construct($param, \Atp\Core $core)
    {
        $this->core = $core;

        $record = $this->get_record($param);
        if (empty($record)) {
            trigger_error('Record not found', E_USER_ERROR);
            exit;
        }
        $this->record = &$record;
    }

    /**
     *
     * @param mixed $param <p>Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @return array
     */
    private function get_record($param)
    {
        //if ($param instanceof Document_Record_Fake)
        //	return $param->get_record_relation($param);
        return $this->core->logic2->get_record_relation($param);
    }
}
