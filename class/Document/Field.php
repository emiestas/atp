<?php

namespace Atp\Document;

use \Atp\Entity\FieldConfiguration;
use \Atp\Entity\FieldConfigurationType;

class Field
{
    const COMPETENCE_NONE = 0;

    const COMPETENCE_ALL = 1;

    const COMPETENCE_INVESTIGATE = 2;

    const COMPETENCE_EXAMINATE = 3;

    const TYPE_ACT = 'ACT';

    const TYPE_CLASSIFICATOR = 'CLASS';

    const TYPE_CLAUSE = 'CLAUSE';

    const TYPE_DEED = 'DEED';

    const TYPE_DOCUMENT = 'DOC';

    const TYPE_WEBSERVICE = 'WS';

    private $atp;

    private $cache;

    /**
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;
        $this->cache = $this->atp->factory->Cache();
    }

    /**
     * Get field structure.
     * 
     * @param int $fieldId Field ID.
     * @return \Atp\Entity\Field
     */
    public function getField($fieldId)
    {
        return $this->atp->factory->Doctrine()
                ->getRepository('Atp\Entity\Field')
                ->find($fieldId);
    }

    /**
     * Get children fields structure of WS field.
     *
     * @param int $wsFieldId WS parent field ID.
     * @return \Atp\Entity\Field
     */
    public function getWsFieldChildren($wsFieldId)
    {
        return $this->atp->factory->Doctrine()
                ->getRepository('Atp\Entity\Field')
                ->findBy([
                    'itemType' => \Atp\Document\Field::TYPE_WEBSERVICE,
                    'itemId' => $wsFieldId
        ]);
    }

    /**
     * Get field structure.
     *
     * @param int $documentId Document ID.
     * @param string $fieldName Field name.
     * @return \Atp\Entity\Field
     * @throws \Exception
     */
    public function getFieldByName($documentId, $fieldName) {
        $fieldsMap = $this->getFieldsMapByDocument($documentId, 'assoc');
        $fieldId = $fieldsMap[$fieldName];

        if (empty($fieldId)) {
            throw new \Exception('Field with "' . $fieldName . '" name in "' . $documentId . '" document not found');
        }

        return $this->getField($fieldId);
    }

    /**
     * Get fields structures of document.
     * 
     * @param int $documentId Document ID.
     * @return \Atp\Entity\Field[]
     */
    public function getFieldsByDocument($documentId)
    {
        $cache_name = __METHOD__;
        $data = $this->cache->get($cache_name, $documentId);
        if ($data !== false) {
            return $data;
        }

        $data = $this->atp->logic2->get_fields_all([
            'ITEM_ID' => $documentId,
            'ITYPE' => 'DOC'
        ]);
        foreach ($data as $key => $field) {
            $data[$key] = $this->getField($field['ID']);
        }

        $this->cache->set($cache_name, $documentId, $data);

        return $data;
    }

    /**
     * Get fields structures map of document. $map['{fieldId}'] = '{fieldName}'; and vice versa.
     *
     * @param int $documentId Document ID.
     * @param string $type [optional] Map type. 'assoc' (name as index, id as value) arba 'enum' (id as index, name as value). <b>Default:</b> 'enum'.
     * @return strin[]|int[]
     */
    public function getFieldsMapByDocument($documentId, $type = 'enum')
    {
        $cache_name = __METHOD__;
        $cache_key = serialize(func_get_args());
        $data = $this->cache->get($cache_name, $cache_key);
        if ($data !== false) {
            return $data;
        }

        $fields = $this->getFieldsByDocument($documentId);

        if ($type === 'assoc') {
            $key = 'name';
            $value = 'id';
        } else {
            $key = 'id';
            $value = 'name';
        }

        // Užpildoma dokumento laukų struktūra ir laukų ryšiai
        foreach ($fields as $field) {
            $data[$field->$key] = $field->$value;
        }

        $this->cache->set($cache_name, $cache_key, $data);

        return $data;
    }

    /**
     * @param int $fieldId
     * @return boolean
     */
    public function isGrt($fieldId)
    {
        $result = $this->atp->db_query_fast('
            SELECT COUNT(ID) CNT
            FROM ' . PREFIX . 'ATP_WEBSERVICES
            WHERE
                FIELD_ID = "' . $this->atp->db_escape($fieldId) . '" AND
                WS_NAME = "GRT"');

        return (bool) $this->atp->db_next($result);
    }

    /**
     * Get parent field of field which is attached to classificator.
     *
     * @param int $documentId
     * @param \Atp\Entity\Field $field
     * @return \Atp\Entity\Field|null
     */
    public function getClassificatorFieldParent($documentId, $field)
    {
        /* @var $classificatorRepository \Atp\Repository\ClassificatorRepository */
        $parentClassificatorId = $this->atp->factory->Doctrine()
            ->getRepository('Atp\Entity\Classificator')
            ->getParentById($field->getItemId())
            ->getId();

        $documentFields = $this->getFieldsByDocument($documentId);
        foreach ($documentFields as $documentField) {
            if ($documentField->getItype() === \Atp\Document\Field::TYPE_DOCUMENT
                && $documentField->getType() === 18
                && $documentField->getSource() === $parentClassificatorId) {
                return $documentField;
            }
        }
    }

    /**
     *
     * @param int $recordId
     * @param int $fieldId
     * @param int $workerId
     */
    public function isEditable($recordId, $fieldId, $workerId)
    {
        $recordHelper = $this->atp->factory->Record();

        $record = $recordHelper->getEntity($recordId);
        if($record->getIsLast() !== true) {
            return false;
        }
        
        $documentId = $record->getDocument()->getId();
        $field = $this->getField($fieldId);


        // If field is attached to classificator, set its visibility by parent classificator field
        if ($field->getItype() === \Atp\Document\Field::TYPE_CLASSIFICATOR) {
            $parentField = $this->getClassificatorFieldParent($documentId, $field);
            if ($parentField) {
                return $this->isEditable($record->getId(), $parentField->getId(), $workerId);
            }
        }

        $documentHelper = $this->atp->factory->Document();
        $userHelper = $this->atp->factory->User();
//        $workerManager = new \Atp\Record\Worker($this->atp);

        $isTemporary = $recordHelper->isTemporary($record->getId());
        /* @var $document \Atp\Entity\Document */
        $document = $documentHelper->get($documentId);
        /* @var $user \Atp\Entity\User */
        $user = $userHelper->getUserDataByPersonDutyId($workerId);
        $personDuty = $userHelper->getPersonDuty($workerId);
//        $isCurrentWorker = $workerManager->isCurrentRecordWorker($record->getId(), $personDuty->id);
        $isCurrentWorker = $recordHelper->isWorker($record->getId(), $personDuty->id);

        // TODO: Kai statusas "nutrauktas" ?
//        $editableRecordStatuses = [1, 2, 3];
        $editableRecordStatuses = [
            \Atp\Record::STATUS_DISCONTINUED,
            \Atp\Record::STATUS_INVESTIGATING,
            \Atp\Record::STATUS_EXAMINING
        ];
        $fieldCompetencesForInvestigator = [
            \Atp\Document\Field::COMPETENCE_ALL,
            \Atp\Document\Field::COMPETENCE_INVESTIGATE
        ];
        $fieldCompetencesForExaminator = [
            \Atp\Document\Field::COMPETENCE_ALL,
            \Atp\Document\Field::COMPETENCE_EXAMINATE
        ];

        $isDisabled = false;
        if ($field->competence === \Atp\Document\Field::COMPETENCE_NONE) {
            $isDisabled = true;
        } else if ($isTemporary) {
            // If is temporary document
            $isDisabled = false;
        } elseif ($isCurrentWorker === false) {
            // If is not temporary and worker is not valid record worker
            $isDisabled = true;
        } elseif ($document->getDisableEdit()) {
            // If is not temporary, worker is valid, but edit is disabled
            $isDisabled = true;
        } elseif (in_array($record->getStatus(), $editableRecordStatuses) === false) {
            // If is not temporary, worker is valid, is editable, record status does not exist (?)
            $isDisabled = true;
        } elseif ($record->getStatus() == \Atp\Record::STATUS_INVESTIGATING && in_array($field->competence, $fieldCompetencesForInvestigator) === false) {
            // If is not temporary, worker is valid, is editable, record status does not match field status
            $isDisabled = true;
        } elseif ($record->getStatus() == \Atp\Record::STATUS_EXAMINING && in_array($field->competence, $fieldCompetencesForExaminator) === false) {
            // If is not temporary, worker is valid, is editable, record status does not match field status
            $isDisabled = true;
        }

        // If is not disabled, lets check if worker has has permissions
        if ($isDisabled === false) {
            // If is not temporary, worker is valid, is editable, record status match field status, worker does not have permission to work with document
            $privileges = [
                'OR' => [
                    'INVESTIGATE_RECORD',
                    'EXAMINATE_RECORD'
                ]
            ];
            $workerData = [
                'table_id' => $document->getId(),
//                'user_id' => $user->person->id,
//                'department_id' => $personDuty->department->id,
                'worker_id' => $personDuty->id
            ];

            if ($this->atp->logic->check_privileges($privileges, $workerData) === false) {
                $isDisabled = true;
            }
        }

        return !$isDisabled;
    }
}
