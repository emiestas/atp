<?php

namespace ATP\Record\Manager\Action;

class Up extends AbstractAction {

	protected $atp;
	protected $data;

	public function __construct(\ATP_Core $atp) {
		$this->atp = $atp;

		$this->d = $this->atp->fs()->Document($this->atp);
		$this->r = $this->atp->fs()->Record($this->atp);
		$this->rw = new \ATP\Record\Worker($this->atp);
		$this->rv = new \ATP\Record\Values($this->atp);
		$this->file = new \ATP\File($this->atp);
		$this->rfile = new \ATP\File\Record($this->atp);
	}

	public function init($data) {
		$this->data = $data;

		$new_id = $this->version_up();
		$this->data->record->info = $this->r->Get($new_id);

		var_dump($this->data);
		die;

		$this->save_fields();

		return true;
	}

	/**
	 * @TODO Pakeitus pagalvoti: "Ar nereikia valyti kešo?"
	 */
	private function version_up() {
		// Dokumento įrašas
		$record = $this->data->record->info;
		// Dokumento laukų struktūra
		$structure = $this->d->get_structure($record->document_id);
		// Įrašo laukų reikšmės
		$values = $this->data->record->values;
		// Įrašo laukų failai
		$rfiles = $this->data->record->files;


		// Kopijuoja ryšių informaciją
		$qry = 'SELECT MAIN_ID, RECORD_ID, TABLE_FROM_ID, TABLE_TO_ID, STATUS, IS_PAID, DOC_STATUS
			FROM ' . PREFIX . 'ATP_RECORD
			WHERE ID = :ID';
		$result = $this->atp->db_query_fast($qry, array('ID' => $record->id));
		$record_row = $this->atp->db_next($result);
		$record_row['RECORD_FROM_ID'] = $record->id;
		$record_row['IS_LAST'] = 1;
		$record_row['CREATE_DATE'] = date('Y-m-d H:i:s');
		$new_record_id = $this->atp->db_quickinsert(PREFIX . 'ATP_RECORD', $record_row);


		// Kopijuoja dokumento informaciją
		$qry = 'SELECT *
			FROM ' . PREFIX . 'ATP_TBL_' . $record->document_id . '
			WHERE ID = :ID';
		$result = $this->atp->db_query_fast($qry, array('ID' => $record->id_doc));
		$document_row = $this->atp->db_next($result);
		unset($document_row['ID']);
		$document_row['RECORD_VERSION'] = (int) $document_row['RECORD_VERSION'] + 1;
		$document_row['RECORD_LAST'] = 1;
		$document_row['CREATE_DATE'] = $record_row['CREATE_DATE'];
		$new_document_id = $this->atp->db_quickinsert(PREFIX . 'ATP_TBL_' . $record->document_id, $document_row);


		// Sukuriamas ryšys tarp įrašo ryšių ir dokumento informacijos įrašų
		$qry = 'INSERT INTO ' . PREFIX . 'ATP_RECORD_X_RELATION (DOCUMENT_ID, RECORD_ID, RELATION_ID)
			VALUES (:DOCUMENT, :ID_DOC, :RECORD)';
		$this->atp->db_query_fast($qry, array(
			'RECORD' => $new_record_id,
			'ID_DOC' => $new_document_id,
			'DOCUMENT' => $record->document_id
		));


		// Kopijuoja priskirtus darbuotojus
		$workers = $this->atp->logic2->get_record_worker(array('A.RECORD_ID = ' . $record->id));
		foreach ($workers as $worker) {
			$this->rw->Set($new_record_id, $worker['WORKER_ID'], $worker['TYPE']);
		}


		// Kopijuoja laukų reikšmes
		$values = $this->rv->GetByRecord($record->id);
		$exec = $this->atp->performance->start('Dokumento įrašo laukų kopijavimas');
		foreach ($values as $field_id => $value) {
			$field = $structure[$field_id];
			if (isset($field) === FALSE)
				continue;

			$this->rv->Set($new_record_id, $field->id, $value);
		}
		$this->rv->Save();
		$this->atp->performance->finish($exec);


		// Kopijuoja prie laukų pririštus failus
		$rfiles = $this->rfile->GetByRecord($record->id);
		// Saugomi prie laukų priskirti failai
		$exec = $this->atp->performance->start('Dokumento įrašo laukų failų ryšių kopijavimas');
		foreach ($rfiles as $rfile) {
			$field = $structure[$rfile->field_id];
			if (isset($field) === FALSE)
				continue;

			// Failas susiejamas su įrašu ir lauku
			$this->rfile->Save($rfile->file_id, $new_record_id, $field->id);
		}
		$this->atp->performance->finish($exec);



		$qry = 'UPDATE ' . PREFIX . 'ATP_RECORD
			SET IS_LAST = 0
			WHERE ID = :ID';
		$this->atp->db_query_fast($qry, array('ID' => $record->id));

		$qry = 'UPDATE ' . PREFIX . 'ATP_TBL_' . $record->document_id . '
			SET RECORD_LAST = 0
			WHERE ID = :ID';
		$this->atp->db_query_fast($qry, array('ID' => $record->id_doc));


		return $new_record_id;
	}

}
