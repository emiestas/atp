<?php

namespace ATP\Record\Manager\Action;

class Save extends AbstractAction {

	protected $atp;
	protected $data;

	public function __construct(\ATP_Core $atp) {
		$this->atp = $atp;

		$this->d = $this->atp->fs()->Document($this->atp);
		$this->r = $this->atp->fs()->Record($this->atp);
		$this->rv = new \ATP\Record\Values($this->atp);
		$this->file = new \ATP\File($this->atp);
		$this->rfile = new \ATP\File\Record($this->atp);
	}

	public function init($data) {
		$this->data = $data;

		$this->save_fields();

		return true;
	}

}
