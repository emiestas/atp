<?php

namespace ATP\Record\Manager\Action;

class AbstractAction implements IAction {

	protected function save_fields() {
		$record = $this->data->record->info;
		
		// Dokumento įrašo ID
		$id = $record->id;
		// Dokumento laukų struktūra
		$structure = $this->d->get_structure($record->document_id);
		// Įrašo laukų reikšmės
		$values = $this->data->record->values;
		// Įrašo laukų failai
		$rfiles = $this->data->record->files;


		// Saugomos laukų reikšmės
		$exec = $this->atp->performance->start('Dokumento įrašo laukų saugojimas');
		foreach ($values as $field_id => $value) {
			$field = $structure[$field_id];
			if (isset($field) === FALSE)
				continue;

			$this->rv->Set($id, $field->id, $value);
		}
		$this->rv->Save();
		$this->atp->performance->finish($exec);

		// Saugomi prie laukų priskirti failai
		$exec = $this->atp->performance->start('Dokumento įrašo failų saugojimas');
		foreach ($rfiles as $field_id => $files) {
			$field = $structure[$field_id];
			if (isset($field) === FALSE)
				continue;

			foreach ($files as $file) {

				//if (isset($file->id) && $file->id > 353967 && $id === 252340) {
				//	$this->atp->db_query_fast('DELETE FROM TST_ATP_RECORD_FILES WHERE FILE_ID = ' . $file->id);
				//}
				// Ar failas yra įkeltas į ATP
				if (empty($file->id)) {
					$file->id = $this->file->Save($file->path, $file->name, $file->type);
				}

				// Failas susiejamas su įrašu ir lauku
				$this->rfile->Save($file->id, $id, $field->id);
			}
		}
		$this->atp->performance->finish($exec);
	}

}
