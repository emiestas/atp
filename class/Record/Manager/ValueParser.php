<?php

namespace Atp\Record\Manager;

class ValueParser
{
    public function __construct()
    {
    }

    public function int($value)
    {
        return (int) $value;
    }

    public function float($value)
    {
        return (float) str_replace(',', '.', $value);
    }

    public function text($value)
    {
        return (string) $value;
    }

    public function year($value)
    {
        return $this->date_parsers($value, '#^(19|20)([0-9]{2})$#', 'Y');
    }

    public function date($value)
    {
        return $this->date_parsers($value, '#^(19|20)([0-9]{2})[-./](0[1-9]|1[012])[-./](0[1-9]|[12][0-9]|3[01])$#', 'Y-m-d');
    }

    public function time($value)
    {
        return $this->date_parsers($value, '#^([01]?[0-9]|2[0-3]):([0-5][0-9])$#', 'H:i');
    }

    public function datetime($value)
    {
        return $this->date_parsers($value, '#^(19|20)([0-9]{2})[-./](0[1-9]|1[012])[-./](0[1-9]|[12][0-9]|3[01]) ([01]?[0-9]|2[0-3]):([0-5][0-9]))$#', 'Y-m-d H:i');
    }

    private function date_parsers($value, $pattern, $default)
    {
        $is_empty = empty($value);
        if (!preg_match($pattern, $value)) {
            if ($is_empty === true) {
                $value = '';
            } else {
                $value = date($default, strtotime($value));
            }
        }

        return $value;
    }

    public function file($value)
    {
    }
}
