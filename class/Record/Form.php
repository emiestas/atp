<?php

namespace Atp\Record;

class Form
{
    /**
     * @var \Atp\Core
     */
    private $atp;

    /**
     *
     * @var \Atp\Entity\Record
     */
    private $record;

    //    /**
    //     *
    //     * @var \Atp\Record\Form\Button
    //     */
    //    private $buttonsManager;

    public function __construct(\Atp\Entity\Record $record, \Atp\Core $atp)
    {
        $this->record = $record;
        $this->atp = $atp;

        //        $this->buttonsManager = new \Atp\Record\Form\Button();
    }

    public function render()
    {
        $atp = $this->atp;

        $record = $atp->logic2->get_record_relation($this->record->getId());

        if (!empty($_POST)) {
            throw new \Ecxeption('POST data can not be present');
        }

        $recordHelper = $atp->factory->Record();

        $adminJournal = $atp->factory->AdminJournal();
        $adminJournal->addRecord($adminJournal->getVisitRecord('Dokumento įrašą "' . $this->record->getNumber() . '" (' . $this->record->getId() . ')'));


        $atp->manage->field_sort = $atp->logic->get_field_sort();
        $table_structure = $atp->logic->get_table_fields($this->record->getDocument()->getId());


        // Surenka dokumento laukų sąryšius (lauko sąryšis su teviniu lauku)
        $atp->manage->table_column_relation = [];
        foreach ($table_structure as $structure) {
            if (empty($structure['RELATION']) === false) {
                $atp->manage->table_column_relation[$structure['ID']] = [
                    'COLUMN_ID' => $structure['ID'],
                    'COLUMN_PARENT_ID' => $structure['RELATION']
                ];
            }
        }


        // Pagal laukų sąryšius suformuoja laukų hierarchiją
        foreach ($table_structure as $structure) {
            $relation_status = $atp->logic->get_column_relation_status($structure['ID'], $atp->manage->table_column_relation);
            if ($relation_status == 1) {
                $column_relations[$structure['ID']] = $atp->logic->get_column_relation_order($structure['ID'], $atp->manage->table_column_relation);
            } elseif ($relation_status == 2) {
                $column_relations[$structure['ID']] = [];
            }
        }


        // Formos laukų HTML'as
        $template_html = $atp->manage->view_column_order($column_relations, null, $this->record->getId());


        $tmpl = $atp->factory->Template();
        $tmpl->handle = 'table_record_edit_file';
        $tmpl->set_file($tmpl->handle, 'table_record_edit.tpl');

        $handles = $tmpl->set_multiblock($tmpl->handle, [
            'Marker_2',
            'select_document_template',
            'button_view_document',
            'button_register_case',
            'button_get_atpr',
            'button_get_RN',
            'create_document',
            'Marker_10',
            'create_new',
            // Įrašo ir tiriančio asmens duomenų laukas
            'doc_author_field',
            // Įrašo ir tiriančio asmens duomenys
            'doc_author',
            // Dokumento būsenos pasirinkimas
            'doc_status_option',
            // Dokumento būsenos blokas
            'doc_status_block',
            'button_create_atpr',
            'button_edit_atpr',
            'button_save', // "Išsaugoti"
            'button_save_new', // "Išsaugoti naują"
            'button_finish_investigation', // "Baigti tyrimą"
            'button_finish_examination', // "Baigti nagrinėjimą"
            'button_finish_proccess', // "Baigti procesą"
            'button_finish_idle', // "Baigti tarpinę stadiją"
            'button_register_document',
            'button_register_e_document',
        ]);

        $atp->_load_files['js'][] = 'jquery.form.min.js';


        // Blokas dokumento spausdinimui / registravimui
        // Iš viso dokumento šablonų
        $templates_count = count(
            (new \Atp\Document\Templates\TemplateNew($atp))
                ->GetTemplates(['DOCUMENT_ID' => $this->record->getDocument()->getId()])
        );

        // Jei yra priskirtų šablonų
        if (empty($templates_count) === false) {
            // Dokumento šablonai atitinkantys sąlygas
            $templateManager = new \Atp_Document_Templates_Template($atp);
            $templates = $templateManager->GetAvailable(new \Atp\Document\Record($this->record->getId(), $atp));

            // Registruoto šablono informacija
            $current_template = [];

            // Išvedamas šablonų sąrašas arba registruotas šablonas
            foreach ($templates as $template) {
                if ((int) $record['REGISTERED_TEMPLATE_ID'] !== 0) {
                    if ((int) $template['ID'] === (int) $record['REGISTERED_TEMPLATE_ID']) {
                        $tmpl->set_var([
                            'template_value' => $template['ID'],
                            'template_label' => $template['LABEL']
                        ]);
                        $tmpl->parse($handles['Marker_2'], 'Marker_2');
                        $current_template = $template;
                        break;
                    }
                    continue;
                }

                $tmpl->set_var([
                    'template_value' => $template['ID'],
                    'template_label' => $template['LABEL']
                ]);
                $tmpl->parse($handles['Marker_2'], 'Marker_2', true);
            }

            $attributes = [];
            // Jei dokumentas registruotas, tai neleidžia keisti šablono pasirinkimo
            if (empty($record['REGISTERED_TEMPLATE_ID']) === false) {
                $attributes[] = 'disabled="disabled"';
                $attributes[] = ' title="' . htmlspecialchars($current_template['LABEL']) . '"';
            }
            $tmpl->set_var('attributes', join(' ', $attributes));
            $tmpl->parse($handles['select_document_template'], 'select_document_template');

            // Mygtukas "Peržiurėti dokumentą"
            if ($this->isButtonVisible('button_view_document')) {
                $tmpl->parse($handles['button_view_document'], 'button_view_document');
            } else {
                $tmpl->set_var($handles['button_view_document'], '');
            }
            // Mygtukas "Užregistruoti bylą"
            if ($this->isButtonVisible('button_register_case')) {
                $tmpl->parse($handles['button_register_case'], 'button_register_case');
            } else {
                $tmpl->set_var($handles['button_register_case'], '');
            }
            // Mygtukai "Gauti RN" Patikrinti |RN
            if ($this->isButtonVisible('button_get_RN')) {
                $_POST['record'] = $this->record->getId();
                $postRnManager = new \Atp_Post_RN($atp);
                $rn_code = $postRnManager->get_code();
                $rn_free_code = '';
                $get_new_button = '<input class="atp-button get_RN_code" name="get_RN" type="button" value="gauti RN kodą" />';
                $rn_checkbox = ''; //$rn_checkbox = '<input type="checkbox" id="ACQ_CONFIRMATION" name="ACQ_CONFIRMATION" value="1">&nbsp;Su&nbsp;patvirtinimu.';
                if (empty($rn_code) === false && empty($postRnManager->data['POST_CODE']) === false) {
                    $rn_checkbox = ''; //$rn_checkbox = ($postRnManager->data['ACQ_CONFIRMATION'] == 1 ? ', su įteikimo patvirtinimu.' : ', be įteikimo patvirtinimo.');
                    $get_new_button = 'RN kodas: ' . $rn_code . '';
                    $rn_free_code = '<br>' . '<input class="atp-button free_RN_code" name="free_RN_code" type="button" value="atlaisvinti RN kodą" />';
                }
                $rn_info = $get_new_button . $rn_checkbox . $rn_free_code;
                $tmpl->set_var([
                    'RN_FIELD_DATA' => $rn_info
                ]);

                $tmpl->parse($handles['button_get_RN'], 'button_get_RN');
            } else {
                $tmpl->set_var($handles['button_get_RN'], '');
            }

            $tmpl->parse($handles['create_document'], 'create_document');
        }

        if (empty($templates_count) === true) {
            $tmpl->clean($handles['create_document']);
        }


        // Kai dokumentas yra sukurtas, išvesti kūrėjo informaciją
        if (empty($this->record->getId()) === false) {
            $fieldsToParse = ['TICOL_0', 'TICOL_1', 'TICOL_2', 'TICOL_3'];

            $fields = $atp->logic->record_worker_info_to_fields($this->record->getId()/* , $this->record->getDocument()->getStatus() */);
			foreach ($fields as &$field) {
                if (empty($field['VALUE']) === true || in_array($field['NAME'], $fieldsToParse) === false) {
                    continue;
                }

                $tmpl->set_var([
                    'title' => $field['LABEL'],
                    'value' => $field['VALUE']
                ]);
                $tmpl->parse($handles['doc_author_field'], 'doc_author_field', true);
            }
            $tmpl->parse($handles['doc_author'], 'doc_author');
        }


        $statuses = $atp->logic2->get_status();
        $tmpl->set_var([
            'selected' => ($selected ? ' selected="selected"' : ''),
            'value' => 0,
            'label' => $atp->lng('not_chosen')
        ]);
        $tmpl->parse($handles['doc_status_option'], 'doc_status_option');
        foreach ($statuses as $status) {
            $statusData = $atp->logic2->get_status(['ID' => $status['ID']]);

            $tmpl->set_var([
                'selected' => ($this->record->getRecordStatus() === (int) $statusData['ID'] ? ' selected="selected"' : ''),
                'value' => $statusData['ID'],
                'label' => $statusData['LABEL']
            ]);
            $tmpl->parse($handles['doc_status_option'], 'doc_status_option', true);
        }

        if ($this->record->getDocument()->getId() === \Atp\Document::DOCUMENT_CASE) {
            $tmpl->parse($handles['doc_status_block'], 'doc_status_block');
        }


        // Sekančio dokumento kūrimo blokas
        if ($this->isButtonVisible('create_new')) {
            $tmpl->set_var([
                $handles['create_new'] => $recordHelper->getNextListHtml($this->record->getId())
            ]);
        } else {
            $tmpl->clean($handles['create_new']);
        }
        // Veiksmų mygtukai
        if ($this->isButtonVisible('button_save')) {
            $tmpl->parse($handles['button_save'], 'button_save');
        } else {
            $tmpl->clean($handles['button_save']);
        }
        if ($this->isButtonVisible('button_save_new')) {
            $tmpl->parse($handles['button_save_new'], 'button_save_new');
        } else {
            $tmpl->clean($handles['button_save_new']);
        }
        if ($this->isButtonVisible('button_finish_investigation')) {
            $tmpl->parse($handles['button_finish_investigation'], 'button_finish_investigation');
        } else {
            $tmpl->clean($handles['button_finish_investigation']);
        }
        if ($this->isButtonVisible('button_finish_examination')) {
            $tmpl->parse($handles['button_finish_examination'], 'button_finish_examination');
        } else {
            $tmpl->clean($handles['button_finish_examination']);
        }
        if ($this->isButtonVisible('button_finish_proccess')) {
            $tmpl->parse($handles['button_finish_proccess'], 'button_finish_proccess');
        } else {
            $tmpl->clean($handles['button_finish_proccess']);
        }
        if ($this->isButtonVisible('button_finish_idle')) {
            $tmpl->parse($handles['button_finish_idle'], 'button_finish_idle');
        } else {
            $tmpl->clean($handles['button_finish_idle']);
        }
        // Mygtukas "Užregistruoti dokumentą"
        if ($this->isButtonVisible('button_register_document')) {
            throw new \Exception('Dokumento registravimo funkcija pašalinta, naudoti elektroninio dokumento registravimo funkciją.');
            $tmpl->parse($handles['button_register_document'], 'button_register_document');
        } else {
            $tmpl->set_var($handles['button_register_document'], '');
        }
        // Mygtukas "Užregistruoti el. dokumentą"
        if ($this->isButtonVisible('button_register_e_document')) {
            $tmpl->parse($handles['button_register_e_document'], 'button_register_e_document');
        } else {
            $tmpl->set_var($handles['button_register_e_document'], '');
        }
        // Mygtukas "Užregistruoti į atpr"
        if ($this->isButtonVisible('button_create_atpr')) {
            $tmpl->parse($handles['button_create_atpr'], 'button_create_atpr');
        } else {
            $tmpl->set_var($handles['button_create_atpr'], '');
        }
        // Mygtukas "Koreguoti atpr"
        if ($this->isButtonVisible('button_edit_atpr')) {
            $tmpl->parse($handles['button_edit_atpr'], 'button_edit_atpr');
        } else {
            $tmpl->set_var($handles['button_edit_atpr'], '');
        }
        // Mygtukas "Atnaujinti iš atpr"
        if ($this->isButtonVisible('button_get_atpr')) {
            $tmpl->parse($handles['button_get_atpr'], 'button_get_atpr');
        } else {
            $tmpl->set_var($handles['button_get_atpr'], '');
        }


        // Dokumento administravime nurodyti formos įvedimo laukų pavadinimai
        $all_fields = $atp->logic2->get_fields_all([
            'ITYPE' => 'DOC',
            'ITEM_ID' => $this->record->getDocument()->getId()
        ]);
		$formFields = [
            'violationDate' => 'violation_date',
            'decisionDate' => 'decision_date',
            'penaltySum' => 'penalty_sum',
            'classType' => 'class_type',
            'examinatorDate' => 'examinator_date',
            'examinatorStartTime' => 'examinator_start_time',
            'examinatorFinishTime' => 'examinator_finish_time',
            'personCode' => 'person_code'
        ];
        foreach ($formFields as $key => $configurationType) {
            $configurationFieldId = $atp->factory->Document()
                ->getOptions($this->record->getDocument()->getId())->$configurationType;
            $formFields[$key] = strtolower($all_fields[$configurationFieldId]['NAME']);
        }
		$tmpl->set_var('form_fields', json_encode($formFields));


        $minifyCss = function ($css) {
            //            $css = preg_replace('@(/\*.*?\*/|//.*?\n|\n|\r|\t)@', '', $css);
            $css = preg_replace('@(/\*.*?\*/|\n|\r|\t)@', '', $css);
            $css = preg_replace('/(\s{2,})/', ' ', $css);
            $css = preg_replace('/(:|;|,|{|}|>)\s/', '$1', $css);
            $css = preg_replace('/\s(:|,|{|}|>|!)/', '$1', $css);

            return $css;
        };

        $tmpl->set_var([
            'title' => $this->record->getDocument()->getLabel() . ' - ' . $this->record->getNumber(),
            'advanced_class' => $this->record->getDocument()->getId(),
            'sys_message' => $atp->get_messages(),
            // Dokumento įrašo pranešimas // Informacija apie ATPR'e sukurtą dokumentą // Informacija apie bados apmokėjimą
            'doc_message' => $this->getAtprMessage() . $this->getPaymentMessage(),
            'action_edit' => 'index.php?m=5',
            'record_id' => $this->record->getId(),
            'table_id' => $this->record->getDocument()->getId(),
            //            'table_from_id' => $atp->logic->_r_table_from_id,
            'template_html' => $template_html,
            'path_table' => $atp->manage->record_path_table($this->record->getId()),
            'js_folder' => $atp->config['JS_URL'],
            'GLOBAL_SITE_URL' => $atp->config['GLOBAL_SITE_URL'],
            'css' => $minifyCss(file_get_contents(substr($tmpl->get_file('file'), 0, -4) . '.css'))
        ]);

        /*
          // Uploadinti failai
          $file = new \Atp\File($atp, new \Atp\File\RecordFilesHandler($atp));
          $related_files = $file->GetRelatedFiles(['RECORD_ID' => $this->record->getId(), 'FIELD_ID' => 2125]);
          $file_info = $file->GetFileInfo($related_files[0]);
          // Sugeneruoti šablonai
          $file->SetHandler(new \Atp\File\GeneratedTemplatesHandler($atp));
          $related_files = $file->GetRelatedFiles(['RECORD_ID' => $this->record->getId()]);
          $file_info = $file->GetFileInfo($related_files[3]); */

        $tmpl->parse($tmpl->handle . '_out', $tmpl->handle);
        return $tmpl->get($tmpl->handle . '_out');
    }

    private function isButtonVisibleCustom($buttonName)
    {
        //        $button = $this->buttonsManager->getByName($buttonName);
        //        $resolver = new \Atp\Record\Form\ButtonResolver($button);
        //        return $resolver->isVisisble();

        $loggedInWorkerId = $this->atp->factory->User()->getLoggedPersonDuty()->id;
        $loggedInPersonId = $this->atp->factory->User()->getLoggedUser()->person->id;

        $recordManager = $this->atp->factory->Record();

        $record = $this->record;

        if ($record->getIsLast() !== true) {
            return false;
        }

        $recordId = $record->getId();


        $workerManager = new \Atp\Record\Worker($this->atp);
//        $isRecordWorker = $workerManager->isCurrentRecordWorker($recordId, $loggedInWorkerId);
        $isRecordWorker = $recordManager->isWorker($recordId, $loggedInWorkerId);


        if ($isRecordWorker === false) {
            return false;
        }

        // TODO: for now allow creation of next document everywhere
        if ($buttonName === 'create_new') {
            if($record->getDocument()->getId() !== \Atp\Document::DOCUMENT_PROTOCOL
                && $record->getDocument()->getId() !== \Atp\Document::DOCUMENT_INFRACTION_MINI) {
                return true;
            }
        }

//        $recordWorker = $workerManager->getRecordWorker([
//            'A.RECORD_ID = ' . $recordId,
//            'A.WORKER_ID = ' . $loggedInWorkerId
//            ], [
////            'LIMIT' => true,
//            'LIMIT' => false,
//            'SELECT' => 'A.ID, A.TYPE'
//            ]
//        );
        $recordWorker = $recordManager->getWorker($recordId);
//        $workerTypes = array_column($recordWorker, 'TYPE');
//        $workerType = current($workerTypes);
        $workerType = $recordWorker['TYPE'];
        $workerTypes = [$workerType];
		switch ($record->getDocument()->getId()) {
            /*
             * Pažeidimo duomenys mini
             *
             * ATPtestinis:
             * 1. Issaugoti (nebelieka sukurus sekanti dokumenta arba perdavus kitam vartotojui)
             */
            case \Atp\Document::DOCUMENT_INFRACTION_MINI:
                if ($buttonName === 'button_save') {
                    $isNext = $recordManager->isNextRecordFor($recordId);
                    if ($isNext === false) {
                        return true;
                    }
                }
                break;

            /*
             * Pazeidimo duomenys
             * 
             * ATPtyrejas:
             * 1. issaugoti (nebelieka po to kai procese sukuria protokola)
             * 2. issaugoti nauja, buna tol kol nesibaigia stadija (protokole)
             */
            case \Atp\Document::DOCUMENT_INFRACTION:
                if (in_array(\Atp\Record\Worker::TYPE_INVESTIGATOR, $workerTypes)) {
                    if (in_array($buttonName, ['button_save', 'button_save_new'])) {
                        $isNext = $recordManager->isNextDocumentFor($recordId, \Atp\Document::DOCUMENT_PROTOCOL);
                        if ($isNext === false) {
                            return true;
                        }

                        if ($buttonName === 'button_save_new') {
                            $nextRecord = $recordManager->getNextDocumentFor($recordId, \Atp\Document::DOCUMENT_PROTOCOL);
                            $validStatuses = [
                                \Atp\Record::STATUS_DISCONTINUED, // TODO: Kai statusas "nutrauktas" ?
                                \Atp\Record::STATUS_INVESTIGATING,
                                \Atp\Record::STATUS_IDLE
                            ];
                            if ($nextRecord && in_array($nextRecord->getStatus(), $validStatuses)) {
                                return true;
                            }
                        }
                    }
                }
                break;

            /*
             * Protokolas
             *
             * ATPtyrejas:
             * 1. issaugoti (nebelieka paspaudus Baigti tyrima)
             * 2. Perduoti i ATPEIR (sekmingai perdavus pasikeicia i koregavimo mygtuka)
             * 3. Koreguoti ATPEIR (mygtuka panaikinti po to kai gaunami duomenys is ATPEIR)
             * 4. Gauti duomenis is ATPEIR (atsiranda po to kai duomenys perduodami i ATEIR, panaikinti po to kai baigiama stadija)
             * 5. Uzregistruoti el.dokumenta (turi buti rodomas tada kai yra gautas pdf dokumentas is Atpeir, nebelieka po to kai jau dokumentas viena karta sekmingai perduotas i Avili)
             * 6. Baigti tyrima atsiranda, kai užregistruojamas avilio dokumentas (nebelieka viena karta paspaudus)
             * 7. Baigti tarpine stadija. po stadijos pabaigimo nebelieka jokiu mygtuku
             * 8. Sukurti naująatsiranda, kai užregistruojamas avilio dokumentas.
             *
             * ATPnagrinetojas:
             * 1. registruoti byla (uzregistravus sekmingai dingsta)
             */
            case \Atp\Document::DOCUMENT_PROTOCOL:
                if (in_array(\Atp\Record\Worker::TYPE_INVESTIGATOR, $workerTypes)) {
                    $availableButtons = [
                        'create_new',
//                        'button_save',
                        'button_create_atpr',
                        'button_edit_atpr',
                        'button_get_atpr',
                        'button_register_e_document',
                        'button_finish_investigation',
                        'button_finish_idle',
                    ];

                    $atpeirButtons = [
                        'button_create_atpr',
                        'button_edit_atpr',
                        'button_get_atpr',
                    ];

                    $validStatuses = [
                        \Atp\Record::STATUS_DISCONTINUED, // TODO: Kai statusas "nutrauktas" ?
                        \Atp\Record::STATUS_INVESTIGATING,
                        \Atp\Record::STATUS_IDLE
                    ];
                    if (in_array($buttonName, $availableButtons) && in_array($record->getStatus(), $validStatuses)) {
                        if ($buttonName === 'button_save') {
                            return $record->getStatus() === \Atp\Record::STATUS_INVESTIGATING || $record->getStatus() === \Atp\Record::STATUS_DISCONTINUED;
                        } elseif (in_array($buttonName, $atpeirButtons)) {
                            if ($recordManager->isAtpeirFor($recordId)) {
                                if ($buttonName === 'button_edit_atpr') {
                                    return $recordManager->isAtpeirResultFile($recordId) === false;
                                } else if ($buttonName === 'button_get_atpr') {
                                    return true;
                                }
                            } else {
                                return $buttonName === 'button_create_atpr';
                            }
                        } elseif ($buttonName === 'button_register_e_document') {
                            if ($recordManager->isAtpeirFor($recordId) && $recordManager->isAtpeirResultFile($recordId)) {
                                return $recordManager->isAvilysFor($recordId) === false;
                            }
                        } elseif ($buttonName === 'button_finish_investigation') {
                            if ($record->getStatus() === \Atp\Record::STATUS_INVESTIGATING || $record->getStatus() === \Atp\Record::STATUS_DISCONTINUED) {
                                return $recordManager->isAvilysRegisteredFor($recordId);
                            }
                        } elseif ($buttonName === 'button_finish_idle') {
                            return $record->getStatus() === \Atp\Record::STATUS_IDLE;
                        } elseif ($buttonName === 'create_new') {
                            return $recordManager->isAvilysRegisteredFor($recordId);
                        }
                    }
                }
                
                if (in_array(\Atp\Record\Worker::TYPE_EXAMINATOR, $workerTypes)) {
                    if ($record->getStatus() === \Atp\Record::STATUS_EXAMINING) {
                        if ($buttonName === 'button_register_case') {
                            $isNext = $recordManager->isNextDocumentFor($recordId, \Atp\Document::DOCUMENT_CASE);
                            if ($isNext === false) {
                                return true;
                            }
                        }
                    }
                }
                break;

            /*
             * Lydrastis
             *
             * ATPtyrejas:
             * 1. issaugoti (nebelieka po to kai perduoda el.dokumenta)
             * 2. Uzregistruoti el.dokumenta (nebelieka po to kai jau dokumentas viena karta sekmingai perduotas i Avili)
             * 3. issaugoti nauja, buna tol kol nesibaigia stadija (protokole) (tyrimo stadija)
             *
             * ATPnagrinetojas:
             * 1. issaugoti (nebelieka po to kai perduoda el.dokumenta)
             * 2. Uzregistruoti el.dokumenta (nebelieka po to kai jau dokumentas viena karta sekmingai perduotas i Avili)
             * 3. issaugoti nauja, buna tol kol nesibaigia stadija (protokole) (nagrinėjimo stadija)
             */
            case \Atp\Document::DOCUMENT_ACCOMPANYING:
                if (in_array(\Atp\Record\Worker::TYPE_INVESTIGATOR, $workerTypes)) {
                    $availableButtons = [
                        'button_save',
                        'button_register_e_document',
                        'button_save_new',
                    ];
                    if (in_array($buttonName, $availableButtons)) {
                        if (in_array($buttonName, ['button_save', 'button_register_e_document'])) {
                            return $recordManager->isAvilysFor($recordId) === false;
                        } else if ($buttonName === 'button_save_new') {
                            $previousRecord = $recordManager->getPreviousDocumentFor($recordId, \Atp\Document::DOCUMENT_PROTOCOL);
                            $validStatuses = [
                                \Atp\Record::STATUS_INVESTIGATING,
                                \Atp\Record::STATUS_IDLE
                            ];
                            if ($previousRecord && in_array($previousRecord->getStatus(), $validStatuses)) {
                                return true;
                            }
                        }
                    }
                } elseif (in_array(\Atp\Record\Worker::TYPE_EXAMINATOR, $workerTypes)) {
                    $availableButtons = [
                        'button_save',
                        'button_register_e_document',
                        'button_save_new',
                    ];
                    if (in_array($buttonName, $availableButtons)) {
                        if (in_array($buttonName, ['button_save', 'button_register_e_document'])) {
                            return $recordManager->isAvilysFor($recordId) === false;
                        } else if ($buttonName === 'button_save_new') {
                            $previousRecord = $recordManager->getPreviousDocumentFor($recordId, \Atp\Document::DOCUMENT_PROTOCOL);
                            $validStatuses = [
                                \Atp\Record::STATUS_EXAMINING
                            ];
                            if ($previousRecord && in_array($previousRecord->getStatus(), $validStatuses)) {
                                return true;
                            }
                        }
                    }
                }
                break;

            /*
             * Saukimas
             *
             * ATPtyrejas:
             * 1. issaugoti (nebelieka po to kai perduoda el.dokumenta)
             * 2. Uzregistruoti el.dokumenta (nebelieka po to kai jau dokumentas viena karta sekmingai perduotas i Avili)
             * 3. išsaugoti naują (rodomas, kai einamasis laikas yra vėlesnis už datą nurodytą "Atvykimo data" lauke)
             */
            case \Atp\Document::DOCUMENT_SUMMONS:
                if (in_array(\Atp\Record\Worker::TYPE_INVESTIGATOR, $workerTypes)) {
                    $availableButtons = [
                        'button_save',
                        'button_save_new',
                        'button_register_e_document',
                    ];
                    if (in_array($buttonName, $availableButtons)) {
                        if ($buttonName === 'button_save_new') {
                            $options = $this->atp->factory->Document()
                                ->getOptions($record->getDocument()->getId());
                            if ($options->arrival_date) {
                                $arrivalDate = $recordManager->getFieldValue($recordId, $options->arrival_date);
                                if ($arrivalDate) {
                                    $arrivalTimestamp = strtotime($arrivalDate);
                                    return $arrivalTimestamp > 0 && time() > $arrivalTimestamp;
                                }
                            }
                        } else {
                            return $recordManager->isAvilysFor($recordId) === false;
                        }
                    }
                }
                break;

            /*
             * Pranesimas
             *
             * ATPtyrejas:
             * 1. issaugoti (nebelieka po to kai perduoda el.dokumenta)
             * 2. Uzregistruoti el.dokumenta (nebelieka po to kai jau dokumentas viena karta sekmingai perduotas i Avili)
             * 3. išsaugoti naują (kai užregistruotas el. dokumentas)
             *
             * ATPnagrinetojas:
             * 1. issaugoti (nebelieka po to kai perduoda el.dokumenta)
             * 2. Uzregistruoti el.dokumenta (nebelieka po to kai jau dokumentas viena karta sekmingai perduotas i Avili)
             * 3. issaugoti nauja, buna tol kol nesibaigia stadija (protokole)
             */
            case \Atp\Document::DOCUMENT_REPORT:
                if (in_array(\Atp\Record\Worker::TYPE_INVESTIGATOR, $workerTypes)) {
                    $availableButtons = [
                        'button_save',
                        'button_save_new',
                        'button_register_e_document',
                    ];
                    if (in_array($buttonName, $availableButtons)) {
                        if ($recordManager->isAvilysFor($recordId)) {
                            if ($buttonName === 'button_save_new') {
                                return $recordManager->isAvilysRegisteredFor($recordId);
                            }
                        } else {
                            if (in_array($buttonName, ['button_save', 'button_register_e_document'])) {
                                return true;
                            }
                        }
                    }
                }

                if (in_array(\Atp\Record\Worker::TYPE_EXAMINATOR, $workerTypes)) {
                    $availableButtons = [
                        'button_save',
                        'button_register_e_document',
                        'button_save_new',
                    ];
                    if (in_array($buttonName, $availableButtons)) {
                        if (in_array($buttonName, ['button_save', 'button_register_e_document'])) {
                            return $recordManager->isAvilysFor($recordId) === false;
                        } else if ($buttonName === 'button_save_new') {
                            $previousRecord = $recordManager->getPreviousDocumentFor($recordId, \Atp\Document::DOCUMENT_PROTOCOL);
                            $validStatuses = [
                                \Atp\Record::STATUS_EXAMINING
                            ];
                            if ($previousRecord && in_array($previousRecord->getStatus(), $validStatuses)) {
                                    return true;
                            }
                        }
                    }
                }
                break;

            /*
             * Byla
             *
             * ATPnagrinetojas:
             * 1. issaugoti (nebelieka paspaudus Baigti nagrinejima)
             * 2. Perduoti i ATPEIR (sekmingai perdavus pasikeicia i koregavimo mygtuka)
             * 3. Koreguoti ATPEIR (mygtuka panaikinti po to kai gaunami duomenys is ATPEIR)
             * 4. Gauti duomenis is ATPEIR (atsiranda po to kai duomenys perduodami i ATEIR, panaikinti po to kai baigiama stadija)
             * 5. Uzregistruoti el.dokumenta (turi buti rodomas tada kai yra gautas pdf dokumentas is Atpeir, nebelieka po to kai jau dokumentas viena karta sekmingai perduotas i Avili)
             * 6. Baigti nagrinejima (nebelieka viena karta paspaudus)
             * 7. Baigti tarpine stadija. po stadijos pabaigimo nebelieka jokiu mygtuku
             * 8. issaugoti nauja
             */

			case \Atp\Document::DOCUMENT_CASE:
                if (in_array(\Atp\Record\Worker::TYPE_EXAMINATOR, $workerTypes)) {
                    $availableButtons = [
                        'button_save',
                        'button_create_atpr',
                        'button_edit_atpr',
                        'button_get_atpr',
                        'button_register_e_document',
                        'button_finish_examination',
                        'button_finish_idle',
                        'button_save_new',
                    ];

                    $atpeirButtons = [
                        'button_create_atpr',
                        'button_edit_atpr',
                        'button_get_atpr',
                    ];

					$validStatuses = [
                        \Atp\Record::STATUS_EXAMINING, STATUS_BOTH
                    ];
                    if (in_array($buttonName, $availableButtons) && in_array($record->getStatus(), $validStatuses)) {

                        if ($buttonName === 'button_save') {
                            return $record->getStatus() === \Atp\Record::STATUS_EXAMINING;
                        } elseif (in_array($buttonName, $atpeirButtons)) {
                            if ($recordManager->isAtpeirFor($recordId)) {
                                if ($buttonName === 'button_edit_atpr') {
                                    return $recordManager->isAtpeirResultFile($recordId) === false;
                                } else if ($buttonName === 'button_get_atpr') {
                                    return $record->getStatus() === \Atp\Record::STATUS_EXAMINING;
                                }
                            } else {
                                return $buttonName === 'button_create_atpr';
                            }
                        } elseif ($buttonName === 'button_register_e_document') {
                            if ($recordManager->isAtpeirFor($recordId) && $recordManager->isAtpeirResultFile($recordId)) {
                                return $recordManager->isAvilysFor($recordId) === false;
                            }
                        } elseif ($buttonName === 'button_finish_examination') {
                            return $record->getStatus() === \Atp\Record::STATUS_EXAMINING;
                        } elseif ($buttonName === 'button_finish_idle') {
                            return $record->getStatus() === \Atp\Record::STATUS_IDLE;
                        } elseif ($buttonName === 'button_save_new') {
                            return true;
                        }
                    }
                }
                break;
        }

        return false;
    }

    private function isButtonVisible($buttonName)
    {
        return $this->isButtonVisibleCustom($buttonName);

        $privileges_data = array(
            'user_id' => $this->atp->factory->User()->getLoggedUser()->person->id,
            'department_id' => $this->atp->factory->User()->getLoggedPersonDuty()->department->id,
            'table_id' => $this->record->getDocument()->getId(),
            'record_id' => $this->record->getId()
        );

        $loggedInWorkerId = $this->atp->factory->User()->getLoggedPersonDuty()->id;
        $loggedInPersonId = $this->atp->factory->User()->getLoggedUser()->person->id;

//        $workerManager = new \Atp\Record\Worker($this->atp);
//        $isRecordWorker = $workerManager->isCurrentRecordWorker($this->record->getId(), $loggedInWorkerId);
        $isRecordWorker = $this->atp->factory->Record()->isWorker($this->record->getId(), $loggedInWorkerId);

        if ($isRecordWorker === false) {
            return false;
        }

        // Protokole, nagrinėjimo būsenoje rodomas tik bylos registravimo mygtukas
        if ($this->record->getDocument()->getId() === \Atp\Document::DOCUMENT_PROTOCOL && $this->record->getStatus() === \Atp\Record::STATUS_EXAMINING) {
            if (in_array($buttonName, ['button_register_case', 'create_new']) === false) {
                return false;
            }
        }
        // Byloje turi buti galimybe Perduoti i ATPEIR, kurti sekancius dokumentus.
		if ($this->record->getDocument()->getId() === \Atp\Document::DOCUMENT_CASE) {
            if (in_array($buttonName, ['button_create_atpr', 'button_edit_atpr',
                    'button_edit_atpr', 'create_new', 'button_save']) === false) {
                return false;
            }
        }

        switch ($buttonName) {
            case 'button_view_document':
                return true;
                break;
            /* @deprecated */
            // Mygtukas "Užregistruoti dokumentą"
            case 'button_register_document':
                return false;
                return (
                    in_array($this->record->getDocument()->getId(), [\Atp\Document::DOCUMENT_INFRACTION]) === false
                    && (int) $record['REGISTERED_TEMPLATE_ID'] === 0
                    && $this->isButtonVisible('button_register_case') === false
                    );
                break;
            // Mygtukas "Užregistruoti el. dokumentą"
            case 'button_register_e_document':
                if (in_array($this->record->getDocument()->getId(), [\Atp\Document::DOCUMENT_INFRACTION]) === false) {
                    /* @var $repository \Atp\Repository\Avilys\DocumentRepository */
                    $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Avilys\Document');
                    if ($repository->isByRecordId($this->record->getId()) === false) {
                        return true;
                    }
                }
                break;
            // Mygtukas "Užregistruoti bylą"
            case 'button_register_case':
                // Matomas "Protokolo" dokumente, tarpinės būsenos ir nagrinėjimo stadijojose, nagrinėjančiam pareigūnui
                if ($this->record->getDocument()->getId() === \Atp\Document::DOCUMENT_PROTOCOL
                    && in_array($this->record->getStatus(), [
                        \Atp\Record::STATUS_EXAMINING,
                        \Atp\Record::STATUS_IDLE
                        ], true)) {
                    $workers = $this->atp->logic2->get_record_worker([
                        'A.RECORD_ID' => $this->record->getId(),
                        'A.WORKER_ID' => $loggedInWorkerId,
                        'A.TYPE IN("exam", "next")'
                    ]);

                    if (count($workers)) {
                        return true;
                    }
                }
                break;
            // Mygtukas "Užregistruoti į atpr"
            case 'button_create_atpr':
                if (in_array($this->record->getDocument()->getId(), [\Atp\Document::DOCUMENT_PROTOCOL,
                        \Atp\Document::DOCUMENT_CASE], true)) {
                    /* @var $repository \Atp\Repository\Atpr\AtpRepository */
                    $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Atpr\Atp');
                    if ($repository->isByRecordId($this->record->getId()) === false) {
                        return true;
                    }
                }
                break;
            // Mygtukas "Koreguoti atpr"
            case 'button_edit_atpr':
                if (in_array($this->record->getDocument()->getId(), [\Atp\Document::DOCUMENT_PROTOCOL,
                        \Atp\Document::DOCUMENT_CASE], true)) {
                    /* @var $repository \Atp\Repository\Atpr\AtpRepository */
                    $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Atpr\Atp');
                    if ($repository->isByRecordId($this->record->getId())) {
                        return true;
                    }
                }
                break;
            // Mygtukas "Atnaujinti iš atpr"
            case 'button_get_atpr':
                if (in_array($this->record->getDocument()->getId(), [\Atp\Document::DOCUMENT_PROTOCOL,
                        \Atp\Document::DOCUMENT_CASE], true)) {
                    /* @var $repository \Atp\Repository\Atpr\AtpRepository */
                    $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Atpr\Atp');
                    if ($repository->isByRecordId($this->record->getId())) {
                        return true;
                    }
                }
                break;
            // Mygtukai "Gauti RN" Patikrinti |RN
            case 'button_get_RN':
                if (in_array($this->record->getDocument()->getId(), [\Atp\Document::DOCUMENT_ACCOMPANYING,
                        \Atp\Document::DOCUMENT_SUMMONS, \Atp\Document::DOCUMENT_REPORT/* DOKUMENTO RUSIS,\Atp\Document::DOCUMENT_CASE,\Atp\Document::DOCUMENT_PROTOCOL */])) { //)/* && in_array($this->atp->factory->User()->getLoggedPersonDuty()->department->id, [1/*SKYRIAUS ID,\Atp\Document::DOCUMENT_CASE,\Atp\Document::DOCUMENT_REPORT,\Atp\Document::DOCUMENT_SUMMONS,\Atp\Document::DOCUMENT_PROTOCOL*/])*/ ) {
                    return true;
                }
                break;
            case 'create_new':
                return true;
                break;
            case 'button_save':
                $recordHelper = $this->atp->factory->Record();
                $isTemporary = $recordHelper->isTemporary($this->record->getId());

                if ($isTemporary === false && $this->record->getDocument()->getDisableEdit()) {
                    return false;
                } else
                if ($this->atp->logic->privileges('table_record_edit_button_save', $privileges_data) === true) {
                    return true;
                }
                break;
            case 'button_save_new':
                $recordHelper = $this->atp->factory->Record();
                $isTemporary = $recordHelper->isTemporary($this->record->getId());

                if ($isTemporary === false && $this->record->getDocument()->getDisableEdit()) {
                    return false;
                } else
                // Turi nebūti "Pažeidimo duomenys mini", "Pažeidimo duomenys", "Šaukimas", "Pranešimas" ir "Lydraštis" dokumentuose.
                if (in_array($this->record->getDocument()->getId(), [\Atp\Document::DOCUMENT_INFRACTION_MINI,
                        \Atp\Document::DOCUMENT_INFRACTION,
                        \Atp\Document::DOCUMENT_SUMMONS,
                        \Atp\Document::DOCUMENT_REPORT,
                        \Atp\Document::DOCUMENT_ACCOMPANYING]) === false) {
                    if ($this->atp->logic->privileges('table_record_edit_button_save_new', $privileges_data) === true) {
                        return true;
                    }
                }
                break;
            case 'button_finish_investigation':
                if (in_array($this->record->getDocument()->getId(), [\Atp\Document::DOCUMENT_PROTOCOL])) {
                    if ($this->atp->logic->privileges('table_record_edit_button_finish_investigation', $privileges_data) === true) {
                        return true;
                    }
                }
                break;
            case 'button_finish_examination':
                if ($this->atp->logic->privileges('table_record_edit_button_finish_examination', $privileges_data) === true) {
                    return true;
                }
                break;
            case 'button_finish_proccess':
                if ($this->atp->logic->privileges('table_record_edit_button_finish_proccess', $privileges_data) === true) {
                    return true;
                }
                break;
            case 'button_finish_idle':
                if ($this->atp->logic->privileges('table_record_edit_button_finish_idle', $privileges_data) === true) {
                    return true;
                }
                break;
        }

        return false;
    }

    private function getAtprMessage()
    {
        // Informacija apie ATPR'e sukurtą dokumentą
        $entityManager = $this->atp->factory->Doctrine();
        /* @var $repository \Atp\Repository\Atpr\AtpRepository */
        $repository = $entityManager->getRepository('Atp\Entity\Atpr\Atp');
        $atprDocument = $repository->getByRecordId($this->record->getId());
        if ($atprDocument) {
            return '<div class="atp-doc-message">Susijęs ATPR objektas:'
                . '<a href="index.php?m=25&id=' . $this->record->getId() . '" target="_blank">'
                . $atprDocument->getRoik()
                . '</a></div>';
        }
    }

    private function getPaymentMessage()
    {
        if (in_array($this->record->getStatus(), [\Atp\Record::STATUS_EXAMINING,
                \Atp\Record::STATUS_EXAMINED,
                \Atp\Record::STATUS_FINISHED]) === true && $this->record->getDocument()->getCanIdle()) {
            $recordObject = new \Atp\Document\Record($this->record->getId(), $this->atp);

            $paymentManager = new \Atp\Document\Record\Payment($this->atp);
            $payment_option = $paymentManager->GetRecordPaymentOption($recordObject);

            if (empty($payment_option)) {
                $paymentManager->SetRecordPaymentOption($recordObject, $paymentManager::STATUS_NOT_PAID);
                $payment_option = $paymentManager->GetRecordPaymentOption($recordObject);
            }

            // Jei baudos mokėjimas būsena yra "Apmokėta" arba "Neapmokėta"
            if (empty($payment_option) === false && in_array($payment_option['ID'], [$paymentManager::STATUS_PAID,
                    $paymentManager::STATUS_NOT_PAID], true)) {
                $message_tpl = '<div class="atp-doc-message"><div class="atp-doc-message-{$type}">{$message} ({$date}{$paid_date})</div><div class="separator"></div></div>';

                // AN būsena
                $is_an_done = $payment_option['ID'] === $paymentManager::STATUS_PAID;
                $to = ($is_an_done === true ? ['success', 'Įvykdytas AN'] : ['error',
                    'Neįvykdytas AN']);

                // Patikrinimo data
                $to[2] = 'patikrinta: ' . $payment_option['DATE'];

                // Apmokėjimo data
                $payment_date = null;
                $payments = $this->atp->logic2->get_payments([
                    'DOCUMENT_ID' => $this->record->getDocument()->getId(),
                    'RECORD_ID' => $this->record->getId()
                ]);
                foreach ($payments as &$payment) {
                    $found_payments = $this->atp->getRDBWsData('getVMISearchResult', [
                        'registry' => 'vmi',
                        'SEARCH_ID' => $payment['PAYMENT_ID']
                    ]);
                    $payment_date = max($payment_date, strtotime($found_payments[0]['OPERACIJOS_DATA']));
                }
                if (empty($payment_date) === false) {
                    $to[3] = '; apmokėta: ' . date('Y-m-d', $payment_date);
                }


                return str_replace(['{$type}', '{$message}', '{$date}', '{$paid_date}'], $to, $message_tpl);
            }
        }
    }
}
