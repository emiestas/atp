<?php

namespace Atp\Record;

class Worker
{
    const TYPE_INVESTIGATOR = 'invest';

    const TYPE_EXAMINATOR = 'exam';

    const TYPE_NEXT = 'next';

    /**
     * @var Core $atp
     */
    private $atp;

    /**
     * @var \Atp\Cache
     */
    private $cache;

    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;
        $this->cache = $this->atp->factory->Cache();
    }

    /**
     * @param int $recordId
     * @param int $workerId
     * @param string $type
     * @return int
     */
    public function addRecordWorker($recordId, $workerId, $type)
    {
        if (empty($recordId)) {
            throw new \Atp\Exception\InvalidArgumentException($recordId, 'integer');
        }
        if (empty($workerId)) {
            throw new \Atp\Exception\InvalidArgumentException($workerId, 'integer');
        }
        if (empty($type)) {
            throw new \Atp\Exception\InvalidArgumentException($type, 'string', [
            \Atp\Record\Worker::TYPE_EXAMINATOR,
            \Atp\Record\Worker::TYPE_INVESTIGATOR,
            \Atp\Record\Worker::TYPE_NEXT
            ]);
        }

        return $this->atp->db_quickinsert(PREFIX . 'ATP_RECORD_X_WORKER', [
                'RECORD_ID' => $recordId,
                'WORKER_ID' => $workerId,
                'TYPE' => $type
        ]);
    }

    /**
     *
     * @param string|strin[] $condition Example: 'RECORD_ID=1 AND TYPE="invest"'; array('RECORD_ID=1', 'TYPE="invest"')
     * @return boolean
     * @throws \Exception
     */
    public function removeRecordWorker($condition = null)
    {
        if (is_array($condition)) {
            $condition = join(
                ' AND ', array_filter(array_map('trim', $condition))
            );
        }
        $condition = trim($condition);

        if (empty($condition)) {
            throw new \Exception('At leas one parameter must be specified');
        }
        if (!preg_match('/\b(?:ID|RECORD_ID|WORKER_ID)\b/i', $condition)) {
            throw new \Exception('No mandatory parameter is set');
        }

        $this->atp->db_query_fast('
            DELETE
            FROM ' . PREFIX . 'ATP_RECORD_X_WORKER
			WHERE ' . $condition);

        return true;
    }

    /**
     * Gauna nebaigtus darbuotojo dokumentus.
     * 
     * @param array $options
     * @param int $options['DOCUMENT_ID']
     * @param int $options['WORKER_ID']
     * @param int $options['START'] [optional]
     * @param int $options['OFFSET'] [optional]
     * @param int $options['ORDER'] [optional]
     * @param boolean $count [optional] Default: false
     * @return null|int|array
     */
    public function getWorkerRecords($options, $count = false, $filterArray = array())
    {
        $defaultOptions = [
            'DOCUMENT_ID' => null,
            'WORKER_ID' => null,
            'START' => null,
            'OFFSET' => null,
            'ORDER' => null
        ];

        $options = array_merge($defaultOptions, $options);
        if (empty($options['DOCUMENT_ID']) || empty($options['WORKER_ID'])) {
            return;
        }

        $return = [];

        if ($count === true) {
            $fields = 'COUNT(A.ID) CNT';
        } else {
            $fields = 'A.ID';
        }

		// FILTER array SQL
		$filter_sql = false;
		foreach ($filterArray as $fkey => $fval) {
			if (strpos($fkey, 'FILTER') !== FALSE) {
				if ($fkey == 'FILTER_RECORD_ID') {
					if (empty($filterArray['FILTER_RECORD_ID']) === FALSE) {
						$filter_sql.= "AND A.RECORD_ID LIKE '%".CleanText($filterArray['FILTER_RECORD_ID'])."%' ";
					}
				} else if ($fkey == 'FILTER_STATUS') {
					if (empty($filterArray['FILTER_STATUS']) === FALSE) {
						$filter_sql.= "AND A.RECORD_ID IN (SELECT RECORD_ID FROM ".PREFIX."ATP_RECORD WHERE DOC_STATUS IN (SELECT ID FROM ".PREFIX."ATP_STATUS WHERE LOWER(LABEL) LIKE LOWER('%".my_strtolower(CleanText($filterArray['FILTER_STATUS']))."%')))";
					}
				} else if ($fkey == 'FILTER_ROIK') {
					$filter_sql.= "AND A.ID IN (SELECT RECORD_ID FROM ".PREFIX."ATP_ATPR_ATP WHERE LOWER(ROIK) LIKE LOWER('%".my_strtolower(CleanText($filterArray['FILTER_ROIK']))."%')) ";
				} else {
					$true_name = str_replace('FILTER_', '', $fkey);
					$true_name = str_replace('_clause', '', $true_name);
					if (empty($filterArray[$fkey]) === FALSE) {
						$field_info = array();
						if (strpos($true_name, 'WCOL') === FALSE) {
							$field_info = $this->atp->db_getItemBy2Columns(PREFIX."ATP_FIELD", 'ITEM_ID', 'NAME', $options['DOCUMENT_ID'], $true_name);
						} else {
							$field_info = $this->atp->db_getItemByColumn(PREFIX."ATP_FIELD", 'NAME', $true_name);
						}
						if ($field_info['TYPE'] == '23') {
							if (strpos($fkey, 'clause') !== FALSE) {
								$filter_sql.=" AND (A.RECORD_ID IN (SELECT RECORD_ID FROM ".PREFIX."ATP_TBL_".(int) $options['DOCUMENT_ID']." WHERE ".$true_name." IN (SELECT DEED_ID FROM ".PREFIX."ATP_DEED_EXTENDS WHERE ITYPE = 'CLAUSE' AND ITEM_ID IN (SELECT ID FROM ".PREFIX."ATP_CLAUSES WHERE LOWER(NAME) LIKE LOWER('%".my_strtolower(CleanText($filterArray[$fkey]))."%'))))) ";
							} else {
								$filter_sql.=" AND (A.RECORD_ID IN (SELECT RECORD_ID FROM ".PREFIX."ATP_TBL_".(int) $options['DOCUMENT_ID']." WHERE ".$true_name." IN (SELECT ID FROM ".PREFIX."ATP_DEEDS WHERE LOWER(LABEL) LIKE LOWER('%".my_strtolower(CleanText($filterArray[$fkey]))."%')))) ";
							}
						}
						else if ($field_info['TYPE'] == '18') {
							$filter_sql.=" AND (A.RECORD_ID IN (SELECT RECORD_ID FROM ".PREFIX."ATP_TBL_".(int) $options['DOCUMENT_ID']." WHERE ".$true_name." IN (SELECT ID FROM ".PREFIX."ATP_CLASSIFICATORS WHERE LOWER(LABEL) LIKE LOWER('%".my_strtolower(CleanText($filterArray[$fkey]))."%')))) ";
						} 
						else if ($field_info['TYPE'] == '7') {
							if (strpos($true_name, 'WCOL') === FALSE) {
								$filter_sql.= "AND (A.ID IN (SELECT RECORD_ID FROM ".PREFIX."ATP_FIELDS_VALUES WHERE LOWER(VALUE) LIKE LOWER('%".my_strtolower(CleanText($filterArray[$fkey]))."%') AND FIELD_ID IN (SELECT ID FROM ".PREFIX."ATP_FIELD WHERE NAME = '".$true_name."')) ) OR ((A.RECORD_ID IN (SELECT RECORD_ID FROM ".PREFIX."ATP_TBL_".(int) $options['DOCUMENT_ID']." WHERE LOWER(".$true_name.") LIKE LOWER('%".my_strtolower(CleanText($filterArray[$fkey]))."%')))) ";
							} else {
								$filter_sql.= "AND A.ID IN (SELECT RECORD_ID FROM ".PREFIX."ATP_FIELDS_VALUES WHERE LOWER(VALUE) LIKE LOWER('%".my_strtolower(CleanText($filterArray[$fkey]))."%') AND FIELD_ID IN (SELECT ID FROM ".PREFIX."ATP_FIELD WHERE NAME = '".$true_name."'))";
							}
						} else {
							$filter_sql.=" AND (A.RECORD_ID IN (SELECT RECORD_ID FROM ".PREFIX."ATP_TBL_".(int) $options['DOCUMENT_ID']." WHERE LOWER(".$true_name.") LIKE LOWER('%".my_strtolower(CleanText($filterArray[$fkey]))."%'))) ";
						}
					}
				}
			}
		}

		$qry = 'SELECT ' . $fields . '
			FROM ' . PREFIX . 'ATP_RECORD A
				INNER JOIN `' . PREFIX . 'ATP_RECORD_X_WORKER` B ON
					B.RECORD_ID = A.ID AND
                    B.WORKER_ID = ' . (int) $options['WORKER_ID'] . ' AND
                    A.STATUS != '.\Atp\Record::STATUS_FINISHED.'
				LEFT JOIN `' . PREFIX . 'ATP_RECORD_UNSAVED` C ON
                    C.RECORD_NR = A.RECORD_ID
			WHERE 1=1 '.$filter_sql.' AND 
				A.TABLE_TO_ID = ' . (int) $options['DOCUMENT_ID'] . ' AND
				A.IS_LAST = 1 AND
				A.IN_LIST = 1 AND
				C.RECORD_NR IS NULL';
        if ($options['ORDER'] !== null) {
            $qry .= ' ORDER BY ' . $options['ORDER'];
        }
        if ($options['START'] !== null && $options['OFFSET'] !== null) {
            $qry .= ' LIMIT ' . (int) $options['START'] . ', ' . (int) $options['OFFSET'];
        }
        $result = $this->atp->db_query_fast($qry);

        if ($count === true) {
            $row = $this->atp->db_next($result);
            return (int) $row['CNT'];
        }

        while ($row = $this->atp->db_next($result)) {
            $return[] = $row['ID'];
        }

        return $return;
    }

    /**
     * Gauna WebPartner darbuotojų informaciją
     * @param array $params [optional] Parametrų masyvas
     * @param array $options [optional]
     * @param string $options['SELECT'] [optional] Duomenų bazės laukai, kuriuos surinkti. Default: 'A.ID, E.ID AS USER_ID, A.PEOPLE_ID, B.FIRST_NAME, B.LAST_NAME, A.STRUCTURE_ID, C.SHOWS AS STRUCTURE_NAME, A.CONTACT_ID, A.OFFICE_ID'
     * @param string $options['ORDER'] [optional] SQL query order string.
     * @param bool $options['LIMIT'] [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @return array Darbuotojų informacija
     */
    public function getWorkerInfo(array $params = null, array $options = null)
    {
        $defaultOptions = [
            'SELECT' => 'A.ID, E.ID AS USER_ID, A.PEOPLE_ID, B.FIRST_NAME, B.LAST_NAME, A.STRUCTURE_ID, C.SHOWS AS STRUCTURE_NAME, A.CONTACT_ID, A.OFFICE_ID',
            'ORDER' => null,
            'LIMIT' => false
        ];

        $options = array_merge($defaultOptions, $options);

        // TODO: laikinai
        if (isset($params['DEPARTMENT_ID'])) {
            $params['STRUCTURE_ID'] = $params['DEPARTMENT_ID'];
            unset($params['DEPARTMENT_ID']);
        }
        // TODO: laikinai
        if (isset($params['PERSON_ID'])) {
            $params['PEOPLE_ID'] = $params['PERSON_ID'];
            unset($params['PERSON_ID']);
        }

        $joins = [
            'B' => 'INNER JOIN `' . PREFIX . 'MAIN_PEOPLE` B ON B.ID = A.PEOPLE_ID',
            'E' => 'LEFT JOIN `' . PREFIX . 'ADM_USERS` E ON E.PEOPLE_ID = A.PEOPLE_ID',
            'F' => 'LEFT JOIN `' . PREFIX . 'MAIN_OFFICE` F ON F.ID = A.OFFICE_ID',
            'C' => 'INNER JOIN `' . PREFIX . 'MAIN_STRUCTURE` C ON C.ID = A.STRUCTURE_ID',
            'D' => 'LEFT JOIN `' . PREFIX . 'MAIN_STRUCTURETYPE` D ON D.ID = C.TYPE_ID'
        ];

        return $this->atp->logic2->get_db(
                PREFIX . 'MAIN_WORKERS', $params, $options['LIMIT'], $options['ORDER'], false, $options['SELECT'], $joins
        );
    }

    /**
     * Gauna informaciją iš dokumento įrašų ir asmens ryšių
     * @param array $params [optional] Parametrų masyvas
     * @param array $options [optional]
     * @param string $options['SELECT'] [optional] Duomenų bazės laukai, kuriuos surinkti. Default: 'A.ID, E.ID AS USER_ID, A.PEOPLE_ID, B.FIRST_NAME, B.LAST_NAME, A.STRUCTURE_ID, C.SHOWS AS STRUCTURE_NAME, A.CONTACT_ID, A.OFFICE_ID'
     * @param string $options['ORDER'] [optional] SQL query order string.
     * @param bool $options['LIMIT'] [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @return array
     */
    public function getRecordWorker(array $params = null, array $options = null)
    {
        $defaultOptions = [
            'SELECT' => 'A.ID, A.RECORD_ID, F.RECORD_ID as "RECORD_NR", A.WORKER_ID, A.TYPE, G.ID AS USER_ID,
				D.PEOPLE_ID, B.FIRST_NAME, B.LAST_NAME,
				D.STRUCTURE_ID, C.SHOWS AS STRUCTURE_NAME,
				D.CONTACT_ID, D.OFFICE_ID',
            'ORDER' => null,
            'LIMIT' => false
        ];

        $options = array_merge($defaultOptions, $options);

        $joins = [
            'D' => 'INNER JOIN `' . PREFIX . 'MAIN_WORKERS` D ON D.ID = A.WORKER_ID',
            'B' => 'INNER JOIN `' . PREFIX . 'MAIN_PEOPLE` B ON B.ID = D.PEOPLE_ID',
            'G' => 'LEFT JOIN `' . PREFIX . 'ADM_USERS` G ON G.PEOPLE_ID = D.PEOPLE_ID',
            'H' => 'LEFT JOIN `' . PREFIX . 'MAIN_OFFICE` G ON G.ID = D.OFFICE_ID',
            'C' => 'INNER JOIN `' . PREFIX . 'MAIN_STRUCTURE` C ON C.ID = D.STRUCTURE_ID',
            'E' => 'LEFT JOIN `' . PREFIX . 'MAIN_STRUCTURETYPE` E ON E.ID = C.TYPE_ID',
            'F' => 'INNER JOIN `' . PREFIX . 'ATP_RECORD` F ON F.ID = A.RECORD_ID'
        ];

        return $this->atp->logic2->get_db(
                PREFIX . 'ATP_RECORD_X_WORKER', $params, $options['LIMIT'], $options['ORDER'], false, $options['SELECT'], $joins
        );
    }

    /**
     * Check whether worker is addedd to record document as main worker (is allowed to work with record document).
     * 
     * @param int $recordId
     * @param int $workerId
     * @return boolean
     */
    public function isCurrentRecordWorker($recordId, $workerId)
    {
        $cacheName = __METHOD__;
        $key = serialize(func_get_args());
        if ($this->cache->is($cacheName, $key)) {
            return $this->cache->get($cacheName, $key);
        }

        $return = false;

        $result = $this->atp->db_query_fast('
            SELECT
                A.TYPE,
                B.STATUS
            FROM
                ' . PREFIX . 'ATP_RECORD_X_WORKER A
                    INNER JOIN ' . PREFIX . 'ATP_RECORD B ON
                        B.ID = A.RECORD_ID
			WHERE
                A.RECORD_ID = :RECORD_ID AND
                A.WORKER_ID = :WORKER_ID', [
            'RECORD_ID' => $recordId,
            'WORKER_ID' => $workerId
        ]);

//        $recordStatus = null;
//        $availableStatuses = $this->atp->logic->get_document_statuses();

        $return = false;

        $count = $this->atp->db_num_rows($result);
        for ($i = 0; $i < $count; $i++) {
            $row = $this->atp->db_next($result);

//            if (empty($recordStatus)) {
//                if (isset($availableStatuses[$row['STATUS']])) {
//                    $recordStatus = $availableStatuses[$row['STATUS']];
//                } else {
//                    break;
//                }
//            }

            if (in_array($row['TYPE'], [\Atp\Record\Worker::TYPE_INVESTIGATOR, \Atp\Record\Worker::TYPE_EXAMINATOR]) === false) {
                continue;
            }

//            switch ($recordStatus['ID']) {
            switch ($row['STATUS']) {
                case \Atp\Record::STATUS_DELETED:
                    $return = false;
                    break;
                // TODO: Kai statusas "nutrauktas" ?
                // Buna perdavus Protokolą į Bylą
                case \Atp\Record::STATUS_DISCONTINUED:
//                    $return = false;
                    $return = (
                        $row['TYPE'] === \Atp\Record\Worker::TYPE_INVESTIGATOR
                         || $row['TYPE'] === \Atp\Record\Worker::TYPE_EXAMINATOR
                        );
                    break;
                case \Atp\Record::STATUS_INVESTIGATING:
                    $return = $row['TYPE'] === \Atp\Record\Worker::TYPE_INVESTIGATOR;
                    break;
                case \Atp\Record::STATUS_IDLE:
                    $return = $row['TYPE'] === \Atp\Record\Worker::TYPE_INVESTIGATOR;
                    break;
                case \Atp\Record::STATUS_EXAMINING:
                    $return = $row['TYPE'] === \Atp\Record\Worker::TYPE_EXAMINATOR;
                    break;
                case \Atp\Record::STATUS_EXAMINED:
                    $return = $row['TYPE'] === \Atp\Record\Worker::TYPE_EXAMINATOR;
                    break;
                case \Atp\Record::STATUS_FINISHED:
                    $return = false;
                    break;
            }

            if ($return === true) {
                break;
            }
        }

        $this->cache->set($cacheName, serialize(func_get_args()), $return);

        return $return;
    }

    /**
     * Get worker types by document status.
     *
     * @deprecated Removed
     * @param int $documentStatus
     * @return null|string[]
     * @throws \Exception
     */
    public function getWorkerTypesByDocumentStatus($documentStatus)
    {
        throw new \Exception(__METHOD__ . ' was removed.' );
        
        $workerTypes = null;
//        if ((int) $documentStatus === \Atp\Entity\Document::STATUS_NONE) {
//            return $workerTypes;
//        } else
        if ((int) $documentStatus === \Atp\Entity\Document::STATUS_BOTH) {
            $workerTypes = [
                \Atp\Record\Worker::TYPE_INVESTIGATOR,
                \Atp\Record\Worker::TYPE_EXAMINATOR
            ];
        } elseif ((int) $documentStatus === \Atp\Entity\Document::STATUS_INVEST) {
            $workerTypes = [
                \Atp\Record\Worker::TYPE_INVESTIGATOR
            ];
        } elseif ((int) $documentStatus === \Atp\Entity\Document::STATUS_EXAM) {
            $workerTypes = [
                \Atp\Record\Worker::TYPE_EXAMINATOR
            ];
        }
        
        if (empty($workerTypes)) {
            $documentStatuses = [
//                \Atp\Entity\Document::STATUS_NONE,
                \Atp\Entity\Document::STATUS_BOTH,
                \Atp\Entity\Document::STATUS_INVEST,
                \Atp\Entity\Document::STATUS_EXAM,
            ];
            throw new \Exception(
            'Invalid worker type.'
            . ' Received "' . (is_object($documentStatus) ? get_class($documentStatus) : $documentStatus) . '".'
            . ' Expected one of following: ' . join(', ', $documentStatuses) . '.'
            );
        }

        return $workerTypes;
    }
}
