<?php

namespace Atp\Record\Form\Button;

class FinishProccess implements Button
{
    protected $name = 'button_finish_proccess';

    public function getName()
    {
        return $this->name;
    }
}
