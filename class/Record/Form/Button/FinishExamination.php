<?php

namespace Atp\Record\Form\Button;

class FinishExamination implements Button
{
    protected $name = 'button_finish_examination';

    public function getName()
    {
        return $this->name;
    }
}
