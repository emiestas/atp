<?php

namespace Atp\Record\Form\Button;

class FinishIdle implements Button
{
    protected $name = 'button_finish_idle';

    public function getName()
    {
        return $this->name;
    }
}
