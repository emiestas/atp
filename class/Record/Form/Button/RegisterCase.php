<?php

namespace Atp\Record\Form\Button;

class RegisterCase implements Button
{
    protected $name = 'button_register_case';

    public function getName()
    {
        return $this->name;
    }
}
