<?php

namespace Atp\Record\Form\Button;

class Save implements Button
{
    protected $name = 'button_save';

    public function getName()
    {
        return $this->name;
    }
}
