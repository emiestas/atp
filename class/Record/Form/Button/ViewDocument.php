<?php

namespace Atp\Record\Form\Button;

class ViewDocument implements Button
{
    protected $name = 'button_view_document';

    public function getName()
    {
        return $this->name;
    }
}
