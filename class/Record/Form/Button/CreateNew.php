<?php

namespace Atp\Record\Form\Button;

class CreateNew implements Button
{
    protected $name = 'create_new';

    public function getName()
    {
        return $this->name;
    }
}
