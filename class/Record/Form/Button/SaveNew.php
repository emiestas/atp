<?php

namespace Atp\Record\Form\Button;

class SaveNew implements Button
{
    protected $name = 'button_save_new';

    public function getName()
    {
        return $this->name;
    }
}
