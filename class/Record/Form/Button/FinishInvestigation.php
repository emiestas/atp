<?php

namespace Atp\Record\Form\Button;

class FinishInvestigation implements Button
{
    protected $name = 'button_finish_investigation';

    public function getName()
    {
        return $this->name;
    }
}
