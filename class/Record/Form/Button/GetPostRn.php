<?php

namespace Atp\Record\Form\Button;

class GetPostRn implements Button
{
    protected $name = 'button_get_RN';

    public function getName()
    {
        return $this->name;
    }
}
