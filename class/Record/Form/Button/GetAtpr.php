<?php

namespace Atp\Record\Form\Button;

class GetAtpr implements Button
{
    protected $name = 'button_get_atpr';

    public function getName()
    {
        return $this->name;
    }
}
