<?php

namespace Atp\Record\Form\Button;

class RegisterDocument implements Button
{
    protected $name = 'button_register_document';

    public function getName()
    {
        return $this->name;
    }
}
