<?php

namespace Atp\Record\Form\Button;

class EditAtpr implements Button
{
    protected $name = 'button_edit_atpr';

    public function getName()
    {
        return $this->name;
    }
}
