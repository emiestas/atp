<?php

namespace Atp\Record\Form\Button;

class CreateAtpr implements Button
{
    protected $name = 'button_create_atpr';

    public function getName()
    {
        return $this->name;
    }
}
