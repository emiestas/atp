<?php

namespace Atp\Record\Form\Button;

class RegisterElectronicDocument implements Button
{
    protected $name = 'button_register_e_document';

    public function getName()
    {
        return $this->name;
    }
}
