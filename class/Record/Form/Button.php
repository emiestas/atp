<?php

namespace Atp\Record\Form;

use \Atp\Record\Form\Button\Button as ButtonInterface;

class Button
{
    /**
     *
     * @var ButtonInterface[]
     */
    protected $buttons;

    public function __construct()
    {
        $this->buttons = [
            new Button\CreateAtpr(),
            new Button\CreateNew(),
            new Button\EditAtpr(),
            new Button\FinishExamination(),
            new Button\FinishIdle(),
            new Button\FinishInvestigation(),
            new Button\FinishProccess(),
            new Button\GetAtpr(),
            new Button\GetPostRn(),
            new Button\RegisterCase(),
            new Button\RegisterDocument(),
            new Button\RegisterElectronicDocument(),
            new Button\Save(),
            new Button\SaveNew(),
            new Button\ViewDocument(),
        ];
    }

    /**
     * @param string $buttonName
     * @return ButtonInterface
     */
    public function getByName($buttonName)
    {
        foreach ($this->buttons as $button) {
            if ($button->getName() === $buttonName) {
                return $button;
            }
        }
    }
}
