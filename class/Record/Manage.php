<?php

namespace Atp\Record;

// TODO: Į čia perkelti saugojimą
class Manage
{
    /**
     * @var \Atp\Core
     */
    private $atp;

    /**
     * @var \Atp\Document\Record
     */
    private $record;

    /**
     * Talpina saugojimo informaciją
     * @var array
     */
    private $save;

    /**
     * Talpina perkėlimo informaciją
     * @var array
     */
    private $transfer;

    /**
     * Saugojimo rezultato informacija
     * @var array
     */
    public $result;

    /**
     *
     * @param \Atp\Document\Record $record
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Document\Record $record, \Atp\Core &$atp)
    {
        $this->atp = &$atp;
        $this->record = &$record;

        $this->save = [];
        $this->transfer = [];
        $this->result = [];
    }
    //public function Save() {
    // TODO: saugojimas
    // Po išsaugoijimo
    //	$this->save_after();
    //}

    /**
     * <i>save_after()</i> iškvietimas, kai sugojimas vyksta ne per šią (<i>\Atp\Record\Manage</i>) klasę
     * @return null
     */
    public function SaveAfter_Walkaround()
    {
        $this->save_after();
        $this->set_result($result);
        return $this->result;
    }

    // TODO: Save()
    public function Save()
    {
        die('Save()');
        $result = $this->save_record($document);
        $this->set_result($result);

        return $this->result;
    }

    /**
     * Dokumento įrašo perkelimas į kitą dokumentą
     * @param int $document
     * @return array
     */
    public function TransferTo($document)
    {
        $result = $this->transfer_to_document($document);
        $this->set_result($result);

        return $this->result;
    }

    // TODO: Pritaikius pašalinti $redirect iš transferTo, transferToDocument ir create_record
    /**
     * Surenka saugojimo informaciją
     * @param bool $result
     */
    private function set_result($result)
    {
        $this->atp->get_messages_sess();
        $default = [
            'result' => (bool) $result,
            'messages' => $this->atp->_message,
            'redirect' => $this->atp->logic->create_record_redirect_url,
            'saved_record_id' => null
        ];
        $this->atp->unset_messages_sess();

        $this->result = array_merge($default, (array) $this->result);
    }

    // TODO: save_record()
    private function save_record()
    {
        die('save_record()');
    }

    /**
     * Perkelia dokumento įrašą į kitą dokumentą
     * @param int $document
     * @return boolean
     */
    private function transfer_to_document($document)
    {
        $atp = &$this->atp;
        $record = &$this->record->record;
        $to_document = $atp->logic2->get_document($document);

        if (empty($record['ID']) === true) {
            return false;
        }

        $this->transfer = [
            'record' => $record,
            'to_document' => $to_document];

        $atp->logic->_r_path = true;
        $atp->logic->_r_r_id = $record['ID'];
//        $atp->logic->_r_record_id = $record['RECORD_ID'];
        $atp->logic->_r_record_status = $record['STATUS'];

        $atp->logic->_r_table_id = $atp->logic->_r_table_from_id = $record['TABLE_TO_ID'];
        $atp->logic->_r_table_to_id = $to_document['ID'];

        $result = $this->transfer_validate_path();
        if ($result === false) {
            $atp->set_message('error', 'Iš šios dokumento versijos jau buvo sukurtas sekantis dokumentas');
            $this->result['redirect'] = 'index.php?m=5&id=' . $record['ID'];
            return false;
        }

        $result = $this->transfer_values();

        $atp->logic->_r_values = $result['values'];
        $atp->logic->_r_files = $result['files'];

        $new_record_id = $atp->logic->create_record('submit_path', null, $record['RECORD_ID']);
        if (ctype_digit((string) $new_record_id) === true) {
            $this->result['saved_record_id'] = (int) $new_record_id;

            return true;
        }

        return false;
    }

    /**
     * Tikrina ar leidžaiama perkelti į nurodytą dokumentą
     * @return boolean
     */
    private function transfer_validate_path()
    {
        return $this->atp->factory->Record()->validateTransferPath(
                $this->transfer['record']['ID'], $this->transfer['to_document']['ID']
        );
    }

    /**
     * Perkeliamo dokumento reikšmės naujame dokumente
     * @return array
     */
    private function transfer_values()
    {
        $atp = &$this->atp;
        $transfer = &$this->transfer;

        // Naujo dokumento perduoti duomenys
//        $values = $this->table_compatibility([
//            'RECORD_NR' => $transfer['record']['ID'],
//            'FROM_TABLE' => $transfer['record']['TABLE_TO_ID'],
//            'TO_TABLE' => $transfer['to_document']['ID']
//        ]);
        $values = $this->table_compatibility($transfer['record']['ID'], $transfer['to_document']['ID']);
        $values = array_change_key_case((array) $values, CASE_LOWER);


        // Naujo dokumento perduoti duomenys // Tikslinimas pagal tipus
        $relations = $atp->logic->get_table_relations($transfer['record']['TABLE_TO_ID'], $transfer['to_document']['ID']);

        foreach ($relations as $relation) {
            $field = $atp->logic2->get_fields(['ID' => $relation['COLUMN_ID']]);
            $value = $values[strtolower($field['NAME'])];

            // Veika // Veikos lauko ryšys perduoda visus žemesnio lygio laukus (laukus, kurie yra saugomi laukų lent.)
            if ($field['TYPE'] === '23') {
                $from_record_table = $atp->logic2->get_record_table($transfer['record']['ID'], 'C.ID, A.ID RID');
                $from_document = $atp->logic2->get_document($transfer['record']['TABLE_TO_ID']);

                // Gauna pažeidimo datą, pagal kurią atrenkamas teisės akto punktas
                $from_date = date('Y-m-d H:i:s');
                $violationDateFieldId = $this->atp->factory->Document()->getOptions($from_record_table['ID'])->violation_date;
                if (empty($violationDateFieldId) === false) {
                    $from_date = $atp->factory->Record()->getFieldValue($from_record_table['RID'], $violationDateFieldId);
                }

                // Žemesni veikos laukai
                $params = [
                    'ID' => $value,
                    'TYPE' => 'DEED',
                    'RECORD_DATE' => $from_date
                ];
                $deed_children_fields = $atp->logic->get_fields_by_type_and_id($params);

                $new_values = [];
                foreach ($deed_children_fields as $child_field) {
                    $new_values[strtolower($child_field['NAME'])] = $atp->factory->Record()->getFieldValue($from_record_table['RID'], $child_field['ID']);
                }
                $values = array_merge((array) $values, $new_values);
            } else {
                // Failas
                if ($field['SORT'] === '10' && $value === '1') {
                    //$parentField = $atp->logic2->get_fields(['ID' => $relation['COLUMN_ID']]);
                    $parentField = $atp->logic2->get_fields(['ID' => $relation['COLUMN_PARENT_ID']]);

                    $parent_files = $atp->logic2->get_record_files([
                        'FIELD_ID' => $parentField['ID'],
                        'RECORD_ID' => $transfer['record']['ID']
                        ], false, null, 'ID, FILE_ID');
                    foreach ($parent_files as $file) {
                        $files[$field['ID']][] = $file['FILE_ID'];
                    }
                    unset($parent_files);
                }
            }
        }

        return [
            'values' => $values,
            'files' => $files
        ];
    }

    /**
     * Get new document values filled from specified record according to relations.
     *
     * @param int $recordId Record for transfering.
     * @param int $toDocumentId Document where to transfer record.
     * @return string[]
     * @throws \Exception
     */
    public function table_compatibility($recordId, $toDocumentId)
    {
        if (empty($recordId)) {
            throw new \Exception('Record is not set');
        }

        $recordManager = new \Atp\Record($this->atp);
        $record = $recordManager->Get($recordId);

        $fromDocumentId = $record->document_id;

        if (empty($fromDocumentId) || empty($toDocumentId)) {
            throw new \Exception('Transfer path is not correct');
        }

        $relations = $this->atp->logic->get_table_relations($fromDocumentId, $toDocumentId);

        if (empty($relations)) {
            return;
        }

        $valuesManager = new \Atp\Record\Values($this->atp);
        $fieldsManager = new \Atp\Document\Field($this->atp);

        $parentValues = $valuesManager->getValuesByRecord($record->id);
        $parentFields = $fieldsManager->getFieldsByDocument($fromDocumentId);
        $fields = $fieldsManager->getFieldsByDocument($toDocumentId);

        foreach ($relations as $relation) {
            /* @var $field \Atp\Entity\Field */
            $field = $fields[$relation['COLUMN_ID']];
            
            if (empty($relation['DEFAULT']) === FALSE && empty($relation['COLUMN_PARENT_ID']) === TRUE) {
                $value = $relation['DEFAULT'];
                $info_table[$field->getName()] = $value;
                continue;
            }

            /* @var $parentField \Atp\Entity\Field */
            $parentField = $parentFields[$relation['COLUMN_PARENT_ID']];

            if (empty($field) || empty($parentField)) {
                continue;
            }

            $info_table[$field->getName()] = $value = $parentValues[$parentField->getId()];
            

            // Klasifikatorius // Jei klasifikatoriaus šaltiniai skirtingi, tai klasifikatorių bando perduoti identifikuojant pagal pasirinkimo pavadinimą
            if ($field->getSort() === 4 && $field->getType() === 18) {
                if (empty($value) === false) {
                    if ($field->getSource() && $field->getSource() !== $parentField->getSource()) {
                        $entityManager = $this->atp->factory->Doctrine();
                        /* @var $repository \Atp\Repository\ClassificatorRepository */
                        $repository = $entityManager->getRepository('Atp\Entity\Classificator');
                        /* @var $classificator \Atp\Entity\Classificator */
                        $classificator = $repository->find($value);

                        if ($classificator) {
//                            foreach ($repository->getChildrenById($field->getSource()) as $childClassificator) {
//                                if ($childClassificator->getLabel() === $classificator->getLabel()) {
//                                    $value = $childClassificator->getId();
//                                    break;
//                                }
//                            }
                            $classificator = $repository->findChildByLabel($field->getSource(), $classificator->getLabel());
                            if ($classificator) {
                                $value = $classificator->getId();
                                }

                            $info_table[$field->getName()] = $value;
                        }
                    }
                }
            }

            // Webserviso duomenų laukams
            if ($field && $parentField) {
                if ($field->getWs() && $parentField->getWs()) {
                    // Laukai, kuriuos reikia užpildyti // WS ir ATP laukų ryšiai
                    $wsFieldsXRef = $this->atp->logic2->get_webservices([
                        'PARENT_FIELD_ID' => $field->getId()
                    ]);
                    // Laukai, iš kurių galima užpildyti // WS ir ATP laukų ryšiai
                    $wsParentFieldsXRef = $this->atp->logic2->get_webservices([
                        'PARENT_FIELD_ID' => $parentField->getid()
                    ]);
                    foreach ($wsFieldsXRef as $wsFieldXRef) {
                        foreach ($wsParentFieldsXRef as $wsParentFieldXRef) {
                            // Turi sutapti registras
                            if ($wsFieldXRef['WS_NAME'] !== $wsParentFieldXRef['WS_NAME']) {
                                continue;
                            }
                            // Turi sutapti laukai
                            if ($wsFieldXRef['WS_FIELD_ID'] !== $wsParentFieldXRef['WS_FIELD_ID']) {
                                continue;
                            }

                            /* @var $wsField \Atp\Entity\Field */
                            $wsField = $fields[$wsFieldXRef['FIELD_ID']];
                            /* @var $wsParentField \Atp\Entity\Field */
                            $wsParentField = $parentFields[$wsParentFieldXRef['FIELD_ID']];

                            $info_table[$wsField->getName()] = $parentValues[$wsParentField->getId()];

                            break;
                        }
                    }
                }
            }
            
            $info_table[$field->getName()] = $value;
        }

        foreach ($info_table as $fieldName => $fieldValue) {
            if (strpos($fieldName, $this->atp->_prefix->item['CLASS'] . $this->atp->_prefix->column) === false) {
                continue;
            }
            $field = $fieldsManager->getFieldByName($toDocumentId, $fieldName);
            $parentField = $fieldsManager->getClassificatorFieldParent($toDocumentId, $field);

            if (!$parentField) {
                continue;
            }

            $parentFieldValue = $info_table[$parentField->getName()];
            if ($parentFieldValue != $field->getItemId()) {
                $info_table[$fieldName] = null;
            }
        }

        return $info_table;
    }

    private function save_after()
    {
        $atp = &$this->atp;
        $record = &$this->record->record;

        // Būsenai "Nutraukta byla, gražinta tirti"
        $statusManager = new \Atp\Document\Record\Status(['RECORD' => $record['ID']], $atp);
        $result = $statusManager->CheckSingleStatus(63);

        if (empty($result) === false) {
            $result = array_shift($result);
            $result = $statusManager->SetRecordStatus($result['RECORD'], $result['STATUS']);

            if ($result === true) {
                // Suranda paskutinį pažeidimo duomenų dokumento įrašą
                // TODO: naudoti \Atp\Document\Record\Path
                $record_path = $atp->logic->get_record_path($record['ID']);
                $filtered_records = array_filter($record_path, function ($array) {
                    if ((int) $array['TABLE_TO_ID'] !== \Atp\Document::DOCUMENT_INFRACTION) {
                        return false;
                    }
                    return true;
                });

                $count = count($filtered_records);
                if ($count === 0) {
                    throw new \Exception('Dokumentų kelyje nerastas nei vienas pažeidimo duomenų dokumentas');
                } elseif ($count === 1) {
                    $from_record = array_shift($filtered_records)['ID'];
                } elseif ($count > 1) {
                    $versions = array_map(function ($array) {
                        return (int) $array['RECORD_VERSION'];
                    }, $filtered_records);
                    $from_record = $filtered_records[array_search(max($versions), $versions, true)]['ID'];
                }

                $result = $this->save_new_version(new \Atp\Document\Record($from_record, $atp));

                if ($result === true) {
                    // Baigiamas bylos dokumentas
                    $atp->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD
						SET
							STATUS = ' . \Atp\Record::STATUS_FINISHED . '
						WHERE
							ID = ' . (int) $record['ID']);
                    // Nauja pažeidimo duomenų versija perkeliama į tyrimą
                    $atp->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD
						SET
							STATUS = ' . \Atp\Record::STATUS_INVESTIGATING . '
						WHERE
							ID = ' . (int) $this->result['saved_record_id']);

                    $record_table = $atp->logic2->get_record_table($this->result['saved_record_id']);




                    $worker = $atp->logic2->get_record_worker(['A.RECORD_ID = ' . (int) $this->result['saved_record_id'],
                        'A.TYPE = "invest"'], true);
                    $message = ['Nutraukta byla, gražinta tirti.'];
                    $message[] = 'Sukurtas dokumentas "<a href="' . $atp->config['SITE_URL'] . '/index.php?m=5&id=' . $this->result['saved_record_id'] . '">' . $record_table['RECORD_ID'] . '</a>".';
                    $message[] = 'Dokumentas perduotas į "' . $worker['STRUCTURE_NAME'] . '" skyrių<br>' .
                        'Atsakingas asmuo: ' . $worker['FIRST_NAME'] . ' ' . $worker['LAST_NAME'];
                    $atp->set_message('success', join('<br/>', $message));
                } else {
                    // TODO: ???
                }

                $this->set_result($result);
            }
        }
    }

    private function save_new_version(\Atp\Document\Record $record)
    {
        $atp = &$this->atp;
        $record = &$record->record;

        if (empty($record['ID']) === true) {
            return false;
        }

        $this->save = ['record' => $record];

        $atp->logic->_r_path = false;
        $atp->logic->_r_r_id = $record['ID'];
//        $atp->logic->_r_record_id = $record['RECORD_ID'];
        $atp->logic->_r_record_status = $record['STATUS'];

        $atp->logic->_r_table_id = $atp->logic->_r_table_from_id = $record['TABLE_TO_ID'];
        $atp->logic->_r_table_to_id = $record['TABLE_TO_ID'];

        $result = $this->save_values();

        $atp->logic->_r_values = $result['values'];
        $atp->logic->_r_files = $result['files'];

        $new_record_id = $atp->logic->create_record('save_new', null, $record['RECORD_ID']);
        if (ctype_digit((string) $new_record_id) === true) {
            $this->result['saved_record_id'] = (int) $new_record_id;
            return true;
        }

        return false;
    }

    private function save_values()
    {
        $atp = $this->atp;
        $save = $this->save;

        $rv = new \Atp\Record\Values($this->atp);
        $values = $rv->getValuesByRecord($save['record']['ID'], null, true);

        $values = array_change_key_case($values, CASE_LOWER);

        $files = [];
        $fields = $atp->logic2->get_fields_all([
            'ITEM_ID' => $save['record']['TABLE_TO_ID'],
            'ITYPE' => 'DOC']);
        foreach ($fields as &$field) {
            if ($field['SORT'] !== '10') {
                continue;
            }

            $files_data = $atp->logic2->get_record_files([
                'FIELD_ID' => $field['ID'],
                'RECORD_ID' => $save['record']['ID']
                ], false, null, 'ID, FILE_ID');
            foreach ($files_data as &$file) {
                $files[$field['ID']][] = $file['FILE_ID'];
            }
        }

        return [
            'values' => $values,
            'files' => $files
        ];
    }
}
