<?php

/**
 * @todo nenaudojamas
 */

namespace Atp\Record\Action;

class BaseAction extends AbstractAction
{

    /**
     * Dokumento įrašo ID
     * @var int
     */
    public $record_id;

    /**
     * Priskirtos lauko reikšmės
     * @var array [$field_id => $value]
     */
    public $values;

    /**
     * Failai priskirti prie laukų
     * @var array [$field_id => [
     * 		{'path' => $file_path, 'name' => $filename, 'types' => $mime_type, 'size' => $filesize},
     * 		{'path' => $file_path, 'name' => $filename, 'types' => $mime_type, 'size' => $filesize},
     * ]]
     */
    public $files;

    public function __construct()
    {
        $this->files = new FilesCollection();
    }

    /**
     * Nurodomas dokumento įrašas
     * @param int $record_id Dokumento įrašo ID
     * @return boolean
     */
    public function SetRecord($record_id)
    {
        $this->record_id = $record_id;
    }

    public function GetRecord()
    {
        return $this->record_id;
    }

    /**
     * Priskiria naują lauko reikšmę
     * @TODO Reikšmę paduoti parseriui?
     * @param int $field_id Lauko ID
     * @param string $value Lauko reikšmė
     */
    public function SetValue($field_id, $value)
    {
        $this->values[$field_id] = $value;
//		$val = new Value();
//		$val->field_id = $field_id;
//		$val->value = $value;
//		$this->values[$field_id] = $val;
    }

    public function GetValues()
    {
        return $this->values;
    }

    /**
     * Priskiria naują lauko failą
     * @param int $field_id Lauko ID
     * @param string $path Kelias iki failo
     * @param string $name Failo pavadinimas
     * @param string $mime_type [optional] Failo mime-type
     * @param int $size [optional] Failo dydis
     */
    public function SetFile($field_id, $path, $name, $mime_type = null, $size = null)
    {
        $file = new \stdClass();

        if (empty($mime_type)) {
            $finfo = new \finfo(FILEINFO_MIME_TYPE);
            $mime_type = $finfo->file($path);
        }
        if (empty($size)) {
            $size = filesize($path);
        }
        /*
          $file->path = $path;
          $file->name = $name;
          $file->type = $mime_type;
          $file->size = $size;

          $this->files[$field_id][] = $file; */
        $file = new File();
        $file->path = $path;
        $file->name = $name;
        $file->type = $mime_type;
        $file->size = $size;
        $this->files->$field_id = $file;
    }

    public function GetFiles()
    {
        return $this->files;
    }

    /**
     * Priskirai paramterą
     * @param string $name Parametro pavadinimas
     * @param mixed $value Parametro reikšmė
     */
    public function SetParameter($name, $value)
    {
        $this->$name = $value;
    }

    public function GetParameter($name)
    {
        return $this->$name;
    }
}

class FilesCollection extends \Iterator
{

}

class File
{

    protected $path;

    protected $name;

    protected $type;

    protected $size;

    public function __set($name, $value)
    {
        $method = 'Set' . ucfirst($name);
        if (method_exists($this, $method)) {
            $value = $this->$method($value);
        }
        $this->$name = $value;
    }

    public function __get($name)
    {
        $method = 'Get' . ucfirst($name);
        if (method_exists($this, $method)) {
            return $this->$method($name);
        }
        return $this->$name;
    }
}
