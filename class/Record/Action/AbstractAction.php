<?php

/**
 * @todo nenaudojamas
 */

namespace Atp\Record\Action;

abstract class AbstractAction
{
    /**
     * Išsaugo formos laukų reikšmes
     * @param int $record_id Dokumento įrašo ID
     * @param array $values
     * @param null|int|string $values[$field_id] Lauko reikšmė. Indeksas yra lauko ID
     * @param array $rfiles
     * @param array $rfiles[$field_id]
     * @return boolean
     */
    protected function saveFields($record_id, $values, $rfiles)
    {
        // Dokumento įrašas
        $record = $this->r->Get($record_id);
        // Dokumento laukų struktūra
        $structure = $this->f->getFieldsByDocument($record->document_id);

        // Saugomos nurodytos laukų reikšmės
        $exec = $this->atp->performance->start('Dokumento įrašo laukų saugojimas');
        if (empty($values) === false) {
            foreach ($values as $field_id => $value) {
                $field = $structure[$field_id];
                if (isset($field) === false) {
                    continue;
                }

                $this->rv->setValue($record->id, $field->id, $value);
            }
        }
        $this->rv->flushValues();
        $this->atp->performance->finish($exec);

        // Saugomi nurodyti laukų failai
        $exec = $this->atp->performance->start('Dokumento įrašo failų saugojimas');
        if (empty($rfiles) === false) {
            foreach ($rfiles as $field_id => $files) {
                $field = $structure[$field_id];
                if (isset($field) === false) {
                    continue;
                }

                foreach ($files as $file) {
                    //if (isset($file->id) && $file->id > 353967 && $record->id === 252340) {
                    //	$this->atp->db_query_fast('DELETE FROM TST_ATP_RECORD_FILES WHERE FILE_ID = ' . $file->id);
                    //}
                    // Ar failas yra įkeltas į ATP
                    if (empty($file->id)) {
                        $file->id = $this->file->Save($file->path, $file->name, $file->type);
                    }

                    // Failas susiejamas su įrašu ir lauku
                    $this->rfile->Save($file->id, $record->id, $field->id);
                }
            }
        }
        $this->atp->performance->finish($exec);

        return true;
    }

    public function create_empty($document_id, $worker_id)
    {
        $atp = &$this->atp;

        /* @var $documentManager \Atp\Document */
        $documentManager = $atp->factory->Document();
        $record_row = [
            'TABLE_TO_ID' => $document_id,
            'STATUS' => $documentManager->get($document_id)->getStatus(),
            'IS_LAST' => 1,
            'CREATE_DATE' => date('Y-m-d H:i:s')
        ];
        $new_record_id = $atp->db_quickinsert(PREFIX . 'ATP_RECORD', $record_row);


        $document_row = [
            'RECORD_LAST' => 1,
            'RECORD_VERSION' => 1,
            'CREATE_DATE' => $record_row['CREATE_DATE']
        ];
        $new_document_id = $atp->db_quickinsert(PREFIX . 'ATP_TBL_' . $document_id, $document_row);


        // Sukuriamas ryšys tarp įrašo ryšių ir dokumento informacijos įrašų
        $qry = 'INSERT INTO ' . PREFIX . 'ATP_RECORD_X_RELATION (DOCUMENT_ID, RECORD_ID, RELATION_ID)
			VALUES (:DOCUMENT, :ID_DOC, :RECORD)';
        $atp->db_query_fast($qry, [
            'RECORD' => $new_record_id,
            'ID_DOC' => $new_document_id,
            'DOCUMENT' => $document_id
        ]);


        // Suteikiamas dokumento įrašo numeris ir versijų bendras ID
        $new_nr = $document_id . 'p' . $new_document_id;
        $qry = 'UPDATE ' . PREFIX . 'ATP_RECORD
			SET
				MAIN_ID = :ID,
				RECORD_ID = :NR
			WHERE ID = :ID';
        $this->atp->db_query_fast($qry, [
            'ID' => $new_record_id,
            'NR' => $new_nr
        ]);
        // Suteikiamas dokumento įrašo numeris ir įrašų kelio (pirmo dokumento) numeris
        $qry = 'UPDATE ' . PREFIX . 'ATP_TBL_' . $document_id . '
			SET
				RECORD_ID = :NR, './* TODO: remove*/'
				RECORD_MAIN_ID = :NR
			WHERE ID = :ID';
        $this->atp->db_query_fast($qry, [
            'ID' => $new_document_id,
            'NR' => $new_nr
        ]);


        // Pažymimas, kaip neišsaugotas
        $atp->db_quickinsert(PREFIX . 'ATP_RECORD_UNSAVED', [
            'RECORD_ID' => $new_record_id,
            'RECORD_NR' => $new_nr,
            'WORKER_ID' => $worker_id
        ]);


        return $new_record_id;
    }

    /**
     * Padidina dokumento įrašo versiją
     * @TODO Pakeitus pagalvoti: "Ar nereikia valyti kešo?"
     * @param int $record_id Įrašo ID, kurio versija didinama
     * @return int Naujo įrašo ID
     */
    public function version_up($record_id)
    {
        $atp = $this->atp;

        // Dokumento įrašas
        $record = $this->r->Get($record_id);
        // Dokumento laukų struktūra
        $structure = $this->f->getFieldsByDocument($record->document_id);


        // Kopijuoja ryšių informaciją
        $qry = 'SELECT MAIN_ID, RECORD_ID, TABLE_FROM_ID, TABLE_TO_ID, STATUS, IS_PAID, DOC_STATUS
			FROM ' . PREFIX . 'ATP_RECORD
			WHERE ID = :ID';
        $result = $atp->db_query_fast($qry, ['ID' => $record->id]);
        $record_row = $atp->db_next($result);
        $record_row['RECORD_FROM_ID'] = $record->id;
        $record_row['IS_LAST'] = 1;
        $record_row['CREATE_DATE'] = date('Y-m-d H:i:s');
        $new_record_id = $atp->db_quickinsert(PREFIX . 'ATP_RECORD', $record_row);


        // Kopijuoja dokumento informaciją
        $qry = 'SELECT *
			FROM ' . PREFIX . 'ATP_TBL_' . $record->document_id . '
			WHERE ID = :ID';
        $result = $atp->db_query_fast($qry, ['ID' => $record->id_doc]);
        $document_row = $atp->db_next($result);
        unset($document_row['ID']);
        $document_row['RECORD_LAST'] = 1;
        $document_row['CREATE_DATE'] = $record_row['CREATE_DATE'];
        $document_row['RECORD_VERSION'] = $this->r->GetMaxVersionByNr($record->nr) + 1;
        $new_document_id = $atp->db_quickinsert(PREFIX . 'ATP_TBL_' . $record->document_id, $document_row);


        // Sukuriamas ryšys tarp įrašo ryšių ir dokumento informacijos įrašų
        $qry = 'INSERT INTO ' . PREFIX . 'ATP_RECORD_X_RELATION (DOCUMENT_ID, RECORD_ID, RELATION_ID)
			VALUES (:DOCUMENT, :ID_DOC, :RECORD)';
        $atp->db_query_fast($qry, [
            'RECORD' => $new_record_id,
            'ID_DOC' => $new_document_id,
            'DOCUMENT' => $record->document_id
        ]);


        // Kopijuoja priskirtus darbuotojus
        $workers = $atp->logic2->get_record_worker(['A.RECORD_ID = ' . $record->id]);
        foreach ($workers as $worker) {
            $this->rw->Set($new_record_id, $worker['WORKER_ID'], $worker['TYPE']);
        }


        // Kopijuoja laukų reikšmes
        $values = $this->rv->getValuesByRecord($record->id);
        $exec = $atp->performance->start('Dokumento įrašo laukų kopijavimas');
        foreach ($values as $field_id => $value) {
            $field = $structure[$field_id];
            if (isset($field) === false) {
                continue;
            }

            $this->rv->setValue($new_record_id, $field->id, $value);
        }
        $this->rv->flushValues();
        $atp->performance->finish($exec);


        // Kopijuoja prie laukų pririštus failus
        $rfiles = $this->rfile->getValuesByRecord($record->id);
        // Saugomi prie laukų priskirti failai
        $exec = $atp->performance->start('Dokumento įrašo laukų failų ryšių kopijavimas');
        foreach ($rfiles as $rfile) {
            $field = $structure[$rfile->field_id];
            if (isset($field) === false) {
                continue;
            }

            // Failas susiejamas su įrašu ir lauku
            $this->rfile->Save($rfile->file_id, $new_record_id, $field->id);
        }
        $atp->performance->finish($exec);


        // Atnaujinama senesnių versijų paskutinės versijos žymą
        $qry = 'UPDATE ' . PREFIX . 'ATP_RECORD
			SET IS_LAST = 0
			WHERE
				RECORD_ID = :NR AND
				ID != :NEW';
        $atp->db_query_fast($qry, ['NR' => $record->nr, 'NEW' => $new_record_id]);

        $qry = 'UPDATE ' . PREFIX . 'ATP_TBL_' . $record->document_id . '
			SET RECORD_LAST = 0
			WHERE
				RECORD_ID = :NR AND './* TODO: remove */'
				ID != :NEW';
        $atp->db_query_fast($qry, ['NR' => $record->nr, 'NEW' => $new_document_id]);


        return $new_record_id;
    }
}
