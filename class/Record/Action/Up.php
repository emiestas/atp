<?php

/**
 * @todo nenaudojamas
 */

namespace Atp\Record\Action;

class Up extends BaseAction implements IAction
{
    protected $atp;

    protected $data;

    /**
     * @var \Atp\Record
     */
    public $r;

    /**
     * @var \Atp\Record\Values
     */
    public $rv;

    /**
     * @var \Atp\Record\Worker
     */
    public $rw;

    /**
     * @var \Atp\File
     */
    public $file;

    /**
     * @var \Atp\File\Record
     */
    public $rfile;

    /**
     * @var \Atp\Document
     */
    public $d;

    /**
     * @var \Atp\Document\Field
     */
    public $f;

    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;

        $this->d = $this->atp->factory->Document();
        $this->r = $this->atp->factory->Record();
        $this->f = $this->atp->factory->Field();
        $this->rw = new \Atp\Record\Worker($this->atp);
        $this->rv = new \Atp\Record\Values($this->atp);
        $this->file = new \Atp\File($this->atp);
        $this->rfile = new \Atp\File\Record($this->atp);
    }

    public function init()
    {
        $exec = $this->atp->performance->start('Naujos versijos saugojimas');
        $new_id = $this->version_up($this->GetRecord());

        //$this->SetRecord($new_id);
        $this->saveFields($new_id, $this->GetValues(), $this->GetFiles());
        $this->atp->performance->finish($exec);

        return true;
    }
}
