<?php

/**
 * @todo nenaudojamas
 */

namespace Atp\Record\Action;

class Save extends BaseAction implements IAction
{
    protected $atp;

    /**
     * @var \Atp\Record
     */
    public $r;

    /**
     * @var \Atp\Record\Values
     */
    public $rv;

    /**
     * @var \Atp\Record\Worker
     */
    public $rw;

    /**
     * @var \Atp\File
     */
    public $file;

    /**
     * @var \Atp\File\Record
     */
    public $rfile;

    /**
     * @var \Atp\Document
     */
    public $d;

    /**
     * @var \Atp\Document\Field
     */
    public $f;

    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;

        parent::__construct($atp);
        $this->d = $this->atp->factory->Document();
        $this->r = $this->atp->factory->Record();
        $this->f = $this->atp->factory->Field();
        $this->rw = new \Atp\Record\Worker($this->atp);
        $this->rv = new \Atp\Record\Values($this->atp);
        $this->file = new \Atp\File($this->atp);
        $this->rfile = new \Atp\File\Record($this->atp);
    }

    public function init()
    {
        $exec = $this->atp->performance->start('Saugojimas');
        var_dump($this->GetFiles());
        die;
        $this->saveFields($this->GetRecord(), $this->GetValues(), $this->GetFiles());

        // Jei buvo saugomas laikinas įrašas, tai atnaujinama sukūrimo data
        // ir pašalinama laikino įrašo žyma
        $record = $this->r->Get($this->GetRecord());
        $param = [
            'RECORD_ID' => $record->id,
            'ID_DOC' => $record->id_doc,
            'DATE' => date('Y-m-d H:i:s')
        ];
        $qry = 'SELECT ID
			FROM ' . PREFIX . 'ATP_RECORD_UNSAVED
			WHERE
				RECORD_ID = :RECORD_ID';
        $result = $this->atp->db_query_fast($qry, $param);
        $count = $this->atp->db_num_rows($result);
        if ($count) {
            $qry = 'UPDATE ' . PREFIX . 'ATP_RECORD
				SET CREATE_DATE = :DATE
				WHERE ID = :RECORD_ID';
            $this->atp->db_query_fast($qry, $param);

            $qry = 'UPDATE ' . PREFIX . 'ATP_TBL_' . $record->document_id . '
				SET CREATE_DATE = :DATE
				WHERE ID = :ID_DOC';
            $this->atp->db_query_fast($qry, $param);

            $qry = 'DELETE
				FROM ' . PREFIX . 'ATP_RECORD_UNSAVED
				WHERE
					RECORD_ID = :RECORD_ID';
            $this->core->db_query_fast($qry, $param);
        }
        $this->atp->performance->finish($exec);

        return true;
    }
}
