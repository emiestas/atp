<?php

/**
 * @todo nenaudojamas
 */

namespace Atp\Record\Action;

interface IAction
{
    public function SetRecord($record_id);

    public function GetRecord();

    public function SetValue($field_id, $value);

    public function GetValues();

    public function SetFile($field_id, $path, $name, $mime_type = null, $size = null);

    public function GetFiles();

    public function SetParameter($name, $value);

    public function GetParameter($name);
}
