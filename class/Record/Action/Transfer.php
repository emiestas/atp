<?php

/**
 * @todo nenaudojamas
 */

namespace Atp\Record\Action;

class Transfer extends BaseAction implements IAction
{
    protected $atp;

    protected $data;

    /**
     * @var \Atp\Record
     */
    public $r;

    /**
     * @var \Atp\Record\Values
     */
    public $rv;

    /**
     * @var \Atp\Record\Worker
     */
    public $rw;

    /**
     * @var \Atp\File
     */
    public $file;

    /**
     * @var \Atp\File\Record
     */
    public $rfile;

    /**
     * @var \Atp\Document
     */
    public $d;

    /**
     * @var \Atp\Document\Field
     */
    public $f;

    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;

        $this->d = $this->atp->factory->Document();
        $this->r = $this->atp->factory->Record();
        $this->f = $this->atp->factory->Field();
        $this->rw = new \Atp\Record\Worker($this->atp);
        $this->rv = new \Atp\Record\Values($this->atp);
        $this->file = new \Atp\File($this->atp);
        $this->rfile = new \Atp\File\Record($this->atp);

        $this->param = new \stdClass;
        $this->param->transferTo = null;
    }

    public function init()
    {
        $exec = $this->atp->performance->start('Dokumento siuntimas');
        $this->saveFields($this->GetRecord(), $this->GetValues(), $this->GetFiles());

        $record = $this->r->Get($this->GetRecord());
        $result = $this->is_valid($record->id, $this->GetParameter('transferTo'));
        $result = true;
        if ($result) {
            $new_id = $this->transfer($record->id, $this->GetParameter('transferTo'));
        } else {
            // TODO: 'Iš šios dokumento versijos jau buvo sukurtas sekantis dokumentas'
        }
        $this->atp->performance->finish($exec);

        return true;
    }

    /**
     * Tikrina ar dokumento įrašo perdavimas į kitą dokumentą yra galimas
     * @param int $record_id Įrašo ID
     * @param int $to_document_id Dokumento ID, į kurį perkeliama
     * @return boolean
     */
    private function is_valid($record_id, $to_document_id)
    {
        return $this->atp->factory->Record()->validateTransferPath(
                $record_id, $to_document_id
        );
    }

    private function transfer($record_id, $to_document_id)
    {
        $atp = $this->atp;

        // Perkeliamas dokumento įrašas
        $record = $this->r->Get($record_id);
        // Įrašo laukų reikšmės
        $values = $this->rv->getValuesByRecord($record->id);
        // Įrašo laukų failai
        $rfiles = $this->rfile->getValuesByRecord($record->id);

        // Dokumentas iš kurio keliami duomenys
        $from_document = $this->d->Get($record->document_id);
        // Dokumentas į kurį keliami duomenys
        $to_document = $this->d->Get($to_document_id);


        $new_values = [];
        $new_rfiles = [];

        // Naujo dokumento
        $qry = 'SELECT COLUMN_ID FIELD_ID, COLUMN_PARENT_ID PARENT_FIELD_ID, IF(`DEFAULT` = "", NULL, `DEFAULT`) DEFAULT_VALUE
			FROM ' . PREFIX . 'ATP_TABLE_RELATIONS
			WHERE
				DOCUMENT_ID = :TO AND
				DOCUMENT_PARENT_ID = :FROM';
        $result = $this->atp->db_query_fast($qry, ['TO' => $to_document->getId(), 'FROM' => $from_document->getId()]);
        $count = $this->atp->db_num_rows($result);
        for ($i = 0; $i < $count; $i++) {
            $row = (object) array_change_key_case($this->atp->db_next($result), CASE_LOWER);

            $field = $this->f->getField($row->field_id);
            $parent_field = $this->f->getField($row->parent_field_id);

            if (empty($values[$parent_field->id])) {
                $new_values[$field->id] = $row->default_value;
            } else {
                $new_values[$field->id] = $values[$parent_field->id];
            }

            // Failas
            if ($field->sort === 10) {
                if (count($rfiles)) {
                    foreach ($rfiles as $r) {
                        if ($r->field_id === $parent_field->id) {
                            $new_rfiles[$field->id][] = (object) ['id' => $r->file_id];
                        }
                    }
                }
            }
        }

        $new_record_id = $this->create_empty($to_document->getId(), $atp->factory->User()->getLoggedPersonDuty()->id);

        $new_record = $this->r->Get($new_record_id);

        $qry = 'UPDATE ' . PREFIX . 'ATP_RECORD
			SET
				RECORD_FROM_ID = :FROM_RECORD,
				TABLE_FROM_ID = :FROM_DOCUMENT
			WHERE ID = :ID';
        $this->atp->db_query_fast($qry, [
            'ID' => $new_record->id,
            'FROM_RECORD' => $record->id,
            'FROM_DOCUMENT' => $record->document_id
        ]);
        $qry = 'UPDATE ' . PREFIX . 'ATP_TBL_' . $new_record->document_id . '
			SET
				RECORD_MAIN_ID = :MAIN_NR
			WHERE ID = :ID';
        $this->atp->db_query_fast($qry, [
            'ID' => $new_record->id_doc,
            'MAIN_NR' => $record->main_nr,
        ]);

        $qry = 'DELETE
			FROM ' . PREFIX . 'ATP_RECORD_UNSAVED
			WHERE
				RECORD_ID = :RECORD_ID';
        $this->atp->db_query_fast($qry, ['RECORD_ID' => $new_record->id]);

        // TODO: išvalyti $new_record kešą
        $atp->factory->Cache()->delete('ATP\Record::Get', $new_record->id);

        $this->saveFields($new_record->id, $new_values, $new_rfiles);

        return $new_record_id;
    }
}
