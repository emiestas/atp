<?php

/**
 * @todo nenaudojamas
 */

namespace Atp\Record;

/**
 * @TODO Iškelti requesto parsinimą į formos action'o apdorojimą
 * @TODO Nereikia dalies iškelti į Action? NE. NE? NE? NE. NE? NE! trolololo
 */
class Manager
{
    /**
     * @var \Atp\Core
     */
    private $atp;

    /**
     * @var \stdClass
     */
    private $data;

    /**
     * @var Action\IAction
     */
    private $action_handler;

    /**
     * @var \Atp\Record
     */
    public $r;

    /**
     * @var \Atp\Record\Values
     */
    public $rv;

    /**
     * @var \Atp\File\Record
     */
    public $rf;

    /**
     * @var \Atp\Document
     */
    public $d;

    /**
     * @var \Atp\Document\Field
     */
    public $f;

    /**
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;

        $this->d = $this->atp->factory->Document();
        $this->r = $this->atp->factory->Record();
        $this->f = $this->atp->factory->Field();
        $this->rv = new \Atp\Record\Values($this->atp);
        $this->rf = new \Atp\File\Record($this->atp);

        $this->data = new \stdClass();
    }

    /**
     * HTTP REQUEST'e migruoja formos web-service laukų duomenis į paprastus laukus
     */
    private function ws_fields_to_form_fields($record_id)
    {
        $data = $this->request['ws_field'];

//        // Web-serviso laukų pasrsinimas
//        // Jei magic_quotes_gpc įšjungtas lokaliai, bet įjungtas globaliai, tai visteik naudoja globalų nustatymą
//        $php_ini = ini_get_all();
//        if (isset($php_ini['magic_quotes_gpc']) === true && in_array(strtolower($php_ini['magic_quotes_gpc']['global_value']), ['on',
//                '1']) === true) {
//            $data = stripslashes($data);
//        } else {
//            $data = $data;
//        }
//        $data = json_decode($data, true);

        $record = $this->r->Get($record_id);

        $fieldsIdNameMap = $this->f->getFieldsMapByDocument($record->document_id);

        /*
          Formatas:
          [ 0: { 1948: { 1955: '348646348', 1956: 'VALDAS', 1957: 'PUČKIS' } },
          1: { 1949: { 1958: '13246687', 1959: 'UAB "Plytos"' } } ]
         */
        foreach ($data as $field) {
//            $field = current($field);
            foreach ($field as $fieldId => $value) {
                $fieldName = $fieldsIdNameMap[$fieldId];
                if (empty($fieldName)) {
                    continue;
                }

                $this->request[$fieldName] = $value;
            }
        }
        unset($this->request['ws_field']);
    }

    /**
     * Tikrina ir performuoja formos laukų reikšmes, juos pateikia saugojimui
     */
    private function parse_form_fields($record_id)
    {
        // Formos laukų reikšmių parsinimas ir setinimas
        $parse = new Manager\ValueParser();
        $record = $this->r->Get($record_id);

        $structure = $this->f->getFieldsByDocument($record->document_id);


        foreach ($structure as $field) {
            $name_lower = strtolower($field->name);

            // Failas // TODO: gal į parserį? Gal ne!
            if ($field->sort === 10 && isset($this->files[$name_lower])) {
                $files = $this->files[$name_lower];

                $count = count($files['tmp_name']);
                if ($count) {
                    for ($i = 0; $i < $count; $i++) {
                        // Failas neprikabintas
                        if ($files['error'][$i] === 4) {
                            continue;
                        }

                        if ($files['error'][$i] === 0) {
                            $this->SetFile($field->id, $files['tmp_name'][$i], $files['name'][$i], $files['type'][$i], $files['size'][$i]);
                        } else {
                            // TODO: klaidos pranešimas
                        }
                    }

                    $this->request[$name_lower] = '1';
                }
            }


            if (isset($this->request[$name_lower]) === false) {
                continue;
            }

            $value = $this->request[$name_lower];

            // Skaičius, laiko žyma, loginis pasirinkimas, klasifikatorius, failas
            if (in_array($field->type, [1, 2, 3, 4, 5, 15, 16, 18]) === true || $field->sort === 10) {
                $value = $parse->int($value);
            } else
            // Skaičius su kableliu
            if (in_array($field->type, [6]) === true) {
                $value = $parse->float($value);
            } else
            // Tekstas
            if (in_array($field->type, [7, 8, 9, 10]) === true) {
                $value = $parse->text($value);
            } else
            // Metai, data, laikas, data ir laikas
            if (in_array($field->type, [11, 12, 13, 14]) === true) {
                // Metai
                if ($field->type == 11) {
                    $value = $parse->year($value);
                } else
                // Data
                if ($field->type == 12) {
                    $value = $parse->date($value);
                } else
                // Laikas
                if ($field->type == 13) {
                    $value = $parse->time($value);
                } else
                // Data ir laikas
                if ($field->type == 14) {
                    $value = $parse->datetime($value);
                }
            }

            // TODO: regis tokio nebėra, straipsnis yra rišamas į veiką
            // Nurodomas straipsnis
            if ($field->sort == 6 && empty($value) === false) {
                $clause_id = $value;
            }

            // Varnelė arba loginis pasirinkimas (pastarojo ties sort nebėra, yra tik prie type)
            if ($field->sort == 5 || $field->sort == 8 && empty($value) === false) {
                if ($field->sort == 8) {
                    $value = [$value];
                }
                $value = json_encode($value);
            }

//			/**
//			 * @TODO: [done] Neaišku kam to reikia. Turi būti saugomi visi įjungti, matomi formos laukai.
//			 * Buvo reikalinga, kad nesaugotu tuščių papildomų laukų reikšmių.
//			 */
//			//if(empty($value) === TRUE)
//			if ($value === NULL || $value === '')
//				continue;
//			/**
//			 * @TODO: [done] Taip pat netinka. Saugant tą pačią versiją turi perrašyti esamus išsaugotų laukų reikšmes
//			 */
//			// Tuščias laukas. Jei dokumento laukas, tai nurodoma NULL reikšmė, jei papildomas laukas - išvis praleidžiamas.
//			if ($value === NULL || $value === '')
//				if ($structure['ITYPE'] !== 'DOC')
//					continue;
//				$value = NULL;
            //Jei reikšmė tuščia, bet ne 0, '0'
            if ($value === null || $value === '') {
                $value = null;
            }

            $this->SetValue($field->id, $value);
        }
    }

    /**
     * Parsina formos saugojimo requesto duomenis
     * @TODO Gražinti formos veiksmo pavadinimą
     */
    public function ParseFormData()
    {
        $this->files = &$_FILES;
        $this->request = &$_REQUEST;

        if (isset($this->request['record_id'])) {
            $this->request['id'] = $this->request['record_id'];
            unset($this->request['record_id']);
        }

        // Setina informaciją į $this->record
        $this->SetRecord($this->request['id']);
        // Migruoja formos web-serviso laukų reikšmes ($_REQUEST['ws_field']) į paprastus laukus
        $this->ws_fields_to_form_fields($this->request['id']);
        // Apdoroja formos laukų informaciją ir setina naujas reikšmes
        $this->parse_form_fields($this->request['id']);
    }

    /**
     * Nurodomas dokumento įrašas
     * @param int $record_id Dokumento įrašo ID
     * @return boolean
     */
    public function SetRecord($id)
    {
        $this->data->record_id = $id;
    }

    /**
     * Priskiria naują lauko reikšmę
     * @TODO Reikšmę paduoti parseriui?
     * @param int $field_id Lauko ID
     * @param string $value Lauko reikšmė
     */
    public function SetValue($field_id, $value)
    {
        $this->data->values[$field_id] = $value;
    }

    /**
     * Priskiria naują lauko failą
     * @param int $field_id Lauko ID
     * @param string $path Kelias iki failo
     * @param string $name Failo pavadinimas
     * @param string $mime_type [optional] Failo mime-type
     * @param int $size [optional] Failo dydis
     */
    public function SetFile($id, $path, $name, $mime_type = null, $size = null)
    {
        $file = new \stdClass();

        if (empty($mime_type)) {
            $finfo = new \finfo(FILEINFO_MIME_TYPE);
            $mime_type = $finfo->file($path);
        }
        if (empty($size)) {
            $size = filesize($path);
        }

        $file->path = $path;
        $file->name = $name;
        $file->type = $mime_type;
        $file->size = $size;

        $this->data->files[$id][] = $file;
    }

    /**
     * Nurodo veiksmo valdymo įrankį
     * @param string $action_name Valdymo įrankio pavadinimas
     * @return boolean
     */
    public function SetAction($action_name)
    {
        $class = 'ATP\Record\Action\\' . ucfirst($action_name);
        if (class_exists($class) === false) {
            return false;
        }

        $handler = new $class($this->atp);
        if ($handler instanceof Action\IAction === false) {
            return false;
        }

        $this->action_handler = $handler;

        return true;
    }

    /**
     * Priskirai paramter
     * @param string $name Parametro pavadinimas
     * @param mixed $value Parametro reikšmė
     */
    public function SetParameter($name, $value)
    {
        $this->data->parameters[$name] = $value;
    }

    public function init()
    {
        $handler = $this->action_handler;
        if (isset($handler) === false) {
            throw new \Exception('Nežinomas veiksmas');
        }


        $handler->SetRecord($this->data->record_id);

        if (empty($this->data->values) === false) {
            foreach ($this->data->values as $field_id => $value) {
                $handler->SetValue($field_id, $value);
            }
        }
        if (empty($this->data->files) === false) {
            foreach ($this->data->files as $field_id => $arr) {
                foreach ($arr as $file) {
                    $handler->SetFile($field_id, $file->path, $file->name, $file->type, $file->size);
                }
            }
        }
        if (empty($this->data->parameters) === false) {
            foreach ($this->data->parameters as $name => $value) {
                $handler->SetParameter($name, $value);
            }
        }


        $result = $handler->init();
    }
}
