<?php

namespace Atp\Record;

class Values
{
    private $atp;

    /**
     * @var \Atp\Cache
     */
    private $cache;
    
    /**
     * @var \Atp\Document\Field
     */
    private $field;

    /**
     * @var \Atp\Record
     */
    private $record;

    private $items_to_save;

    /**
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;

        $this->cache = $this->atp->factory->Cache();
        $this->field = $this->atp->factory->Field();
        $this->record = $this->atp->factory->Record();
    }

    /**
     * @param int $recordId Global record ID
     * @param int $fieldId Filed ID
     * @return null|string
     */
    public function getValue($recordId, $fieldId)
    {
        $cacheName = __METHOD__;
        $cacheKey = serialize(func_get_args());
        if ($this->cache->is($cacheName, $cacheKey)) {
            return $this->cache->get($cacheName, $cacheKey);
        }

        $return = null;

        $record = $this->record->Get($recordId);
        $fieldInfo = $this->field->getField($fieldId);

        if ($fieldInfo->itemType === 'DOC') {
            $result = $this->atp->db_query_fast('
                SELECT
                    B.`' . $fieldInfo->name . '` VALUE
                FROM
                    ' . PREFIX . 'ATP_RECORD_X_RELATION A
                        INNER JOIN ' . PREFIX . 'ATP_TBL_' . $fieldInfo->itemId . ' B ON
                            B.ID = A.RECORD_ID
                WHERE
                    A.RELATION_ID = ' . $record->id);
        } else {
            $result = $this->atp->db_query_fast('
                SELECT
                    VALUE
                FROM
                ' . PREFIX . 'ATP_FIELDS_VALUES
                WHERE
                    RECORD_ID = ' . (int) $record->id . ' AND
                    FIELD_ID = ' . (int) $fieldInfo->id);
        }

        if ($this->atp->db_num_rows($result)) {
            $row = $this->atp->db_next($result);

            $return = $row['VALUE'];
        }


        $this->cache->set($cacheName, $cacheKey, $return);

        return $return;
    }

    /**
     *
     * @param int $recordId
     * @param boolean $parseValues [optional] Default: false.
     * @param boolean $withName [optional] Default: false.
     * @param boolean $reCache Default: false.
     * @return array
     */
    public function getValuesByRecord($recordId, $parseValues = false, $withName = false, $reCache = false)
    {
        $record = $this->record->Get($recordId);

        $cache_name = json_encode([
            'method' => __METHOD__,
            'parseValues' => (int) $parseValues,
            'withName' => (int) $withName
        ]);
        if ($reCache === false) {
            $data = $this->cache->get($cache_name, $record->id);
            if ($data !== false) {
                return $data;
            }
        }

        $prameters = [
            'RECORD' => $record->id
        ];
        if ($parseValues === true) {
            $prameters['PARSE_VALUES'] = true;
        }
        if ($withName === true) {
            $prameters['WITH_NAME'] = true;
        }

        $data = $this->get_record_values($prameters);

        $this->cache->set($cache_name, $record->id, $data);

        return $data;
    }

    /**
     * Set record field value.
     * 
     * To save values call $this->flushValues() to flush all values at once or set $autoFlush to true to flush on fly.
     * 
     * @param int $recordId
     * @param int $fieldId
     * @param string $value
     * @param boolean $autoFlush [optional] Set to true if you want to flush on fly. Default: false.
     */
    public function setValue($recordId, $fieldId, $value, $autoFlush = false)
    {
        $record = $this->record->Get($recordId);
        $field = $this->field->getField($fieldId);

        $this->items_to_save[] = (object) [
                'record' => $record->id,
                'field' => $field->id,
                'value' => $value
        ];

        if ($autoFlush) {
            $this->flushValues();
        }
    }

    /**
     * Flush values.
     */
    public function flushValues()
    {
        if (count($this->items_to_save)) {
            $valuesData = [];
            foreach ($this->items_to_save as $item) {
                $field = $this->field->getField($item->field);
                $record = $this->record->Get($item->record);

                $param = [null];
                $param[] = ['name' => 'VALUE', 'value' => $item->value];
                if (strpos($field->getName(), $this->atp->_prefix->column) === 0) {
                    $set = $this->atp->db_bind('`' . $field->getName() . '` = :VALUE', $param);

                    $valuesData['DOC'][$record->document_id][$record->id_doc][$field->getName()] = $set;
                } else {
                    $param[] = ['name' => 'FIELD', 'value' => $field->getId()];
                    $param[] = ['name' => 'RECORD_ID', 'value' => $record->id];
                    $valuesData['EXTRA'][] = $this->atp->db_bind('(:FIELD, :RECORD_ID, :VALUE)', $param);
                }
            }

            $this->items_to_save = null;

            if (empty($valuesData) === false) {
                $qrys = [];
                if (isset($valuesData['EXTRA'])) {
                    if (count($valuesData['EXTRA'])) {
                        $qrys[] = 'INSERT INTO ' . PREFIX . 'ATP_FIELDS_VALUES
                                (`FIELD_ID`, `RECORD_ID`, `VALUE`)
							VALUES
                                ' . join(',', $valuesData['EXTRA']) . '
                            ON DUPLICATE KEY UPDATE
                                `VALUE` = VALUES(`VALUE`)';
                    }
                }

                if (isset($valuesData['DOC'])) {
                    if (count($valuesData['DOC'])) {
                        foreach ($valuesData['DOC'] as $documentId => $records) {
                            if (count($records)) {
                                foreach ($records as $recordId => $fields) {
                                    if (count($fields)) {
                                        $qrys[] = 'UPDATE ' . PREFIX . 'ATP_TBL_' . $documentId . '
											SET ' . join(',', $fields) . '
											WHERE `ID` = ' . $recordId;
                                    }
                                }
                            }
                        }
                    }
                }

                $count = count($qrys);
                if ($count) {
                    $this->atp->db_query_fast('START TRANSACTION');
                    for ($i = 0; $i < $count; $i++) {
                        $this->atp->db_query_fast($qrys[$i]);
                    }
                    $this->atp->db_query_fast('COMMIT');
                }
            }
        }
    }

    /**
     * Dokumento įrašo laukų reikšmės
     * @param mixed $param Parametrai | Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param mixed $param['RECORD'] Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param boolen $param['TABLE'] [optional]<p>Surinkti įrašo laukų duomenis iš dokumento laukų. <b>Default:</b> TRUE</p>
     * @param boolen $param['EXTRA'] [optional]<p>Surinkti įrašo laukų duomenis iš papildomų laukų. <b>Default:</b> TRUE</p>
     * @param boolen $param['WITH_NAME'] [optional]<p><i>TRUE</i> - lauko indeksas yra lauko pavadinimas; <i>FALSE</i> - lauko ID. <b>Default:</b> FALSE</p>
     * @param boolen $param['PARSE_VALUES'] [optional]<p> TODO: pakeisti laukų reikšmes į formoje atvaizduojamą formatą. <b>Default:</b> FALSE</p>
     * @return array
     */
    private function get_record_values($param)
    {
        if (is_array($param) === false) {
            $param = ['RECORD' => $param];
        }

        $default = [
            'TABLE' => true,
            'EXTRA' => true,
            'WITH_NAME' => false,
            'PARSE_VALUES' => false
        ];
        $param = array_merge($default, $param);

        $record = $this->atp->logic2->get_record_relation($param['RECORD']);
        if (empty($record) === true) {
            trigger_error('Record not found', E_USER_ERROR);
            exit;
        }

        /**
         * @return [$index, $value]
         */
        $parse = function(\Atp\Entity\Field $field, $value, $parameters) {
            if (empty($value) === false && $parameters['PARSE_VALUES']) {
                if ($field->getType() === 18) {
                    $classificator = $this->atp->logic2->get_classificator(['ID' => $value], true);
                    $value = $classificator['LABEL'];
                }
            }

            // Indekso pakeitimas
            if ($parameters['WITH_NAME']) {
                $index = $field->getName();
            } else {
                $index = $field->getId();
            }

            return [
                $index,
                $value
            ];
        };

        $return = [];

        // Laukų duomenys iš ATP_TBL_{dokumento_id}
        if ($param['TABLE'] === true) {
            $record_table = $this->atp->logic2->get_record_table($record);
            foreach ($record_table as $column => $value) {
                if (strpos($column, $this->atp->_prefix->column) === false) {
                    continue;
                }

                $field = $this->field->getFieldByName($record['TABLE_TO_ID'], $column);
                if (empty($field)) {
                    continue;
                }

                list($index, $value) = $parse($field, $value, $param);

                $return[$index] = $value;

                unset($record_table[$column]);
            }
        }

        // Laukų duomenys iš papildomų laukų
        if ($param['EXTRA'] === true) {
            $qry = 'SELECT FIELD_ID, VALUE
				FROM ' . PREFIX . 'ATP_FIELDS_VALUES
				WHERE
                    RECORD_ID = :RECORD_ID
                    ';
            $result = $this->atp->db_query_fast(
                $qry, [
                'RECORD_ID' => $record['ID']
                ]
            );
            $count = $this->atp->db_num_rows($result);
            for ($i = 0; $i < $count; $i++) {
                $row = $this->atp->db_next($result);
                $value = $row['VALUE'];

                $field = $this->field->getField($row['FIELD_ID']);
                if (empty($field)) {
                    continue;
                }
                
                list($index, $value) = $parse($field, $value, $param);
                
                // Jei laukas kartojasi, tai neperrašo ankstesnės reikšmės
                if (isset($return[$index]) === true) {
                    continue;
                }

                $return[$index] = $value;
            }
        }

        return $return;
    }
}
