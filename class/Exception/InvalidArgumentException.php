<?php

namespace Atp\Exception;

class InvalidArgumentException extends \InvalidArgumentException
{

    /**
     * @param mixed $value
     * @param string|string[] $expectedType
     * @param string|string[] $expectedValues [optional]
     */
    public function __construct($value, $expectedType, $expectedValues = null)
    {
        $message = sprintf('Expected argument of type "%s", "%s" given', join('", "', (array) $expectedType), is_object($value) ? get_class($value) : gettype($value));

        if ($expectedValues) {
            $message .= ' Did you mean one of these: "';
            $message .= implode('", "', (array) $expectedValues) . '"?';
        }
        
        parent::__construct($message);
    }
}
