<?php

namespace Atp\Exception;

class AvilysException extends \Exception
{

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        parent::__construct('Avilio klaida: ' . $message, $code, $previous);
    }
}
