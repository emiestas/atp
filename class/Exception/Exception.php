<?php

namespace Atp\Exception;

class Exception extends \Exception
{

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        parent::__construct('ATP klaida: ' . $message, $code, $previous);
    }
}
