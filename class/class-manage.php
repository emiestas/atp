<?php

namespace Atp;

class Manage
{
    /**
     * @var \Atp\Core
     */
    public $core;

//    public $user;

//    public $table;

//    public $field_type;

    public $table_record;

    public $table_structure;

    //public $new_table;
    //public $webservices;

    /**
     * @param \Atp\Core $core
     */
    public function __construct(\Atp\Core $core)
    {
        $this->core = $core;
    }

    /**
     * Dokumentai (Administravimas)
     * @return string
     */
    public function tables()
    {
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_TABLE')) {
            return $this->core->printForbidden();
        }

        $documentManager = $this->core->factory->Document();

        $input = filter_var_array($_REQUEST, [
            'table_id' => [
                'filter' => FILTER_VALIDATE_INT
            ],
            'table_parent' => [
                'filter' => FILTER_VALIDATE_INT
            ],
            'action' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => ["regexp" => "/^(edit|delete)$/"],
            ]
        ]);

        $document = null;
        $parentDocument = null;

        if (empty($input['table_id']) === false) {
            $document = $documentManager->Get($input['table_id']);
            if (empty($input['table_parent']) === false) {
                $parentDocument = $documentManager->Get($input['table_parent']);
            }
        }

        if (isset($input['action'])) {
            if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_TABLE') <= 1) {
                return $this->core->printForbidden();
            }

            try {
                if ($input['action'] === 'edit') {
                    $editedDocument = $documentManager->edit();

                    if (empty($input['table_id'])) {
                        $adminJournal = $this->core->factory->AdminJournal();
                        $adminJournal->addRecord(
                            $adminJournal->getCreateRecord(
                                'Dokumentą "' . $editedDocument->getLabel() . '"'
                            )
                        );


                        $message = $this->core->lng('success_table_created');
                    } else {
                        $recordMessage = 'Dokumentą "' . $editedDocument->getLabel() . '"';
                        if ($editedDocument->getLabel() !== $document->getLabel()) {
                            $recordMessage .= ' (' . $document->getLabel() . ')';
                        }
                        $adminJournal = $this->core->factory->AdminJournal();
                        $adminJournal->addRecord($adminJournal->getEditRecord($recordMessage));


                        $message = $this->core->lng('success_table_edited');
                    }

                    $this->core->set_message('success', $message . ' "' . $editedDocument->getLabel() . '"');
                } elseif ($input['action'] === 'delete') {
                    $result = false;
                    if (empty($document) === false) {
                        $result = $documentManager->delete($document->getId());
                    }

                    if ($result) {
                        $adminJournal = $this->core->factory->AdminJournal();
                        $adminJournal->addRecord(
                            $adminJournal->getDeleteRecord(
                                'Dokumentą "' . $document->getLabel() . '"'
                            )
                        );


                        $this->core->set_message('success', $this->core->lng('success_table_deleted') . ' "' . $document->getLabel() . '"');
                    } else {
                        $this->core->set_message('error', $this->core->lng('error_table_not_found'));
                    }
                }
            } catch (\Exception $e) {
                error_log($e);
                $this->core->set_message('error', $e->getMessage());
            }

            header('Location: index.php?m=1');
            exit;
        }

        unset($input);


        if (empty($document) === false) {
            $adminJournal = $this->core->factory->AdminJournal();
            $adminJournal->addRecord($adminJournal->getVisitRecord('Dokumentą "' . $document->getLabel() . '"'));
        }

        $field_sort = $this->core->logic->get_field_sort();
        $field_type = $this->core->logic->get_field_type();
        $classifications = $this->core->logic2->get_classificator(null, null, null, 'ID, LABEL');

        $tmpl = $this->core->factory->Template();
        $tmpl->handle = 'manage_tables_file';
        $tmpl->set_file($tmpl->handle, 'manage_tables.tpl');

        $handles = $tmpl->set_multiblock($tmpl->handle, [
            'document_option',
            'parent_document_option',
            'form',
        ]);

        // TODO: lng_table nenaudoajamas
        $tmpl->set_var(
            $this->core->set_lng_vars(
                ['title', 'table', 'not_chosen', 'save'], [
                'action_edit_table' => 'index.php?m=1&action=edit',
//                    'action_new_table' => 'index.php?m=1&action=new_table'
                ]
            )
        );
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_TABLE') > 1) {
            $tables = $this->core->logic2->get_document();

            foreach ($tables as &$table) {
                $tmpl->set_var(array(
                    'selected' => '',
                    'table_id' => $table['ID'],
                    'table_label' => $table['LABEL']));
                $tmpl->parse($handles['document_option'], 'document_option', true);

                $tmpl->set_var(array(
                    'value' => $table['ID'],
                    'label' => $table['LABEL']));
                $tmpl->parse($handles['parent_document_option'], 'parent_document_option', true);
            }

            $tmpl->set_var($this->core->set_lng_vars(array('parent_table', 'not_chosen')));
            $tmpl->set_var($this->core->set_lng_vars(array('save', 'chosen_table_structure',
                    'title', 'not_chosen')));


            /* @var $fieldConfigurationTypeRepository \Atp\Repository\FieldConfigurationTypeRepository */
            $fieldConfigurationTypeRepository = $this->core->factory->Doctrine()->getRepository('Atp\Entity\FieldConfigurationType');
            $field_configurations = [];
            foreach ($fieldConfigurationTypeRepository->getTypes() as $configuration) {
                $field_configurations['fields[' . $configuration->getName() . ']'] = [
                    'LABEL' => $configuration->getLabel(),
                    'TYPE' => null,
                    'ELEMENT' => strtoupper($configuration->getName())
                ];
            }
            $tmpl->set_var('field_configurations', json_encode($field_configurations));

            $tmpl->set_var($this->core->set_lng_vars(array('not_chosen', 'save'), array(
                    'action_delete_table' => 'index.php?m=1&action=delete')));
            $tmpl->parse($handles['form'], 'form');
        } else {
            $tmpl->clean($handles['form']);
        }

        $tmpl->set_var(array(
            'sys_message' => $this->core->get_messages(),
            'table_id' => (empty($document) ? 'null' : $document->getId()),
            'parent_id' => (empty($parentDocument) ? 0 : $parentDocument->getId()),
            'field_type' => json_encode($field_type),
            'field_sort' => json_encode($field_sort),
//            'classifications' => json_encode($classifications),
            'nr' => (empty($_POST['nr']) ? uniqid() : $_POST['nr']),
            'atp_table_list' => $this->table_list(),
            'fields_edit_block' => $this->fields_edit_form()
        ));

        $tmpl->set_var('GLOBAL_SITE_URL', $this->core->config['GLOBAL_SITE_URL']);

        $tmpl->parse($tmpl->handle . '_out', $tmpl->handle);
        $output = $tmpl->get($tmpl->handle . '_out');

        return $this->core->atp_content($output);
    }

    public function fields_edit_form($asExtraFields = false)
    {
        $tmpl = $this->core->factory->Template();
        $tmpl->handle = 'fields_edit_form';
        $tmpl->set_file($tmpl->handle, 'fields_edit_form.tpl');

        $handles = $tmpl->set_multiblock($tmpl->handle, [
            'field_sort_option',
            'configuration_field_option',
            'ws_hidden_fields_block',
        ]);


        if ($asExtraFields) {
            $tmpl->parse($handles['ws_hidden_fields_block'], 'ws_hidden_fields_block');
        } else {
            $tmpl->set_var($handles['ws_hidden_fields_block'], '');
        }

        /* @var $fieldConfigurationTypeRepository \Atp\Repository\FieldConfigurationTypeRepository */
        $fieldConfigurationTypeRepository = $this->core->factory->Doctrine()->getRepository('Atp\Entity\FieldConfigurationType');
        foreach ($fieldConfigurationTypeRepository->getTypes() as $configuration) {

            $tmpl->set_var([
                'configuration_id' => $configuration->getId(),
                'configuration_label' => $configuration->getLabel()
            ]);
            $tmpl->parse($handles['configuration_field_option'], 'configuration_field_option', true);
        }

        foreach ($this->core->logic->get_field_sort() as $sort) {
            $tmpl->set_var([
                'field_sort_id' => $sort['ID'],
                'field_sort_label' => $sort['LABEL']
            ]);
            $tmpl->parse($handles['field_sort_option'], 'field_sort_option', true);
        }

//        $tmpl->set_var([
//            'source_label' => '',
//            'source_id' => ''
//        ]);

        $tmpl->set_var('GLOBAL_SITE_URL', $this->core->config['GLOBAL_SITE_URL']);
        $tmpl->set_var('GLOBAL_STYLES_URL', $this->core->config['GLOBAL_SITE_URL'] . '/styles/');

        $tmpl->parse($tmpl->handle . '_out', $tmpl->handle);
        $output = $tmpl->get($tmpl->handle . '_out');

        return $output;
    }

    /**
     * Generuoja klasifikatorių laukų pasirinkimą ( Dokumentai (Administravimas))
     * @param array $param Parametrai
     * @param object $tmpl Šablono objektas
     * @param array $data Papildomi duomenys
     * @param int $depth [optional] Lygis. Default: 0
     */
    private function tables_classificator_fields_parse($param, &$tmpl, Array &$data, $depth = 0)
    {
        if (empty($param) === true) {
            $param = array();
        }
        if (empty($param['PARENT_ID']) === true) {
            $tmpl->set_var(array(
                'value' => '',
                'disabled' => ' disabled="true"',
                'label' => str_repeat('&nbsp;', $depth * 3) . 'Klasifikatoriai',
            ));
            foreach ($data as $key => $val) {
                $tmpl->parse($val, $key, true);
            }
            //$tmpl->parse($data['penalty_field'], 'penalty_field', true);
            $depth++;
        }
        $classificators = $this->core->logic2->get_classificator($param, false, 'LABEL DESC');
        foreach ($classificators as &$classificator) {
            $tmpl->set_var(array(
                'value' => '',
                'disabled' => ' disabled="true"',
                'label' => str_repeat('&nbsp;', $depth * 3) . $classificator['LABEL'],
            ));
            foreach ($data as $key => $val) {
                $tmpl->parse($val, $key, true);
            }
            //$tmpl->parse($data['penalty_field'], 'penalty_field', true);

            $fields = $this->core->logic2->get_fields(array('ITEM_ID' => $classificator['ID'],
                'ITYPE' => 'CLASS'), false, 'LABEL DESC');
            foreach ($fields as &$field) {
                $tmpl->set_var(array(
                    'value' => $field['ID'],
                    'disabled' => '',
                    'label' => str_repeat('&nbsp;', ($depth + 1) * 3) . $field['LABEL'],
                ));
                //$tmpl->parse($data['penalty_field'], 'penalty_field', true);
                foreach ($data as $key => &$val) {
                    $tmpl->parse($val, $key, true);
                }
            }

            $this->tables_classificator_fields_parse(array('PARENT_ID' => $classificator['ID']), $tmpl, $data, $depth + 1);
        }
    }

    /**
     * Dokumentų ryšiai (Administravimas)
     * @return string
     */
    public function documents_relations()
    {
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_RELATION')) {
            return $this->core->printForbidden();
        }

        $documentManager = $this->core->factory->Document();

        $table_id = (!empty($_POST['table_id'])) ? $_POST['table_id'] : 0;
        $table_path_id = (!empty($_POST['table_path_id'])) ? $_POST['table_path_id'] : 0;

        if (empty($table_id) === false) {
            $document = $documentManager->Get($table_id);
            if (empty($table_path_id) === false) {
                $parentDocument = $documentManager->Get($table_path_id);
            }
        }

        if (isset($_GET['a']) && $this->core->factory->User()->getLoggedPersonDutyStatus('A_RELATION') > 1) {
            if ($_GET['a'] == 'edit_table_relations') {
                $adminJournal = $this->core->factory->AdminJournal();
                $adminJournal->addRecord($adminJournal->getEditRecord('Dokumento "' . $document->getLabel() . '" ryšį su "' . $parentDocument->getLabel() . '"'));

                $this->core->logic->edit_table_relations();
            }
        }

        if (empty($document) === false && empty($parentDocument) === false) {
            $adminJournal = $this->core->factory->AdminJournal();
            $adminJournal->addRecord($adminJournal->getVisitRecord('Dokumento "' . $document->getLabel() . '" ryšį su "' . $parentDocument->getLabel() . '"'));
        }

        $tmpl = $this->core->factory->Template();
        $tmpl_handler = 'documents_relations_file';
        $tmpl->set_file($tmpl_handler, 'documents_relations.tpl');

        // Tėvinis dokumentas
        $parent_doc_item = $tmpl_handler . 'parent_doc_item_block';
        $tmpl->set_block($tmpl_handler, 'parent_doc_item', $parent_doc_item);
        // Ryšys su dokumentu
        $doc_item = $tmpl_handler . 'doc_item';
        $tmpl->set_block($tmpl_handler, 'doc_item', $doc_item);

        // Tėvinio dokumento lauko pasirinkimai
        //$parent_doc_row_option = $tmpl_handler . 'parent_doc_row_option';
        //$tmpl->set_block($tmpl_handler, 'parent_doc_row_option', $parent_doc_row_option);
        // Tėvinio dokumento lauko pasirinkimų grupės pradžia
        //$parent_doc_row_option_group_begin = $tmpl_handler . 'parent_doc_row_option_group_begin';
        //$tmpl->set_block($tmpl_handler, 'parent_doc_row_option_group_begin', $parent_doc_row_option_group_begin);
        // Tėvinio dokumento lauko pasirinkimų grupės pabaiga
        //$parent_doc_row_option_group_end = $tmpl_handler . 'parent_doc_row_option_group_end';
        //$tmpl->set_block($tmpl_handler, 'parent_doc_row_option_group_end', $parent_doc_row_option_group_end);
        // Tėvinio dokumento lauko pasirinkimas [visas]
        //$parent_doc_row_option_all = $tmpl_handler . 'parent_doc_row_option_all';
        //$tmpl->set_block($tmpl_handler, 'parent_doc_row_option_all', $parent_doc_row_option_all);
        // Tėvinio dokumento straipsnio/klasifikatoriaus pavadinimas
        $parent_doc_row_parent = $tmpl_handler . 'parent_doc_row_parent';
        $tmpl->set_block($tmpl_handler, 'parent_doc_row_parent', $parent_doc_row_parent);
        // Tėvinio dokumento lauko duomenys
        $parent_doc_data = $tmpl_handler . 'parent_doc_data';
        $tmpl->set_block($tmpl_handler, 'parent_doc_data', $parent_doc_data);
        // Tėvinio dokumento laukas
        $parent_doc_row = $tmpl_handler . 'parent_doc_row';
        $tmpl->set_block($tmpl_handler, 'parent_doc_row', $parent_doc_row);

        // Ryšio su dokumentu straipsnio/klasifikatoriaus pavadinimas
        $doc_row_parent = $tmpl_handler . 'doc_row_parent';
        $tmpl->set_block($tmpl_handler, 'doc_row_parent', $doc_row_parent);
        // Ryšio su dokumentu lauko duomenys
        $doc_data = $tmpl_handler . 'doc_data';
        $tmpl->set_block($tmpl_handler, 'doc_data', $doc_data);
        // Ryšio su dokumentu laukai
        $doc_row = $tmpl_handler . 'doc_row';
        $tmpl->set_block($tmpl_handler, 'doc_row', $doc_row);

        // Saugojimo mygtukas
        $button = $tmpl_handler . 'button';
        $tmpl->set_block($tmpl_handler, 'button', $button);
        // Redagavimo forma
        $form_block = $tmpl_handler . 'form_block';
        $tmpl->set_block($tmpl_handler, 'form_block', $form_block);

        $field_sort = $this->core->logic->get_field_sort();
        $field_type = $this->core->logic->get_field_type();

        // Tėvinio dokumento laukai
        $tables = $this->core->logic2->get_document();
        array_unshift($tables, array('ID' => 0, 'LABEL' => '--nepasirinkta--'));
        foreach ($tables as &$table) {
            $tmpl->set_var(array(
                'id' => $table['ID'],
                'label' => $table['LABEL'],
                'selected' => $table_id == $table['ID'] ? ' selected="selected"' : ''));
            $tmpl->parse($parent_doc_item, 'parent_doc_item', true);
        }

        // Laukai su kuriais gali būti kuriamas ryšys
        if (empty($table_id) === false) {
            $table_path = $this->core->logic->get_table_path($table_id, true);
            foreach ($table_path as &$paths) {
                array_unshift($paths, array('ID' => 0, 'LABEL' => '--nepasirinkta--'));
                foreach ($paths as &$path) {
                    $tmpl->set_var(array(
                        'id' => $path['ID'],
                        'label' => $path['LABEL'],
                        'selected' => ($table_path_id == $path['ID']) ? ' selected="selected"' : ''));
                    $tmpl->parse($doc_item, 'doc_item', true);
                }
            }
        }

        if (!empty($table_id) && !empty($table_path_id)) {
            //$table_relations = $this->core->logic->get_table_relations($table_id, $table_path_id);
            $table_relations = $this->core->logic->get_table_relations($table_path_id, $table_id);

            $t_default_relations = $t_relations = array();
            foreach ($table_relations as $relation) {
                //if($relation['DEFAULT'] === NULL && $relation['COLUMN_PARENT_ID'] !== NULL) {
                if (empty($relation['DEFAULT']) === true && empty($relation['COLUMN_PARENT_ID']) === false) {
                    $t_relations[$relation['COLUMN_ID']][] = $relation['COLUMN_PARENT_ID'];
                    //} elseif($relation['DEFAULT'] !== NULL && $relation['COLUMN_PARENT_ID'] === NULL) {
                } elseif (empty($relation['DEFAULT']) === false && empty($relation['COLUMN_PARENT_ID']) === true) {
                    $t_default_relations[$relation['COLUMN_ID']][] = $relation['DEFAULT'];
                }
            }

            $data = array();
            $data['field_type'] = &$field_type;
            $data['t_relations'] = &$t_relations;
            $data['t_default_relations'] = &$t_default_relations;
            //$data['parent_structure_select'] = &$parent_structure_select;
            $data['parent_doc_row_parent'] = &$parent_doc_row_parent;
            //$data['parent_doc_row_option'] = &$parent_doc_row_option;
            //$data['parent_doc_row_option_group_begin'] = &$parent_doc_row_option_group_begin;
            //$data['parent_doc_row_option_group_end'] = &$parent_doc_row_option_group_end;
            //$data['parent_doc_row_option_all'] = &$parent_doc_row_option_all;

            $data['parent_doc_data'] = &$parent_doc_data;
            $data['parent_doc_row'] = &$parent_doc_row;

            $data['doc_row_parent'] = &$doc_row_parent;
            $data['doc_data'] = &$doc_data;
            $data['doc_row'] = &$doc_row;
            $all_fields = array();
            $select_hierarchy = $this->tables_relations_parent_fields($table_path_id, 'DOC', $data, $all_fields, /* $select_hierarchy, */ $tmpl);

            $select_hierarchy = array_reverse($select_hierarchy, true);

            $select_hierarchy[0]['OPT'] = '-- nepasirinkta --';
            $select_hierarchy = array_reverse($select_hierarchy, true);

            $data['select_hierarchy'] = $select_hierarchy;

            $data['all_fields'] = &$all_fields;
            $data['fields_by_type'] = array();
            foreach ($all_fields as $f) {
                $data['fields_by_type'][$f['TYPE']][] = $f['ID'];
            }
//$s = microtime(true);
            $this->tables_relations_fields($table_id, 'DOC', $data, $tmpl);
//$e = microtime(true);
            // Saugojimo mygtukas
            if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_RELATION') > 1) {
                $tmpl->set_var($this->core->set_lng_vars(array('save')));
                $tmpl->parse($button, 'button');
            }

            // Antraštės
            $tmpl->set_var(
                array_merge(
                    (array)
                    $this->core->set_lng_vars(array(
                        'field_name',
                        'field_type',
                        'parent_fields',
                        'column_relations'), array('table_id' => $table_id,
                        'table_path_id' => $table_path_id,
                        'action_edit_realtions' => 'index.php?m=2&a=edit_table_relations')), (array)
                    $this->core->set_lng_vars(array(
                        'field_name',
                        'field_type',
                        'parent_table_sturucture'))
                )
            );
            $tmpl->parse($form_block, 'form_block');
        } else {
            $tmpl->clean($form_block);
        }

        $tmpl->set_var(
            array_merge(
                (array) $this->core->set_lng_vars(
                    array(
                    'title',
                    'table_relations',
                    'not_chosen'
                    ), array('action_choose_table' => 'index.php?m=2')
                ), array('sys_message' => $this->core->get_messages())
            )
        );

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');
        return $this->core->atp_content($output);
    }

//    /**
//     * @deprecated Nenaudojamas
//     */
//    function set_parents(&$h, $a, $bt, &$r = array(), $parents = array())
//    {
//        return null;
//        $has_child = false;
//        foreach ($h as &$hi) {
//            if (isset($hi['CHILDREN'])) {
//                $has_child = true;
//                if (isset($hi['FIELD'])) {
//                    $parents[] = $hi['FIELD']['LABEL'];
//                }
//                if (isset($hi['GROUP'])) {
//                    $parents[] = $hi['GROUP']['LABEL'];
//                }
//                foreach ($hi['CHILDREN'] as &$arr) {
//                    $arr['FIELD']['PARENTS'] = $parents;
//                }
//                $this->set_parents($hi['CHILDREN'], $a, $bt, $r, $parents);
//            }
//        }
//        return $r;
//    }

    private function tables_relations_fields_options($data, &$tmpl = null, $depth = 0, $data2 = array())
    {
        return null;

        if (empty($data2) === false) {
            $fields = &$data2;
        } else {
            $fields = &$data['fields_hierarchy'];
        }

        if (empty($data['fields_by_type'][$data['field']['TYPE']])) {
            return null;
        }
        $valid_fields = &$data['fields_by_type'][$data['field']['TYPE']];
        foreach ($fields as $field2) {
            if (isset($field2['FIELD']) === true && in_array($field2['FIELD']['ID'], $valid_fields)) {
                $tmpl->set_var(array(
                    'selected' => !empty($data['t_relations'][$field['ID']]) && in_array($field2['FIELD']['ID'], $data['t_relations'][$field['ID']]) ? ' selected="selected"' : '',
                    'value' => $field2['FIELD']['ID'],
                    'label' => $field2['FIELD']['LABEL']
                ));
            } elseif (isset($field2['GROUP']) === true) {
                $tmpl->set_var(array(
                    'label' => $field2['FIELD']['LABEL']
                ));
            } else {
                return null;
            }

            if (isset($field2['CHILDREN']) === true) {
                $this->tables_relations_fields_options($data, $tmpl, null, $field2['CHILDREN']);
            }
        }
    }

    // TODO: Nebaigta. Reikia, kad nerodytu šakos, jei nėra rodomų tinkamų lapų.
    function printTree2($tree, $depth = 0, $all_fields, $valid_fields, $selected, &$html = null, $is_group = false, $found = false/* , &$good_field = 0 */)
    {
        if (empty($valid_fields) === true && $valid_fields !== false) {
            return null;
        }

        //$found = false;
        $tabs = str_repeat('&nbsp;', $depth * 3);
        foreach ($tree as $i => $t) {
            if ($is_group === false) {
                if (in_array($i, (array) $valid_fields) || $valid_fields === false) {
                    $found = true;
                    $sel = (in_array($all_fields[$i]['ID'], (array) $selected) ? ' selected="selected"' : '');
                    $html .= sprintf("\t\t<option value='%d' %s class=\"%s\">%s%s</option>\n", $all_fields[$i]['ID'], $sel, 'child level-' . $depth, $tabs, $all_fields[$i]['LABEL']);
                } elseif (/* $found === true && */ isset($t['GROUP'])) {
                    /* $html .= */$parent = sprintf("<option value='-1' class=\"bb %s\" disabled=\"disabled\">%s%s</option>", 'parent level-' . $depth, $tabs, $all_fields[$i]['LABEL']);
                    $found = false;
                }
            } else {
                $parent2 = sprintf("<option value='-1' class=\"aa %s\" disabled=\"disabled\">%s%s</option>", 'parent level-' . $depth, $tabs, $t['OPT']);
            }
            $temp = '';
            if (isset($t['GROUP'])) {
                $found = $this->printTree($t['GROUP'], $depth + 1, $all_fields, $valid_fields, $selected, $temp, true, false);

                $temp = $parent . $temp;
            } elseif ($t['CHILDREN']) {
                $found = $this->printTree($t['CHILDREN'], $depth + 1, $all_fields, $valid_fields, $selected, $temp);
            }
            if ($found === true && $is_group === true) {
                $temp = $parent2 . $temp;
            }

            $html .= sprintf("%s", $temp);
        }

        return $found;
    }

    /**
     *
     * @param array $tree
     * @param int $depth
     * @param array $all_fields
     * @param array $valid_fields
     * @param array $selected
     * @param string $html
     * @param boolen $is_group
     * @return boolean|null
     */
    function printTree($tree, $depth = 0, $all_fields, $valid_fields, $selected, &$html = null, $is_group = false)
    {
        return $this->printTree2($tree, $depth, $all_fields, $valid_fields, $selected, $html, $is_group);
        if (empty($valid_fields) === true) {
            return null;
        }

        $found = false;
        $tabs = str_repeat('&nbsp;', $depth);
        foreach ($tree as $i => $t) {
            if (in_array($i, $valid_fields) && $is_group === false) {
                $found = true;
                $sel = (in_array($all_fields[$i]['ID'], (array) $selected) ? ' selected="selected"' : '');
                $html .= sprintf("\t\t<option value='%d' %s class=\"%s\">%s%s</option>\n", $all_fields[$i]['ID'], $sel, 'child level-' . $depth, $tabs, $all_fields[$i]['LABEL']);
            } else {
                $html .= sprintf("<option value='-1' class=\"%s\" disabled=\"disabled\">%s%s</option>", 'parent level-' . $depth, $tabs, $t['OPT']);
            }
            $temp = '';

            if (isset($t['GROUP'])) {
                $depth++;
                $j = 0;
                foreach ($t['GROUP'] as $group) {
                    if (empty($group['CHILDREN']) === false) {
                        $depth++;
                        $children = $this->printTree($group['CHILDREN'], $depth, $all_fields, $valid_fields, $selected);
                        if ($children === true) {
                            $tabs = str_repeat('&nbsp;', $depth);
                            $temp .= sprintf("<option value='-1' class=\"%s\" disabled=\"disabled\">%s%s</option>", 'parent level-' . $depth, $tabs, $group['OPT']);
                            $depth++;
                            $this->printTree($group['CHILDREN'], $depth, $all_fields, $valid_fields, $selected, $temp);
                            $depth--;
                        }
                        $depth--;
                        $j++;
                    }
                }
                if ($j === 0) {
                    $temp = '';
                    $depth++;
                    $this->printTree($t['GROUP'], $depth, $all_fields, $valid_fields, $selected, $temp, true);
//HERE/				ĘĘĖĮŠŲŪ(	//	$this->printTree($t['GROUP'], $depth, $all_fields, $valid_fields, $selected);
                }
                $depth--;
            }
            /*
              if(isset($t['CHILDREN'])) {
              $depth++;
              $children = $this->printTree($group['CHILDREN'], $depth, $all_fields, $valid_fields, $selected);
              if ($children === true) {
              $tabs = str_repeat('&nbsp;', $depth);
              $temp .= sprintf("<option value='-1' class=\"%s\" disabled=\"disabled\">%s%s</option>", 'parent level-' . $depth, $tabs, $group['OPT']);
              $depth++;
              $this->printTree($group['CHILDREN'], $depth, $all_fields, $valid_fields, $selected, $temp);
              $depth--;
              }
              $depth--;
              $j++;

              } */
            if (empty($temp) === false) {
                $depth++;
                $tabs = str_repeat('&nbsp;', $depth);
                $html .= sprintf("<option value='-1' class=\"%s\" disabled=\"disabled\">%s%s</option>%s", 'parent level-' . $depth, $tabs, $all_fields[$i]['LABEL'], $temp);
            }
        }
        return $found;
    }

    public function tables_relations_fields($id, $type, $data, &$tmpl = null, $depth = 0)
    {
        // TODO: ACT, jei reikės (turėtu) Veiką plečiančių laukų
        if (in_array($type, array('DOC', 'CLASS', 'CLAUSE', 'DEED', 'ACT'), true) === false) {
            //if (in_array($type, array_keys($this->core->_prefix->item), true) === false) {
            trigger_error('Unknown item type', E_USER_WARNING);
            return false;
        }

        if (empty($tmpl) === false) {
            if (isset($data['multiplier'])) {
                $data['multiplier'] = (int) $data['multiplier'];
            } else {
                $data['multiplier'] = 3;
            }
        }

        // Gauna įrašo laukus
        //$fields = $this->core->logic->get_extra_fields($id, $type);
        $fields = $this->core->logic2->get_fields(array('ITYPE' => $type, 'ITEM_ID' => $id), false, '`ORD` ASC');

        foreach ($fields as &$field) {
            if (empty($field) === true) {
                continue;
            }

            $selected = array();
            if (isset($data['t_relations'][$field['ID']])) {
                $selected = $data['t_relations'][$field['ID']];
            }

            $default_value = null;
            $default_field_html = null;
            if (isset($data['t_default_relations'][$field['ID']])) {
                $default_value = $data['t_default_relations'][$field['ID']][0];
                $default_field_html = $this->get_field_html(array(
                    'ID' => $field['ID'],
                    'VALUE' => $default_value));
            }

            $valid_fields = $data['fields_by_type'][$field['TYPE']];
            $valid_fields[] = 0;
            $data['all_fields'][0] = array('ID' => 0, 'LABEL' => '-- nepasirinkta --');
            $opts = '';
            $this->printTree($data['select_hierarchy'], 0, $data['all_fields'], $valid_fields, $selected, $opts);
            $tmpl->set_var('options', $opts);

            // Lauko duomenys
            $tmpl->set_var(array(
                'id' => $field['ID'],
                'disabled' => (isset($default_value) ? ' disabled="disabled" style="display: none;"' : ''),
                'active' => (isset($default_value) ? ' active' : ''),
                'default' => (isset($default_field_html) ? $default_field_html : ''),
                'label' => str_repeat('&nbsp;', $data['multiplier'] * $depth) . $field['LABEL'],
                'type' => $data['field_type'][$field['TYPE']]['LABEL'],
                'type_attr' => (empty($field['TYPE']) ? '' : ' type="' . $field['TYPE'] . '"'),
                $data['parent_doc_row_parent'] => ''));
            $tmpl->parse($data['parent_doc_data'], 'parent_doc_data');
            $tmpl->parse($data['parent_doc_row'], 'parent_doc_row', true);

            // Klasifikatorius arba Straipsnis
            if ($field['TYPE'] === '18' || $field['SORT'] === '6') {
                // Klasifikatorius
                if ($field['TYPE'] === '18') {
                    // TODO: gauna tik pirmo lygio klasifikatorius, reikia visų?
                    //$items = $this->core->logic->get_classification(null, $field['SOURCE']);
                    $items = $this->core->logic2->get_classificator(array('PARENT_ID' => $field['SOURCE']));
                    $type = 'CLASS';
                    $label_attr = 'LABEL';
                } elseif ($field['SORT'] === '6') {
                    // Straipsnis
                    $items = $this->core->logic2->get_clause();
                    $type = 'CLAUSE';
                    $label_attr = 'NAME';
                }
                foreach ($items as &$item) {
                    $tmpl->set_var(array(
                        'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item[$label_attr],
                        $data['parent_doc_data'] => ''));
                    $tmpl->parse($data['parent_doc_row_parent'], 'parent_doc_row_parent');
                    $tmpl->parse($data['parent_doc_row'], 'parent_doc_row', true);
                    $this->tables_relations_fields($item['ID'], $type, $data, $tmpl, $depth + 2);
                }
                /* } else
                  // Klasifikatorius
                  if ($field['TYPE'] === '18') {
                  // TODO: gauna tik pirmo lygio klasifikatorius, reikia visų?
                  //$items = $this->core->logic->get_classification(null, $field['SOURCE']);
                  $items = $this->core->logic2->get_classificator(array('PARENT_ID' => $field['SOURCE']));
                  foreach ($items as &$item) {
                  $tmpl->set_var(array(
                  'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['LABEL'],
                  $data['parent_doc_data'] => ''));
                  $tmpl->parse($data['parent_doc_row_parent'], 'parent_doc_row_parent');
                  $tmpl->parse($data['parent_doc_row'], 'parent_doc_row', true);
                  $this->tables_relations_fields($item['ID'], 'CLASS', $data, $tmpl, $depth + 2);
                  }
                  } else
                  // Straipsnis
                  if ($field['SORT'] === '6') {
                  $items = $this->core->logic2->get_clause();
                  foreach ($items as &$item) {
                  $tmpl->set_var(array(
                  'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['NAME'],
                  $data['parent_doc_data'] => ''));
                  $tmpl->parse($data['parent_doc_row_parent'], 'parent_doc_row_parent');
                  $tmpl->parse($data['parent_doc_row'], 'parent_doc_row', true);
                  $this->tables_relations_fields($item['ID'], 'CLAUSE', $data, $tmpl, $depth + 2);
                  }
                  } else */
                // Veika
            } elseif ($field['TYPE'] === '23') {
                $deed_groups = array(
                    0 => array('ID' => 0, 'TYPE' => 'ACT', 'LABEL' => 'Punktas'),
                    1 => array('ID' => 0, 'TYPE' => 'CLAUSE', 'LABEL' => 'Straipsnis')
                );

                foreach ($deed_groups as $key => &$item) {
                    $tmpl->set_var(array(
                        'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['LABEL'],
                        $data['parent_doc_data'] => ''));
                    $tmpl->parse($data['parent_doc_row_parent'], 'parent_doc_row_parent');
                    $tmpl->parse($data['parent_doc_row'], 'parent_doc_row', true);
                    $this->tables_relations_fields(0, $item['TYPE'], $data, $tmpl, $depth + 2);
                }
            }
        }
    }

    /**
     * <b>(Dokumentų šablonas)</b> Spausdina tėvinio dokumento kinatmuosius ir/arba garžina rastus laukus
     * @author Justinas Malūkas
     * @param int $id Įrašo ID
     * @param string $type Įrašo tipas
     * @param array $data Papildomi duomenys
     * @param array $all_fields Kintamasis į kurį talpins rastus laukus
     * @param object $tmpl [optional] Šablonų valdymo objektas
     * @param int $depth Lauko gylis
     * @return array Laukų hierarchijos masyvas<br/>
     * array(
     *     array(
     *       'FIELD' => array( # Laukas
     *         'ID' => '',
     *         'LABEL' => '',
     *         'TYPE' => ''
     *       ),
     *       'CHILDREN' => array( # Lauko vaikai
     *         array(
     *           'FIELD' => array( # Laukas
     *             'ID' => '', // lauko ID
     *             'LABEL' => '', // lauko pavadinimas
     *             'TYPE' => '', // lauko tipo ID
     *           ),
     *           'CHILDREN' => array( # Laukao vaikai
     *             * CHILDREN RECURSION*
     *           )
     *         ),
     *         array(
     *           'GROUP' => array( # Grupė
     *             'ID' => '', // grupės ID
     *             'LABEL' => '', // grupės pavadinimas
     *           ),
     *           'CHILDREN' => array( # Gupės vaikai
     *             * CHILDREN RECURSION*
     *           )
     *         )
     *       )
     *     )
     * );
     */
    public function tables_relations_parent_fields($id, $type, &$data, &$all_fields = array(), /* &$select_hierarchy, */ &$tmpl = null, $depth = 0)
    {
        // TODO: ACT, jei reikės (turėtu) Veiką plečiančių laukų
        if (in_array($type, array_keys($this->core->_prefix->item), true) === false) {
            trigger_error('Unknown item type', E_USER_WARNING);
            return false;
        }

        if (empty($tmpl) === false) {
            $data['multiplier'] = (isset($data['multiplier']) === true ? (int) $data['multiplier'] : 3);
        }

        // Gauna įrašo laukus
        if ($type === 'WS') {
            $fields = array($this->core->logic2->get_fields(array('ITYPE' => $type,
                    'ID' => $id)));
        } else {
            $fields = $this->core->logic2->get_fields(array('ITYPE' => $type, 'ITEM_ID' => $id), false, '`ORD` ASC');
        }

        //$i = 0;
        //$fields_arr = array();
        $select_hierarchy = array();
        foreach ($fields as &$field) {
            if (empty($field) === true) {
                continue;
            }

            $all_fields[$field['ID']] = $field;
            $select_hierarchy[$field['ID']]['OPT'] = $field['LABEL'];

            // Lauko duomenys
            if (empty($tmpl) === false) {
                $tmpl->set_var(array(
                    'label' => str_repeat('&nbsp;', $data['multiplier'] * $depth) . $field['LABEL'],
                    'type' => $data['field_type'][$field['TYPE']]['LABEL'],
                    $data['doc_row_parent'] => ''));
                $tmpl->parse($data['doc_data'], 'doc_data');
                $tmpl->parse($data['doc_row'], 'doc_row', true);
            }
            // Klasifikatorius
            if ($field['TYPE'] === '18') {
                // TODO: gauna tik pirmo lygio klasifikatorius, reikia visų?
                //$items = $this->core->logic->get_classification(null, $field['SOURCE']);
                $items = $this->core->logic2->get_classificator(array('PARENT_ID' => $field['SOURCE']));
                $j = 0;
                foreach ($items as &$item) {
                    $select_hierarchy[$field['ID']]['GROUP'][$item['ID']]['OPT'] = $item['LABEL'];
                    if (empty($tmpl) === false) {
                        $tmpl->set_var(array(
                            'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['LABEL'],
                            $data['doc_data'] => ''));
                        $tmpl->parse($data['doc_row_parent'], 'doc_row_parent');
                        $tmpl->parse($data['doc_row'], 'doc_row', true);
                    }

                    $select_hierarchy[$field['ID']]['GROUP'][$item['ID']]['CHILDREN'] = $this->tables_relations_parent_fields($item['ID'], 'CLASS', $data, $all_fields, /* $select_hierarchy, */ $tmpl, $depth + 2);

                    $j++;
                }
            } elseif ($field['SORT'] === '6') {
                // Straipsnis
                $items = $this->core->logic2->get_clause();
                $j = 0;

                foreach ($items as &$item) {
                    $select_hierarchy[$field['ID']]['GROUP'][$item['ID']]['OPT'] = $item['NAME'];

                    if (empty($tmpl) === false) {
                        $tmpl->set_var(array(
                            'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['NAME'],
                            $data['doc_data'] => ''));
                        $tmpl->parse($data['doc_row_parent'], 'doc_row_parent');
                        $tmpl->parse($data['doc_row'], 'doc_row', true);
                    }

                    $select_hierarchy[$field['ID']]['GROUP'][$item['ID']]['CHILDREN'] = $this->tables_relations_parent_fields($item['ID'], 'CLAUSE', $data, $all_fields, /* $select_hierarchy, */ $tmpl, $depth + 2);

                    $j++;
                }
            } elseif ($field['TYPE'] === '23') {
                // Veika
                $deed_groups = array(
                    0 => array('ID' => 0, 'TYPE' => 'ACT', 'LABEL' => 'Punktas'),
                    1 => array('ID' => 0, 'TYPE' => 'CLAUSE', 'LABEL' => 'Straipsnis')
                );

                foreach ($deed_groups as $key => &$arr) {
                    $select_hierarchy[$field['ID']]['GROUP'][$key]['OPT'] = $arr['LABEL'];
                    if (empty($tmpl) === false) {
                        $tmpl->set_var(array(
                            'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $arr['LABEL'],
                            $data['doc_data'] => ''));
                        $tmpl->parse($data['doc_row_parent'], 'doc_row_parent');
                        $tmpl->parse($data['doc_row'], 'doc_row', true);
                    }

                    $select_hierarchy[$field['ID']]['GROUP'][$key]['CHILDREN'] = $this->tables_relations_parent_fields(0, $arr['TYPE'], $data, $all_fields, /* $select_hierarchy, */ $tmpl, $depth + 2);
                }
            }
            if ($field['WS'] === '1') {
                $h = &$select_hierarchy[$field['ID']]['GROUP'][$field['ID']];
                //$h = &$select_hierarchy[$field['ID']]['GROUP'][0];
                $h['OPT'] = 'Web-servisai';

                if (empty($tmpl) === false) {
                    $tmpl->set_var(array(
                        'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . 'Web-servisai',
                        $data['doc_data'] => ''));
                    $tmpl->parse($data['doc_row_parent'], 'doc_row_parent');
                    $tmpl->parse($data['doc_row'], 'doc_row', true);
                }

                $ws = $this->core->logic2->get_ws();
                $relations = $this->core->logic2->get_webservices_fields(array('FIELD_ID' => $field['ID']));
                $fields_struct = $this->core->logic2->get_fields(array('ITYPE' => 'WS',
                    'ITEM_ID' => $field['ID']), false, '`ORD` ASC');

                foreach ($relations as &$relation) {
                    $info = $ws->get_info($relation['WS_NAME']);
                    if (empty($info)) {
                        continue;
                    }

                    $h['GROUP'][$info['name']]['OPT'] = $info['label'];

                    if (empty($tmpl) === false) {
                        $tmpl->set_var(array(
                            'label' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 2)) . $info['label'],
                            $data['doc_data'] => ''));
                        $tmpl->parse($data['doc_row_parent'], 'doc_row_parent');
                        $tmpl->parse($data['doc_row'], 'doc_row', true);
                    }

                    $ws_fields = $this->core->logic2->get_webservices(array('WS_NAME' => $relation['WS_NAME'],
                        'PARENT_FIELD_ID' => $field['ID']));

                    $children = array();
                    foreach ($ws_fields as $ws_field) {
                        $arr = $fields_struct[$ws_field['FIELD_ID']];
                        $ch = $this->tables_relations_parent_fields($arr['ID'], 'WS', $data, $all_fields, $tmpl, $depth + 3);
                        foreach ($ch as $k => $c) {
                            $h['GROUP'][$info['name']]['CHILDREN'][$k] = $c;
                        }
                    }
                }
            }

            //$i++;
        }

        return $select_hierarchy;
    }

    /**
     * Kompetencijų lentelė straipsnių prijungimui prie vartotojo / skyriaus vartotojų
     * @param int $user_id
     * @param int $department_id
     * @return string
     */
    public function table_user_competence($user_id, $department_id)
    {
        $html_manage = '';

        if (empty($user_id) === true && empty($department_id) === true) {
            return $html_manage;
        }

        $clauses = $clauses = $this->core->logic2->get_clause();
        $user_clauses = $this->core->logic->get_user_clauses(array('PEOPLE_ID' => $user_id,
            'STRUCTURE_ID' => $department_id));

        $tpl_arr = $this->core->getTemplate('table');
        $columns = array(
            'NAME' => $this->core->lng('atp_clause_clause'),
            'ADD' => $this->core->lng('atp_table_user_competence_add')
        );

        // table
        $table = array();
        $table['table_class'] = 'user_competences';
        $html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

        // header
        $header = array('row_html' => '');
        $header['row_class'] = 'table-row table-header';
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);

        foreach ($columns as $col_name => $col) {
            // cell
            $cell = array('cell_html' => '');
            $cell['cell_class'] = 'table-cell cell-' . $col_name;
            $cell['cell_content'] = $col;
            $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
        }
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

        foreach ($clauses as $key => $clause) {
            // row
            $row = array('row_html' => '', 'row_class' => '');
            $row['row_class'][] = 'table-row';
            $row['row_class'] = join(' ', $row['row_class']);
            $html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

            foreach ($columns as $col_name => $col) {
                // cell
                $cell = array('cell_html' => '', 'cell_content' => '');
                $cell['cell_class'] = 'table-cell cell-' . $col_name;
                if ($col_name === 'ADD') {
                    $cell['cell_content'] .= '<input type="checkbox" name="clauses[]" value="' . $clause['ID'] . '"'
                        . (isset($user_clauses[$clause['ID']]) ? ' checked="checked"' : '') . '/>';
                } else {
                    $cell['cell_content'] .= $clause[$col_name];
                }
                $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
            }

            $html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
        }

        $html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

        return $html_manage;
    }

    /**
     * Sugeneruoja teisės aktų lentelę
     * @return string
     */
    public function table_acts()
    {
        $html_manage = '';

        // Surenka klasifikatorius
        $tables = $this->core->logic2->get_acts();

        // Surenkami papildomi duomenys apie įrašus
        foreach ($tables as $table) {
            $new_tables[$table['ID']] = $table;

            // Surenka galimus veiksmus
            $new_tables[$table['ID']]['ACTIONS'] = array();
            $new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="expand_collapse_block">
					<div class="expand_collapse_img_block">
						<img style="cursor: pointer;" src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/atp/expand_doc_action.png" width="29" height="29">
					</div>
					<div class="actions_block" style="display: none;">
						<img style="display: block;" src="' . $this->core->config['GLOBAL_SITE_URL'] . 'actions/top_img_actions_buttons.png" width="132" height="8">
						<div class="actions_buttons">';
            $new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button addChild"><span>Pridėti vaiką</span></div>';
            $new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button remove"><span>Trinti</span></div>';
            // Išjungta
            /* $new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button addFields"><span>Sukurti laukus</span></div>'; */
            $new_tables[$table['ID']]['ACTIONS'][] = '
						</div>
					</div>
				</div>';
            $new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="action_label">'/*
                  '<div class="atp-html-button edit"><span>' . $this->core->lng('edit') . '</span></div>' */ .
                '<a class="atp-html-button edit dialog" href="index.php?m=120&id=' . $table['ID'] . '&action=editform">' . $this->core->lng('edit') . '</a>
				</div>';

            $new_tables[$table['ID']]['ACTIONS'] = join(' ', $new_tables[$table['ID']]['ACTIONS']);

            // Gaunamas įrašo lygis
            $new_tables[$table['ID']]['LEVEL'] = $this->core->logic->get_table_level($tables, $table['ID']);
            // Nurodomas išskleidimo stulpelis, jei laukas turi vaikų
            if ($this->core->logic->table_has_children($tables, $table['ID'], 'ACTS')) {
                $new_tables[$table['ID']]['TOGGLE'] = '<div class="collapse atp-table-list-toggle"></div>';
                $toggle_class = 'collapse';
            } else {
                $toggle_class = 'none';
            }

            $new_tables[$table['ID']]['LABEL'] = '<div class="cell-LABEL-container"><div class="' . $toggle_class . ' atp-table-list-toggle"></div><span>' . $new_tables[$table['ID']]['LABEL'] . '</span></div>';
        }
        $tables = &$new_tables;

        // Įrašai surikiuojami pagal hierarchiją (tevas -> vaikai, tevas -> vaikai)
        $tables = $this->core->logic->sort_children_to_parent($tables, 'ID', 'PARENT_ID');

        $tpl_arr = $this->core->getTemplate('table2');

        // Nurodomi išvedami lentelės stulpeliai
        $columns = array(
            /* 'TOGGLE' => $this->core->lng('atp_table_toggle'), */
            'NR' => $this->core->lng('atp_act_number'),
            'LABEL' => $this->core->lng('atp_act_label'),
            'VALID_FROM' => $this->core->lng('atp_act_valid_from'),
            'VALID_TILL' => $this->core->lng('atp_act_valid_till')
        );
        // Tikrinama teisė į veiksmų stulpelio matymą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_ACT') > 1) {
            $columns['ACTIONS'] = $this->core->lng('atp_table_actions');
        }

        // table
        $table = array();
        $table['table_class'] = 'acts';
        $html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

        // Lentelės antraštė
        $header = array('row_html' => '');
        $header['row_class'] = 'div-row div-header';
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);
        foreach ($columns as $col_name => $col) {
            // cell
            $cell = array('cell_html' => '');
            $cell['cell_class'] = 'div-cell cell-' . $col_name;
            $cell['cell_content'] = $col;
            $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
        }
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

        // Lentelės įrašų eilutės
        foreach ($tables as $key => $table) {
            // Eilutės duomenys
            $row = array();
            $row['row_html'] = 'id="atp-table-list-item-id-' . $table['ID'] . '"';
            $row['row_class'][] = 'div-row';
            if ($table['LEVEL']) {
                $row['row_class'][] = 'child';
            }
            $row['row_class'][] = 'level-' . $table['LEVEL'];
            $row['row_class'][] = 'child-of-' . $table['PARENT_ID'];
            $row['row_class'] = join(' ', $row['row_class']);
            $html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

            // Eilutės stulpeiai
            foreach ($columns as $col_name => $col) {
                // Stulpelio duomenys
                $cell = array('cell_html' => '', 'cell_content' => '');
                if (in_array($col_name, array('VALID_FROM', 'VALID_TILL'))) {
                    $table[$col_name] = date('Y-m-d', strtotime($table[$col_name]));
                }
                $cell['cell_class'] = 'div-cell cell-' . $col_name;
                $cell['cell_content'] .= $table[$col_name];
                $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
            }

            $html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
        }

        $html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

        return $html_manage;
    }

    function appendHTML(\DOMNode $parent, $source)
    {
        $dom = new \DOMDocument('1.0', 'UTF-8');
        $source = mb_convert_encoding($source, 'HTML-ENTITIES', 'UTF-8');
        @$dom->loadHTML($source);
        foreach ($dom->getElementsByTagName('body')->item(0)->childNodes as $node) {
            $importedNode = $parent->ownerDocument->importNode($node, true);
            $parent->appendChild($importedNode);
        }
    }

    /**
     * Formuoja dokumento - atp būsenų - būsenų ryšio lentelę
     * @return string
     */
    public function table_status_relations()
    {
        $html_manage = '';

        // Sukurti ryšiai
        $relations = $this->core->logic2->get_status_relations();

        // Dokumentų informacija
        $documents = $this->core->logic2->get_document(null, null, 'LABEL ASC');
        // Būsenų informacija
        $status = $this->core->logic2->get_status(null, null, 'LABEL ASC');
        // ATP būsenų informacija
        $atp_status = $this->core->logic->get_document_statuses();

        // ATP būsenos abecelės tvarka
        usort($atp_status, function ($a, $b) {
            return strcmp($a['LABEL'], $b['LABEL']);
        });

        // Atskato ATP būsenų indeksus
        $atp_status_tmp = array();
        foreach ($atp_status as &$arr) {
            $atp_status_tmp[$arr['ID']] = $arr;
        }
        $atp_status = &$atp_status_tmp;

        // Perfomuoja ryšių masyvą. DOCUMENT_NAME ASC, ATP_SATUS_NAME ASC, STATUS_NAME ASC
        $md_relation = array();
        foreach ($relations as &$relation) {
            $i = array_search($relation['DOCUMENT_ID'], array_keys($documents));
            $j = array_search($relation['ATP_STATUS_ID'], array_keys($atp_status));
            $k = array_search($relation['STATUS_ID'], array_keys($status));

            $md_relation[$i]['ID'] = $relation['DOCUMENT_ID'];
            $md_relation[$i]['LABEL'] = $documents[$relation['DOCUMENT_ID']]['LABEL'];
            $md_relation[$i]['CHILDREN'][$j]['ID'] = $relation['ATP_STATUS_ID'];
            $md_relation[$i]['CHILDREN'][$j]['LABEL'] = $atp_status[$j]['LABEL'];
            $md_relation[$i]['CHILDREN'][$j]['CHILDREN'][$k] = $relation['ID'];
        }
        ksort($md_relation);
        foreach ($md_relation as &$relation) {
            foreach ($relation['CHILDREN'] as &$a) {
                ksort($a['CHILDREN']);
            }
            ksort($relation['CHILDREN']);
        }
        $ordered_relations = array();
        foreach ($md_relation as &$relation) {
            foreach ($relation['CHILDREN'] as &$a) {
                foreach ($a['CHILDREN'] as &$b) {
                    $ordered_relations[] = $relations[$b];
                }
            }
        }

        // Lentelės spausdinimas
        $current_document = '';
        $current_atp_status = '';

        $document_row_count = 1;
        $atp_status_row_count = 1;

        $dom = new \DOMDocument('1.0', 'UTF-8');

        $table = $dom->createElement('table');
        $table->setAttribute('border', 1);
        $table->setAttribute('id', 'table-table');
        $table->setAttribute('class', 'gridtable');
        $dom->appendChild($table);

        $action = '
			<div class="expand_collapse_block">
				<div class="expand_collapse_img_block">
					<img style="cursor: pointer;" src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/atp/expand_doc_action.png" width="29" height="29">
				</div>
				<div class="actions_block" style="display: none;">
					<img style="display: block;" src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/actions/top_img_actions_buttons.png" width="132" height="8">
					<div class="actions_buttons">
						<div class="atp-html-button remove"><span>' . $this->core->lng('delete') . '</span></div>
					</div>
				</div>
			</div>
			<div class="action_label">
				<a class="atp-html-button edit dialog" href="index.php?m=126&id=[doc_id]&action=editrelation">' . $this->core->lng('edit') . '</a>
			</div>';

        $this->appendHTML($table, '<tr class="table-row table-header"><td class="table-cell cell-DOCUMENT">Dokumentas</td>' . /* '<td class="table-cell cell-ATP_STATUS">ATP būsena</td>'. */'<td class="table-cell cell-STATUS">Būsena</td><td class="table-cell cell-ACTIONS">Veiksmai</td></tr>');

        foreach ($ordered_relations as $relation) {
            $row = $dom->createElement('tr');
            $row->setAttribute('class', 'table-row');
            $table->appendChild($row);

            if ($current_document !== $relation['DOCUMENT_ID']) {
                $current_document = $relation['DOCUMENT_ID'];
                $document_row_count = 1;

                $doc_col = $dom->createElement('td', $documents[$relation['DOCUMENT_ID']]['LABEL']);
                $doc_col->setAttribute('class', 'table-cell cell-DOCUMENT');
                $action_col = $dom->createElement('td');
                $action_col->setAttribute('class', 'table-cell cell-ACTIONS');
                $action_col->setAttribute('for', $relation['DOCUMENT_ID']);
                $this->appendHTML($action_col, str_replace('[doc_id]', $relation['DOCUMENT_ID'], $action));
                $row->appendChild($doc_col);
            } else {
                $doc_col->setAttribute('rowspan', ++$document_row_count);
                $action_col->setAttribute('rowspan', $document_row_count);
            }
            /*
              if ($current_atp_status !== $relation['ATP_STATUS_ID']) {
              $current_atp_status = $relation['ATP_STATUS_ID'];
              $atp_status_row_count = 1;

              $atp_col = $dom->createElement('td', $atp_status[$relation['ATP_STATUS_ID']]['LABEL']);
              $atp_col->setAttribute('class', 'table-cell cell-ATP_STATUS');
              $row->appendChild($atp_col);
              } else {
              $atp_col->setAttribute('rowspan', ++$atp_status_row_count);
              } */
            $status_col = $dom->createElement('td', $status[$relation['STATUS_ID']]['LABEL']);
            $status_col->setAttribute('class', 'table-cell cell-STATUS');
            $row->appendChild($status_col);

            if ($document_row_count === 1) {
                $row->appendChild($action_col);
            }
        }

        return $html_manage = $dom->saveHTML();
    }

    public function table_status_list()
    {
        $html_manage = '';

        // Surenka klasifikatorius
        $tables = $this->core->logic2->get_status(null, null, 'LABEL ASC');

        // Surenkami papildomi duomenys apie įrašus
        foreach ($tables as $table) {
            $new_tables[$table['ID']] = $table;

            // Surenka galimus veiksmus
            $new_tables[$table['ID']]['ACTIONS'] = array();
            $new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="expand_collapse_block">
					<div class="expand_collapse_img_block">
						<img style="cursor: pointer;" src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/atp/expand_doc_action.png" width="29" height="29">
					</div>
					<div class="actions_block" style="display: none;">
						<img style="display: block;" src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/actions/top_img_actions_buttons.png" width="132" height="8">
						<div class="actions_buttons">';
            $new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button remove"><span>Trinti</span></div>';
            $new_tables[$table['ID']]['ACTIONS'][] = '
						</div>
					</div>
				</div>';
            $new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="action_label">'/*
                  '<div class="atp-html-button edit"><span>' . $this->core->lng('edit') . '</span></div>' */ .
                '<a class="atp-html-button edit dialog" href="index.php?m=126&id=' . $table['ID'] . '&action=editstatus">' . $this->core->lng('edit') . '</a>
				</div>';

            $new_tables[$table['ID']]['ACTIONS'] = join(' ', $new_tables[$table['ID']]['ACTIONS']);

            // Gaunamas įrašo lygis
            $new_tables[$table['ID']]['LEVEL'] = $this->core->logic->get_table_level($tables, $table['ID']);
            // Nurodomas išskleidimo stulpelis, jei laukas turi vaikų
//            if ($this->core->logic->table_has_children($tables, $table['ID'], 'STATUS')) {
//                $new_tables[$table['ID']]['TOGGLE'] = '<div class="collapse atp-table-list-toggle"></div>';
//                $toggle_class = 'collapse';
//            } else {
            $toggle_class = 'none';
//            }

            $new_tables[$table['ID']]['LABEL'] = '<div class="cell-LABEL-container"><div class="' . $toggle_class . ' atp-table-list-toggle"></div><span>' . $new_tables[$table['ID']]['LABEL'] . '</span></div>';
        }
        $tables = &$new_tables;

        // Įrašai surikiuojami pagal hierarchiją (tevas -> vaikai, tevas -> vaikai)
        $tables = $this->core->logic->sort_children_to_parent($tables, 'ID', 'PARENT_ID');

        $tpl_arr = $this->core->getTemplate('table2');

        // Nurodomi išvedami lentelės stulpeliai
        $columns = array(
            'LABEL' => $this->core->lng('atp_act_label'),
        );
        // Tikrinama teisė į veiksmų stulpelio matymą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_ACT') > 1) {
            $columns['ACTIONS'] = $this->core->lng('atp_table_actions');
        }

        // table
        $table = array();
        $table['table_class'] = 'status';
        $html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

        // Lentelės antraštė
        $header = array('row_html' => '');
        $header['row_class'] = 'div-row div-header';
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);
        foreach ($columns as $col_name => $col) {
            // cell
            $cell = array('cell_html' => '');
            $cell['cell_class'] = 'div-cell cell-' . $col_name;
            $cell['cell_content'] = $col;
            $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
        }
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

        // Lentelės įrašų eilutės
        foreach ($tables as $key => $table) {
            // Eilutės duomenys
            $row = array();
            $row['row_html'] = 'id="atp-table-list-item-id-' . $table['ID'] . '"';
            $row['row_class'][] = 'div-row';
            if ($table['LEVEL']) {
                $row['row_class'][] = 'child';
            }
            $row['row_class'][] = 'level-' . $table['LEVEL'];
            $row['row_class'][] = 'child-of-' . $table['PARENT_ID'];
            $row['row_class'] = join(' ', $row['row_class']);
            $html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

            // Eilutės stulpeiai
            foreach ($columns as $col_name => $col) {
                // Stulpelio duomenys
                $cell = array('cell_html' => '', 'cell_content' => '');
                $cell['cell_class'] = 'div-cell cell-' . $col_name;
                $cell['cell_content'] .= $table[$col_name];
                $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
            }

            $html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
        }

        $html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

        return $html_manage;
    }

    public function table_search_list($data)
    {
        // Puslapiavimas
        include_once($this->core->config['REAL_URL'] . 'helper/paginator.php');
        $pg = new \Paginator();
        $pg->current_page = empty($_GET['_page']) ? 1 : $_GET['_page'];
        $pg->items_total = count($data);
        $pg->items_per_page = 10;
        $pg->paginate();

        // Lentelės šablonas
        $tpl_arr = $this->core->getTemplate('table');

        // Lentelė
        $html_manage = $this->core->returnHTML(array('table_class' => 'search_fields'), $tpl_arr[0]);

        // Lentelės antraštė
        $html_manage .= $this->core->returnHTML(array('row_html' => '', 'row_class' => 'div-row div-header'), $tpl_arr[1]);

        // Lentelės stulpeliai
        $columns = array(
            'DATE' => $this->core->lng('atp_search_item_date') . 'Data, Nr.',
            'TYPE' => $this->core->lng('atp_search_item_type') . 'Tipas',
            'OFFICER' => $this->core->lng('atp_search_item_officer') . 'Pareigūnas',
            'STATUS' => $this->core->lng('atp_search_item_status') . 'Būsena',
        );
        foreach ($columns as $col_name => $col) {
            // Lentelės langelis
            $html_manage .= $this->core->returnHTML(array(
                'cell_html' => '',
                'cell_class' => 'div-cell cell-' . $col_name,
                'cell_content' => $col), $tpl_arr[2]);
        }
        $html_manage .= $tpl_arr[3];


        if ($pg->items_total) {
            // Visų dokumentų duomenys
            $documents = $this->core->logic2->get_document();

            // Lentelės įrašų eilutės
            foreach ($data as $key => $record) {
                // Pagal puslapiavimą apribojamas duomenų kiekis
                if ($key < $pg->limit_start) {
                    continue;
                } elseif ($key >= $pg->limit_end) {
                    break;
                }

                // Įrašo dokumentas
                list($doc_id) = explode('p', $record);
                // Įrašo dokumento duomenys
                $document = &$documents[$doc_id];
                // Įrašo duoemnys
                $record = $this->core->logic2->get_record_table($record, 'C.*,E.PEOPLE_ID,E.STRUCTURE_ID,F.FIRST_NAME,F.LAST_NAME,A.STATUS, A.DOC_STATUS, A.ID RID');
                // Lentelės eilutė
                $row = array();
                $row['row_html'] = '';
                $row['row_class'][] = 'div-row';
                $row['row_class'] = join(' ', $row['row_class']);
                $html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

                // Eilutės stulpeiai
                foreach ($columns as $col_name => $col) {
                    // Stulpelio duomenys
                    $cell = array(
                        'cell_html' => '',
                        'cell_content' => '',
                        'cell_class' => 'div-cell cell-' . $col_name);
                    $url = array(
                        '<a href="index.php?m=5&id=' . $record['RID'] . '">',
                        '</a>'
                    );
                    if ($col_name === 'DATE') {
                        $cell['cell_content'] .= $url[0] . date('Y-m-d', strtotime($record['CREATE_DATE'])) . $url[1] . '<br/>' . $url[0] . $record['RECORD_ID'] . $url[1];
                    } elseif ($col_name === 'TYPE') {
                        $cell['cell_content'] .= $url[0] . $document['LABEL'] . $url[1];
                    } elseif ($col_name === 'OFFICER') {
                        $cell['cell_content'] .= $url[0] . join(' ', array_filter(array(
                                $record['FIRST_NAME'], $record['LAST_NAME']))) . $url[1];
                    } elseif ($col_name === 'STATUS') {
//                      $status = $this->core->logic->find_document_status(array(
//                          'ID' => $record['DOC_STATUS']));
						$status = $this->core->db_getItemByID(PREFIX."ATP_STATUS", $record['DOC_STATUS']);
                        $cell['cell_content'] .= '<div class="status_label"><span class="document_status-' . $status['ID'] . ' document_status-' . $status['LABEL'] . '">' . $url[0] . $status['LABEL'] . $url[1] . '</span></div>';
                    }
                    $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
                }

                $html_manage .= $tpl_arr[3];
            }
        }
        // Lentelės pabaiga
        $html_manage .= $tpl_arr[4];

        // Puslapiai
        $html_manage .= $pg->display_pages();

        return $html_manage;
    }

    public function table_reports_list()
    {
        $html_manage = '';
        /*
          $total = count($data);
          $per_page = 10;
          $page = empty($page) ? 1 : $_GET['_page'];

          include_once($this->core->config['REAL_URL'] . 'helper/paginator.php');
          $pg = new \Paginator();
          $pg->current_page = $page;
          $pg->items_total = $total;
          $pg->items_per_page = $per_page;
          $pg->paginate(); */

        $tpl_arr = $this->core->getTemplate('table');

        // Nurodomi išvedami lentelės stulpeliai
        $columns = array(
            'NAME' => $this->core->lng('') . 'Pavadinimas',
            'DATE_FROM' => $this->core->lng('') . 'Data nuo',
            'DATE_TILL' => $this->core->lng('') . 'Data iki',
            'GENERATE' => $this->core->lng('') . 'Generuoti ataskaitą'
        );

        // table
        $table = array();
        $table['table_class'] = 'search_fields';
        $html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

        // Lentelės antraštė
        $header = array('row_html' => '');
        $header['row_class'] = 'div-row div-header';
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);
        foreach ($columns as $col_name => $col) {
            // cell
            $cell = array('cell_html' => '');
            $cell['cell_class'] = 'div-cell cell-' . $col_name;
            $cell['cell_content'] = $col;
            $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
        }
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

        $data = $this->core->logic2->get_reports(array('PEOPLE_ID' => $this->core->factory->User()->getLoggedUser()->person->id,
            'DEPARTMENT_ID' => $this->core->factory->User()->getLoggedPersonDuty()->department->id));
        if (count($data)) {
            $dparams = array(
                'm' => 19,
                'action' => 'download'
            );
            // Lentelės įrašų eilutės
            foreach ($data as $key => $report) {
                $dparams['id'] = $report['ID'];
                $durl = 'index.php?' . http_build_query($dparams);

                // Eilutės duomenys
                $row = array();
                $row['row_html'] = '';
                $row['row_class'][] = 'div-row';
                $row['row_class'] = join(' ', $row['row_class']);
                $html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);
                // Eilutės stulpeiai
                foreach ($columns as $col_name => $col) {
                    // Stulpelio duomenys
                    $cell = array('cell_html' => '', 'cell_content' => '');
                    $cell['cell_class'] = 'div-cell cell-' . $col_name;
                    if ($col_name === 'DATE_FROM') {
                        $cell['cell_content'] .= '<input type="text" class="atp-date from atp-input">';
                    } elseif ($col_name === 'DATE_TILL') {
                        $cell['cell_content'] .= '<input type="text" class="atp-date till atp-input">';
                    } elseif ($col_name === 'GENERATE') {
                        /* $cell['cell_content'] .=
                          '<span class="icon xls"><img src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/icons/xls.gif" alt="XLS" title="XLS"/></span>
                          <span class="icon doc"><img src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/icons/doc.gif" alt="DOC" title="DOC"/></span>
                          <span class="icon pdf"><img src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/icons/pdf.gif" alt="PDF" title="PDF"/></span>'; */
                        $cell['cell_content'] .= '<span class="icon" title="Generuoti CVS failą"><a href="' . $durl . '&type=xml&from=&till="><div class="xls"></div></a></span>
								<span class="icon" title="Generuoti DOC failą"><a href="' . $durl . '&type=doc&from=&till="><div class="doc"></div></a></span>
								<span class="icon" title="Generuoti PDF failą"><a href="' . $durl . '&type=pdf&from=&till="><div class="pdf"></div></a></span>';
                    } else {
                        $cell['cell_content'] .= $report[$col_name];
                    }
                    $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
                }

                $html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
            }
        }

        $html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);
        //$html_manage .=  $pg->display_pages();

        return $html_manage;
    }

    /**
     * Sugeneruoja paieškos laukų lentelę
     * @return string
     */
    public function table_fields()
    {
        $html_manage = '';

        // Surenka klasifikatorius
        $tables = $this->core->logic2->get_search_field(array('VALID' => 1), false, null, 'ID, NAME LABEL');

        // Surenkami papildomi duomenys apie įrašus
        foreach ($tables as $table) {
            $new_tables[$table['ID']] = $table;

            // Surenka galimus veiksmus
            $new_tables[$table['ID']]['ACTIONS'] = array();
            $new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="expand_collapse_block">
					<div class="expand_collapse_img_block">
						<img style="cursor: pointer;" src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/atp/expand_doc_action.png" width="29" height="29">
					</div>
					<div class="actions_block" style="display: none;">
						<img style="display: block;" src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/actions/top_img_actions_buttons.png" width="132" height="8">
						<div class="actions_buttons">';
            $new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button remove"><span>Trinti</span></div>';
            $new_tables[$table['ID']]['ACTIONS'][] = '
						</div>
					</div>
				</div>';
            $new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="action_label">'/*
                  '<div class="atp-html-button edit"><span>' . $this->core->lng('edit') . '</span></div>' */ .
                '<a class="atp-html-button edit dialog" href="index.php?m=125&id=' . $table['ID'] . '&action=editform">' . $this->core->lng('edit') . '</a>
				</div>';

            $new_tables[$table['ID']]['ACTIONS'] = join(' ', $new_tables[$table['ID']]['ACTIONS']);

            // Gaunamas įrašo lygis
            //$new_tables[$table['ID']]['LEVEL'] = $this->core->logic->get_table_level($tables, $table['ID']);
            // Nurodomas išskleidimo stulpelis, jei laukas turi vaikų
            //if ($this->core->logic->table_has_children($tables, $table['ID'])) {
            //	$new_tables[$table['ID']]['TOGGLE'] = '<div class="collapse atp-table-list-toggle"></div>';
            //	$toggle_class = 'collapse';
            //} else
            $toggle_class = 'none';

            $new_tables[$table['ID']]['LABEL'] = '<div class="cell-LABEL-container"><div class="' . $toggle_class . ' atp-table-list-toggle"></div><span>' . $new_tables[$table['ID']]['LABEL'] . '</span></div>';
        }
        $tables = &$new_tables;

        // Įrašai surikiuojami pagal hierarchiją (tevas -> vaikai, tevas -> vaikai)
        //$tables = $this->core->logic->sort_children_to_parent($tables, 'ID', 'PARENT_ID');

        $tpl_arr = $this->core->getTemplate('table');

        // Nurodomi išvedami lentelės stulpeliai
        $columns = array(
            /* 'TOGGLE' => $this->core->lng('atp_table_toggle'), */
            'LABEL' => $this->core->lng('atp_act_label')
        );
        // Tikrinama teisė į veiksmų stulpelio matymą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_TABLE') > 1) {
            $columns['ACTIONS'] = $this->core->lng('atp_table_actions');
        }

        // table
        $table = array();
        $table['table_class'] = 'search_fields';
        $html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

        // Lentelės antraštė
        $header = array('row_html' => '');
        $header['row_class'] = 'div-row div-header';
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);
        foreach ($columns as $col_name => $col) {
            // cell
            $cell = array('cell_html' => '');
            $cell['cell_class'] = 'div-cell cell-' . $col_name;
            $cell['cell_content'] = $col;
            $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
        }
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

        if (count($tables)) {
            // Lentelės įrašų eilutės
            foreach ($tables as $key => $table) {
                // Eilutės duomenys
                $row = array();
                $row['row_html'] = 'id="atp-table-list-item-id-' . $table['ID'] . '"';
                $row['row_class'][] = 'div-row';
                //if ($table['LEVEL'])
                //	$row['row_class'][] = 'child';
                //$row['row_class'][] = 'level-' . $table['LEVEL'];
                //$row['row_class'][] = 'child-of-' . $table['PARENT_ID'];
                $row['row_class'] = join(' ', $row['row_class']);
                $html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

                // Eilutės stulpeiai
                foreach ($columns as $col_name => $col) {
                    // Stulpelio duomenys
                    $cell = array('cell_html' => '', 'cell_content' => '');
                    $cell['cell_class'] = 'div-cell cell-' . $col_name;
                    $cell['cell_content'] .= $table[$col_name];
                    $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
                }

                $html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
            }
        }

        $html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

        return $html_manage;
    }

    /**
     * Sugeneruoja veikų lentelę
     * @return string
     */
    public function table_deeds()
    {
        $html_manage = '';

        // Surenka klasifikatorius
        $tables = $this->core->logic->get_deeds();

        // Surenkami papildomi duomenys apie įrašus
        foreach ($tables as $table) {
            $new_tables[$table['ID']] = $table;

            // Surenka galimus veiksmus
            $new_tables[$table['ID']]['ACTIONS'] = array();
            $new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="expand_collapse_block">
					<div class="expand_collapse_img_block">
						<img style="cursor: pointer;" src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/atp/expand_doc_action.png" width="29" height="29">
					</div>
					<div class="actions_block" style="display: none;">
						<img style="display: block;" src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/actions/top_img_actions_buttons.png" width="132" height="8">
						<div class="actions_buttons">';
            $new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button addChild"><span>Pridėti vaiką</span></div>';
            $new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button remove"><span>Trinti</span></div>';
            $new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button addFields"><span>Sukurti laukus</span></div>';
            $new_tables[$table['ID']]['ACTIONS'][] = '
						</div>
					</div>
				</div>';
            $new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="action_label">
					<div class="atp-html-button edit"><span>' . $this->core->lng('edit') . '</span></div>
				</div>';
            $new_tables[$table['ID']]['ACTIONS'] = join(' ', $new_tables[$table['ID']]['ACTIONS']);

            // Gaunamas įrašo lygis
            $new_tables[$table['ID']]['LEVEL'] = $this->core->logic->get_table_level($tables, $table['ID']);
            // Nurodomas išskleidimo stulpelis, jei laukas turi vaikų
            if ($this->core->logic->table_has_children($tables, $table['ID'], 'DEEDS')) {
                $new_tables[$table['ID']]['TOGGLE'] = '<div class="collapse atp-table-list-toggle"></div>';
                $toggle_class = 'collapse';
            } else {
                $toggle_class = 'none';
            }

            $new_tables[$table['ID']]['LABEL'] = '<div class="cell-LABEL-container"><div class="' . $toggle_class . ' atp-table-list-toggle"></div><span>' . $new_tables[$table['ID']]['LABEL'] . '</span></div>';
        }
        $tables = &$new_tables;

        // Įrašai surikiuojami pagal hierarchiją (tevas -> vaikai, tevas -> vaikai)
        $tables = $this->core->logic->sort_children_to_parent($tables, 'ID', 'PARENT_ID');

        $tpl_arr = $this->core->getTemplate('table2');

        // Nurodomi išvedami lentelės stulpeliai
        $columns = array(
            /* 'TOGGLE' => $this->core->lng('atp_table_toggle'), */
            'LABEL' => $this->core->lng('atp_deed_label')
        );
        // Tikrinama teisė į veiksmų stulpelio matymą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_DEED') > 1) {
            $columns['ACTIONS'] = $this->core->lng('atp_table_actions');
        }

        // table
        $table = array();
        $table['table_class'] = 'deeds';
        $html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

        // Lentelės antraštė
        $header = array('row_html' => '');
        $header['row_class'] = 'div-row div-header';
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);
        foreach ($columns as $col_name => $col) {
            // cell
            $cell = array('cell_html' => '');
            $cell['cell_class'] = 'div-cell cell-' . $col_name;
            $cell['cell_content'] = $col;
            $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
        }
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

        // Lentelės įrašų eilutės
        foreach ($tables as $key => $table) {
            // Eilutės duomenys
            $row = array();
            $row['row_html'] = 'id="atp-table-list-item-id-' . $table['ID'] . '"';
            $row['row_class'][] = 'div-row';
            if ($table['LEVEL']) {
                $row['row_class'][] = 'child';
            }
            $row['row_class'][] = 'level-' . $table['LEVEL'];
            $row['row_class'][] = 'child-of-' . $table['PARENT_ID'];
            $row['row_class'] = join(' ', $row['row_class']);
            $html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

            // Eilutės stulpeiai
            foreach ($columns as $col_name => $col) {
                // Stulpelio duomenys
                $cell = array('cell_html' => '', 'cell_content' => '');
                if (in_array($col_name, array('VALID_FROM', 'VALID_TILL'))) {
                    $table[$col_name] = date('Y-m-d', strtotime($table[$col_name]));
                }
                $cell['cell_class'] = 'div-cell cell-' . $col_name;
                $cell['cell_content'] .= $table[$col_name];
                $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
            }

            $html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
        }

        $html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

        return $html_manage;
    }

    /**
     * Sugeneruoja klasifikatorių lentelę
     * @return string
     */
    public function table_classifications($parentId = null, $parentLevel = -1)
    {
        $html_manage = '';

        // Surenka klasifikatorius
        $entityManager = $this->core->factory->Doctrine();
        /* @var $repository \Atp\Repository\ClassificatorRepository */
        $repository = $entityManager->getRepository('\Atp\Entity\Classificator');
        $node = null;
        if (empty($parentId) === false) {
            $node = $entityManager->getReference('\Atp\Entity\Classificator', $parentId);
        }
        $tables = $repository->getChildren($node, true);

        // Surenkami papildomi duomenys apie įrašus
        $new_tables = array();
        foreach ($tables as $table) {
            $id = $table->getId();
            $new_tables[$id] = [
                'ID' => $table->getId(),
                'LABEL' => $table->getLabel()
            ];
            // Surenka galimus veiksmus
            $new_tables[$id]['ACTIONS'] = array();
            // Tikrina ar vartotojas turi teisę matyti mygtukus
            if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_CLASSIFICATION') > 1) {
                $new_tables[$id]['ACTIONS'][] = '<div class="atp-html-button edit"><span>' . $this->core->lng('edit') . '</span></div>';
                $new_tables[$id]['ACTIONS'][] = '<div class="atp-html-button addChild"><span>Pridėti vaiką</span></div>';
                $new_tables[$id]['ACTIONS'][] = '<div class="atp-html-button remove"><span>Trinti</span></div>';
                $new_tables[$id]['ACTIONS'][] = '<div class="atp-html-button addFields"><span>Sukurti laukus</span></div>';
            }
            $new_tables[$id]['ACTIONS'] = join(' ', $new_tables[$id]['ACTIONS']);

            // Gaunamas įrašo lygis
            $new_tables[$id]['LEVEL'] = $parentLevel + 1;

            $childrenCount = $repository->getChildrenQueryBuilder($entityManager->getReference('\\Atp\\Entity\\Classificator', $id), true)
                ->select('COUNT(node)')
                ->getQuery()
                ->getSingleScalarResult();
            // Nurodomas išskleidimo stulpelis, jei laukas turi vaikų
            if ($childrenCount) {
                $new_tables[$id]['TOGGLE'] = '<div class="collapse atp-table-list-toggle"></div>';
            }
        }
        $tables = &$new_tables;

        $tpl_arr = $this->core->getTemplate('table');

        // Nurodomi išvedami lentelės stulpeliai
        $columns = array(
            'TOGGLE' => $this->core->lng('atp_table_toggle'),
            'LABEL' => $this->core->lng('atp_classificator_label')
        );
        // Tikrinama teisė į veiksmų stulpelio matymą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_CLASSIFICATION') > 1) {
            $columns['ACTIONS'] = $this->core->lng('atp_table_actions');
        }

        // table
        $table = array();
        $table['table_class'] = 'classifications';
        $html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

        // Lentelės antraštė
        $header = array('row_html' => '');
        $header['row_class'] = 'table-row table-header';
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);
        foreach ($columns as $col_name => $col) {
            // cell
            $cell = array('cell_html' => '');
            $cell['cell_class'] = 'table-cell cell-' . $col_name;
            $cell['cell_content'] = $col;
            $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
        }
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

        // Lentelės įrašų eilutės
        foreach ($tables as $key => $table) {
            // Eilutės duomenys
            $row = array();
            $row['row_html'] = 'id="atp-table-list-item-id-' . $table['ID'] . '"';
            $row['row_class'][] = 'table-row';
            if ($table['LEVEL']) {
                $row['row_class'][] = 'child';
            }
            $row['row_class'][] = 'level-' . $table['LEVEL'];
            if (empty($parentId) === false) {
                $row['row_class'][] = 'child-of-' . $parentId;
            }
            $row['row_class'] = join(' ', $row['row_class']);
            $html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

            // Eilutės stulpeiai
            foreach ($columns as $col_name => $col) {
                // Stulpelio duomenys
                $cell = array('cell_html' => '', 'cell_content' => '');
                if ($col_name === 'LABEL' && $table['LEVEL'] > 0) {
                    $cell['cell_content'] .= '<div class="level-img level-' . $table['LEVEL'] . '"><img src="' . $this->core->config['GLOBAL_SITE_URL'] . 'styles/new/images/dok_rodykle_extended_left.png" alt="">' .
                        str_repeat('<img src="' . $this->core->config['GLOBAL_SITE_URL'] . 'styles/new/images/dok_rodykle_extended_middle.png" alt="">', $table['LEVEL']) .
                        '<img src="' . $this->core->config['GLOBAL_SITE_URL'] . 'styles/new/images/dok_rodykle_extended_right.png" alt=""></div>';
                }
                $cell['cell_class'] = 'table-cell cell-' . $col_name;
                $cell['cell_content'] .= $table[$col_name];
                $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
            }

            $html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
        }

        $html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

        return $html_manage;
    }

    /**
     * Klasifikatorių puslapis
     * @return string
     */
    public function classifications()
    {
        // Tikrina ar vartotojas turi teisę matyti puslapį
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_CLASSIFICATION')) {
            return $this->core->printForbidden();
        }

        $tmpl = $this->core->factory->Template();
        $tmpl_handler = 'classifications_file';
        $tmpl->set_file($tmpl_handler, 'classifications.tpl');

        $new_block = $tmpl_handler . 'new';
        $tmpl->set_block($tmpl_handler, 'new', $new_block);

        // Naujo klasifikatoriaus kūrimo bloko išvedimas
        // Tikrina ar vartotojas turi teisę kurti naują klasifikatorių
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_CLASSIFICATION') > 1) {
            $tmpl->parse($new_block, 'new');
        } else {
            $tmpl->clean($new_block);
        }

        // Klasifikatorių lentelė
        $tmpl->set_var('classifications_table', $this->table_classifications());


        $tmpl->set_var(array(
            'GLOBAL_SITE_URL' => $this->core->config['GLOBAL_SITE_URL'],
            'nr' => (empty($_POST['nr']) ? uniqid() : $_POST['nr'])
        ));
        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    /**
     * Teisės aktų punktai (ex. Teisės aktai)
     * @return string
     */
    public function acts()
    {
        // Tikrina ar vartotojas turi teisę matyti puslapį
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_ACT')) {
            return $this->core->printForbidden();
        }

//$this->core->_load_files['js'][] = 'jquery.form.min.js';
        $this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
        $this->core->_load_files['js'][] = 'jquery.validationEngine.js';
        $this->core->_load_files['css'][] = 'validationEngine.jquery.css';

        $tmpl = $this->core->factory->Template();
        $tmpl_handler = 'acts_file';
        $tmpl->set_file($tmpl_handler, 'acts.tpl');

        $new_block = $tmpl_handler . 'new';
        $tmpl->set_block($tmpl_handler, 'new', $new_block);

        // Naujo teisės akto kūrimo bloko išvedimas
        // Tikrina ar vartotojas turi teisę kurti naują teisės aktą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_ACT') > 1) {
            $tmpl->parse($new_block, 'new');
        } else {
            $tmpl->clean($new_block);
        }

        $tmpl->set_var(array(
            'sys_message' => $this->core->get_messages(),
            'acts_table' => $this->table_acts()
        ));


        $tmpl->set_var('GLOBAL_SITE_URL', $this->core->config['GLOBAL_SITE_URL']);
        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    /**
     * Dokumentų būsenos
     * @return string
     */
    public function status()
    {
        // Tikrina ar vartotojas turi teisę matyti puslapį
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_STATUS')) {
            return $this->core->printForbidden();
        }

//$this->core->_load_files['js'][] = 'jquery.form.min.js';
        $this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
        $this->core->_load_files['js'][] = 'jquery.validationEngine.js';
        $this->core->_load_files['css'][] = 'validationEngine.jquery.css';

        $tmpl = $this->core->factory->Template();
        $tmpl_handler = 'status_file';
        $tmpl->set_file($tmpl_handler, 'status.tpl');

        $new_block = $tmpl_handler . 'new';
        $tmpl->set_block($tmpl_handler, 'new', $new_block);

        // Naujo teisės akto kūrimo bloko išvedimas
        // Tikrina ar vartotojas turi teisę kurti naują teisės aktą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_STATUS') > 1) {
            $tmpl->parse($new_block, 'new');
        } else {
            $tmpl->clean($new_block);
        }

        $tmpl->set_var(array(
            'sys_message' => $this->core->get_messages(),
            'table' => $this->table_status_relations()
        ));

        $tmpl->set_var('GLOBAL_SITE_URL', $this->core->config['GLOBAL_SITE_URL']);

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    function status_list()
    {
        // Tikrina ar vartotojas turi teisę matyti puslapį
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_STATUS')) {
            return $this->core->printForbidden();
        }

//$this->core->_load_files['js'][] = 'jquery.form.min.js';
        $this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
        $this->core->_load_files['js'][] = 'jquery.validationEngine.js';
        $this->core->_load_files['css'][] = 'validationEngine.jquery.css';

        $tmpl = $this->core->factory->Template();
        $tmpl_handler = 'status_list_file';
        $tmpl->set_file($tmpl_handler, 'status_list.tpl');

        $new_block = $tmpl_handler . 'new';
        $tmpl->set_block($tmpl_handler, 'new', $new_block);

        // Naujo teisės akto kūrimo bloko išvedimas
        // Tikrina ar vartotojas turi teisę kurti naują teisės aktą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_STATUS') > 1)
            $tmpl->parse($new_block, 'new');
        else {
            $tmpl->clean($new_block);
        }

        $tmpl->set_var(array(
            'sys_message' => $this->core->get_messages(),
            'table' => $this->table_status_list()
        ));

        $tmpl->set_var('GLOBAL_SITE_URL', $this->core->config['GLOBAL_SITE_URL']);

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    /**
     * Dokumento būsenos redagavimo forma
     * @author Justinas Malūkas
     * @return string
     */
    public function status_editstatus()
    {
        $tmpl = $this->core->factory->Template();
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_STATUS')) {
            return $this->core->printForbidden();
        }

        if ($this->core->content_type !== 2) {
            $this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
            $this->core->_load_files['js'][] = 'jquery.validationEngine.js';
            $this->core->_load_files['css'][] = 'validationEngine.jquery.css';
        }

        $tmpl_handler = 'status_editstatus_file';
        $tmpl->set_file($tmpl_handler, 'status_editstatus.tpl');

        // Formos blokas
        $form_block = $tmpl_handler . 'form';
        $tmpl->set_block($tmpl_handler, 'form', $form_block);

        $vars = array();
        $m = (int) $this->core->extractVariable('m');

        $id = (int) $this->core->extractVariable('id');
        //$parent_id = (int) $this->core->extractVariable('parent');

        $vars['sys_message'] = $this->core->get_messages();
        $vars['save'] = 'Išsaugoti';

        $data = array();
        if (empty($id) === false) {
            $data = $this->core->logic2->get_status(array('ID' => $id));
        }

        //
        if (empty($data) === true) {
            //$vars['url'] = 'index.php?m=' . $m . '&parent=' . $parent_id . '&action=save';
            $vars['url'] = 'index.php?m=' . $m . '&parent=0&action=savestatus';
            $vars['title'] = 'Nauja būsena';
            $vars['label'] = '';
        } else {
            $vars['url'] = 'index.php?m=' . $m . '&id=' . $id . '&action=savestatus';
            $vars['title'] = 'Būsena: ' . $data['LABEL'];
            $vars['label'] = htmlspecialchars($data['LABEL']);


            $adminJournal = $this->core->factory->AdminJournal();
            $adminJournal->addRecord(
                $adminJournal->getVisitRecord(
                    'Dokumento būsenos "' . $data['LABEL'] . '" redagavimo formą.'
                )
            );
        }

        $tmpl->set_var($vars);

        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_STATUS') > 1) {
            $tmpl->parse($form_block, 'form');
        } else {
            $tmpl->clean($form_block);
        }

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    public function status_editrelation()
    {
        $tmpl = $this->core->factory->Template();
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_STATUS')) {
            return $this->core->printForbidden();
        }

        if ($this->core->content_type !== 2) {
            $this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
            $this->core->_load_files['js'][] = 'jquery.validationEngine.js';
            $this->core->_load_files['css'][] = 'validationEngine.jquery.css';
        }

        $tmpl_handler = 'status_editrelation_file';
        $tmpl->set_file($tmpl_handler, 'status_editrelation.tpl');

        // Ryšio laukas
        $relation_block = $tmpl_handler . 'relation';
        $tmpl->set_block($tmpl_handler, 'relation', $relation_block);

        // Dokumento pasirinkimas
        $document_option_block = $tmpl_handler . 'document_option';
        $tmpl->set_block($tmpl_handler, 'document_option', $document_option_block);
        // Dokumento laukas
        $document_block = $tmpl_handler . 'document';
        $tmpl->set_block($tmpl_handler, 'document', $document_block);

        // ATP būsena
        $atp_status_option_block = $tmpl_handler . 'atp_status_option';
        $tmpl->set_block($tmpl_handler, 'atp_status_option', $atp_status_option_block);
        // Būsena
        $status_option_block = $tmpl_handler . 'status_option';
        $tmpl->set_block($tmpl_handler, 'status_option', $status_option_block);
        // Naujo ryšio kūrimas
        $relation_new_block = $tmpl_handler . 'relation_new';
        $tmpl->set_block($tmpl_handler, 'relation_new', $relation_new_block);

        // Formos blokas
        $form_block = $tmpl_handler . 'form';
        $tmpl->set_block($tmpl_handler, 'form', $form_block);


        $vars = array();
        $m = (int) $this->core->extractVariable('m');
        $id = (int) $this->core->extractVariable('id');


        $vars['sys_message'] = $this->core->get_messages();
        $vars['save'] = 'Išsaugoti';


        // Surenka dumenis apie ryšius
        $data = array();
        if (empty($id) === false) {
            $data = $this->core->logic2->get_status_relations(array('DOCUMENT_ID' => $id));
        }

        if (empty($data) === true) {
            $vars['title'] = 'Naujas ryšys';
            $vars['url'] = 'index.php?m=' . $m . '&action=saverelation';

            // Dokumentai
            $documents = $this->core->logic2->get_document(null, false, 'PARENT_ID, LABEL');
            $documents = $this->core->logic->sort_children_to_parent($documents, 'ID', 'PARENT_ID');

            $tmpl->set_var(array(
                'value' => null,
                'label' => $this->core->lng('not_chosen')
            ));
            $tmpl->parse($document_option_block, 'document_option');
            foreach ($documents as &$document) {
                $tmpl->set_var(array(
                    'value' => $document['ID'],
                    'label' => str_repeat('&nbsp;', $document['depth'] * 3) . $document['LABEL']
                ));
                $tmpl->parse($document_option_block, 'document_option', true);
            }
            $tmpl->parse($document_block, 'document');
        } else {
            $document = $this->core->logic2->get_document(array('ID' => $id));
            $vars['title'] = 'Ryšys dokumentui: ' . $document['LABEL'];
            $vars['url'] = 'index.php?m=' . $m . '&id=' . $id . '&action=saverelation';


            $adminJournal = $this->core->factory->AdminJournal();
            $adminJournal->addRecord(
                $adminJournal->getVisitRecord(
                    'Dokumento "' . $document['LABEL'] . '" būsenos ryšių redagavimą.'
                )
            );

            $tmpl->clean($document_block);
        }

        /*
         * ATP būsenos pasirinkimas
         */
        $tmpl->set_var(array(
            'value' => 0,
            'label' => $this->core->lng('not_chosen')
        ));
        $tmpl->parse($atp_status_option_block, 'atp_status_option');
        $atp_status = $this->core->logic->get_document_statuses();

        $atp_status_sorted = $atp_status;
        usort($atp_status_sorted, function($a, $b) {
            return strcmp($a['LABEL'], $b['LABEL']);
        });

        foreach ($atp_status_sorted as &$status) {
            $tmpl->set_var(array(
                'value' => $status['ID'],
                'label' => $status['LABEL']
            ));
            $tmpl->parse($atp_status_option_block, 'atp_status_option', true);
        }


        /*
         * Būsenos pasirinkimas
         */
        $tmpl->set_var(array(
            'value' => 0,
            'label' => $this->core->lng('not_chosen')
        ));
        $tmpl->parse($status_option_block, 'status_option');
        $statuses = $this->core->logic2->get_status(null, null, 'LABEL ASC');
        foreach ($statuses as &$status) {
            $tmpl->set_var(array(
                'value' => $status['ID'],
                'label' => $status['LABEL']
            ));
            $tmpl->parse($status_option_block, 'status_option', true);
        }

        $tmpl->parse($relation_new_block, 'relation_new');


        // Dokumentų, klasifikatorių, webservisų informacija
        $items_names['DOC'] = $this->core->logic2->get_document(null, false, null, 'ID, LABEL, PARENT_ID');
        $items_names['CLASS'] = $this->core->logic2->get_classificator(null, false, null, 'ID, LABEL, PARENT_ID');

        /*
         * Priskirtų laukų išvedimas
         */
        $tmpl->set_var(array(
            'class' => ' sample',
            'realtion_str' => '',
            'atp_status' => '',
            'status' => ''
        ));
        $tmpl->parse($relation_block, 'relation');
        if (empty($data) === false) {
            foreach ($data as &$arr) {
                $arr['ATP_STATUS_NAME'] = $atp_status[$arr['ATP_STATUS_ID']]['LABEL'];
                $arr['STATUS_NAME'] = $statuses[$arr['STATUS_ID']]['LABEL'];
            }

            usort($data, function($a, $b) {
                $res = strcmp($a['ATP_STATUS_NAME'], $b['ATP_STATUS_NAME']);

                if ($res == 0) {
                    return strcmp($a['STATUS_NAME'], $b['STATUS_NAME']);
                } else {
                    return $res;
                }
            });

            foreach ($data as &$arr) {
                $tmpl->set_var(array(
                    'class' => '',
                    'realtion_str' => $arr['ATP_STATUS_ID'] . '-' . $arr['STATUS_ID'],
                    'atp_status' => $arr['ATP_STATUS_NAME'],
                    'status' => $arr['STATUS_NAME']
                ));
                $tmpl->parse($relation_block, 'relation', true);
            }
        }

        $tmpl->set_var($vars);

        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_STATUS') > 1) {
            $tmpl->parse($form_block, 'form');
        } else {
            $tmpl->clean($form_block);
        }

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    /**
     * Veikos
     * @return string
     */
    public function deeds()
    {
        // Tikrina ar vartotojas turi teisę matyti puslapį
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_DEED')) {
            return $this->core->printForbidden();
        }

//$this->core->_load_files['js'][] = 'jquery.form.min.js';
        $this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
        $this->core->_load_files['js'][] = 'jquery.validationEngine.js';
        $this->core->_load_files['css'][] = 'validationEngine.jquery.css';


        $tmpl = $this->core->factory->Template();
        $tmpl_handler = 'deeds_file';
        $tmpl->set_file($tmpl_handler, 'deeds.tpl');

        $new_block = $tmpl_handler . 'new';
        $tmpl->set_block($tmpl_handler, 'new', $new_block);

        // Naujo teisės akto kūrimo bloko išvedimas
        // Tikrina ar vartotojas turi teisę kurti naują teisės aktą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_DEED') > 1)
            $tmpl->parse($new_block, 'new');
        else {
            $tmpl->clean($new_block);
        }
        $tmpl->set_var(array(
            'sys_message' => $this->core->get_messages(),
            'deeds_table' => $this->table_deeds()
        ));

        $tmpl->set_var('GLOBAL_SITE_URL', $this->core->config['GLOBAL_SITE_URL']);

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    /**
     * Dokumentas (Dokumentų sąrašas)
     * @return string HTML'as
     */
    public function table_records()
    {
        $this->core->logic->_r_table_id = (int) $_GET['id'];
        $privileges_data = array(
            'user_id' => $this->core->factory->User()->getLoggedUser()->person->id,
            'department_id' => $this->core->factory->User()->getLoggedPersonDuty()->department->id,
            'table_id' => $this->core->logic->_r_table_id
        );
        if ($this->core->logic->privileges('table_records', $privileges_data) === false) {
            return $this->core->printForbidden();
        }

        $document = $this->core->logic2->get_document($this->core->logic->_r_table_id);

		$tmpl = $this->core->factory->Template();
		$tmpl_handler = 'table_records_file';
        $tmpl->set_file($tmpl_handler, 'table_records.tpl');

        $button_new = $tmpl_handler . 'button_new';
		$tmpl->set_block($tmpl_handler, 'button_new', $button_new);
        $tmpl->set_var(array(
            'title' => $document['LABEL'],
            'sys_message' => $this->core->get_messages(),
            'action_create_new' => 'index.php?m=5&table_id=' . $this->core->logic->_r_table_id,
            'table_record' => $this->table_record($document),
            'GLOBAL_SITE_URL' => $this->core->config['GLOBAL_SITE_URL']
        ));
		if ($this->core->logic->privileges('table_records_button_new', $privileges_data) === true)
            $tmpl->parse($button_new, 'button_new');
        else {
            $tmpl->clean($button_new);
        }


        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');
		return $this->core->atp_content($output);
    }

    /**
     * Mokėjimų atnaujinimas (Dokumentų sąrašas)
     * @todo Mygtukas "Tikrinti su VMI" pašalintas, tikrinimo su VMI nebereikia, nes tikrinama iš ATPEIR.
     * @param int $table_id Dokumento ID
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @return string HTML'as
     */
    function record_payments_form($table_id, $record)
    {
        $record = $this->core->logic2->get_record_relation($record);
        if (empty($record) === true) {
            trigger_error('Record not found', E_USER_ERROR);
            exit;
        }

        $privileges_data = array(
            'user_id' => $this->core->factory->User()->getLoggedUser()->person->id,
            'department_id' => $this->core->factory->User()->getLoggedPersonDuty()->department->id,
            'record_id' => $record['ID'],
            'table_id' => $record['TABLE_TO_ID']
        );
        if ($this->core->logic->privileges('table_record_update', $privileges_data) === false) {
            return $this->core->printForbidden();
        }

        if ($_GET['action'] === 'submit') {
            $result = $this->core->logic->record_payments_save();

            if ($result) {

                $adminJournal = $this->core->factory->AdminJournal();
                $adminJournal->addRecord(
                    $adminJournal->getSaveRecord(
                        'Dokumento įrašo mokėjimai'
                        . ' (Dokumento įrašas "' . $record['RECORD_ID'] . '").'
                    )
                );


                $this->core->set_message('success', $this->core->lng('saved'));
            }

            header('Location: index.php?m=4&id=' . $record['TABLE_TO_ID']);
            exit;
        }


        $adminJournal = $this->core->factory->AdminJournal();
        $adminJournal->addRecord(
            $adminJournal->getVisitRecord(
                'Dokumento įrašo mokėjimai'
                . ' (Dokumento įrašas "' . $record['RECORD_ID'] . '").'
            )
        );


        $record_table = $this->core->logic2->get_record_table($record);
        $document = $this->core->logic2->get_document($record['TABLE_TO_ID']);
        $doc_structure = $this->core->logic2->get_fields_all(array('ITEM_ID' => $document['ID'],
            'ITYPE' => 'DOC'));


        $tmpl = $this->core->factory->Template();
        $tmpl->handler = 'table_record_payments_file';
        $tmpl->set_file($tmpl->handler, 'table_record_payments.tpl');

        $handler = $tmpl->set_multiblock($tmpl->handler, array(
            array(
                'status', // Dokumento baudos būsena
                'penalty', // Baudos suma
                array(
                    'extra', // Mokėjimo lauko papildoma informacija
                    'payment_field' // Mokėjimo informacijos laukas
                ),
                'payment'// Mokėjimo informacija
            ),
            'form' // Mokėjimų forma
        ));


        // Dokumento įrašo mokėjimai
        $payments = $this->core->logic2->get_payments(array('DOCUMENT_ID' => $document['ID'],
            'RECORD_ID' => $record['ID']));
        $payment_values = array_map(function($arr) {
            return (int) $arr['PAID'];
        }, $payments);
        $array_keys = array_map(function($arr) {
            return (int) $arr['PAYMENT_ID'];
        }, $payments);
        $payments = array_combine($array_keys, $payment_values);

        // Gauna nustatytos baudos sumą
        $penalty_sum = null;
        $penaltySumField = $this->core->factory->Document()->getOptions($document['ID'])->penalty_sum;
        if (empty($doc_structure[$penaltySumField]) === false) {
            $penalty_sum = $this->core->factory->Record()->getFieldValue($record['ID'], $penaltySumField);
        }

        $penalty_data = array(
            'paid' => 0,
            'sum' => 0,
            'currency' => 'Lt'
        );
        // Priskirtų mokėjimų suma
        if (empty($payments) === false) {
            $penalty_data['paid'] = array_sum($payments);
        }
        // Baudos informacija
        if (empty($penalty_sum) === false) {
            if (preg_match('/([0-9]+)/', $penalty_sum, $matches) === 1) {
                $penalty_data['sum'] = (int) $matches[0];
                $penalty_data['currency'] = 'Lt'; // todo: baudos valiuta?
            }
            $tmpl->set_var('penalty_sum', $penalty_sum);
            $tmpl->parse($handler['penalty'], 'penalty', false);
        } else {
            $tmpl->clean($handler['penalty']);
        }

        $tmpl->set_var(array(
            'payments' => json_encode($payments), // todo: kaip dėl baudos mokėjimo valiutos?
            'penalty_data' => json_encode($penalty_data)
        ));


        // Iš VMI registro gauna atliktų mokėjimų informaciją
        $data = array('registry' => 'vmi');
        foreach (array('SEARCH_CODE' => $this->core->factory->Document()->getOptions($document['ID'])->person_code,
        'SEARCH_FROM' => $this->core->factory->Document()->getOptions($document['ID'])->fill_date) as $key => $field) {
            if (empty($field) === true)
                continue;

            if (isset($doc_structure[$field])) {
                if (isset($record_table[$doc_structure[$field]['NAME']])) {
                    $data[$key] = $record_table[$doc_structure[$field]['NAME']];
                }
            }
        }

        //$data = array('registry' => 'vmi', 'SEARCH_CODE' => 38512110322, 'SEARCH_FROM' => '2013-01-01 01:00:00');
        if (empty($data['SEARCH_CODE']) === true) {
            throw new \Exception('Nepateiktas pažeidėjo asmens kodas');
        }

        $found_payments = $this->core->getRDBWsData('getVMISearchResult', $data);
        $fp_count = count($found_payments);


        // Gauna sistemoje priskirtas apmokėtų mokėjimų sumas
        $found_payments_ids = array();
        for ($i = 0; $i < $fp_count; $i++) {
            $found_payments_ids[] = $found_payments[$i]['ID'];
        }
        $paid = array();
        if (count($found_payments_ids)) {
            $get_paid_payments = $this->core->logic2->get_payments(array(0 => 'PAYMENT_ID IN(' . join(',', $found_payments_ids) . ')'));
            foreach ($get_paid_payments as &$paid_payment) {
                $paid[$paid_payment['PAYMENT_ID']] += $paid_payment['PAID'];
            }
        }


        // Formoje nereikalingi laukai
        $exclude = array(
            'ID',
            'DOK_NUMERIS',
            'DOK_DATA',
            'SAVIVALDYBES_ID',
            'OP_TIPAS',
            'SUMA_BV',
            'VALIUTA',
            'IRS_UPD_DATA',
            'IRS_INS_DATA',
        );

        // Generuoja mokėjimo informacijos bloką
        for ($i = 0; $i < $fp_count; $i++) {
            $j = 0;
            // Generuoja mokėjimo informacijos lauko bloką
            foreach ($found_payments[$i] as $key => &$field) {
                if (in_array($key, $exclude, true) === true)
                    continue;

                $tmpl->set_var(array(
                    'class' => $key,
                    'label' => $this->core->lng('wmi_field_' . $key) . ':',
                    'value' => htmlspecialchars($field)
                ));

                // Papildoma informacija
                if ($key === 'SUMA') {
                    $tmpl->set_var('extra', '<span class="paid" default="' . $paid[$found_payments[$i]['ID']] . '">&nbsp;(-' . $paid[$found_payments[$i]['ID']] . ')</span><div class="max" style="display:none">' . $field . '</div>');
                    $tmpl->parse($handler['extra'], 'extra');
                } else {
                    $tmpl->set_var($handler['extra'], '');
                }

                $tmpl->parse($handler['payment_field'], 'payment_field', $j);
                $j++;
            }
            $tmpl->set_var(array(
                'id' => $found_payments[$i]['ID'],
                'dvalue' => isset($payments[$found_payments[$i]['ID']]) ? $payments[$found_payments[$i]['ID']] : '0',
                'value' => isset($payments[$found_payments[$i]['ID']]) ? $payments[$found_payments[$i]['ID']] : '',
                'disabled' => isset($payments[$found_payments[$i]['ID']]) ? '' : ' disabled="disabled"',
                'checked' => isset($payments[$found_payments[$i]['ID']]) ? ' checked="checked"' : ''
            ));

            $tmpl->parse($handler['payment'], 'payment', $i);
        }

        $tmpl->set_var(array(
            'url' => 'index.php?m=17&action=submit&id=' . $record['ID'],
            'save' => $this->core->lng('save')
        ));


        // Dokumento mokėjimo būsena
        $paymentManager = new \Atp\Document\Record\Payment($this->core);
        $options = $paymentManager->getPaymentStatusesLabels();

        //if(empty($payments) === true)  {
        //	$selected_option = $paymentManager->getPaymentStatus($paymentManager::STATUS_NULL);
        //} else {
        $selected_option = $paymentManager->getRecordPaymentOption(new \Atp\Document\Record($record['ID'], $this->core));
        //}
        array_walk($options, function(&$value, $key, $selected_key) {
            $value = '<option value="' . $key . '"' . ((int) $selected_key === (int) $key ? ' selected="selected"' : '') . '>' . $value . '</option>';
        }, (int) $selected_option['ID']);
        $tmpl->set_var(array(
            'options' => join('', $options)
        ));
        $tmpl->parse($handler['status'], 'status');


        $tmpl->parse($handler['form'], 'form');

        $tmpl->set_var(array(
            'title' => 'Dokumento "' . $record['RECORD_ID'] . '" mokėjimai',
            'sys_message' => $this->core->get_messages()
        ));

        $tmpl->parse($tmpl->handler . '_out', $tmpl->handler);
        $output = $tmpl->get($tmpl->handler . '_out');

        return $this->core->atp_content($output);
    }

    /**
     * Dokumento įrašų sąrašas
     * @param array $document Dokumento duomenys
     * @return null
     */
    public function table_record($document)
    {
        if (empty($document['ID']) === true) {
            trigger_error('Table ID is empty', E_USER_WARNING);
            return null;
        }

        $html = '';

        $workerManager = new \Atp\Record\Worker($this->core);


        // Puslapiavimas
        include_once($this->core->config['REAL_URL'] . 'helper/paginator.php');
        $pg = new \Paginator();
        $pg->current_page = empty($_GET['_page']) ? 1 : $_GET['_page'];
        $pg->items_per_page = 20;
        $documentOptions = [
            'WORKER_ID' => $this->core->factory->User()->getLoggedPersonDuty()->id,
            'DOCUMENT_ID' => $document['ID']
        ];

        if($document['ID'] == \Atp\Document::DOCUMENT_INFRACTION_MINI) {
            $pg->items_per_page = 5;
            $documentOptions['INTERVAL'] = 'A.CREATE_DATE BETWEEN "' . date('Y-m-d') . ' 00:00:00" AND "' . date('Y-m-d') . ' 23:59:59"';
        }
        // Viso įrašų
        $count = $workerManager->getWorkerRecords($documentOptions, true, $_GET);
        $pg->items_total = $count;
        $pg->paginate();

        // Pagal puslapiavimą apribojamas duomenų kiekis. Skaičiuojama nuo paskutinio įrašo
        $start = $pg->items_total - $pg->limit_end;
        $limit = $pg->items_per_page;
        if ($start < 0) {
            $limit = $limit + $start;
            $start = 0;
        }

        $documentOptions['START'] = $start;
        $documentOptions['OFFSET'] = $limit;

        // Puslapio dokumentai
		$records = $workerManager->getWorkerRecords($documentOptions, false, $_GET);
		$terms = $this->core->logic->get_records_terms_by_table_id($document['ID'], $document);

		// get all ROIK for Record IDs
		$roik_arr = array();

		$roik_qur = str_replace('()', '(0)', "SELECT DISTINCT ROIK, RECORD_ID FROM ".PREFIX."ATP_ATPR_ATP WHERE RECORD_ID IN (".implode(',',$records).") ");
		$roik_col = $this->core->db_query_fast($roik_qur);
		while($roik_res = $this->core->db_next($roik_col)) {
			$roik_arr[$roik_res['RECORD_ID']] = $roik_res['ROIK'];
		}
        $privileges_data = array(
            'user_id' => $this->core->factory->User()->getLoggedUser()->person->id,
            'department_id' => $this->core->factory->User()->getLoggedPersonDuty()->department->id,
            'table_id' => $document['ID']
        );


        //$i = -1;
        $new_records = array();

        //$records = array_reverse($records);
		foreach ($records as $record) {
            $record = $this->core->logic2->get_record_relation($record);
			if (empty($record) === true)
                continue;

            $record = current($this->core->logic->get_table_records(array('INCLUDE_OWNER' => false,
                    'RECORD_NR' => $record['ID'])));

            $new_records[$record['ID']] = $record;
            $temp = &$new_records[$record['ID']];

            $privileges_data['record_id'] = $record['ID'];

            //$temp['TERM'] = $this->core->logic->get_record_term($record['RECORD_ID'], $document);
            $temp['TERM'] = $terms[$record['RECORD_ID']];
            $temp['ACTIONS'] = array();
            $temp['ACTIONS'][] = '<div class="expand_collapse_block"><div class="expand_collapse_img_block"><img style="cursor: pointer;" src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/atp/expand_doc_action.png" width="29" height="29"></div><div class="actions_block" style="display: none;"><img style="display: block;" src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/actions/top_img_actions_buttons.png" width="132" height="8"><div class="actions_buttons">';

            if ($this->core->logic->privileges('table_record_button_view', $privileges_data) === true)
                $temp['ACTIONS'][] = '<a alt="Peržiūrėti" title="Peržiūrėti" id="atp-record-' . $record['RECORD_ID'] . '" class="atp-record-edit atp-html-button edit" href="index.php?m=5&id=' . $record['RECORD_ID'] . '">Peržiūrėti</a>';

            if ($this->core->logic->privileges('table_record_button_update', $privileges_data) === true)
                $temp['ACTIONS'][] = '<a alt="Atnaujinti" title="Atnaujinti" class="atp-html-button update dialog" href="index.php?m=17&id=' . $record['RECORD_ID'] . '">Atnaujinti</a>';

            $temp['ACTIONS'][] = '<a alt="Kelias" title="Kelias" class="atp-html-button road" href="index.php?m=6&table_id=' . $document['ID'] . '&record_id=' . $record['RECORD_ID'] . '">Kelias</a>';

            if ($this->core->logic->privileges('table_record_button_delete', $privileges_data) === true)
                $temp['ACTIONS'][] = '<a alt="Trinti" title="Trinti" class="atp-html-button remove" href="index.php?m=98&table_id=' . $document['ID'] . '&record_id=' . $record['RECORD_ID'] . '&delete=1">Trinti</a>';

            $temp['ACTIONS'][] = '</div></div></div>';
            /*
              // Dokumento įrašo būklė
              $status = $this->core->logic->find_document_status(array('ID' => $record['STATUS']));
              $status2 = $this->core->logic2->get_status(array('ID' => $record['DOC_STATUS']));
              if (empty($status2)) {
              $status_name = '----';
              } else {
              $status_name = $status2['LABEL'];
              }
              $temp['ACTIONS'][] = '<div class="status_label"><span class="document_status-' . $status['ID'] . ' document_status-' . $status['SHORT'] . '">' . $status_name . '</span></div>'; */


            // Paskutinio dokumento įrašo kelyje būklė
            // Gaunamas RECORD_RELATIONS.ID
            $qry = 'SELECT B.ID
				FROM
					' . PREFIX . 'ATP_RECORD_X_RELATION A
						INNER JOIN ' . PREFIX . 'ATP_RECORD B ON B.ID = A.RELATION_ID
				WHERE
					A.RECORD_ID = :RECORD_ID AND
					A.DOCUMENT_ID = :DOCUMENT_ID
				LIMIT 1';
            $result = $this->core->db_query_fast($qry, array(
                'RECORD_ID' => $record['ID'],
                'DOCUMENT_ID' => $document['ID']
            ));
			$record_relation = $this->core->db_next($result);

            $last_record = $this->core->logic->record_path_last($record_relation['ID']);
            $last_status = $this->core->logic->find_document_status(array('ID' => $last_record['STATUS']));
            $last_status2 = $this->core->logic2->get_status(array('ID' => $last_record['DOC_STATUS']));
            if (empty($last_status2)) {
                $last_status_name = '----';
            } else {
                $last_status_name = $last_status2['LABEL'];
            }
            //$temp['STATUS'] = '<div class="status_label last"><span class="document_status-' . $last_status['ID'] . ' document_status-' . $last_status['SHORT'] . '">' . $last_status_name . '</span></div>';
            $temp['STATUS'] = $last_status_name;


            $temp['ACTIONS'] = join(' ', $temp['ACTIONS']);
        }
        $records = &$new_records;


        $template = 'table';
        switch ($template) {
            case 'table':
                $css_pre = 'table';
                break;
            case 'table2':
                $css_pre = 'div';
                break;
            default:
                $css_pre = 'table';
        }
        $tpl_arr = $this->core->getTemplate($template);

        $table_structure = $this->core->logic2->get_fields_all(array('ITEM_ID' => $document['ID'],
            'ITYPE' => 'DOC'));
		if (is_array($table_structure)) {
            foreach ($table_structure as $key => $item) {
				if ($item['IN_LIST'] == 0)
                    unset($table_structure[$key]);
			}
        }

		if (in_array($document['ID'], array('57', '36', '53'))) {
			$columns = array('RECORD_ID' => 'ATP ID', 'ROIK' => 'ROIK');
		} else {
			$columns = array('RECORD_ID' => 'ATP ID');
		}
        //$table_structure_order = array();
        $table_structure_by_col_name = array();
        //$order_offset = 0;
        $extra_columns = array();
        foreach ($table_structure as $structure) {
            //$table_structure_order[((int) $structure['ORD'] + (int) $order_offset)] = $structure;
            $table_structure_by_col_name[$structure['NAME']] = $structure;
            $columns[$structure['NAME']] = $structure['LABEL'];
            if ((int) $structure['TYPE'] === 23) {
                //$order_offset++;
                $extra_columns['CLAUSE'] = $structure['NAME'] . '_clause';
                $columns[$extra_columns['CLAUSE']] = 'Straipsnis';
                $table_structure_by_col_name[$extra_columns['CLAUSE']] = array(
                    'SORT' => 6
                );
            }
        }

        if (empty($document['CAN_TERM']) === false) {
            $columns['TERM'] = 'TERMINAS'; //$this->core->lng('atp_table_actions');
        }

        $columns['STATUS'] = $this->core->lng('atp_table_status');
//        $columns['ACTIONS'] = $this->core->lng('atp_table_actions');
        // Apjungiami stulpeliai su tokiais pačiais pavadinimais, pagrindinė reikšmė yra pirma netuščia reikšmė.
        $uniqueColumnsMap = [];
        foreach ($columns as $key => $label) {
            $uniqueColumnsMap[$label][] = $key;
        }
        $uniqueColumnsMap = array_filter($uniqueColumnsMap, function($items) {
            return count($items) > 1;
        });
		foreach ($uniqueColumnsMap as $columnNames) {
            foreach ($records as $key => $record) {
                $columnValue = null;
//                $firstColumnName = null;
                foreach ($columnNames as $columName) {
                    if (empty($record[$columName]) === false) {
                        $columnValue = $record[$columName];
                        break;
                    }
                }

                foreach ($columnNames as $columName) {
                    $records[$key][$columName] = $columnValue;
                }
            }

            array_shift($columnNames);
            foreach ($columnNames as $columnName) {
                unset($columns[$columnName]);
            }
        }


        // table
        $table = array();
        $table['table_class'] = $css_pre . ' gridtable';
		$table['m'] = $_REQUEST['m'];
		$table['id'] = $_REQUEST['id'];
		$html .= $this->core->returnHTML($table, $tpl_arr[0]);

        // header
        $header = array('row_html' => '');
        $header['row_class'] = $css_pre . '-row ' . $css_pre . '-header';
        $html .= $this->core->returnHTML($header, $tpl_arr[1]);
		foreach ($columns as $col_name => $col) {

			$col_arr = array();
			$col_arr = $this->core->db_getItemBy2Columns(PREFIX."ATP_FIELD", "NAME", "SORT", $col_name, '2');
			$mycolumns[$col_name] = $col_arr['ID'];
			
			// cell
            $cell = array('cell_html' => '');
            $cell['cell_class'] = $css_pre . '-cell cell-' . $col_name;
            /* if ($col_name === 'ACTIONS')
              //$cell['cell_html'] = 'style="width: 235px;"';
              $cell['cell_html'] = 'style="width: 132px;"'; */

            $cell['cell_content'] = $col.'<br/>'.html_input_text('FILTER_'.$col_name, $_GET['FILTER_'.$col_name], 15, 300);
            $html .= $this->core->returnHTML($cell, $tpl_arr[2]);
        }
        $html .= $this->core->returnHTML($header, $tpl_arr[3]);

		//foreach ($records as $key => &$record) {
        // Eilutės spauzdinamos atvirkštine tvarka
        $array_keys = array_keys($records);
        $count = count($array_keys);
        for ($i = $count - 1; $i >= 0; $i--) {
            $key = $array_keys[$i];
            $record = &$records[$key];

			// Add ROIK
			$record['ROIK'] = $roik_arr[$record[RID]];

			// ADD WS cached data
			if (file_exists(GLOBAL_REAL_URL.'subsystems/atp/files/wscache/'.$record[RID].'.txt')) {
				$wsRecordData = array();
				$wsRecordData = json_decode(file_get_contents(GLOBAL_REAL_URL.'subsystems/atp/files/wscache/'.$record[RID].'.txt'), true);
				foreach ($mycolumns as $mkey => $mval) {
					if (empty($wsRecordData['data'][$mval]) === FALSE) {
						$record[$mkey] = $wsRecordData['data'][$mval];
					}
				}
			}
            // row
            $row = array('row_html' => '', 'row_class' => $css_pre . '-row');
            $html .= $this->core->returnHTML($row, $tpl_arr[1]);

            $regId = $record['RECORD_ID'];
            foreach ($columns as $col_name => $col) {
                // cell
                $cell = array('cell_html' => '', 'cell_content' => '');

                $empty = false;
                // Checkbox'as
                if ($table_structure_by_col_name[$col_name]['SORT'] == 5) {
                    $record[$col_name] = implode(', ', json_decode(stripslashes($record[$col_name])));
                } else
                // Veika
                if ($table_structure_by_col_name[$col_name]['TYPE'] == 23) {
                    if (empty($record[$col_name]) === false) {
                        $deed = $this->core->logic->get_deed(array('ID' => $record[$col_name]), true);
                        if (empty($deed) === false) {
                            $record[$col_name] = $deed['LABEL'];

                            // Nurodomas veikos straipsnis
                            $item = $this->core->logic2->get_record_clause($record['RID'], array(
                                'DEED' => $deed['ID']));
                            if (empty($item) === false) {
                                $record[$extra_columns['CLAUSE']] = $item['ID'];
                            }
                            unset($item);
                        }
                    }
                    if (empty($deed) === true || empty($record[$col_name]) === true)
                        $empty = true;

                    unset($deed);
                } else
                // Datos ir laiko tipas
                if ($table_structure_by_col_name[$col_name]['SORT'] == 7) {
                    $timestamp = strtotime($record[$col_name]);

                    // Laikas
                    if ($table_structure_by_col_name[$col_name]['TYPE'] == 13) {
                        $record[$col_name] = date('H:i', $timestamp);
                    } else
                    // Data
                    if ($table_structure_by_col_name[$col_name]['TYPE'] == 14) {
                        $record[$col_name] = date('Y-m-d H:i', $timestamp);
                    }
                } else
                // Klasifikatorius
                if ((int) $table_structure_by_col_name[$col_name]['TYPE'] === 18) {
                    if (empty($record[$col_name]) === false) {
                        $item = $this->core->logic2->get_classificator(array('ID' => (int) $record[$col_name]));
                        if (empty($item) === false)
                            $record[$col_name] = $item['LABEL'];
                    }
                    if (empty($item) === true || empty($record[$col_name]) === true)
                        $empty = true;

                    unset($item);
                } else
                // Straipsnis
                if ((int) $table_structure_by_col_name[$col_name]['SORT'] === 6) {
                    if (empty($record[$col_name]) === false) {
                        $item = $this->core->logic2->get_clause(array('ID' => (int) $record[$col_name]));
                        if (empty($item) === false)
                            $record[$col_name] = $item['NAME'];
                    }
                    if (empty($item) === true || empty($record[$col_name]) === true)
                        $empty = true;

                    unset($item);
                }

                if ($empty === true) {
                    $record[$col_name] = '';
                }

//                // TODO: kas čia vyksta?
//                //if (!empty($table_structure_by_col_name[$structure['NAME']]['FIELD_RELATION'])) {
//                if (!empty($table_structure_by_col_name[$col_name]['FIELD_RELATION'])) {
//                    //$field_select = $this->core->logic->get_field_select($table_structure_by_col_name[$structure['NAME']]['FIELD_RELATION']);
//                    $field_select = $this->core->logic->get_field_select($table_structure_by_col_name[$col_name]['FIELD_RELATION']);
//                    foreach ($field_select as $select) {
//                        if ($select['ID'] == $record[$col_name]) {
//                            $record[$col_name] = $select['NAME'];
//                            break;
//                        }
//                    }
//                }
                // Jei tai dokumento įrašo numerio stulpelis, ant jo reikšmės formuojama redagavimo nuoroda
                if ($col_name === 'RECORD_ID') {
                    $record[$col_name] = $record[$col_name];
                } else
                // Jei nėra teisingos reikšmės, ją pakeičia į ""
                if (empty($record[$col_name]) === true)
                    $record[$col_name] = '';
                else
                // Jei tai ne ACTION stulpelis ir jei reikia, trumpinama išvedama informacija
                if ($col_name !== 'ACTIONS') {
                    $max_length = 35;
                    if (mb_strlen($record[$col_name], 'UTF-8') > $max_length) {
                        $shortened = shortHtml($record[$col_name], $max_length, '...', false, true);
                        $record[$col_name] = '<span title="' . htmlspecialchars($record[$col_name]) . '">' . $shortened . '</span>';
                    }
                }

                if ($col_name !== 'ACTIONS' && empty($record[$col_name]) === false) {
                    $record[$col_name] = '<a href="index.php?m=5&id=' . $regId . '">' . $record[$col_name] . '</a>';
                }


                $cell['cell_class'] = $css_pre . '-cell cell-' . $col_name;
                $cell['cell_content'] = $record[$col_name];
                $html .= $this->core->returnHTML($cell, $tpl_arr[2]);
            }

            $html .= $this->core->returnHTML($row, $tpl_arr[3]);
        }

        $html .= $this->core->returnHTML($table, $tpl_arr[4]);

        // Puslapiai
        $html .= $pg->display_pages();

        return $html;
    }

    public function table_record_edit()
    {
        // Jei pasitaikytu request'as su "record_id" parametru
        if (empty($_REQUEST['record_id']) === false && empty($_REQUEST['id']) === true) {
            $_REQUEST['id'] = $_REQUEST['record_id'];
            unset($_REQUEST['record_id']);
        }

        // Jei nepateikas ID, tuomet tai naujo įrašo kūrimo forma. Gaunamas / kuriamas laikinas dokumento įrašas
        $is_temp_record = false;
        if (empty($_REQUEST['id']) === true) {
            if (empty($_REQUEST['table_id']) === true) {
                throw new \Exception('Document ID is not provided');
            }

            $record_nr = $this->core->logic->create_record_empty(['DOCUMENT_ID' => $_REQUEST['table_id']]);
            if (empty($record_nr) === false) {
                $_REQUEST['id'] = $record_nr;
            }

            $is_temp_record = true;
        }
        
        $record = $this->core->logic2->get_record_relation($_REQUEST['id']);
        if (empty($record) === true) {
            throw new \Exception('Record not found');
        }


        $this->core->logic->_r_r_id = $record['ID']; // Jei ką, jis visada set'intas
//        $this->core->logic->_r_record_id = empty($record['RECORD_ID']) ? 0 : $record['RECORD_ID']; // perziurai / redagavimui
        $this->core->logic->_r_table_id = (int) $record['TABLE_TO_ID'];
        //$this->core->logic->_r_table_to_id = empty($_REQUEST['table_path']) ? $this->core->logic->_r_table_id : $_REQUEST['table_path'];
        $this->core->logic->_r_table_to_id = $this->core->logic->_r_table_id;

        // Dokumento duomenys
        $document = $this->core->logic2->get_document($this->core->logic->_r_table_id);


        // Tipo "Automatinis saugojimas" prieš veiksmą
        if (empty($_POST) === false) {
            // Jei tai naujas dokumentas
            if ($is_temp_record === true) {
                if (isset($_POST['submit_path'])) {
                    // TODO: failai i post kitur
                    foreach ($_FILES as $key => $val) {
                        $_POST[$key] = '1';
                    }
                    $this->core->logic->_r_values = $_POST;
                    $this->core->logic->create_record('save', null, $record['RECORD_ID']);
                } elseif (isset($_POST['save_new'])) {
                    unset($_POST['save_new']);
                    $_POST['save'] = 'Išsaugoti';
                }
            } else {
                if (isset($_POST['submit_path'])) {
                    foreach ($_FILES as $key => $val) {
                        $_POST[$key] = '1';
                    }
                    $this->core->logic->_r_values = $_POST;
                    $this->core->logic->create_record('save', null, $record['RECORD_ID']);
                } elseif (isset($_POST['register_document'])) {
                    throw new \Exception('Dokumento registravimo funkcija pašalinta, naudoti elektroninio dokumento registravimo funkciją.');
                } elseif (isset($_POST['view_document']) /* || ( false && isset($_POST['register_document'])) */ || isset($_POST['register_e_document']) || isset($_POST['register_case']) || isset($_POST['create_atpr']) || isset($_POST['edit_atpr']) || isset($_POST['get_atpr'])) {
                    foreach ($_FILES as $key => $val) {
                        $_POST[$key] = '1';
                    }
                    $this->core->logic->_r_values = $_POST;
                    $this->core->logic->create_record('save', null, $record['RECORD_ID']);
                    
                    $this->core->unset_messages_sess();
                    unset($this->core->logic->_r_values);

                    // Jei dokumentas užregistruotas, tai POST'u neperduodamas "template_id"
                    // Gali būti be šalono (t.y. be dokumento)
                    if (empty($_POST['template_id']) === true && empty($record['REGISTERED_TEMPLATE_ID']) === false) {
                        $_POST['template_id'] = $record['REGISTERED_TEMPLATE_ID'];
                    }

                    if (isset($_POST['view_document'])) {
                        $url = 'index.php?m=9&'
                            . '&id=' . $this->core->logic->_r_r_id
                            . '&template_id=' . $_POST['template_id'];
                    } elseif (isset($_POST['register_document'])) {
                        throw new \Exception('Dokumento registravimo funkcija pašalinta, naudoti elektroninio dokumento registravimo funkciją.');
//
//                        if (false) {
//                            $url = 'index.php?m=128'
//                                . '&table_id=' . $this->core->logic->_r_table_id
//                                . '&id=' . $this->core->logic->_r_r_id
//                                . '&template_id=' . $_POST['template_id'];
//                        }
                    } elseif (isset($_POST['register_e_document'])) {
						$tpl_url = '';
						$tpl_id = $this->core->extractVariable('template_id');
						if (empty($tpl_id) == TRUE) {
							$tpl_db_arr = array();
							$tpl_db_arr = $this->core->db_getItemByColumn(PREFIX."ATP_TABLE_TEMPLATE", "DOCUMENT_ID", $this->core->logic->_r_table_id);
							$tpl_id = $tpl_db_arr['ID'];
						}
						if (empty($tpl_id) === FALSE) {
							$tpl_url = 'template_id='.$tpl_id;
						} else {

						}
						$url = 'index.php?m=145'
                            . '&table_id=' . $this->core->logic->_r_table_id
                            . '&id=' . $this->core->logic->_r_r_id.'&'.$tpl_url;
                    } elseif (isset($_POST['register_case'])) {
                        $url = 'index.php?m=138'
                            . '&table_id=' . $this->core->logic->_r_table_id
                            . '&id=' . $this->core->logic->_r_r_id
                            . '&template_id=' . $_POST['template_id'];
                    } elseif (isset($_POST['create_atpr']) || isset($_POST['edit_atpr'])) {
                        if (isset($_POST['create_atpr'])) {
                            $type = 'create';
                        } else {
                            $type = 'edit';
                        }
                        $url = 'index.php?m=139'
                            . '&id=' . $this->core->logic->_r_r_id . '&type=' . $type;
                    } elseif (isset($_POST['get_atpr'])) {
                        $url = 'index.php?m=147'
                            . '&id=' . $this->core->logic->_r_r_id;
                    }

                    header('Location: ' . $url);
                    exit;
                }
            }
            $this->core->unset_messages_sess();
            unset($this->core->logic->_r_values);
        }

        $privileges_data = array(
            'user_id' => $this->core->factory->User()->getLoggedUser()->person->id,
            'department_id' => $this->core->factory->User()->getLoggedPersonDuty()->department->id,
            'table_id' => $this->core->logic->_r_table_id,
            'record_id' => $this->core->logic->_r_r_id
        );

        if (!empty($_POST)) {
            $action = null;
            if (isset($_POST['save']) && $this->core->logic->privileges('table_record_edit_save', $privileges_data) === true) {
                $action = 'save';
                foreach ($_FILES as $key => $val) {
                    $_POST[$key] = '1';
                }
            } elseif (isset($_POST['save_new']) && $this->core->logic->privileges('table_record_edit_save_new', $privileges_data) === true) {
                $fields = $this->core->logic2->get_fields_all([
                    'ITEM_ID' => $this->core->logic->_r_table_id,
                    'ITYPE' => 'DOC'
                ]);

                foreach ($fields as &$field) {
                    if ($field['SORT'] === '10') {
                        $files = $this->core->logic2->get_record_files([
                            'FIELD_ID' => $field['ID'],
                            'RECORD_ID' => $this->core->logic->_r_r_id
                            ], false, null, 'ID, FILE_ID');
                        foreach ($files as $file) {
                            $this->core->logic->_r_files[$field['ID']][] = $file['FILE_ID'];
                        }
                    }
                }

                foreach ($_FILES as $key => $val) {
                    $_POST[$key] = '1';
                }

                $action = 'save_new';
            } elseif (isset($_POST['finish_investigation']) && $this->core->logic->privileges('table_record_edit_finish_investigation', $privileges_data) === true) {
                $action = 'finish_investigation';
            } elseif (isset($_POST['finish_idle']) && $this->core->logic->privileges('table_record_edit_finish_idle', $privileges_data) === true) {
                $action = 'finish_idle';
            } elseif (isset($_POST['finish_examination']) && $this->core->logic->privileges('table_record_edit_finish_examination', $privileges_data) === true) {
                $action = 'finish_examination';
            } elseif (isset($_POST['finish_proccess']) && $this->core->logic->privileges('table_record_edit_finish_proccess', $privileges_data) === true) {
                $action = 'finish_proccess';
            } elseif (isset($_POST['submit_path'])) {
                if (empty($_REQUEST['table_path'])) {
                    $this->core->set_message('error', 'Nepasirinktas sekantis dokumentas');
                    $url = 'index.php?m=5&table_id=' . $this->core->logic->_r_table_id . '&id=' . $this->core->logic->_r_r_id;
                } else if ($this->core->logic->privileges('table_record_edit_submit_path', array_merge($privileges_data, ['table_id' => $_REQUEST['table_path']])) === true) {
                    $manager = new \Atp\Record\Manage(new \Atp\Document\Record($this->core->logic->_r_r_id, $this->core), $this->core);
                    $manager->transferTo($_REQUEST['table_path']);

                    $this->core->set_messages($manager->result['messages']);

                    $url = $manager->result['redirect'];
                } else {
                    $this->core->set_message('error', 'Neturite teisės kurti naujo dokumento');
                    $url = 'index.php?m=5&table_id=' . $this->core->logic->_r_table_id . '&id=' . $this->core->logic->_r_r_id;
                }

                $action = null;
            } else {
                $this->core->set_message('error', 'Pasirinktas veiksmas draudžimas');
                $url = 'index.php?m=4&id=' . $this->core->logic->_r_table_id;
            }

            if ($action !== NULL) {
                $this->core->logic->_r_values = $_POST;

                $newRecordId = $this->core->logic->create_record($action, null, $record['RECORD_ID']);
                $url = $this->core->logic->create_record_redirect_url;

                if ($action === 'save_new' || $action === 'save') {
                    $manager = new \Atp\Record\Manage(new \Atp\Document\Record((int) $newRecordId, $this->core), $this->core);
                    $manager->SaveAfter_Walkaround();
                    $this->core->set_messages($manager->result['messages']);


                    // "Pažeidimo duomenys mini" perdavimas į "Pažeidimo duomenys"
                    if ($this->core->logic->_r_table_id == \Atp\Document::DOCUMENT_INFRACTION_MINI) {
                        $manager = new \Atp\Record\Manage(
                            new \Atp\Document\Record((int) $newRecordId, $this->core),
                            $this->core
                        );
                        $manager->transferTo(\Atp\Document::DOCUMENT_INFRACTION);

                        $recordIsHiddenInCreatorList = false;
                        if ($recordIsHiddenInCreatorList) {
                            $loggedInWorkerId = $this->core->factory->User()
                                    ->getLoggedPersonDuty()
                                ->id;
                            $isRecordWorker = $this->core->factory->Record()
                                ->isWorker(
                                    $manager->result['saved_record_id'],
                                    $loggedInWorkerId
                            );

                            if ($isRecordWorker === false) {
                                $entityManager = $this->core->factory->Doctrine();
                                $repository = $entityManager->getRepository('\Atp\Entity\Record');

                                /* @var $record \Atp\Entity\Record */
                                $record = $repository->find($newRecordId);
                                $record->setInList(false);
                                $entityManager->persist($record);
                                $entityManager->flush($record);
                            }
                        }

                        $this->core->set_messages($manager->result['messages']);
                    }
                }
            }

            header('Location: ' . $url);
            exit;
        }
        
		$entityManager = $this->core->factory->Doctrine();
        $repository = $entityManager->getRepository('Atp\Entity\Record');
		$this->core->fix_ROIKToCase($repository->find($record['ID']));
		$recordEntity = $repository->find($record['ID']);
		$recordForm = new \Atp\Record\Form($recordEntity, $this->core);
        $output = $recordForm->render();

        return $this->core->atp_content($output);
    }

    /**
     * Pagal laukų hierarchiją formuoja HTML'ą
     * 
     * @param array $relations Laukų hierarchija.<br><b>array('Tevo ID' =><br>&nbsp;&nbsp;array('Vaiko 1 ID' => array(), 'Vaiko 2 ID' => array()),<br>'Lauko ID' => array())</b>
     * @param array $params [optional]
     * @return string HTML'as
     */
    public function view_column_order($relations, $params = [], $recordId = null)
    {
		$html = array(
            'all' => '',
            'field' => '',
            'children' => '',
            'classificator_fields' => '',
            'data' => array(
                'CLASS' => array(
                    'id' => 'atp-column-classificator-',
                    'field_class' => 'atp-column-classificator-fields fields'
                ),
                'DEED' => array(
                    'id' => 'atp-column-deed-',
                    'field_class' => 'atp-column-deed-fields fields'
                ),
                'CLAUSE' => array(
                    'id' => 'atp-column-clause-',
                    'field_class' => 'atp-column-clause-fields fields'
                )
            )
        );

        if (empty($params['ID']) === true) {
            // Veikos informacijai gauti per AJAX'ą
            if ($params['TYPE'] === 'DEED' && $_REQUEST['action'] === 'deedrecordfields' && $_GET['m'] === '121') {
                $template_html = '';
                // Duomenys reikalingi $this->manage->view_column_order();
                $record = $this->core->logic2->get_record_relation($recordId);
                //$this->record_id = $record['RECORD_ID'];
                $document = $this->core->logic2->get_document($_POST['table_id']); // reikalingas self::view_column_structure()
                //$this->field_sort = $this->core->logic->get_field_sort();
//                // reikalingas self::view_column_structure()
//                // Tai bl ten ir naudok
//                $this->field_type = $this->core->logic->get_field_type();
                $params = array('ID' => $_POST['deed_id'], 'TYPE' => 'DEED');

                // Dokumento data
                if (empty($_POST['date'])) {
                    // Jei nėra įvestos pažeidimo datos, neišvesti veikos informacijos
                    $params['RECORD_DATE'] = date('Y-m-d H:i:s');
                    //return null;
                } else {
                    $params['RECORD_DATE'] = $_POST['date'];
                }

//                $this->table_structure = $this->core->logic->get_fields_by_type_and_id($params);
                //$params2 = array('ITEM_ID' => $params['deed_id'], 'ITYPE' => 'DEED');
                //$this->table_structure = $this->core->logic2->get_fields_all($params2);

                if (empty($record) === false) {
                    $params['RECORD_ID'] = $record['RECORD_ID'];
                    //$table_record = $this->core->logic->get_table_records($document['ID'], $record['RECORD_ID']);
                    $table_record = $this->core->logic->get_table_records(array(
                        'DOC_ID' => $document['table_id'],
                        'RECORD_NR' => $record['RECORD_ID']
                    ));
                    $table_record = array_shift($table_record);
                    foreach ($table_record as $key => $val) {
                        if (strpos($key, $this->core->_prefix->column) !== false) {
                            unset($table_record[$key]);
                        }
                    }
                    /* $this->table_record = $this->core->logic->get_fields_values_by_type_and_id($params);
                      $this->table_record = array_merge((array) $table_record, (array) $this->table_record); */
//                    $valuesManager = new \Atp\Record\Values($this->core);
//                    $this->table_record = $valuesManager->getValuesByRecord($record, null, true);
//                    $this->table_record = array_merge($record, (array) $this->table_record);
                }
                /*
                 *  Surenka veikos laukus
                 */
                // Veikos informacija
                $deed = $this->core->logic->get_deed(array('ID' => $_POST['deed_id']));
                // Veiką praplečia
                $ddata = $this->core->logic->get_deed_extend_data(array('DEED_ID' => $deed['ID']));

                // Surenka veikos praplečiančių elementų ID
                $act_ids = array();
                $clause_ids = array();
                foreach ($ddata as $arr) {
                    if ($arr['ITYPE'] === 'ACT') {
                        $act_ids[$arr['ID']] = $arr['ITEM_ID'];
                    } elseif ($arr['ITYPE'] === 'CLAUSE') {
                        $clause_ids[$arr['ID']] = $arr['ITEM_ID'];
                    }
                }
// TODO: veika savo laukų nebeturi
                /*
                  // Veikos laukai
                  $fields = (array) $this->core->logic->get_table_fields_w_extra2r($deed['ID'], array('ITYPE' => 'DEED'));
                  $relations = array();
                  foreach ($fields as $val) {
                  $relations[$val['ID']] = array();
                  }
                  // Suformuoja įvedimo laukų HTML'ą
                  $template_html .= $this->view_column_order($relations);
                 */
                // Teisės akto laukai
				if (count($act_ids)) {
                    $data = $this->core->logic2->get_act(array(
                        0 => '`ID` IN(' . join(',', $act_ids) . ')',
                        1 => '"' . $params['RECORD_DATE'] . '"' . ' > `VALID_FROM` AND "' . $params['RECORD_DATE'] . '" <= `VALID_TILL`'
                    ));
					if (count($data)) {
                        foreach ($data as &$arr) {
                            //$fields = (array) $this->core->logic->get_table_fields_w_extra2r($arr['ID'], array('ITYPE' => 'ACT'));
                            //$fields = $this->core->logic2->get_fields(array('ITYPE' => 'ACT', 'ITEM_ID' => $arr['ID']));
                            //$this->table_structure = $fields;
                            // TODO: HOTFIX. get rid of $this->act_structure
                            $this->act_structure = $this->core->logic2->get_fields(array('ITYPE' => 'ACT', 'ITEM_ID' => $arr['ID']));
                            $field_info = $this->get_info_fields_html(array('ID' => $arr['ID'],
                                'TYPE' => 'ACT'));



                            // Papildomi laukai
                            $fields = $this->core->logic2->get_act_default_fields_structure();
                            $fields = sort_by($fields, 'ORD', 'asc');
                            // Laukų reikšmės
                            //$fields_values = $this->core->logic2->get_fields(array('ITYPE' => 'ACT', 'ITEM_ID' => $arr['ID']));

                            $relations = array();
                            foreach ($fields as $val) {
                                $relations[$val['ID']] = array();
                            }
                            // Suformuoja įvedimo laukų HTML'ą
                            $fields = $this->view_column_order($relations, null, $record['ID']);

                            // Gauna straipsnio lauką, kurio pavadinimas yra "acol_5" ("Tekstas") ir jį pašalina
                            $parent_html = '';
                            $dom = new \DOMDocument('1.0', 'UTF-8');
                            $html = mb_convert_encoding($fields, 'HTML-ENTITIES', 'UTF-8');
                            $dom->loadHTML($html);
                            // Pakeitimas pateiktame HTML'e
                            $xpath = new \DOMXPath($dom);
                            $body = $xpath->query("//body")->item(0);
                            $tags = $xpath->query("//textarea[@name='acol_5']");

                            if ($tags->length) {
                                $tag = $tags->item(0);
                                $parent_node = $tag->parentNode;
								if (version_compare(phpversion(), '5.3.6', '>=') === true) {
                                    $parent_html = $dom->saveHTML($parent_node);
                                } else {
                                    $p_dom = new \DOMDocument('1.0', 'UTF-8');
                                    $p_node = $p_dom->importNode($parent_node, true);
                                    $p_dom->appendChild($p_node);
                                    $parent_html = $p_dom->saveHTML();
                                }

                                $body->removeChild($parent_node);
                                $html = $dom->saveHTML();

                                $preg = array(
                                    'pattern' => '~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i',
                                    'replacement' => ''
                                );
                                $fields = preg_replace($preg['pattern'], $preg['replacement'], $html);
                            }

							$a_col_field_arr = array();
							$a_col_field_arr = FindAddText($parent_html, '<textarea', '</textarea>', 1, 1);
							if (empty($a_col_field_arr[0]) === FALSE) {
								$a_col_value_arr = array();
								$a_col_value_arr = FindAddText($a_col_field_arr[0], '">', '</textarea>');
								if (empty($a_col_value_arr) == false) {
									$fdata = reset($data);
									$parent_html = str_replace($a_col_value_arr[0], $fdata['LABEL'], $parent_html);
								}
							}

							// "acol_5" prideda priekyje
                            $fields = $parent_html . $field_info . $fields;
                            $template_html .= $fields;
                            break; // only one
                        }
                    }
                }
                
                // Straipsnio laukai
                if (count($clause_ids)) {
                    //$data = $this->core->logic2->get_clause(array(
                    //	0 => '`ID` IN(' . join(',', $clause_ids) . ')'
                    //));
                    $data = $this->core->logic2->get_clause(array(
                        0 => '`ID` IN(' . join(',', $clause_ids) . ')',
                        1 => '"' . $params['RECORD_DATE'] . '"' . ' > `VALID_FROM` AND "' . $params['RECORD_DATE'] . '" <= `VALID_TILL`'
                    ));
                    if (count($data)) {
                        foreach ($data as &$arr) {
                            //$fields = (array) $this->core->logic->get_table_fields_w_extra2r($arr['ID'], array('ITYPE' => 'CLAUSE'));
                            $fields = $this->core->logic2->get_fields(array('ITYPE' => 'CLAUSE',
                                'ITEM_ID' => $arr['ID']));
                            // TODO: HOTFIX. get rid of $this->clause_structure
                            $this->clause_structure = $fields;
                            $field_info = $this->get_info_fields_html(array('ID' => $arr['ID'],
                                'TYPE' => 'CLAUSE'));

                            $relations = array();
                            foreach ($fields as $val) {
                                $relations[$val['ID']] = array();
                            }

                            // Suformuoja įvedimo laukų HTML'ą
                            $template_html .= $field_info . $this->view_column_order($relations, null, $recordId);
                            break; // only one
                        }
                    }
                }

                $dom = new \DOMDocument('1.0', 'UTF-8');
                $html = mb_convert_encoding($template_html, 'HTML-ENTITIES', 'UTF-8');
                $dom->loadHTML($html);
                // Pakeitimas pateiktame HTML'e
                $xpath = new \DOMXPath($dom);
                $body = $xpath->query("//body")->item(0);

                $tags = $xpath->query("//input[@name='acol_1']|//input[@name='acol_2']|//input[@name='acol_3']|//input[@name='acol_4']|//textarea[@name='scol_1']");
                foreach ($tags as $tag) {
                    $tag->setAttribute('readonly', 'readonly');
                    $tag->setAttribute('style', 'display: none;');

                    if($tag->nodeName === 'textarea') {
                        $value = $tag->nodeValue;
                    } else {
                        $value = $tag->getAttribute('value');
                    }

                    $valueElement = $dom->createElement('span', $value);
                    $valueElement->setAttribute('class', 'value');
                    $valueElement->setAttribute('value', $value);

                    $tag->parentNode->insertBefore($valueElement);
                }
                $html = $dom->saveHTML();
                $preg = array(
                    'pattern' => '~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i',
                    'replacement' => ''
                );
                $template_html = preg_replace($preg['pattern'], $preg['replacement'], $html);

                return $template_html;
            }

//            $structure = $this->table_structure;
        }
//        else {
//            $structure = $this->core->logic->get_extra_fields($params['ID'], $params['TYPE']);
//        }


        $fieldHelper = $this->core->factory->Field();

        foreach ($relations as $fieldId => $children) {
            /* @var $field \Atp\Entity\Field */
            $field = $fieldHelper->getField($fieldId);
            // Lauko HTML'as
            $html['field'] .= $this->view_column_structure($field->getId(), $recordId);
            if (empty($field->getVisible())) {
                $html['field'] = str_replace('atp-table-fill', 'atp-table-fill hidden', $html['field']);
            }

            // Jei turi vaikų, gauna vaikų HTML'ą
            if (empty($children) === false) {
                $html['children'] .= $this->view_column_order($children, null, $recordId);
            }

            $type = '';
            $classes = $fields = array();
            // Jei yra klasifikatorius, surenka klasifikatoriaus laukus
            if (in_array($field->getSort(), [4, 6]) || $field->getWS()) {
                // Pagal tevinį ID suranda visų vaikų laukus
                if ($field->getType() == 18) {
                    $type = 'CLASS';
                    $classes[] = 'atp-column-classificator';
                    $classes[] = 'group';
                    $fields = $this->core->logic->get_extra_fields($field->getSource(), $type, 'parent');
                } elseif ($field->getType() == 23) {
                    $type = 'DEED';
                    $classes[] = 'atp-column-deed';
                    $classes[] = 'group';
                    $html['extra_fields'] = null
                        . '<div id="atp-column-deed-' . $field->getId() . '" class="atp-column-deed-fields">'
                        . '</div>';
                } elseif ($field->getType() == 19) {
                    throw new \Exception('Clause fields where removed');
//                    $type = 'CLAUSE';
//                    $classes[] = 'atp-column-clause';
//                    $classes[] = 'group';
//                    //$fields = $this->core->logic->get_all_extra_fields_by_type($type, 'A.ORD ASC');
//
//                    $fields = $this->core->logic2->get_fields(array('ITYPE' => 'CLAUSE'));
//
//                    $fields_tmp = array();
//                    $clauses = $this->core->logic2->get_clause();
//                    foreach ($clauses as &$clause) {
//                        $fields_tmp[$clause['ID']] = $fields;
//                    }
//                    $fields = $fields_tmp; // Don't make reference!!!
                }

                // Webserviso laukas
                if ($field->getWs()) {
                    $type = 'WS';
                    $classes[] = 'atp-column-ws';
                    $classes[] = 'group';
                    $html['extra_fields'] .= null
                        . '<div id="atp-column-ws-' . $field->getId() . '" class="atp-column-ws-fields">'
                        . '</div>';
                }

                if (empty($fields) === false) {
                    foreach ($fields as $field_id_2 => $children_2) {
//                        if ($type === 'CLAUSE') {
//                            $html['field_info'] = $this->get_info_fields_html(array(
//                                'ID' => $field_id_2, 'TYPE' => $type));
//                        }
                        // Pagal laukų sąryšius suformuoja laukų hierarchiją
                        $table_column_relation = [];
                        foreach ($children_2 as $arr) {
                            if (empty($arr['RELATION']) === false)
                                $table_column_relation[$arr['ID']] = [
                                    'COLUMN_ID' => $arr['ID'],
                                    'COLUMN_PARENT_ID' => $arr['RELATION']
                                ];
                        }
                        $column_relations = [];
                        foreach ($children_2 as $arr) {
                            $relation_status = $this->core->logic->get_column_relation_status($arr['ID'], $table_column_relation);
                            if ($relation_status == 1) {
                                $column_relations[$arr['ID']] = $this->core->logic->get_column_relation_order($arr['ID'], $table_column_relation);
                            } elseif ($relation_status == 2) {
                                $column_relations[$arr['ID']] = [];
                            }
                        }
                        // TODO: pažymėta su PHP, o ne JS'u
                        $html['extra_fields'] .= '<div id="' . $html['data'][$type]['id'] . $field_id_2 . '" class="' . $html['data'][$type]['field_class'] . '">'
                            . $html['field_info']
                            . $this->view_column_order(
                                $column_relations,
                                [
                                    'ID' => $field_id_2,
                                    'TYPE' => $type
                                ],
                                $recordId
                            )
                            . '</div>';
                    }
                }
            }

            // Gautą HTML'ą prijungia prie rezultato
            if (empty($html['children']) === false || empty($html['extra_fields']) === false) {
                if (empty($html['children']) === false) {
                    $classes[] = 'atp-column-parent';
                    $classes[] = 'group';
                }

                // Per WS lauką gauti duomenys
                if ($type === 'WS' && empty($recordId) === false) {
                    $ws = $this->core->logic2->get_ws();
                    
                    $html_arr = [];
                    $values_arr = [];
                    // Sąryšiai tarp web-serviso laukų ir dokumento laukų į kuriuos keliaus informacija
                    $ws_relations = $this->core->logic2->get_webservices([
                        'PARENT_FIELD_ID' => $field->getId()
                    ]);
                    foreach ($ws_relations as $relation) {
                        // Web-serviso lauko informaciją
                        $info = $ws->get_field($relation['WS_NAME'], [
                            'ID' => $relation['WS_FIELD_ID']
                        ]);
                        if (empty($info)) {
                            continue;
                        }

                        // Lauko informacija, į kurį keliaus informacija
                        $wsDestinationField = $this->core->factory->Field()
                            ->getField($relation['FIELD_ID']);
                        if (empty($wsDestinationField)) {
                            continue;
                        }

                        $value = $this->core->factory->Record()
                            ->getFieldValue($recordId, $wsDestinationField->getId());
                        
                        if (empty($value) === true) {
//                            $empty_arr[$wsDestinationField->getId()] = '';
                            continue;
                        }

                        $html_arr[$wsDestinationField->getOrd()] = ''
                            . '<div class="atp-table-fill info">'
                            . ' <div class="atp-rec-title">'
                            . '  ' . $wsDestinationField->getlabel() . ':'
                            . ' </div>'
                            . ' <span>' . $value . '</span>'
                            . '</div>';
                        
                        $values_arr[$wsDestinationField->getId()] = $value;
                    }
                    
                    if(!count($values_arr)) {
                        $classes[] = 'atp-no-data';
                    }
                    
//                    $values_arr = $values_arr + $empty_arr;

//                    dump($values_arr);
                    $str = join('', $html_arr);
                    foreach ($values_arr as $wsFieldId => $value) {
                        $str .= '<input type="hidden" name="ws_field[' . $field->getId() . '][' . $wsFieldId . ']" value="' . htmlspecialchars($value) . '"/>';
                    }
                    $html['extra_fields'] = str_replace(
                        'class="atp-column-ws-fields">',
                        'class="atp-column-ws-fields">' . $str,
                        $html['extra_fields']
                    );
                }

                // TODO; sukelti į šablonus
                $contains = '';
                if (empty($contains)) {
                    $contains .= ''
                        . '<div id="atp-column' . $field->getId() . '" class="' . join(' ', array_unique($classes)) . '">'
                        . ' ' . $html['field']
                        . (empty($html['extra_fields']) ? null : null
                        . ' ' . $html['extra_fields'])
                        . (empty($html['children']) ? null : null
                        . ' ' . '<div id="atp-column-parent-' . $field->getId() . '" class="atp-column-child fields">'
                        . '  ' . $html['children']
                        . ' ' . '</div>'
                        )
                        . '</div>';
                }
                $html['all'] .= $contains;
            } else {
                $html['all'] .= $html['field'];
            }

            unset($html['field'], $html['children'], $html['extra_fields'], $html['field_info']);
        }
        return $html['all'];
    }

    /**
     * Informacinių laukų HTML'as
     * @param array $params
     * @param int $params['ID'] Elemento ID
     * @param string $params['TYPE'] CLAUSE / ACT
     * @return string HTML'as
     */
    public function get_info_fields_html($params)
    {
        $data = $this->core->logic->get_info_fields($params);
        $ret = null;
        if (empty($data) === false)
            foreach ($data['header'] as $key => $val) {
                if (empty($data['value'][$key]) === true)
                    continue;
                $ret .= '<div class="atp-table-fill info field-' . $data['column'][$key] . '"><div class="atp-rec-title">' . $val . ':</div><span class="value" value="' . htmlspecialchars($data['data'][$key]) . '">' . $data['value'][$key] . '</span></div>';
            }

        return $ret;
    }

    /**
     * Sugeneruoja dokumento lauko HTML'ą
     * @author Martynas Boika
     * @param int $fieldId
     * @return string HTML'as
     */
    public function view_column_structure($fieldId, $recordId)
    {
		$recordHelper = $this->core->factory->Record();
        $fieldHelper = $this->core->factory->Field();
        $record = $recordHelper->Get($recordId);
        
        $isTemporary = $recordHelper->isTemporary($recordId);

        $table = $this->core->logic2->get_document($record->document_id);
        $field_type = $this->core->logic->get_field_type();
        $loggedInWorkerId = $this->core->factory->User()->getLoggedPersonDuty()->id;
        $loggedInPersonId = $this->core->factory->User()->getLoggedUser()->person->id;
        
        /* @var $field \Atp\Entity\Field */
        $field = $fieldHelper->getField($fieldId);

        // TODO: ACT laukai vistiek turi teisingus ID, tai reikia pridėti paiešką pagal `ID`
        if (in_array($field->getItype(), [
                \Atp\Document\Field::TYPE_ACT,
                \Atp\Document\Field::TYPE_CLAUSE
            ])) {

            if ($field->getItype() === \Atp\Document\Field::TYPE_ACT) {
                foreach($this->act_structure as $act_field) {
                    if((int) $act_field['ID'] === $field->getId()) {
                        $field->setDefault($act_field['DEFAULT']);
                        break;
                    }
                }
            } elseif ($field->getItype() === \Atp\Document\Field::TYPE_CLAUSE) {
                foreach($this->clause_structure as $clause_field) {
                    if((int) $clause_field['ID'] === $field->getId()) {
                        $field->setDefault($clause_field['DEFAULT']);
                        break;
                    }
                }
            }
        }

        $structure_field_type = $field_type[$field->getType()]['VALUE'];

        //$template_hidden_html = '';
        $template_name = '';
		$helpTip = false;
		if (empty($field->getHelpTip()) === FALSE) {
			$helpTip = '<img title="'.$field->getHelpTip().'" src="/images/help.png" border="0" width="15"/>';
		}
		if (empty($field->getMandatory()) === FALSE) {
			$helpTip = '(<span style="color:red">*</span>)'
			.$helpTip
			.str_replace('type="hidden"', 'type="hidden" class="atp-mandatory"', html_input_hidden('mandatoryField['.strtolower($field->getName()).']', strtolower($field->getName())));
		}
		$template_data = [
            'title' => $field->getLabel().$helpTip,
            'name' => strtolower($field->getName()),
            'html' => '',
            'readonly' => '',
            'disabled' => '',
            'unallowed' => false,
            'v' => '',
            'no_check' => false,
            'data-init' => ''
        ];
        
        $isEditable = $this->core->factory->Field()->isEditable($recordId, $fieldId, $loggedInWorkerId);
        if ($isEditable === false) {
            $template_data['disabled'] = ' disabled="disabled"';
            $template_data['unallowed'] = true;
        }

        $valuesHelper = new \Atp\Record\Values($this->core);
        $values =  $valuesHelper->getValuesByRecord($recordId, null, true);
        $structure_value = isset($values[$field->getName()]) ? $values[$field->getName()] : NULL;
        
        $template_data['id'] = '';
        $template_data['rows'] = '';
        $template_data['type'] = 'text';
        $template_data['value'] = $structure_value;
		if ($field->mandatory == '1') {
//			$template_data['class'] = ' atp-mandatory ';
			$template_data['class'] = '';
		} else {
			$template_data['class'] = '';
		}
        $template_data['rec_class'] = '';
        $template_data['onkeypress'] = '';
        $template_data['max_length'] = '';
        $template_data['note_class'] = '';
        $template_data['note_text'] = '';
        $template_html_data = [];

        // TROL LOGIKOS
        // Protokolas
        $logics_fields = [1124, 1126];
        if (in_array($field->getId(), $logics_fields) && $table['ID'] == \Atp\Document::DOCUMENT_PROTOCOL) {
            $logics = new \Atp\Document\Record\Logics($recordId, $this->core);

            // Protokolas: Protokolo tipas
            if ($field->getId() === 1124) {
                if (empty($structure_value)) {
                    $structure_value = $logics->getProtocolClassificator();
                }
                $template_data['disabled'] = ' disabled="disabled"';
                $template_data['class'] = ' reenable';
            } else
            // Protokolas: AN baudos suma
            if ($field->getId() === 1126) {
                if (empty($structure_value) === true) {
                    $structure_value = $logics->getFirstPenalty();
                }
            }

            $template_data['value'] = $structure_value;
        }

        // Pažeidimo duomenys mini
        $dateTimeFields = [
            'date' => $this->core->factory->Document()->getOptions($table['ID'])->violation_date,
            'time' => $this->core->factory->Document()->getOptions($table['ID'])->violation_time,
        ];
        if ((empty($structure_value) || in_array($field->getId(), $dateTimeFields)) && $table['ID'] == \Atp\Document::DOCUMENT_INFRACTION_MINI) {
            // Užpildyta pažeidimo data ir laikas
            if (in_array($field->getId(), $dateTimeFields)) {
                switch ($field->getId()) {
                    case $dateTimeFields['date']:
                        $structure_value = date('Y-m-d');
                        break;
                    case $dateTimeFields['time']:
                        $structure_value = date('H:i');
                        break;
                }
                $template_data['value'] = $structure_value;
            } else {
                // Liudytojas yra Pareigūnas
                $classificatorFieldId = 2557;
                // Vardas // Pavardė // Adresas // Telefonas // El. paštas
                $logics_fields = [
                    [2672, 2534, 2522, 2523, 1269],
                    [2670, 2490, 1264, 1265, 1266]
                ];
                if ($field->getId() == $classificatorFieldId) {
                    $template_data['value'] = $structure_value = 115;
                } else
                if (in_array($field->getId(), $logics_fields[0]) || in_array($field->getId(), $logics_fields[1])) {
                    switch ($field->getId()) {
                        // Vardas
                        case $logics_fields[0][0]:
                        case $logics_fields[1][0]:
                            $fieldName = 'TICOL_4';
                            break;
                        // Pavardė
                        case $logics_fields[0][1]:
                        case $logics_fields[1][1]:
                            $fieldName = 'TICOL_5';
                            break;
                        // Adresas
                        case $logics_fields[0][2]:
                        case $logics_fields[1][2]:
                            $fieldName = 'TICOL_11';
                            break;
                        // Telefonas
                        case $logics_fields[0][3]:
                        case $logics_fields[1][3]:
                            $fieldName = 'TICOL_1';
                            break;
                        // El. paštas
                        case $logics_fields[0][4]:
                        case $logics_fields[1][4]:
                            $fieldName = 'TICOL_2';
                            break;
                    }

                    $infoFields = $this->core->logic->record_worker_info_to_fields($recordId);
                    foreach ($infoFields as $infoField) {
                        if ($infoField['NAME'] === $fieldName) {
                            $structure_value = $infoField['VALUE'];
                            break;
                        }
                    }

                    $template_data['value'] = $structure_value;
                }
            }
        }

        // Skaičius
        if ($field->getSort() == 1 && $field->getType() != 6) {
            $template_name = 'input';

            $template_data['type'] = 'text';
            $template_data['onkeypress'] = ' onkeypress="return isNumberKey(event)"';

            $max_length = array(1 => 3, 2 => 5, 3 => 7, 4 => 11, 5 => 19);
        } else
        // Skaičius su kableliu
        if ($field->getSort() == 1 && $field->getType() == 6) {
            $template_name = 'input';

            $template_data['type'] = 'text';
            $template_data['onkeypress'] = ' onkeypress="return isFloatNumberKey(event)"';
            $template_data['value'] = empty($structure_value) === false ? $structure_value : '0.00';
        } else
        // Tekstinis tipas
        if ($field->getSort() == 2) {
            // Smulkus tekstas
            if ($field->getType() == 7) {
                $template_name = 'input';
            } else {
                $template_name = 'textarea';
            }

            $rows = array(8 => 2, 9 => 4, 10 => 6);
            $max_length = array(7 => 256, 8 => 65000, 9 => 16000000, 10 => 4000000000);
        } else
        // Klasifikatorius
        if ($field->getSort() == 4 && $field->getType() == 18) {
            if ($field->getRender() == 1) {
                $template_name = 'classificator_ajax';

                if (empty($field->getDefault()) === false && empty($field->getDefaultId()) === false && empty($structure_value) === true) {
                    $structure_value = $field->getDefaultId();
                }

                $selectedValue = 0;

                if (empty($structure_value) === false) {
                    /* @var $repository \Atp\Repository\ClassificatorRepository */
                    $repository = $this->core->factory->Doctrine()->getRepository('Atp\Entity\Classificator');
                    $classifiactor = $repository->find($structure_value);
                    if ($classifiactor) {
                        $selectedValue = $classifiactor->getId();
                        $selectedLabel = $classifiactor->getLabel();
                    }
                }

                $template_data = array_merge($template_data, [
                    'uuid' => uniqid(),
                    'value' => $selectedValue,
                    'label' => $selectedLabel,
                    'source' => $field->getSource()
                ]);

                $structure_value = $selectedValue;
            } else {

                $template_name = 'select';

                if (empty($field->getDefault()) === false && empty($field->getDefaultId()) === false && empty($structure_value) === true) {
                    $structure_value = $field->getDefaultId();
                }

                $selectedValue = 0;

                /* @var $repository \Atp\Repository\ClassificatorRepository */
                $repository = $this->core->factory->Doctrine()->getRepository('Atp\Entity\Classificator');
                $classifiactors = $repository->getChildrenById($field->getSource());
                foreach ($classifiactors as $classifiactor) {
                    $selected = false;
                    if ($structure_value == $classifiactor->getId()) {
                        $selected = true;
                        $selectedValue = $classifiactor->getId();
                    }

                    $template_html_data[] = [
                        'value' => $classifiactor->getId(),
                        'selected' => ($selected ? ' selected="selected"' : null),
                        'label' => $classifiactor->getLabel()
                    ];
                }

                $structure_value = $selectedValue;
            }
        } else
        // Teisės aktas (nebenaudojamas)
        if ($field->getSort() == 4 && $field->getType() == 22) {
            throw new \Exception('Teisės akto tipo laukas nebepalaikomas');
        } else
        // Veika
        if ($field->getSort() == 4 && $field->getType() == 23) {
            $template_name = 'select';

            if (empty($field->getDefault()) === false && empty($field->getDefaultId()) === false && empty($structure_value) === true) {
                $structure_value = $field->getDefaultId();
            }

            $selectedValue = 0;

            $data = $this->core->logic->get_deed_search_values();
            foreach ($data as $arr) {
                $selected = false;
                if ($structure_value == $arr['ID'] && $arr['TYPE'] === 'DEED') {
                    $selected = true;
                    $selectedValue = $arr['VALUE'];
                }

                $template_html_data[] = [
                    'value' => $arr['VALUE'],
                    'selected' => ($selected ? ' selected="selected"' : null),
                    'label' => $arr['LABEL']
                ];
            }

            $structure_value = $selectedValue;
        } else
        // Checkbox ir radio
        if ($field->getSort() == 5 || $field->getSort() == 8) {
            $template_name = 'pick';

            $pk_type = '';
            if ($field->getSort() == 5) {
                $pk_type = 'checkbox';
                $template_data['name'] = $template_data['name'] . '[]';
                $structure_value = json_decode(stripslashes($structure_value));
            } else
            if ($field->getSort() == 8) {
                $pk_type = 'radio';
                $structure_value = [$structure_value];
            }

            $selectedValue = 0;

            /* @var $repository \Atp\Repository\ClassificatorRepository */
            $repository = $this->core->factory->Doctrine()->getRepository('Atp\Entity\Classificator');
            $classifiactors = $repository->getChildrenById($field->getSource());
            foreach ($classifiactors as $classifiactor) {
                $pk_checked = false;
                if (is_array($structure_value) && in_array($classifiactor->getId(), $structure_value)) {
                    $pk_checked = true;
                    $selectedValue = $classifiactor->getId();
                }

                $template_html_data[] = [
                    'pk_type' => $pk_type,
                    'pk_id' => $classifiactor->getId(),
                    'pk_label' => $classifiactor->getLabel(),
                    'pk_class' => '',
                    'pk_name' => $template_data['name'],
                    'pk_checked' => $pk_checked,
                    'pk_checked' => ($pk_checked ? ' checked="checked"' : null),
                    'pk_value' => $classifiactor->getId()
                ];
            }

            $structure_value = $selectedValue;
        } else
        // Straipsnis
        if ($field->getSort() == 6) {
            $template_name = 'select';

            $user_clauses = $this->core->logic->get_user_clauses($this->core->factory->User()->getLoggedPersonDuty()->id);

            if (empty($field->getDefault()) === false && empty($field->getDefaultId()) === false && empty($structure_value) === true)
                $structure_value = $field->getDefaultId();

            $str_val = 0;
            foreach ($user_clauses as $clause) {
                $selected = '';
                //if ($structure_value == $clause['NAME']) {
                if ($structure_value == $clause['ID']) {
                    $selected = ' selected="selected"';
                    $str_val = $clause['ID'];
                }
                $template_html_data[] = array('value' => $clause['ID'], 'selected' => $selected,
                    'label' => $clause['NAME']);
            }
            $structure_value = $str_val;
        } else
        // Datos ir laiko laukai
        if ($field->getSort() == 7) {
            $template_name = 'input';

            $is_empty = false;

            // Jeigu tuščia, data ar laikas prasideda nuliais
            if (empty($structure_value) === true || preg_match('/^([0]{4}.*|[0]{2}:[0]{2}(:[0]{2})?)$/', $structure_value) === 1 || strtotime($structure_value) <= 0)
                $is_empty = true;

            $timestamp = strtotime($structure_value);
            if ($field->getType() == 11) {
                $max_length = array(11 => 4);
                $template_data['class'] .= ' atp-date-year';
                $time_string = date('Y', $timestamp);
            } else
            if ($field->getType() == 12) {
                $max_length = array(12 => 10);
                $template_data['class'] .= ' atp-date';
                $time_string = date('Y-m-d', $timestamp);
            } else
            if ($field->getType() == 13) {
                $max_length = array(13 => 8);
                $template_data['class'] .= ' atp-time';
                //$time_string = date('H:i:s', $timestamp);
                $time_string = date('H:i', $timestamp);
            } else
            if ($field->getType() == 14) {
                $max_length = array(14 => 19);
                $template_data['class'] .= ' atp-datetime';
                //$time_string = date('Y-m-d H:i:s', $timestamp);
                $time_string = date('Y-m-d H:i', $timestamp);
            }

            if ($structure_value === NULL) {
                $structure_value = NULL;
            } elseif ($is_empty === true)
            //$structure_value = '';
            /**
             * @TODO: [done] Netinka, kadangi radus NULL reikšmę įkeliama reikšmė pagal nutylėjimą
              $structure_value = NULL;
             */
                $structure_value = '';
            else {
                $structure_value = $time_string;
            }

            $template_data['value'] = $structure_value;
        } else
        // Antraštė
        if ($field->getSort() == 9) {
            $template_name = 'text';
            $template_data['title'] = $template_data['title'];
            $template_data['value'] = $field->getText();
        } else
        // Failas
        if ($field->getSort() == 10) {
            $template_name = 'files';

            $template_data['type'] = 'file';
            $template_data['name'] = $template_data['name'] . '[]';
            $template_data['class'] = '-file' . $template_data['class'];

            // Failo lauko nustatymai
            $file_params = $this->core->logic->get_file_params(array(
                'DOCUMENT_ID' => $field->getItemId(),
                'STRUCTURE_ID' => $field->getId(),
                'LIMIT' => 1));
            if (!empty($file_params)) {
                $file_params = array_shift($file_params);

                $template_data['note_class'] = ' atp-note-show';
                $template_data['note_text'] = 'Failų kiekis: <span class="atp-file-cnt">' . $file_params['CNT'] . '</span>, '
                    . 'dydis: ' . $file_params['SIZE_FROM'] . ' - ' . $file_params['SIZE_TO'] . ' Mb';
            }

            // TODO: RECORD_ID | į RECORD_ID turi ateiti RECORD_RELATION.ID
            // Dokumento įrašo lauko ir failų ryšiai
            $files = $this->core->logic2->get_record_files(array(
                'RECORD_ID' => $recordId,
                'FIELD_ID' => $field->getId()));

            // TODO: atskirti failo išvedimą pagal failo tipą
            $file_list = '';
            if (empty($files) === false) {
                foreach ($files as &$file) {
                    // Failo informacija
                    $file_info = $this->core->logic2->get_files(array('ID' => $file['FILE_ID']));

                    if (/* strpos($file_info['TYPE'], 'image') === 0 && */$field->getType() === 20) {
                        // Sugeneruoja thumbnail'ą
                        $path = 'files/tmp/';
                        $thumb_name = $this->core->action->createThumbnail($this->core->config['REAL_URL'] . $file_info['PATH'], $this->core->config['REAL_URL'] . $path);
                        $thumb_path = $this->core->config['SITE_URL'] . $path . $thumb_name;
                        // Failo išvedimas
                        $file_list .= '<li for="' . $file['ID'] . '">'
                            . '<a href="' . $this->core->config['SITE_URL'] . $file_info['PATH'] . '" target="_blank">'
                            . '<img class="photo" style="background: url(' . $thumb_path . ') center center no-repeat;"/>'
                            . $file_info['NAME']
                            . '</a>'
                            . '</li>';
                    } elseif ($field->getType() === 21) {
                        $file_list .= '<li for="' . $file['ID'] . '">'
                            . '<a href="' . $this->core->config['SITE_URL'] . $file_info['PATH'] . '" target="_blank">'
                            . $file_info['NAME']
                            . '</a>'
                            . '</li>';
                    }
                }
            }

            if (empty($file_list) === false) {
                if ($field->getType() === 20) {
                    $file_list = '<div class="file-list"><ul id="photos_b" class="photo-list fl">' . $file_list . '</ul></div>';
                } elseif ($field->getType() === 21) {
                    $file_list = '<div class="file-list"><ul class="file-list">' . $file_list . '</ul></div>';
                }
            }
            $template_data['file_list'] = &$file_list;
        }

        // Web-serviso laukui
        if ($field->getWs() && $isEditable) {
            if ($this->core->logic2->is_VMSA_record($record->id) === true) {
                $template_data['no_check'] = true;
            } else {
                // Rodoma tik pažeidimo duomenyse
                if ((int) $table['ID'] === \Atp\Document::DOCUMENT_INFRACTION) {
                    // Pridedamas mygtukas tikrinimui
                    $template_data['html'] .= '<div class="check ws">tikrinti</div>';
                    //$template_data['data-init'][] = 'attachWs';
                }
            }
        }

        // Pažeidimo duomenyse pažeidėjo asmens kodo laukui pridedamas mygtukas baustumo tikrinimui
        if ($table['ID'] == \Atp\Document::DOCUMENT_INFRACTION && $isEditable) {
            if ((int) $field->getId() === (int) $this->core->factory->Document()->getOptions($table['ID'])->person_code) {
                $template_data['html'] .= '<div class="check punishment">tikrinti baustumą</div>';
                //$template_data['data-init'][] = 'attachPenaltyCheck';
            }
        }

        if (!empty($rows[$field->getType()])) {
            $template_data['rows'] = ' rows="' . $rows[$field->getType()] . '"';
        }

        if (!empty($max_length[$field->getType()])) {
            $template_data['max_length'] = ' maxlength="' . $max_length[$field->getType()] . '"';
        }

        $configurations = $field->getConfigurations();
        if($configurations) {
            foreach($configurations as $configuration) {
                if($configuration->getType()->getName() === 'address_search') {
                    $template_data['data-init'][] = 'attachAddressSearch';
                    break;
                }
            }
        }

        if (!empty($template_name)) {
            $template = $this->core->getTemplate('template_' . $template_name);

            if ($template_data['unallowed'] === true) {
                $template_data['disabled'] .= ' unallowed="unallowed"';
            }
            if ($template_data['no_check'] === true) {
                $template_data['no_check'] = ' atp-no-check="atp-no-check"';
            }
            if ($template_data['data-init']) {
                $template_data['data-init'] = ' data-init="' . join(';', $template_data['data-init']) . '"';
            }
            if (in_array($field->getSort(), array(4, 5, 6, 8)) === false) {
                /* if (isset($template_data['value']) === true && empty($template_data['value']) === true && empty($field->getDefault()) === false) { */
                // Pagal nutylėjimą papildomiems laukams $template_data['value'] yra NULL (pagrindiniams laukams įvairiai, t.y. "" arba 0), o isset(NULL) yra false
                if (empty($template_data['value']) === true && empty($field->getDefault()) === false) {
                    // Jei datos ir laiko tipas
                    if ($field->getSort() == 7 && strtotime($field->getDefault()) <= 0) {
                        $template_data['value'] = '';
                    } else {


                        /* Sako, kad to nereikia:
                         * Kad vietoje papildomo lauko tuščio string reikšmės neįdėtu reikšmės pagal nutylėjimą
                         */
                        //if ($template_data['value'] !== NULL && $field->getIType() !== 'DOC') { } else
                        $template_data['value'] = $field->getDefault();
                    }
                }

                $template_html = $this->core->returnHTML($template_data, $template[0]);
            } else {
                $template_data['lng_not_chosen'] = $this->core->lng('not_chosen');
                $template_html = $this->core->returnHTML($template_data, $template[0]);
                foreach ($template_html_data as $template_dt) {
                    foreach ($template_dt as $val => $dt)
                        $template_data[$val] = $dt;

                    $template_html .= $this->core->returnHTML($template_data, $template[1]);
                }
                $template_html .= $template[2];
            }

            //if($template_data['disabled'])
            //    $template_hidden_html .= '<input name="'.$template_data['name'].'" type="hidden" value="'.$structure_value.'" />';
        }

        // Kad DOMDocument nesisvaidytu pranešimais apie besidubliuojančius ID
        $template_html = str_replace(' id=""', '', $template_html);
        //return $template_hidden_html . $template_html;
        return $template_html;
    }

    /**
     * Dokumento įrašo kelias
     * @return string HTML'as
     */
    public function record_path()
    {
        $table_id = (!empty($_REQUEST['table_id'])) ? $_REQUEST['table_id'] : 0;
        $record_nr = (!empty($_REQUEST['record_id'])) ? $_REQUEST['record_id'] : 0;

        $table = $this->core->logic2->get_document(array('ID' => $table_id), true, null, 'ID, LABEL');

        $tpl_arr = $this->core->getTemplate('record_path');
        $html_data['title'] = $table['LABEL'] . ' - ' . $record_nr;
        $html_data['table'] = $this->record_path_table($record_nr);

        $html = $this->core->returnHTML($html_data, $tpl_arr[0]);

        return $this->core->atp_content($html);
    }

    /**
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param int $table_id
     * @return boolean
     */
    public function record_path_table($record)
    {
        if (empty($record)) {
            return false;
        }

        $recordHelper = $this->core->factory->Record();

        $tables = $this->core->logic2->get_document(null, false, null, 'ID, LABEL');
        $record = $this->core->logic2->get_record_relation($record);
        $record_path = $this->core->logic->get_record_path($record);

        $tmpl = $this->core->factory->Template();
        $tmpl_handler = 'record_path_table_file';
        $tmpl->set_file($tmpl_handler, 'record_path_table.tpl');

        $file_block = $tmpl_handler . 'file';
        $tmpl->set_block($tmpl_handler, 'file', $file_block);

        $attachment_block = $tmpl_handler . 'attachment';
        $tmpl->set_block($tmpl_handler, 'attachment', $attachment_block);

        $row_block = $tmpl_handler . 'row_block';
        $tmpl->set_block($tmpl_handler, 'row_block', $row_block);

//        $record_path = sort_by($record_path, 'CREATE_DATE', 'asc');
//        usort($record_path, function($a, $b) {
//            $ats = strtotime($a['CREATE_DATE']);
//            $bts = strtotime($b['CREATE_DATE']);
//
//            if($ats === $bts) {
//                return $a['ID'] > $b['ID'];
//            } else {
//                return $ats > $bts;
//            }
//        });
//        $record_path = sort_by($record_path, 'ID', 'asc');
        usort($record_path, function($a, $b) {
            return $a['ID'] > $b['ID'];
        });

        foreach ($record_path as $path) {
            $atp_status = $this->core->logic->find_document_status(array('ID' => $path['STATUS']));
            $status = $this->core->logic2->get_status(array('ID' => $path['DOC_STATUS']));

            if ($path['TABLE_TO_ID'] == \Atp\Document::DOCUMENT_ACCOMPANYING) {
                $_POST['record'] = $path['ID'];
                $RN = new \Atp_Post_RN($this->core);
                $ret = $RN->check_rn_state();

                if ($ret !== false) {
                    $post_state = $ret;
                }
            } else {
                $post_state = '';
            }

            // Susieti failai
            $files_info = array();
            //$file = new \Atp\File($this->core, new \Atp\File\RecordFilesHandler($this->core));
            $file = $this->core->factory->File();

            $file->SetHandler(new \Atp\File\RecordFilesHandler($this->core));
            $related_files = $file->GetRelatedFiles(array('RECORD_ID' => $path['ID']));
            if (count($related_files)) {
                foreach ($related_files as $file_id) {
                    $files_info[] = $file->GetFileInfo($file_id);
                }
            }
            // Sugeneruoti šablonai
            $file->SetHandler(new \Atp\File\GeneratedTemplatesHandler($this->core));
            $related_files = $file->GetRelatedFiles(array('RECORD_ID' => $path['ID']));
            if (count($related_files)) {
                foreach ($related_files as $file_id) {
                    $files_info[] = $file->GetFileInfo($file_id);
                }
            }

            if (count($files_info)) {
				$used_files = array();
				foreach ($files_info as $key => $file_info) {
					$url = $file->ParseFilePath($file_info->path, $file::FILE_PATH_FULL_URL);
					if (!in_array($url, $used_files)) {
						$tmpl->set_var(array(
	                        'url' => $url,
	                        'filename' => $file_info->name
	                    ));
						$used_files[] = $url;
						$tmpl->parse($file_block, 'file', $key);
					}
                }
                $tmpl->parse($attachment_block, 'attachment');
            } else {
                $tmpl->clean($attachment_block);
            }

            $worker = $this->core->factory->Record()->getWorker($path['ID']);


            $recordNrUrl = $recordUrl = 'index.php?m=5&id=' . $path['ID'];
            $recordNrUrlHtml = null;
            $avilysDocument = $recordHelper->getAvilysFor($path['ID']);
            if ($avilysDocument) {
                if ($avilysDocument->getRegistrationNo()) {
                    $path['RECORD_ID'] = $avilysDocument->getRegistrationNo();
                } else {
                    $path['RECORD_ID'] = $avilysDocument->getOid();
                }
                $recordNrUrl = $avilysUrl = $avilysDocument->getUrl();
                $recordNrUrlHtml = ' target="_blank"';
            }

            $arr = array(
                'record_url' => $recordUrl,
                'record_nr_url' => $recordNrUrl,
                'record_nr_url_html' => $recordNrUrlHtml,
                'record_avilys_url' => $avilysUrl,
                'record_id' => $path['ID'],
                'record_nr' => $path['RECORD_ID'],
                'record_table' => $tables[$path['TABLE_TO_ID']]['LABEL'],
                'record_version' => $path['RECORD_VERSION'],
                'record_person' => join(' ', array_filter(array($worker['FIRST_NAME'],
                    $worker['LAST_NAME']))),
                'record_atp_status' => $atp_status['LABEL'],
                'record_status' => $status['LABEL'],
                'record_post_status' => $post_state, //'Nusiustas :)',//$status['LABEL'],
                'record_created' => $path['CREATE_DATE'],
                'record_current_path' => ($path['ID'] === $record['ID'] ? ' class="atp-current-path"' : '')
            );
            $tmpl->set_var($arr);
            $tmpl->parse($row_block, 'row_block', true);
        }

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');
        return $this->core->atp_content($output, 2);
    }

    /**
     * Administravimas (Vartotojų teisės)
     * @return string
     */
    public function user_administration()
    {
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_COMPETENCE')) {
            return $this->core->printForbidden();
        }

        if (isset($_POST['submit_status']) && $this->core->factory->User()->getLoggedPersonDutyStatus('A_COMPETENCE') > 1) {
            $user_id = (!empty($_POST['user_admin_id'])) ? $_POST['user_admin_id'] : 0;
            $department_id = (!empty($_POST['user_department_id'])) ? $_POST['user_department_id'] : 0;


            $adminJournal = $this->core->factory->AdminJournal();
            $adminJournal->addRecord(
                $adminJournal->getSaveRecord(
                    'Administravime, vartotojų teises'
                    . ' (Skyrius: ' . $department_id . ';'
                    . ' Vartotojas: ' . $user_id . ').'
                )
            );


            $this->core->logic->edit_status();
            $params = array(
                'm' => $_GET['m'],
                'department_id' => $department_id,
                'user_id' => $user_id
            );
            header('Location: index.php?' . http_build_query($params));
            exit;
        } else {
            $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
            $department_id = (!empty($_REQUEST['department_id'])) ? $_REQUEST['department_id'] : 0;


            $adminJournal = $this->core->factory->AdminJournal();
            $adminJournal->addRecord(
                $adminJournal->getVisitRecord(
                    'Administravime, vartotojų teises.'
                    . ' (Skyrius: ' . $department_id . ';'
                    . ' Vartotojas: ' . $user_id . ')'
                )
            );
        }

        $tmpl = $this->core->factory->Template();
        $tmpl_handler = 'user_administration_file';
        $tmpl->set_file($tmpl_handler, 'user_administration.tpl');

        $submit_block = $tmpl_handler . 'submit';
        $tmpl->set_block($tmpl_handler, 'submit', $submit_block);
        $table_block = $tmpl_handler . 'table';
        $tmpl->set_block($tmpl_handler, 'table', $table_block);

        $user_data = null;
        if (!empty($user_id)) {
            $user_data = $this->core->logic->get_m_user($user_id, $department_id);
        }

        $department = $this->core->logic->get_m_department($department_id);

        $arr = array(
            'sys_message' => $this->core->get_messages(),
            'user_id' => $user_data['ID'],
            'user_name' => $user_data['SHOWS'],
            'department_id' => $department['ID'],
            'department_label' => $department['SHOWS'],
            'm' => $_GET['m'],
            'GLOBAL_SITE_URL' => $this->core->config['GLOBAL_SITE_URL'],
        );

        if (empty($user_id) === false || empty($department_id) === false) {
            $tmpl->set_var('table_user_administration', $this->table_user_administration($user_id, $department_id));
            $tmpl->parse($table_block, 'table');
        } else {
            $tmpl->clean($table_block);
        }

        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_COMPETENCE') > 1) {
            $tmpl->parse($submit_block, 'submit');
        } else {
            $tmpl->clean($submit_block);
        }

        $tmpl->set_var($arr);

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    /**
     * Kompetencijos (Administravimas)
     * @return type
     */
    public function user_competences()
    {
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_COMPETENCE')) {
            return $this->core->printForbidden();
        }

        if (isset($_POST['submit_competence']) && $this->core->factory->User()->getLoggedPersonDutyStatus('A_COMPETENCE') > 1) {
            $user_id = (!empty($_POST['user_admin_id'])) ? $_POST['user_admin_id'] : 0;
            $department_id = (!empty($_POST['user_department_id'])) ? $_POST['user_department_id'] : 0;
            $result = $this->core->logic->edit_competence();

            if ($result) {


                $adminJournal = $this->core->factory->AdminJournal();
                $adminJournal->addRecord(
                    $adminJournal->getSaveRecord(
                        'Administravime, kompetencijas'
                        . ' (Skyrius: ' . $department_id . ';'
                        . ' Vartotojas: ' . $user_id . ').'
                    )
                );
            }
        } else {
            $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
            $department_id = (!empty($_REQUEST['department_id'])) ? $_REQUEST['department_id'] : 0;


            $adminJournal = $this->core->factory->AdminJournal();
            $adminJournal->addRecord(
                $adminJournal->getVisitRecord(
                    'Administravime, vartotojų teises.'
                    . ' (Skyrius: ' . $department_id . ';'
                    . ' Vartotojas: ' . $user_id . ')'
                )
            );
        }

        $tmpl = $this->core->factory->Template();
        $tmpl_handler = 'user_competences_file';
        $tmpl->set_file($tmpl_handler, 'user_competences.tpl');

        $submit_block = $tmpl_handler . 'submit';
        $tmpl->set_block($tmpl_handler, 'submit', $submit_block);
        $table_block = $tmpl_handler . 'table';
        $tmpl->set_block($tmpl_handler, 'table', $table_block);

        $user_data = null;
        if (!empty($user_id))
            $user_data = $this->core->logic->get_m_user($user_id, $department_id);

        $department = $this->core->logic->get_m_department($department_id);

        $arr = array(
            'sys_message' => $this->core->get_messages(),
            'user_id' => $user_data['ID'],
            'user_name' => $user_data['SHOWS'],
            'department_id' => $department['ID'],
            'department_label' => $department['SHOWS'],
            'm' => $_GET['m'],
            'GLOBAL_SITE_URL' => $this->core->config['GLOBAL_SITE_URL'],
        );
        if (empty($user_id) === false || empty($department_id) === false) {
            $tmpl->set_var('table_user_competence', $this->table_user_competence($user_id, $department_id));
            $tmpl->parse($table_block, 'table');
        } else {
            $tmpl->clean($table_block);
        }
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_COMPETENCE') > 1)
            $tmpl->parse($submit_block, 'submit');
        else {
            $tmpl->clean($submit_block);
        }

        $tmpl->set_var($arr);

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    /**
     * Dokumentų teisės (Administravimas)
     * @return type
     */
    public function user_rights()
    {
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_COMPETENCE')) {
            return $this->core->printForbidden();
        }

        if (isset($_POST['submit_privileges']) && $this->core->factory->User()->getLoggedPersonDutyStatus('A_COMPETENCE') > 1) {
            $arg = array(
                'department' => (int) $_POST['user_department_id'],
                'users' => (empty($_POST['user_admin_id']) ? array() : (array) $_POST['user_admin_id']),
                'privileges' => (empty($_POST['user_privileges']) ? array() : $_POST['user_privileges'])
            );

            $result = $this->core->logic->edit_rights($arg['department'], $arg['users'], $arg['privileges']);
            if ($result) {

                $adminJournal = $this->core->factory->AdminJournal();
                $adminJournal->addRecord(
                    $adminJournal->getSaveRecord(
                        'Administravime, dokumentų teises'
                        . ' (Skyrius: ' . $_POST['user_department_id'] . ';'
                        . ' Vartotojas: ' . $_POST['user_admin_id'] . ').'
                    )
                );

                $this->core->set_message('success', $this->core->lng('success_edited'));
            }

            $query = array_filter(array(
                'm' => 14,
                'department_id' => $arg['department'],
                'user_id' => $_POST['user_admin_id']
            ));

            header('Location: index.php?' . http_build_query($query));
            exit;
        } else {
            $user_id = (!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;
            $department_id = (!empty($_REQUEST['department_id'])) ? $_REQUEST['department_id'] : 0;


            $adminJournal = $this->core->factory->AdminJournal();
            $adminJournal->addRecord(
                $adminJournal->getVisitRecord(
                    'Administravime, dokumentų teises'
                    . ' (Skyrius: ' . $department_id . ';'
                    . ' Vartotojas: ' . $user_id . ').'
                )
            );
        }

        $tmpl = $this->core->factory->Template();
        $tmpl_handler = 'user_rights_file';
        $tmpl->set_file($tmpl_handler, 'user_rights.tpl');

        $Marker_24 = $tmpl_handler . '_Marker_24';
        $tmpl->set_block($tmpl_handler, 'Marker_24', $Marker_24);
        $submit_block = $tmpl_handler . 'submit';
        $tmpl->set_block($tmpl_handler, 'submit', $submit_block);
        $table_block = $tmpl_handler . 'table';
        $tmpl->set_block($tmpl_handler, 'table', $table_block);

        $user_data = null;
        if (!empty($user_id))
            $user_data = $this->core->logic->get_m_user($user_id, $department_id);

        $department = $this->core->logic->get_m_department($department_id);

        $tables = $this->core->logic2->get_document();


        //TODO: kas čia?
        $default_user = false;
        if ($user_id === 'x') {//default user
            $user_id = 0;
            $default_user = true;
        }

        if (((!empty($user_id) || $default_user) && !empty($department_id)) || !empty($department_id)) {
            $priveleges_data = array('user_id' => $user_id, 'department_id' => $department_id);
            foreach ($tables as &$table) {
                $priveleges_data['table_id'] = $table['ID'];
                $arr = array(
                    'label' => $table['LABEL'],
                    'table_id' => $table['ID'],
                    'view_record' => $this->core->logic->check_privileges('VIEW_RECORD', $priveleges_data) ? ' checked="checked"' : '',
                    'edit_record' => $this->core->logic->check_privileges('EDIT_RECORD', $priveleges_data) ? ' checked="checked"' : '',
                    'delete_record' => $this->core->logic->check_privileges('DELETE_RECORD', $priveleges_data) ? ' checked="checked"' : '',
                    'investigate_record' => $this->core->logic->check_privileges('INVESTIGATE_RECORD', $priveleges_data) ? ' checked="checked"' : '',
                    'examinate_record' => $this->core->logic->check_privileges('EXAMINATE_RECORD', $priveleges_data) ? ' checked="checked"' : '',
                    'end_record' => $this->core->logic->check_privileges('END_RECORD', $priveleges_data) ? ' checked="checked"' : ''
                );
                $tmpl->set_var($arr);
                $tmpl->parse($Marker_24, 'Marker_24', true);
            }

            if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_COMPETENCE') > 1) {
                $tmpl->parse($submit_block, 'submit');
            } else {
                $tmpl->clean($submit_block);
            }

            $tmpl->parse($table_block, 'table');
        } else {
            $tmpl->clean($table_block);
        }


        $arr = array(
            'sys_message' => $this->core->get_messages(),
            'user_id' => $user_data['ID'],
            'user_name' => $user_data['SHOWS'],
            'department_id' => $department['ID'],
            'department_label' => $department['SHOWS'],
            'm' => $_GET['m'],
            'GLOBAL_SITE_URL' => $this->core->config['GLOBAL_SITE_URL'],
        );

        $tmpl->set_var($arr);

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    /**
     * Dokumentai (Administravimas) puslapio dokumentų lentelė
     * @return string HTML'as
     */
    public function table_user_administration($user_id, $department_id)
    {
        $html_manage = '';

        $tpl_arr = $this->core->getTemplate('table');
        $user_status = $this->core->factory->User()->getUserStatus($user_id, $department_id);
        $columns = array(
            'right' => '',
            /* 'none' => $this->core->lng('atp_user_administration_none'),
              'see' => $this->core->lng('atp_user_administration_see'), */
            'edit' => $this->core->lng('atp_user_administration_edit')
        );
        $rows = array(
            'view_table' => 'A_TABLE',
            'view_relation' => 'A_RELATION',
            'view_status' => 'A_STATUS',
            'view_document' => 'A_DOCUMENT',
            'view_classification' => 'A_CLASSIFICATION',
            'view_act' => 'A_ACT',
            'view_deed' => 'A_DEED',
            'view_competence' => 'A_COMPETENCE',
            'view_clause' => 'A_CLAUSE'/* ,
              'view_department' => 'A_DEPARTMENT' */);

        // table
        $clause = array();
        $clause['table_class'] = 'user_administration';
        $html_manage .= $this->core->returnHTML($clause, $tpl_arr[0]);

        // header
        $header = array('row_html' => '');
        $header['row_class'] = 'table-row table-header';
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);

        foreach ($columns as $col_name => $col) {
            // cell
            $cell = array('cell_html' => '');
            $cell['cell_class'] = 'table-cell cell-' . $col_name;
            $cell['cell_content'] = $col;
            $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
        }
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

        foreach ($rows as $key => $row) {
            // row
            $row_var = array('row_html' => '', 'row_class' => '');
            $html_manage .= $this->core->returnHTML($row_var, $tpl_arr[1]);

            foreach ($columns as $col_name => $col) {
                // cell
                $cell = array('cell_html' => '', 'cell_content' => '');
                $cell['cell_class'] = 'table-cell cell-' . $col_name;
                switch ($col_name) {
                    case 'right':
                        //$clause[$col_name] = $this->core->lng('atp_user_administration_right_' . $row);
                        $clause[$col_name] = $this->core->lng('menu_' . $row);
                        break;
                    /* case 'none':
                      $clause[$col_name] = '<input type="radio" name="' . $key . '" value="0"' . ($user_status[$row] === '0' || $user_status === false ? ' checked="checked"' : '') . '/>';
                      break;
                      case 'see':
                      $clause[$col_name] = '<input type="radio" name="' . $key . '" value="1"' . ($user_status[$row] === '1' ? ' checked="checked"' : '') . '/>';
                      break;
                     */case 'edit':
                        //$clause[$col_name] = '<input type="radio" name="' . $key . '" value="2"' . ($user_status[$row] === '2' ? ' checked="checked"' : '') . '/>';
                        $clause[$col_name] = '<input type="checkbox" name="' . $key . '" value="2"' . ($user_status[$row] === '2' ? ' checked="checked"' : '') . '/>';
                        break;
                }

                $cell['cell_content'] .= $clause[$col_name];
                $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
            }

            $html_manage .= $this->core->returnHTML($row_var, $tpl_arr[3]);
        }
        $html_manage .= $this->core->returnHTML($clause, $tpl_arr[4]);

        return $html_manage;
    }

    /**
     * Administravimas (Administravimas) puslapių teisių lentelė
     * @return string HTML'as
     */
    public function table_list()
    {

        $html_manage = '';
        $tables = $this->core->logic2->get_document(null, false, 'PARENT_ID, LABEL');

        $new_tables = array();
        foreach ($tables as $table) {
            $new_tables[$table['ID']] = $table;

            $new_tables[$table['ID']]['ACTIONS'] = array();
            if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_TABLE') > 1) {
                $new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button edit" id="atp-table-id-' . $table['ID'] . '"><span>' . $this->core->lng('edit') . '</span></div>';
                $new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button remove" id="atp-table-id-' . $table['ID'] . '"><span>Trinti</span></div>';
            }
            $new_tables[$table['ID']]['ACTIONS'] = join(' ', $new_tables[$table['ID']]['ACTIONS']);

            $new_tables[$table['ID']]['LEVEL'] = $this->core->logic->get_table_level($tables, $table['ID']);

            if ($this->core->logic->table_has_children($tables, $table['ID'], 'DOCUMENTS')) {
                $toggle_class = 'collapse';
            } else {
                $toggle_class = 'none';
            }

            $new_tables[$table['ID']]['LABEL'] = '<div class="cell-LABEL-container"><div class="' . $toggle_class . ' atp-table-list-toggle"></div><span>' . $new_tables[$table['ID']]['LABEL'] . '</span></div>';
        }

        $tables = &$new_tables;

        $tables = $this->core->logic->sort_children_to_parent($tables, 'ID', 'PARENT_ID');

        $tpl_arr = $this->core->getTemplate('table2');
        $columns = array(
            'LABEL' => $this->core->lng('atp_table_label'),
            'CREATE_DATE' => $this->core->lng('atp_table_create_date'),
            'UPDATE_DATE' => $this->core->lng('atp_table_update_date')
        );
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_TABLE') > 1) {
            $columns['ACTIONS'] = $this->core->lng('atp_table_actions');
        }

        // table
        $table = array();
        $table['table_class'] = 'div';
        $html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

        // header
        $header = array('row_html' => '');
        $header['row_class'] = 'div-row div-header';
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);

        foreach ($columns as $col_name => $col) {
            // cell
            $cell = array('cell_html' => '');
            $cell['cell_class'] = 'div-cell cell-' . $col_name;
            $cell['cell_content'] = $col;
            $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
        }
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

        foreach ($tables as $key => $table) {
            // row
            $row = array();
            $row['row_html'] = 'id="atp-table-list-item-id-' . $table['ID'] . '"';
            $row['row_class'][] = 'div-row';
            if ($table['LEVEL']) {
                $row['row_class'][] = 'child';
            }
            $row['row_class'][] = 'level-' . $table['LEVEL'];
            $row['row_class'][] = 'child-of-' . $table['PARENT_ID'];
            $row['row_class'] = join(' ', $row['row_class']);
            $html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

            foreach ($columns as $col_name => $col) {
                // cell
                $cell = array('cell_html' => '', 'cell_content' => '');
                $cell['cell_class'] = 'div-cell cell-' . $col_name;
                $cell['cell_content'] .= $table[$col_name];
                $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
            }

            $html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
        }

        $html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

        return $html_manage;
    }

    /**
     * <b>(Dokumentų šablonas)</b> Spausdina dokumento kinatmuosius ir/arba garžina rastus laukus
     * @author Justinas Malūkas
     * @param int $id Įrašo ID
     * @param string $type Įrašo tipas
     * @param array $data Papildomi duomenys
     * @param object $tmpl [optional] Šablonų valdymo objektas
     * @param int $depth Lauko gylis
     * @param array $all_fields Visis rasti laukai
     * @param array $nr ID masyvas unikaliam kintamųjų pavadinimų generavimui
     * @return array Visi rasti laukai
     */
    public function document_fields($id, $type, $data, &$tmpl = null, $depth = 0, &$all_fields = array(), &$nr = array())
    {
        if (in_array($type, array_keys($this->core->_prefix->item), true) === false) {
            trigger_error('Unknown item type', E_USER_WARNING);
            return false;
        }
        if (empty($data['prefix']) && empty($data['table_name']) === false) {
            $data['prefix'] = $this->parse_document_template_variable_prefix_from_string($data['table_name']);
        }

        if (empty($tmpl) === false) {
            if (isset($data['multiplier']))
                $data['multiplier'] = (int) $data['multiplier'];
            else {
                $data['multiplier'] = 6;
            }
        }

        // Gauna įrašo laukus
        /* if ($type === 'DEED') {
          // TODO: Veikų laukai vienodi
          $fields = array(
          array('NAME' => 'DCOL_1', 'LABEL' => 'Punktas', 'TYPE' => '7'),
          array('NAME' => 'DCOL_2', 'LABEL' => 'Tekstas', 'TYPE' => '8')
          );
          } else */
        if ($type === 'ACT') {
            //$info = $this->core->logic->get_info_fields(array('ID' => $arr['ID'], 'TYPE' => 'ACT'));
            $info = $this->core->logic->get_info_fields(array('TYPE' => 'ACT'));
            foreach ($info['header'] as $key => $arr) {
                $fields[] = array('NAME' => 'AICOL_' . $key, 'LABEL' => $arr, 'TYPE' => '7',
                    'SORT' => '2');
            }
            $fields = array_merge($fields, $this->core->logic2->get_fields(array(
                    'ITYPE' => $type), false, 'ORD ASC'));
        } else
        if ($type === 'CLAUSE') {
            //$info = $this->core->logic->get_info_fields(array('ID' => $arr['ID'], 'TYPE' => 'CLAUSE'));
            $info = $this->core->logic->get_info_fields(array('TYPE' => 'CLAUSE'));
            foreach ($info['header'] as $key => $arr) {
                $fields[] = array('NAME' => 'SICOL_' . $key, 'LABEL' => $arr, 'TYPE' => '7',
                    'SORT' => '2');
            }
            $fields = array_merge($fields, $this->core->logic2->get_fields(array(
                    'ITYPE' => $type), false, 'ORD ASC'));
        } else {
            $fields = $this->core->logic->get_extra_fields($id, $type);
        }

        if ($type === 'WS') {
            $fields2 = array($this->core->logic2->get_fields(array('ID' => $id, 'ITYPE' => $type), false));
        } else {
            #$fields = $this->core->logic->get_extra_fields($id, $type); }
            $fields2 = array();
        }

        $fields = array_merge((array) $fields, (array) $fields2);

        if ($type === 'DOC') {
            $new_fields = $this->core->logic->record_worker_info_to_fields(null);
            $fields = array_merge($fields, $new_fields);
            $new_fields = $this->core->logic->record_post_info_to_fields(null);
            $fields = array_merge($fields, $new_fields);
            $new_fields = $this->core->logic->record_penalty_info_to_fields(null);
            $fields = array_merge($fields, $new_fields);
        }

        foreach ($fields as &$field) {
            if (empty($field) === true)
                continue;

            // Suformuoja kintamojo pavadinimą
            $expl = explode('_', $field['NAME']);
            $expl[1] = end($expl);
            $expl[0] = str_replace('COL', '', $expl[0]);
            if (empty($expl[0]) === true)
                $expl[0] = 'T'; //$this->core->_prefix->item['DOC'];
            $nr[$depth] = $expl[0] . $expl[1];
            $name = $data['prefix'] . join('', $nr);

            // Kintamasis
            $field['VAR_NAME'] = '${' . $name . '}';

            // Spausdinama kintamojo informacija
            $this->document_fields_field_parser(array(
                'NAME' => str_repeat('&nbsp;', $data['multiplier'] * $depth) . $field['VAR_NAME'],
                'LABEL' => $field['LABEL'],
                'TYPE' => $data['field_type'][$field['TYPE']]['LABEL'],
                ), $data, $tmpl);

            $field['depth'] = $depth;


            if (isset($data['options']) && $data['options'] === true) {
                if ($data['options_value_use_name']) {
                    $value = $name;
                } else {
                    $value = $field['ID'];
                }

                $all_fields[] = '<option value="' . $value . '" for="' . $field['ID'] . '" class="sort-' . $field['SORT'] . ' type-' . $field['TYPE'] . ' level' . (int) $depth . '">' . (str_repeat('&nbsp;', $data['multiplier'] * $depth) . $field['LABEL']) . '</option>';
            }

            // Tikrinamas kintamojo tipas nustatyti tolimesnius veiksmus
            // Klasifikatorius kintamasis
            if ($field['TYPE'] === '18') {
                // TODO: gauna tik pirmo lygio klasifikatorius, gal reikia visų?
                //$items = $this->core->logic->get_classification(null, $field['SOURCE']);
                $items = $this->core->logic2->get_classificator(array('PARENT_ID' => $field['SOURCE']));
                foreach ($items as &$item) {
                    // Spausdina klasifikatoriaus pavadinimą
                    $this->document_fields_field_parser(array(
                        'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['LABEL']
                        ), $data, $tmpl);

                    if (isset($data['options']) && $data['options'] === true) {
                        $all_fields[] = '<option value="-1" disabled="disabled" class="parent level' . (int) ($depth + 1) . '">' . (str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['LABEL']) . '</option>';
                    }
                    // Gilesnių laukų spausdinimui
                    $nr[$depth + 1] = $this->core->_prefix->item['CLASS'] . $item['ID'];
                    $this->document_fields($item['ID'], 'CLASS', $data, $tmpl, $depth + 2, $all_fields, $nr);
                    unset($nr[$depth + 1]);
                }
            } else
            // Straipsnio kintamasis
            if ($field['SORT'] === '6') {
                /* $items = $this->core->logic2->get_clause();
                  foreach ($items as &$item) {
                  // Spausdina straipsnio pavadinimą
                  $this->document_fields_field_parser(array(
                  'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['NAME']
                  ), $data, $tmpl);

                  if ($data['options'] === true) {
                  $all_fields[] = '<option value="-1" disabled="disabled" class="parent level' . (int) ($depth + 1) . '">' . (str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['NAME']) . '</option>';
                  }
                  // Gilesnių laukų spausdinimui
                  $nr[$depth + 1] = $this->core->_prefix->item['CLAUSE'] . $item['ID'];
                  $this->document_fields($item['ID'], 'CLAUSE', $data, $tmpl, $depth + 2, $all_fields, $nr);
                  unset($nr[$depth + 1]);
                  } */
                $this->document_fields_field_parser(array(
                    'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . 'Straipsnis'
                    ), $data, $tmpl);
                $this->document_fields($item['ID'], 'CLAUSE', $data, $tmpl, $depth + 2, $all_fields, $nr);
            } else
            // Veika, teisės aktas ir straipsniai
            if ($field['TYPE'] === '23') {
                // Gilesnių veikos laukų spausdinimui
                //$nr[$depth + 2] = $this->core->_prefix->item['DEED'];
                //$this->document_fields(null, 'DEED', $data, $tmpl, $depth + 1, $all_fields, $nr);
                // Gilesnių teisės akto laukų spausdinimui
                $this->document_fields_field_parser(array(
                    'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . 'Teisės akto punktas',
                    'LABEL' => '',
                    'TYPE' => '',
                    ), $data, $tmpl);
                $this->document_fields(null, 'ACT', $data, $tmpl, $depth + 2, $all_fields, $nr);
                // Gilesnių teisės akto laukų spausdinimui
                $this->document_fields_field_parser(array(
                    'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . 'Straipsnis',
                    'LABEL' => '',
                    'TYPE' => '',
                    ), $data, $tmpl);

                $this->document_fields(null, 'CLAUSE', $data, $tmpl, $depth + 2, $all_fields, $nr);
                //unset($nr[$depth + 2]);
                /*

                  // Surenka visus straipsnius priskirtus veikoms
                  $extends = $this->core->logic->get_deed_extend_data(array('A.ITYPE = "CLAUSE"'));
                  // Surenka unikalius straipsnius
                  $clauses = array();
                  foreach ($extends as &$extend) {
                  if ($extend['ITYPE'] !== 'CLAUSE')
                  continue;
                  $clauses[$extend['ITEM_ID']] = $extend;
                  }
                  $extends = &$clauses;

                  foreach ($extends as &$item) {
                  // Spausdina straipsnio pavadinimą
                  $this->document_fields_field_parser(array(
                  'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['INAME']
                  ), $data, $tmpl);

                  if ($data['options'] === true) {
                  $all_fields[] = '<option value="-1" disabled="disabled" class="parent level' . (int) ($depth + 1) . '">' . (str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . $item['INAME']) . '</option>';
                  }
                  // Gilesnių laukų spausdinimui
                  $nr[$depth + 1] = $this->core->_prefix->item['DEED'] . $this->core->_prefix->item['CLAUSE'] . $item['ITEM_ID'];
                  $this->document_fields($item['ITEM_ID'], 'CLAUSE', $data, $tmpl, $depth + 2, $all_fields, $nr);
                  unset($nr[$depth + 1]);
                  }
                 */
                //unset($nr[$depth]);
            }
            // Web-servisas
            if (isset($field['WS']) && $field['WS'] === '1') {
                $this->document_fields_field_parser(array(
                    'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 1)) . 'Web-servisai',
                    'LABEL' => '',
                    'TYPE' => '',
                    ), $data, $tmpl);

                $ws = $this->core->logic2->get_ws();
                $relations = $this->core->logic2->get_webservices_fields(array('FIELD_ID' => $field['ID']));
                $fields_struct = $this->core->logic2->get_fields(array('ITYPE' => 'WS',
                    'ITEM_ID' => $field['ID']), false, '`ORD` ASC');

                foreach ($relations as &$relation) {
                    $info = $ws->get_info($relation['WS_NAME']);
                    if (empty($info))
                        continue;

                    $this->document_fields_field_parser(array(
                        'NAME' => str_repeat('&nbsp;', $data['multiplier'] * ($depth + 2)) . $info['label'],
                        'LABEL' => '',
                        'TYPE' => '',
                        ), $data, $tmpl);

                    $nr[$depth + 1] = 'WS' . $info['name'];

                    $ws_fields = $this->core->logic2->get_webservices(array('WS_NAME' => $relation['WS_NAME'],
                        'PARENT_FIELD_ID' => $field['ID']));
                    foreach ($ws_fields as &$ws_field) {
                        $arr = &$fields_struct[$ws_field['FIELD_ID']];
                        if (empty($arr))
                            continue;
                        $this->document_fields($arr['ID'], 'WS', $data, $tmpl, $depth + 3, $all_fields, $nr);
                    }

                    unset($nr[$depth + 1]);
                }
            }

            unset($nr[$depth]);

            if (isset($data['options']) === false || $data['options'] !== true)
                $all_fields[] = $field;
        }

        if ($type === 'DOC') {

        }
        return $all_fields;
    }

    /**
     * <b>(Dokumentų šablonas)</b> Spausdina lauko informaciją
     * @param array $data
     * @param array $extradata
     * @param object $tmpl
     * @return boolean
     */
    private function document_fields_field_parser($data, &$extradata, &$tmpl)
    {
        if (empty($tmpl) === true)
            return false;

        $default = array(
            'NAME' => null,
            'LABEL' => null,
            'TYPE' => null
        );
        $data = array_merge($default, (array) $data);

        $tmpl->set_var(array(
            'field_varname' => $data['NAME'],
            'field_label' => $data['LABEL'],
            'field_type' => $data['TYPE']));

        $tmpl->parse($extradata['var_row_block'], 'var_row_block', true);
        return true;
    }

    /**
     * Iš dokumento pavadinimo sugeneruoja dokumento šablono kintamojo priešdėlį
     * @param string $str Dokumento pavadinimas
     * @return string Dokumento priešdelis
     */
    public function parse_document_template_variable_prefix_from_string($str)
    {
        return strtoupper(substr(preg_replace('/[^\da-z]/i', '', $str), 0, 4));
    }

    /**
     * Straipsnių puslapis
     * @return string
     */
    public function clause()
    {
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_CLAUSE')) {
            return $this->core->printForbidden();
        }

        $tmpl = $this->core->factory->Template();
        $tmpl_handler = 'clause_file';
        $tmpl->set_file($tmpl_handler, 'clause.tpl');

        $new_block = $tmpl_handler . 'new';
        $tmpl->set_block($tmpl_handler, 'new', $new_block);

        $tmpl->set_var(array(
            'GLOBAL_SITE_URL' => $this->core->config['GLOBAL_SITE_URL'],
            'sys_message' => $this->core->get_messages(),
            'clause_list' => $this->table_clauses()
        ));

        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_CLAUSE') > 1)
            $tmpl->parse($new_block, 'new');
        else {
            $tmpl->clean($new_block);
        }

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    public function table_clauses()
    {
        $html_manage = '';

        // Surenka klasifikatorius
        $tables = $this->core->logic2->get_clause();

        // Surenkami papildomi duomenys apie įrašus
        foreach ($tables as $table) {
            $new_tables[$table['ID']] = $table;

            // Surenka galimus veiksmus
            $new_tables[$table['ID']]['ACTIONS'] = array();
            $new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="expand_collapse_block">
					<div class="expand_collapse_img_block">
						<img style="cursor: pointer;" src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/atp/expand_doc_action.png" width="29" height="29">
					</div>
					<div class="actions_block" style="display: none;">
						<img style="display: block;" src="' . $this->core->config['GLOBAL_SITE_URL'] . 'images/actions/top_img_actions_buttons.png" width="132" height="8">
						<div class="actions_buttons">';
            $new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button addChild" onClick="window.location=\'index.php?m=10&id=' . $table['ID'] . '&action=sub\';"><span>Pridėti vaiką</span></div>';
            $new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button remove" id="atp-table-id-' . $table['ID'] . '"><span>Trinti</span></div>';
//			$new_tables[$table['ID']]['ACTIONS'][] = '<div class="atp-html-button addFields"><span>Sukurti laukus</span></div>';
            $new_tables[$table['ID']]['ACTIONS'][] = '
						</div>
					</div>
				</div>';
            $new_tables[$table['ID']]['ACTIONS'][] = '
				<div class="action_label">' .
                /* <div class="atp-html-button edit" id="atp-table-id-' . $table['ID'] . '" onClick="window.location=\'index.php?m=11&clause_id=' . $table['ID'] . '&edit=1\';"><span>' . $this->core->lng('edit') . '</span></div> */
                '<a class="atp-html-button edit dialog" href="index.php?m=10&id=' . $table['ID'] . '&action=editform">' . $this->core->lng('edit') . '</a>
				</div>';
            $new_tables[$table['ID']]['ACTIONS'] = join(' ', $new_tables[$table['ID']]['ACTIONS']);

            // Gaunamas įrašo lygis
            $new_tables[$table['ID']]['LEVEL'] = $this->core->logic->get_table_level($tables, $table['ID']);

            // Nurodomas išskleidimo stulpelis, jei laukas turi vaikų
            if ($this->core->logic->table_has_children($tables, $table['ID'], 'CLAUSES')) {
                $new_tables[$table['ID']]['TOGGLE'] = '<div class="collapse atp-table-list-toggle"></div>';
                $toggle_class = 'collapse';
            } else {
                $toggle_class = 'none';
            }

            $new_tables[$table['ID']]['CLAUSE'] = '<div class="cell-CLAUSE-container"><div class="' . $toggle_class . ' atp-table-list-toggle"></div><span>' . $new_tables[$table['ID']]['CLAUSE'] . ' str.' . '</span></div>';
        }
        $tables = &$new_tables;

        // Įrašai surikiuojami pagal hierarchiją (tevas -> vaikai, tevas -> vaikai)
        $tables = $this->core->logic->sort_children_to_parent($tables, 'ID', 'PARENT_ID');

        $tpl_arr = $this->core->getTemplate('table2');

        // Nurodomi išvedami lentelės stulpeliai
        $columns = array(
            //'TOGGLE' => $this->core->lng('atp_table_toggle'),
            'CLAUSE' => $this->core->lng('atp_clause_clause'),
            'SECTION' => $this->core->lng('atp_clause_section'),
            'PARAGRAPH' => $this->core->lng('atp_clause_paragraph'),
            'SUBPARAGRAPH' => $this->core->lng('atp_clause_subparagraph'),
            'LABEL' => $this->core->lng('atp_clause_label'),
            'PENALTY_MIN' => $this->core->lng('atp_clause_penalty_min'),
            'PENALTY_MAX' => $this->core->lng('atp_clause_penalty_max'),
            'NOTICE' => $this->core->lng('atp_clause_notice'),
            'NOTICE_LABEL' => $this->core->lng('atp_clause_notice_label'),
            'TERM' => $this->core->lng('atp_clause_term'),
            'INVESTIGATION_CNT' => $this->core->lng('atp_clause_investigation_cnt'),
            'EXAMINATION_CNT' => $this->core->lng('atp_clause_examination_cnt')
        );
        // Tikrinama teisė į veiksmų stulpelio matymą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_CLAUSE') > 1)
            $columns['ACTIONS'] = $this->core->lng('atp_clause_actions');

        // table
        $table = array();
        $table['table_class'] = 'div';
        $html_manage .= $this->core->returnHTML($table, $tpl_arr[0]);

        // Lentelės antraštė
        $header = array('row_html' => '');
        $header['row_class'] = 'div-row div-header';
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[1]);
        foreach ($columns as $col_name => $col) {
            // cell
            $cell = array('cell_html' => '');
            $cell['cell_class'] = 'div-cell cell-' . $col_name;
            $cell['cell_content'] = $col;
            $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
        }
        $html_manage .= $this->core->returnHTML($header, $tpl_arr[3]);

        // Lentelės įrašų eilutės
        foreach ($tables as $key => $table) {
            // Eilutės duomenys
            $row = array();
            $row['row_html'] = 'id="atp-table-list-item-id-' . $table['ID'] . '"';
            $row['row_class'][] = 'div-row';
            if ($table['LEVEL'])
                $row['row_class'][] = 'child';
            $row['row_class'][] = 'level-' . $table['LEVEL'];
            $row['row_class'][] = 'child-of-' . $table['PARENT_ID'];
            $row['row_class'] = join(' ', $row['row_class']);
            $html_manage .= $this->core->returnHTML($row, $tpl_arr[1]);

            $investigation_cnt = 0;
            $examination_cnt = 0;
            /* $clauses_competence = $this->core->logic->get_clause_competence(array('CLAUSE_ID' => $table['ID']));
              foreach ((array) $clauses_competence as $competence) {
              if ($competence['TYPE'] == 1)
              $investigation_cnt++;
              elseif ($competence['TYPE'] == 2)
              $examination_cnt++;
              } */
            $table['INVESTIGATION_CNT'] = $investigation_cnt;
            $table['EXAMINATION_CNT'] = $examination_cnt;

            // Eilutės stulpeiai
            foreach ($columns as $col_name => $col) {
                // Stulpelio duomenys
                $cell = array('cell_html' => '', 'cell_content' => '');
                $cell['cell_class'] = 'div-cell cell-' . $col_name;
                switch ($col_name) {
                    //	case 'CLAUSE':
                    //		//$table[$col_name] = $table[$col_name] . ' str.';
                    //		$table[$col_name] = '<div class="cell-LABEL-container"><div class="' . $toggle_class . ' atp-table-list-toggle"></div><span>' . $table[$col_name] . ' str.' . '</span></div>';
                    //		break;
                    case 'SECTION':
                        $table[$col_name] = (!empty($table[$col_name]) ? $table[$col_name] . ' d.' : '');
                        break;
                    case 'PARAGRAPH':
                        $table[$col_name] = (!empty($table['PARAGRAPH']) ? $table['PARAGRAPH'] . ' p.' : '');
                        break;
                    case 'SUBPARAGRAPH':
                        $table[$col_name] = (!empty($table['SUBPARAGRAPH']) ? $table['SUBPARAGRAPH'] : '');
                        break;
                    case 'NOTICE':
                        $table[$col_name] = ($table['NOTICE'] ? 'Galima skirti' : 'Negalima skirti');
                        break;
                }

                $cell['cell_content'] .= $table[$col_name];
                $html_manage .= $this->core->returnHTML($cell, $tpl_arr[2]);
            }

            $html_manage .= $this->core->returnHTML($row, $tpl_arr[3]);
        }

        $html_manage .= $this->core->returnHTML($table, $tpl_arr[4]);

        return $html_manage;
    }
    /*
      public function departments() {
      if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_DEPARTMENT'))
      return false;

      if (isset($_GET['action']) && $this->core->factory->User()->getLoggedPersonDutyStatus('A_DEPARTMENT') > 1)
      $this->core->logic->edit_department();

      $tpl_arr = $this->core->getTemplate('departments');

      $html_data['sys_message'] = $this->core->get_messages();
      $html_manage = $this->core->returnHTML($html_data, $tpl_arr[0]);

      $m_departments[490] = $this->core->logic->get_m_department_list(490);
      $m_departments[189] = $this->core->logic->get_m_department_list(189);
      $m_departments = array_merge($m_departments[490], $m_departments[189]);

      $cnt = 0;
      foreach ($m_departments as $m_department) {
      $department = $this->core->logic->get_department($m_department['ID'], true);
      $department_action = (!empty($department) ? 'remove' : 'add');
      $department_class = (!empty($department) ? 'selected' : 'select');
      $html_manage .= $this->core->returnHTML(array('cnt' => ++$cnt, 'department_link' => 'index.php?m=10&action=' . $department_action . '&main_structure_id=' . $m_department['ID'], 'department_class' => 'atp-department-' . $department_class, 'department_level' => str_repeat('----', $m_department['LEVEL']), 'department_label' => $m_department['SHOWS']), $tpl_arr[1]);
      }
      $html_manage .= $tpl_arr[2];

      return $this->core->atp_content($html_manage);
      } */

    /**
     * Teisės akto redagavimo forma
     * @author Justinas Malūkas
     * @param boolen $ajax Nurodo ar formuoti pilną puslapio HTML'ą
     * @return string
     */
    public function act_editform($ajax = false)
    {
        $tmpl = $this->core->factory->Template();
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_ACT')) {
            return $this->core->printForbidden();
        }


        if ($this->core->content_type !== 2) {
            $this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
            $this->core->_load_files['js'][] = 'jquery.validationEngine.js';
            $this->core->_load_files['css'][] = 'validationEngine.jquery.css';
        }

        $tmpl_handler = 'act_editform_file';
        $tmpl->set_file($tmpl_handler, 'act_editform.tpl');

        // Papildomo lauko blokas
        $field_block = $tmpl_handler . 'field';
        $tmpl->set_block($tmpl_handler, 'field', $field_block);
        // Formos blokas
        $form_block = $tmpl_handler . 'form';
        $tmpl->set_block($tmpl_handler, 'form', $form_block);

        $vars = array();
        $m = (int) $this->core->extractVariable('m');

        $id = (int) $this->core->extractVariable('id');
        $parent_id = (int) $this->core->extractVariable('parent');

        $vars['sys_message'] = $this->core->get_messages();
        $vars['save'] = 'Išsaugoti';

        $data = array();
        if (empty($id) === false) {
            $data = $this->core->logic2->get_act(array('ID' => $id));
        }

        //
        if (empty($data) === true) {
            $vars['url'] = 'index.php?m=' . $m . '&parent=' . $parent_id . '&action=save';
            //$vars['title'] = 'Naujas teisės aktas';
            $vars['title'] = 'Naujas punktas';
            $vars['act_id'] = $vars['act_name'] = $vars['number'] = $vars['name'] = $vars['valid_from'] = $vars['valid_till'] = '';
        } else {
            $vars['url'] = 'index.php?m=' . $m . '&id=' . $id . '&action=save';
            //$vars['title'] = 'Teisės aktas: ' . $data['LABEL'];
            $vars['title'] = 'Punktas: ' . $data['LABEL'];

            //$vars['name'] = htmlspecialchars($data['LABEL']);
            $vars['number'] = $data['NR'];
            $vars['valid_from'] = $data['VALID_FROM'];
            $vars['valid_till'] = $data['VALID_TILL'];

            // Klasifikatoriaus (Teisės akto) laukas
            $classificator = $this->core->logic2->get_classificator(array('ID' => $data['CLASSIFICATOR']));
            $vars['act_id'] = $classificator['ID'];
            $vars['act_name'] = htmlspecialchars($classificator['LABEL']);


            $adminJournal = $this->core->factory->AdminJournal();
            $adminJournal->addRecord(
                $adminJournal->getVisitRecord(
                    'Teisės akto "' . $data['LABEL'] . '" redagavimo formą.'
                )
            );
        }


        // Papildomi laukai
        $fields = $this->core->logic2->get_act_default_fields_structure();
        $fields = sort_by($fields, 'ORD', 'asc');
        // Laukų reikšmės
        $fields_values = $this->core->logic2->get_fields(array('ITYPE' => 'ACT',
            'ITEM_ID' => $data['ID']));

        foreach ($fields as &$field) {
            $info = &$fields_values[$field['ID']];
            $tmpl->set_var(array(
                'id' => $field['ID'],
                'name' => $field['LABEL'],
                'value' => htmlspecialchars($info['DEFAULT'])
            ));
            $tmpl->parse($field_block, 'field', true);
        }


        $vars['atp_act_number'] = $this->core->lng('atp_act_number');
        //$vars['atp_act_label'] = $this->core->lng('atp_act_label');
        $vars['atp_act_classificator'] = $this->core->lng('atp_act_classificator');
        $tmpl->set_var($vars);

        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_ACT') > 1) {
            $tmpl->parse($form_block, 'form');
        } else {
            $tmpl->clean($form_block);
        }

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    /**
     * Veikos redagavimo forma
     * @param boolen $ajax Nurodo ar formuoti pilną puslapio HTML'ą
     * @return string
     */
    public function deed_editform($ajax = false)
    {
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_DEED')) {
            return $this->core->printForbidden();
        }

        $tmpl = $this->core->factory->Template();
        $tmpl_handler = 'deed_editform_file';
        $tmpl->set_file($tmpl_handler, 'deed_editform.tpl');

        // Straipnsių sąrašas
        $clauses_list = $tmpl_handler . 'clauses_list';
        $tmpl->set_block($tmpl_handler, 'clauses_list', $clauses_list);
        // Teisės aktų sąrašas
        $acts_list = $tmpl_handler . 'acts_list';
        $tmpl->set_block($tmpl_handler, 'acts_list', $acts_list);
        // Forma
        $form_block = $tmpl_handler . 'form';
        $tmpl->set_block($tmpl_handler, 'form', $form_block);

        $vars = array();
        $m = (int) $this->core->extractVariable('m');

        if ($ajax === true)
            $vars['tmpl_class'] = ' class="dialog"';

        $id = (int) $this->core->extractVariable('id');
        $parent_id = (int) $this->core->extractVariable('parent');
        $vars['sys_message'] = $this->core->get_messages();
        $vars['GLOBAL_SITE_URL'] = $this->core->config['GLOBAL_SITE_URL'];
        $vars['save'] = 'Išsaugoti';

        $tmpl->set_var(array(
            'clause_id' => 0,
            'clause_name' => '',
            'class' => 'sample'));
        $tmpl->parse($clauses_list, 'clauses_list');
        $tmpl->set_var(array(
            'act_id' => 0,
            'act_name' => '',
            'class' => 'sample'));
        $tmpl->parse($acts_list, 'acts_list');

        $data = array();
        if (empty($id) === false) {
            $data = $this->core->logic->get_deed(array('ID' => $id));
        }

        $i = $j = 0;
        if (empty($data) === true) {
            $vars['url'] = 'index.php?m=' . $m . '&parent=' . $parent_id . '&action=save';
            $vars['title'] = 'Naujas teisės aktas';
            $vars['name']/* = $vars['valid_from'] = $vars['valid_till'] */ = null;
        } else {
            $vars['url'] = 'index.php?m=' . $m . '&id=' . $id . '&action=save';
            $vars['title'] = 'Veika: ' . $data['LABEL'];
            $vars['name'] = htmlspecialchars($data['LABEL']);

            $extends = $this->core->logic->get_deed_extend_data(array('DEED_ID' => $id));
            foreach ($extends as $extend) {
                if ($extend['ITYPE'] === 'CLAUSE') {
                    $tmpl->set_var(array(
                        'clause_id' => $extend['ITEM_ID'],
                        'clause_name' => $extend['INAME'],
                        'class' => ''));
                    $tmpl->parse($clauses_list, 'clauses_list', true);
                    $i++;
                } elseif ($extend['ITYPE'] === 'ACT') {
                    $tmpl->set_var(array(
                        'act_id' => $extend['ITEM_ID'],
                        'act_name' => $extend['INAME'],
                        'class' => ''));
                    $tmpl->parse($acts_list, 'acts_list', true);
                    $j++;
                }
            }


            $adminJournal = $this->core->factory->AdminJournal();
            $adminJournal->addRecord(
                $adminJournal->getVisitRecord(
                    'Veikos "' . $data['LABEL'] . '" redagavimą.'
                )
            );
        }

        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_DEED') > 1) {
            $tmpl->parse($form_block, 'form');
        } else {
            $tmpl->clean($form_block);
        }

        $tmpl->set_var($vars);

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        if ($ajax === true)
            return $output;
        return $this->core->atp_content($output);
    }

    //http://edarbuotojas.vilnius.lt/subsystems/atp/index.php?m=11&clause_id=59&edit=1
    //http://wp1.dev//subsystems/atp/index.php?m=11&clause_id=42&edit=1
    /**
     * Straipsnio redagavimo forma
     * @return string
     */
    public function clause_editform()
    {
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_CLAUSE')) {
            return $this->core->printForbidden();
        }

        if ($this->core->content_type !== 2) {
            $this->core->_load_files['js'][] = 'jquery.autogrow-textarea.js';
            $this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
            $this->core->_load_files['js'][] = 'jquery.validationEngine.js';
            $this->core->_load_files['css'][] = 'validationEngine.jquery.css';
        }

        $tmpl = $this->core->factory->Template();
        $tmpl->handler = 'clause_editform_file';
        $tmpl->set_file($tmpl->handler, 'clause_editform.tpl');

        $handler = $tmpl->set_multiblock($tmpl->handler, array(
            array(
                'field', // Papildomo lauko blokas
                'clause_type_option', // Pažeidimo tipo pasirinkimas
                'clause_type_optgroup', // Pažeidimo tipų grupė
                'clause_group_option' // Straipsnio grupės pasirinkimas
            ),
            'form' // Formos blokas
        ));

        $m = (int) $this->core->extractVariable('m');


        $id = (int) $this->core->extractVariable('id');
        $parent = (int) $this->core->extractVariable('parent');



        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_CLAUSE') > 1) {
            if (isset($_POST['submit'])) {
                $this->core->action->clause();
            }
        }

        $vars = array(
            'm' => $m,
            'GLOBAL_SITE_URL' => $this->core->config['GLOBAL_SITE_URL'],
            'sys_message' => $this->core->get_messages(),
            'save' => 'Išsaugoti',
            'hidden_fields' => ''
        );

        $vars['clause_codex_label'] = $this->core->lng('atp_clause_codex');
        $vars['clause_codex'] = '';
        $vars['e_clause_clause'] = '';
        $vars['e_clause_section'] = '';
        $vars['e_clause_paragraph'] = '';
        $vars['e_clause_subparagraph'] = '';
        $vars['e_clause_label'] = '';
        $vars['e_clause_deed'] = '';
        $vars['e_clause_penalty_min'] = '';
        $vars['e_clause_penalty_max'] = '';
        $vars['e_clause_penalty_first'] = '';
        $vars['e_clause_notice'] = '';
        $vars['e_clause_notice_label'] = '';
        $vars['e_clause_term'] = '';
        $vars['e_clause_create_section'] = '';
        $vars['e_clause_create_paragraph'] = '';
        $vars['e_clause_create_subparagraph'] = '';
        $vars['e_show_clause_section'] = '';
        $vars['e_show_clause_paragraph'] = '';
        $vars['e_show_clause_subparagraph'] = '';
//        $vars['e_clause_class_type'] = '0';
        $vars['e_clause_group'] = '0';

        $data = array();
        if (empty($id) === false) {
            $data = $this->core->logic2->get_clause(array('ID' => $id), true);
        }

        if (empty($data) === true) {
            $vars['url'] = 'index.php?m=' . $m . '&id=' . $parent . '&action=save';
            $vars['title'] = 'Naujas straipsnis';
            $vars['name']/* = $vars['valid_from'] = $vars['valid_till'] */ = null;
        } else {
            $adminJournal = $this->core->factory->AdminJournal();
            $adminJournal->addRecord(
                $adminJournal->getVisitRecord(
                    'Straipsnio "' . $data['NAME'] . '" redagavimą.'
                )
            );


            $vars['url'] = 'index.php?m=' . $m . '&id=' . $id . '&action=save';
            $vars['title'] = 'Straipsnis: ' . $data['NAME'];
            $vars['name'] = htmlspecialchars($data['LABEL']);

            foreach ($data as $key => $value) {
                if ($key == 'NOTICE')
                    $value = ($value ? ' checked="checked"' : '');
                elseif ($key == 'VALID_FROM') {
                    if (empty($value) === false)
                        $value = date('Y-m-d', strtotime($value));
                } elseif ($key == 'VALID_TILL') {
                    if (empty($value) === false)
                        $value = date('Y-m-d', strtotime($value));
                }

                $vars['e_clause_' . strtolower($key)] = htmlspecialchars($value);
            }
            $vars['clause_codex'] = htmlspecialchars($data['CODEX']);
        }

        if (empty($_GET['id']) === false && $_GET['action'] === 'sub') {
            $clauses = $this->core->logic->search_clauses(array(
                'clause' => $vars['e_clause_clause'],
                'section' => $vars['e_clause_section'],
                'paragraph' => $vars['e_clause_paragraph'],
                'subparagraph' => $vars['e_clause_subparagraph']));
            $max = array();
            if (count($clauses)) {
                for ($i = 0; $i < count($clauses); $i++) {
                    $max['section'] = max($max['section'], $clauses[$i]['SECTION']);
                    $max['paragraph'] = max($max['paragraph'], $clauses[$i]['PARAGRAPH']);
                    $max['subparagraph'] = max($max['subparagraph'], $clauses[$i]['SUBPARAGRAPH']);
                }
            }
        }
        $create_new = true;
        if (empty($data['SUBPARAGRAPH'])) {
            $vars['e_show_clause_subparagraph'] = ' style="display: none;"';
        } else {
            $vars['e_show_clause_subparagraph'] = '';
            $create_new = false;
        }
        if (empty($data['PARAGRAPH'])) {
            $vars['e_show_clause_paragraph'] = ' style="display: none;"';
            $vars['e_clause_create_subparagraph'] = '';
        } else {
            $vars['e_show_clause_paragraph'] = '';
            if ($create_new === true) {
                if ($_GET['action'] === 'sub') {
                    $vars['e_show_clause_subparagraph'] = '';
                    $vars['e_clause_subparagraph'] = $max['subparagraph'] + 1;
                } else {
                    $vars['e_clause_create_subparagraph'] = '<a href="index.php?m=' . $m . '&id=' . $id . '&action=sub">Sukurti papunktį</a>';
                }
                $create_new = false;
            }
        }
        if (empty($data['SECTION'])) {
            $vars['e_show_clause_section'] = ' style="display: none;"';
            $vars['e_clause_create_paragraph'] = '';
        } else {
            $vars['e_show_clause_section'] = '';
            if ($create_new === true) {
                if ($_GET['action'] === 'sub') {
                    $vars['e_show_clause_paragraph'] = '';
                    $vars['e_clause_paragraph'] = $max['paragraph'] + 1;
                } else {
                    $vars['e_clause_create_paragraph'] = '<a href="index.php?m=' . $m . '&id=' . $id . '&action=sub">Sukurti punktą</a>';
                }
                $create_new = false;
            }
        }
        if (empty($data['CLAUSE'])) {
            $vars['e_clause_create_section'] = '';
        } else {
            if ($create_new === true) {
                if ($_GET['action'] === 'sub') {
                    $vars['e_show_clause_section'] = '';
                    $vars['e_clause_section'] = $max['section'] + 1;
                } else {
                    $vars['e_clause_create_section'] = '<a href="index.php?m=' . $m . '&id=' . $id . '&action=sub">Sukurti dalį</a>';
                }
                $create_new = false;
            }
        }

        if (empty($id) === false && $_GET['action'] === 'sub') {
            $vars['e_submit_title'] = 'Sukurti';
            $vars['e_title'] = 'Naujas straipsnis';
            $pass_keys = array('CLAUSE', 'SECTION', 'PARAGRAPH', 'SUBPARAGRAPH');
            foreach ($data as $key => $value) {
                if ($key == 'NOTICE') {
                    $value = ($value ? ' checked="checked"' : '');
                }
                if (in_array($key, $pass_keys)) {
                    continue;
                }
                $vars['e_clause_' . strtolower($key)] = null;
            }
            $vars['hidden_fields'] = '<input type="hidden" name="parent_id" value="' . $id . '">';
        }

        // Papildomi laukai
        $fields = $this->core->logic2->get_fields(array('ITYPE' => 'CLAUSE', 'ITEM_ID' => $data['ID']));
        foreach ($fields as &$field) {
            $tmpl->set_var(array(
                'id' => $field['ID'],
                'name' => $field['LABEL'],
                'value' => htmlspecialchars($field['DEFAULT'])
            ));
            $tmpl->parse($handler['field'], 'field', true);
        }


        $clauseManager = new \Atp\Document\Clause($this->core);

        $selectedClassificators = $clauseManager->getClassificators($data['ID']);

        // Pažeidimo tipo pasirinkiams
        foreach ($clauseManager->class_type_parent_id as $classificatorId) {
            $classificators = $this->core->logic2->get_classificator(array('PARENT_ID' => $classificatorId), null, null, 'ID, LABEL');

            if (count($classificators) === 0) {
                continue;
            }

            $parentClassificator = $this->core->logic2->get_classificator(['ID' => $classificatorId], true, null, 'ID, LABEL');

            $i = 0;
            foreach ($classificators as &$classificator) {
                $tmpl->set_var(array(
                    'value' => $classificator['ID'],
                    'label' => $classificator['LABEL'],
                    'option_attr' => (in_array($classificator['ID'], $selectedClassificators) ? ' selected="selected"' : '')
                ));
                $tmpl->parse($handler['clause_type_option'], 'clause_type_option', $i);
                $i++;
            }
            $tmpl->set_var(array(
                'label' => $parentClassificator['LABEL'],
            ));
            $tmpl->parse($handler['clause_type_optgroup'], 'clause_type_optgroup', true);
        }

        // Straipsnio grupės pasirinkiams
        $classificators = $this->core->logic2->get_classificator(array('PARENT_ID' => $clauseManager->group_parent_id), null, null, 'ID, LABEL');
        foreach ($classificators as &$classificator) {
            $tmpl->set_var(array(
                'value' => $classificator['ID'],
                'label' => $classificator['LABEL'],
                'option_attr' => ((int) $classificator['ID'] === (int) $vars['e_clause_group'] ? ' selected="selected"' : '')
            ));
            $tmpl->parse($handler['clause_group_option'], 'clause_group_option', true);
        }


        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_CLAUSE') > 1) {
            $tmpl->parse($handler['form'], 'form');
        } else {
            $tmpl->clean($handler['form']);
        }

        $tmpl->set_var($vars);

// <editor-fold defaultstate="collapsed" desc="Ryšių kolkas nebereikia (Jau ląąąbai seniai nebereikia)">
        /*
          $clause_relations = array();
          if (empty($clause_id) === false) {
          $clause_relations = $this->core->logic->get_clause_relation(array('CLAUSE_ID' => $clause_id));
          }
          $Marker_12 = $tmpl->handler . '_Marker_12';
          $tmpl->set_block($tmpl->handler, 'Marker_12', $Marker_12);
          $Marker_14 = $tmpl->handler . '_Marker_14';
          $tmpl->set_block($tmpl->handler, 'Marker_14', $Marker_14);
          $Marker_16 = $tmpl->handler . '_Marker_16';
          $tmpl->set_block($tmpl->handler, 'Marker_16', $Marker_16);
          $Marker_11 = $tmpl->handler . '_Marker_11';
          $tmpl->set_block($tmpl->handler, 'Marker_11', $Marker_11);

          $tables = $this->core->logic2->get_document();
          $clause_structures = $this->core->logic->get_clause_structures();
          $loop = count($clause_relations) ? count($clause_relations) : 1;

          for ($i = 0; $i < $loop; $i++) {

          // Dokumentas
          $selected = false;
          $current_table = null;
          $j = 0;
          foreach ($tables as $table_id => $table) {
          $select = false;
          if ($selected === false) {
          foreach ($clause_relations as $relation_id => $relation) {
          if ($relation['TABLE_ID'] == $table_id && $relation['SELECTED'] !== true) {
          $selected = $select = true;
          $clause_relations[$relation_id]['SELECTED'] = true;
          $current_table = $table_id;
          break;
          }
          }
          }

          $tmpl->set_var(array(
          'table_id_12' => $table['ID'],
          'table_label_12' => $table['LABEL'],
          'table_selected_12' => $select ? 'selected="selected"' : ''));
          $tmpl->parse($Marker_12, 'Marker_12', $j);
          $j++;
          }

          // Dokumento laukas
          if (empty($current_table) === false) {
          $table_structure = $this->core->logic->get_table_fields($current_table);
          $selected = false;
          $j = 0;
          foreach ($table_structure as $col_id => $col) {
          $select = false;
          if ($selected === false) {
          foreach ($clause_relations as $relation_id => $relation) {
          if ($relation['TABLE_ID'] == $current_table && $relation['TABLE_STRUCTURE_ID'] == $col_id && $relation['SELECTED_STRUCTURE'] !== true) {
          $selected = $select = true;
          $clause_relations[$relation_id]['SELECTED_STRUCTURE'] = true;
          $current_field = $clause_relations[$relation_id]['TABLE_STRUCTURE_ID'];
          break;
          }
          }
          }
          $tmpl->set_var(array(
          'column_id_14' => $col['ID'],
          'column_label_14' => $col['LABEL'],
          'column_selected_14' => $select ? 'selected="selected"' : ''));
          $tmpl->parse($Marker_14, 'Marker_14', $j);
          $j++;
          }
          }

          // Straipsnio laukas (?)
          $selected = false;
          $j = 0;
          foreach ($clause_structures as $key => $clause) {
          $select = false;
          if ($selected === false) {
          foreach ($clause_relations as $relation_id => $relation) {
          if ($relation['TABLE_ID'] == $current_table && $relation['CLAUSE_STRUCTURE_ID'] == $clause['ID'] && $relation['TABLE_STRUCTURE_ID'] == $current_field && $relation['SELECTED_CLAUSE'] !== true) {
          $selected = $select = true;
          $clause_relations[$relation_id]['SELECTED_CLAUSE'] = true;
          break;
          }
          }
          }
          $tmpl->set_var(array(
          'clause_id_16' => $clause['ID'],
          'clause_label_16' => $clause['LABEL'],
          'clause_selected_16' => $select ? 'selected="selected"' : ''));
          $tmpl->parse($Marker_16, 'Marker_16', $j);
          $j++;
          }

          $tmpl->parse($Marker_11, 'Marker_11', true);
          }
         */// </editor-fold>

        $tmpl->parse($tmpl->handler . '_out', $tmpl->handler);
        $output = $tmpl->get($tmpl->handler . '_out');

        return $this->core->atp_content($output);
    }

    /**
     * Paieškas
     * @author Justinas Malūkas
     * @return string
     */
    function search()
    {
        $tmpl = $this->core->factory->Template();

        $tmpl_handler = 'searchfile';
        $tmpl->set_file($tmpl_handler, 'search.tpl');

        // Paieškos lauko reikšmė
        $field_input_block = $tmpl_handler . 'field_input';
        $tmpl->set_block($tmpl_handler, 'field_input', $field_input_block);

        // Paieškos lauko checkbox'as
        $field_checkbox_block = $tmpl_handler . 'field_checkbox';
        $tmpl->set_block($tmpl_handler, 'field_checkbox', $field_checkbox_block);
        //$tmpl->parse($field_checkbox_block, 'field_checkbox');
        // Paieškos lauko blokas
        $field_block = $tmpl_handler . 'field';
        $tmpl->set_block($tmpl_handler, 'field', $field_block);

        // Paieškos forma
        $form_block = $tmpl_handler . 'form';
        $tmpl->set_block($tmpl_handler, 'form', $form_block);

        // Paieškos rezultatų sąrašas
        $list_block = $tmpl_handler . 'list';
        $tmpl->set_block($tmpl_handler, 'list', $list_block);

        // Paieškos rezultatų sąrašas
        $reports_block = $tmpl_handler . 'reports';
        $tmpl->set_block($tmpl_handler, 'reports', $reports_block);

        $m = $this->core->extractVariable('m');

        $vars['GLOBAL_SITE_URL'] = $this->core->config['GLOBAL_SITE_URL'];
        $vars['sys_message'] = $this->core->get_messages();

        $fdata = &$_REQUEST;

        if ($_GET['type'] === 'advanced' && isset($fdata['field']) === false) {
            $vars['reports'] = $vars['list'] = '';
            $vars['title'] = 'Išplėstinė dokumentų paieška';
            $vars['action'] = 'index.php?m=' . $m . '&type=advanced';


            //require_once($this->core->config['REAL_URL'] . 'helper/slug.php');
            $slug = new \Slug();


            // Sukurti galiojantys paieškos laukai
            $fields = $this->core->logic2->get_search_field(array('VALID' => 1), false, null, 'ID, NAME');


            // Spauzdinami paieškos laukai
            foreach ($fields as $field) {
                $name = 'field-' . $slug->makeSlugs($field['NAME']);
                // Lauko reikšmės įvedimas
                $tmpl->set_var(array(
                    'class' => '', //$name,
                    'type' => 'text',
                    'name' => 'field[' . $field['ID'] . ']',
                    'value' => '',
                    'extra' => '',
                    $field_checkbox_block => ''
                ));
                $tmpl->parse($field_input_block, 'field_input');
                // Lauko pavadinimas
                $tmpl->set_var(array(
                    'class' => $name,
                    'label' => $field['NAME']
                ));
                $tmpl->parse($field_block, 'field', true);
            }


            // Saugojimo kaip "Ataskaita" laukai
            $name = 'field-as-report';
            // Lauko pasirinkimas
            //$tmpl->set_var(array(
            //	'name' => 'field[as_report]',
            //	'checked' => ''
            //));
            $tmpl->parse($field_checkbox_block, 'field_checkbox');
            // Lauko reikšmės įvedimas
            $tmpl->set_var(array(
                'class' => '', //$name,
                'type' => 'text',
                'name' => 'field[as_report]',
                'value' => '',
                'extra' => 'disabled="disabled"'
            ));
            $tmpl->parse($field_input_block, 'field_input');
            // Lauko pavadinimas
            $tmpl->set_var(array(
                'class' => $name,
                'label' => 'Išsaugoti kaip ataskaitą'
            ));
            $tmpl->parse($field_block, 'field', true);

            $tmpl->parse($form_block, 'form');


            $tmpl->set_var($vars);
        } elseif ($_GET['type'] === 'reports') {
            $vars['form'] = $vars['list'] = '';
            $vars['title'] = 'Ataskaitos';
            $tmpl->set_var('table_reports_list', $this->table_reports_list());
            $tmpl->parse($reports_block, 'reports');

            $tmpl->set_var($vars);
        } elseif (empty($fdata['field']) === false) {
            $vars['form'] = $vars['reports'] = '';
            $vars['title'] = 'Greitoji dokumentų paieška';

            $data = $this->core->action->searchFields($fdata, 'search', ['quicksearch' => true]);
			$tmpl->set_var('table_search_list', $this->table_search_list($data));
            $tmpl->parse($list_block, 'list');

            $tmpl->set_var($vars);
        } else {
            header('Location: index.php?m=18&type=advanced');
            exit;
        }

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

		return $this->core->atp_content($output);
    }

    /**
     * Paieškos laukai
     * @author Justinas Malūkas
     * @return string
     */
    function searchFields()
    {
        $tmpl = $this->core->factory->Template();

        $tmpl_handler = 'search_fields_file';
        $tmpl->set_file($tmpl_handler, 'search_fields.tpl');

        // Naujo peiškos lauko kūrimo mygtukas
        $new_block = $tmpl_handler . 'new';
        $tmpl->set_block($tmpl_handler, 'new', $new_block);
        $tmpl->parse($new_block, 'new');

        $vars['fields_table'] = $this->table_fields();
        $vars['GLOBAL_SITE_URL'] = $this->core->config['GLOBAL_SITE_URL'];
        $vars['sys_message'] = $this->core->get_messages();
        $vars['save'] = 'Išsaugoti';

        $tmpl->set_var($vars);

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    /**
     * Paieškos lauko redagavimo forma
     * @author Justinas Malūkas
     * @return string
     */
    public function search_fields_editform()
    {
        $tmpl = $this->core->factory->Template();
        /* 	if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_TABLE')) {
          return $this->core->printForbidden();
          } */


        if ($this->core->content_type !== 2) {
            $this->core->_load_files['js'][] = 'jquery.validationEngine-lt.js';
            $this->core->_load_files['js'][] = 'jquery.validationEngine.js';
            $this->core->_load_files['css'][] = 'validationEngine.jquery.css';
        }

        $tmpl_handler = 'search_field_editform_file';
        $tmpl->set_file($tmpl_handler, 'search_field_editform.tpl');

        // Papildomo lauko blokas
        $form_field_block = $tmpl_handler . 'form_field';
        $tmpl->set_block($tmpl_handler, 'form_field', $form_field_block);

        // Naujo lauko saltinio tipas
        $field_source_type_option_block = $tmpl_handler . 'field_source_type_option';
        $tmpl->set_block($tmpl_handler, 'field_source_type_option', $field_source_type_option_block);
        // Naujo lauko prijungimas
        $form_field_new_block = $tmpl_handler . 'form_field_new';
        $tmpl->set_block($tmpl_handler, 'form_field_new', $form_field_new_block);

        // Lauko tipo pasirinkimai
        $field_type_option_block = $tmpl_handler . 'field_type_option';
        $tmpl->set_block($tmpl_handler, 'field_type_option', $field_type_option_block);

        // Formos blokas
        $form_block = $tmpl_handler . 'form';
        $tmpl->set_block($tmpl_handler, 'form', $form_block);


        $vars = array();
        $m = (int) $this->core->extractVariable('m');
        $id = (int) $this->core->extractVariable('id');
        //$parent_id = (int) $this->core->extractVariable('parent');


        $vars['sys_message'] = $this->core->get_messages();
        $vars['save'] = 'Išsaugoti';
        $vars['name'] = '';
        $vars['quick_search_checked'] = '';


        // Surenka dumenis apie paieškos lauką
        $info = array();
        if (empty($id) === false) {
            $info = $this->core->logic2->get_search_field(array('ID' => $id));
        }

        if (empty($info) === true) {
            $vars['title'] = 'Naujas paieškos laukas';
            $vars['url'] = 'index.php?m=' . $m . '&action=save';
        } else {
            $vars['title'] = 'Laukas: ' . $info['NAME'];
            $vars['url'] = 'index.php?m=' . $m . '&id=' . $id . '&action=save';
            $vars['name'] = htmlspecialchars($info['NAME']);


            $adminJournal = $this->core->factory->AdminJournal();
            $adminJournal->addRecord(
                $adminJournal->getVisitRecord(
                    'Paieškos lauko "' . $info['NAME'] . '" redagavimą.'
                )
            );


            if (empty($info['USE_IN_QUICK_SEARCH']) === false) {
                $vars['quick_search_checked'] = ' checked="checked"';
            }

            // Priskirti laukai
            $field_fields = $this->core->logic2->get_search_field_form_fields(array(
                'SEARCH_FIELD_ID' => $info['ID']), false, null, 'ID, FIELD_ID');
            $ids_arr = array();
            foreach ($field_fields as &$arr) {
                $ids_arr[] = $arr['FIELD_ID'];
            }
            $fields = array();
            if (count($ids_arr)) {
                $fields = $this->core->logic2->get_fields(array(0 => 'ID IN(' . join(',', $ids_arr) . ')'));
            }
            unset($field_fields, $ids_arr);

            // Priskirti informaciniai laukai
            $ifields = array();
            $field_fields = $this->core->logic2->get_search_field_form_ifields(array(
                'SEARCH_FIELD_ID' => $info['ID']), false, null);
            $act_info = $this->core->logic->get_info_fields(array('TYPE' => 'ACT'));
            $clause_info = $this->core->logic->get_info_fields(array('TYPE' => 'CLAUSE'));
            foreach ($field_fields as $ifield) {
                if ($ifield['ITYPE'] === 'ACT') {
                    $itype = 'IACT';
                    $name = 'AICOL_';
                    $label = $act_info['header'][$ifield['FIELD_ID']];
                } elseif ($ifield['ITYPE'] === 'CLAUSE') {
                    $itype = 'ICLAUSE';
                    $name = 'SICOL_';
                    $label = $clause_info['header'][$ifield['FIELD_ID']];
                }
                $ifields[] = array('ID' => $ifield['FIELD_ID'], 'ITYPE' => $itype,
                    'NAME' => $name . $ifield['FIELD_ID'], 'LABEL' => $label, 'TYPE' => '7');
            }
            unset($field_fields);
        }


        /*
         *  Paieškos lauko tipo pasirinkimas
         */
        $field_types = $this->core->logic->get_field_type();
        $allowed_types = array(8, 12, 13, 14);

        $tmpl->set_var(array(
            'value' => 0,
            'label' => $this->core->lng('not_chosen'),
            'selected' => ''
        ));
        $tmpl->parse($field_type_option_block, 'field_type_option');

        foreach ($field_types as &$val) {
            if (in_array($val['ID'], $allowed_types) === false)
                continue;

            $tmpl->set_var(array(
                'value' => $val['ID'],
                'label' => $val['LABEL'],
                'selected' => ((int) $val['ID'] === (int) $info['TYPE'] ? ' selected="selected"' : '')
            ));
            $tmpl->parse($field_type_option_block, 'field_type_option', true);
        }


        /*
         * Priskirto lauko šaltinio tipo pasirinkimas
         */
        $source_types = array(
            0 => $this->core->lng('not_chosen'),
            'DOC' => 'Dokumentas',
            'CLASS' => 'Klasifikatorius',
            'CLAUSE' => 'Straipsnis',
            'ICLAUSE' => 'Straipsnis',
            'ACT' => 'Teisės akto punktas',
            'IACT' => 'Teisės akto punktas',
            'WS' => 'Web-servisas'
        );
        foreach ($source_types as $val => &$label) {
            if (in_array($val, array('WS', 'IACT', 'ICLAUSE'), true)) {
                continue;
            }
            $tmpl->set_var(array(
                'value' => $val,
                'label' => $label
            ));
            $tmpl->parse($field_source_type_option_block, 'field_source_type_option', true);
        }
        $tmpl->parse($form_field_new_block, 'form_field_new');


        // Dokumentų, klasifikatorių, webservisų informacija
        $items_names['DOC'] = $this->core->logic2->get_document(null, false, null, 'ID, LABEL, PARENT_ID');
        $items_names['CLASS'] = $this->core->logic2->get_classificator(null, false, null, 'ID, LABEL, PARENT_ID');
        $ws = $this->core->logic2->get_ws();

        /*
         * Priskirtų laukų išvedimas
         */
        $tmpl->set_var(array(
            'class' => ' sample',
            'field_id' => '',
            'field_source_type' => '',
            'field_source' => '',
            'field_name' => ''
        ));
        $tmpl->parse($form_field_block, 'form_field');
        if (empty($fields) === false || empty($ifields) === false) {
            foreach (array(0 => $fields, 'I' => $ifields) as $key => $fields) {
                foreach ($fields as &$field) {
                    // Pagal dokumento tipą nustatomas šaltinio pavadinimas
                    $item_name = array();
                    switch ($field['ITYPE']) {
                        case 'DOC':
                        case 'CLASS':
                            $item_name[] = $items_names[$field['ITYPE']][$field['ITEM_ID']]['LABEL'];
                            break;
                        case 'WS':
                            $parent_field_info = $this->core->logic2->get_fields(array(
                                'ID' => $field['ITEM_ID']));
                            switch ($parent_field_info['ITYPE']) {
                                case 'DOC':
                                    $item_info = $this->core->logic2->get_document(array(
                                        'ID' => $parent_field_info['ITEM_ID']), true, null, 'ID, LABEL');
                                    $item_name[] = $source_types['DOC'] . ': ' . $item_info['LABEL'];
                                    break;
                                case 'CLASS':
                                    $item_info = $this->core->logic2->get_classificator(array(
                                        'ID' => $parent_field_info['ITEM_ID']), true, null, 'ID, LABEL');
                                    $item_name[] = $source_types['CLASS'] . ': ' . $item_info['LABEL'];
                                    break;
                                default:
                                    $item_name[] = 'Unknown: ???';
                            }
                            $item_name[] = $parent_field_info['LABEL'];
                            break;
                        default:
                            $item_name[] = '-------';
                    }

                    $type = '';
                    if ($key === 'I') {
                        $type = substr($field['NAME'], 0, 2);
                    }

                    $tmpl->set_var(array(
                        'class' => '',
                        'field_id' => $type . $field['ID'],
                        'field_source_type' => $source_types[$field['ITYPE']],
                        'field_source' => join(' > ', $item_name),
                        'field_name' => $field['LABEL']
                    ));
                    $tmpl->parse($form_field_block, 'form_field', true);
                }
            }
        }

        /*
         * Galimų laukų pasirinkimų hierarchija
         */
        $fields_data = array();
        // Surenka dokumentų ir klasifikatorių laukus
        $tfields = $this->core->logic2->get_fields(array(0 => 'ITYPE IN ("DOC", "CLASS")'), false, null, 'ID, LABEL, ITYPE, ITEM_ID, WS');

        // Straipsnių laukai
        $clause_info = $this->core->logic->get_info_fields(array('TYPE' => 'CLAUSE'));
        $ifields = array();
        foreach ($clause_info['header'] as $key => $arr) {
            $ifields[] = array('ID' => 'SI' . $key, 'ITYPE' => 'ICLAUSE', 'NAME' => 'SICOL_' . $key,
                'LABEL' => $arr, 'TYPE' => '7');
        }
        $tfields2['CLAUSE'] = array_merge($ifields, $this->core->logic2->get_fields(array(
                'ITYPE' => 'CLAUSE'), false, null, 'ID, LABEL, ITYPE, ITEM_ID, WS'));
        foreach ($tfields2['CLAUSE'] as $f) {
            $fields_data['CLAUSE']['FIELDS'][$f['ID']]['NAME'] = $f['LABEL'];
        }

        // Teisės aktų punktų laukai
        $act_info = $this->core->logic->get_info_fields(array('TYPE' => 'ACT'));
        $ifields = array();
        foreach ($act_info['header'] as $key => $arr) {
            $ifields[] = array('ID' => 'AI' . $key, 'ITYPE' => 'IACT', 'NAME' => 'AICOL_' . $key,
                'LABEL' => $arr, 'TYPE' => '7');
        }
        $tfields2['ACT'] = array_merge($ifields, $this->core->logic2->get_fields(array(
                'ITYPE' => 'ACT'), false, null, 'ID, LABEL, ITYPE, ITEM_ID, WS'));
        foreach ($tfields2['ACT'] as $f) {
            $fields_data['ACT']['FIELDS'][$f['ID']]['NAME'] = $f['LABEL'];
        }

        foreach ($tfields as $tfid => &$tfield) {
            $tfid = $tfield['ID'];
            // Šaltinio informacija
            $tfield_info = $items_names[$tfield['ITYPE']][$tfield['ITEM_ID']];
            // Šaltinio pavadinimas
            $tfield_name = $tfield_info['LABEL'];

            if (empty($tfield_name))
                continue;

            $ref = &$fields_data[$tfield['ITYPE']];

            // Surenka šaltinio tėvinius šaltinius
            $parents = array();
            if (empty($tfield_info['PARENT_ID']) === false) {
                $fff = $tfield_info;
                $i = 0;
                while (empty($fff['PARENT_ID']) === false) {
                    $i++;
                    if ($i > 50) {
                        trigger_error('To many loops', E_USER_WARNING);
                        break;
                    }
                    $parents[] = $fff['PARENT_ID'];
                    $fff = $items_names[$tfield['ITYPE']][$fff['PARENT_ID']];
                }
            }
            // Į masyvą hierarchiškai sudeda rėvinius šaltinius
            $parents = array_reverse($parents);
            foreach ($parents as $parent) {
                $ref['GROUP'][$parent]['NAME'] = $items_names[$tfield['ITYPE']][$parent]['LABEL'];
                $ref = &$ref['GROUP'][$parent];
            }

            $g_ref = &$ref['GROUP'][$tfield['ITEM_ID']];
            // Pagrindinio šaltinio pavadinimas
            $g_ref['NAME'] = $tfield_name;
            // Šaltinio laukai
            $g_ref['FIELDS'][$tfid]['NAME'] = $tfield['LABEL'];

            // Surenka web-serviso lauko laukus
            if ($tfield['WS'] === '1') {
                $g_ref['FIELDS'][$tfid]['WS'] = array();
                // Web-serviso laukui sukurti laukai
                $ws_fields = $this->core->logic2->get_webservices(array('PARENT_FIELD_ID' => $tfid));

                foreach ($ws_fields as $id => &$wsfield) {
                    $wsfinfo = $this->core->logic2->get_fields(array('ID' => $wsfield['FIELD_ID']), true, null, 'ID, LABEL');
                    $wsinfo = $ws->get_info($wsfield['WS_NAME']);
                    $g_ref['FIELDS'][$tfid]['WS'][$wsfield['WS_NAME']]['NAME'] = $wsinfo['label'];
                    $g_ref['FIELDS'][$tfid]['WS'][$wsfield['WS_NAME']]['FIELDS'][$wsfinfo['ID']]['NAME'] = $wsfinfo['LABEL'];
                }
            }
        }

        $fields_data = json_encode($fields_data);
        $vars['fields_data'] = &$fields_data;

        $tmpl->set_var($vars);

        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_TABLE') > 1) {
            $tmpl->parse($form_block, 'form');
        } else {
            $tmpl->clean($form_block);
        }

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    // TODO: Straipsnio laukams turi leisti pasirinkti staipsnius, kalasifikatorių - klasifikatorius, veikos - veikas ir pan.?
    /**
     * Suformuoja formos lauką (koreguota view_column_structure() kopija. Daryta "Dokumnetų ryšiai" puslapiui, kai yra nurodoma reikšmę pagal nutylėjimą, pagal lauko tipą suformuojamas įvesties laukas)
     * @param array $arr Parametrai
     * @param int $arr['ID'] [optional] Formos lauko ID
     * @param int $arr['FIELD'] [optional] Formos lauko informacija
     * @param int $arr['NAME'] [optional] Nurodoma kaip suformuojamas formos lauko vardas
     * @param int $arr['NAME_TMPL'] [optional] Nurodoma kaip suformuojamas formos lauko vardas
     * @param int $arr['VALUE'] [optional] Formos lauko reikšmė
     * @param array $param TODO: kad atitiktu $this->manage->record_field(&$field, &$param)
     */
    function get_field_html($arr, &$param = null)
    {
        // Vienas parametras privalomas
        if (isset($arr['ID']) === false && isset($arr['FIELD']) === false) {
            trigger_error('Field is not set', E_USER_WARNING);
            return false;
        }

        // Gauna lauko informaciją
        if (empty($arr['FIELD']) === false) {
            $field = &$arr['FIELD'];
        } elseif (empty($arr['ID']) === false) {
            $field = $this->core->logic2->get_fields(array('ID' => $arr['ID']));
        }
        if (empty($field)) {
            trigger_error('Field not found', E_USER_WARNING);
            return false;
        }


        if (isset($arr['VALUE']) === true) {
            $value = &$arr['VALUE'];
        } else {
            $record_id = $param['record_id'];
            $table = $param['table'];
            $table_record = $param['record'];
            $value = isset($param['record'][$field['NAME']]) ? $param['record'][$field['NAME']] : '';
        }

        $field['LABEL'] = '';
//-----ČIA BAIGIAU
        // Lauko "name" atributo reikšmė
        switch ($arr['NAME']) {
            case 'PREFIX';
                $index = $param['prefix'] = $param['prefix'] . 'F' . end(explode('_', $field['NAME']));
                break;
            case 'ID':
            default:
                $index = $field['ID'];
        }
        if (strpos((string) $arr['NAME_TMPL'], '{$name}') === false) {
            $arr['NAME_TMPL'] = 'field[{$name}]';
        }
        $name = str_replace('{$name}', $index, $arr['NAME_TMPL']);

        // Šablono laukai
        $tpl = array(
            'filename' => '', // Lauko šablonas
            'title' => $field['LABEL'], // Lauko pavadinimas
            'id' => '', // [id] atributas
            'rows' => '', // [rows] atributas
            'type' => 'text', // input[type] atributas
            'class' => '', // [class] atributas
            'onkeypress' => '', // [onkeypress] atributas
            'max_length' => '', // [maxlength] atributas
            'note_class' => '', // Failui
            'note_text' => '', // Failui
            //'name' => strtolower($field['NAME']), // [name] atributas
            'name' => strtoupper($name), // [name] atributas
            'value' => $value, // Lauko reikšmė
            'disabled' => '', // [disabled] atributas
            'readonly' => '',
            'children' => '',
            'unallowed' => false // [unallowed] atributas
        );

        $tpl_html_data = array(); // Papildomi šablono kintamieji
        $result = null; // Galutinis HTML'as

        /*
         * Lauko informacija pagal rūšį ir tipą
         */
        // Skaičius
        if ($field['SORT'] == 1 && $field['TYPE'] != 6) {
            $tpl['filename'] = 'input';

            $tpl['type'] = 'text';
            $tpl['onkeypress'] = ' onkeypress="return isNumberKey(event)"';

            $max_length = array(1 => 3, 2 => 5, 3 => 7, 4 => 11, 5 => 19);
        } else
        // Skaičius su kableliu
        if ($field['SORT'] == 1 && $field['TYPE'] == 6) {
            $tpl['filename'] = 'input';

            $tpl['type'] = 'text';
            $tpl['onkeypress'] = ' onkeypress="return isFloatNumberKey(event)"';
            $tpl['value'] = empty($value) === false ? $value : '0.00';
        } else
        // Tekstinis tipas
        if ($field['SORT'] == 2) {
            // Smulkus tekstas
            if ($field['TYPE'] == 7)
                $tpl['filename'] = 'input';
            else {
                $tpl['filename'] = 'textarea';
            }

            $rows = array(8 => 2, 9 => 4, 10 => 6);
            $max_length = array(7 => 256, 8 => 65000, 9 => 16000000, 10 => 4000000000);
        } else
        // Klasifikatorius
        if ($field['SORT'] == 4 && $field['TYPE'] == 18) {
            $tpl['filename'] = 'select';

            $classifications = $this->core->logic2->get_classificator(array('PARENT_ID' => $field['SOURCE']));
            if (empty($field['DEFAULT']) === false && empty($field['DEFAULT_ID']) === false && empty($value) === true)
                $value = $field['DEFAULT_ID'];

            $str_val = 0;
            foreach ($classifications as $classification) {
                $selected = '';
                if ($value == $classification['ID']) {
                    $selected = ' selected="selected"';
                    $str_val = $classification['ID'];
                }
                $tpl_html_data[] = [
                    'value' => $classification['ID'],
                    'selected' => $selected,
                    'label' => $classification['LABEL']
                ];
            }
            $value = $str_val;
        } else
        // "Teisės aktas" (nenaudojamas)
        if ($field['SORT'] == 4 && $field['TYPE'] == 22) {
            throw new \Exception('Teisės akto laukas nebepalaikomas');
        } else
        // Veika
        if ($field['SORT'] == 4 && $field['TYPE'] == 23) {
            $tpl['filename'] = 'select';

            $data = $this->core->logic->get_deed_search_values();
            if (empty($field['DEFAULT']) === false && empty($field['DEFAULT_ID']) === false && empty($value) === true) {
                $value = $field['DEFAULT_ID'];
            }

            $str_val = 0;
            foreach ($data as $arr) {
                $selected = '';
                if ($value == $arr['ID']) {
                    $selected = ' selected="selected"';
                    $str_val = $arr['VALUE'];
                }
                $tpl_html_data[] = [
                    'value' => $arr['VALUE'],
                    'selected' => $selected,
                    'label' => $arr['LABEL']
                ];
            }
            $value = $str_val;
        } else
        // Checkbox ir radio
        if ($field['SORT'] == 5 || $field['SORT'] == 8) {
            $tpl['filename'] = 'pick';

            $pk_type = '';
            if ($field['SORT'] == 5) {
                $pk_type = 'checkbox';
                $tpl['name'] = $tpl['name'] . '[]';
                $value = json_decode(stripslashes($value));
            }

            if ($field['SORT'] == 8) {
                $pk_type = 'radio';
                $value = array($value);
            }
            $classifications = $this->core->logic2->get_classificator(array('PARENT_ID' => $field['SOURCE']));
            foreach ($classifications as $classification) {
                $pk_checked = '';
                //if (in_array($classification['LABEL'], $value)) {
                if (in_array($classification['ID'], $value)) {
                    $pk_checked = ' checked="checked"';
                    $value = (int) $classification['ID'];
                }

                $tpl_html_data[] = array(
                    'pk_type' => $pk_type,
                    'pk_id' => $classification['ID'],
                    'pk_label' => $classification['LABEL'],
                    'pk_class' => '',
                    'pk_name' => $tpl['name'],
                    'pk_checked' => $pk_checked,
                    'pk_value' => $classification['ID']);
            }
        } else
        // Straipsnis
        if ($field['SORT'] == 6) {
            $tpl['filename'] = 'select';

            // Vartotojo straipsniai
            $user_clauses = $this->core->logic->get_user_clauses(array('PEOPLE_ID' => $param['user']['ID'],
                'STRUCTURE_ID' => $param['user']['STRUCTURE_ID']));

            // Reikšmė pagal nutylėjimą
            if (empty($field['DEFAULT']) === false && empty($field['DEFAULT_ID']) === false && empty($value) === true)
                $value = $field['DEFAULT_ID'];

            //$str_val = 0;
            foreach ($user_clauses as $clause) {
                $selected = '';
                if ($value == $clause['ID']) {
                    $selected = ' selected="selected"';
                    //$str_val = $clause['ID'];
                    $value = (int) $clause['ID'];
                }
                $tpl_html_data[] = array(
                    'value' => $clause['ID'],
                    'selected' => $selected,
                    'label' => $clause['NAME']);
            }
            //$value = (int) $str_val;
        } else
        // Datos ir laiko laukai
        if ($field['SORT'] == 7) {
            $tpl['filename'] = 'input';

            $is_empty = false;
            if (empty($value) === true)
                $is_empty = true;

            $value = strtotime($value);
            $max_length = array(11 => 4, 12 => 10, 13 => 8, 14 => 19);

            // Metai
            if ($field['TYPE'] == 11) {
                $tpl['class'] .= ' atp-date-year';
                $value = date('Y', $value);
            } else
            // Data
            if ($field['TYPE'] == 12) {
                $tpl['class'] .= ' atp-date';
                $value = date('Y-m-d', $value);
            } else
            // Laikas
            if ($field['TYPE'] == 13) {
                $tpl['class'] .= ' atp-time';
                $value = date('H:i', $value);
            } else
            // Data ir laikas
            if ($field['TYPE'] == 14) {
                $tpl['class'] .= ' atp-datetime';
                $value = date('Y-m-d H:i', $value);
            }

            if ($is_empty === true)
                $value = '';

            $tpl['value'] = $value;
        } else
        // Antraštė
        if ($field['SORT'] == 9) {
            $tpl['filename'] = 'text';
            $tpl['title'] = $tpl['title'];
            $tpl['value'] = $field['TEXT'];
        } else
        // Failas
        if ($field['SORT'] == 10) {
            $tpl['filename'] = 'input';

            $tpl['type'] = 'file';
            $tpl['name'] = $tpl['name'] . '[]';
            $tpl['class'] = '-file' . $tpl['class'];

            $file_params = $this->core->logic->get_file_params(array(
                'DOCUMENT_ID' => $field['ITEM_ID'],
                'STRUCTURE_ID' => $field['ID'],
                'LIMIT' => 1));
            if (!empty($file_params)) {
                $file_params = array_shift($file_params);

                $tpl['note_class'] = ' atp-note-show';
                $tpl['note_text'] = 'Failų kiekis: <span class="atp-file-cnt">' . $file_params['CNT'] . '</span>, dydis: ' . $file_params['SIZE_FROM'] . ' - ' . $file_params['SIZE_TO'] . ' Mb';
            }
            /*
              // Dokumento įrašo lauko ir failų ryšiai
              $files = $this->core->logic2->get_record_files(array(
              'TABLE_ID' => $table['ID'],
              'RECORD_ID' => $table_record['ID'],
              'FIELD_ID' => $structure['ID']));

              // TODO: atskirti failo išvedimą pagal failo tipą
              $file_list = '';
              if (empty($files) === false) {
              foreach ($files as &$file) {
              // Failo informacija
              $file_info = $this->core->logic2->get_files(array('ID' => $file['FILE_ID']));

              if (// strpos($file_info['TYPE'], 'image') === 0 &&
              $structure['TYPE'] === '20') {
              // Sugeneruoja thumbnail'ą
              $path = 'files/tmp/';
              $thumb_name = $this->core->action->createThumbnail($this->core->config['REAL_URL'] . $file_info['PATH'], $this->core->config['REAL_URL'] . $path);
              $thumb_path = $this->core->config['SITE_URL'] . $path . $thumb_name;
              // Failo išvedimas
              $file_list .= '<li for="' . $file['ID'] . '">'
              . '<a href="' . $this->core->config['SITE_URL'] . $file_info['PATH'] . '" target="_blank">'
              . '<img class="photo" style="background: url(' . $thumb_path . ') center center no-repeat;"/>'
              . $file_info['NAME']
              . '</a>'
              . '</li>';
              } elseif ($structure['TYPE'] === '21') {
              $file_list .= '<li for="' . $file['ID'] . '">'
              . '<a href="' . $this->core->config['SITE_URL'] . $file_info['PATH'] . '" target="_blank">'
              . $file_info['NAME']
              . '</a>'
              . '</li>';
              }
              }
              }

              if (empty($file_list) === false) {
              if ($structure['TYPE'] === '20') {
              $file_list = '<div class="file-list"><ul id="photos_b" class="photo-list fl">' . $file_list . '</ul></div>';
              } elseif ($structure['TYPE'] === '21') {
              $file_list = '<div class="file-list"><ul class="file-list">' . $file_list . '</ul></div>';
              }
              }
              $template_data['file_list'] = &$file_list;
             */
        }

        // Šablono generavimas
        if (!empty($tpl['filename'])) {
            $tmpl = $this->core->getTemplate('template2_' . $tpl['filename']);

            // [row] atributo formavimas
            if (!empty($rows[$field['TYPE']]))
                $tpl['rows'] = ' rows="' . $rows[$field['TYPE']] . '"';
            // [maxlength] atributo formavimas
            if (!empty($max_length[$field['TYPE']]))
                $tpl['max_length'] = ' maxlength="' . $max_length[$field['TYPE']] . '"';
            // [unallowed] atributo formavimas
            if ($tpl['unallowed'] === true)
                $tpl['disabled'] .= ' unallowed="unallowed"';

            // Jei ne veika/klasifikatorius, checkbox, straipsnis, radio button
            if (in_array($field['SORT'], array(4, 5, 6, 8)) === false) {
                if (isset($tpl['value']) === true && empty($tpl['value']) === true && empty($field['DEFAULT']) === false)
                    $tpl['value'] = $field['DEFAULT'];
                $result = $this->core->returnHTML($tpl, $tmpl[0]);
            } else {
                $tpl['lng_not_chosen'] = $this->core->lng('not_chosen');
                $result = $this->core->returnHTML($tpl, $tmpl[0]);
                foreach ($tpl_html_data as $template_dt) {
                    foreach ($template_dt as $val => $dt)
                        $tpl[$val] = $dt;

                    $result .= $this->core->returnHTML($tpl, $tmpl[1]);
                }
                $result .= $this->core->returnHTML($tpl, $tmpl[2]);
            }
        }

        return $result;
    }

    /**
     * @param string|string[] $message
     */
    public function writeATPactionLog($message)
    {
        $adminJournalManager = $this->core->factory->AdminJournal();
        $journalRecord = $adminJournalManager->getDefaultRecord();

        // TODO: join by '<br/>'; remove '<br/>' (and alternatives) from $message where call to writeATPactionLog is performed.
        $journalRecord
            ->setMessage((is_array($message) ? join('', $message) : $message));
        $adminJournalManager->addRecord($journalRecord);
    }

    /**
     * @deprecated nenaudojamas
     * @return type
     */
    public function login()
    {
        $userId = filter_input(INPUT_GET, 'user_id', FILTER_VALIDATE_INT);

        if ($userId) {
            $userManager = $this->core->factory->User();
            $userData = $userManager->loginById($userId);
            if (isset($userData->person) && empty($userData->person->personDuties) === false) {
                $personDuty = current($userData->person->personDuties);

                $userManager->setLoggedPersonDuty($personDuty->department->id);
            }

            header('Location: ' . $this->core->config['SITE_URL'] . 'index.php');
            exit;
        }

        $tpl_arr = $this->core->getTemplate('login');

        $html_data['sys_message'] = $this->core->get_messages();
        $html_manage = $this->core->returnHTML($html_data, $tpl_arr[0]);

        $result = $this->core->db_query("
            SELECT
                D.ID,
                B.FIRST_NAME,
                B.LAST_NAME,
                C.SHOWS STRUCTURE_NAME
			FROM
                " . PREFIX . "MAIN_WORKERS A
                    INNER JOIN " . PREFIX . "MAIN_PEOPLE B ON B.ID = A.PEOPLE_ID
                    INNER JOIN " . PREFIX . "MAIN_STRUCTURE C ON C.ID = A.STRUCTURE_ID
                    INNER JOIN " . PREFIX . "ADM_USERS D ON D.PEOPLE_ID = B.ID");
        $user_privileges[$res['TABLE_ID']] = $res;
        $count = $this->core->db_num_rows($result);
        for ($i = 0; $i < $count; $i++) {
            $row = $this->core->db_next($result);
            $html_manage .= $this->core->returnHTML(array(
                'user_login' => 'index.php?m=99&user_id=' . $row['ID'],
                'user_surname' => $row['LAST_NAME'],
                'user_name' => $row['FIRST_NAME'],
                'user_department' => $row['STRUCTURE_NAME']
                ), $tpl_arr[1]);
        }

        $html_manage .= $tpl_arr[2];

        return $this->core->atp_content($html_manage);
    }
}
