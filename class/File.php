<?php

namespace Atp;

use \Atp\File\IHandler;

class File
{
    const FILE_PATH_FULL_SYS = 1;

    const FILE_PATH_FULL_URL = 2;

    const FILE_PATH_LOCAL_SYS = 3;

    const FILE_PATH_LOCAL_URL = 4;

    private $handlers = [];

    private $current = [];

    private $atp;

    private $cache;

    /**
     *
     * @param \Atp\Core $atp
     * @param IHandler $handler [optional]<p>Vykdytojo objektas. <b>Default:</b> NULL</p>
     */
    public function __construct(\Atp\Core $atp, IHandler $handler = null)
    {
        $this->atp = $atp;
        $this->cache = $this->atp->factory->Cache();

        $this->SetHandler($handler);
    }

    /**
     * Vykdytojo objekto klasės pavadinimas
     * @param IHandler $handler Vykdytojo objektas
     * @return string
     */
    private function get_handler_name(IHandler $handler)
    {
        return get_class($handler);
    }

    /**
     * Nustato naudojamą vykdytoją
     * @param string|IHandler $handler Vykdytojo klasės pavadinimas, jei vykdytojas jau yra pridėtas, arba vykdytojo objektas
     * @return boolean
     */
    public function SetHandler($handler)
    {
        if (empty($handler) === true) {
            return false;
        }

        if (is_object($handler) === true) {
            if ($handler instanceof IHandler === false) {
                return false;
            }

            $name = $this->get_handler_name($handler);
            $this->PushHandler($handler);
        } else {
            $name = $handler;
            $handler = $this->GetHandler($name);
            if ($handler === false) {
                return false;
            }
        }

        $this->current = $name;

        return true;
    }

    /**
     * Gauna pridėta vykdytojo objektą
     * @param string $name Vykdytojo clasės pavadinimas
     * @return boolean
     */
    public function GetHandler($name)
    {
        if (empty($name) === true || isset($this->handlers[$name]) === false) {
            return false;
        }

        return $this->handlers[$name];
    }

    /**
     * Nustatyto vykdytojo objektas
     * @return bool|IHandler
     */
    public function GetCurrentHandler()
    {
        return $this->GetHandler($this->current);
    }

    /**
     * Prideda vykdytoją
     * @param IHandler $handler Vykdytojo klasės objektas
     * @return boolean
     */
    public function PushHandler(IHandler $handler)
    {
        $name = $this->get_handler_name($handler);
        if (isset($this->handlers[$name]) === false) {
            $this->handlers[$name] = $handler;
        }

        return true;
    }

    /**
     * Priskirtų failų paieška
     * @param mixed $param Parametrų masyvas
     * @return array ID masyvas
     * @throws Exception
     */
    public function GetRelatedFiles($param)
    {
        $handler = $this->GetCurrentHandler();
        if ($handler === false) {
            throw new \Exception('Nenustatytas handler\'is');
        }

        return $handler->GetRelatedFiles($param);
    }

    /**
     * Failo informacija
     * @param int $id Failo ID
     * @return object
     * @throws Exception
     */
    public function GetFileInfo($id)
    {
        $handler = $this->GetCurrentHandler();
        if ($handler === false) {
            throw new \Exception('Nenustatytas handler\'is');
        }

        $cache_name = $this->get_handler_name($handler) . __METHOD__;
        $data = $this->cache->get($cache_name, $id);
        if ($data !== false) {
            return $data;
        }

        $data = $handler->GetFileInfo($id);
        $this->cache->set($cache_name, $id, $data);

        return $data;
    }

    /**
     * @param string $path Kelias iki failo
     * @param string $name Failo pavadinimas
     * @param string $type [optional] Failo mime-type
     */
    public function Save($path, $name, $type = null)
    {
        $slug = new \Slug();
        $info = pathinfo($name);
        $dir = 'files/files/' . date('Y/m/d/H/i/');
        $global_dir = $this->atp->config['REAL_URL'] . $dir;

        if (empty($type)) {
            $type = (new \finfo(FILEINFO_MIME_TYPE))->file($path);
        }

        $extension = $this->getExtension($path, $name, $type);
        if (empty($info['extension']) && empty($extension) === false) {
            $name .= '.' . $extension;
        }

        // Failo perkėlimas
        if (is_dir($global_dir) === false) {
            $old_mask = umask(0);
            mkdir($global_dir, 0777, true);
            umask($old_mask);
        }

        $filename = $slug->makeSlugs($info['filename']);

        $new_path = $global_dir . $filename . '.' . $extension;
        while (is_file($new_path)) {
            $new_filename = $filename . '_[' . substr(hash('sha256', microtime()), 0, 5) . ']';
            $new_path = $global_dir . $new_filename . '.' . $extension;
        }

        $old_mask = umask(0);
        $result = copy($path, $new_path);
        umask($old_mask);

        if ($result === false) {
            return false;
        }

        $local_path = $dir . pathinfo($new_path, PATHINFO_BASENAME);

        $row = [
            'TYPE' => $type,
            'SIZE' => filesize($path),
            'NAME' => $name,
            'PATH' => $local_path
        ];
        $id = $this->atp->db_quickInsert(PREFIX . 'ATP_FILES', $row);

        return $id;
    }

    /**
     * @param string $path
     * @param string $fileName
     * @param string $mimeType
     * @return null|string
     */
    private function getExtension($path, $fileName, $mimeType)
    {
        $info = pathinfo($fileName);
        if (empty($info['extension']) === false) {
            return $info['extension'];
        }

        if (empty($mimeType)) {
            $mimeType = (new \finfo(FILEINFO_MIME_TYPE))->file($path);
        }

        if (empty($mimeType) === false) {
            $systemExtension = $this->systemMimeTypeExtension($mimeType);
            if (empty($systemExtension) === false) {
                return $systemExtension;
            }
        }
    }

    /**
     * @return array
     */
    private function systemMimeTypeExtensionMap()
    {
        $out = array();
        $file = fopen('/etc/mime.types', 'r');
        while (($line = fgets($file)) !== false) {
            $line = trim(preg_replace('/#.*/', '', $line));
            if (!$line) {
                continue;
            }

            $parts = preg_split('/\s+/', $line);
            if (count($parts) == 1) {
                continue;
            }

            $type = array_shift($parts);
            if (!isset($out[$type])) {
                $out[$type] = array_shift($parts);
            }
        }
        fclose($file);

        return $out;
    }

    /**
     * @param string $mimeType
     * @return null|string
     */
    private function systemMimeTypeExtension($mimeType)
    {
        $map = $this->systemMimeTypeExtensionMap();
        return (isset($map[$mimeType]) ? $map[$mimeType] : null);
    }

    /**
     *
     * @param string $path Posistemės kelias iki failo
     * @param const $type [optional]<p>Kelio iki failo formatas.<br/>
     * <i>FILE_PATH_FULL_SYS</i> - pilnas sistemos kelias;<br/>
     * <i>FILE_PATH_LOCAL_SYS</i> - kelias nuo WebPartner šakninio katalogo;<br/>
     * <i>FILE_PATH_LOCAL_URL</i> - pilnas URI;<br/>
     * <i>FILE_PATH_FULL_URL</i> - URI nuo WebPartner šakninio katalogo.<br/>
     * <b>Default:</b> FILE_PATH_FULL_SYS</p>
     * @return boolean|string
     */
    public function ParseFilePath($path, $type = self::FILE_PATH_FULL_SYS)
    {
        $atp = $this->atp;

        if ($type === self::FILE_PATH_FULL_SYS) {
            return $atp->config['REAL_URL'] . $path;
        } elseif ($type === self::FILE_PATH_LOCAL_SYS || $type === self::FILE_PATH_LOCAL_URL) {
            return $atp->config['SUBSYSTEM'] . $path;
        } elseif ($type === self::FILE_PATH_FULL_URL) {
            return $atp->config['SITE_URL'] . $path;
        }

        return false;
    }
}
