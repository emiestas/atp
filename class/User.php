<?php

namespace Atp;

/**
 * User             - ADM_USER          - Vartotojas
 * Person           - MAIN_PEOPLE       - Asmuo
 * Person duty      - MAIN_WORKERS      - Darbuotojas
 * Duty             - MAIN_OFFICE       - Pareigos
 * Department       - MAIN_STRUCTURE    - Skyrius
 */
class User
{
    /**
     * @var \Atp\Entity\User
     */
    private $loggedUser;

    /**
     * @var \Atp\Core
     */
    private $core;

    /**
     * @param \Atp\Core $core
     */
    public function __construct(\Atp\Core $core)
    {
        $this->core = $core;

        if (isset($_SESSION['ATP']['LOGGED_USER'])) {
            $this->loggedUser = $this->getUserData($_SESSION['ATP']['LOGGED_USER']);
        }
        if (empty($_SESSION['ATP']['LOGED_USER_DEPARTMENT'])) {
            $this->setLoggedPersonDuty();
        } else {
            $this->setLoggedPersonDuty($_SESSION['ATP']['LOGED_USER_DEPARTMENT']);
        }
    }

    /**
     * Gauti prisijungusio vartotojo aktyvaus darbuotojo duomenis.
     * 
     * @return boolean|\Atp\Entity\PersonDuty
     */
    public function getLoggedPersonDuty()
    {
        $userData = $this->getLoggedUser();
        if ($userData && $userData->person && $userData->person->personDuties) {
            foreach ($userData->person->personDuties as $personDuty) {
                if ($personDuty->department->id === (int) $_SESSION['ATP']['LOGED_USER_DEPARTMENT']) {
                    return $personDuty;
                }
            }
        }

        return false;
    }

    /**
     * Nurodyti prisijungusio vartotojo aktyvų darbuotoją pagal skyrių.
     * 
     * @param int $departmentId
     */
    public function setLoggedPersonDuty($departmentId = null)
    {
        foreach ($this->getLoggedUser()->person->personDuties as $personDuty) {
            if ($personDuty->isTemporary) {
                continue;
            }

            if ($departmentId === null) {
                $_SESSION['ATP']['LOGED_USER_DEPARTMENT'] = $personDuty->department->id;
                break;
            } else
            if ($personDuty->department->id === (int) $departmentId) {
                $_SESSION['ATP']['LOGED_USER_DEPARTMENT'] = (int) $departmentId;
                break;
            }
        }
    }

    /**
     * Gauti prisijungusio vartotojo aktyvaus darbuotojo administravimo teises.
     * 
     * @param string $statusName [optional] If provided, only specific status value will be returned.
     * @return boolean|int|int[]
     */
    public function getLoggedPersonDutyStatus($statusName = null)
    {
        $statusData = $this->getUserStatus(
            $this->getLoggedUser()->person->id, $this->getLoggedPersonDuty()->department->id
        );

        if (isset($statusName)) {
            return $statusData[$statusName];
        }

        return $statusData;
    }

    /**
     * Gauti prisijungusio vartotojo aktyvays darbuotojo dokumentų teises.
     * 
     * @param string $documentId [optional] If provided, data will be returned only for specific document.
     * @return array[]|string[]
     */
    public function getLoggedPersonDutyPrivileges($documentId = null)
    {
        $privilegesData = $this->getUserPrivileges(
            $this->getLoggedUser()->person->id, $documentId, $this->getLoggedPersonDuty()->department->id
        );

        return $privilegesData;
    }

    /**
     * Gauti darbuotojo administravimo teises pagal asmens id ir skyriaus id.
     * 
     * @param int $personId
     * @param int $departmentId
     * @return boolean|array
     */
    public function getUserStatus($personId, $departmentId)
    {
        $result = $this->core->db_query_fast('
            SELECT *
            FROM
                ' . PREFIX . 'ATP_USER_STATUS
            WHERE
                M_PEOPLE_ID = "' . $this->core->db_escape($personId) . '" AND
                M_DEPARTMENT_ID = "' . $this->core->db_escape($departmentId) . '"
            LIMIT 1');
        $statusData = $this->core->db_next($result);

        return $statusData;
    }

    /**
     * Gauti dokumentų teises pagal asmens id, skyriaus id ir dokumento id.
     * 
     * @param int $personId
     * @param int $documentId [optional]
     * @param int $departmentId [optional]
     * @return array[]|string[]
     */
    public function getUserPrivileges($personId, $documentId = 0, $departmentId = 0)
    {
        $result = $this->core->db_query_fast('
            SELECT
                DOCUMENT_ID,
                VIEW_RECORD,
                EDIT_RECORD,
                DELETE_RECORD,
                INVESTIGATE_RECORD,
                EXAMINATE_RECORD,
                END_RECORD
			FROM
                ' . PREFIX . 'ATP_USER_PRIVILEGES
			WHERE
                M_PEOPLE_ID = ' . (int) $personId . '
                ' . (empty($departmentId) ? null : ' AND M_DEPARTMENT_ID = ' . (int) $departmentId) . '
                ' . (empty($documentId) ? null : ' AND DOCUMENT_ID = ' . (int) $documentId . ' LIMIT 1') . '
            ');

        $return = array();
        while ($row = $this->core->db_next($result)) {
            $return[$row['DOCUMENT_ID']] = $row;
            unset($return[$row['DOCUMENT_ID']]['DOCUMENT_ID']);

            if (empty($documentId) === FALSE) {
                return $return[$documentId];
            }
        }

        return $return;
    }

    /**
     * Gauti prisijungusio vartotoo duomenis.
     * 
     * @return null|\Atp\Entity\User
     */
    public function getLoggedUser()
    {
        return $this->loggedUser ? $this->loggedUser : null;
    }

    /**
     * Is user logged in.
     * 
     * @return boolean
     */
    public function isLoggedIn()
    {
        return $this->loggedUser ? true : false;
    }

    /**
     * Is user not logged in.
     * 
     * @return boolean
     */
    public function isGuest()
    {
        return !$this->isLoggedIn();
    }

    /**
     * Login user by credentials.
     * 
     * @param string $username
     * @param string $password
     * @return boolean|\Atp\Entity\User
     */
    public function login($username, $password)
    {
        if (($data = $this->authenticate($username, $password)) === false) {
            return false;
        }

        $this->loggedUser = $data;
        $_SESSION['ATP']['LOGGED_USER'] = $this->loggedUser->id;

        return $data;
    }

    /**
     * Login user by user is.
     * 
     * @param int $userId
     * @return boolean|\Atp\Entity\User
     */
    public function loginById($userId)
    {
        $data = $this->getUserData($userId);

        $this->loggedUser = $data;
        $_SESSION['ATP']['LOGGED_USER'] = $this->loggedUser->id;

        return $data;
    }

    /**
     * Logout user.
     */
    public function logout()
    {
        $this->loggedUser = null;
        unset($_SESSION['ATP']['LOGGED_USER']);
    }

    /**
     * Authenticate credentials.
     * 
     * @param string $username User name.
     * @param string $password Password in plain text.
     * @return boolean|\Atp\Entity\User
     */
    public function authenticate($username, $password)
    {
        $result = $this->core->db_query_fast('
            SELECT ID
            FROM ' . PREFIX . 'ADM_USERS
            WHERE
                USERNAME = "' . $this->core->db_escape($username) . '" AND
                PASSWORD = "' . $this->core->db_escape($this->core->makeHash($password)) . '"
            ');

        if ($this->core->db_num_rows($result) === 0) {
            return false;
        }

        $userId = $this->core->db_next($result)['ID'];
        $userData = $this->getUserData($userId);

        return $userData;
    }

    /**
     * Gauti vartotojo duomenis.
     * 
     * @param int $userId Vartotojo id.
     * @return boolean|\Atp\Entity\User
     */
    public function getUserData($userId)
    {
        $result = $this->core->db_query_fast('
            SELECT
                A.ID            USER_ID,
                A.FNAME         USER_FIRST_NAME,
                A.LNAME         USER_LAST_NAME,
                A.EMAIL         USER_EMAIL,
                B.ID            PERSON_ID,
                B.CODE          PERSON_CODE,
                B.SHOWS         PERSON_NAME,
                B.FIRST_NAME    PERSON_FIRST_NAME,
                B.LAST_NAME     PERSON_LAST_NAME,
                B.EMAIL         PERSON_EMAIL,
                B.PHONE_MOBILE  PERSON_PHONE_MOBILE,
                B.PHONE_SHORT   PERSON_PHONE_SHORT,
                B.PHONE_FULL    PERSON_PHONE_FULL
            FROM
                ' . PREFIX . 'ADM_USERS A
                    LEFT JOIN ' . PREFIX . 'MAIN_PEOPLE B ON
                        B.ID = A.PEOPLE_ID
            WHERE
                A.ID = "' . $this->core->db_escape($userId) . '"
            LIMIT 1
            ');

        if ($this->core->db_num_rows($result) === 0) {
            return false;
        }

        $data = $this->core->db_next($result);

        $user = new \Atp\Entity\User();
        $user->id = (int) $data['USER_ID'];
        $user->firstName = $data['USER_FIRST_NAME'];
        $user->lastName = $data['USER_LAST_NAME'];
        $user->email = $data['USER_EMAIL'];
        $user->person = null;

        if (empty($data['PERSON_ID']) === false) {
            $user->person = $person = new \Atp\Entity\Person();
            $person->id = (int) $data['PERSON_ID'];
            $person->code = $data['PERSON_CODE'];
            $person->name = $data['PERSON_NAME'];
            $person->firstName = $data['PERSON_FIRST_NAME'];
            $person->lastName = $data['PERSON_LAST_NAME'];
            $person->email = $data['PERSON_EMAIL'];

            $phones = null;
            if (empty($data['PERSON_PHONE_MOBILE']) === false) {
                $phone = new \Atp\Entity\Phone();
                $phone->type = \Atp\Entity\Phone::TYPE_MOBILE;
                $phone->number = $data['PERSON_PHONE_MOBILE'];
                $phones[] = $phone;
            }
            if (empty($data['PERSON_PHONE_SHORT']) === false) {
                $phone = new \Atp\Entity\Phone();
                $phone->type = \Atp\Entity\Phone::TYPE_SHORT;
                $phone->number = $data['PERSON_PHONE_SHORT'];
                $phones[] = $phone;
            }
            if (empty($data['PERSON_PHONE_FULL']) === false) {
                $phone = new \Atp\Entity\Phone();
                $phone->type = \Atp\Entity\Phone::TYPE_FULL;
                $phone->number = $data['PERSON_PHONE_FULL'];
                $phones[] = $phone;
            }
            $person->phones = $phones;
            $person->personDuties = $this->getPersonDuties($person->id);
        }

        return $user;
    }

    /**
     * Gauti vartotojo duomenis pagal asmens id.
     * 
     * @param int $personId Asmens id.
     * @return boolean|\Atp\Entity\User
     */
    public function getUserDataByPersonId($personId)
    {
        $result = $this->core->db_query_fast('
            SELECT
                A.ID
            FROM
                ' . PREFIX . 'ADM_USERS A
            WHERE
                A.PEOPLE_ID = "' . $this->core->db_escape($personId) . '"
            LIMIT 1
        ');

        if ($this->core->db_num_rows($result) === 0) {
            return false;
        }

        $data = $this->core->db_next($result);

        return $this->getUserData($data['ID']);
    }

    /**
     * Gauti vartotojo duomenis pagal darbuotojo id.
     * 
     * @param int $emloeeDutyId Darbuotojo id.
     * @return boolean|\Atp\Entity\User
     */
    public function getUserDataByPersonDutyId($emloeeDutyId)
    {
        $result = $this->core->db_query_fast('
            SELECT
                A.ID
            FROM
                ' . PREFIX . 'ADM_USERS A
                    INNER JOIN ' . PREFIX . 'MAIN_PEOPLE B ON
                        B.ID = A.PEOPLE_ID
                    INNER JOIN ' . PREFIX . 'MAIN_WORKERS C ON
                        C.PEOPLE_ID = B.ID
            WHERE
                C.ID = "' . $this->core->db_escape($emloeeDutyId) . '"
            LIMIT 1
        ');

        if ($this->core->db_num_rows($result) === 0) {
            return false;
        }

        $data = $this->core->db_next($result);

        return $this->getUserData($data['ID']);
    }

    /**
     * Gauti darbuotojo duomenis.
     * 
     * @param int $personDutyId Darbuotojo id.
     * @return null|\Atp\Entity\PersonDuty
     */
    public function getPersonDuty($personDutyId)
    {
        $duty = null;

        $result = $this->core->db_query_fast('
            SELECT
                    A.ID                    PERSON_DUTY_ID,
                    B.ID                    DEPARTMENT_ID,
                    B.SHOWS                 DEPARTMENT_LABEL,
                    C.ID                    DUTY_ID,
                    C.SHOWS                 DUTY_LABEL
                FROM
                    ' . PREFIX . 'MAIN_WORKERS A
                        INNER JOIN ' . PREFIX . 'MAIN_STRUCTURE B ON
                            B.ID = A.STRUCTURE_ID
                        INNER JOIN ' . PREFIX . 'MAIN_OFFICE C ON
                            C.ID = A.OFFICE_ID
                WHERE
                    A.ID = "' . $this->core->db_escape($personDutyId) . '"');

        $count = $this->core->db_num_rows($result);

        if ($count) {
            $dutyData = $this->core->db_next($result);

            $personDuty = new \Atp\Entity\PersonDuty();
            $personDuty->id = (int) $dutyData['PERSON_DUTY_ID'];
            $personDuty->isTemporary = null;

            $personDuty->duty = $duty = new \Atp\Entity\Duty();
            $duty->id = (int) $dutyData['DUTY_ID'];
            $duty->label = $dutyData['DUTY_LABEL'];

            $personDuty->department = $department = new \Atp\Entity\Department();
            $department->id = (int) $dutyData['DEPARTMENT_ID'];
            $department->label = $dutyData['DEPARTMENT_LABEL'];
        }

        return $personDuty;
    }

    /**
     * Gauti darbuotojus pagal asmenį.
     *
     * @param int $personId Asmens id.
     * @param bool $includeTemporary [optional] Default: true
     * @return \Atp\Entity\PersonDuty
     */
    public function getPersonDuties($personId, $includeTemporary = true)
    {
        $dutyList = null;

        $sqls = [
            'main' => '
                SELECT
                    A.ID            PERSON_DUTY_ID,
                    B.ID            DEPARTMENT_ID,
                    B.SHOWS         DEPARTMENT_LABEL,
                    C.ID            DUTY_ID,
                    C.SHOWS         DUTY_LABEL,
                    0               IS_TEMPORARY
                FROM
                    ' . PREFIX . 'MAIN_WORKERS A
                        INNER JOIN ' . PREFIX . 'MAIN_STRUCTURE B ON
                            B.ID = A.STRUCTURE_ID
                        INNER JOIN ' . PREFIX . 'MAIN_OFFICE C ON
                            C.ID = A.OFFICE_ID
                WHERE
                    A.PEOPLE_ID = "' . $this->core->db_escape($personId) . '"',
            'temporary' => '
                SELECT
                    B.ID            PERSON_DUTY_ID,
                    C.ID            DEPARTMENT_ID,
                    C.SHOWS         DEPARTMENT_LABEL,
                    D.ID            DUTY_ID,
                    D.SHOWS         DUTY_LABEL,
                    1               IS_TEMPORARY
                FROM
                    ' . PREFIX . 'MAIN_DEPUTY A
                        INNER JOIN ' . PREFIX . 'MAIN_WORKERS B ON
                            B.PEOPLE_ID = A.PEOPLE_ID
                        INNER JOIN ' . PREFIX . 'MAIN_STRUCTURE C ON
                            C.ID = B.STRUCTURE_ID
                        INNER JOIN ' . PREFIX . 'MAIN_OFFICE D ON
                            D.ID = B.OFFICE_ID
                WHERE
                    A.DEPUTY_ID = "' . $this->core->db_escape($personId) . '" AND
                    NOW() BETWEEN FROM_DATE AND TILL_DATE'
        ];

        if ($includeTemporary === false) {
            unset($sqls['temporary']);
        }

        $result = $this->core->db_query_fast(join(' UNION ', $sqls));

        $count = $this->core->db_num_rows($result);

        if ($count) {
            for ($i = 0; $i < $count; $i++) {
                $dutyData = $this->core->db_next($result);

                $dutyList[] = $personDuty = new \Atp\Entity\PersonDuty();
                $personDuty->id = (int) $dutyData['PERSON_DUTY_ID'];
                if ($dutyData['IS_TEMPORARY']) {
                    $personDuty->isTemporary = true;
                } else {
                    $personDuty->isTemporary = false;
                }

                $personDuty->duty = $duty = new \Atp\Entity\Duty();
                $duty->id = (int) $dutyData['DUTY_ID'];
                $duty->label = $dutyData['DUTY_LABEL'];

                $personDuty->department = $department = new \Atp\Entity\Department();
                $department->id = (int) $dutyData['DEPARTMENT_ID'];
                $department->label = $dutyData['DEPARTMENT_LABEL'];
            }
        }

        return $dutyList;
    }

    /**
     * Gauti prisijungusio vartotojo aktyvaus darbuotojo Avilio etato oid.
     *
     * @return null|string
     */
    public function getLoggedPersonDutyAvilysId()
    {
        return $this->getPersonDutyAvilysId($this->getLoggedPersonDuty()->id);
    }

    /**
     * Gauti darbuotojo Avilio etato oid.
     *
     * @param int $personDutyId Darbuotojo id.
     * @return null|string
     */
    public function getPersonDutyAvilysId($personDutyId)
    {
        $duty = null;

        $result = $this->core->db_query_fast('
            SELECT
                    A.AVILIO_ID
                FROM
                    ' . PREFIX . 'MAIN_WORKERS A
                        INNER JOIN ' . PREFIX . 'MAIN_STRUCTURE B ON
                            B.ID = A.STRUCTURE_ID
                        INNER JOIN ' . PREFIX . 'MAIN_OFFICE C ON
                            C.ID = A.OFFICE_ID
                WHERE
                    A.ID = "' . $this->core->db_escape($personDutyId) . '"');

        $count = $this->core->db_num_rows($result);

        if ($count) {
            $data = $this->core->db_next($result);
            $avilioId = trim($data['AVILIO_ID']);

            if (empty($avilioId) === false) {
                return $avilioId;
            }
        }
    }
}
