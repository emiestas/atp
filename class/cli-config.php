<?php

require_once(__DIR__ . '/../../../inc/mainconf.php');
ini_set('display_errors', true);
$atp = prepare_atp();

$fs = new \Atp\FactoryStorage($atp);
$entityManager = $fs->Doctrine();

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);
//$helperSet = new \Symfony\Component\Console\Helper\HelperSet(array(
//    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($entityManager->getConnection()),
//    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($entityManager)
//));
//return $helperSet;
