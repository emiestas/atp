<?php

namespace Atp;

use Atp\Entity\Record as RecordEntity;

/**
 * @TODO Iškelti laukų reikšmių gavimą
 * @TODO Iškelti laukų reikšmių set'inimą ir saugojimą
 */
class Record
{
    const STATUS_DELETED = 0;

    const STATUS_DISCONTINUED = 1;

    const STATUS_INVESTIGATING = 2;

    const STATUS_EXAMINING = 3;

    const STATUS_EXAMINED = 4;

    const STATUS_FINISHED = 5;

    const STATUS_IDLE = 6;

    private $atp;

    private $field;
    
    /**
     * @var \Atp\Record\Values
     */
    private $valueHelper;

    /**
     * @var \Atp\Cache
     */
    private $cache;

    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;
        $this->cache = $this->atp->factory->Cache();
        $this->field = $this->atp->factory->Field();
    }

    /**
     * @param int $recordId
     * @return \Atp\Entity\Record
     */
    public function getEntity($recordId)
    {
        /* @var $repository \Atp\Repository\RecordRepository */
        $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Record');
        return $repository->find($recordId);
    }

    /**
     * Įrašo informacija
     * @param int $id Įrašo ID
     * @return \stdClass
     * @throws Exception
     */
    public function Get($id)
    {
        if (is_array($id) === true) {
            if (isset($id['ID'])) {
                $id = (int) $id['ID'];
            }
        } elseif (ctype_digit((string) $id) === true) {
            $id = (int) $id;
        }

        $cache_name = __METHOD__;
        $data = $this->cache->get($cache_name, $id);
        if ($data !== false) {
            return $data;
        }

        $atp = $this->atp;
        $record = $atp->logic2->get_record_relation($id);
        $record_table = $atp->logic2->get_record_table($id);

        $data = new \stdClass();
        // Įrašo ID
        $data->id = (int) $record['ID'];
        // Įrašo ID
        $data->main_id = (int) $record['MAIN_ID'];
        // Įrašo ID dokumente
        $data->id_doc = (int) $record_table['ID'];
        // Tėvinio įrašo ID
        $data->parent_id = (int) $record['RECORD_FROM_ID'];
        // Dokumento ID
        $data->document_id = (int) $record['TABLE_TO_ID'];
        // Tėvinio dokumento ID
        $data->parent_document_id = (int) $record['TABLE_FROM_ID'];
        // Įrašo numeris
        $data->nr = $record['RECORD_ID'];
        // Įrašo pagrindinis numeris
        $data->main_nr = $record_table['RECORD_MAIN_ID'];
        // Įrašo versija
        $data->version = $record_table['RECORD_VERSION'];
        // Ar tai paskutinė įrašo versija
        $data->last = (bool) (int) $record_table['RECORD_LAST'];
        // Įrašo sukūrimo data ir laikas
        $data->create_date = $record['CREATE_DATE'];
        // Įrašo statusas
        $data->status = (int) $record['STATUS'];
        // Įrašo būsena
        $data->doc_status = (int) $record['DOC_STATUS'];
        // Ar apmokėta // Nebenaudojamas
        //$data->is_paid = (bool) (int) $record['IS_PAID'];
        // Ar patikrintas baustumas // Nebenaudojamas
        //$data->punisment_checked = (bool) (int) $record['PUNISMENT_CHECKED'];
        // Užregistruoto šablono ID
//        $data->registered_template = (int) $record['REGISTERED_TEMPLATE_ID'];
//        $record = new \Atp\Entity\Record();
//        $record->id = (int) $record['ID'];
//        $record->mainId = (int) $record['MAIN_ID']; // $data->main_id
////        $data->id_doc = (int) $record_table['ID'];
//        $record->recordFromId = (int) $record['RECORD_FROM_ID']; // $data->parent_id
//        $record->document = (int) $record['TABLE_TO_ID']; // $data->document_id
//        $record->tableFromId = (int) $record['TABLE_FROM_ID']; // $data->parent_document_id
//        $record->recordId = $record['RECORD_ID']; // $data->nr
////        $data->main_nr = $record_table['RECORD_MAIN_ID'];
////        $data->version = $record_table['RECORD_VERSION'];
//        $record->isLast = (bool) (int) $record['IS_LAST']; // $data->last = (bool) (int) $record_table['RECORD_LAST'];
//        $record->createDate = new \DateTime($record['CREATE_DATE']); // $data->create_date = $record['CREATE_DATE'];
//        $record->status = (int) $record['STATUS'];
//        // Įrašo būsena
//        $record->docStatus = (int) $record['DOC_STATUS']; // $data->doc_status
//        // Užregistruoto šablono ID
//        $record->registeredTemplateId = (int) $record['REGISTERED_TEMPLATE_ID']; // $data->registered_template

        $this->cache->set($cache_name, $id, $data);

        return $data;
    }

    /**
     * Suranda aukščiausią įrašo versiją pagal jo numerį
     * @param string $record_nr
     * @return int
     */
    public function GetMaxVersionByNr($record_nr)
    {
        $qry = 'SELECT MAX(A.RECORD_VERSION) AS VERSION
			FROM
                ' . PREFIX . 'ATP_TBL_' . $record['TABLE_TO_ID'] . ' A
                    INNER JOIN ' . PREFIX . 'ATP_RECORD_X_RELATION B ON
                        B.RECORD_ID = A.ID AND
                        B.DOCUMENT_ID = ' . $record['TABLE_TO_ID'] . '
                    INNER JOIN ' . PREFIX . 'ATP_RECORD C ON
                        C.ID = B.RELATION_ID
			WHERE
				C.RECORD_ID = :RECORD_ID';
        $result = $this->atp->db_query_fast($qry, ['RECORD_ID' => $record_nr]);

        if ($this->atp->db_num_rows($result)) {
            $version = (int) $this->atp->db_next($result)['VERSION'];
        } else {
            $version = 0;
        }

        return $version;
    }
//    /**
//     * @deprecated use \Atp\Record\Values::setValue()
//     * @todo Galimybe kad būtų galima setinti reikšmes, o saugojimą iškviesti rankiniu budu
//     * @param int $recordId Įrašo ID
//     * @param int $field_id Lauko ID
//     * @param null|int|string $value Lauko reikšmė
//     */
//    public function set_value($recordId, $field_id, $value)
//    {
//        $f = $this->atp->factory->Field();
//
//        //$record = $this->get($recordId);
//        $field = $f->getField($field_id);
//
//        if (strpos($field->name, 'COL_') === 0) {
//            $callback = 'set_doc_value';
//        } else {
//            $callback = 'set_extra_value';
//        }
//
//        $result = $this->$callback($recordId, $field->id, $value);
//
//        return;
//    }
//    private $values = [];
//    /**
//     * @deprecated use \Atp\Record\Values::flushValues()
//     */
//    public function save_values()
//    {
//        $values = $this->values;
//
//        $qrys = [];
//        if (isset($values['EXTRA'])) {
//            if (count($values['EXTRA'])) {
//                $qrys[] = 'INSERT INTO ' . PREFIX . 'ATP_FIELDS_VALUES
//                    ' . /* '(`FIELD_ID`, `TABLE_ID`, `RECORD_ID`, `VALUE`)' */'
//                    (`FIELD_ID`, `RECORD_ID`, `VALUE`)
//			VALUES ' . join(',', $values['EXTRA']) . '
//				ON DUPLICATE KEY UPDATE
//					`VALUE` = VALUES(`VALUE`)';
//            }
//        }
//
//        if (isset($values['DOC'])) {
//            if (count($values['DOC'])) {
//                foreach ($values['DOC'] as $doc_id => $recs) {
//                    if (count($recs)) {
//                        foreach ($recs as $rec_id => $fields) {
//                            if (count($fields)) {
//                                $qrys[] = 'UPDATE ' . PREFIX . 'ATP_TBL_' . $doc_id . '
//									SET ' . join(',', $fields) . '
//									WHERE `ID` = ' . $rec_id;
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        $count = count($qrys);
//        for ($i = 0; $i < $count; $i++) {
//            $this->atp->db_query_fast($qrys[$i]);
//        }
//    }
//
//    /**
//     * @deprecated use \Atp\Record\Values::setValue()
//     * @param type $record_id
//     * @param type $field_id
//     * @param type $value
//     * @return boolean
//     */
//    private function set_doc_value($record_id, $field_id, $value)
//    {
//        $record = $this->get($record_id);
//        $f = $this->atp->factory->Field();
//        $field = $f->getField($field_id);
//
//        $param = [null];
//        $param[] = ['name' => 'VALUE', 'value' => $value];
//        $set = $this->atp->db_bind('`' . $field->name . '` = :VALUE', $param);
//
//        $this->values['DOC'][$record->document_id][$record->id_doc][$field->name] = $set;
//
//        return true;
//    }
//
//    /**
//     * @deprecated use \Atp\Record\Values::setValue()
//     * @param type $record_id
//     * @param type $field_id
//     * @param type $value
//     * @return boolean
//     */
//    private function set_extra_value($record_id, $field_id, $value)
//    {
//        $record = $this->get($record_id);
//        $f = $this->atp->factory->Field();
//        $field = $f->getField($field_id);
//
//        $param = [null];
//        $param[] = ['name' => 'FIELD', 'value' => $field->id];
////        $param[] = ['name' => 'DOCUMENT', 'value' => $record->document_id];
////        $param[] = ['name' => 'ID_DOC', 'value' => $record->id_doc];
//        $param[] = ['name' => 'RECORD_ID', 'value' => $record->id];
//        $param[] = ['name' => 'VALUE', 'value' => $value];
//
////        $this->values['EXTRA'][] = $this->atp->db_bind('(:FIELD, :DOCUMENT, :ID_DOC, :VALUE)', $param);
//        $this->values['EXTRA'][] = $this->atp->db_bind('(:FIELD, :RECORD_ID, :VALUE)', $param);
//
//        return true;
//    }

    /**
     *
     * @param int|\Atp\Document\Record|\Atp\Entity\Record $record
     * @param string $registrationNo
     * @param \DateTime $registrationDate
     * @return boolean
     * @throws \Atp\Exception\InvalidArgumentException
     */
    public function setRegistrationFields($record, $registrationNo, \DateTime $registrationDate)
    {
        if ($record instanceof \Atp\Document\Record) {
            $recordId = $record->record['ID'];
        } else
        if ($record instanceof \Atp\Entity\Record) {
            $recordId = $record->getId();
        } else
        if (ctype_digit((string) $record)) {
            $recordId = $record;
        } else {
            throw new \Atp\Exception\InvalidArgumentException($record, ['int', '\Atp\Document\Record']);
        }

        $record = $this->getEntity($recordId);
        $documentId = $record->getDocument()->getId();

        $valueManager = new \Atp\Record\Values($this->atp);
        $documentOptions = $this->atp->factory->Document()->getOptions($documentId);

        if ($documentOptions->avilys_number) {
            $valueManager->setValue(
                $record->getId(), $documentOptions->avilys_number, $registrationNo
            );
        }

        if ($documentOptions->avilys_date) {
            $valueManager->setValue(
                $record->getId(), $documentOptions->avilys_date, $registrationDate->format('Y-m-d H:i:s')
            );
        }

        $valueManager->flushValues();

        return true;
    }

    /**
     * @param int $fromRecordId
     * @param int $toDocumentId
     * @return boolean
     */
    public function validateTransferPath($fromRecordId, $toDocumentId)
    {
        $fromRecord = $this->getEntity($fromRecordId);
        $fromDocumentId = $fromRecord->getDocument()->getId();

        $mainPath = [
            \Atp\Document::DOCUMENT_INFRACTION_SISP,
            \Atp\Document::DOCUMENT_INFRACTION,
            \Atp\Document::DOCUMENT_PROTOCOL,
            \Atp\Document::DOCUMENT_CASE
        ];

        // Negalima tos pačios dokumento versijos antrą kartą perduoti į kitą dokumentą pagrindiniame kelyje
        if (in_array($fromDocumentId, $mainPath) && in_array($toDocumentId, $mainPath)) {
            $nextRecord = $this->atp->logic2->get_record_relations([
                'RECORD_FROM_ID' => $fromRecord->getId(),
                'TABLE_TO_ID' => $toDocumentId], true, null, 'A.*');

            if (empty($nextRecord) === false) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param int $recordId
     * @return \stdClass
     */
    public function getVehicleData($recordId)
    {
        $valuesManager = new \Atp\Record\Values($this->atp);
        $recordValues = $valuesManager->getValuesByRecord($this->get($recordId)->id, true);
        $filledFieldsIds = array_keys(array_filter($recordValues));

        $regitraName = 'RGT';
        $repeatRegistryRequest = true;


        // Užpildyti laukai, kurie yra surišti su regitros webservisu.
        $result = $this->atp->db_query_fast('
            SELECT A.FIELD_ID
            FROM
                ' . PREFIX . 'ATP_WEBSERVICES_FIELDS A
            WHERE
                A.WS_NAME = "' . $regitraName . '" AND
                A.FIELD_ID IN ("' . implode('","', array_map([$this->atp, 'db_escape'], $filledFieldsIds)) . '")');

        $count = $this->atp->db_num_rows($result);

        // Domina tik vienas užpildytas valstybinio numerio laukas
        for ($i = 0; $i < $count; $i++) {
            $row = $this->atp->db_next($result);
            $vnFieldId = $row['FIELD_ID'];

            if (empty($recordValues[$vnFieldId])) {
                continue;
            }

            // Surenkami su valstybinio numerio lauku susije Regitros webserviso rezultato laukai
            $childrenResult = $this->atp->db_query_fast('
                SELECT A.FIELD_ID, A.WS_FIELD_ID
                FROM
                    ' . PREFIX . 'ATP_WEBSERVICES A
                WHERE
                    A.WS_NAME = "' . $regitraName . '" AND
                    A.PARENT_FIELD_ID = ' . $vnFieldId);

            $childrenCount = $this->atp->db_num_rows($childrenResult);
            if (!$childrenCount) {
                break;
            }

            $ws = $this->atp->logic2->get_ws();

            $data = array();
            for ($i = 0; $i < $childrenCount; $i++) {
                $row = $this->atp->db_next($childrenResult);

                $wsField = $ws->get_field($regitraName, ['ID' => $row['WS_FIELD_ID']]);
                $data[$wsField['TAG']] = $recordValues[$row['FIELD_ID']];
            }

            if (empty($data['REG_DATA_VNZ_INFO_KTPR_VNZ'])) {
                $data['REG_DATA_VNZ_INFO_KTPR_VNZ'] = $recordValues[$vnFieldId];
            }

            $toMap = array(
                'unikalusIdentifikatorius' => 'TP_DATA_KTPR_TP_ID',
                'registracijosNumeris' => 'REG_DATA_VNZ_INFO_KTPR_VNZ',
                'vinKodas' => 'TP_DATA_KTPR_VIN_KODAS',
                'marke' => 'TP_DATA_KTPR_MARKE',
                'modelis' => 'TP_DATA_KTPR_MODELIS',
                'kategorija' => 'TP_DATA_KTPR_KATEGORIJA_KLASE',
//                'pagaminimoMetai' => 'TP_DATA_KTPR_GAMYBOS_METAI',
                'pagaminimoMetai' => 'REG_DATA_MAIN_INFO_KTPR_PIRMOS_REG_DATA',
//                'savininkas->tipas' => '', // fizinis / juridinis
//                
                'savininkas->asmensKodas' => 'REG_DATA_PERS_INFO_A_KTPR_ASMENS_KODAS',
                'savininkas->vardas' => 'REG_DATA_PERS_INFO_A_KTPR_ASMENS_VARDAS',
                'savininkas->pavarde' => 'REG_DATA_PERS_INFO_A_KTPR_ASMENS_PAVARDE'
//
//                'savininkas->asmensKodas' => 'REG_DATA_PERS_INFO_A_KTPR_IMONES_KODAS',
//                'savininkas->pavadinimas' => 'REG_DATA_PERS_INFO_A_KTPR_IMONES_PAV',
            );
            $result = new \stdClass();
            foreach ($toMap as $propertyName => $wsFieldTag) {
                $result->$propertyName = $data[$wsFieldTag];
            }

            $result->savininkas = new \stdClass();
			if (empty($data['REG_DATA_PERS_INFO_A_KTPR_IMONES_KODAS']) === false) {
                $result->savininkas->tipas = 'juridinis';
                $result->savininkas->asmensKodas = $data['REG_DATA_PERS_INFO_A_KTPR_IMONES_KODAS'];
                $result->savininkas->pavadinimas = $data['REG_DATA_PERS_INFO_A_KTPR_IMONES_PAV'];
            } elseif (empty($data['REG_DATA_PERS_INFO_A_KTPR_ASMENS_KODAS']) === false) {
                $result->savininkas->tipas = 'fizinis';
                $result->savininkas->asmensKodas = $data['REG_DATA_PERS_INFO_A_KTPR_ASMENS_KODAS'];
                $result->savininkas->vardas = $data['REG_DATA_PERS_INFO_A_KTPR_ASMENS_VARDAS'];
                $result->savininkas->pavarde = $data['REG_DATA_PERS_INFO_A_KTPR_ASMENS_PAVARDE'];
            }

            return $result;
        }

        return null;
    }

    /**
     * @param int $recordId Global record ID
     * @param int $fieldId Filed ID
     * @return null|string
     */
    public function getFieldValue($recordId, $fieldId)
    {
        if (!$this->valueHelper) {
            $this->valueHelper = new \Atp\Record\Values($this->atp);
        }
        return $this->valueHelper->getValue($recordId, $fieldId);
    }

    /**
     * @see \Atp\Record\Values::getValuesByRecord()
     */
    public function getValues($recordId, $parseValues = false, $withName = false, $reCache = false)
    {
        if (!$this->valueHelper) {
            $this->valueHelper = new \Atp\Record\Values($this->atp);
        }
        return $this->valueHelper->getValuesByRecord($recordId, $parseValues, $withName, $reCache);
    }

    /**
     *
     * @param int $recordId
     * @return boolean
     */
    public function isTemporary($recordId)
    {
        $cacheName = __METHOD__;
        if ($this->cache->is($cacheName, $recordId)) {
            return $this->cache->get($cacheName, $recordId);
        }

        $result = $this->atp->db_query_fast('
            SELECT ID
            FROM ' . PREFIX . 'ATP_RECORD_UNSAVED
            WHERE
                RECORD_ID = "' . $this->atp->db_escape($recordId) . '"');
        $return = (bool) $this->atp->db_num_rows($result);


        $this->cache->set($cacheName, $recordId, $return);

        return $return;
    }

    /**
     * Get list of next documents.
     *
     * @param int $recordId
     * @return array[]
     */
    public function getNextList($recordId)
    {
        $documentList = [];

        $record = $this->get($recordId);
        $isTemporary = $this->isTemporary($record->id);

        $documentManager = $this->atp->factory->Document();
        /* @var $document \Atp\Entity\Document */
        $document = $documentManager->get($record->document_id);


        if ($isTemporary === false && $document->getDisableEdit()) {
            return $documentList;
        }
        
        $recordWorker = $this->getWorker($record->id);
        $recordWorkerType = $recordWorker['TYPE'];

        if (in_array($record->status, [\Atp\Record::STATUS_EXAMINED]) === true || ($document->getCanNext() && in_array($record->status, [\Atp\Record::STATUS_DISCONTINUED,
                \Atp\Record::STATUS_INVESTIGATING, \Atp\Record::STATUS_EXAMINING,
                \Atp\Record::STATUS_EXAMINED]) === true)) {
            $table_path = $this->atp->logic->get_table_path($document->getId());
            if (!empty($table_path)) {
                $table_path_tmp = [
                    $document->getId() => [
                        'ID' => $document->getId(),
                        'PARENT_ID' => ($document->getParent() ? $document->getParent()->getId() : 0),
                        'LABEL' => $document->getLabel()
                    ]
                ];
                foreach ($table_path as $type => $path) {
                    foreach ($path as $table) {
                        $table_path_tmp[$table['ID']] = $table;
                    }
                }

                $table_path = $this->atp->logic->sort_children_to_parent($table_path_tmp, 'ID', 'PARENT_ID', $document->getId());


                // Pažeidimo duomenys // Rodyti tik Šaukimo dokumentą, jei asmuo yra baustas ir nėra sukurto šaukimo dokumento
                $showOnlySummonDocument = false;
                $showOnlyProtocolDocument = false;
                if ($document->getId() == \Atp\Document::DOCUMENT_INFRACTION && $isTemporary === false) {
                    $punismentManager = new \Atp\Document\Record\Punishment($this->atp);
                    if ($punismentManager->getCheckStatus($record->id)) {
                        $isSummonRecord = false;
                        $forwardRecords = $this->atp->logic->record_path_forward($record->id);
                        if (empty($forwardRecords) === false) {
                            foreach ($forwardRecords as $forwardRecord) {
                                if ($forwardRecord['TABLE_TO_ID'] == \Atp\Document::DOCUMENT_SUMMONS) {
                                    $isSummonRecord = true;
                                    break;
                                }
                            }
                        }

                        if ($isSummonRecord === false) {
                            $showOnlySummonDocument = $punismentManager->hasAtprViolations($record->id);
                        }
                    }

                    if ($showOnlySummonDocument === false) {
                        $showOnlyProtocolDocument = true;
                    }
                }

                // is protokolo dokumento tyrejui reikia leisti kurti tik Lydrascio dokumenta
                $showOnlyAccompanyingDocument = false;
                $accompanyingDocumentId = \Atp\Document::DOCUMENT_ACCOMPANYING;
                if ($document->getId() == \Atp\Document::DOCUMENT_PROTOCOL
                    && $recordWorkerType === \Atp\Record\Worker::TYPE_INVESTIGATOR
                ) {
                    $showOnlyAccompanyingDocument = true;
                }

                $disableAll = false;
                if ($document->getId() == \Atp\Document::DOCUMENT_PROTOCOL
                    && $recordWorkerType === \Atp\Record\Worker::TYPE_INVESTIGATOR) {

                    $isAn = $this->isAnProtocol($record->id);
                    if ($isAn !== null) {
                        if ($isAn === false) {
                            $disableAll = true;
                        }
                    } else {
                        $logics = new \Atp\Document\Record\Logics($record->id, $this->atp);
                        if ($logics->getProtocolClassificator() === \Atp\Document::PROTOCOL_WITHOUT_AN) {
                            $disableAll = true;
                        }
                    }
                    
                    if ($disableAll === false && $this->isNextDocumentFor($record->id, \Atp\Document::DOCUMENT_ACCOMPANYING)) {
                        $disableAll = true;
                    }
                }

                $showOnlyCaseDocument = false;
                $caseDocumentId = \Atp\Document::DOCUMENT_CASE;
                if ($document->getId() == \Atp\Document::DOCUMENT_PROTOCOL && $isTemporary === false && in_array($record->status, [\Atp\Record::STATUS_EXAMINING])) {
                    $showOnlyCaseDocument = true;
                }

                $privileges_data = array(
                    'user_id' => $this->atp->factory->User()->getLoggedUser()->person->id,
                    'department_id' => $this->atp->factory->User()->getLoggedPersonDuty()->department->id
                );
                foreach ($table_path as $key => $path) {
                    $available = true;
                    
                    if ($disableAll) {
                        $available = false;
                    }

                    if ($this->atp->logic->privileges('table_record_edit_button_submit_path', array_merge($privileges_data, ['table_id' => $path['ID']])) === false) {
                        $available = false;
                    }
                    if ($path['ID'] == $document->getId()) {
                        $available = false;
                    }
                    if ($showOnlySummonDocument && $path['ID'] != \Atp\Document::DOCUMENT_SUMMONS) {
                        $available = false;
                    }
                    if ($showOnlyProtocolDocument && $path['ID'] != \Atp\Document::DOCUMENT_PROTOCOL) {
                        $available = false;
                    }
                    if ($showOnlyCaseDocument && $path['ID'] != $caseDocumentId) {
                        $available = false;
                    }
                    if ($showOnlyAccompanyingDocument && $path['ID'] != $accompanyingDocumentId) {
                        $available = false;
                    }

                    $documentList[] = [
                        'ID' => $path['ID'],
                        'PARENT_ID' => $path['PARENT_ID'],
                        'LABEL' => $path['LABEL'],
                        'LEVEL' => $path['depth'],
                        'AVAILABLE' => $available,
                        'HIDDEN' => false
                    ];
                }

                // Pažeidimo duomenyse, jei įvestas juridinio asmens kodas, tai galima kurti tik pranešimo dokumentą.
                if ($document->getId() === \Atp\Document::DOCUMENT_INFRACTION) {
                    $personCodeFieldId = $documentManager->getOptions($document->getId())->person_code;
                    $personCode = $this->getFieldValue($record->id, $personCodeFieldId);
                    $isJuridical = preg_match('/^[0-9]{7,9}$/', trim($personCode));
$isJuridical = false;

                    foreach ($documentList as &$document) {
                        if ($isJuridical) {
                            $document['AVAILABLE'] = false;
                        }
                        if ($document['ID'] == \Atp\Document::DOCUMENT_REPORT) {
                            $document['AVAILABLE'] = true;
                            if (!$isJuridical) {
                                $document['HIDDEN'] = true;
                            }
                        }
                    }
                }
            }
        }
        
        return $documentList;
    }

    /**
     * Get HTML list of available next documents.
     *
     * @param int $recordId
     * @return string
     */
    public function getNextListHtml($recordId)
    {
        $documentList = $this->getNextList($recordId);
        $documentList = array_filter($documentList, function($document) {
            return $document['AVAILABLE'];
        });
        if (empty($documentList)) {
            return;
        }

        $tmpl = $this->atp->factory->Template();
        $tmpl->handle = 'table_record_edit_file';
        $tmpl->set_file($tmpl->handle, 'table_record_edit.tpl');

        $handles = $tmpl->set_multiblock($tmpl->handle, [
            'Marker_10',
            'create_new',
        ]);

        $record = $this->get($recordId);

        $documentManager = $this->atp->factory->Document();
        /* @var $document \Atp\Entity\Document */
        $document = $documentManager->get($record->document_id);


        $isOnlyOne = count(array_filter($documentList, function($document) {
            return $document['HIDDEN'] === false;
        })) === 1;

        foreach ($documentList as $document) {
            $tmpl->set_var([
                'value' => $document['ID'],
                'label' => str_repeat('&nbsp;', $document['LEVEL'] * 3) . $document['LABEL'],
                'opt_extra' => ($isOnlyOne && !$document['HIDDEN'] ? ' selected="selected"' : null)
                . ($document['HIDDEN'] ? ' style="display:none"' : null)
            ]);
            $tmpl->parse($handles['Marker_10'], 'Marker_10', true);
        }

        $tmpl->set_var($this->atp->set_lng_vars(['create']));
        $tmpl->parse($handles['create_new'], 'create_new');

        return $tmpl->get($handles['create_new']);
    }

    /**
     * Check whether next document record was created.
     *
     * @param int $recordId
     * @return bool
     */
    public function isNextRecordFor($recordId)
    {
        /* @var $repository \Atp\Repository\RecordRepository */
        $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Record');
        return $repository->isNextForId($recordId);
    }

    /**
     * Check whether next document record was created in document.
     *
     * @param int $recordId
     * @param int $documentId
     * @return bool
     */
    public function isNextDocumentFor($recordId, $documentId)
    {
        /* @var $repository \Atp\Repository\RecordRepository */
        $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Record');
        return $repository->isNextDocumentForId($recordId, $documentId);
    }

    /**
     * Get next document record created in specific document.
     *
     * @param int $recordId
     * @param int $documentId
     * @return RecordEntity
     */
    public function getNextDocumentFor($recordId, $documentId)
    {
        /* @var $repository \Atp\Repository\RecordRepository */
        $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Record');
        return $repository->getNextDocumentForId($recordId, $documentId);
    }

    /**
     * Get previous document record created in specific document.
     *
     * @param int $recordId
     * @param int $documentId
     * @return RecordEntity
     */
    public function getPreviousDocumentFor($recordId, $documentId)
    {
        /* @var $repository \Atp\Repository\RecordRepository */
        $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Record');
        return $repository->getPreviousDocumentForId($recordId, $documentId);
    }

    /**
     * Check whether ATPEIR was created for document record.
     * 
     * @param int $recordId
     * @return boolean
     */
    public function isAtpeirFor($recordId)
    {
        /* @var $repository \Atp\Repository\Atpr\AtpRepository */
        $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Atpr\Atp');
        return $repository->isByRecordId($recordId);
    }

    /**
     * Check whether there is file from ATPEIR.
     * 
     * @param int $recordId
     * @return boolean
     */
    public function isAtpeirResultFile($recordId)
    {
        /* @var $repository \Atp\Repository\Atpr\AtpRepository */
        $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Atpr\Atp');
        /* @var $atpr \Atp\Entity\Atpr\Atp */
        $atpr = $repository->getByRecordId($recordId);

        return (bool) count($repository->getFilesIds($atpr->getRoik(), \Atp\Entity\Atpr\AtpDocument::SOURCE_ATPEIR));
    }

    /**
     * Check whether Avilys document was created for document record.
     *
     * @param int $recordId
     * @return boolean
     */
    public function isAvilysFor($recordId)
    {
        /* @var $repository \Atp\Repository\Avilys\DocumentRepository */
        $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Avilys\Document');
        return $repository->isByRecordId($recordId);
    }
    
    /**
     * Get Avilys document for document record.
     *
     * @param int $recordId
     * @return \Atp\Entity\Avilys\Document
     */
    public function getAvilysFor($recordId)
    {
        /* @var $repository \Atp\Repository\Avilys\DocumentRepository */
        $repository = $this->atp->factory->Doctrine()->getRepository('Atp\Entity\Avilys\Document');
        return $repository->findOneByRecord($recordId);
    }

    /**
     * Is avilys document which created for document record registered.
     *
     * @param int $recordId
     * @return boolean
     */
    public function isAvilysRegisteredFor($recordId)
    {
        $avilysDocument = $this->getAvilysFor($recordId);
        if ($avilysDocument) {
            return $avilysDocument->getDocumentStatus() === \Atp\Entity\Avilys\Document::STATUS_REGISTERED;
    }
        return false;
    }

    /**
     * Get current record worker.
     * 
     * @param int $recordId
     * @return null|array see \Atp\Record\Worker::getRecordWorker()
     */
    public function getWorker($recordId)
    {
        $record = $this->getEntity($recordId);

        $workerType = null;

        switch ($record->getStatus()) {
            // TODO: Kai statusas "nutrauktas" ?
            // Buna perdavus Protokolą į Bylą
            case \Atp\Record::STATUS_DISCONTINUED:
                break;
            case \Atp\Record::STATUS_INVESTIGATING:
            case \Atp\Record::STATUS_IDLE:
                $workerType = \Atp\Record\Worker::TYPE_INVESTIGATOR;
                break;
            case \Atp\Record::STATUS_EXAMINING:
            case \Atp\Record::STATUS_EXAMINED:
                $workerType = \Atp\Record\Worker::TYPE_EXAMINATOR;
                break;
//            case \Atp\Record::STATUS_DELETED:
//            case \Atp\Record::STATUS_FINISHED:
        }

        $order = null;
        if (!$workerType) {
            $order = 'FIELD(A.TYPE, "' . \Atp\Record\Worker::TYPE_EXAMINATOR . '", "' . \Atp\Record\Worker::TYPE_INVESTIGATOR . '")';
            $criteria = 'A.TYPE IN ("' . \Atp\Record\Worker::TYPE_INVESTIGATOR . '","' . \Atp\Record\Worker::TYPE_EXAMINATOR . '")';
        } else {
            $criteria = 'A.TYPE = "' . $workerType . '"';
        }

        $workerManager = new \Atp\Record\Worker($this->atp);
        $worker = $workerManager->getRecordWorker(
            [
                'A.RECORD_ID = ' . $record->getId(),
                $criteria
            ],
            [
                'LIMIT' => true,
                'ORDER' => $order
            ]
        );

        if ($this->isWorker($record->getId(), $worker['WORKER_ID'])) {
            return $worker;
        }
    }

    public function isWorker($recordId, $workerId)
    {
        $cacheName = __METHOD__;
        $cacheKey = serialize(func_get_args());
        if ($this->cache->is($cacheName, $cacheKey)) {
            return $this->cache->get($cacheName, $cacheKey);
        }

        $record = $this->getEntity($recordId);
        $workerManager = new \Atp\Record\Worker($this->atp);
        $result = $workerManager->isCurrentRecordWorker($record->getId(), $workerId);

        $this->cache->set($cacheName, $cacheKey, $result);

        return $result;
    }

    /**
     * Is protocol document with AN.
     * Returns NULL if type is not set.
     * FALSE if type is without AN.
     * TRUE if type is with AN.
     *
     * @param int $recordId
     * @return null|boolean
     * @throws \Exception
     */
    public function isAnProtocol($recordId)
    {
        $record = $this->getEntity($recordId);
        $document = $record->getDocument();

        if ($document->getId() !== \Atp\Document::DOCUMENT_PROTOCOL) {
            throw new \Exception('Document "' . $document->getLabel() . '" is not a protocol document');
        }

//        $typeFieldId = $this->atp->factory->Document()
//            ->getOptions($document->getId())->class_type;
        $typeFieldId = 1124;
        if (!$typeFieldId) {
            throw new \Exception('Protocol type field is not configured');
        }

        $typeId = $this->getFieldValue($record->getId(), $typeFieldId);
        if ($typeId == \Atp\Document::PROTOCOL_WITH_AN) {
            return true;
        } else if ($typeId == \Atp\Document::PROTOCOL_WITHOUT_AN) {
            return false;
        }

        return;
    }
}
