<?php

namespace Atp\Entity;

/**
 * MAIN_OFFICE
 */
class Duty
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $label;

}
