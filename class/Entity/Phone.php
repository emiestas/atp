<?php

namespace Atp\Entity;

class Phone
{
    const TYPE_MOBILE = 'mobile';

    const TYPE_SHORT = 'short';

    const TYPE_FULL = 'full';

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $number;

}
