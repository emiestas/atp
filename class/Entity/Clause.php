<?php

namespace Atp\Entity;

use \Doctrine\ORM\Mapping as ORM;

/**
 * Clause
 *
 * @ORM\Entity
 * @ORM\Table(name="ATP_CLAUSES", options={"comment":"Straipsiu informacija"}, uniqueConstraints={@ORM\UniqueConstraint(name="CLAUSE", columns={"CLAUSE", "SECTION", "PARAGRAPH", "SUBPARAGRAPH"})})
 */
class Clause
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=false, options={"unsigned":true})
     */
    private $parentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="GROUP", type="integer", nullable=true, options={"unsigned":true})
     */
    private $group;

    /**
     * @var boolean
     *
     * @ORM\Column(name="TYPE", type="boolean", nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="CODEX", type="string", length=5000, nullable=false)
     */
    private $codex;

    /**
     * @var string
     *
     * @ORM\Column(name="CLAUSE", type="string", length=25, nullable=false)
     */
    private $clause;

    /**
     * @var string
     *
     * @ORM\Column(name="SECTION", type="string", length=25, nullable=false)
     */
    private $section;

    /**
     * @var string
     *
     * @ORM\Column(name="PARAGRAPH", type="string", length=25, nullable=false)
     */
    private $paragraph;

    /**
     * @var string
     *
     * @ORM\Column(name="SUBPARAGRAPH", type="string", length=25, nullable=false)
     */
    private $subparagraph;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=25, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=5000, nullable=false)
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="NOTICE", type="boolean", nullable=false)
     */
    private $notice;

    /**
     * @var string
     *
     * @ORM\Column(name="NOTICE_LABEL", type="string", length=255, nullable=false)
     */
    private $noticeLabel;

    /**
     * @var integer
     *
     * @ORM\Column(name="PENALTY_MIN", type="integer", nullable=false)
     */
    private $penaltyMin;

    /**
     * @var integer
     *
     * @ORM\Column(name="PENALTY_MAX", type="integer", nullable=false)
     */
    private $penaltyMax;

    /**
     * @var integer
     *
     * @ORM\Column(name="PENALTY_FIRST", type="integer", nullable=false)
     */
    private $penaltyFirst;

    /**
     * @var string
     *
     * @ORM\Column(name="PENALTY_LABEL", type="string", length=255, nullable=false)
     */
    private $penaltyLabel;

    /**
     * @var integer
     *
     * @ORM\Column(name="TERM", type="integer", nullable=false)
     */
    private $term;

    // TODO: many-to-many
//    /**
//     * @var integer
//     *
//     * @ORM\Column(name="CLASS_TYPE", type="integer", nullable=false, options={"unsigned":true})
//     */
//    private $classType;

    /**
     * @todo pašalinti
     * @var integer
     *
     * @ORM\Column(name="INVESTIGATION_ID", type="integer", nullable=false)
     */
    private $investigationId;

    /**
     * @todo pašalinti
     * @var integer
     *
     * @ORM\Column(name="EXAMINATION_ID", type="integer", nullable=false)
     */
    private $examinationId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="VALID_FROM", type="datetime", nullable=true)
     */
    private $validFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="VALID_TILL", type="datetime", nullable=true)
     */
    private $validTill;

    /**
     * @var string
     *
     * @ORM\Column(name="FIELDS_VALUES", type="text", length=65535, nullable=false)
     */
    private $fieldsValues;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VALID", type="boolean", nullable=false)
     */
    private $valid = '1';
}
