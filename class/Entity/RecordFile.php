<?php

namespace Atp\Entity;

use \Doctrine\ORM\Mapping as ORM;

/**
 * RecordFile
 *
 * @ORM\Entity
 * @ORM\Table(name="ATP_RECORD_FILES", indexes=
 * {
 *      @ORM\Index(name="FIELD_ID", columns={"FIELD_ID"})
 * })
 */
class RecordFile
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="FIELD_ID", type="integer", nullable=false, options={"unsigned":true})
     */
    private $fieldId;

    /**
     * @var Record
     *
     * @ORM\ManyToOne(targetEntity="Record", inversedBy="files")
     * @ORM\JoinColumn(name="RECORD_ID", referencedColumnName="ID", nullable=false)
     */
    private $record;

    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="File", inversedBy="records")
     * @ORM\JoinColumn(name="FILE_ID", referencedColumnName="ID")
     */
    private $file;
}
