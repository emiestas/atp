<?php

namespace Atp\Entity;

use Gedmo\Tree\Entity\MappedSuperclass\AbstractClosure;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="ATP_CLASSIFICATOR_CLOSURE")
 */
class ClassificatorClosure extends AbstractClosure
{

}
