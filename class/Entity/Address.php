<?php

namespace Atp\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Address
 *
 * @ORM\Table(name="VAR_DATA", indexes={@ORM\Index(name="GAT", columns={"GAT", "ADRESAS_KM"})})
 * @ORM\Entity(readOnly=true, repositoryClass="\Atp\Repository\AddressRepository")
 */
class Address
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="GAT_ID", type="integer", nullable=true)
     */
    private $gatId;

    /**
     * @var integer
     *
     * @ORM\Column(name="GAT", type="integer", nullable=true)
     */
    private $gat;

    /**
     * @var string
     *
     * @ORM\Column(name="GATVE", type="string", length=50, nullable=true)
     */
    private $gatve;

    /**
     * @var integer
     *
     * @ORM\Column(name="NAMO_NR", type="smallint", nullable=true)
     */
    private $namoNr;

    /**
     * @var string
     *
     * @ORM\Column(name="NAMO_R", type="string", length=1, nullable=true)
     */
    private $namoR;

    /**
     * @var integer
     *
     * @ORM\Column(name="GYV_ID", type="integer", nullable=true)
     */
    private $gyvId;

    /**
     * @var string
     *
     * @ORM\Column(name="GYV_PAV", type="string", length=50, nullable=true)
     */
    private $gyvPav;

    /**
     * @var integer
     *
     * @ORM\Column(name="KORPUSO_NR", type="smallint", nullable=true)
     */
    private $korpusoNr;

    /**
     * @var integer
     *
     * @ORM\Column(name="X_KOORD", type="integer", nullable=true)
     */
    private $xKoord;

    /**
     * @var integer
     *
     * @ORM\Column(name="Y_KOORD", type="integer", nullable=true)
     */
    private $yKoord;

    /**
     * @var string
     *
     * @ORM\Column(name="ADRESAS_KM", type="string", length=7, nullable=true)
     */
    private $adresasKm;

    /**
     * @var string
     *
     * @ORM\Column(name="ADRESAS", type="string", length=66, nullable=true)
     */
    private $adresas;

    /**
     * @var string
     *
     * @ORM\Column(name="SENIUNIJA", type="string", length=50, nullable=true)
     */
    private $seniunija;

    /**
     * @var integer
     *
     * @ORM\Column(name="SENIUNNR", type="smallint", nullable=true)
     */
    private $seniunnr;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get gatId
     *
     * @return integer
     */
    public function getGatId()
    {
        return $this->gatId;
    }

    /**
     * Get gat
     *
     * @return integer
     */
    public function getGat()
    {
        return $this->gat;
    }

    /**
     * Get gatve
     *
     * @return string
     */
    public function getGatve()
    {
        return $this->gatve;
    }

    /**
     * Get namoNr
     *
     * @return integer
     */
    public function getNamoNr()
    {
        return $this->namoNr;
    }

    /**
     * Get namoR
     *
     * @return string
     */
    public function getNamoR()
    {
        return $this->namoR;
    }

    /**
     * Get gyvId
     *
     * @return integer
     */
    public function getGyvId()
    {
        return $this->gyvId;
    }

    /**
     * Get gyvPav
     *
     * @return string
     */
    public function getGyvPav()
    {
        return $this->gyvPav;
    }

    /**
     * Get korpusoNr
     *
     * @return integer
     */
    public function getKorpusoNr()
    {
        return $this->korpusoNr;
    }

    /**
     * Get xKoord
     *
     * @return integer
     */
    public function getXKoord()
    {
        return $this->xKoord;
    }

    /**
     * Get yKoord
     *
     * @return integer
     */
    public function getYKoord()
    {
        return $this->yKoord;
    }

    /**
     * Get adresasKm
     *
     * @return string
     */
    public function getAdresasKm()
    {
        return $this->adresasKm;
    }

    /**
     * Get adresas
     *
     * @return string
     */
    public function getAdresas()
    {
        return $this->adresas;
    }

    /**
     * Get seniunija
     *
     * @return string
     */
    public function getSeniunija()
    {
        return $this->seniunija;
    }

    /**
     * Get seniunnr
     *
     * @return integer
     */
    public function getSeniunnr()
    {
        return $this->seniunnr;
    }
}
