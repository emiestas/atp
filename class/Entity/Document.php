<?php

namespace Atp\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Document
 *
 * @ORM\Entity(repositoryClass="Atp\Repository\DocumentRepository")
 * @ORM\Table(name="ATP_DOCUMENTS", indexes={@ORM\Index(name="_PARENT_ID", columns={"PARENT_ID"}), @ORM\Index(name="_VALID", columns={"VALID"})})
 */
class Document
{
    const STATUS_NONE = 0;
    const STATUS_BOTH = 1;
    const STATUS_INVEST = 2;
    const STATUS_EXAM = 3;

    /**
     * @var int ID
     *
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Document
     *
     * @ORM\ManyToOne(targetEntity="Document")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PARENT_ID", referencedColumnName="ID")
     * })
     */
    private $parent = null;

    /**
     * @var string Pavadinimas.
     *
     * @ORM\Column(name="LABEL", type="string", length=255, nullable=false)
     */
    private $label;

    /**
     * @var int Dokumento pradinė būsena.
     *
     * @ORM\Column(name="STATUS", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $status = \Atp\Entity\Document::STATUS_NONE;

    /**
     * @var \DateTime Paskutinio atnaujinimo data ir laikas.
     *
     * @ORM\Column(name="UPDATE_DATE", type="datetime", nullable=false)
     */
    private $updateDate;

    /**
     * @var \DateTime Sukūrimo data ir laikas.
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;

    /**
     * @var int Ar galiojantis.
     *
     * @ORM\Column(name="VALID", type="boolean", nullable=false, options={"unsigned"=true,"default"="1"})
     */
    private $valid = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="CAN_EDIT", type="boolean", nullable=false)
     */
    private $canEdit;

    /**
     * @var int
     *
     * @ORM\Column(name="DISABLE_EDIT", type="boolean", nullable=false)
     */
    private $disableEdit;

    /**
     * @var int
     *
     * @ORM\Column(name="CAN_END", type="boolean", nullable=false)
     */
    private $canEnd;

    /**
     * @var int
     *
     * @ORM\Column(name="CAN_NEXT", type="boolean", nullable=false)
     */
    private $canNext;

    /**
     * @var int
     *
     * @ORM\Column(name="CAN_IDLE", type="boolean", nullable=false)
     */
    private $canIdle;

    /**
     * @var int
     *
     * @ORM\Column(name="IDLE_DAYS", type="integer", nullable=false)
     */
    private $idleDays;

    /**
     * @var int
     *
     * @ORM\Column(name="CAN_TERM", type="boolean", nullable=false)
     */
    private $canTerm;

    /**
     * @var int
     *
     * @ORM\Column(name="TERM_DAYS", type="integer", nullable=false)
     */
    private $termDays;

    public function __construct()
    {
        ;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Document
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @return int
     */
    public function getValid()
    {
        return $this->valid;
    }

    public function getCanEdit()
    {
        return $this->canEdit;
    }
    
    public function getDisableEdit()
    {
        return $this->disableEdit;
    }

    public function getCanEnd()
    {
        return $this->canEnd;
    }

    public function getCanNext()
    {
        return $this->canNext;
    }

    public function getCanIdle()
    {
        return $this->canIdle;
    }

    public function getIdleDays()
    {
        return $this->idleDays;
    }

    public function getCanTerm()
    {
        return $this->canTerm;
    }

    public function getTermDays()
    {
        return $this->termDays;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setParent(Document $parent)
    {
        $this->parent = $parent;
        return $this;
    }

    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
        return $this;
    }

    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
        return $this;
    }

    public function setValid($valid)
    {
        $this->valid = $valid;
        return $this;
    }

    public function setCanEdit($canEdit)
    {
        $this->canEdit = $canEdit;
        return $this;
    }

    public function setCanEnd($canEnd)
    {
        $this->canEnd = $canEnd;
        return $this;
    }

    public function setDisableEdit($disableEdit)
    {
        $this->disableEdit = $disableEdit;
        return $this;
    }

     public function setCanNext($canNext)
    {
        $this->canNext = $canNext;
        return $this;
    }

    public function setCanIdle($canIdle)
    {
        $this->canIdle = $canIdle;
        return $this;
    }

    public function setIdleDays($idleDays)
    {
        $this->idleDays = $idleDays;
        return $this;
    }

    public function setCanTerm($canTerm)
    {
        $this->canTerm = $canTerm;
        return $this;
    }

    public function setTermDays($termDays)
    {
        $this->termDays = $termDays;
        return $this;
    }
}
