<?php

namespace Atp\Entity;

/**
 * ADM_USER
 */
class User
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $email;

    /**
     * @var \Atp\Entity\Person
     */
    public $person;

}
