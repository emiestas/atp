<?php

namespace Atp\Entity;

class Emploee
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $email;

    /**
     * @var \Atp\Entity\Phone[]
     */
    public $phones;

    /**
     * @var \Atp\Entity\EmploeeDuty[]
     */
    public $emploeeDuties;

}
