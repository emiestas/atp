<?php

namespace Atp\Entity;

use \Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Entity
 * @ORM\Table(name="ATP_MESSAGE")
 */
class Message
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var integer
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SUBJECT", type="string", length=255, nullable=false)
     */
    public $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="BODY", type="text", nullable=true)
     */
    public $body;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    public $createDate;

    /**
     * @var MessageReceiver[]
     *
     * @ORM\OneToMany(targetEntity="MessageReceiver", mappedBy="message")
     */
    public $receivers;

//    public $sender;
    /**
     * @var Record
     *
     * @ORM\ManyToOne(targetEntity="Record")
     * @ORM\JoinColumn(name="RECORD_ID", referencedColumnName="ID")
     */
    private $record;

    public function getId()
    {
        return $this->id;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getCreateDate()
    {
        return $this->createDate;
    }

    public function getReceivers()
    {
        return $this->receivers;
    }

    public function getRecord()
    {
        return $this->record;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    public function setCreateDate(\DateTime $createDate)
    {
        $this->createDate = $createDate;
        return $this;
    }

    public function setReceivers(array $receivers)
    {
        $this->receivers = $receivers;
        return $this;
    }

    public function setRecord(Record $record)
    {
        $this->record = $record;
        return $this;
    }
}
