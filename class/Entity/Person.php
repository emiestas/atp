<?php

namespace Atp\Entity;

class Person
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $email;

    /**
     * @var \Atp\Entity\Phone[]
     */
    public $phones;

    /**
     * @var \Atp\Entity\PersonDuty[]
     */
    public $personDuties;

    /**
     * Get full name (combination of first and last name).
     *
     * @return string
     */
    public function getFullName()
    {
        return join(' ', array_filter([
            trim($this->firstName),
            trim($this->lastName)
        ]));
    }
}
