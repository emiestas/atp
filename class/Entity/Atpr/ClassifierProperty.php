<?php

namespace Atp\Entity\Atpr;

use \Doctrine\ORM\Mapping as ORM;

/**
 * ClassifierProperty
 *
 * @ORM\Entity
 * @ORM\Table(name="ATP_ATPR_CLASSIFIER_PROPERTY", options={"comment":"ATPR klasifikatoriai"})
 */
class ClassifierProperty
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="VALUE", type="string", length=255, nullable=false)
     */
    private $value;

    /**
     * @var ClassifierPropertyKey
     *
     * @ORM\ManyToOne(targetEntity="ClassifierPropertyKey")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="KEY_ID", referencedColumnName="ID")
     * })
     */
    private $key;

    /**
     * @var Classifier
     *
     * @ORM\ManyToOne(targetEntity="Classifier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CLASSIFIER_ID", referencedColumnName="EXTERNAL_ID")
     * })
     */
    private $classifier;
}
