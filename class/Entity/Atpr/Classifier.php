<?php

namespace Atp\Entity\Atpr;

use \Atp\Entity\Classificator;
use \Atp\Entity\Clause;
use \Doctrine\ORM\Mapping as ORM;

/**
 * Classifier
 *
 * @ORM\Entity(repositoryClass="\Atp\Repository\Atpr\ClassifierRepository")
 * @ORM\Table(name="ATP_ATPR_CLASSIFIER", options={"comment":"ATPR klasifikatoriai"})
 */
class Classifier
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="EXTERNAL_ID", type="bigint", nullable=false, options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $externalId;

    /**
     * @var string
     *
     * @ORM\Column(name="CODE", type="string", length=255, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="SHORT_NAME", type="string", length=5000, nullable=false)
     */
    private $shortName;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=5000, nullable=false)
     */
    private $name;

    /**
     * @var Classifier
     *
     * @ORM\ManyToOne(targetEntity="Classifier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PARENT_EXTERNAL_ID", referencedColumnName="EXTERNAL_ID")
     * })
     */
    private $parentExternal;

    /**
     * @var Classificator
     *
     * @ORM\OneToOne(targetEntity="\Atp\Entity\Classificator")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CLASSIFICATOR_ID", referencedColumnName="ID", onDelete="SET NULL")
     * })
     */
    private $classificator;

    /**
     * @var ClassifierType
     *
     * @ORM\ManyToOne(targetEntity="ClassifierType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TYPE_ID", referencedColumnName="ID")
     * })
     */
    private $type;

    /**
     * @var Clause
     *
     * @ORM\OneToOne(targetEntity="\Atp\Entity\Clause")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CLAUSE_ID", referencedColumnName="ID")
     * })
     */
    private $clause;

    /**
     * @var integer
     *
     * @ORM\Column(name="ACT_ID", type="integer", nullable=true, options={"unsigned":true})
     */
    private $actId;

    /**
     * @return integer
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return Classifier
     */
    public function getParentExternal()
    {
        return $this->parentExternal;
    }

    /**
     * Get ATP classificator.
     * 
     * @return Classificator
     */
    public function getClassificator()
    {
        return $this->classificator;
    }
}
