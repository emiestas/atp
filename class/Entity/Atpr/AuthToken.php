<?php

namespace Atp\Entity\Atpr;

use \Doctrine\ORM\Mapping as ORM;

/**
 * AuthToken
 *
 * @ORM\Entity(repositoryClass="\Atp\Repository\Atpr\AuthTokenRepository")
 * @ORM\Table(name="ATP_ATPR_AUTH_TOKEN", uniqueConstraints={@ORM\UniqueConstraint(columns={"ATPR_VARTOTOJO_VARDAS", "ROIK"})})
 */
class AuthToken
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer", name="ID")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false, name="ATPR_VARTOTOJO_VARDAS")
     */
    private $atprVartotojoVardas;

    /**
     * @var Atp
     *
     * @ORM\ManyToOne(targetEntity="Atp")
     * @ORM\JoinColumn(name="ROIK", referencedColumnName="ROIK", nullable=false)
     */
    private $roik;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false, name="PRE_AUTH_TOKEN")
     */
    private $preAuthToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true, name="GALIOJIMO_PABAIGOS_DATA")
     */
    private $galiojimoPabaigosData;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=500, nullable=true, name="ATPR_ISORINIS_URL")
     */
    private $atprIsorinisUrl;

    /**
     * @return string
     */
    public function getPreAuthToken()
    {
        return $this->preAuthToken;
    }

    /**
     * @return string
     */
    public function getAtprIsorinisUrl()
    {
        return $this->atprIsorinisUrl;
    }

    /**
     * @return \DateTime
     */
    public function getGaliojimoPabaigosData()
    {
        return $this->galiojimoPabaigosData;
    }

    /**
     * @param string $atprVartotojoVardas
     * @return AuthToken
     */
    public function setAtprVartotojoVardas($atprVartotojoVardas)
    {
        $this->atprVartotojoVardas = $atprVartotojoVardas;
        return $this;
    }

    /**
     * @param Atp $roik
     * @return AuthToken
     */
    public function setRoik(Atp $roik)
    {
        $this->roik = $roik;
        return $this;
    }

    /**
     * @param string $preAuthToken
     * @return AuthToken
     */
    public function setPreAuthToken($preAuthToken)
    {
        $this->preAuthToken = $preAuthToken;
        return $this;
    }

    /**
     * @param \DateTime $galiojimoPabaigosData
     * @return AuthToken
     */
    public function setGaliojimoPabaigosData(\DateTime $galiojimoPabaigosData = null)
    {
        $this->galiojimoPabaigosData = $galiojimoPabaigosData;
        return $this;
    }

    /**
     * @param type $atprIsorinisUrl
     * @return AuthToken
     */
    public function setAtprIsorinisUrl($atprIsorinisUrl = null)
    {
        $this->atprIsorinisUrl = $atprIsorinisUrl;
        return $this;
    }
}
