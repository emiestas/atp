<?php

namespace Atp\Entity\Atpr;

use \Atp\Entity\File;
use \Atp\Entity\Record;
use \Atp\Repository\Atpr\AtpRepository;
use \Doctrine\Common\Collections\ArrayCollection;
use \Doctrine\ORM\Mapping as ORM;

/**
 * Atp
 *
 * @ORM\Entity(repositoryClass="\Atp\Repository\Atpr\AtpRepository")
 * @ORM\Table(name="ATP_ATPR_ATP", uniqueConstraints={@ORM\UniqueConstraint(name="_ROIK", columns={"ROIK"})})
 */
class Atp
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string", length=15, nullable=false, name="ROIK")
     */
    private $roik;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=500, nullable=true, name="PILNA_NUORODA_I_ATPR")
     */
    private $pilnaNuorodaIAtpr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false, name="CREATE_DATE")
     */
    private $createDate;

    /**
     * @var Message[]
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="atp")
     */
    private $messages;

    /**
     * @var AtpDocument[]
     *
     * @ORM\OneToMany(targetEntity="AtpDocument", mappedBy="roik", cascade={"persist"})
     */
    private $documents;

    /**
     * @var Record
     *
     * @ORM\OneToOne(targetEntity="\Atp\Entity\Record", inversedBy="atpr")
     * @ORM\JoinColumn(name="RECORD_ID", referencedColumnName="ID", unique=true)
     */
    private $record;

    /**
     * @param string $roik
     */
    public function __construct($roik)
    {
        $this->roik = $roik;
        $this->messages = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->createDate = new \DateTime('now');
    }

    /**
     * @return string
     */
    public function getRoik()
    {
        return $this->roik;
    }

    /**
     * @return string
     */
    public function getPilnaNuorodaIAtpr()
    {
        return $this->pilnaNuorodaIAtpr;
    }

    /**
     * @return Message[]
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return Record
     */
    public function getRecord()
    {
        return $this->record->first();
    }

    /**
     * @return AtpDocument[]
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param string $pilnaNuorodaIAtpr
     * @return Atp
     */
    public function setPilnaNuorodaIAtpr($pilnaNuorodaIAtpr)
    {
        $this->pilnaNuorodaIAtpr = $pilnaNuorodaIAtpr;
        return $this;
    }

    /**
     * @param Record $record
     * @return Atp
     */
    public function setRecord(Record $record)
    {
        $this->record = $record;
        return $this;
    }

    /**
     *
     * @param File $document
     * @param int $documentSource Where file came from. Enumerator: AtpDocument::SOURCE_ATP (1), AtpDocument::SOURCE_ATPEIR (2)
     * @return $this
     */
    public function addDocument(File $document, $documentSource)
    {
        $this->documents->add(new AtpDocument($this, $document, $documentSource));
        return $this;
    }
}
