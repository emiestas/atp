<?php
//
//namespace Atp\Entity\Atpr;
//
//use Doctrine\ORM\Mapping as ORM;
//
///**
// * MessageWorker
// *
// * @ORM\Entity(repositoryClass="Atp\Repository\Atpr\MessageWorkerRepository")
// * @ORM\Table(
// *      name="ATP_ATPR_MESSAGE_WORKER",
// *      options={"comment":"Messages of workers"},
// *      indexes={
// *          @ORM\Index(name="_MESSAGE_ID", columns={"MESSAGE_ID"}),
// *          @ORM\Index(name="_WORKER_ID", columns={"WORKER_ID"}),
// *          @ORM\Index(name="_IS_READ", columns={"IS_READ"}),
// *          @ORM\Index(name="_WORKER_ID__IS_READ", columns={"WORKER_ID","IS_READ"})
// *      }
// * )
// */
//class MessageWorker
//{
//
//    /**
//     * @var integer
//     *
//     * @ORM\Id
//     * @ORM\Column(type="integer", name="ID", options={"unsigned":true})
//     * @ORM\GeneratedValue(strategy="IDENTITY")
//     */
//    private $id;
//
//    /**
//     * @var integer
//     *
//     * @ORM\Column(type="integer", nullable=false, name="WORKER_ID", options={"unsigned":true, "comment":"Worker who has a message"})
//     */
//    private $workerId;
//
//    /**
//     * @var boolean
//     *
//     * @ORM\Column(type="boolean", nullable=false, name="IS_READ", options={"comment":"Was message read"})
//     */
//    private $isRead = false;
//
//    /**
//     * @var \DateTime
//     *
//     * @ORM\Column(type="datetime", nullable=false, name="SEND_DATE", options={"comment":"When a message was sent"})
//     */
//    private $sendDate;
//
//    /**
//     * @var \DateTime
//     *
//     * @ORM\Column(type="datetime", nullable=true, name="READ_DATE", options={"comment":"When a message was read"})
//     */
//    private $readDate;
//
//    /**
//     * @var Message
//     *
//     * @ORM\ManyToOne(targetEntity="Message", inversedBy="workers")
//     * @ORM\JoinColumns({
//     *      @ORM\JoinColumn(name="MESSAGE_ID", referencedColumnName="MESSAGE_ID", nullable=false)
//     * })
//     */
//    private $message;
//
//    /**
//     * @return int
//     */
//    public function getId()
//    {
//        return $this->id;
//    }
//
//    /**
//     * @return int
//     */
//    public function getWorkerId()
//    {
//        return $this->workerId;
//    }
//
//    /**
//     * @return int
//     */
//    public function isRead()
//    {
//        return $this->isRead;
//    }
//
//    /**
//     * @return \DateTime
//     */
//    public function getSendDate()
//    {
//        return $this->sendDate;
//    }
//
//    /**
//     * @return \DateTime
//     */
//    public function getReadDate()
//    {
//        return $this->readDate;
//    }
//
//    /**
//     * @return Message
//     */
//    public function getMessage()
//    {
//        return $this->message;
//    }
//
//    /**
//     * @param int $workerId
//     * @return MessageWorker
//     */
//    public function setWorkerId($workerId)
//    {
//        $this->workerId = $workerId;
//        return $this;
//    }
//
//    /**
//     * @param boolean $isRead
//     * @return MessageWorker
//     */
//    public function setIsRead($isRead)
//    {
//        $this->isRead = $isRead;
//        return $this;
//    }
//
//    /**
//     * @param \DateTime $sendDate
//     * @return MessageWorker
//     */
//    public function setSendDate(\DateTime $sendDate)
//    {
//        $this->sendDate = $sendDate;
//        return $this;
//    }
//
//    /**
//     * @param \DateTime $readDate
//     * @return MessageWorker
//     */
//    public function setReadDate(\DateTime $readDate)
//    {
//        $this->readDate = $readDate;
//        return $this;
//    }
//
//    /**
//     * @param Message $message
//     * @return MessageWorker
//     */
//    public function setMessage(Message $message)
//    {
//        $this->message = $message;
//        return $this;
//    }
//}

