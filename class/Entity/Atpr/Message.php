<?php

namespace Atp\Entity\Atpr;

use \Atp\Entity\Message as AtpMessage;
use \Atp\Repository\Atpr\MessageRepository;
use \Doctrine\Common\Collections\ArrayCollection;
use \Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Entity(repositoryClass="\Atp\Repository\Atpr\MessageRepository")
 * @ORM\Table(name="ATP_ATPR_MESSAGE")
 */
class Message
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="bigint", name="MESSAGE_ID", options={"unsigned":true})
     */
    private $messageId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=15, nullable=false, name="ROIK")
     */
    private $roik;

    /**
     * @var Atp
     *
     * @ORM\ManyToOne(targetEntity="Atp", inversedBy="messages")
     * @ORM\JoinColumn(name="ROIK", referencedColumnName="ROIK", nullable=false)
     */
    private $atp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false, name="BUSENOS_PASIKEITIMO_DATA")
     */
    private $busenosPasikeitimoData;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false, name="BUSENOS_TIPAS")
     */
    private $busenosTipas;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false, name="IS_READ")
     */
    private $isRead = false;

//    /**
//     * @var MessageWorker[]
//     *
//     * @ORM\OneToMany(targetEntity="MessageWorker", mappedBy="message")
//     */
//    private $workers;

    /**
     * @var AtpMessage
     *
     * @ORM\OneToOne(targetEntity="\Atp\Entity\Message")
     * @ORM\JoinColumn(name="ATP_MESSAGE_ID", referencedColumnName="ID")
     */
    private $atpMessage;

    public function __construct()
    {
        $this->workers = new ArrayCollection();
    }

    /**
     * @return bigint
     */
    public function getMessageId()
    {
        return $this->messageId;
    }

    /**
     * @return \DateTime
     */
    public function getBusenosPasikeitimoData()
    {
        return $this->busenosPasikeitimoData;
    }

    /**
     * @return string
     */
    public function getBusenosTipas()
    {
        return $this->busenosTipas;
    }
//    /**
//     * @return MessageWorker[]
//     */
//    public function getWorkers()
//    {
//        return $this->workers;
//    }

    /**
     * @return Atp
     */
    public function getAtp()
    {
        return $this->atp;
    }

    /**
     * @param \DateTime $busenosPasikeitimoData
     * @return AtpMessage
     */
    public function setBusenosPasikeitimoData(\DateTime $busenosPasikeitimoData)
    {
        $this->busenosPasikeitimoData = $busenosPasikeitimoData;
        return $this;
    }

    /**
     * @param string $busenosTipas
     * @return AtpMessage
     */
    public function setBusenosTipas($busenosTipas)
    {
        $this->busenosTipas = $busenosTipas;
        return $this;
    }

    public function setMessageId($messageId)
    {
        $this->messageId = $messageId;
        return $this;
    }

    public function setAtp(Atp $atp)
    {
        $this->atp = $atp;
        return $this;
    }

    public function setAtpMessage(AtpMessage $atpMessage)
    {
        $this->atpMessage = $atpMessage;
        return $this;
    }

    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;
        return $this;
    }
}
