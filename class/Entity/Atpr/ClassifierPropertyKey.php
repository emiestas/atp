<?php

namespace Atp\Entity\Atpr;

use \Doctrine\ORM\Mapping as ORM;

/**
 * ClassifierPropertyKey
 *
 * @ORM\Entity
 * @ORM\Table(name="ATP_ATPR_CLASSIFIER_PROPERTY_KEY", options={"comment":"ATPR savybes klasifikatoriams"})
 */
class ClassifierPropertyKey
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=255, nullable=false)
     */
    private $name;
}
