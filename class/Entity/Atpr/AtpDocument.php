<?php

namespace Atp\Entity\Atpr;

use \Atp\Entity\File;
use \Doctrine\ORM\Mapping as ORM;

/**
 * AtpDocument
 *
 * @ORM\Entity(repositoryClass="\Atp\Repository\Atpr\AtpDocumentRepository")
 * @ORM\Table(name="ATP_ATPR_ATP_DOCUMENT_XREF")
 */
class AtpDocument
{
    const SOURCE_ATP = 1;

    const SOURCE_ATPEIR = 2;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer", name="ID")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Atp
     *
     * @ORM\ManyToOne(targetEntity="Atp", inversedBy="documents")
     * @ORM\JoinColumn(name="ROIK", referencedColumnName="ROIK", nullable=false)
     */
    private $roik;

    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="\Atp\Entity\File")
     * @ORM\JoinColumn(name="FILE_ID", referencedColumnName="ID", nullable=false)
     */
    private $file;

    /**
     * @var integer
     *
     * @ORM\Column(name="SOURCE", type="integer", nullable=false)
     */
    private $source;

    /**
     * @param \Atp\Entity\Atpr\Atp $roik
     * @param File $file
     * @param int $source Where file came from. Enumerator: self::SOURCE_ATP (1), self::SOURCE_ATPEIR (2)
     */
    public function __construct(Atp $roik, File $file, $source)
    {
        $this->roik = $roik;
        $this->file = $file;
        $this->source = $source;
    }
}
