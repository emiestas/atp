<?php

namespace Atp\Entity;

class PersonDuty
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var \Atp\Entity\Duty
     */
    public $duty;

    /**
     * @var \Atp\Entity\Department
     */
    public $department;

    /**
     * @var boolean
     */
    public $isTemporary;

}
