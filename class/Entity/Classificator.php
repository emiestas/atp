<?php

namespace Atp\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Classificator
 *
 * @Gedmo\Tree(type="closure")
 * @Gedmo\TreeClosure(class="Atp\Entity\ClassificatorClosure")
 * @ORM\Entity(repositoryClass="Atp\Repository\ClassificatorRepository")
 * @ORM\Table(name="ATP_CLASSIFICATORS", options={"comment":"Klasifikatorių informacija"}, indexes={@ORM\Index(name="VALID", columns={"VALID"}), @ORM\Index(name="PARENT_ID", columns={"PARENT_ID", "VALID"})})
 */
class Classificator
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Classificator
     *
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Classificator", inversedBy="children")
     * @ORM\JoinColumn(name="PARENT_ID", referencedColumnName="ID", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=5000, nullable=false)
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VALID", type="boolean", nullable=false)
     */
    private $valid = '1';

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="LEVEL", type="integer")
     */
    private $level;

    /**
     * @var Classificator[]
     *
     * @ORM\OneToMany(targetEntity="Classificator", mappedBy="parent")
     */
    private $children;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parent
     *
     * @param Classificator $parent
     *
     * @return Classificator
     */
    public function setParent(Classificator $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return Classificator
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Classificator
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set valid
     *
     * @param boolean $valid
     *
     * @return Classificator
     */
    public function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * Get valid
     *
     * @return boolean
     */
    public function getValid()
    {
        return $this->valid;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Classificator
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Add closure
     *
     * @param ClassificatorClosure $closure
     */
    public function addClosure(ClassificatorClosure $closure)
    {
        $this->closures[] = $closure;
    }
}
