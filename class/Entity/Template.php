<?php

namespace Atp\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Template
 *
 * @ORM\Table(name="ATP_TABLE_TEMPLATE")
 * @ORM\Entity
 */
class Template
{

    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var integer
     *
     * @deprecated
     * @ORM\Column(name="SERVICE_ID", type="integer", nullable=true)
     */
    protected $serviceId;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=255, nullable=false)
     */
    protected $label;

    /**
     * @var string
     *
     * @ORM\Column(name="HANDLER", type="string", length=25, nullable=false)
     */
    protected $handler;

    /**
     * @var Document
     *
     * @ORM\ManyToOne(targetEntity="Document")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DOCUMENT_ID", referencedColumnName="ID", nullable=false)
     * })
     */
    protected $document;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @deprecated
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * @return Document
     */
    public function getDocument()
    {
        return $this->document;
    }
}
