<?php

namespace Atp\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Department
 *
 * @ORM\Entity(readOnly=true)
 * @ORM\Table(name="MAIN_STRUCTURE")
 */
class Department
{

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string.
     *
     * @ORM\Column(name="SHOWS", type="string", length=255, nullable=false)
     */
    public $label;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
}
