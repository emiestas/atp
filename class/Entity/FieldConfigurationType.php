<?php

namespace Atp\Entity;

use \Doctrine\ORM\Mapping as ORM;
use \Atp\Repository\FieldConfigurationTypeRepository;

/**
 * FieldConfigurationType
 *
 * @ORM\Entity(repositoryClass="\Atp\Repository\FieldConfigurationTypeRepository")
 * @ORM\Table(name="ATP_FIELD_CONFIGURATION_TYPE")
 */
class FieldConfigurationType
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=255, nullable=false, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=255, nullable=false)
     */
    private $label;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param int $id
     * @return FieldConfigurationType
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $name
     * @return FieldConfigurationType
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $label
     * @return FieldConfigurationType
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }
}
