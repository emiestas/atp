<?php

namespace Atp\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RecordDocument
 *
 * @ORM\Entity
 * @ORM\Table(name="ATP_RECORD_DOCUMENTS", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="FILE_ID", columns={"FILE_ID"})
 * },
 * indexes=
 * {
 *      @ORM\Index(name="RECORD_ID", columns={"RECORD_ID"}),
 *      @ORM\Index(name="TEMPLATE_ID", columns={"TEMPLATE_ID"})
 * })
 */
class RecordDocument
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="File")
     * @ORM\JoinColumn(name="FILE_ID", referencedColumnName="ID", nullable=false)
     */
    private $file;

    /**
     * @var Record
     *
     * @ORM\ManyToOne(targetEntity="Record", inversedBy="documents")
     * @ORM\JoinColumn(name="RECORD_ID", referencedColumnName="ID", nullable=false)
     */
    private $record;

    /**
     * @var integer
     *
     * @ORM\Column(name="TEMPLATE_ID", type="integer", nullable=true)
     */
    private $template;

}
