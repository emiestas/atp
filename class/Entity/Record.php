<?php

namespace Atp\Entity;

use \Atp\Entity\Atpr\Atp;
use \Atp\Entity\Avilys\Document as AvilysDocument;
use \Doctrine\ORM\Mapping as ORM;

/**
 * Record
 *
 * @ORM\Table(name="ATP_RECORD", indexes={
 * @ORM\Index(name="RECORD_ID", columns={"RECORD_ID"}),
 * @ORM\Index(name="TABLE_TO_ID", columns={"TABLE_TO_ID", "WORKER_ID"}),
 * @ORM\Index(name="RECORD_FROM_ID", columns={"RECORD_FROM_ID", "TABLE_TO_ID"}),
 * })
 * @ORM\Entity(repositoryClass="\Atp\Repository\RecordRepository")
 */
class Record
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="MAIN_ID", type="integer", nullable=false, options={"unsigned":true, "comment":"ID, kuris nesikeičia keičiantis versijai"})
     */
    private $mainId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $number;

    /**
     * @var Record
     *
     * @ORM\ManyToOne(targetEntity="Record")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="RECORD_FROM_ID", referencedColumnName="ID")
     * })
     */
    private $parent;

    /**
     * @var integer
     *
     * @ORM\Column(name="TABLE_FROM_ID", type="integer", nullable=false, options={"unsigned":true})
     */
    private $tableFromId;

    /**
     * @var Document
     *
     * @ORM\ManyToOne(targetEntity="Document")
     * @ORM\JoinColumn(name="TABLE_TO_ID", referencedColumnName="ID", nullable=false)
     */
    private $document;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_LAST", type="boolean", nullable=false)
     */
    private $isLast;

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS", type="integer", length=1, nullable=false, options={"unsigned":true})
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_PAID", type="boolean", nullable=false)
     */
    private $isPaid = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_STATUS", type="integer", nullable=true, options={"unsigned":true})
     */
    private $recordStatus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PUNISHMENT_CHECKED", type="boolean", nullable=false, options={"comment":"Nurodo ar dokumente buvo tikrintas baustumas"})
     */
    private $punishmentChecked = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="REGISTERED_TEMPLATE_ID", type="integer", nullable=false, options={"unsigned":true, "comment":"Nurodo koks šablonas registruotas avilyje"})
     */
    private $registeredTemplateId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="WORKER_ID", type="integer", nullable=false, options={"unsigned":true})
     */
    private $workerId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IN_LIST", type="boolean", nullable=false, options={"default":1})
     */
    private $inList = true;

    /**
     *
     * @var RecordFile[]
     *
     * @ORM\OneToMany(targetEntity="RecordFile", mappedBy="record")
     */
    private $files;

    /**
     *
     * @var RecordDocument[]
     *
     * @ORM\OneToMany(targetEntity="RecordDocument", mappedBy="record")
     */
    private $documents;

    /**
     * @var Atp
     *
     * @ORM\OneToOne(targetEntity="Atp\Entity\Atpr\Atp", mappedBy="record")
     */
    private $atpr;

    /**
     * @var Avilys\Document
     *
     * @ORM\OneToOne(targetEntity="Atp\Entity\Avilys\Document", mappedBy="record")
     */
    private $avilysDocument;

    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return Document
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Get process status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get record status.
     *
     * @return int
     */
    public function getRecordStatus()
    {
        return $this->recordStatus;
    }

    /**
     * Set whether record is shown in document list.
     * 
     * @param boolean $inList
     * @return $this
     */
    public function setInList($inList)
    {
        $this->inList = $inList;
        return $this;
    }

    /**
     * Is last version.
     *
     * @return bool
     */
    public function getIsLast()
    {
        return $this->isLast;
    }

    /**
     * Get parent record.
     *
     * @return Record
     */
    public function getParent()
    {
        return $this->parent;
    }
}
