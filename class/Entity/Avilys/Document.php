<?php

namespace Atp\Entity\Avilys;

use \Atp\Entity\Record;
use \Atp\Repository\Avilys\DocumentRepository;
use \Doctrine\ORM\Mapping as ORM;
use \Gedmo\Timestampable\Traits\TimestampableEntity;
use \Atp\Entity\File;

/**
 * Document
 *
 * @ORM\Entity(repositoryClass="Atp\Repository\Avilys\DocumentRepository")
 * @ORM\Table(name="ATP_AVILYS_DOCUMENT")
 */
class Document
{
    use TimestampableEntity;
    
    const STATUS_REGISTERED = 'registered';

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string", length=255, name="OID")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $oid;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, name="REGISTRATION_NO")
     */
    private $registrationNo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", length=255, nullable=true, name="REGISTRATION_DATE")
     */
    private $registrationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", length=255, nullable=true, name="CHECK_DATE")
     */
    private $checkDate;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, name="DOCUMENT_CATEGORY")
     */
    private $documentCategory;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, name="DOCUMENT_STATUS")
     */
    private $documentStatus;

    /**
     * @var Record
     *
     * @ORM\OneToOne(targetEntity="\Atp\Entity\Record", inversedBy="avilysDocument")
     * @ORM\JoinColumn(name="RECORD_ID", referencedColumnName="ID", unique=true)
     */
    private $record;
    
    /**
     * @var \Atp\Entity\File
     *
     * @ORM\OneToOne(targetEntity="\Atp\Entity\File")
     * @ORM\JoinColumn(name="RESULT_FILE_ID", referencedColumnName="ID")
     */
    private $resultFile;

    /**
     * @param string $oid
     * @param string $registrationNo [optional]
     * @param \DateTime $registrationDate [optional]
     * @param string $documentCategory [optional]
     */
    public function __construct($oid, $registrationNo = null, \DateTime $registrationDate = null, $documentCategory = null, $documentStatus = null)
    {
        $this->oid = $oid;
        $this->registrationNo = $registrationNo;
        $this->registrationDate = $registrationDate;
        $this->documentCategory = $documentCategory;
        $this->documentStatus = $documentStatus;
    }

    /**
     * @return string
     */
    public function getOid()
    {
        return $this->oid;
    }

    /**
     * @param string $registrationNo
     * @return Document
     */
    public function setRegistrationNo($registrationNo)
    {
        $this->registrationNo = $registrationNo;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getRegistrationNo()
    {
        return $this->registrationNo;
    }

    /**
     * @param \DateTime $registrationDate
     * @return Document
     */
    public function setRegistrationDate(\DateTime $registrationDate = null)
    {
        $this->registrationDate = $registrationDate;
        return $this;
    }

    /**
     * @param \DateTime $checkDate
     * @return Document
     */
    public function setCheckDate(\DateTime $checkDate = null)
    {
        $this->checkDate = $checkDate;
        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    /**
     * @return null|\DateTime
     */
    public function getCheckDate()
    {
        return $this->checkDate;
    }

    /**
     * @param string $documentCategory
     * @return Document
     */
    public function setDocumentCategory($documentCategory)
    {
        $this->documentCategory = $documentCategory;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDocumentCategory()
    {
        return $this->documentCategory;
    }

    /**
     * @param string $documentStatus
     * @return Document
     */
    public function setDocumentStatus($documentStatus)
    {
        $this->documentStatus = $documentStatus;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDocumentStatus()
    {
        return $this->documentStatus;
    }

    /**
     * @param Record $record
     */
    public function setRecord(Record $record)
    {
        $this->record = $record;
    }

    /**
     * @return Record
     */
    public function getRecord()
    {
        return $this->record;
    }

    /**
     * @param File $resultFile
     */
    public function setResultFile(File $resultFile)
    {
        $this->resultFile = $resultFile;
    }

    /**
     * @return File
     */
    public function getResultFile()
    {
        return $this->resultFile;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        $config = require __DIR__ . '/../../../../avilys/config/config.php';
        return $config['system']['root_url']
            . '/actDHSDocumentShow?docOid='
            . $this->getOid();
    }
}
