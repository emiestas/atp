<?php

namespace Atp\Entity\Avilys;

use \Doctrine\ORM\Mapping as ORM;
use \Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Configuration
 *
 * @ORM\Entity(repositoryClass="Atp\Repository\Avilys\ConfigurationRepository")
 * @ORM\Table(name="ATP_AVILYS_CONFIGURATION")
 */
class Configuration
{

    use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \Atp\Entity\Document
     *
     * @ORM\ManyToOne(targetEntity="\Atp\Entity\Document")
     * @ORM\JoinColumn(name="ATP_DOCUMENT", referencedColumnName="ID", nullable=false)
     */
    protected $atpDocument;

    /**
     * @var \Atp\Entity\Template
     *
     * @ORM\ManyToOne(targetEntity="\Atp\Entity\Template")
     * @ORM\JoinColumn(name="ATP_TEMPLATE", referencedColumnName="ID")
     */
    protected $atpTemplate;

    /**
     * @var \Atp\Entity\Department
     *
     * @ORM\ManyToOne(targetEntity="\Atp\Entity\Department")
     * @ORM\JoinColumn(name="ATP_DEPARTMENT", referencedColumnName="ID")
     */
    protected $atpDepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="AVILYS_JOURNAL_OID", type="string", length=255, nullable=false)
     */
    protected $avilysJournalOid;

    /**
     * @var string
     *
     * @ORM\Column(name="AVILYS_CASE_OID", type="string", length=255, nullable=false)
     */
    protected $avilysCaseOid;

    /**
     * @var string
     *
     * @ORM\Column(name="AVILYS_TEMPLATE_OID", type="string", length=255, nullable=false)
     */
    protected $avilysTemplateOid;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Atp\Entity\Document
     */
    public function getAtpDocument()
    {
        return $this->atpDocument;
    }

    /**
     * @return \Atp\Entity\Template
     */
    public function getAtpTemplate()
    {
        return $this->atpTemplate;
    }

    /**
     * @return \Atp\Entity\Department
     */
    public function getAtpDepartment()
    {
        return $this->atpDepartment;
    }

    /**
     * @return string
     */
    public function getAvilysJournalOid()
    {
        return $this->avilysJournalOid;
    }

    /**
     * @return string
     */
    public function getAvilysCaseOid()
    {
        return $this->avilysCaseOid;
    }

    /**
     * @return string
     */
    public function getAvilysTemplateOid()
    {
        return $this->avilysTemplateOid;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param \Atp\Entity\Document $atpDocument
     * @return $this
     */
    public function setAtpDocument(\Atp\Entity\Document $atpDocument)
    {
        $this->atpDocument = $atpDocument;
        return $this;
    }

    /**
     * @param \Atp\Entity\Template $atpTemplate
     * @return $this
     */
    public function setAtpTemplate(\Atp\Entity\Template $atpTemplate = null)
    {
        $this->atpTemplate = $atpTemplate;
        return $this;
    }

    /**
     * @param \Atp\Entity\Department $atpDepartment
     * @return $this
     */
    public function setAtpDepartment(\Atp\Entity\Department $atpDepartment = null)
    {
        $this->atpDepartment = $atpDepartment;
        return $this;
    }

    /**
     * @param string $avilysJournalOid
     * @return $this
     */
    public function setAvilysJournalOid($avilysJournalOid)
    {
        $this->avilysJournalOid = $avilysJournalOid;
        return $this;
    }

    /**
     * @param string $avilysCaseOid
     * @return $this
     */
    public function setAvilysCaseOid($avilysCaseOid)
    {
        $this->avilysCaseOid = $avilysCaseOid;
        return $this;
    }

    /**
     * @param string $avilysTemplateOid
     * @return $this
     */
    public function setAvilysTemplateOid($avilysTemplateOid)
    {
        $this->avilysTemplateOid = $avilysTemplateOid;
        return $this;
    }
}
