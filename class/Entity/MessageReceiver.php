<?php

namespace Atp\Entity;

use \Doctrine\ORM\Mapping as ORM;

/**
 * MessageReceiver
 *
 * @ORM\Entity(repositoryClass="Atp\Repository\MessageReceiverRepository")
 * @ORM\Table(name="ATP_MESSAGE_RECEIVER")
 */
class MessageReceiver
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var Message
     *
     * @ORM\ManyToOne(targetEntity="Message", inversedBy="receivers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="MESSAGE_ID", referencedColumnName="ID", nullable=false)
     * })
     */
    public $message;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ID", type="integer", nullable=false, options={"unsigned":true,"comment":"Receiver (worker ID) who has a message"})
     */
    public $receiverId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_READ", type="boolean", nullable=false, options={"comment":"Was message read"})
     */
    public $isRead = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RECEIVE_DATE", type="datetime", nullable=false, options={"comment":"When a message was received"})
     */
    public $receiveDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="READ_DATE", type="datetime", nullable=true, options={"comment":"When a message was read"})
     */
    public $readDate;

    public function getId()
    {
        return $this->id;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getReceiverId()
    {
        return $this->receiverId;
    }

    public function isRead()
    {
        return $this->isRead;
    }

    public function getReceiveDate()
    {
        return $this->receiveDate;
    }

    public function getReadDate()
    {
        return $this->readDate;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setMessage(Message $message)
    {
        $this->message = $message;
        return $this;
    }

    public function setReceiverId($receiverId)
    {
        $this->receiverId = $receiverId;
        return $this;
    }

    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;
        return $this;
    }

    public function setReceiveDate(\DateTime $receiveDate)
    {
        $this->receiveDate = $receiveDate;
        return $this;
    }

    public function setReadDate(\DateTime $readDate)
    {
        $this->readDate = $readDate;
        return $this;
    }
}
