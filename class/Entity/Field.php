<?php

namespace Atp\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Field
 *
 * @ORM\Table(name="ATP_FIELD", indexes={
 * @ORM\Index(name="NAME", columns={"NAME", "ITEM_ID", "ITYPE"}),
 * @ORM\Index(name="ORD", columns={"ORD", "ITYPE"}),
 * @ORM\Index(name="ITYPE", columns={"ITYPE", "ITEM_ID", "ORD"}),
 * @ORM\Index(name="ITEM_ID", columns={"ITEM_ID", "ITYPE"})})
 * @ORM\Entity
 */
class Field
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ITEM_ID", type="integer", nullable=false)
     */
    public $itemId;

    /**
     * @var string
     *
     * @ORM\Column(name="ITYPE", type="string", length=6, nullable=false)
     */
    public $itemType = 'DOC';

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=75, nullable=false)
     */
    public $label;

    /**
     * @var string
     *
     * @ORM\Column(name="HELPTIP", type="string", length=100, nullable=true)
     */
    public $helpTip;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=25, nullable=false)
     */
    public $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="SORT", type="integer", nullable=false)
     */
    public $sort;

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE", type="integer", nullable=false)
     */
    public $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="RENDER", type="integer", nullable=false)
     */
    public $render;

    /**
     * @var integer
     *
     * @ORM\Column(name="SOURCE", type="integer", nullable=false)
     */
    public $source;

    /**
     * @var boolean
     *
     * @ORM\Column(name="WS", type="boolean", nullable=false)
     */
    public $ws = '0';
//
//    /**
//     * @var integer
//     *
//     * @ORM\Column(name="VALUE", type="integer", nullable=false)
//     */
//    public $value;
//
//    /**
//     * @var boolean
//     *
//     * @ORM\Column(name="VALUE_TYPE", type="boolean", nullable=false)
//     */
//    public $valueType;
//
//    /**
//     * @var integer
//     *
//     * @ORM\Column(name="OTHER_SOURCE", type="integer", nullable=false)
//     */
//    public $otherSource;
//
//    /**
//     * @var integer
//     *
//     * @ORM\Column(name="OTHER_VALUE", type="integer", nullable=false)
//     */
//    public $otherValue;

//    /**
//     * @var boolean
//     *
//     * @ORM\Column(name="FILL", type="boolean", nullable=false)
//     */
//    public $fill;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IN_LIST", type="boolean", nullable=false)
     */
    public $inList;

    /**
     * @var boolean
     *
     * @ORM\Column(name="SEARCHABLE", type="boolean", nullable=true)
     */
    public $searchable;

    /**
     * @var integer
     *
     * @ORM\Column(name="RELATION", type="integer", nullable=false)
     */
    public $relation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="MANDATORY", type="boolean", nullable=false)
     */
    public $mandatory;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VISIBLE", type="boolean", nullable=false)
     */
    public $visible;

    /**
     * @var integer
     *
     * @ORM\Column(name="COMPETENCE", type="integer", nullable=false)
     */
    public $competence;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORD", type="integer", nullable=false)
     */
    public $ord;

    /**
     * @var string
     *
     * @ORM\Column(name="TEXT", type="text", length=65535, nullable=false)
     */
    public $text;

    /**
     * @var string
     *
     * @ORM\Column(name="DEFAULT", type="string", length=1000, nullable=false)
     */
    public $default;

    /**
     * @var integer
     *
     * @ORM\Column(name="DEFAULT_ID", type="integer", nullable=true)
     */
    public $defaultId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="UPDATE_DATE", type="datetime", nullable=false)
     */
    public $updateDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    public $createDate;

    /**
     * @var \Atp\Entity\FieldConfiguration
     *
     * @ORM\OneToMany(targetEntity="\Atp\Entity\FieldConfiguration", mappedBy="field")
     */
    protected $configurations;

    /**
     * 
     */
    public function __construct()
    {
        $this->configurations = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itemId
     *
     * @param integer $itemId
     *
     * @return Field
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Get itemId
     *
     * @return integer
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set itemType
     *
     * @param string $itemType
     *
     * @return Field
     */
    public function setItype($itemType)
    {
        $this->itemType = $itemType;

        return $this;
    }

    /**
     * Get itemType
     *
     * @return string
     */
    public function getItype()
    {
        return $this->itemType;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Field
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set helpTip
     *
     * @param string $helpTip
     *
     * @return Field
     */
    public function setHelpTip($helpTip)
    {
        $this->helpTip = $helpTip;

        return $this;
    }

    /**
     * Get helpTip
     *
     * @return string
     */
    public function getHelpTip()
    {
        return $this->helpTip;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Field
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return Field
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Field
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set render
     *
     * @param integer $render
     *
     * @return Field
     */
    public function setRender($render)
    {
        $this->render = $render;
        return $this;
    }

    /**
     * Get render
     *
     * @return integer
     */
    public function getRender()
    {
        return $this->render;
    }

    /**
     * Set source
     *
     * @param integer $source
     *
     * @return Field
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return integer
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set ws
     *
     * @param boolean $ws
     *
     * @return Field
     */
    public function setWs($ws)
    {
        $this->ws = $ws;

        return $this;
    }

    /**
     * Get ws
     *
     * @return boolean
     */
    public function getWs()
    {
        return $this->ws;
    }
//
//    /**
//     * Set value
//     *
//     * @param integer $value
//     *
//     * @return Field
//     */
//    public function setValue($value)
//    {
//        $this->value = $value;
//
//        return $this;
//    }
//
//    /**
//     * Get value
//     *
//     * @return integer
//     */
//    public function getValue()
//    {
//        return $this->value;
//    }
//
//    /**
//     * Set valueType
//     *
//     * @param boolean $valueType
//     *
//     * @return Field
//     */
//    public function setValueType($valueType)
//    {
//        $this->valueType = $valueType;
//
//        return $this;
//    }
//
//    /**
//     * Get valueType
//     *
//     * @return boolean
//     */
//    public function getValueType()
//    {
//        return $this->valueType;
//    }
//
//    /**
//     * Set otherSource
//     *
//     * @param integer $otherSource
//     *
//     * @return Field
//     */
//    public function setOtherSource($otherSource)
//    {
//        $this->otherSource = $otherSource;
//
//        return $this;
//    }
//
//    /**
//     * Get otherSource
//     *
//     * @return integer
//     */
//    public function getOtherSource()
//    {
//        return $this->otherSource;
//    }
//
//    /**
//     * Set otherValue
//     *
//     * @param integer $otherValue
//     *
//     * @return Field
//     */
//    public function setOtherValue($otherValue)
//    {
//        $this->otherValue = $otherValue;
//
//        return $this;
//    }
//
//    /**
//     * Get otherValue
//     *
//     * @return integer
//     */
//    public function getOtherValue()
//    {
//        return $this->otherValue;
//    }
//
//    /**
//     * Set fill
//     *
//     * @param boolean $fill
//     *
//     * @return Field
//     */
//    public function setFill($fill)
//    {
//        $this->fill = $fill;
//
//        return $this;
//    }
//
//    /**
//     * Get fill
//     *
//     * @return boolean
//     */
//    public function getFill()
//    {
//        return $this->fill;
//    }

    /**
     * Set inList
     *
     * @param boolean inList
     *
     * @return Field
     */
    public function setInList($inList)
    {
        $this->inList = $inList;

        return $this;
    }

    /**
     * Get inList
     *
     * @return boolean
     */
    public function getInList()
    {
        return $this->inList;
    }

    /**
     * Set searchable
     *
     * @param boolean $searchable
     *
     * @return Field
     */
    public function setSearchable($searchable)
    {
        $this->searchable = $searchable;

        return $this;
    }

    /**
     * Get searchable
     *
     * @return boolean
     */
    public function getSearchable()
    {
        return $this->searchable;
    }

    /**
     * Set relation
     *
     * @param integer $relation
     *
     * @return Field
     */
    public function setRelation($relation)
    {
        $this->relation = $relation;

        return $this;
    }

    /**
     * Get relation
     *
     * @return integer
     */
    public function getRelation()
    {
        return $this->relation;
    }

    /**
     * Set mandatory
     *
     * @param boolean $mandatory
     *
     * @return Field
     */
    public function setMandatory($mandatory)
    {
        $this->mandatory = $mandatory;

        return $this;
    }

    /**
     * Get mandatory
     *
     * @return boolean
     */
    public function getMandatory()
    {
        return $this->mandatory;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return Field
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set competence
     *
     * @param boolean $competence
     *
     * @return Field
     */
    public function setCompetence($competence)
    {
        $this->competence = $competence;

        return $this;
    }

    /**
     * Get competence
     *
     * @return boolean
     */
    public function getCompetence()
    {
        return $this->competence;
    }

    /**
     * Set ord
     *
     * @param integer $ord
     *
     * @return Field
     */
    public function setOrd($ord)
    {
        $this->ord = $ord;

        return $this;
    }

    /**
     * Get ord
     *
     * @return integer
     */
    public function getOrd()
    {
        return $this->ord;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Field
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set default
     *
     * @param string $default
     *
     * @return Field
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Get default
     *
     * @return string
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Set defaultId
     *
     * @param integer $defaultId
     *
     * @return Field
     */
    public function setDefaultId($defaultId)
    {
        $this->defaultId = $defaultId;

        return $this;
    }

    /**
     * Get defaultId
     *
     * @return integer
     */
    public function getDefaultId()
    {
        return $this->defaultId;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     *
     * @return Field
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return Field
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Get configurations
     *
     * @return \Atp\Entity\FieldConfiguration[]
     */
    public function getConfigurations()
    {
        return $this->configurations;
    }
}
