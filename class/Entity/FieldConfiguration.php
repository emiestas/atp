<?php

namespace Atp\Entity;

use \Doctrine\ORM\Mapping as ORM;

/**
 * FieldConfiguration
 *
 * @ORM\Entity(repositoryClass="\Atp\Repository\FieldConfigurationRepository")
 * @ORM\Table(name="ATP_FIELD_CONFIGURATION")
 */
class FieldConfiguration
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Field
     *
     * @ORM\ManyToOne(targetEntity="\Atp\Entity\Field", inversedBy="configurations")
     * @ORM\JoinColumn(name="FIELD_ID", referencedColumnName="ID", nullable=false)
     */
    private $field;

    /**
     * @var FieldConfigurationType
     *
     * @ORM\ManyToOne(targetEntity="\Atp\Entity\FieldConfigurationType")
     * @ORM\JoinColumn(name="TYPE_ID", referencedColumnName="ID", nullable=false)
     */
    private $type;

    /**
     * @param int $id
     * @return FieldConfiguration
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Field $field
     * @return FieldConfiguration
     */
    public function setField(Field $field)
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return Field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param FieldConfigurationType $type
     * @return FieldConfiguration
     */
    public function setType(FieldConfigurationType $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return FieldConfigurationType
     */
    public function getType()
    {
        return $this->type;
    }
}
