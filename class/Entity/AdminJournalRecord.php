<?php

namespace Atp\Entity;

class AdminJournalRecord
{

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $personName;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $subsystemName;

    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * @param string $message
     * @param string $personName
     * @param string $url [optional]
     * @param string $subsystemName [optional]
     */
    public function __construct($message, $personName, $url = null, $subsystemName = null)
    {
        $this->message = $message;
        $this->personName = $personName;
        $this->url = $url;
        $this->subsystemName = $subsystemName;
        $this->createDate = new \DateTime('now');
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getPersonName()
    {
        return $this->personName;
    }

    public function getSubsystemName()
    {
        return $this->subsystemName;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getCreateDate()
    {
        return $this->createDate;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function setPersonName($personName)
    {
        $this->personName = $personName;
        return $this;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function setSubsystemName($subsystemName)
    {
        $this->subsystemName = $subsystemName;
        return $this;
    }
}
