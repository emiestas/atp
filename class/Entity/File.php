<?php

namespace Atp\Entity;

use \Doctrine\ORM\Mapping as ORM;

/**
 * File
 *
 * @ORM\Entity
 * @ORM\Table(name="ATP_FILES")
 */
class File
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=500, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="PATH", type="string", length=5000, nullable=false)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=200, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="SIZE", type="string", length=50, nullable=false)
     */
    private $size;

    /**
     * @var RecordFile[]
     *
     * @ORM\OneToMany(targetEntity="RecordFile", mappedBy="file")
     */
    private $records;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getContent()
    {
        return file_get_contents(__DIR__ . '/../../' . $this->path);
    }
}
