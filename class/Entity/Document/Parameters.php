<?php

namespace Atp\Entity\Document;

class Parameters
{
    /**
     * @var Parameter[]
     */
    private $parameters;

    /**
     * @param Parameter[] $parameters [optional]
     */
    public function __construct($parameters = null)
    {
        $this->setOptions($parameters);
    }

    /**
     * @param Parameter[] $parameters
     * @return Parameters
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @param Parameter $parameter
     * @return Parameters
     */
    public function addParameter(Parameter $parameter)
    {
        $this->parameters[] = $parameter;

        return $this;
    }

    /**
     * @return Parameter[]
     */
    public function getParameters()
    {
        return $this->parameters;
    }
}
