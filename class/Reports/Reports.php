<?php

class Atpreports
{
    /**
     *
     * @var \Atp\Core
     */
    public $core;

    public function __construct(\Atp\Core $core)
    {
        $this->core = $core;
    }

    public function generateXML($data)
    {
        require_once($this->core->config['REAL_URL'] . 'helper/PHPExcel.php');
        require_once($this->core->config['REAL_URL'] . 'helper/helper-phpexcel.php');
        require_once($this->core->config['REAL_URL'] . 'helper/helper-report.php');

        //$_helper_report = new HReport();
        $helper = new HPhpexcel();  // vaiksciojimo per cell helpers
        $objPHPExcel = new PHPExcel(); // PHPExcel klases iskvietimas ir apdorijimas excel failo
        // * bendri nustatymai BEGIN
        $objPHPExcel->getProperties()->setCreator($_SESSION['WP']['PEOPLE']['SHOWS']);
        $objPHPExcel->getProperties()->setLastModifiedBy($_SESSION['WP']['PEOPLE']['SHOWS']);
        $objPHPExcel->getProperties()->setTitle('Ataskaita');
        $objPHPExcel->getProperties()->setSubject('Ataskaita: ' . $data['name']);
        $objPHPExcel->getProperties()->setDescription('Ataskaita: ' . $data['name']);

        $objPHPExcel->setActiveSheetIndex(0);
        $sh1 = $objPHPExcel->getActiveSheet();
        $sh1->setTitle($data['name']);

        $pageMargins = $sh1->getPageMargins();
        //in inches (0.5cm)
        $margin = 0.55 / 2.54;
        $margin2 = 0.75 / 2.54;
        $pageMargins->setTop($margin2);
        $pageMargins->setBottom($margin);
        $pageMargins->setLeft($margin);
        $pageMargins->setRight($margin);
        // * bednri nustatymai END
        //
        $row = (object) array('start' => '2');
        $col = (object) array('start' => 'B', 'end' => 'F');
        $row->current = $row->start;
        $col->current = $col->start;

        $sh1->mergeCells($col->current . $row->current . ":" . $col->end . $row->current);
        $sh1->SetCellValue($col->current . $row->current, 'Ataskaita: ' . $data['name']);

        if (empty($data['interval']['from']) === false || empty($data['interval']['till']) === false) {
            $row->current++;
            $sh1->mergeCells($col->start . $row->current . ":" . $col->end . $row->current);
            $value = 'Intervale';
            if (empty($data['interval']['from']) === false) {
                $value .= ' nuo ' . $data['interval']['from'];
            }
            if (empty($data['interval']['till']) === false) {
                $value .= ' iki ' . $data['interval']['till'];
            }
            $sh1->SetCellValue($col->start . $row->current, $value);
        }


        // Lentelės stulpeliai
        $columns = array(
            'DATE' => $this->core->lng('atp_search_item_date') . 'Data',
            'NR' => $this->core->lng('atp_search_item_date') . 'Nr.',
            'TYPE' => $this->core->lng('atp_search_item_type') . 'Tipas',
            'OFFICER' => $this->core->lng('atp_search_item_officer') . 'Pareigūnas',
            'STATUS' => $this->core->lng('atp_search_item_status') . 'Būsena',
        );
        // Lentelės stulpelių pavadinimai
        $row->current += 2;
        $col->current = $col->start;
        foreach ($columns as &$column) {
            $sh1->SetCellValue($col->current . $row->current, $column);
            $col->current = $helper->GetNextLetter($col->current);
        }

        $info = array();
        if (count($data['records'])) {
            // Visų dokumentų duomenys
            $documents = $this->core->logic2->get_document();

            // Lentelės įrašų eilutės
            foreach ($data['records'] as &$record) {
                $row->current++;
                $col->current = $col->start;

                // Įrašo dokumentas
                list($doc_id) = explode('p', $record);
                // Įrašo dokumento duomenys
                $document = &$documents[$doc_id];
                // Įrašo duoemnys
                $record = $this->core->logic2->get_record_table($record, 'C.*,F.FIRST_NAME,F.LAST_NAME,A.STATUS');

                $info['documents'][$doc_id] ++;
                $info['status'][$record['STATUS']] ++;
                // Eilutės stulpeiai
                foreach ($columns as $col_name => &$column) {
                    // Stulpelio duomenys
                    if ($col_name === 'DATE') {
                        $value = date('Y-m-d', strtotime($record['CREATE_DATE']));
                        if ($value === '1970-01-01') {
                            $value = '';
                        }
                    } elseif ($col_name === 'NR') {
                        $value = $record['RECORD_ID'];
                    } elseif ($col_name === 'TYPE') {
                        $value = $document['LABEL'];
                    } elseif ($col_name === 'OFFICER') {
                        $value = join(' ', array_filter(array($record['FIRST_NAME'],
                            $record['LAST_NAME'])));
                    } elseif ($col_name === 'STATUS') {
                        $status = $this->core->logic->find_document_status(array(
                            'ID' => $record['STATUS']));
                        $value = $status['LABEL'];
                    }

                    $sh1->SetCellValue($col->current . $row->current, $value);
                    $col->current = $helper->GetNextLetter($col->current);
                }
            }
        }

        $col->current = $col->start;

        $row->current += 2;
        $sh1->SetCellValue($col->current . $row->current, 'Skaitiniai laukai');
        $sh1->SetCellValue($helper->GetNextLetter($col->current) . $row->current, '0');

        $row->current += 2;
        $sh1->SetCellValue($col->current . $row->current, 'Dokumentai');
        $sh1->SetCellValue($helper->GetNextLetter($col->current) . $row->current, 'Viso');
        foreach ($info['documents'] as $document_id => &$count) {
            $row->current++;
            $document = &$documents[$document_id];
            $sh1->SetCellValue($col->current . $row->current, $document['LABEL']);
            $sh1->SetCellValue($helper->GetNextLetter($col->current) . $row->current, $count);
        }

        $row->current += 2;
        $sh1->SetCellValue($col->current . $row->current, 'Busenos');
        $sh1->SetCellValue($helper->GetNextLetter($col->current) . $row->current, 'Viso');
        foreach ($info['status'] as $status_id => &$count) {
            $row->current++;
            $status = $this->core->logic->find_document_status(array('ID' => $status_id));
            $sh1->SetCellValue($col->current . $row->current, $status['LABEL']);
            $sh1->SetCellValue($helper->GetNextLetter($col->current) . $row->current, $count);
        }

        // XML užbaigimas
        $objPHPExcel->setActiveSheetIndex(0);

        $baseFileName = time();
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $baseFileName . '.xlsx"');
        header('Cache-Control: max-age=0');
        // !!! unsetinti $objPHPExcel kazkurioje vietoje del RAM
        ob_end_clean(); // jei koks buferis uzsilikes butu
        $objWriter->save("php://output");
        unset($objWriter, $objPHPExcel);

        exit();
    }

    public function generateDOC()
    {

    }

    public function generatePDF()
    {

    }
}
