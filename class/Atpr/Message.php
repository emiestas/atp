<?php

namespace Atp\Atpr;

use \Atp\Entity\Atpr\Atp;
use \Atp\Entity\Atpr\Message as AtprMessageEntity;
use \Atp\Entity\Message as AtpMessage;
use \Atp\Entity\MessageReceiver;
use \Atp\Entity\Record;
use \Atp\REpository\RecordRepository;
use \Atp\Repository\Atpr\MessageRepository;
use \Atpr\Kis;
use \Atpr\Kis\SearchMessagesType;
use \Atp\Atpr\Service as AtprService;
use \Doctrine\ORM\EntityManager;
use \Doctrine\ORM\Query\ResultSetMapping;

class Message
{
    /**
     * @var EntityManager
     */
    protected $em;
    protected $emr;

    public function __construct(\Atp\Core $core)
    {
        $this->em = $core->factory->Doctrine();
    }

    /**
     * @return MessageRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository(AtprMessageEntity::class);
    }

    /**
     * @return RecordRepository
     */
    public function getRecordRepository()
    {
        return $this->em->getRepository(Record::class);
    }


    /**
     * Import new messages.
     */
    public function importNew()
    {
        // Newest message
        $dateTime = $this->getRepository()->getNewestMessageDate();

        // Filter
        $filter = new Kis\SearchMessagesType();
        if ($dateTime) {
            $filter->setDataNuo($dateTime);
        }

        $this->import($filter);
    }

    /**
     * Mark Atpr messages as read which still have not been marked, but are read in Atp.
     */
    public function updateReadStatus()
    {
        $unmarkedMessages = $this->getRepository()->getReadUnmarkedMessages();

        $this->markAsRead($unmarkedMessages);
    }

    /**
     * Get array of Message receiver (worker id) and record id by ROIK.
     *
     * @param int $messages
     * @return array
     */

	private function getMessageReceiverByROIK($ROIK) {
	    $rsm = new ResultSetMapping();
	    $rsm->addScalarResult('WORKER_ID','workerId');
	    $rsm->addScalarResult('RECORD_ID','recordId');
		$qry = 'SELECT TA.WORKER_ID, TA.RECORD_ID 
		FROM '.PREFIX.'ATP_RECORD_X_WORKER TA
		INNER JOIN `'.PREFIX.'ATP_ATPR_ATP` TB ON TA.RECORD_ID = TB.RECORD_ID AND ROIK = :roik
		ORDER BY TA.RECORD_ID DESC, TA.ID DESC';
		$result = $this->em->createNativeQuery($qry, $rsm);
		$result->setParameter('roik', $ROIK);
		$res = $result->getResult();
		$recordId = $res['0']['recordId'];
		$workers = array();
		foreach ($res as $key => $val) {
			if ($recordId != $res[$key]['recordId']) {
				break;
			}
			$workers[] = $res[$key]['workerId'];
		}
		return array('recordId' => $recordId, 'workers' => $workers);
	}

    /**
     * Check for message by record id and subject
     *
     * @param int $RECORD_ID
     * @param string $SUBJECT
	 * @return bool
     */

	private function checkForMessageByRecordAndSubject($RECORD_ID, $SUBJECT) {
		$rsm = new ResultSetMapping();
	    $rsm->addScalarResult('ID','Id');
		$qry = 'SELECT ID FROM '.PREFIX.'ATP_MESSAGE WHERE
		SUBJECT = :subject AND RECORD_ID = :recordId';
		$result = $this->em->createNativeQuery($qry, $rsm);
		$result->setParameter('recordId', $RECORD_ID);
		$result->setParameter('subject', $SUBJECT);
		$res = $result->getResult();
		$messageId = $res['0']['Id'];
		if (empty($messageId) === FALSE) {
			return true;
		} else {
			return false;
		}
	}

    /**
     * Mark Atpr message as read.
     *
     * @param AtprMessageEntity|AtprMessageEntity[] $messages
     * @return boolean
     */

	protected function markAsRead($messages)
    {
        if (empty($messages) === false) {
            if (is_array($messages) === false) {
                $messages = array($messages);
            }

            $messagesId = [];
            foreach ($messages as $message) {
                $messagesId[] = $message->getMessageId();

                $message->setIsRead(true);
                $this->em->persist($message);
            }

            if (count($messagesId)) {
                $this->markAsReadCall($messagesId);
                $this->em->flush();

                return true;
            }
        }

        return false;
    }

    /**
     * Perform web-service to mark Atpr message as read.
     * 
     * @param int|int[] $messageId
     * @throws \Exception
     */
    protected function markAsReadCall($messageId)
    {
        $service = new AtprService();

        try {
            $response = $service->MarkMessagesAsRead($messageId);
        } catch (\SoapFault $soapFault) {
            error_log($soapFault);
        }

        // On error
        // Busena naudoja sumavimą.
        // Jei yra pateikti 4 pranešimai, tai sekminga užklausa gražina busaną su reikšme 4
        $messagesCount = (is_array($messageId) ? count($messageId) : 1);
        $sucessfulStatus = ($response::BUSENA_OK * $messagesCount);

        if (($response && $response->getBusena() !== $sucessfulStatus) || isset($soapFault)) {
            $message = 'Nepavyko sekmingai atlikti ATPR operacijos.';

            if (isset($soapFault)) {
                $message .= ' ' . $soapFault->getMessage();
            } else {
                if (empty($response->getKlaidosPranesimas()) === false) {
                    $message .= ' ' . $response->getKlaidosPranesimas();
                } elseif ($response->getBusena() > $sucessfulStatus) {
                    $message .= ' Nepavyko pažymėti visų pranešimų.';
                }
            }

            throw new \Exception($message);
        }
    }

    /**
     * Import messages
     * @param SearchMessagesType $filter [optional]
     */
    protected function import(Kis\SearchMessagesType $filter = null)
    {
		if ($filter === null) {
            $filter = new Kis\SearchMessagesType();
        }

        // Query
        $service = new AtprService();
        $response = $service->SearchMessages($filter);
		$atprAtpMessageCheck = $this->getRepository();
        if ($response) {
            $responseData = $response->getResponseData();
            if (empty($responseData) === false) {
                foreach ($responseData->getMessage() as $messagesData) {
					// Get existing or create new ATPR object
                    $atprAtp = $this->getRepository()
                        ->find($messagesData->getROIK());
					$ROIK = $messagesData->getROIK();
					$workerArray = array();
					$workerArray = $this->getMessageReceiverByROIK($ROIK);
					$record = $this->getRecordRepository()->findOneBy(array('id' => $workerArray['recordId']));
					if (empty($workerArray['workers'][0]) === FALSE) {
						if (!$atprAtp) {
	                        $atprAtp = new Atp(
	                            $messagesData->getROIK()
	                        );
	                        $this->em->persist($atprAtp);
						}
						foreach ($messagesData->getMessages() as $message) {
							// ATP message
							$check = $atprAtpMessageCheck->find($message->getMessageId());
							if (!$check) {
								$messageCheck = $this->checkForMessageByRecordAndSubject($record->getId(), $message->getBusenosTipas());
								if (!$messageCheck) {
									$atpMessage = new AtpMessage();
									if ($record) {
										$atpMessage->setRecord($record);
									}
									$atpMessage->setSubject($message->getBusenosTipas());
			                        $atpMessage->setCreateDate($message->getBusenosPasikeitimoData());
									$this->em->persist($atpMessage);

			                        // TODO: Check if ATPR object is related to ATP record. If is - add message receivers
			                        // ATP message receiver
									foreach ($workerArray['workers'] as $workerId) {
										$atpMessageReceiver = new MessageReceiver();
				                        $atpMessageReceiver->setReceiveDate($message->getBusenosPasikeitimoData());
				                        $atpMessageReceiver->setMessage($atpMessage);
				                        $atpMessageReceiver->setReceiverId($workerId);
				                        $this->em->persist($atpMessageReceiver);
									}

			                        // ATPR message
			                        $atprMessage = new AtprMessageEntity();
			                        $atprMessage->setMessageId($message->getMessageId());
			                        $atprMessage->setAtp($atprAtp);
			                        $atprMessage->setBusenosPasikeitimoData($message->getBusenosPasikeitimoData());
			                        $atprMessage->setBusenosTipas($message->getBusenosTipas());
			                        $atprMessage->setAtpMessage($atpMessage);
			                        $this->em->persist($atprMessage);
								}
							}
						}
					}
				}
		        $this->em->flush();
			}
		}
    }
}
