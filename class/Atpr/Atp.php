<?php

namespace Atp\Atpr;

use Atp\Address\AddressHelper,
    Atp\Address\Model\AddressModel,
    Atp\Atpr\Service as AtprService,
    Atp\Core,
    Atp\Document\Record,
    Atp\Document\Record\Payment,
    Atp\Entity\Address as AddressEntity,
    Atp\Entity\Atpr\Atp as AtprAtpEntity,
    Atp\Entity\Atpr\AtpDocument as AtprDocument,
    Atp\Entity\Atpr\Classifier as AtprClassifier,
    Atp\Entity\File as FileEntity,
    Atp\Entity\Record as RecordEntity,
    Atp\File\GeneratedTemplatesHandler,
    Atp\File\RecordFilesHandler,
    Atp\Record as AtpRecord,
    Atp\Record\Values as FieldValues,
    Atp\Repository\Atpr\AtpDocumentRepository,
    Atp\Repository\Atpr\AtpRepository,
    Atp\Repository\Atpr\ClassifierRepository,
    Atpr\Kis as AtprType,
    Atpr\Kis\AtpDetailsType,
    Atpr\Kis\DocumentType,
	DateTime,
    Doctrine\ORM\Query,
    Doctrine\ORM\Query\Expr\Join,
    Exception,
    SoapFault,
    stdClass;

class Atp
{

    /**
     * @var Core
     */
    private $core;

    /**
     * @param Core $core
     */
    public function __construct(Core $core)
    {
        $this->core = $core;
    }

    /**
     * @return AtpRepository
     */
    public function getRepository()
    {
        return $this->core->factory->Doctrine()
                ->getRepository(AtprAtpEntity::class);
    }

    /**
     * @return ClassifierRepository
     */
    public function getClassifierRepository()
    {
        $classifierHelper = new Classifier($this->core);
        return $classifierHelper->getRepository();
    }

    /**
     *
     * @param AtprType\NewAtpDetailsType $atp
     * @param int $recordId
     * @return Atp|boolean
     */
    public function create(AtprType\NewAtpDetailsType $atp, $recordId)
    {
        $service = new AtprService();
        $response = $service->CreateNewAtp($atp);

        if ($response && $response->getBusena() === $response::BUSENA_OK) {
            $entityManager = $this->core->factory->Doctrine();

            $entity = new AtprAtpEntity($response->getROIK());
            $entity->setPilnaNuorodaIAtpr($response->getPilnaNuorodaIATPR());
            $entity->setRecord(
                $entityManager->getReference(RecordEntity::class, $recordId)
            );

            $entityManager->persist($entity);
            $entityManager->flush($entity);

            return $entity;
        }

        return false;
    }

    /**
     * @param AtprType\NewAtpDetailsType $atp
     * @return Atp|boolean
     */
    public function edit(AtprType\NewAtpDetailsType $atp)
    {
        $service = new AtprService();
        $response = $service->EditAtp($atp);

        if ($response && $response->getBusena() === $response::BUSENA_OK) {
            return $this->getRepository()->find($response->getROIK());
        }

        return false;
    }

    /**
     * @param string $recordId
     * @return boolean
     */
    public function updateAtp($recordId)
    {
		ini_set('display_errors', true);
		$flushed = false;
		$record = $this->core->factory->Record()->getEntity($recordId);
        $atpr = $this->getRepository()->getByRecordId($recordId);
        if (empty($atpr)) {
            throw new \Exception('Atpr document not found');
        }
        
        $documentOptions = $this->core->factory->Document()
            ->getOptions($record->getDocument()->getId());
        $documentOptionsMultiple = $this->core->factory->Document()
            ->getOptions($record->getDocument()->getId(), true);
		$atpList = $this->getAtpList($atpr);
		if ($atpList) {
            $valueManager = new FieldValues($this->core);
            /* @var $atp AtprType\AtpDetailsType */
			foreach ($atpList->getAtp() as $atp) {
				// Asmens kodas, vardas, pavardė ir gimimo data pakeičiami gauti iš ATPR
				$pazeidejas = $atp->getPazeidejas();
				if ($pazeidejas) {
					if (!is_array($documentOptionsMultiple->person_code) AND empty($documentOptionsMultiple->person_code) === FALSE) {
						$valueManager->setValue(
		                    $record->getId(),
		                    $documentOptionsMultiple->person_code,
		                    $pazeidejas->getAsmensKodas()
		                );
					} else {
						foreach ($documentOptionsMultiple->person_code as $setFieldId) {
							if (empty($setFieldId) === FALSE) {
								$valueManager->setValue(
				                    $record->getId(),
				                    $setFieldId,
				                    $pazeidejas->getAsmensKodas()
				                );
							}
						}
					}
					if (!is_array($documentOptionsMultiple->person_name) AND empty($documentOptionsMultiple->person_name) === FALSE) {
						$valueManager->setValue(
		                    $record->getId(),
		                    $documentOptionsMultiple->person_name,
		                    $pazeidejas->getVardas()
		                );
					} else {
						foreach ($documentOptionsMultiple->person_name as $setFieldId) {
							if (empty($setFieldId) === FALSE) {
								$valueManager->setValue(
				                    $record->getId(),
				                    $setFieldId,
				                    $pazeidejas->getVardas()
				                );
							}
						}
					}
					if (!is_array($documentOptionsMultiple->person_surname) AND empty($documentOptionsMultiple->person_surname) === FALSE) {
						$valueManager->setValue(
		                    $record->getId(),
		                    $documentOptionsMultiple->person_surname,
		                    $pazeidejas->getPavarde()
		                );
					} else {
						foreach ($documentOptionsMultiple->person_surname as $setFieldId) {
							if (empty($setFieldId) === FALSE) {
								$valueManager->setValue(
				                    $record->getId(),
				                    $setFieldId,
				                    $pazeidejas->getPavarde()
				                );
							}
						}
					}
					if (!is_array($documentOptionsMultiple->person_birthday) AND empty($documentOptionsMultiple->person_birthday) === FALSE) {
						$valueManager->setValue(
		                    $record->getId(),
		                    $documentOptionsMultiple->person_birthday,
		                    $pazeidejas->getGimimoData()
		                );
					} else {
						foreach ($documentOptionsMultiple->person_birthday as $setFieldId) {
							if (empty($setFieldId) === FALSE) {
								$valueManager->setValue(
				                    $record->getId(),
				                    $setFieldId,
				                    $pazeidejas->getGimimoData()
				                );
							}
						}
					}
				}

				$sprendimai = $atp->getProcesiniaiSprendimai();
                if (!$sprendimai) {
                    continue;
                }

                /* @var $sprendimai AtprType\SprendimasType */
                foreach ($sprendimai->getProcesinisSprendimas() as $sprendimas) {
                    if ($sprendimas->getBusena() !== 'SURASYTAS') {
                        continue;
                    }

                    $registrationDate = $sprendimas->getSurasymoData();
//                    $registrationNo = $sprendimas->getProtokoloNumeris();
                    $registrationNo = $sprendimas->getProtokoloDokumentoNumeris();

                    if (empty($registrationDate) || empty($registrationNo)) {
                        continue;
                    }

                    $this->core->factory->Record()
                        ->setRegistrationFields(
                            $record, $registrationNo, $registrationDate
                    );

                    break;
                }


                /* @var $sprendimai AtprType\SprendimasType */
                foreach ($sprendimai->getProcesinisSprendimas() as $sprendimas) {
                    if ($sprendimas->getTipas()->getClassifierCode() != "2") {
                        continue;
                    }
                    if ($sprendimas->getBusena() !== 'PRIIMTAS') {
                        continue;
                    }
                    if ($sprendimas->getArPaskutinisSprendimas() !== true) {
                        continue;
                    }

                    $nutarimas = $sprendimas->getNutarimas();
                    if ($nutarimas) {


                        $rusis = $nutarimas->getRusis();
                        $fieldId = $documentOptions->nutarimas_sprendimas; //2032; //'col_24';
                        $classificator = $this->getClassifierRepository()
                            ->getClassificator($rusis);
//                        // If field is connected with OLD ATP classificators,
//                        // find respective classificator by label.
//                        $source = $this->core->factory->Field()
//                                ->getField($fieldId)->getSource();
//                        if ($classificator->getParent() && $source !== $classificator->getParent()->getId()) {
//                            /* @var $classificatorRepository \Atp\Repository\ClassificatorRepository */
//                            $classificatorRepository = $this->core->factory->Doctrine()
//                                ->getRepository(\Atp\Entity\Classificator::class);
//                            $classificator = $classificatorRepository->findChildByLabel($source, $classificator->getLabel());
//                        }
                        $valueManager->setValue(
                            $record->getId(),
                            $fieldId,
                            $classificator->getId()
                        );


                        $aplinkybes = $nutarimas->getLengvinanciosAplinkybes();
                        if ($aplinkybes) {
                            $fieldId = $documentOptions->nutarimas_lengvinantiAplinkybe; //2030;//'col_22';
                            foreach ($aplinkybes->getLengvinantiAplinkybe() as $aplinkybe) {
                                $classificator = $this->getClassifierRepository()
                                    ->getClassificator($aplinkybe);
                                $valueManager->setValue(
                                    $record->getId(),
                                    $fieldId,
                                    $classificator->getId()
                                );
                                break;
                            }
                        }


                        $aplinkybes = $nutarimas->getSunkinanciosAplinkybes();
                        if ($aplinkybes) {
                            $fieldId = $documentOptions->nutarimas_sunkinantiAplinkybe; //2031;//'col_23';
                            foreach ($aplinkybes->getSunkinantiAplinkybe() as $aplinkybe) {
                                $classificator = $this->getClassifierRepository()
                                    ->getClassificator($aplinkybe);
                                $valueManager->setValue(
                                    $record->getId(),
                                    $fieldId,
                                    $classificator->getId()
                                );
                                break;
                            }
                        }


                        $nuobaudos = $nutarimas->getAdministracinesNuobaudos();
                        if ($nuobaudos) {
                            $fieldId = $documentOptions->nutarimas_nuobaudosRusis; //2309; // 'kcol_185_1?';
                            foreach ($nuobaudos->getAdministracineNuobauda() as $nuobauda) {
                                if ($nuobauda->getArNuobaudaGaliojanti() !== true) {
                                    continue;
                                }
                                if ($nuobauda->getPapildomaNuobauda()) {
                                    continue;
                                }

                                $pagrindineNuobauda = $nuobauda->getPagrindineNuobauda();
                                $rusis = $pagrindineNuobauda->getRusis1();
                                $classificator = $this->getClassifierRepository()
                                    ->getClassificator($rusis);
                                
//                                // If field is connected with OLD ATP classificators,
//                                // find respective classificator by label.
//                                $source = $this->core->factory->Field()
//                                        ->getField($fieldId)->getSource();
//                                if ($classificator->getParent() && $source !== $classificator->getParent()->getId()) {
//                                    /* @var $classificatorRepository \Atp\Repository\ClassificatorRepository */
//                                    $classificatorRepository = $this->core->factory->Doctrine()
//                                        ->getRepository(\Atp\Entity\Classificator::class);
//                                    $classificator = $classificatorRepository->findChildByLabel($source, $classificator->getLabel());
//                                }
                                $valueManager->setValue(
                                    $record->getId(),
                                    $fieldId,
                                    $classificator->getId()
                                );
                                break;
                            }
                        }


                        $skundas = $nutarimas->getPateiktasSkundas();
                        if ($skundas) {
//                            // optional
//                            $skundas->getSkundoId(); // int
//                            $skundas->getApskundusiIstaiga1(); // Klasifikatorius POLICIJOS_ISTAIGA I lygis
//                            $skundas->getApskundusiIstaiga2(); // Klasifikatorius POLICIJOS_ISTAIGA II lygis
//                            $skundas->getInstitucijosTipas(); // Klasifikatorius DOKUMENTO_GAVEJAS
//                            $skundas->getKitaInstitucija(); // string 255
//                            $skundas->getSkundoTekstas(); // string 3000
//                            $skundas->getPastabos(); // string 2000


                            $apskundusiSalis = $skundas->getApskundusiSalis(); // Klasifikatorius APSKUNDUSI_SALIS
                            $fieldId = $documentOptions->nutarimas_apskundusiSalis; //2050; //'col_42';
                            $classificator = $this->getClassifierRepository()
                                ->getClassificator($apskundusiSalis);
//                            // If field is connected with OLD ATP classificators,
//                            // find respective classificator by label.
//                            $source = $this->core->factory->Field()
//                                    ->getField($fieldId)->getSource();
//                            if ($classificator->getParent() && $source !== $classificator->getParent()->getId()) {
//                                /* @var $classificatorRepository \Atp\Repository\ClassificatorRepository */
//                                $classificatorRepository = $this->core->factory->Doctrine()
//                                    ->getRepository(\Atp\Entity\Classificator::class);
//                                $classificator = $classificatorRepository->findChildByLabel($source, $classificator->getLabel());
//                            }
                            $valueManager->setValue(
                                $record->getId(),
                                $fieldId,
                                $classificator->getId()
                            );

                            
                            $data = $skundas->getApskundimoData(); // date
                            $fieldId = $documentOptions->nutarimas_apskundimoData; //2051; //'col_43';
                            $valueManager->setValue(
                                $record->getId(),
                                $fieldId,
                                $data->format('Y-m-d')
                            );


                            $institucija = $skundas->getInstitucija(); // Klasifikatorius
                            if ($institucija) {
                                $fieldId = $documentOptions->nutarimas_apskundusiInstitucija; //2052; //'col_44';
                                $valueManager->setValue(
                                    $record->getId(),
                                    $fieldId,
                                    $institucija->getClassifierEntryName()
                                );
                            }
                        }
                    }
                }

                $bauda = $atp->getBauda();
                if ($bauda) {
                    $baudosDydis = $bauda->getBaudosDydis();
                    if ($baudosDydis) {
                        $valueManager->setValue(
                            $record->getId(),
                            $documentOptions->penalty_sum,
                            $baudosDydis
                        );
                    }
                }

                $valueManager->flushValues();
                $this->updatePaymentStatus($atp, $record->getId());
				$flushed = true;
            }
		}

		$entityManager = $this->core->factory->Doctrine();
        $service = new AtprService();
        $response = $service->GetAtpDocuments(new AtprType\RequestData($atpr->getRoik()));
        $atpDocumentList = $response->getAtpDocumentList();
		if ($atpDocumentList) {
            $file = $this->core->factory->File();
            $file->SetHandler(new GeneratedTemplatesHandler($this->core));

            /* @var $atpDocument AtprType\AtpDocument */
            foreach ($atpDocumentList->getAtpDocument() as $atpDocument) {

				if (empty($atpDocument->DokumentoTurinys) === FALSE) {
					$tempFile = tmpfile();
	                fwrite($tempFile, $atpDocument->DokumentoTurinys);
	                rewind($tempFile);
	                $metaData = stream_get_meta_data($tempFile);
	
	                // Tirkina ar failas jau yra įkeltas
	                /* @var $documentRepository AtpDocumentRepository */
	                $documentRepository = $entityManager->getRepository(AtprDocument::class);
	                $isDocument = $documentRepository->isDocument($atpDocument->ROIK, $atpDocument->Pavadinimas);
	                if (!$isDocument) {
	                    $fileId = $file->Save(
	                        $metaData['uri'], $atpDocument->Pavadinimas, $atpDocument->DokumentoTipas
	                    );

	                    $atprEntity = $this->getRepository()->find($atpDocument->ROIK);
	                    $atprEntity->addDocument(
	                        $entityManager->getRepository(FileEntity::class)->find($fileId),
	                        AtprDocument::SOURCE_ATPEIR
	                    );
	                    $entityManager->persist($atprEntity);
	
	                    $file->GetCurrentHandler()->Save((object) [
	                            'id' => $fileId,
	                            'record_id' => $record->getId()
	                    ]);
	                }
	                fclose($tempFile);
				}
			}
			if ($flushed == false) {
	               $valueManager->flushValues();
			}
            $entityManager->flush();
		}
		return true;
    }

    /**
     * @param int $recordId
     * @return AtprType\AtpList
     * @throws Exception
     */
    public function getAtpListByRecordId($recordId)
    {
        $atpr = $this->getRepository()->getByRecordId($recordId);
        if (empty($atpr)) {
            throw new \Exception('Atpr document not found');
        }

        return $this->getAtpList($atpr);
    }

    /**
     * @param string $roik
     * @return AtprType\AtpList
     * @throws Exception
     */
    public function getAtpListByRoik($roik)
    {
        $atpr = $this->getRepository()->find($roik);
        if (empty($atpr)) {
            throw new \Exception('Atpr document not found');
        }

        return $this->getAtpList($atpr);
    }

    /**
     * @param AtprAtpEntity $atpr
     * @return AtprType\AtpList
     * @throws SoapFault
     * @throws Exception
     */
    public function getAtpList(AtprAtpEntity $atpr)
    {
        $service = new AtprService();

        try {
            $response = $service->SearchFullAtp((new AtprType\SearchAtpDataType())->setROIK($atpr->getRoik()));
            if (empty($response) || !$response->getAtpList()) {
                throw new \Exception('Atpr document not found');
            }
			return $response->getAtpList();
        } catch (SoapFault $soapFault) {
            throw $soapFault;
        }
    }

    /**
     * @param type $recordId
     * @return null|AtprType\BaudaType
     * @throws Exception
     * @throws SoapFault
     */
    public function getBauda($recordId)
    {
        $atpList = $this->getAtpListByRecordId($recordId);
        if ($atpList) {
            /* @var $atp AtprType\AtpDetailsType */
            foreach ($atpList->getAtp() as $atp) {
                return $atp->getBauda();
            }
        }
    }

    /**
     * @param AtpDetailsType $atp Atp object from Atpr SearchFullAtp service
     * @param int $recordId
     * @return boolean FALSE if no penalty was found or payment status is not recognized.
     */
    private function updatePaymentStatus(AtprType\AtpDetailsType $atp, $recordId)
    {
        $result = false;

        $bauda = $atp->getBauda();
        if ($bauda) {
            $paymentStatus = $bauda->getApmokejimoBusena();
            if ($paymentStatus) {
                $paymentManager = new Payment($this->core);

                $record = new Record($recordId, $this->core);

                if ($paymentStatus === 'APMOKETA') {
                    $paymentManager->SetRecordPaymentOption($record, $paymentManager::STATUS_PAID);
                    $result = true;
                } elseif ($paymentStatus === 'NEAPMOKETA') {
                    $paymentManager->SetRecordPaymentOption($record, $paymentManager::STATUS_NOT_PAID);
                    $result = true;
                }

                // Atnaujinamos būsenos
                $this->core->logic->update_record_status($recordId);
            }
        }

        return $result;
    }

    /**
     * Update record payment status.
     *
     * @param int $recordId
     * @return boolean
     */
    public function updateRecordPaymentStatus($recordId)
    {
        $atpList = $this->getAtpListByRecordId($recordId);
        if ($atpList) {
            /* @var $atp AtprType\AtpDetailsType */
            foreach ($atpList->getAtp() as $atp) {
                $this->updatePaymentStatus($atp, $recordId);
            }

            return true;
        }
    }

    public function collectAtpData($recordId)
    {
        $recordHelper = $this->core->factory->Record();

        $record = $recordHelper->getEntity($recordId);
        $recordValues = $recordHelper->getValues($record->getId(), true);
        $recordValuesUnparsed = $recordHelper->getValues($record->getId());

        $documentOptions = $this->core->factory->Document()
            ->getOptions($record->getDocument()->getId(), true);


        // Pažeidimo data
        if (true) {
            $violationDate = $this->extractFieldValue($documentOptions->violation_date, $recordValues);
            if (empty($violationDate)) {
                throw new Exception('Nepavyko nustatyti pažeidimo datos.');
            }
            $violationTime = $this->extractFieldValue($documentOptions->violation_time, $recordValues);
            if (empty($violationTime)) {
                $violationTime = '00:00:00';
            }
            $PazeidimoDataLaikas = new DateTime($violationDate . ' ' . $violationTime);
        }


        // Pažeidimo vieta
        if (true) {
            $violationPlace = $this->extractFieldValue($documentOptions->violation_place, $recordValues);
            $violationCity = $this->extractFieldValue($documentOptions->violation_city, $recordValues);

            $violationPlaceHouse = false;
			$vhouse_arr = array();
			$vhouse_arr = explode('.', $violationPlace);
			$violationPlaceHouse = str_replace(' ', '' ,$vhouse_arr[count($vhouse_arr)-1]);			
			if (empty($violationPlaceHouse) === FALSE) {
	            $violationPlace = str_replace($violationPlaceHouse, '', $violationPlace);
			}
			
			$PadarymoVietosAdresas = new AtprType\AdresasType();
            $PadarymoVietosAdresas
                ->setSalis(new AtprType\ClassifierEntryType('VALSTYBE', 'LTU'))
                ->setSavivaldybe(
                    new AtprType\AdresoElementasType(
                        AtprType\AdresoElementasType::TIPAS_SAVIVALDYBE,
                        'Vilniaus m. sav.'
                ))
                ->setVietove(
                    new AtprType\AdresoElementasType(
                        AtprType\AdresoElementasType::TIPAS_VIETOVE,
                        $violationCity
                ))
                ->setGatve(
                    new AtprType\AdresoElementasType(
                        AtprType\AdresoElementasType::TIPAS_GATVE,
                        $violationPlace
                ));

			if (empty($violationPlaceHouse) === false) {
                $PadarymoVietosAdresas->setNamas(
                    new AtprType\AdresoElementasType(
                        AtprType\AdresoElementasType::TIPAS_NAMAS,
                        $violationPlaceHouse
                ));
            }

            $violationPlaceMore = $this->extractFieldValue($documentOptions->violation_place_more, $recordValues);
            if (empty($violationPlaceMore) === false) {
                $PadarymoVietosAdresas->setGatve2(
                    new AtprType\AdresoElementasType(
                        AtprType\AdresoElementasType::TIPAS_GATVE,
                        $violationPlaceMore
                ));
            }
        }


        // Pažeidimo "tiksli vieta"
        if (true) {
            $atprAddressClassifiactorId = $this->extractFieldValue($documentOptions->atpr_address, $recordValuesUnparsed);
            if (empty($atprAddressClassifiactorId) === false) {
                $atprAddressClassifier = $this->getClassifierRepository()
                    ->getByClassificator($atprAddressClassifiactorId);

                if ($atprAddressClassifier) {
                    if ($atprAddressClassifier->getParentExternal()) {
                        $PadarymoTiksliVieta1 = new AtprType\ClassifierEntryType(
                            'ATP_TIKSLI_VIETA',
                            $atprAddressClassifier->getParentExternal()->getCode()
                        );
                        $PadarymoTiksliVieta2 = new AtprType\ClassifierEntryType(
                            'ATP_TIKSLI_VIETA',
                            $atprAddressClassifier->getCode()
                        );
                    } else {
                        $PadarymoTiksliVieta1 = new AtprType\ClassifierEntryType(
                            'ATP_TIKSLI_VIETA',
                            $atprAddressClassifier->getCode()
                        );
                    }
                }
            }
        }


        // Pažeidėjas
        if (true) {
            $personName = $this->extractFieldValue($documentOptions->person_name, $recordValues);
            if (empty($personName)) {
                throw new Exception('Nepavyko nustatyti pažeidėjo vardo.');
            }
            $personSurname = $this->extractFieldValue($documentOptions->person_surname, $recordValues);
            if (empty($personSurname)) {
                throw new Exception('Nepavyko nustatyti pažeidėjo pavardės.');
            }
            $Pazeidejas = new AtprType\PazeidejasType($personName, $personSurname);

            $personCode = $this->extractFieldValue($documentOptions->person_code_foreign, $recordValues);
            if ($personCode) {
                $Pazeidejas->setUzsieniecioAsmensKodas($personCode);
            } else {
                $personCode = $this->extractFieldValue($documentOptions->person_code, $recordValues);
                if (empty($personCode) === false) {
                    $Pazeidejas->setAsmensKodas($personCode);
                }
            }

            $personBirthday = $this->extractFieldValue($documentOptions->person_birthday, $recordValues);
            if (empty($personBirthday) === false) {
                $Pazeidejas->setGimimoData((new DateTime($personBirthday))->format('Y-m-d'));
            }


            $declaredAddress = $this->extractFieldValue($documentOptions->person_declared_address, $recordValues);
            if (empty($declaredAddress) === false && $Pazeidejas->getUzsieniecioAsmensKodas()) {
                $DeklaruotaGyvenamojiVieta = new AtprType\AdresasType();
                // TODO: valstybę imti iš sukonfiguruoto ATPR klasifikatoriaus lauko
                $DeklaruotaGyvenamojiVieta
                    ->setSalis(new AtprType\ClassifierEntryType('VALSTYBE', 'neLTU'))
                    ->setSavivaldybe(
                        new AtprType\AdresoElementasType(
                            AtprType\AdresoElementasType::TIPAS_SAVIVALDYBE,
                            $declaredAddress
                        )
                );
                $Pazeidejas->setDeklaruotaGyvenamojiVieta($DeklaruotaGyvenamojiVieta);
            } else if (empty($declaredAddress) === false) {

                // TODO: TST_VAR_DATA is utf-8 table with utf-8 data. But TST client uses utf-8 tables with latin1 data.
                $addressHelper = new AddressHelper(
                    $this->core->factory->Doctrine(true, true)
                        ->getRepository(AddressEntity::class)
                );

                try {
                    /* @var $address AddressModel */
                    $address = $addressHelper->getAddressByString($declaredAddress);

                    $DeklaruotaGyvenamojiVieta = new AtprType\AdresasType();

                    $DeklaruotaGyvenamojiVieta
                        ->setSalis(new AtprType\ClassifierEntryType('VALSTYBE', 'LTU'))
                        ->setSavivaldybe(
                            new AtprType\AdresoElementasType(
                                AtprType\AdresoElementasType::TIPAS_SAVIVALDYBE,
                                'Vilniaus m. sav.'
                            )
                    );

                    $DeklaruotaGyvenamojiVieta->setVietove(
                        new AtprType\AdresoElementasType(
                            AtprType\AdresoElementasType::TIPAS_VIETOVE,
                            $address->getCity()->getName()
                        )
                    );
                    $DeklaruotaGyvenamojiVieta->setGatve(
                        new AtprType\AdresoElementasType(
                            AtprType\AdresoElementasType::TIPAS_GATVE,
                            $address->getStreet()->getLine()
                        )
                    );
                    $DeklaruotaGyvenamojiVieta->setNamas(
                        new AtprType\AdresoElementasType(
                            AtprType\AdresoElementasType::TIPAS_NAMAS,
                            $address->getBuilding()->getLine()
                    ));
                    if ($address->getApartment()) {
                        $DeklaruotaGyvenamojiVieta->setButas(
                            new AtprType\AdresoElementasType(
                                AtprType\AdresoElementasType::TIPAS_BUTAS,
                                $address->getApartment()->getNumber()
                        ));
                    }

                    $Pazeidejas->setDeklaruotaGyvenamojiVieta($DeklaruotaGyvenamojiVieta);
                } catch (\Exception $e) {
                    error_log($e);
                    $DeklaruotaGyvenamojiVieta = new AtprType\AdresasType();
                    $DeklaruotaGyvenamojiVieta
                        ->setSalis(new AtprType\ClassifierEntryType('VALSTYBE', 'LTU'))
                        ->setSavivaldybe(
                            new AtprType\AdresoElementasType(
                                AtprType\AdresoElementasType::TIPAS_SAVIVALDYBE,
                                'Vilniaus m. sav.'
                            )
                        )
                        ->setVietove(
                            new AtprType\AdresoElementasType(
                                AtprType\AdresoElementasType::TIPAS_VIETOVE,
                                $declaredAddress
                            )
                    );
                }
            }

            $patvirtintaTapatybe = false;

            if (!empty($personCode) && !empty($personName) && !empty($personSurname)) {
                $fieldManager = $this->core->factory->Field();

                if ($fieldManager->isGrt($this->extractFieldId($documentOptions->person_name, $recordValues))
                    && $fieldManager->isGrt($this->extractFieldId($documentOptions->person_surname, $recordValues))) {
                    $patvirtintaTapatybe = true;
                }
            }
            if ($patvirtintaTapatybe === false) {
                $personDocument = $this->extractFieldValue($documentOptions->person_document, $recordValues);
                if (!empty($personName) && !empty($personSurname) && !empty($personDocument) && !empty($personBirthday)) {
                    $patvirtintaTapatybe = true;
                }
            }

            $Pazeidejas->setPatvirtintaTapatybe($patvirtintaTapatybe);

            $personPhoneNumber = $this->extractFieldValue($documentOptions->person_phone_number, $recordValues);
            if (empty($personPhoneNumber) === false) {
                $Pazeidejas->setTelefonoNumeris($personPhoneNumber);
            }

            $personEmail = $this->extractFieldValue($documentOptions->person_email, $recordValues);
            if (empty($personPhoneNumber) === false) {
                $Pazeidejas->setElPastas($personEmail);
            }

            $personCodeForeign = $this->extractFieldValue($documentOptions->person_code_foreign, $recordValues);
            if (empty($personCodeForeign) === false) {
                $Pazeidejas->setUzsieniecioAsmensKodas($personCodeForeign);
            }

            $personWorkplace = $this->extractFieldValue($documentOptions->person_workplace, $recordValues);
            if (empty($personWorkplace) === false) {
                $Pazeidejas->setDarboviete((new AtprType\JuridinisAsmuoType())->setPavadinimas($personWorkplace));
            }

            $personWorkplacePosition = $this->extractFieldValue($documentOptions->person_workplace_position, $recordValues);
            if (empty($personWorkplacePosition) === false) {
                $Pazeidejas->setPareigos($personWorkplacePosition);
            }

            $personNationalityClassifiactorId = $this->extractFieldValue($documentOptions->person_nationality, $recordValuesUnparsed);
            if (empty($personNationalityClassifiactorId) === false) {
                $personNationalityClassifier = $this->getClassifierRepository()
                    ->getByClassificator($personNationalityClassifiactorId);
                if ($personNationalityClassifier) {
                    $Pazeidejas->setPilietybe(new AtprType\ClassifierEntryType(
                        'VALSTYBE',
                        $personNationalityClassifier->getCode()
                    ));
                }
            }

            $docIssuerNationalityClassifiactorId = $this->extractFieldValue($documentOptions->doc_issuer_nationality, $recordValuesUnparsed);
			if (empty($docIssuerNationalityClassifiactorId) === false) {
                $docIssuerNationalityClassifier = $this->getClassifierRepository()
                    ->getByClassificator($docIssuerNationalityClassifiactorId);
                if ($docIssuerNationalityClassifier) {
                    $Pazeidejas->setDokumentaIsdavusiSalis(new AtprType\ClassifierEntryType(
                        'VALSTYBE',
                        $docIssuerNationalityClassifier->getCode()
                    ));
                }
            }

            $docOtherCategoryNameClassifiactorId = $this->extractFieldValue($documentOptions->doc_category_name, $recordValuesUnparsed);
			if (empty($docOtherCategoryNameClassifiactorId) === false) {
                $docOtherCategoryNameClassifier = $this->getClassifierRepository()
                    ->getByClassificator($docOtherCategoryNameClassifiactorId);
                if ($docOtherCategoryNameClassifier) {
                    $Pazeidejas->setKitoDokumentoPavadinimas(new AtprType\ClassifierEntryType(
                        'ASMENS_TAPATYBES_DOKUMENTAS',
                        $docOtherCategoryNameClassifier->getCode()
                    ));
                }
            }

            $docOtherNumber = $this->extractFieldValue($documentOptions->doc_other_number, $recordValues);
            if (empty($docOtherNumber) === false) {
                $Pazeidejas->setDokumentoNumeris($docOtherNumber);
            }

            $personGrtGender = $this->extractFieldValue($documentOptions->person_gender, $recordValuesUnparsed);
            if (empty($personGrtGender) === false) {

                if (ctype_digit((string) $personGrtGender)) {
                    $genderClassifier = $this->getClassifierRepository()
                        ->getByClassificator($personGrtGender);
                    if ($genderClassifier) {
                        $genderClassifierCode = $genderClassifier->getCode();
                    }
                } else {
                    $firstGenderLetter = strtolower(substr($personGrtGender, 0, 1));
                    switch ($firstGenderLetter) {
                        case 'm':
                            $genderClassifierCode = '0001';
                            break;
                        case 'v':
                            $genderClassifierCode = '0002';
                            break;
                        default:
                            $genderClassifierCode = '0003';
                    }
                }

                $Pazeidejas->setLytis(new AtprType\ClassifierEntryType(
                    'LYTIS', $genderClassifierCode
                ));
            }
        }


        // Atsakingas pareigūnas (aka. Pareigūnas surašęs ATP) // Tirėjas // nagrinėtojas
		// Pakeista vietomis 2016-09-27
		
		if (true) {
			$witnessName = $this->extractFieldValue($documentOptions->witness_name, $recordValues);
            if (empty($witnessName)) {
                throw new Exception('Nepavyko nustatyti pažeidimą nustačiusio pareigūno vardo.');
            }
			$witnessSurame = $this->extractFieldValue($documentOptions->witness_surname, $recordValues);
            if (empty($witnessSurame)) {
                throw new Exception('Nepavyko nustatyti pažeidimą nustačiusio pareigūno pavardės.');
            }

            $witnessInstitutionClassifierCode = null;
            $witnessInstitutionClassifiactorId = $this->extractFieldValue(
                $documentOptions->witness_institution, $recordValuesUnparsed
            );
			if (empty($witnessInstitutionClassifiactorId) === false) {
                $witnessInstitutionClassifier = $this->getClassifierRepository()
                    ->getByClassificator($witnessInstitutionClassifiactorId);
				if ($witnessInstitutionClassifier) {
                    $witnessInstitutionClassifierCode = $witnessInstitutionClassifier->getCode();
                }
            }

            $witnessInstitutionName = $this->extractFieldValue($documentOptions->witness_institution, $recordValues);
            if (empty($witnessInstitutionClassifierCode)) {
                throw new Exception('Nepavyko nustatyti ATP pažeidimą nustačiusio pareigūno institucijos ATPR kodo. Institucijos pavadinimas "' . $witnessInstitutionName . '".');
            }
/*			$PareigunasSurasesATP = new AtprType\PareigunasType(
                (new AtprType\ClassifierEntryType('POLICIJOS_ISTAIGA', $witnessInstitutionClassifierCode))
                    ->setClassifierEntryName($witnessInstitutionName)
            );
			$PareigunasSurasesATP->setVardas($witnessName);
			$PareigunasSurasesATP->setPavarde($witnessSurame);
            $witnessPosition = $this->extractFieldValue($documentOptions->witness_position, $recordValues);
            if (empty($witnessPosition) === false) {
                $PareigunasSurasesATP->setPareigos($witnessPosition);
            }
*/
			$PareigunasNustatesATP = new AtprType\PareigunasType(
                (new AtprType\ClassifierEntryType('POLICIJOS_ISTAIGA', $witnessInstitutionClassifierCode))
                    ->setClassifierEntryName($witnessInstitutionName)
            );
			$PareigunasNustatesATP->setVardas($witnessName);
			$PareigunasNustatesATP->setPavarde($witnessSurame);
            $witnessPosition = $this->extractFieldValue($documentOptions->witness_position, $recordValues);
            if (empty($witnessPosition) === false) {
                $PareigunasNustatesATP->setPareigos($witnessPosition);
            }
        }

        // Pareigūnas nustatęs ATP
        if (true) {
            $fields = $this->core->logic->record_worker_info_to_fields($record->getId());
            $workerName = null;
            $workerSurname = null;
            $workerInstitution = null;
            if (empty($fields) === false) {
                foreach ($fields as $field) {
                    if ($field['NAME'] === 'TICOL_4') {
                        $workerName = $field['VALUE'];
                        continue;
                    }
                    if ($field['NAME'] === 'TICOL_5') {
                        $workerSurname = $field['VALUE'];
                        continue;
                    }
                    if ($field['NAME'] === 'TICOL_8') {
                        $workerInstitution = $field['VALUE'];
                        continue;
                    }
                    if ($field['NAME'] === 'TICOL_6') {
                        $workerPosition = $field['VALUE'];
                        continue;
                    }
                }
            }

            $workerInstitution = 'Kita';
            if ($workerName === $witnessName && $workerSurname === $witnessSurame) {
//              $PareigunasNustatesATP = clone $PareigunasSurasesATP; //*
  //            $PareigunasNustatesATP->setInstitucija(clone $PareigunasSurasesATP->getInstitucija());
                $PareigunasSurasesATP = clone $PareigunasNustatesATP; //*
                $PareigunasSurasesATP->setInstitucija(clone $PareigunasNustatesATP->getInstitucija());
            } else {
                $workerInstitutionClassifierCode = null;
                if (empty($workerInstitution) === false) {
                    $workerInstitutionClassifier = $this->getClassifierRepository()
                        ->getByTypeAndClassificatorLabel(
                            'POLICIJOS_ISTAIGA',
                            $workerInstitution
                    );
                    if($workerInstitutionClassifier) {
                        $workerInstitutionClassifierCode = $workerInstitutionClassifier->getCode();
                    }
                }

                if (empty($workerInstitutionClassifierCode)) {
                    throw new Exception('Nepavyko nustatyti ATP surašiusio pareigūno'
                    . ' institucijos ATPR kodo. Institucijos pavadinimas "' . $workerInstitution . '".');
                }
/*
                $PareigunasNustatesATP = new AtprType\PareigunasType(
                    (new AtprType\ClassifierEntryType('POLICIJOS_ISTAIGA', $workerInstitutionClassifierCode))
                        ->setClassifierEntryName($workerInstitution)
                );
				$PareigunasNustatesATP->setVardas($workerName);
				$PareigunasNustatesATP->setPavarde($workerSurname);
                if (empty($workerPosition) === false) {
                    $PareigunasNustatesATP->setPareigos($workerPosition);
                }
*/
                $PareigunasSurasesATP = new AtprType\PareigunasType(
                    (new AtprType\ClassifierEntryType('POLICIJOS_ISTAIGA', $workerInstitutionClassifierCode))
                        ->setClassifierEntryName($workerInstitution)
                );
				$PareigunasSurasesATP->setVardas($workerName);
				$PareigunasSurasesATP->setPavarde($workerSurname);
                if (empty($workerPosition) === false) {
                    $PareigunasSurasesATP->setPareigos($workerPosition);
                }
            }
        }
		// Straipsnis
        if (true) {
            $clauseData = $this->core->logic2->get_record_clause($record->getId());
            if (empty($clauseData)) {
                throw new Exception('Nenurodytas straipsnis.');
            }

            $clauseCassifier = $this->getClassifierRepository()
                ->getByClause($clauseData['ID']);

            if (!$clauseCassifier) {
                throw new Exception('Nepavyko nustatyti ATPR klasifikatoriaus kodo'
                . ' straipsniui "' . $clauseData['NAME'] . '".');
            }

            $clauseClassifierCode = null;
            $sectionClassifierCode = null;

            if (empty($clauseCassifier->getParentExternal())) {
                $clauseClassifierCode = $clauseCassifier->getCode();
            } else {
                $clauseClassifierCode = $clauseCassifier->getParentExternal()->getCode();
                $sectionClassifierCode = $clauseCassifier->getCode();
            }

            $straipsnis = new AtprType\StraipsnisType(
                new AtprType\ClassifierEntryType('ATP_KODEKSAS', $clauseClassifierCode)
            );
            if (empty($sectionClassifierCode) === false) {
                $straipsnis->setStraipsnioDalis(
                    new AtprType\ClassifierEntryType(
                        'ATP_KODEKSAS',
                        $sectionClassifierCode
                ));
            }

            $ATPKStraipsniai = new AtprType\Straipsniai([
                $straipsnis
            ]);
        }


        // Punktas
        if (true) {
            $actData = $this->core->logic2->get_record_act($record->getId());
            if (empty($actData) === false) {
                $result = $this->core->db_query_fast('SELECT CLASSIFIER_ID
                    FROM ' . \PREFIX . 'ATP_ATPR_CLASSIFIER_ACT_XREF
                    WHERE ACT_ID = "' . $this->core->db_escape($actData['ID']) . '"');

                if ($this->core->db_num_rows($result)) {
                    $row = $this->core->db_next($result);

                    $clauseCassifier = $this->getClassifierRepository()
                        ->find($row['CLASSIFIER_ID']);

                    if ($clauseCassifier) {
                        $KitasTeisesAktas = new AtprType\KitasTeisesAktasType(
                            new AtprType\ClassifierEntryType(
                                'TEISES_AKTO_TIPAS',
                                $clauseCassifier->getCode()
                        ));
                        if (empty($actData['NR']) === false) {
                            $KitasTeisesAktas->setStraipsnis($actData['NR']);
                        }
                        $KitiTeisesAktai = new AtprType\KitiTeisesAktai([$KitasTeisesAktas]);
                    }

                }
            }
        }


        // Transporto priemonė
        if (true) {
            $recordManager = new AtpRecord($this->core);
            $vehicleData = $recordManager->getVehicleData($record->getId());

            $vehicle = new AtprType\TransportoPriemoneType(true);
			$vehicle->setUnikalusIdentifikatorius($vehicleData->unikalusIdentifikatorius);
			$vehicle->setArPagrindinePriemone(true);
			if (empty($vehicleData->savininkas) === false) {
                $savininkas = new AtprType\Savininkas();

                if ($vehicleData->savininkas->tipas === 'fizinis') {
                    $asmuo = new AtprType\FizinisAsmuoType(
                        $vehicleData->savininkas->vardas,
                        $vehicleData->savininkas->pavarde
                    );
                    $asmuo->setAsmensKodas($vehicleData->savininkas->asmensKodas);

                    $savininkas->setFizinisAsmuo($asmuo);
                } elseif ($vehicleData->savininkas->tipas === 'juridinis') {
                    $asmuo = new AtprType\JuridinisAsmuoType();
                    $asmuo->setKodas($vehicleData->savininkas->asmensKodas);
                    if (empty($vehicleData->savininkas->pavadinimas) === false) {
                        $asmuo->setPavadinimas($vehicleData->savininkas->pavadinimas);
                    } else {
                        $asmuo->setKodas(null);
                        $asmuo->setPavadinimas($vehicleData->savininkas->asmensKodas);
                    }

                    $savininkas->setJuridinisAsmuo($asmuo);
                }

                $vehicle->setSavininkas($savininkas);
            }
            if (empty($vehicleData->registracijosNumeris) === false) {
                $vehicle->setRegistracijosNumeris($vehicleData->registracijosNumeris);
            }
            if (empty($vehicleData->vinKodas) === false) {
                $vehicle->setVinKodas($vehicleData->vinKodas);
            }
            if (empty($vehicleData->pagaminimoMetai) === false) {
                $pagaminimoTimestamp = strtotime($vehicleData->pagaminimoMetai);
                if ($pagaminimoTimestamp > 0) {
                    $vehicle->setPagaminimoMetai(date('Y', $pagaminimoTimestamp));
                }
            }
            if (empty($vehicleData->marke) === false) {
                $markeClassifierData = $this->core->factory->Doctrine()->createQueryBuilder()
                    ->select('classifier.externalId, classifier.code')
                    ->from(AtprClassifier::class, 'classifier')
                    ->innerJoin(
                        'classifier.type', 'classifierType', Join::WITH, "classifierType.name = 'TP_MARKE'"
                    )
                    ->andWhere('classifier.name = :marke')
                    ->setParameter('marke', $vehicleData->marke)
                    ->getQuery()
                    ->getOneOrNullResult(Query::HYDRATE_ARRAY);
//                    ->getSingleResult();

                if ($markeClassifierData) {
                    $vehicle->setMarke(new AtprType\ClassifierEntryType('TP_MARKE', $markeClassifierData['code']));

                    if (empty($vehicleData->modelis) === false) {
                        $modelisCode = $this->core->factory->Doctrine()->createQueryBuilder()
                            ->select('classifier.code')
                            ->from(AtprClassifier::class, 'classifier')
                            ->andWhere('classifier.parentExternal = :parentClassifierId')
                            ->andWhere('classifier.name = :modelis')
                            ->setParameter('parentClassifierId', $markeClassifierData['externalId'])
                            ->setParameter('modelis', $vehicleData->modelis)
                            ->setMaxResults(1)
                            ->getQuery()
                            ->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);

                        if ($modelisCode) {
                            $vehicle->setModelis(new AtprType\ClassifierEntryType('TP_MARKE', $modelisCode));
                        }
                    }
                }
            }
            $transportoPriemones = new AtprType\TransportoPriemones([$vehicle]);
        }


        // Pažeidimo ID
        $PazeidimoId = $record->getId();


        $atp = new AtprType\NewAtpDetailsType(
            $PadarymoVietosAdresas,
            $PareigunasNustatesATP,
            $Pazeidejas,
            'VMSA-ATP-' . $PazeidimoId
        );
        $atp->setPazeidimoDataLaikas($PazeidimoDataLaikas)
			->setPareigunasSurasesATP($PareigunasSurasesATP)
            ->setTransportoPriemones($transportoPriemones);

		// Straipsniai
		if (empty($ATPKStraipsniai) === FALSE) {
			$atp->setStraipsniai($ATPKStraipsniai);
		}

        // Teisės akto punkto "Tekstas"
        if (empty($recordValues[6]) === false) {
            $atp->setPazeidimoEsme($recordValues[6]);
        }

        if (isset($KitiTeisesAktai)) {
            $atp->setKitiTeisesAktai($KitiTeisesAktai);
        }

        if (isset($PadarymoTiksliVieta1)) {
            $atp->setPadarymoTiksliVieta1($PadarymoTiksliVieta1);
            if (isset($PadarymoTiksliVieta2)) {
                $atp->setPadarymoTiksliVieta2($PadarymoTiksliVieta2);
            }
        }

        return $atp;
    }

    /**
     * Add Atp record files to Atpr object
     * @param string $atprRoik
     * @return boolean
     */
    public function addDocumentsFor($atprRoik)
    {
        $entityManager = $this->core->factory->Doctrine();


        // Get ATP record id
        $recordId = $this->getRepository()
            ->getRecordId($atprRoik);

        // Get record files
        $file = $this->core->factory->File();
        // Uploaded record files
        $file->SetHandler(new RecordFilesHandler($this->core));
        $uploadedFileIds = $file->GetRelatedFiles(array('RECORD_ID' => $recordId));
        // Generated record files
        $file->SetHandler(new GeneratedTemplatesHandler($this->core));
        $generatedFileIds = $file->GetRelatedFiles(array('RECORD_ID' => $recordId));
		$recordFileIds = array_unique(array_merge($uploadedFileIds, $generatedFileIds));
        unset($recordId, $file, $uploadedFileIds, $generatedFileIds);

        if (count($recordFileIds)) {
            // Get files mapped to ATPR document
            $atprFileIds = $this->getRepository()->getFilesIds($atprRoik);
            if (count($atprFileIds)) {
                $newFileIds = array_filter($recordFileIds, function ($fileId) use ($atprFileIds) {
                    return !in_array($fileId, $atprFileIds);
                });
            } else {
                $newFileIds = $recordFileIds;
            }
            unset($atprFileIds, $recordFileIds);

            if (count($newFileIds)) {
                // Get files data
                $query = $entityManager->getRepository(FileEntity::class)
                    ->createQueryBuilder('FILE')
                    ->select('FILE')
                    ->andWhere('FILE.id IN (:fileIds)')
                    ->setParameter('fileIds', $newFileIds);

                $files = $query->getQuery()->getResult();

                foreach ($files as $file) {
                    $this->addDocument($atprRoik, $file);
                }
                unset($query, $files);
            }
            unset($newFileIds);
        }

        return true;
    }

    /**
     * Send Atp record file to Atpr and update Atpr object.
     *
     * @param AtprAtpEntity|string $atpr Atpr object
     * @param FileEntity|int $atpFile Atp file
     * @return boolean
     */
    public function addDocument($atpr, $atpFile)
    {
        $entityManager = $this->core->factory->Doctrine();

        // Atp file
        if ($atpFile instanceof FileEntity === false) {
            $atpFile = $entityManager->find(FileEntity::class, $atpFile);
        }
        if ($atpFile instanceof FileEntity === false) {
            throw new Exception('Must be instance of ' . FileEntity::class);
        }

        // Atpr object
        if ($atpr instanceof AtprAtpEntity === false) {
            $atpr = $this->getRepository()->find($atpr);
        }
        if ($atpr instanceof AtprAtpEntity === false) {
            throw new Exception('Must be instance of ' . AtprAtpEntity::class);
        }

        // Request data
        $documentType = new AtprType\DocumentType(
            $atpr->getRoik(), $atpFile->getName(), $atpFile->getType(), $atpFile->getContent()
        );

        // Request
        if ($this->addDocumentCall($documentType)) {
            // On success add ATPR object - ATP file relation
            $atpr->addDocument($atpFile, AtprDocument::SOURCE_ATP);

            $entityManager->persist($atpr);
            $entityManager->flush($atpr);

            return true;
        }

        return false;
    }

    private function addDocumentCall(AtprType\DocumentType $documentType)
    {
        $service = new AtprService();

        try {
            $response = $service->AddNewDocument($documentType);
        } catch (SoapFault $soapFault) {
            error_log($soapFault);
        }

        if (($response && $response->getBusena() !== $response::BUSENA_OK) || isset($soapFault)) {
            $message = 'Nepavyko sekmingai atlikti ATPR operacijos.';

            if (isset($soapFault)) {
                $message .= ' ' . $soapFault->getMessage();
            } else {
                if (empty($response->getKlaidosPranesimas()) === false) {
                    $message .= ' ' . $response->getKlaidosPranesimas();
                }
            }

            throw new Exception($message);
        }

        return true;
    }

    /**
     * Get first not empty id and value pair.
     *
     * @param int|int[] $fieldId
     * @param string[] $recordValues
     * @return null|stdClass
     */
    private function extractField($fieldId, $recordValues)
    {
        if (empty($fieldId) === false) {
            if (is_array($fieldId) === false) {
                $fieldId = array($fieldId);
            }

            foreach ($fieldId as $id) {
                if (empty($recordValues[$id]) === false) {
                    $field = new stdClass();
                    $field->id = $id;
                    $field->value = $recordValues[$id];
                    return $field;
                }
            }
        }
    }

    /**
     * Get first not empty value.
     *
     * @param int|int[] $fieldId
     * @param string[] $recordValues
     * @return null|string
     */
    private function extractFieldValue($fieldId, $recordValues)
    {
        $field = $this->extractField($fieldId, $recordValues);
        if ($field) {
            return $field->value;
        }
    }

    /**
     * Get first not empty id.
     *
     * @param int|int[] $fieldId
     * @param string[] $recordValues
     * @return null|int
     */
    private function extractFieldId($fieldId, $recordValues)
    {
        $field = $this->extractField($fieldId, $recordValues);
        if ($field) {
            return $field->id;
        }
    }
}
