<?php

namespace Atp\Atpr;

use Atp\Atpr\Service as AtprService,
    Atp\Core,
    Atp\Entity\Atpr\Atp as AtprEntity,
    Atp\Entity\Atpr\AuthToken as AuthTokenEntity,
    Atp\Repository\Atpr\AuthTokenRepository,
    Atpr\Kis\authTokenResponse,
    DateTime;

class AuthToken extends authTokenResponse
{

    /**
     * @var Core
     */
    protected $core;

    /**
     * @param Core $core
     */
    public function __construct(Core $core)
    {
        $this->core = $core;
    }

    /**
     * @return AuthTokenRepository
     */
    public function getRepository()
    {
        return $this->core->factory->Doctrine()
                ->getRepository(AuthTokenEntity::class);
    }

    /**
     * Get existing token. If token is expired, a new one is created.
     * 
     * @param string $atprVartotojoVardas
     * @param string $atprRoik
     * @return AuthTokenEntity
     */
    public function get($atprVartotojoVardas, $atprRoik)
    {
        $expire = new DateTime('now + 30 seconds');

        $entityManager = $this->core->factory->Doctrine();

        $authToken = $this->getRepository()
            ->getToken($atprVartotojoVardas, $atprRoik);

        if (!$authToken || $authToken->getGaliojimoPabaigosData() < $expire) {
            $service = new AtprService();
            $response = $service->getAuthToken($atprVartotojoVardas, $atprRoik);

            if (!$authToken) {
                $authToken = new AuthTokenEntity();
                $authToken->setAtprVartotojoVardas($atprVartotojoVardas);
                $authToken->setRoik($entityManager->getReference(AtprEntity::class, $atprRoik));
            }

            $authToken->setPreAuthToken($response->getPreAuthToken());
            $authToken->setGaliojimoPabaigosData(
                (empty($response->getGaliojimoPabaigosData()) ?
                    new DateTime('now - 10 seconds') : $response->getGaliojimoPabaigosData()
                )
            );
            if (empty($response->getAtprIsorinisUrl()) === false) {
                $authToken->setAtprIsorinisUrl($response->getAtprIsorinisUrl());
            }

            $entityManager->persist($authToken);
            $entityManager->flush($authToken);
        }

        return $authToken;
    }
}
