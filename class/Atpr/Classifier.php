<?php

namespace Atp\Atpr;

use Atp\Core,
    Atp\Entity\Atpr\Classifier as ClassifierEntity,
    Atp\Repository\Atpr\ClassifierRepository,
    Doctrine\ORM\EntityManager;

class Classifier
{

    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(Core $core)
    {
        $this->em = $core->factory->Doctrine();
    }

    /**
     * @return ClassifierRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository(ClassifierEntity::class);
    }

    /**
     * Import classifiers
     */
    protected function import()
    {

    }
}
