<?php

namespace Atp\Atpr;

use Atp\Atpr\Service as AtprService,
    Atpr\Kis\AtpDetailsType,
    Atpr\Kis\SearchPersonAtpDataType,
    DateTime;

class CheckCriminalRecord
{
    /**
     * @var AtprService
     */
    private $atpeir;

    public function __construct(AtprService $atpeir = null)
    {
        $this->atpeir = $atpeir;

        if (is_null($this->atpeir)) {
            $this->atpeir = new AtprService();
        }
    }

    /**
     * Create container for search parameters.
     *
     * @param string $personalCode Search by personal code.
     * @param DateTime $atpDateFrom [optional] Filter by ATP date as "from" parameter.
     * @param DateTime $atpDateTill [optional] Filter by ATP date as "till" parameter.
     * @param boolean $validPenalty [optional] Filter only valid penalties. Default: false
     * @return SearchPersonAtpDataType
     */
    public function createSearchParameters(
        $personalCode,
        DateTime $atpDateFrom = null,
        DateTime $atpDateTill = null,
        $validPenalty = false)
    {
        $parameters = new SearchPersonAtpDataType();
        $parameters
            ->setAsmensKodas($personalCode)
            ->setPazeidimoDataNuo($atpDateFrom)
            ->setPazeidimoDataIki($atpDateTill);
        
        if ($validPenalty) {
            $parameters->setNuobaudaGaliojanti($validPenalty);
        }

        return $parameters;
    }

    /**
     * Find ATP documents.
     * 
     * @param SearchPersonAtpDataType $searchParameters
     * @return null|AtpDetailsType[]
     */
    public function findAtp(SearchPersonAtpDataType $searchParameters)
    {
        $result = $this->atpeir->searchPersonAtp($searchParameters);

        if ($result->getAtpList()) {
            return $result->getAtpList()->getAtp();
        }

        return;
    }

    /**
     * Check if person has a criminal record.
     * 
     * @param SearchPersonAtpDataType $searchParameters
     * @return boolean
     */
    public function hasCriminalRecord(SearchPersonAtpDataType $searchParameters)
    {
        return (bool) count($this->findAtp($searchParameters));
    }

    /**
     * Check if person has a criminal record.
     * 
     * @param string $personalCode Search by personal code.
     * @return boolean
     */
    public function hasCriminalRecordByCode($personalCode)
    {
        return $this->hasCriminalRecord(
                $this->createSearchParameters($personalCode)
        );
    }
}
