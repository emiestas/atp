<?php

namespace Atp\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Record
 *
 * @ORM\Table(name="ATP_RECORD", indexes=
 * {
 *  @ORM\Index(name="RECORD_ID", columns={"RECORD_ID"}),
 *  @ORM\Index(name="TABLE_TO_ID", columns={"TABLE_TO_ID"}),
 *  @ORM\Index(name="RECORD_FROM_ID", columns={"RECORD_FROM_ID", "TABLE_TO_ID"}),
 *  @ORM\Index(name="TABLE_TO_ID_2", columns={"TABLE_TO_ID", "WORKER_ID"})
 * })
 * @ORM\Entity
 */
class Record
{

    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="MAIN_ID", type="integer", nullable=false, options={"unsigned"=true,"comment"="ID, kuris nesikeicia keiciantis versijai"})
     */
    private $mainId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $recordId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_FROM_ID", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $recordFromId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TABLE_FROM_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $tableFromId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TABLE_TO_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $tableToId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_LAST", type="boolean", nullable=false, options={"unsigned"=true})
     */
    private $isLast;

    /**
     * @var boolean
     *
     * @ORM\Column(name="STATUS", type="boolean", nullable=false, options={"unsigned"=true})
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_PAID", type="boolean", nullable=false, options={"unsigned"=true})
     */
    private $isPaid = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_STATUS", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $docStatus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PUNISHMENT_CHECKED", type="boolean", nullable=false, options={"unsigned"=true,"comment"="Nurodo ar dokumente buvo tikrintas baustumas"})
     */
    private $punishmentChecked = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="REGISTERED_TEMPLATE_ID", type="integer", nullable=false, options={"unsigned"=true,"comment"="Nurodo koks sablonas registruotas avilyje"})
     */
    private $registeredTemplateId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="WORKER_ID", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $workerId;

}
