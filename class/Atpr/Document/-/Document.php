<?php

namespace Atp\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Document
 *
 * @ORM\Table(name="ATP_DOCUMENTS", indexes={@ORM\Index(name="_VALID", columns={"VALID"}), @ORM\Index(name="_PARENT_ID", columns={"PARENT_ID"})})
 * @ORM\Entity
 */
class Document
{

    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=255, nullable=false)
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="STATUS", type="boolean", nullable=true, options={"unsigned"=true,"comment"="1 - nesvarbu; 2 - tyrimui; 3 - nagrinejimui"})
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CAN_EDIT", type="boolean", nullable=false, options={"unsigned"=true})
     */
    private $canEdit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CAN_END", type="boolean", nullable=false, options={"unsigned"=true})
     */
    private $canEnd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CAN_NEXT", type="boolean", nullable=false, options={"unsigned"=true})
     */
    private $canNext;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CAN_IDLE", type="boolean", nullable=false, options={"unsigned"=true})
     */
    private $canIdle;

    /**
     * @var integer
     *
     * @ORM\Column(name="IDLE_DAYS", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idleDays;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CAN_TERM", type="boolean", nullable=false, options={"unsigned"=true,"comment"="Terminas (0 - nera, 1 - nuo sukurimo, 2 - nuo nagrinejimo)"})
     */
    private $canTerm;

    /**
     * @var integer
     *
     * @ORM\Column(name="TERM_DAYS", type="integer", nullable=false, options={"unsigned"=true,"comment"="Termino dienos"})
     */
    private $termDays;

    /**
     * @var integer
     *
     * @ORM\Column(name="VIOLATION_DATE", type="integer", nullable=false, options={"unsigned"=true,"comment"="Pazeidimo datos laukas"})
     */
    private $violationDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="PERSON_CODE", type="integer", nullable=false, options={"unsigned"=true,"comment"="Pazeidejo asmens kodo laukas"})
     */
    private $personCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="PERSON_ADDRESS", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $personAddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="COMPANY_ADDRESS", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $companyAddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="OTHER_ADDRESS", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $otherAddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="PERSON_NAME", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $personName;

    /**
     * @var integer
     *
     * @ORM\Column(name="PERSON_SURNAME", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $personSurname;

    /**
     * @var integer
     *
     * @ORM\Column(name="COMPANY_NAME", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $companyName;

    /**
     * @var integer
     *
     * @ORM\Column(name="FILL_DATE", type="integer", nullable=false, options={"unsigned"=true,"comment"="Surasymo datos laukas"})
     */
    private $fillDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="AVILYS_DATE", type="integer", nullable=false, options={"unsigned"=true,"comment"="Registracijos avilyje datos laukas"})
     */
    private $avilysDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="AVILYS_NUMBER", type="integer", nullable=false, options={"unsigned"=true,"comment"="Registracijos avilyje numerio laukas"})
     */
    private $avilysNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="PENALTY_SUM", type="integer", nullable=false, options={"unsigned"=true,"comment"="Baudos sumos laukas"})
     */
    private $penaltySum;

    /**
     * @var integer
     *
     * @ORM\Column(name="CLASS_TYPE", type="integer", nullable=false, options={"unsigned"=true,"comment"="Pazeidimo tipo laukas"})
     */
    private $classType;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR", type="integer", nullable=false, options={"unsigned"=true,"comment"="Nagrinejantis pareigunas"})
     */
    private $examinator;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_CABINET", type="integer", nullable=false, options={"unsigned"=true,"comment"="Nagrinejancio pareiguno kabinetas"})
     */
    private $examinatorCabinet;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_PHONE", type="integer", nullable=false, options={"unsigned"=true,"comment"="Nagrinejancio pareiguno telefonas"})
     */
    private $examinatorPhone;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_EMAIL", type="integer", nullable=false, options={"unsigned"=true,"comment"="Nagrinejancio pareiguno el.pastas"})
     */
    private $examinatorEmail;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_INSTITUTION_NAME", type="integer", nullable=false, options={"unsigned"=true,"comment"="Nagrinejanti institucija"})
     */
    private $examinatorInstitutionName;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_UNIT", type="integer", nullable=false, options={"unsigned"=true,"comment"="Nagrinejantis dalinys"})
     */
    private $examinatorUnit;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_SUBUNIT", type="integer", nullable=false, options={"unsigned"=true,"comment"="Nagrinejantis padalinys"})
     */
    private $examinatorSubunit;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_INSTITUTION_ADDRESS", type="integer", nullable=false, options={"unsigned"=true,"comment"="Nagrinejancios institucijos adresas"})
     */
    private $examinatorInstitutionAddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_DATE", type="integer", nullable=false, options={"unsigned"=true,"comment"="Nagrinejimo data"})
     */
    private $examinatorDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_START_TIME", type="integer", nullable=false, options={"unsigned"=true,"comment"="Nagrinejimo pradzios laikas"})
     */
    private $examinatorStartTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_FINISH_TIME", type="integer", nullable=false, options={"unsigned"=true,"comment"="Nagrinejimo pabaigos laikas"})
     */
    private $examinatorFinishTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="DECISION_DATE", type="integer", nullable=false, options={"unsigned"=true,"comment"="Nutarimo priemimo datos laukas"})
     */
    private $decisionDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="APPEALED_DATE", type="integer", nullable=false, options={"unsigned"=true,"comment"="Apeliacijos datos laukas"})
     */
    private $appealedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="APPEAL_STC_DATE", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $appealStcDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="APPEAL_CCD_DATE", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $appealCcdDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="VIOLATION_TIME", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $violationTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="VIOLATION_PLACE", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $violationPlace;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false, options={"comment"="Sukurimo data"})
     */
    private $createDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="UPDATE_DATE", type="datetime", nullable=false)
     */
    private $updateDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VALID", type="boolean", nullable=false, options={"unsigned"=true,"default"="1"})
     */
    private $valid = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="WITNESS_NAME", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $witnessName;

    /**
     * @var integer
     *
     * @ORM\Column(name="WITNESS_SURNAME", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $witnessSurname;

    /**
     * @var integer
     *
     * @ORM\Column(name="WITNESS_INSTITUTION", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $witnessInstitution;

    /**
     * @var Document
     *
     * @ORM\ManyToOne(targetEntity="Document")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PARENT_ID", referencedColumnName="ID")
     * })
     */
    private $parent;

}
