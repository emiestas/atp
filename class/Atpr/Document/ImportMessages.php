<?php

//
//namespace Atp\Atpr;
//
//require_once(__DIR__ . '/../../../atpr/autoload.php');
//
//use \Atpr\KisAdapter;
//use \Atpr\Kis;
//
//class ImportMessages
//{
//
//    protected $em;
//
//    public function __construct(\Atp\Core $core)
//    {
//        $this->em = $core->factory->Doctrine();
//    }
//
//    /**
//     * Import new messages
//     */
//    public function importNew()
//    {
//        // Newest message
//        $repository = $this->em->getRepository('\Atp\Entity\Atpr\Message');
//        $repository instanceof \Atp\Repository\Atpr\MessageRepository;
//        $dateTime = $repository->getNewestMessageDate();
//
//        // Filter
//        $filter = new Kis\SearchMessagesType();
//        if ($dateTime) {
//            $filter->setDataNuo($dateTime);
//        }
//
//
//        $this->import($filter);
//    }
//
//    /**
//     * Import messages
//     * @param \Atpr\Kis\SearchMessagesType $filter [optional]
//     */
//    protected function import(Kis\SearchMessagesType $filter = null)
//    {
//        if ($filter === null) {
//            $filter = new Kis\SearchMessagesType();
//        }
//
//        // Query
//        $adapter = new \Atpr\KisAdapter();
//        $response = $adapter->SearchMessages($filter);
//
//        $responseData = $response->getResponseData();
//        if (empty($responseData) === FALSE) {
//
//            foreach ($responseData->getMessage() as $messagesData) {
//
//                // Get existing or create new ATPR object
//                $atprAtp = $this->em->getRepository('\Atp\Entity\Atpr\Atp')
//                    ->find($messagesData->getROIK());
//
//                if (!$atprAtp) {
//                    $atprAtp = new \Atp\Entity\Atpr\Atp(
//                        $messagesData->getROIK(), NULL
//                    );
//                    $this->em->persist($atprAtp);
//                }
//
//                foreach ($messagesData->getMessages() as $message) {
//
//                    // ATP message
//                    $atpMessage = new \Atp\Entity\Message();
//                    $atpMessage->setSubject($message->getBusenosTipas());
//                    $atpMessage->setCreateDate($message->getBusenosPasikeitimoData());
//                    $this->em->persist($atpMessage);
//
//                    // ATPR message
//                    $atprMessage = new \Atp\Entity\Atpr\Message();
//                    $atprMessage->setMessageId($message->getMessageId());
//                    $atprMessage->setAtp($atprAtp);
//                    $atprMessage->setBusenosPasikeitimoData($message->getBusenosPasikeitimoData());
//                    $atprMessage->setBusenosTipas($message->getBusenosTipas());
//                    $atprMessage->setAtpMessage($atpMessage);
//
//                    $this->em->persist($atprMessage);
//
//                    // TODO: check if ATPR object is related to ATP record. If is - add message receivers
//                }
//            }
//
//            $this->em->flush();
//        }
//    }
//}
