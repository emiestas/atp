<?php

namespace Atp\Entity\Document;

/**
 * Message
 *
 * @Table(name="ATP_DOCUMENT")
 * @Entity
 */
class Optionsa
{

    /**
     * @var integer
     *
     * @Column(name="MESSAGE_ID", type="bigint", nullable=false, options={"unsigned"=true})
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $messageId;

    /**
     * @var \DateTime
     *
     * @Column(name="BUSENOS_PASIKEITIMO_DATA", type="datetime", nullable=false)
     */
    private $busenosPasikeitimoData;

    /**
     * @var string
     *
     * @Column(name="BUSENOS_TIPAS", type="string", length=255, nullable=false)
     */
    private $busenosTipas;

}
