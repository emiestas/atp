<?php

namespace Atp\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * File
 *
 * @ORM\Entity
 * @ORM\Table(name="ATP_FILES")
 * @Gedmo\Uploadable(path="/srv/www/idamas/webPartner2/subsystems/atp/files", callback="myCallbackMethod", filenameGenerator="SHA1", allowOverwrite=true, appendNumber=true)
 */
class File
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=500, nullable=false)
     * @Gedmo\UploadableFileName
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="PATH", type="string", length=5000, nullable=false)
     * @Gedmo\UploadableFilePath
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=200, nullable=false)
     * @Gedmo\UploadableFileMimeType
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="SIZE", type="string", length=50, nullable=false)
     * @Gedmo\UploadableFileSize
     */
    private $size;

    public function myCallbackMethod(array $info)
    {
        // Do some stuff with the file..
    }
}
