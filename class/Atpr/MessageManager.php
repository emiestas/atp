<?php

namespace Atp\Atpr;

require_once(__DIR__ . '/../../../atpr/autoload.php');

use \Atpr\KisAdapter;

class MessageManager
{

    /**
     * @var \Atp_Core 
     */
    private $core;

    /**
     * @param \Atp_Core $core
     */
    public function __construct(\Atp_Core $core)
    {
        $this->core = $core;
    }

    /**
     * @return \Atp\Repository\Atpr\MessageWorkerRepository
     */
    public function getMessageWorkerRepository()
    {
        $entityManager = $this->core->fs()->Doctrine();
        return $entityManager->getRepository(
                '\Atp\Entity\Atpr\MessageWorker'
        );
    }

    /**
     * @param \Atp\Entity\Atpr\MessageWorker|int $workerMessage
     * @return boolean
     */
    public function markRead($workerMessage)
    {
        $entityManager = $this->core->fs()->Doctrine();

        if (is_int($workerMessage) || is_numeric($workerMessage)) {
            $repository = $this->getMessageWorkerRepository();
            $workerMessage = $repository->find($workerMessage);
        }

        if ($workerMessage instanceof \Atp\Entity\Atpr\MessageWorker) {

            // ATPR web-service
            // TODO: move to automatic tasks
            $adapter = new \Atpr\KisAdapter();
            try {
                $response = $adapter->MarkMessagesAsRead(
                    $workerMessage->getMessage()->getMessageId()
                );
            } catch (\SoapFault $soapFault) {
                error_log($soapFault);
            }

            // On error
            if (($response && $response->getBusena() !== $response::BUSENA_OK) || isset($soapFault)) {
                $message = 'ATPR operacijos atlikti nepavyko.';

                if (isset($soapFault)) {
                    $message .= ' ' . $soapFault->getMessage();
                } else {
                    if (empty($response->getKlaidosPranesimas()) === FALSE) {
                        $message .= ' ' . $response->getKlaidosPranesimas();
                    }
                }

                throw new \Exception($message);
            }

            // Marking message as read
            $workerMessage->setIsRead(true);
            $workerMessage->setReadDate(new \DateTime('now'));

            $entityManager->merge($workerMessage);
            $entityManager->flush();

            return true;
        }

        return false;
    }
}
