<?php

namespace Atp\Atpr;

use \Atpr\Kis\MessageInfoType;

class MessageListPage
{

    private $core;

    /**
     * @param \Atp_Core $core
     */
    public function __construct(\Atp_Core $core)
    {
        $this->core = $core;
    }

    public function render()
    {
        $workerId = $this->core->user->worker['ID'];
		$documentId = filter_input(INPUT_GET, 'documentId', FILTER_VALIDATE_INT);


        $tmpl = $this->core->Template();
        $tmpl_handler = 'message_list_file';
        $tmpl->set_file($tmpl_handler, 'message_list.tpl');


        $messageManager = new \Atp\Atpr\MessageManager($this->core);
        $repository = $messageManager->getMessageWorkerRepository();

        $filter = [
            'messageWorker.workerId' => $workerId
        ];
        if (empty($documentId) === FALSE) {
            $filter['record.tableToId'] = $documentId;
        }
        $totalMessages = $repository->count($filter, 'messageWorker.workerId');


        // Puslapiavimas
        include_once($this->core->config['REAL_URL'] . 'helper/paginator.php');
        $paginator = new \Paginator();
        $paginator->items_per_page = (!empty($_GET['ipp'])) ? $_GET['ipp'] : 20;
        $paginator->items_total = $totalMessages;
        $paginator->paginate();


        $messages = $repository
            ->getBuilder($filter)
            ->orderBy('messageWorker.sendDate', 'DESC')
            ->setMaxResults(
                ($paginator->items_per_page === 'All' ? $paginator->items_total : $paginator->items_per_page)
            )
            ->setFirstResult($paginator->limit_start)
            ->getQuery()
            ->getResult();


        // Pranešimų lentelė
        $tableHtml = '';
        $tpl_arr = $this->core->getTemplate('table');

        // table
        $table = array();
        $table['table_class'] = 'message-list';
        $tableHtml .= $this->core->returnHTML($table, $tpl_arr[0]);


        // Lentelės antraštė
        $header = array('row_html' => '');
        $header['row_class'] = 'table-row table-header';
        $tableHtml .= $this->core->returnHTML($header, $tpl_arr[1]);


        $columns = array(
            'ID' => 'Pranešimo ID',
            'DATE' => 'Pasikeitimo data',
            'TYPE' => 'Būsenos tipas',
            'ACTIONS' => $this->core->lng('atp_table_actions')
        );
        foreach ($columns as $columnName => $column) {
            $cell = array(
                'cell_html' => '',
                'cell_class' => 'table-cell cell-' . $columnName,
                'cell_content' => $column
            );
            $tableHtml .= $this->core->returnHTML($cell, $tpl_arr[2]);
        }
        $tableHtml .= $this->core->returnHTML($header, $tpl_arr[3]);

        if (empty($messages) === FALSE) {
            foreach ($messages as $key => $message) {

                $row = array(
                    'row_html' => 'id="atp-table-list-item-id-' . $messageId . '"',
                    'row_class' => 'table-row'
                );
                $tableHtml .= $this->core->returnHTML($row, $tpl_arr[1]);

                $table = array(
                    'ID' => $message->getMessage()->getMessageId(),
                    'DATE' => $message->getMessage()->getBusenosPasikeitimoData()->format('Y-m-d H:i:s'),
                    'TYPE' => $message->getMessage()->getBusenosTipas(),
                    'ACTIONS' => ($message->isRead() ? null : '<a class="atp-html-button" href="index.php?m=23&id=' . $message->getId() . '&redirect=' . urlencode($_SERVER['REQUEST_URI']) . '">Perskaityti</a>')
                );

                foreach ($columns as $columnName => $column) {
                    $cell = array('cell_html' => '', 'cell_content' => '');
                    $cell['cell_class'] = 'table-cell cell-' . $columnName;
                    $cell['cell_content'] .= $table[$columnName];
                    $tableHtml .= $this->core->returnHTML($cell, $tpl_arr[2]);
                }

                $tableHtml .= $this->core->returnHTML($row, $tpl_arr[3]);
            }
        } else {
            $row = array(
                'row_html' => '',
                'row_class' => 'table-row'
            );
            $tableHtml .= $this->core->returnHTML($row, $tpl_arr[1]);

            $cell = array(
                'cell_html' => ' colspan="4"',
                'cell_content' => 'Pranešimų nėra',
                'cell_class' => 'table-cell'
            );
            $tableHtml .= $this->core->returnHTML($cell, $tpl_arr[2]);
        }

        $tableHtml .= $this->core->returnHTML($table, $tpl_arr[4]);


        $tmpl->set_var(array(
            'table' => $tableHtml . $paginator->display_pages() . '<br/>' . $paginator->display_selects(),
            'sys_message' => $this->core->get_messages(),
            'GLOBAL_SITE_URL' => $this->core->config['GLOBAL_SITE_URL']
        ));

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    /**
     * @return boolean
     * @throws \Exception If worker ID of message does not match user ID
     */
    public function markRead()
    {
        if (filter_has_var(INPUT_GET, 'id')) {
            $workerMessageId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

            $entityManager = $this->core->fs()->Doctrine();
            $repository = $entityManager->getRepository('\Atp\Entity\Atpr\MessageWorker');
            $workerMessage = $repository->find($workerMessageId);

            if ($workerMessage->getWorkerId() !== (int) $this->core->user->worker['ID']) {
                throw new \Exception(
                'Nepakanka teisių koreguoti kito darbuotojo pranešimą.', 403
                );
            }

            $messagesManger = new \Atp\Atpr\MessageManager($this->core);
            $result = $messagesManger->markRead($workerMessage);

            return $result;
        }

        return false;
    }
}
