<?php

namespace Atp\Atpr;

class ServiceDataCollector
{

    protected $atp;

    protected $record;

    protected $values;

    protected $unparsedValues;

    protected $documentOptions;

    public function __construct(\Atp\Core $atp, \Atp\Entity\Record $record)
    {
        $this->atp = $atp;
        $this->record = $record;
    }

    protected function getValues($parseValues = true)
    {
        if ($parseValues) {
            if ($this->values !== null) {
                return $this->values;
            }
        } else {
            if ($this->unparsedValues !== null) {
                return $this->unparsedValues;
            }
        }

        $recordHelper = $this->atp->factory->Record();

        $values = $recordHelper->getValues($this->record->getId(), $parseValues);

        if ($parseValues) {
            $this->values = $values;
        } else {
            $this->unparsedValues = $values;
        }

        return $values;
    }
}
