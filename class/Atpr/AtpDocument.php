<?php

namespace Atp\Atpr;

require_once(__DIR__ . '/../../../atpr/autoload.php');

use \Atpr\KisAdapter;

class AtpDocument
{

    function create()
    {
        $PadarymoVietosAdresas = new \Atpr\Kis\AdresasType(); //*

        $salis = new \Atpr\Kis\ClassifierEntryType('VALSTYBE', 'LTU');
        $salis->setClassifierEntryName('LITHUANIA');

        $PadarymoVietosAdresas
            ->setSalis($salis) //*
            ->setSavivaldybe(new \Atpr\Kis\AdresoElementasType(
                'SAVIVALDYBE', 'Vilniaus'
            )) //*
            ->setSeniunija(new \Atpr\Kis\AdresoElementasType(
                'SENIUNIJA', 'Vilniaus'
            ))
            ->setVietove(new \Atpr\Kis\AdresoElementasType(
                'VIETOVE', 'Vilnius'
            ))
            ->setGatve(new \Atpr\Kis\AdresoElementasType(
                'GATVE', 'Žalgirio'
            ))
            ->setGatve2(new \Atpr\Kis\AdresoElementasType(
                'GATVE', 'Žirmūnų'
            ))
            ->setNamas(new \Atpr\Kis\AdresoElementasType(
                'NAMAS', 49
            ))
            ->setButas(new \Atpr\Kis\AdresoElementasType(
                'BUTAS', 11
        ));




        $PareigunasNustatesATP = null;
        $Pazeidejas = null;
        $ATPKStraipsniai = null;
        $PazeidimoId = 1; //*
        $PazeidimoDataLaikas = new \DateTime('now'); //*





        $atp = new \Atpr\Kis\NewAtpDetailsType($PadarymoVietosAdresas,
            $PareigunasNustatesATP, $Pazeidejas, $ATPKStraipsniai, $PazeidimoId);

        $atp->setPazeidimoDataLaikas($PazeidimoDataLaikas);


        $adapter = new \Atpr\KisAdapter();
        $response = $adapter->CreateNewAtp($atp);

        if ($response->getBusena() === $response::BUSENA_OK) {
            return true;
        }

        return false;
    }
}
