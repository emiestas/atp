<?php

namespace Atp\Repository;

use Atp\Entity\FieldConfiguration;
use Atp\Entity\FieldConfigurationType;
use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\Types\Type;

class FieldConfigurationRepository extends EntityRepository
{

    /**
     * Get all configuration entries for field.
     *
     * @param int|int[] $fieldIds
     * @return FieldConfiguration[]
     */
    public function getByFieldId($fieldIds) {
        $query = $this->getEntityManager()->createQuery('
            SELECT
                configuration, field, type
            FROM
                Atp\Entity\FieldConfiguration configuration
                    LEFT JOIN configuration.field field
                    LEFT JOIN configuration.type type
            WHERE
                configuration.field IN(:fieldIds)')
            ->setParameter('fieldIds', $fieldIds);
        
        return $query->getResult();
    }

    /**
     * Get all configuration types for field.
     *
     * @param int $fieldId
     * @return FieldConfigurationType[]
     */
    public function getTypesByField($fieldId)
    {
        $query = $this->getEntityManager()->createQuery('
            SELECT
                configuration, type, field
            FROM
                Atp\Entity\FieldConfiguration configuration
                    INNER JOIN configuration.field field
                    INNER JOIN configuration.type type
            WHERE
                field.id = :fieldId')
            ->setParameter('fieldId', $fieldId, Type::INTEGER);

        return array_map(function (\Atp\Entity\FieldConfiguration $item) {
            return $item->getType();
        }, $query->getResult());
    }

    /**
     * Remove configurations by field.
     *
     * @deprecated
     * @removed Use EntityManager::remove($entity) and flush.
     * @param int $fieldId
     */
    public function removeByField($fieldId)
    {
        throw new \Exception('Method ' . __METHOD . ' was removed.');

        $query = $this->getEntityManager()->createQuery('
            DELETE
            FROM
                Atp\Entity\FieldConfiguration configuration
            WHERE
                configuration.fieldId = :fieldId')
            ->setParameter('fieldId', $fieldId, Type::INTEGER);

        $query->execute();
    }

    /**
     * Remove configurations by list of fields.
     *
     * @deprecated
     * @removed Use EntityManager::remove($entity) and flush.
     * @param int[] $fieldsIds
     */
    public function removeByFields(array $fieldsIds)
    {
        throw new \Exception('Method ' . __METHOD . ' was removed.');

        $query = $this->getEntityManager()->createQuery('
            DELETE
            FROM
                Atp\Entity\FieldConfiguration configuration
            WHERE
                configuration.fieldId IN (:fieldsIds)')
            ->setParameter('fieldsIds', $fieldsIds);

        $query->execute();
    }
}
