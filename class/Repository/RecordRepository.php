<?php

namespace Atp\Repository;

use Atp\Entity\Record;
use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Query\Expr\Join;

class RecordRepository extends EntityRepository
{

    /**
     * Query builder for getting next document records.
     *
     * @param int $recordId
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function nextForIdQueryBuilder($recordId)
    {
        return $this->createQueryBuilder('record')
                ->select('record')
                ->innerJoin('record.parent', 'parentRecord')
                ->where('parentRecord.id = :recordId')
// Better not to use it with adjacency hierarcy               ->andWhere('record.isLast = 1')
                ->setParameter('recordId', $recordId, Type::INTEGER);
    }

    /**
     * Check whether next document record was created.
     *
     * @param int $recordId
     * @return bool
     */
    public function isNextForId($recordId)
    {
        return (bool) $this->nextForIdQueryBuilder($recordId)
                ->select('COUNT(record)')
                ->getQuery()
                ->getSingleScalarResult();
    }

    /**
     * Query builder for getting next document records in specific document.
     *
     * @param int $recordId
     * @param int $documentId
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function nextDocumentForIdQueryBuilder($recordId, $documentId)
    {
        return $this->nextForIdQueryBuilder($recordId)
                ->innerJoin('record.document', 'document', Join::WITH, 'document.id = :documentId')
                ->setParameter('documentId', $documentId, Type::INTEGER);
    }

    /**
     * Check whether next document record was created in document.
     *
     * @param int $recordId
     * @param int $documentId
     * @return bool
     */
    public function isNextDocumentForId($recordId, $documentId)
    {
        return (bool) $this->nextDocumentForIdQueryBuilder($recordId, $documentId)
                ->select('COUNT(record)')
                ->getQuery()
                ->getSingleScalarResult();
    }

    /**
     * Get next document record created in specific document.
     *
     * @param int $recordId
     * @param int $documentId
     * @return Record
     */
    public function getNextDocumentForId($recordId, $documentId)
    {
        return $this->nextDocumentForIdQueryBuilder($recordId, $documentId)
                ->getQuery()
                ->getSingleResult();
    }

    /**
     * Get previous document record created in specific document.
     *
     * @param int $recordId
     * @param int $documentId
     * @return Record
     */
    public function getPreviousDocumentForId($recordId, $documentId)
    {
        do {
            /* @var $record \Atp\Entity\Record */
            $record = $this->find($recordId);
            $parentRecord = $record->getParent();
            if ($parentRecord) {
                if ($parentRecord->getDocument()->getId() === (int) $documentId) {
                    return $parentRecord;
                }
                $recordId = $parentRecord->getId();
            }
        } while ($parentRecord);
    }
}
