<?php

namespace Atp\Repository\Avilys;

use Doctrine\ORM\EntityRepository;

class ConfigurationRepository extends EntityRepository
{

    /**
     * @param int $documentId
     * @param int $templateId [optional]
     * @param int $departmentId [optional]
     * @return null|\Atp\Entity\Avilys\Configuration
     */
    public function findOneConfiguration($documentId, $templateId = null, $departmentId = null)
    {
        $queryBuilder = $this->createQueryBuilder('configuration');
        $queryBuilder
            ->innerJoin('configuration.atpDocument', 'document')
            ->andWhere('document.id = :documentId')
            ->setParameter('documentId', $documentId)
            ->setMaxResults(1);

        if ($templateId) {
            $queryBuilder
                ->leftJoin('configuration.atpTemplate', 'template')
                ->andWhere(
                    $queryBuilder->expr()->orX()
                    ->add('template.id = :templateId')
                    ->add('template.id IS NULL')
                )
                ->setParameter('templateId', $templateId);

            $queryBuilder->addOrderBy('template.id', 'DESC');
        }

        if ($departmentId) {
            $queryBuilder
                ->leftJoin('configuration.atpDepartment', 'department')
                ->andWhere(
                    $queryBuilder->expr()->orX()
                    ->add('department.id = :departmentId')
                    ->add('department.id IS NULL')
                )
                ->setParameter('departmentId', $departmentId);

            $queryBuilder->addOrderBy('department.id', 'DESC');
        }

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
