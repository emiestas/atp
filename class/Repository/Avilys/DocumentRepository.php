<?php

namespace Atp\Repository\Avilys;

use Atp\Entity\Avilys\Document;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Query\Expr\Join;

class DocumentRepository extends EntityRepository
{

    /**
     * Query builder for getting avilys document by ATP record id.
     *
     * @param int $recordId
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function byRecordIdQueryBuilder($recordId)
    {
        return $this->createQueryBuilder('avilysDocument')
                ->select('avilysDocument, record')
                ->innerJoin('avilysDocument.record', 'record', Join::WITH, 'record.id = :recordId')
                ->setParameter('recordId', $recordId, Type::INTEGER);
    }

    /**
     * Get Oid of documents which is still not registered.
     * @return null|string[]
     */
    public function getUnregisteredOids()
    {
		$limit = 5;
		$unregisteredDocumentsOid = $this->createQueryBuilder('avilysDocument')
            ->select('avilysDocument.oid')
            ->andWhere('avilysDocument.registrationNo IS NULL')
			->orderBy('avilysDocument.checkDate IS NULL', 'ASC')
			->orderBy('avilysDocument.checkDate', 'ASC')
			->setFirstResult(0)
			->setMaxResults($limit)
			->getQuery()
            ->getScalarResult();

        if (empty($unregisteredDocumentsOid) === false) {
            return array_column($unregisteredDocumentsOid, 'oid');
        }

        return;
    }

    /**
     * @param string $docOid
     * @return \Atp\Entity\Avilys\Document
     */
    public function getRecordId($docOid)
    {
        $recordId = $this->createQueryBuilder('avilysDocument')
            ->select('record.id')
            ->innerJoin('avilysDocument.record', 'record')
            ->where('avilysDocument.oid = :oid')
            ->setParameter('oid', $docOid)
            ->getQuery()
            ->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);

        return $recordId;
    }

    /**
     * Get avilys document by ATP record id.
     * 
     * @param int $recordId
     * @return null|\Atp\Entity\Avilys\Document
     */
    public function getByRecordId($recordId)
    {
        return $this->byRecordIdQueryBuilder($recordId)
                ->getQuery()
                ->getOneOrNullResult();
    }

    /**
     * Check if Avilys document exists for ATP record.
     * @param int $recordId ATP record id.
     * @return boolean
     */
    public function isByRecordId($recordId)
    {
        return (bool) $this->byRecordIdQueryBuilder($recordId)
                ->select('COUNT(avilysDocument.oid)')
                ->getQuery()
                ->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);
    }
}
