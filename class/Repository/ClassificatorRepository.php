<?php

namespace Atp\Repository;

use Atp\Entity\Classificator;
use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\Types\Type;
use Gedmo\Tree\Traits\Repository\ORM\ClosureTreeRepositoryTrait;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;

class ClassificatorRepository extends EntityRepository
{

    use ClosureTreeRepositoryTrait;

    /**
     * @param EntityManager $em
     * @param ClassMetadata $class
     */
    public function __construct(EntityManager $em, ClassMetadata $class)
    {
        parent::__construct($em, $class);

        $this->initializeTreeRepository($em, $class);
    }

    /**
     * Get direct children of classificator.
     *
     * @param int $classifiactorId Classificator ID.
     * @return Classificator[]
     */
    public function getChildrenById($classifiactorId)
    {
        $node = $this->getEntityManager()->getReference('\Atp\Entity\Classificator', $classifiactorId);

        return $this->getChildren($node, true);
    }

    /**
     * Get all children IDs of classificator.
     * @param int $classificatorId [optional] Classifiactor ID. If not specified, all classificators will be returned.
     * @return int[]
     */
    public function getAllChildrenIds($classificatorId = null)
    {
        $node = null;
        if (empty($classificatorId) === false) {
            $node = $this->getEntityManager()->getReference('\Atp\Entity\Classificator', $classificatorId);
        }
        $allChildren = $this->getChildren($node);

        return array_map(function(Classificator $classificator) {
            return $classificator->getId();
        }, $allChildren);
    }

    /**
     * Get direct parent of classificator.
     *
     * @param int $classifiactorId Classificator ID.
     * @return Classificator[]
     */
    public function getParentById($classifiactorId)
    {
        $node = $this->getEntityManager()->getReference('\Atp\Entity\Classificator', $classifiactorId);

        $ancestors = $this->getAncestors($node, true);
        
        return $ancestors[0];
    }

    /**
     * Get direct child of classificator by child label.
     *
     * @param int $parentClassificatorId
     * @param string $childLabel
     * @return null|Classificator
     */
    public function findChildByLabel($parentClassificatorId, $childLabel)
    {
        $children = $this->getChildrenById($parentClassificatorId);
        foreach ($children as $child) {
            if($child->getLabel() === $childLabel) {
                return $child;
}
        }
    }
}
