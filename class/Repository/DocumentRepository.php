<?php

namespace Atp\Repository;

use Atp\Entity\Document;
use Doctrine\ORM\EntityRepository;
use Doctrine\DBAL\Types\Type;

class DocumentRepository extends EntityRepository
{

    /**
     * Get document by id.
     *
     * @param int $documentId
     * @return Document
     */
    public function getById($documentId)
    {
        $query = $this->getEntityManager()->createQuery('
            SELECT
                document
            FROM
                Atp\Entity\Document document
                    LEFT JOIN document.parent parentDocument
            WHERE
                document.id = :documentId')
            ->setParameter('documentId', $documentId, Type::INTEGER);

        return $query->getSingleResult();
    }

    /**
     * Remove document by id.
     * 
     * @param int $documentId
     */
    public function removeById($documentId)
    {
        $query = $this->getEntityManager()->createQuery('
            DELETE
            FROM
                Atp\Entity\Document document
            WHERE
                document.id = :documentId')
            ->setParameter('documentId', $documentId, Type::INTEGER);

        $query->execute();
    }
}
