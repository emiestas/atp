<?php

namespace Atp\Repository;

use Atp\Entity\FieldConfigurationType;
use Doctrine\ORM\EntityRepository;

class FieldConfigurationTypeRepository extends EntityRepository
{

    /**
     * Get all field configuration types.
     * 
     * @return FieldConfigurationType[]
     */
    public function getTypes()
    {
        $queryBuilder = $this->createQueryBuilder('types')->select('types');
        $query = $queryBuilder->getQuery();
//        $query->useQueryCache(true);
//        $query->useResultCache(true);

        return $query->getResult();
    }
}
