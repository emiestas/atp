<?php

namespace Atp\Repository;

use Atp\Entity\MessageReceiver;
use Doctrine\ORM\EntityRepository;

class MessageReceiverRepository extends EntityRepository
{
    /**
     * Search for worker messages.
     *
     * @param array $filter Key => value pairs.
     * @param int $filter['messageReceiver.id']
     * @param int $filter['messageReceiver.workerId']
     * @param bool $filter['messageReceiver.isRead']
     * @param int $filter[receivedMessage.id]
     * @param int $filter[messageRecordDocument.id]
     *
     * @return MessageReceiver[]
     */
    public function search(array $filter = array())
    {
        return $this->getBuilder($filter)
                ->getQuery()
                ->getResult();
    }

    /**
     * Search for worker messages.
     *
     * @param array $filter Key => value pairs.
     * @param int $filter['messageReceiver.id']
     * @param int $filter['messageReceiver.workerId']
     * @param bool $filter['messageReceiver.isRead']
     * @param int $filter[receivedMessage.id]
     * @param int $filter[messageRecordDocument.id]
     *
     * @return int
     */
    public function count(array $filter = array())
    {
        return $this->getBuilder($filter)
                ->select('COUNT(messageReceiver)')
                ->getQuery()
                ->getSingleScalarResult();
    }

    /**
     * Get query builder.
     *
     * @param array $filter key => value pairs.
     * @param int $filter['messageReceiver.id']
     * @param int $filter['messageReceiver.receiverId']
     * @param bool $filter['messageReceiver.isRead']
     * @param int $filter[receivedMessage.id]
     * @param int $filter[messageRecordDocument.id]
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getBuilder(array $filter = array())
    {
        $filterBy = array(
            'messageReceiver.id',
            'messageReceiver.receiverId',
            'messageReceiver.isRead',
            'receivedMessage.id',
            'messageRecordDocument.id',
        );

        $queryBuilder = $this->createQueryBuilder('messageReceiver')
            ->select(array('messageReceiver'))
            ->innerJoin('messageReceiver.message', 'receivedMessage')
            ->leftJoin('receivedMessage.record', 'messageRecord')
            ->leftJoin('messageRecord.document', 'messageRecordDocument')
        ;

        if (!empty($filter)) {
            $c = 0;
            $parameters = array();

            foreach ($filter as $key => $value) {
                if (!in_array($key, $filterBy, true) || empty($value)) {
                    continue;
                }

                $queryBuilder->andWhere($key . ' = ?' . $c++);
                $parameters[] = $value;
            }

            $queryBuilder->setParameters($parameters);
        }

        return $queryBuilder;
    }

    /**
     * @param int $receiverId
     * @return int[] documentId => countNr
     */
    public function getUnreadMessagesCountList($receiverId)
    {
        $query = $this->getBuilder([
                'messageReceiver.receiverId' => $receiverId
            ])
            ->select('messageRecordDocument.id, COUNT(messageReceiver)')
            ->groupBy('messageRecordDocument.id');

        $result = $query->getQuery()->getArrayResult();

        $return = null;
        foreach ($result as $item) {
            $return[$item['document']] = $item[1];
        }

        return $return;
    }
}
