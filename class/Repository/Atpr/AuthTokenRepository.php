<?php

namespace Atp\Repository\Atpr;

use Atp\Entity\Atpr\AuthToken,
    Doctrine\ORM\EntityRepository;

class AuthTokenRepository extends EntityRepository
{

    /**
     * Get token by ATPEIR user name and document ROIK.
     * 
     * @param string $atprVartotojoVardas
     * @param string $atprRoik
     * @return null|AuthToken
     */
    public function getToken($atprVartotojoVardas, $atprRoik)
    {
        $queryBulder = $this->createQueryBuilder('token')
            ->select('token')
            ->andWhere('token.atprVartotojoVardas = :username')
            ->andWhere('token.roik = :roik')
            ->setParameter('username', $atprVartotojoVardas)
            ->setParameter('roik', $atprRoik);

        return $queryBulder->getQuery()->getOneOrNullResult();
    }
}
