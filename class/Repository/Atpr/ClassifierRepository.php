<?php

namespace Atp\Repository\Atpr;

use Atp\Entity\Atpr\Classifier,
    Doctrine\ORM\EntityRepository,
    Doctrine\ORM\Query\Expr\Join;

class ClassifierRepository extends EntityRepository
{

    const ALIAS_CLASSIFIER = 'classifier';

    const ALIAS_CLASSIFIER_TYPE = 'classifierType';

    const ALIAS_PARENT_CLASSIFIER = 'parent';

    const ALIAS_ATP_CLASSIFICATOR = 'classificator';

    /**
     * Query builder for classifier.
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function classifierQueryBuilder()
    {
        return $this->createQueryBuilder(self::ALIAS_CLASSIFIER)
                ->select(self::ALIAS_CLASSIFIER);
    }

    /**
     * Query builder for getting classifier with parent classfier.
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function classifierWithParentQueryBuilder()
    {
        return $this->classifierQueryBuilder()
                ->select([
                    self::ALIAS_CLASSIFIER,
                    self::ALIAS_PARENT_CLASSIFIER
                ])
                ->leftJoin(
                    self::ALIAS_CLASSIFIER . '.parentExternal',
                    self::ALIAS_PARENT_CLASSIFIER
                );
    }

    /**
     * Get classifier by ATP classificator.
     *
     * @param int $classificatorId
     * @return null|Classifier
     */
    public function getByClassificator($classificatorId)
    {
        return $this->classifierWithParentQueryBuilder()
                ->where(self::ALIAS_CLASSIFIER . '.classificator = :classificator')
                ->setParameter('classificator', $classificatorId)
                ->getQuery()->getOneOrNullResult();
    }

    /**
     * Get classifier by type and ATP classificator.
     *
     * @param string $atpeirType
     * @param string $classificatorLabel
     * @return null|Classifier
     */
    public function getByTypeAndClassificatorLabel($atpeirType, $classificatorLabel)
    {
        return $this->classifierQueryBuilder()
            ->innerJoin(
                self::ALIAS_CLASSIFIER . '.type',
                self::ALIAS_CLASSIFIER_TYPE,
                Join::WITH,
                self::ALIAS_CLASSIFIER_TYPE . '.name = :typeName'
            )
            ->innerJoin(
                self::ALIAS_CLASSIFIER . '.classificator',
                self::ALIAS_ATP_CLASSIFICATOR,
                Join::WITH,
                self::ALIAS_ATP_CLASSIFICATOR . '.label = :classificatorLabel'
            )
            ->setParameter('typeName', $atpeirType)
            ->setParameter('classificatorLabel', $classificatorLabel)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     * Get ATPEIR classifier by ATP clause.
     *
     * @param int $clauseId
     * @return null|Classifier
     */
    public function getByClause($clauseId)
    {
        return $this->classifierWithParentQueryBuilder()
                ->where(self::ALIAS_CLASSIFIER . '.clause = :clause')
                ->setParameter('clause', $clauseId)
                ->getQuery()->getOneOrNullResult();
    }

    /**
     * @param type $id
     * @param type $lockMode
     * @param type $lockVersion
     * @return null|Classifier
     */
    public function find($id, $lockMode = null, $lockVersion = null)
    {
        return parent::find($id, $lockMode, $lockVersion);
    }

    /**
     * Get ATP classificator by ATPR classifier.
     *
     * @param \Atpr\Kis\ClassifierEntryType $classifier
     * @return \Atp\Entity\Classificator
     */
    public function getClassificator(\Atpr\Kis\ClassifierEntryType $classifierEntry)
    {
        /* @var $classifier Classifier */
        $classifier = $this->classifierQueryBuilder()
                ->select([
                    self::ALIAS_CLASSIFIER,
                    self::ALIAS_ATP_CLASSIFICATOR
                ])
                ->innerJoin(
                    self::ALIAS_CLASSIFIER . '.type',
                    self::ALIAS_CLASSIFIER_TYPE
                )
                ->innerJoin(
                    self::ALIAS_CLASSIFIER . '.classificator',
                    self::ALIAS_ATP_CLASSIFICATOR
                )
                ->andWhere(self::ALIAS_CLASSIFIER_TYPE . '.name = :typeName')
                ->andWhere(self::ALIAS_CLASSIFIER . '.code = :code')
                ->setParameter('typeName', $classifierEntry->getClassifierType())
                ->setParameter('code', $classifierEntry->getClassifierCode())
                ->getQuery()->getSingleResult();

        return $classifier->getClassificator();
    }
}
