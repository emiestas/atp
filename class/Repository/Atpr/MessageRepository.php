<?php

namespace Atp\Repository\Atpr;

use \Atp\Entity\Atpr\Message as AtprMessageEntity;
use Doctrine\ORM\EntityRepository;

class MessageRepository extends EntityRepository
{
    /**
     * Get date of newest message
     * @return \DateTime|null
     */
    public function getNewestMessageDate()
    {
        $lastMessage = $this->createQueryBuilder('message')
            ->select('message')
            ->orderBy('message.busenosPasikeitimoData', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if ($lastMessage) {
            return $lastMessage->getBusenosPasikeitimoData();
        }

        return null;
    }

    /**
     * Get messages which have been marked as read in ATP, but not marked in ATPR.
     * @return \Atp\Entity\Atpr\Message[]
     */
    public function getReadUnmarkedMessages()
    {
        $unmarkedMessages = $this->createQueryBuilder('atprMessage')
            ->select('atprMessage')
            ->innerJoin('atprMessage.atpMessage', 'atpMessage')
            ->innerJoin('atpMessage.receivers', 'messageReceiver')
            ->andWhere('atprMessage.isRead = 0')
            ->andWhere('messageReceiver.isRead = 1')
            ->distinct()
            ->getQuery()
            ->getResult();

        return $unmarkedMessages;
    }
}
