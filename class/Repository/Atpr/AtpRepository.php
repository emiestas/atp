<?php

namespace Atp\Repository\Atpr;

use Atp\Entity\Atpr\Atp;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;

class AtpRepository extends EntityRepository
{

    /**
     * Query builder for getting ATPEIR record by roik.
     *
     * @param string $roik ATPEIR roik.
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function byRoikQueryBuilder($roik)
    {
        return $this->createQueryBuilder('ATPR')
                ->select('ATPR, ATP')
                ->innerJoin('ATPR.record', 'ATP')
                ->andwhere('ATPR.roik = :roik')
                ->setParameter('roik', $roik);
    }

    /**
     * Get ATP document record id for ATPEIR roik.
     * 
     * @param string $roik ATPEIR roik.
     * @return int ATP document record id.
     */
    public function getRecordId($roik)
    {
        return $this->byRoikQueryBuilder($roik)
                ->select('ATP.id')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);
//        return $this->createQueryBuilder('ATPR')
//                ->select('ATP.id')
//                ->innerJoin('ATPR.record', 'ATP')
//                ->andwhere('ATPR.roik = :roik')
//                ->setParameter('roik', $roik)
//                ->setMaxResults(1)
//                ->getQuery()
//                ->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);
    }

    /**
     * Get list of file ids which are related to ATPEIR record.
     *
     * @todo Move to AtpDocumentRepository ?
     * @param string $roik ATPEIR roik.
     * @param int $source [optional] Where file came from.
     *      Enumerator:
     *          \Atp\Entity\Atpr\AtpDocument::SOURCE_ATP (1);
     *          \Atp\Entity\Atpr\AtpDocument::SOURCE_ATPEIR (2).
     * @return int[] Array of file IDs.
     */
    public function getFilesIds($roik, $source = null)
    {
        $queryBuilder = $this->createQueryBuilder('ATPR')
            ->select('FILE.id')
            ->innerJoin('ATPR.documents', 'ATPR_FILE')
            ->leftJoin('ATPR_FILE.file', 'FILE')
            ->andWhere('ATPR.roik = :roik')
			->orderBy('FILE.id', 'DESC')
			->setParameter('roik', $roik);

        if ($source) {
            $queryBuilder
                ->andWhere('ATPR_FILE.source = :source')
                ->setParameter('source', $source);
        }

        return array_column($queryBuilder->getQuery()->getArrayResult(), 'id');
    }

    /**
     * Query builder for getting ATPEIR record by ATP document record.
     *
     * @param int $recordId ATP document record id.
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function byRecordIdQueryBuilder($recordId)
    {
        return $this->createQueryBuilder('ATPR')
                ->select('ATPR, ATP')
                ->innerJoin('ATPR.record', 'ATP', Join::WITH, 'ATP.id = :recordId')
                ->setParameter('recordId', $recordId);
    }

    /**
     * Check whether ATPEIR document exists for ATP document record.
     * 
     * @param int $recordId ATP document record id.
     * @return boolean
     */
    public function isByRecordId($recordId)
    {
        return (bool) $this->byRecordIdQueryBuilder($recordId)
                ->select('COUNT(ATPR.roik)')
                ->getQuery()
                ->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);
//        $result = $this->createQueryBuilder('ATPR')
//            ->select('COUNT(ATPR.roik)')
//            ->innerJoin('ATPR.record', 'record', Join::WITH, 'record.id = :recordId')
//            ->setParameter('recordId', $recordId)
//            ->getQuery()
//            ->getOneOrNullResult(Query::HYDRATE_SINGLE_SCALAR);
//
//        return ($result ? true : false);
    }

    /**
     * Get ATPEIR document for ATP document record.
     * 
     * @param int $recordId ATP record id.
     * @return Atp
     */
    public function getByRecordId($recordId)
    {
        return $this->byRecordIdQueryBuilder($recordId)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
//        return $this->createQueryBuilder('ATPR')
//                ->select('ATPR, ATP')
//                ->innerJoin('ATPR.record', 'ATP')
//                ->andwhere('ATP.id = :recordId')
//                ->setParameter('recordId', $recordId)
//                ->setMaxResults(1)
//                ->getQuery()
//                ->getOneOrNullResult();
    }
}
