<?php

//
//namespace Atp\Repository\Atpr;
//
//use Atp\Entity\Atpr\MessageWorker;
//use Doctrine\ORM\EntityRepository;
//
//class MessageWorkerRepository extends EntityRepository
//{
//
//    /**
//     * Search for worker messages.
//     *
//     * @param array $filter Key => value pairs.
//     * @param int $filter['messageWorker.id']
//     * @param int $filter['messageWorker.workerId']
//     * @param bool $filter['messageWorker.isRead']
//     * @param int $filter[message.id]
//     * @param int $filter[record.document]
//     *
//     * @return MessageWorker[]
//     */
//    public function search(array $filter = array())
//    {
//        return $this->getBuilder($filter)
//                ->getQuery()
//                ->getResult();
//    }
//
//    /**
//     * Search for worker messages.
//     *
//     * @param array $filter Key => value pairs.
//     * @param int $filter['messageWorker.id']
//     * @param int $filter['messageWorker.workerId']
//     * @param bool $filter['messageWorker.isRead']
//     * @param int $filter[message.id]
//     * @param int $filter[record.document]
//     *
//     * @return int
//     */
//    public function count(array $filter = array()/* , $groupBy = null */)
//    {
//        return $this->getBuilder($filter/* , $groupBy */)
//                ->select('COUNT(messageWorker)')
//                ->getQuery()
//                ->getSingleScalarResult();
//    }
//
//    /**
//     * Get query builder.
//     *
//     * @param array $filter key => value pairs.
//     * @param int $filter['messageWorker.id']
//     * @param int $filter['messageWorker.workerId']
//     * @param bool $filter['messageWorker.isRead']
//     * @param int $filter[message.id]
//     * @param int $filter[record.document]
//     * @return \Doctrine\ORM\QueryBuilder
//     */
//    public function getBuilder(array $filter = array()/* , $groupBy = null */)
//    {
//        $filterBy = array(
//            'messageWorker.id',
//            'messageWorker.workerId',
//            'messageWorker.isRead',
//            'message.id',
//            'record.document',
//        );
//
//        $queryBuilder = $this->createQueryBuilder('messageWorker')
//            ->select(array('messageWorker', 'messageWorker'))
//            ->innerJoin('messageWorker.message', 'message')
//        ;
//
//        if (isset($filter['record.document'])) {
//            $queryBuilder->innerJoin('message.atp', 'atpr')
//                ->innerJoin('atpr.record', 'record');
//        }
//
////        if (empty($groupBy) === FALSE) {
////            $queryBuilder->groupBy($groupBy);
////        }
//
//        if (!empty($filter)) {
//            $c = 0;
//            $parameters = array();
//
//            foreach ($filter as $key => $value) {
//                if (!in_array($key, $filterBy, true) || empty($value)) {
//                    continue;
//                }
//
//                $queryBuilder->andWhere($key . ' = ?' . $c++);
//                $parameters[] = $value;
//            }
//
//            $queryBuilder->setParameters($parameters);
//        }
//
//        return $queryBuilder;
//    }
//
//    /**
//     * @param int $workerId
//     * @return int[] documentId => countNr
//     */
//    public function getUnreadMessagesCountList($workerId)
//    {
//        $query = $this->getBuilder([
//                'messageWorker.workerId' => $workerId,
//                'messageWorker.isRead' => false
//            ])
//            ->select('record.document, COUNT(messageWorker)')
//            ->innerJoin('message.atp', 'atpr')
//            ->innerJoin('atpr.record', 'record')
//            ->groupBy('record.document');
//
//        $result = $query->getQuery()->getArrayResult();
//
//        $return = null;
//        foreach ($result as $item) {
//            $return[$item['document']] = $item[1];
//        }
//
//        return $return;
//    }
//}
