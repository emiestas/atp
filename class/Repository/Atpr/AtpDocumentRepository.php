<?php

namespace Atp\Repository\Atpr;

use Doctrine\ORM\EntityRepository;

class AtpDocumentRepository extends EntityRepository
{

    /**
     * @param string $roik
     * @param string $fileName
     * @return bool
     */
    public function isDocument($roik, $fileName)
    {
		$queryBulder = $this->createQueryBuilder('atprDocument')
            ->select('COUNT(atprDocument)')
            ->innerJoin('atprDocument.file', 'atpFile')
            ->andWhere('atprDocument.roik = :roik')
            ->andWhere("atpFile.name LIKE :fileName")
            ->setParameter('roik', $roik)
            ->setParameter('fileName', $fileName.'%');
		$res = $queryBulder->getQuery()->getSingleScalarResult();
		if ($res > 0) {return true;} else {return false;}
    }
}
