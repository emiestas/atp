<?php

namespace Atp\Avilys;

use \Avilys\RDODocument;

/**
 * Wrapper for Avilys RDODocument service.
 */
class Service extends RDODocument
{

    public function __construct()
    {
        $config = require __DIR__ . '/../../config/config.php';
        $secutrity = $config['avilys']['security'];

        parent::__construct([], null, $secutrity['username'], $secutrity['password']);
    }
}
