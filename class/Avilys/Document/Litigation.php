<?php

namespace Atp\Avilys\Document;

use \Avilys\RDODocumentWS as WS;

class Litigation extends Document
{

    /**
     * Constructor for creating litigation document
     */
    public function __construct()
    {
        $parameters = new WS\DocumentFromTemplateParam(
            $docAttributes = new WS\docAttributes(),
            $extraAttributes = new WS\extraAttributes(),
            /*(new WS\TemplateParam())->setOid('afd2f29003a911e2a943ebb67d08761d'),*/
            null,
            $register = true
        );

        parent::__construct($parameters);

        $parameters->setProject(false);

        $docAttributes
            ->addEntry(
                (new WS\entry())
                ->setKey('isElectro')
                ->setValue(false)
            )
            ->addEntry(
                (new WS\entry())
                ->setKey('title')
                ->setValue('Bylos dokumentas iš ATP')
            )
//            ->addEntry(
//                (new WS\entry())
//                ->setKey('draftJournal')
//                ->setValue(new WS\JournalParam('9AF90CEF7C2811DAB8E5267812191436'))
//            )
//            ->addEntry(
//                (new WS\entry())
//                ->setKey('draftOfficeCase')
//                ->setValue((new WS\OfficeCaseParam())
//                    ->setOid('babe4541b78511e08802be19b37f12c6')
//                )
//            )
        ;
    }
}
