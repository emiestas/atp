<?php

namespace Atp\Avilys\Document;

use \Avilys\RDODocumentWS as WS;

class Protocol extends ElectronicDocument
{

    /**
     * Constructor for creating protocol document
     */
    public function __construct()
    {
        $parameters = new WS\DocumentFromTemplateParam(
            $docAttributes = new WS\docAttributes(),
            $extraAttributes = new WS\extraAttributes(),
            /*(new WS\TemplateParam())->setOid('afd2f29003a911e2a943ebb67d08761d'),*/
            null,
            $register = false
        );

        parent::__construct($parameters);

        $docAttributes
            ->addEntry(
                (new WS\entry())
                ->setKey('title')
                ->setValue('ADMINISTRACINIO TEISĖS PAŽEIDIMO PROTOKOLAS')
            )
//            ->addEntry(
//                (new WS\entry())
//                ->setKey('draftJournal')
//                ->setValue(new WS\JournalParam('D7BE0D74BCA611D995DA892017285631'))
//            )
//            ->addEntry(
//                (new WS\entry())
//                ->setKey('draftOfficeCase')
//                ->setValue((new WS\OfficeCaseParam())
//                    ->setOid('4ece3640b78711e08802be19b37f12c6')
//                )
//            )
        ;
    }
}
