<?php

namespace Atp\Avilys\Document;

use \Avilys\RDODocumentWS as WS;

class ProtocolAn extends Protocol
{

    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        parent::__construct();

        $parameters = $this->getDocumentFromTemplateParam();

//        $templateParam = $parameters->getTemplateParam();
//        $templateParam->setOid('5d6b7110acc611e1afbf966a8af35ddf');
        
        $docAttributes = $parameters->getDocAttributes();

        $titleEntry = $docAttributes->findEntry('title');
        if (!$titleEntry) {
            $titleEntry = (new WS\entry())
                ->setKey('title');
            $docAttributes->addEntry($titleEntry);
        }

        $titleEntry->setValue('ADMINISTRACINIO TEISĖS PAŽEIDIMO PROTOKOLAS SU ADMINISTRACINIU NURODYMU');
    }
}
