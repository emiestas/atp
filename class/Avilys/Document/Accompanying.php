<?php

namespace Atp\Avilys\Document;

use \Avilys\RDODocumentWS as WS;

class Accompanying extends ElectronicDocument
{

    /**
     * Constructor for creating accompanying document
     */
    public function __construct()
    {
        $parameters = new WS\DocumentFromTemplateParam(
            $docAttributes = new WS\docAttributes(),
            $extraAttributes = new WS\extraAttributes(),
            /*(new WS\TemplateParam())->setOid('c29150209db611e5b1f5a27b12a0020b'),*/
            null,
            $register = false
        );

        parent::__construct($parameters);

        $docAttributes
            ->addEntry(
                (new WS\entry())
                ->setKey('title')
                ->setValue('Elektroninis lydraščio dokumentas iš ATP')
            )
//            ->addEntry(
//                (new WS\entry())
//                ->setKey('draftJournal')
//                ->setValue(new WS\JournalParam('D7BE0D74BCA611D995DA892017285631'))
//            )
//            ->addEntry(
//                (new WS\entry())
//                ->setKey('draftOfficeCase')
//                ->setValue((new WS\OfficeCaseParam())
//                    ->setOid('4ece3640b78711e08802be19b37f12c6')
//                )
//            )
        ;
    }
}
