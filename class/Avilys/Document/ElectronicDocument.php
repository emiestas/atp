<?php

namespace Atp\Avilys\Document;

use \Avilys\RDODocumentWS\DocumentFromTemplateParam;
use \Avilys\RDODocumentWS as WS;

class ElectronicDocument extends Document
{

    /**
     * {@inheritdoc}
     */
    public function __construct(DocumentFromTemplateParam $parameters)
    {
        parent::__construct($parameters);

        $parameters->setRegister(false);
        $parameters->setProject(true);

        $docAttributes = $parameters->getDocAttributes();
        
        $isElectroEntry = $docAttributes->findEntry('isElectro');
        if (!$isElectroEntry) {
            $isElectroEntry = (new WS\entry())
                ->setKey('isElectro');
            $docAttributes->addEntry($isElectroEntry);
        }

        $isElectroEntry->setValue(true);
    }
}
