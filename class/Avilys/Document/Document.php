<?php

namespace Atp\Avilys\Document;

use \Avilys\RDODocumentWS\createDocumentFromTemplate;
use \Avilys\RDODocumentWS\DocumentFromTemplateParam;

class Document extends createDocumentFromTemplate
{

    /**
     * @param DocumentFromTemplateParam $parameters
     */
    public function __construct(DocumentFromTemplateParam $parameters)
    {
        $this->setDocumentFromTemplateParam($parameters);
    }
}
