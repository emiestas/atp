<?php

namespace Atp\Avilys;

use \Avilys\Contact as AvilysContact;

/**
 * Wrapper for Avilys Contact service.
 */
class Contact extends AvilysContact
{

    public function __construct()
    {
        $config = require __DIR__ . '/../../config/config.php';
        $secutrity = $config['avilys']['security'];

        parent::__construct([], null, $secutrity['username'], $secutrity['password']);
    }
}
