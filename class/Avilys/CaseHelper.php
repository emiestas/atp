<?php

namespace Atp\Avilys;

use \Avilys\RDODocumentWS as WS;

/**
 * Helper methods for creating Case document in Avilys.
 */
class CaseHelper
{
//    private $journal = '95873b70259811e19b1ac17bd7a6ad31';
    private $journal = '9AF90CEF7C2811DAB8E5267812191436';

//    private $case = '95873b70259811e19b1ac17bd7a6ad31';
    private $case = 'babe4541b78511e08802be19b37f12c6';

//    private $template = 'templ.RDOINC.PRASY';
    private $template = 'afd2f29003a911e2a943ebb67d08761d';

    /**
     * @param int $recordId
     * @param string $avilysOrgName
     * @return WS\createDocumentFromTemplate
     */
    public function getCreateParameters($recordId, $avilysOrgName = null)
    {
        $parameters = new WS\DocumentFromTemplateParam(
            $docAttributes = new WS\docAttributes(),
            $extraAttributes = new WS\extraAttributes(),
            (new WS\TemplateParam())->setOid($this->template),
            $register = true
        );
        $parameters->setProject(false);
        
        if($avilysOrgName) {
            $parameters->setTargetStaff(
                (new WS\OrgNodeParam())->setOrgName($avilysOrgName)
            );
        }
        $docAttributes
            ->addEntry(
                (new WS\entry())
                ->setKey('isElectro')
                ->setValue(false)
            )
            ->addEntry(
                (new WS\entry())
                ->setKey('title')
                ->setValue('Bylos dokumentas iš ATP')
            )
            ->addEntry(
                (new WS\entry())
                ->setKey('draftJournal')
                ->setValue(new WS\JournalParam($this->journal))
            )
            ->addEntry(
                (new WS\entry())
                ->setKey('draftOfficeCase')
                ->setValue((new WS\OfficeCaseParam())
                    ->setOid($this->case)
                )
            )
            ->addEntry(
                (new WS\entry())
                ->setKey('personCode')
                ->setValue($_SESSION['WP']['PEOPLE']['CODE'])
            )
            ->addEntry(
                (new WS\entry())
                ->setKey('personName')
                ->setValue($_SESSION['WP']['PEOPLE']['SHOWS'])
        );

        return (new WS\createDocumentFromTemplate())
                ->setDocumentFromTemplateParam($parameters);
    }
}
