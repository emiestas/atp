<?php

namespace Atp\Avilys;

class ConfigurationManager
{

    /**
     * @var \Atp\Core
     */
    private $core;

    /**
     * @param \Atp\Core $core
     */
    public function __construct(\Atp\Core $core)
    {
        $this->core = $core;
    }

    /**
     * @return \Atp\Repository\Avilys\ConfigurationRepository
     */
    public function getRepository()
    {
        $entityManager = $this->core->factory->Doctrine();
        return $entityManager->getRepository('Atp\Entity\Avilys\Configuration');
    }
}
