<?php

namespace Atp\Avilys;

class ConfigurationPage
{

    private $atp;

    /**
     * @var \Atp\Avilys\ConfigurationManager
     */
    private $manager;

    /**
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;
        $this->manager = new \Atp\Avilys\ConfigurationManager($atp);
    }

    public function handle()
    {
        if (filter_input(INPUT_GET, 'getAll') !== null) {
            header('Content-type: application/json');
            echo json_encode($this->getAll());
        } else if (filter_input(INPUT_GET, 'get') !== null) {
            header('Content-type: application/json');
            echo json_encode($this->get());
        } else if (filter_input(INPUT_GET, 'save') !== null) {
            header('Content-type: application/json');
            echo json_encode($this->save());
        } else if (filter_input(INPUT_GET, 'delete') !== null) {
            header('Content-type: application/json');
            echo json_encode($this->delete());
        } else {
            echo $this->render();
        }
        exit;
    }

    public function getAll()
    {
        return $result = [
            'data' => array_map(function(\Atp\Entity\Avilys\Configuration $config) {
                    return $this->serialize($config);
                }, $this->manager->getRepository()->findAll())
        ];
    }

    public function get()
    {
        $id = filter_input(INPUT_GET, 'id');

        return $this->serialize($this->manager->getRepository()->find($id));
    }

    public function delete()
    {
        $id = filter_input(INPUT_GET, 'id');

        $entityManager = $this->atp->factory->Doctrine();

        $config = $this->manager->getRepository()->find($id);

        $entityManager->remove($config);
        $entityManager->flush();

        return true;
    }

    public function save()
    {
        $id = filter_input(INPUT_POST, 'id');
        $request = filter_input_array(INPUT_POST);

        $entityManager = $this->atp->factory->Doctrine();
        if ($request['id']) {
            $config = $this->manager->getRepository()->find($request['id']);
        } else {
            $config = new \Atp\Entity\Avilys\Configuration();
        }
        /* @var $config \Atp\Entity\Avilys\Configuration */

        $document = $entityManager->getReference('\Atp\Entity\Document', $request['document']);
        $template = null;
        if (!empty($request['template'])) {
            $template = $entityManager->getReference('\Atp\Entity\Template', $request['template']);
        }
        $department = null;
        if (!empty($request['department'])) {
            $department = $entityManager->getReference('\Atp\Entity\Department', $request['department']);
        }
        $journalOid = $request['journalOid'];
        $caseOid = $request['caseOid'];
        $templateOid = $request['templateOid'];

        $config
            ->setAtpDocument($document)
            ->setAtpTemplate($template ? $template : null)
            ->setAtpDepartment($department ? $department : null)
            ->setAvilysJournalOid($journalOid)
            ->setAvilysCaseOid($caseOid)
            ->setAvilysTemplateOid($templateOid)
        ;

        $entityManager->persist($config);
        $entityManager->flush($config);

        return $this->serialize($config);
    }

    public function render()
    {
        $workerId = $this->atp->factory->User()->getLoggedPersonDuty()->id;
        $documentId = filter_input(INPUT_GET, 'documentId', FILTER_VALIDATE_INT);


        $tmpl = $this->atp->factory->Template();
        $tmpl_handler = 'avilys_configuration_list_file';
        $tmpl->set_file($tmpl_handler, 'avilys_configuration_list.tpl');

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->atp->atp_content($output);
    }

    private function serialize(\Atp\Entity\Avilys\Configuration $config)
    {
        return [
            'id' => $config->getId(),
            'document' => [
                'id' => $config->getAtpDocument()->getId(),
                'label' => $config->getAtpDocument()->getLabel()
            ],
            'template' => [
                'id' => $config->getAtpTemplate() ? $config->getAtpTemplate()->getId() : null,
                'label' => $config->getAtpTemplate() ? $config->getAtpTemplate()->getLabel() : null
            ],
            'department' => [
                'id' => $config->getAtpDepartment() ? $config->getAtpDepartment()->getId() : null,
                'label' => $config->getAtpDepartment() ? $config->getAtpDepartment()->getLabel() : null
            ],
            'journalOid' => $config->getAvilysJournalOid(),
            'caseOid' => $config->getAvilysCaseOid(),
            'templateOid' => $config->getAvilysTemplateOid(),
        ];
    }
}
