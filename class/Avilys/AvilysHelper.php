<?php

namespace Atp\Avilys;

use \Avilys\RDODocumentWS as WS;
use \Avilys\RDODocument;

class AvilysHelper
{
    /**
     * @var \Atp\Core
     */
    private $atp;

    /**
     * @var \Atp\Avilys\Service
     */
    private $service;

    /**
     * @var \Atp\Avilys\Contact
     */
    private $contact;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Atp\Record
     */
    private $recordHelper;

    /**
     * @var \Atp\AdminJournal
     */
    private $journalHelper;

    /**
     * @var \Atp\User
     */
    private $userHelper;

    /**
     *
     * @param \Atp\Core $atp
     */
    public function __construct(\Atp\Core $atp)
    {
        require_once __DIR__ . '/../../../avilys/autoload.php';

        $this->atp = $atp;
        $this->entityManager = $this->atp->factory->Doctrine();
        $this->recordHelper = $this->atp->factory->Record();
        $this->journalHelper = $this->atp->factory->AdminJournal();
        $this->userHelper = $this->atp->factory->User();
        $this->service = new \Atp\Avilys\Service();
        $this->contact = new \Atp\Avilys\Contact();
    }

    /**
     * Register case document.
     *
     * @param int $recordId
     * @throws \Atp\Exception\Exception
     * @throws \Atp\Exception\AvilysException
     */
    public function registerCase($recordId)
    {
        $record = $this->recordHelper->getEntity($recordId);

        if ($record->getDocument()->getId() !== \Atp\Document::DOCUMENT_PROTOCOL) {
            throw new \Atp\Exception\Exception('Bylą galima registruoti tik iš protokolo');
        }


        // Check if it is allowed to create Case document.
        $isValidPath = $this->recordHelper
            ->validateTransferPath(
                $record->getId(),
                \Atp\Document::DOCUMENT_CASE
            );
        if ($isValidPath === false) {
            throw new \Atp\Exception\Exception('Iš šios dokumento versijos jau buvo sukurtas sekantis dokumentas');
        }


        // Register document to Avilys
        try {
            $caseHelper = new \Atp\Avilys\CaseHelper();

            $document = $caseHelper->getCreateParameters(
                $record->getId(),
                $this->userHelper->getLoggedPersonDutyAvilysId()
            );

            $this->addReceiver($document, $record);

            $this->configureDocument(
                $document,
                $record->getDocument()->getId(),
                null,
                $this->atp->factory->User()->getLoggedPersonDuty()->department->getId()
            );

            $response = $this->service->createDocumentFromTemplate($document);

            if (empty($response->getDocumentInfo())) {
                throw new \Atp\Exception\AvilysException('Nėra dokumento informacijos');
            }
        } catch (\SoapFault $e) {
            throw new \Atp\Exception\AvilysException(
                $e->getMessage(), $e->getCode(), $e
            );
        }


        // Create next ATP Case document
        $recordManager = new \Atp\Record\Manage(
            new \Atp\Document\Record($record->getId(), $this->atp),
            $this->atp
        );
        $recordManager->transferTo(\Atp\Document::DOCUMENT_CASE);

        if (empty($recordManager->result['result'])) {
            $message = 'Nepavyko perduoti dokumento.';
            if (isset($recordManager->result['messages']['error'])) {
                $message .= join('. ', $recordManager->result['messages']['error']) . '.';
            }
            throw new \Atp\Exception\Exception($message);
        }
        $newRecord = $this->recordHelper
            ->getEntity($recordManager->result['saved_record_id']);


        $documentInfo = $response->getDocumentInfo();
        // Add Avilys document information to new document
        $this->recordHelper
            ->setRegistrationFields(
                $newRecord->getId(),
                $documentInfo->getRegistrationNo(),
                $documentInfo->getRegistrationDate()
        );

        // TODO: ATP_AVILYS_DOCUMENT dokumento įrašas gali turėti tik 1 avilio dokumentą. Protokole ir byloje vieta rezervuota el. dokumentui. Nuo egzistuojančių įrašų priklauso formos mygtukų matomumas. Bylos mygtuko matomumas nustatomas pagal tai ar egzistuoja Bylos  dokumentas.
//        // Add information to database
//        $this->entityManager = $this->factory->Doctrine();
//        $avilysDocument = new \Atp\Entity\Avilys\Document(
//            $documentInfo->getOid(),
//            $documentInfo->getRegistrationNo(),
//            $documentInfo->getRegistrationDate(),
//            $documentInfo->getDocumentCategory()
//        );
//        $avilysDocument->setRecord($newRecord);
//        $this->entityManager->persist($avilysDocument);
//        $this->entityManager->flush();


        // Add journal entry about action
        $this->journalHelper->addRecord(
            $this->journalHelper->getCreateRecord(
                'Avilio byla "' . $response->getDocumentInfo()->getRegistrationNo() . '"'
                . ' dokumento įrašui "' . $record->getNumber() . '".'
                . ' Naujas ATP bylos dokumento įrašas "' . $newRecord->getNumber() . '".'
            )
        );

        header('Location: ' . $recordManager->result['redirect']);
        exit;
    }

    /**
     * Create electronic document.
     *
     * @param int $recordId
     * @param int $templateId [optional]
     * @throws \Atp\Exception\AvilysException
     */
    public function registerElectronicDocument($recordId, $templateId = null)
    {
        $record = $this->recordHelper->getEntity($recordId);

        // Register document to Avilys
        try {
            $aDocHelper = new \Atp\Avilys\ElectroDocumentHelper();

            $document = $aDocHelper->getCreateParameters(
                $record->getId(),
                $this->userHelper->getLoggedPersonDutyAvilysId()
            );

            $this->configureDocument(
                $document,
                $record->getDocument()->getId(),
                $templateId,
                $this->atp->factory->User()->getLoggedPersonDuty()->department->getId()
            );

            $this->addReceiver($document, $record);

            // Add attachments
            $attachments = null;

            /* @var $repository \Atp\Repository\Atpr\AtpRepository */
            $repository = $this->entityManager->getRepository('\Atp\Entity\Atpr\Atp');
            $atpr = $repository->getByRecordId($record->getId());
            if ($atpr && $atpr->getRoik()) {
                $filesIds = $repository->getFilesIds(
                    $atpr->getRoik(),
                    \Atp\Entity\Atpr\AtpDocument::SOURCE_ATPEIR
                );
                $fileRepository = $this->entityManager->getRepository('\Atp\Entity\File');
				$fileNameNumber = 1;
                foreach ($filesIds as $fileId) {
                    /* @var $file \Atp\Entity\File */
                    $file = $fileRepository->find($fileId);

                    $attachments[] = new WS\AttachmentActionParam(
                        (empty($attachments) ? WS\AttachmentType::main : WS\AttachmentType::supp),
                        WS\AttachmentAction::add,
                        $fileNameNumber.'_'.$file->getName(),
                        $file->getContent(),
                        $file->getType(),
                        new WS\customFields()
                    );
					$fileNameNumber++;
                }
            } else if (empty($templateId) === false) {
                $generator = new \Atp\Document\Templates\Generator($this->atp);
                $generator->Make($record->getId(), $templateId);
                $file = $generator->GetFilePath();

                if ($file !== false) {
                    $attachments[] = new WS\AttachmentActionParam(
                        WS\AttachmentType::main,
                        WS\AttachmentAction::add,
                        $fileNameNumber.'_'.basename($file),
                        file_get_contents($file),
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        new WS\customFields()
                    );
					$fileNameNumber++;
                }
            }
            if ($attachments) {
                $document->getDocumentFromTemplateParam()
                    ->setBodyAttachment($attachments);
            }

            $response = $this->service->createDocumentFromTemplate($document);

            if (empty($response->getDocumentInfo())) {
                throw new \Atp\Exception\AvilysException('Nėra dokumento informacijos');
            }
        } catch (\SoapFault $e) {
            throw new \Atp\Exception\AvilysException(
                $e->getMessage(), $e->getCode(), $e
            );
        }

        $documentInfo = $response->getDocumentInfo();


        $avilysDocument = new \Atp\Entity\Avilys\Document(
            $documentInfo->getOid(),
            $documentInfo->getRegistrationNo(),
            $documentInfo->getRegistrationDate(),
            $documentInfo->getDocumentCategory()
        );
        $avilysDocument->setRecord($record);

        $this->entityManager->persist($avilysDocument);
        $this->entityManager->flush();


        // Add journal entry about action
        $this->journalHelper->addRecord(
            $this->journalHelper->getCreateRecord(
                'Sukurtas elektroninis Avilio dokumentas "' . $documentInfo->getOid() . '"'
                . ' dokumento įrašui "' . $record->getNumber() . '".'
            )
        );

        $statusManager = new \Atp\Document\Record\Status(['RECORD' => $record->getId()], $this->atp);
        $result = $statusManager->CheckSingleStatus(65);
        if (empty($result) === false) {
            $result = array_shift($result);
            $statusManager->SetRecordStatus($result['RECORD'], $result['STATUS']);
        }

        $this->atp->set_message('success',
            'Sukurtas elektroninis Avilio dokumentas "' . $documentInfo->getOid() . '"'
            . ' dokumento įrašui "' . $record->getNumber() . '".'
        );
    }

    /**
     * Create protocol document in avilys.
     *
     * @param \Atp\Entity\Record $record
     * @param int $templateId [optional]
     * @param boolean $isAn [optional] Default: false
     * @throws \Atp\Exception\AvilysException
     */
    public function createProtocol(\Atp\Entity\Record $record, $templateId = null, $isAn = false)
    {
        try {
            if ($isAn) {
                $document = new \Atp\Avilys\Document\ProtocolAn();
            } else {
                $document = new \Atp\Avilys\Document\Protocol();
            }

            $this->configureDocument(
                $document,
                $record->getDocument()->getId(),
                $templateId,
                $this->atp->factory->User()->getLoggedPersonDuty()->department->getId()
            );

            $parameters = $document->getDocumentFromTemplateParam();

            // Set "rengėjas"
            $parameters->setTargetStaff(
                (new WS\OrgNodeParam())
                    ->setOrgName($this->userHelper->getLoggedPersonDutyAvilysId())
            );


            $docAttributes = $parameters->getDocAttributes();

            // Set something
            $loggedUser = $this->userHelper->getLoggedUser();

            $personCodeEntry = $docAttributes->findEntry('personCode');
            if (!$personCodeEntry) {
                $personCodeEntry = (new WS\entry())
                    ->setKey('personCode');
                $docAttributes->addEntry($personCodeEntry);
            }
            $personCodeEntry->setValue($loggedUser->person->code);

            $personNameEntry = $docAttributes->findEntry('personName');
            if (!$personNameEntry) {
                $personNameEntry = (new WS\entry())
                    ->setKey('personName');
                $docAttributes->addEntry($personNameEntry);
            }
            $personNameEntry->setValue($loggedUser->person->getFullName());

            $this->addReceiver($document, $record);

            // Add attachments
            $attachments = null;

            /* @var $repository \Atp\Repository\Atpr\AtpRepository */
            $repository = $this->entityManager->getRepository('\Atp\Entity\Atpr\Atp');
            $atpr = $repository->getByRecordId($record->getId());
            if ($atpr && $atpr->getRoik()) {
                $filesIds = $repository->getFilesIds(
                    $atpr->getRoik(),
                    \Atp\Entity\Atpr\AtpDocument::SOURCE_ATPEIR
                );
                $fileRepository = $this->entityManager->getRepository('\Atp\Entity\File');
				$fileNameNumber = 1;
                foreach ($filesIds as $fileId) {
                    /* @var $file \Atp\Entity\File */
                    $file = $fileRepository->find($fileId);

                    $attachments[] = new WS\AttachmentActionParam(
                        (empty($attachments) ? WS\AttachmentType::main : WS\AttachmentType::supp),
                        WS\AttachmentAction::add,
                        $fileNameNumber.'_'.$file->getName(),
                        $file->getContent(),
                        $file->getType(),
                        new WS\customFields()
                    );
					$fileNameNumber++;
                }
            } else if (empty($templateId) === false) {
                $generator = new \Atp\Document\Templates\Generator($this->atp);
                $generator->Make($record->getId(), $templateId);
                $file = $generator->GetFilePath();

                if ($file !== false) {
                    $attachments[] = new WS\AttachmentActionParam(
                        WS\AttachmentType::main,
                        WS\AttachmentAction::add,
                        $fileNameNumber.'_'.basename($file),
                        file_get_contents($file),
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        new WS\customFields()
                    );
					$fileNameNumber++;
                }
            }
            if ($attachments) {
                $parameters->setBodyAttachment($attachments);
            }

            $response = $this->service->createDocumentFromTemplate($document);

            if (empty($response->getDocumentInfo())) {
                throw new \Atp\Exception\AvilysException('Nėra dokumento informacijos');
            }
        } catch (\SoapFault $e) {
            throw new \Atp\Exception\AvilysException(
                $e->getMessage(), $e->getCode(), $e
            );
        }

        $documentInfo = $response->getDocumentInfo();


        // Save information about created Avilys document
        $avilysDocument = new \Atp\Entity\Avilys\Document(
            $documentInfo->getOid(),
            $documentInfo->getRegistrationNo(),
            $documentInfo->getRegistrationDate(),
            $documentInfo->getDocumentCategory()
        );
        $avilysDocument->setRecord($record);

        $this->entityManager->persist($avilysDocument);
        $this->entityManager->flush();


        // Add journal entry about action
        $this->journalHelper->addRecord(
            $this->journalHelper->getCreateRecord(
                'Sukurtas elektroninis Avilio protokolo dokumentas "' . $documentInfo->getOid() . '"'
                . ' dokumento įrašui "' . $record->getNumber() . '".'
            )
        );


        // Check & update record status
        $statusManager = new \Atp\Document\Record\Status(['RECORD' => $record->getId()], $this->atp);
        $result = $statusManager->CheckSingleStatus(65);
        if (empty($result) === false) {
            $result = array_shift($result);
            $statusManager->SetRecordStatus($result['RECORD'], $result['STATUS']);
        }


        // Add action message
        $this->atp->set_message('success',
            'Sukurtas elektroninis Avilio protokolo dokumentas "' . $documentInfo->getOid() . '"'
            . ' dokumento įrašui "' . $record->getNumber() . '".'
        );
    }

    /**
     * Create accompanying document in avilys.
     *
     * @param \Atp\Entity\Record $record
     * @param int $templateId
     * @param boolean $isAn [optional] Default: false
     * @throws \Atp\Exception\AvilysException
     */
    public function createAccompanying(\Atp\Entity\Record $record, $templateId, $isAn = false)
    {
        if (empty($templateId)) {
            throw new \Exception('Missing template identificator');
        }

        try {
            if ($isAn) {
                $document = new \Atp\Avilys\Document\AccompanyingAn();
            } else {
                $document = new \Atp\Avilys\Document\Accompanying();
            }

            $this->configureDocument(
                $document,
                $record->getDocument()->getId(),
                $templateId,
                $this->atp->factory->User()->getLoggedPersonDuty()->department->getId()
            );

            $parameters = $document->getDocumentFromTemplateParam();


            // Set "rengėjas"
            $parameters->setTargetStaff(
                (new WS\OrgNodeParam())
                    ->setOrgName($this->userHelper->getLoggedPersonDutyAvilysId())
            );


            $docAttributes = $parameters->getDocAttributes();

            // Set something
            $loggedUser = $this->userHelper->getLoggedUser();

            $personCodeEntry = $docAttributes->findEntry('personCode');
            if (!$personCodeEntry) {
                $personCodeEntry = (new WS\entry())
                    ->setKey('personCode');
                $docAttributes->addEntry($personCodeEntry);
            }
            $personCodeEntry->setValue($loggedUser->person->code);

            $personNameEntry = $docAttributes->findEntry('personName');
            if (!$personNameEntry) {
                $personNameEntry = (new WS\entry())
                    ->setKey('personName');
                $docAttributes->addEntry($personNameEntry);
            }
            $personNameEntry->setValue($loggedUser->person->getFullName());

            $this->addReceiver($document, $record);

            // Add attachments
            $attachments = null;

            // Generated from template
            $generator = new \Atp\Document\Templates\Generator($this->atp);
            $generator->Make($record->getId(), $templateId);
            $file = $generator->GetFilePath();

            if (!$file || !is_file($file)) {
                throw new \Exception('Document generation from template failed');
            }
			$fileNumber = 1;
            $attachments[] = new WS\AttachmentActionParam(
                WS\AttachmentType::main,
                WS\AttachmentAction::add,
                $fileNumber.'_'.basename($file),
                file_get_contents($file),
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                new WS\customFields()
            );
			$fileNumber++;

            // Avilys ADOC from protocol
            $protocolRecord = $this->recordHelper->getPreviousDocumentFor($record->getId(), \Atp\Document::DOCUMENT_PROTOCOL);
            if (!$protocolRecord) {
                throw new \Exception('Could not find protocol document for adoc');
            }

            if (!$this->recordHelper->isAvilysRegisteredFor($protocolRecord->getId())) {
                throw new \Exception('Protocol document was not registered in avilys');
            }

            /* @var $repository \Atp\Repository\Avilys\DocumentRepository */
            $repository = $this->entityManager->getRepository('\Atp\Entity\Avilys\Document');
            $avilysDocument = $repository->getByRecordId($protocolRecord->getId());
            $file = $avilysDocument->getResultFile();

            if (!$file) {
                throw new \Exception('Protocol document does not have adoc from avilys');
            }

            $attachments[] = new WS\AttachmentActionParam(
                WS\AttachmentType::adoc,
                WS\AttachmentAction::add,
                $fileNumber.'_'.$file->getName(),
                $file->getContent(),
                $file->getType(),
                new WS\customFields()
            );
			$fileNumber++;

            if ($attachments) {
                $parameters->setBodyAttachment($attachments);
            }

            $response = $this->service->createDocumentFromTemplate($document);

            if (empty($response->getDocumentInfo())) {
                throw new \Atp\Exception\AvilysException('Nėra dokumento informacijos');
            }
        } catch (\SoapFault $e) {
            throw new \Atp\Exception\AvilysException(
                $e->getMessage(), $e->getCode(), $e
            );
        }

        $documentInfo = $response->getDocumentInfo();


        // Save information about created Avilys document
        $avilysDocument = new \Atp\Entity\Avilys\Document(
            $documentInfo->getOid(),
            $documentInfo->getRegistrationNo(),
            $documentInfo->getRegistrationDate(),
            $documentInfo->getDocumentCategory()
        );
        $avilysDocument->setRecord($record);

        $this->entityManager->persist($avilysDocument);
        $this->entityManager->flush();


        // Add journal entry about action
        $this->journalHelper->addRecord(
            $this->journalHelper->getCreateRecord(
                'Sukurtas elektroninis Avilio lydraščio dokumentas "' . $documentInfo->getOid() . '"'
                . ' dokumento įrašui "' . $record->getNumber() . '".'
            )
        );


        // Check & update record status
        $statusManager = new \Atp\Document\Record\Status(['RECORD' => $record->getId()], $this->atp);
        $result = $statusManager->CheckSingleStatus(65);
        if (empty($result) === false) {
            $result = array_shift($result);
            $statusManager->SetRecordStatus($result['RECORD'], $result['STATUS']);
        }


        // Add action message
        $this->atp->set_message('success',
            'Sukurtas elektroninis Avilio lydraščio dokumentas "' . $documentInfo->getOid() . '"'
            . ' dokumento įrašui "' . $record->getNumber() . '".'
        );
    }

    /**
     * Create summons document in avilys.
     *
     * @param \Atp\Entity\Record $record
     * @param int $templateId
     * @throws \Atp\Exception\AvilysException
     */
    public function createSummons(\Atp\Entity\Record $record, $templateId)
    {
        if (empty($templateId)) {
            throw new \Exception('Missing template identificator');
        }

        try {
            $document = new \Atp\Avilys\Document\Summons();

            $this->configureDocument(
                $document,
                $record->getDocument()->getId(),
                $templateId,
                $this->atp->factory->User()->getLoggedPersonDuty()->department->getId()
            );

            $parameters = $document->getDocumentFromTemplateParam();


            // Set "rengėjas"
            $parameters->setTargetStaff(
                (new WS\OrgNodeParam())
                    ->setOrgName($this->userHelper->getLoggedPersonDutyAvilysId())
            );


            $docAttributes = $parameters->getDocAttributes();

            // Set something
            $loggedUser = $this->userHelper->getLoggedUser();

            $personCodeEntry = $docAttributes->findEntry('personCode');
            if (!$personCodeEntry) {
                $personCodeEntry = (new WS\entry())
                    ->setKey('personCode');
                $docAttributes->addEntry($personCodeEntry);
            }
            $personCodeEntry->setValue($loggedUser->person->code);

            $personNameEntry = $docAttributes->findEntry('personName');
            if (!$personNameEntry) {
                $personNameEntry = (new WS\entry())
                    ->setKey('personName');
                $docAttributes->addEntry($personNameEntry);
            }
            $personNameEntry->setValue($loggedUser->person->getFullName());

            $this->addReceiver($document, $record);

            // Add attachments
            $attachments = null;

            // Generated from template
            $generator = new \Atp\Document\Templates\Generator($this->atp);
            $generator->Make($record->getId(), $templateId);
            $file = $generator->GetFilePath();

            if (!$file || !is_file($file)) {
                throw new \Exception('Document generation from template failed');
            }
			$fileNumber = 1;
            $attachments[] = new WS\AttachmentActionParam(
                WS\AttachmentType::main,
                WS\AttachmentAction::add,
                $fileNumber.'_'.basename($file),
                file_get_contents($file),
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                new WS\customFields()
            );
			$fileNumber++;

            if ($attachments) {
                $parameters->setBodyAttachment($attachments);
            }

            $response = $this->service->createDocumentFromTemplate($document);

            if (empty($response->getDocumentInfo())) {
                throw new \Atp\Exception\AvilysException('Nėra dokumento informacijos');
            }
        } catch (\SoapFault $e) {
            throw new \Atp\Exception\AvilysException(
                $e->getMessage(), $e->getCode(), $e
            );
        }

        $documentInfo = $response->getDocumentInfo();


        // Save information about created Avilys document
        $avilysDocument = new \Atp\Entity\Avilys\Document(
            $documentInfo->getOid(),
            $documentInfo->getRegistrationNo(),
            $documentInfo->getRegistrationDate(),
            $documentInfo->getDocumentCategory()
        );
        $avilysDocument->setRecord($record);

        $this->entityManager->persist($avilysDocument);
        $this->entityManager->flush();


        // Add journal entry about action
        $this->journalHelper->addRecord(
            $this->journalHelper->getCreateRecord(
                'Sukurtas elektroninis Avilio šaukimo dokumentas "' . $documentInfo->getOid() . '"'
                . ' dokumento įrašui "' . $record->getNumber() . '".'
            )
        );


        // Check & update record status
        $statusManager = new \Atp\Document\Record\Status(['RECORD' => $record->getId()], $this->atp);
        $result = $statusManager->CheckSingleStatus(65);
        if (empty($result) === false) {
            $result = array_shift($result);
            $statusManager->SetRecordStatus($result['RECORD'], $result['STATUS']);
        }


        // Add action message
        $this->atp->set_message('success',
            'Sukurtas elektroninis Avilio šaukimo dokumentas "' . $documentInfo->getOid() . '"'
            . ' dokumento įrašui "' . $record->getNumber() . '".'
        );
    }

    /**
     * Create report document in avilys.
     *
     * @param \Atp\Entity\Record $record
     * @param int $templateId
     * @param boolean $isAn [optional] Default: false
     * @throws \Atp\Exception\AvilysException
     */
    public function createReport(\Atp\Entity\Record $record, $templateId, $isAn = false)
    {
        if (empty($templateId)) {
            throw new \Exception('Missing template identificator');
        }

        try {
            if ($isAn) {
                $document = new \Atp\Avilys\Document\ReportAn();
            } else {
                $document = new \Atp\Avilys\Document\Report();
            }

            $this->configureDocument(
                $document,
                $record->getDocument()->getId(),
                $templateId,
                $this->atp->factory->User()->getLoggedPersonDuty()->department->getId()
            );

            $parameters = $document->getDocumentFromTemplateParam();


            // Set "rengėjas"
            $parameters->setTargetStaff(
                (new WS\OrgNodeParam())
                    ->setOrgName($this->userHelper->getLoggedPersonDutyAvilysId())
            );


            $docAttributes = $parameters->getDocAttributes();

            // Set something
            $loggedUser = $this->userHelper->getLoggedUser();

            $personCodeEntry = $docAttributes->findEntry('personCode');
            if (!$personCodeEntry) {
                $personCodeEntry = (new WS\entry())
                    ->setKey('personCode');
                $docAttributes->addEntry($personCodeEntry);
            }
            $personCodeEntry->setValue($loggedUser->person->code);

            $personNameEntry = $docAttributes->findEntry('personName');
            if (!$personNameEntry) {
                $personNameEntry = (new WS\entry())
                    ->setKey('personName');
                $docAttributes->addEntry($personNameEntry);
            }
            $personNameEntry->setValue($loggedUser->person->getFullName());

            $this->addReceiver($document, $record);

            // Add attachments
            $attachments = null;

            // Generated from template
            $generator = new \Atp\Document\Templates\Generator($this->atp);
            $generator->Make($record->getId(), $templateId);
            $file = $generator->GetFilePath();

            if (!$file || !is_file($file)) {
                throw new \Exception('Document generation from template failed');
            }
			$fileNumber = 1;
            $attachments[] = new WS\AttachmentActionParam(
                WS\AttachmentType::main,
                WS\AttachmentAction::add,
       			$fileNumber.'_'.basename($file),
                file_get_contents($file),
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                new WS\customFields()
            );
			$fileNumber++;

            if ($attachments) {
                $parameters->setBodyAttachment($attachments);
            }

            $response = $this->service->createDocumentFromTemplate($document);

            if (empty($response->getDocumentInfo())) {
                throw new \Atp\Exception\AvilysException('Nėra dokumento informacijos');
            }
        } catch (\SoapFault $e) {
            throw new \Atp\Exception\AvilysException(
                $e->getMessage(), $e->getCode(), $e
            );
        }

        $documentInfo = $response->getDocumentInfo();


        // Save information about created Avilys document
        $avilysDocument = new \Atp\Entity\Avilys\Document(
            $documentInfo->getOid(),
            $documentInfo->getRegistrationNo(),
            $documentInfo->getRegistrationDate(),
            $documentInfo->getDocumentCategory()
        );
        $avilysDocument->setRecord($record);

        $this->entityManager->persist($avilysDocument);
        $this->entityManager->flush();


        // Add journal entry about action
        $this->journalHelper->addRecord(
            $this->journalHelper->getCreateRecord(
                'Sukurtas elektroninis Avilio pranešimo dokumentas "' . $documentInfo->getOid() . '"'
                . ' dokumento įrašui "' . $record->getNumber() . '".'
            )
        );


        // Check & update record status
        $statusManager = new \Atp\Document\Record\Status(['RECORD' => $record->getId()], $this->atp);
        $result = $statusManager->CheckSingleStatus(65);
        if (empty($result) === false) {
            $result = array_shift($result);
            $statusManager->SetRecordStatus($result['RECORD'], $result['STATUS']);
        }


        // Add action message
        $this->atp->set_message('success',
            'Sukurtas elektroninis Avilio pranešimo dokumentas "' . $documentInfo->getOid() . '"'
            . ' dokumento įrašui "' . $record->getNumber() . '".'
        );
    }

    /**
     * Configures provided avilys document creation from template object.
     *
     * @param \Avilys\RDODocumentWS\createDocumentFromTemplate $document
     * @param int $documentId
     * @param int $templateId [optional]
     * @param int $departmentId [optional]
     * @throws \Exception
     */
    private function configureDocument(\Avilys\RDODocumentWS\createDocumentFromTemplate $document, $documentId, $templateId = null, $departmentId = null)
    {
        $configurationManager = new ConfigurationManager($this->atp);

        $config = $configurationManager->getRepository()
            ->findOneConfiguration($documentId, $templateId, $departmentId);

        if (!$config) {
            throw new \Exception('Not found avilys configuration');
        }

        $documentFromTemplateParam = $document->getDocumentFromTemplateParam();
        $docAttributes = $documentFromTemplateParam->getDocAttributes();

        if ($config->getAvilysTemplateOid()) {
            $templateParam = $documentFromTemplateParam->getTemplateParam();
            if (!$templateParam) {
                $templateParam = new WS\TemplateParam();
                $documentFromTemplateParam->setTemplateParam($templateParam);
            }
            $templateParam->setOid($config->getAvilysTemplateOid());
        }
        if ($config->getAvilysJournalOid()) {
            $draftJournal = $docAttributes->findEntry('draftJournal');
            if (!$draftJournal) {
                $draftJournal = (new WS\entry())
                    ->setKey('draftJournal');
                $docAttributes->addEntry($draftJournal);
            }

            $draftJournal->setValue(
                new WS\JournalParam($config->getAvilysJournalOid())
            );
        }
        if ($config->getAvilysCaseOid()) {
            $draftOfficeCase = $docAttributes->findEntry('draftOfficeCase');
            if (!$draftOfficeCase) {
                $draftOfficeCase = (new WS\entry())
                    ->setKey('draftOfficeCase');
                $docAttributes->addEntry($draftOfficeCase);
            }

            $draftOfficeCase->setValue(
                (new WS\OfficeCaseParam())->setOid($config->getAvilysCaseOid())
            );
        }
    }

    /**
     * Add receiver attribute to avilys document creation from template object.
     *
     * @param \Avilys\RDODocumentWS\createDocumentFromTemplate $document
     * @param \Atp\Entity\Record $record
     * @throws \Exception
     */
    private function addReceiver(\Avilys\RDODocumentWS\createDocumentFromTemplate $document, \Atp\Entity\Record $record)
    {
        $documentParam = $document->getDocumentFromTemplateParam();
        if(empty($documentParam)) {
            throw new \Exception('Document does not contain any parameters');
        }
        $docAttributes = $documentParam->getDocAttributes();
        if(empty($documentParam)) {
            throw new \Exception('Document parameters does not contain any attributes');
        }


        $fieldsConfigurations = $this->atp->factory->Document()
            ->getOptions($record->getDocument()->getId(), true);

		$address = false;

		
		if (isset($fieldsConfigurations->person_code)) {
            $personCode = null;
            foreach ($fieldsConfigurations->person_code as $fieldId) {
                $value = $this->atp->factory->Record()->getFieldValue(
                    $record->getId(),
                    $fieldId
                );

                if (empty($value) === false) {
                    $personCode = $value;
                    break;
                }
            }

			$propertiesArray = array();
            if ($personCode) {
                $receiverEntry = $docAttributes->findEntry('receivers');
                if (!$receiverEntry) {
                    $receiverEntry = (new WS\entry())
                        ->setKey('receivers');
                    $docAttributes->addEntry($receiverEntry);
                }

                $contactRef = $this->contact->findContactRefByCode($personCode);
//				if (!$contactRef) {
                    $isJuridical = preg_match('/^[0-9]{7,9}$/', trim($personCode));

                    $parameters = new \Avilys\ContactWS\createContact();
                    $propertiesArray[0] = new \Avilys\ContactWS\entry('code', (string)$personCode);
                    $documentOptions = $this->atp->factory->Document()
                        ->getOptions($record->getDocument()->getId(), true);

                    if ($isJuridical) {
						$addressId = $fieldsConfigurations->company_address;
						if (isset($addressId)) {
							if (is_array($addressId)) {
								foreach ($addressId as $key => $val) {
									if (empty($address) == TRUE) {
										$address = $this->atp->factory->Record()->getFieldValue(
						                    $record->getId(),
						                    $addressId[$key]
						                );
									}
								}
							} else {
								$address = $this->atp->factory->Record()->getFieldValue(
				                    $record->getId(),
				                    $addressId
				                );
							}
						}
						$propertiesArray[1] = new \Avilys\ContactWS\entry('address', (string)$address);
						if ($documentOptions->company_name) {
                            foreach ($documentOptions->company_name as $fieldId) {
                                $value = $this->atp->factory->Record()->getFieldValue($record->getId(), $fieldId);
                                if (empty($value) === false) {
                                    $officialName = $value;
                                    break;
                                }
                            }
                        }

                        $contactType = \Avilys\ContactWS\ContactEnumType::CONTACT_UNIT;
                    } else {
						$addressId = NULL;
						$fullName = [];
						$addressId = $fieldsConfigurations->person_declared_address;
						if (isset($addressId)) {
							if (is_array($addressId)) {
								foreach ($addressId as $key => $val) {
									if (empty($address) == TRUE) {
										$address = $this->atp->factory->Record()->getFieldValue(
						                    $record->getId(),
						                    $addressId[$key]
						                );
									}
								}
							} else {
								$address = $this->atp->factory->Record()->getFieldValue(
				                    $record->getId(),
				                    $addressId
				                );
							}
						}
	                    $propertiesArray[1] = new \Avilys\ContactWS\entry('address', (string)$address);
                        if ($documentOptions->person_name) {
                            foreach ($documentOptions->person_name as $fieldId) {
                                $value = $this->atp->factory->Record()->getFieldValue($record->getId(), $fieldId);
                                if (empty($value) === false) {
                                    $fullName[] = $value;
                                    break;
                                }
                            }
                        }
                        if ($documentOptions->person_surname) {
                            foreach ($documentOptions->person_surname as $fieldId) {
                                $value = $this->atp->factory->Record()->getFieldValue($record->getId(), $fieldId);
                                if (empty($value) === false) {
                                    $fullName[] = $value;
                                    break;
                                }
                            }
                        }

                        $officialName = join(' ', $fullName);
                        $contactType = \Avilys\ContactWS\ContactEnumType::CONTACT_PERSON;
                    }

				if (!$contactRef) {
					$param = new \Avilys\ContactWS\CreateContactParam(
                        empty($officialName) ? $personCode : $officialName,
                        $contactType,
                        new \Avilys\ContactWS\properties($propertiesArray)
                    );
                    $parameters->setCreateContactParam($param);
					$result = $this->contact->createContact($parameters);
                    $contactResult = $result->getCreateContactResult();
                    if ($contactResult) {
                        $contact = $contactResult->getContact();
                        if ($contact) {
                            $orgNameOid = $contact->getOrgName();
                        }
                    }
              } else {
                    $orgNameOid = $contactRef->getOrgName();
                }

				$propertiesArray = array();
				$propertiesArray[0] = new \Avilys\ContactWS\entry('address', (string)$address);
				$parameters = false;
                $parameters = new \Avilys\ContactWS\modifyContact();
				$param = new \Avilys\ContactWS\ModifyContactParam(
                         $orgNameOid,
                         new \Avilys\ContactWS\properties($propertiesArray)
                );
				$parameters->setModifyContactParam($param);
				$result = $this->contact->modifyContact($parameters);
                $contactResult = $result->getModifyContactResult();
                $receiverEntry->setValue(
                    new WS\OrgNodeListParam([
                    (new WS\OrgNodeParam())->setOrgName($orgNameOid)
                    ])
                );
            }
        }
    }
}
