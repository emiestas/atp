<?php

namespace Atp\Avilys;

use \Avilys\RDODocumentWS as WS;

/**
 * Helper methods for creating electronic document in Avilys.
 */
class ElectroDocumentHelper
{
    private $journal = 'D7BE0D74BCA611D995DA892017285631';

    private $case = '4ece3640b78711e08802be19b37f12c6';

    private $template = 'c29150209db611e5b1f5a27b12a0020b';

    /**
     *
     * @param int $recordId
     * @param string $avilysOrgName
     * @return WS\createDocumentFromTemplate
     */
    public function getCreateParameters($recordId, $avilysOrgName = null)
    {
        $parameters = new WS\DocumentFromTemplateParam(
            $docAttributes = new WS\docAttributes(),
            $extraAttributes = new WS\extraAttributes(),
            (new WS\TemplateParam())->setOid($this->template),
            $register = false
        );
        $parameters->setProject(true);

        if($avilysOrgName) {
            $parameters->setTargetStaff(
                (new WS\OrgNodeParam())->setOrgName($avilysOrgName)
            );
        }
        $docAttributes
            ->addEntry(
                (new WS\entry())
                ->setKey('isElectro')
                ->setValue(true)
            )
            ->addEntry(
                (new WS\entry())
                ->setKey('title')
                ->setValue('Elektroninis dokumentas iš ATP')
            )
            ->addEntry(
                (new WS\entry())
                ->setKey('draftJournal')
                ->setValue(new WS\JournalParam($this->journal))
            )
            ->addEntry(
                (new WS\entry())
                ->setKey('draftOfficeCase')
                ->setValue((new WS\OfficeCaseParam())
                    ->setOid($this->case)
                )
            )
            ->addEntry(
                (new WS\entry())
                ->setKey('personCode')
                ->setValue($_SESSION['WP']['PEOPLE']['CODE'])
            )
            ->addEntry(
                (new WS\entry())
                ->setKey('personName')
                ->setValue($_SESSION['WP']['PEOPLE']['SHOWS'])
            )
//            ->addEntry(
//                (new WS\entry())
//                ->setKey('receivers')
//                ->setValue(new WS\OrgNodeListParam([
//                    (new WS\OrgNodeParam())
//                    ->setOrgName($avilysOrgName)
//                ]))
//            )
        ;
        return (new WS\createDocumentFromTemplate())
                ->setDocumentFromTemplateParam($parameters);
    }
}
