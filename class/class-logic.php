<?php

namespace Atp;

class Logic
{
    /**
     * @var \Atp\Core
     */
    public $core;

    //public $_r_new = FALSE;
    //public $_r_edit = FALSE;
    public $_r_path = FALSE;

    public $_r_table_id = 0;

    public $_r_table_from_id = 0;

    public $_r_table_to_id = 0;

//    public $_r_record_id = 0;

    // TODO: pagal nutylėjimą statusas "nutrauktas" ?
    public $_r_record_status = \Atp\Record::STATUS_DISCONTINUED;

//    public $_r_table_path_id = 0;

    public $_r_values = array();

    // TODO: pašalinti, kai bus sutvarkytas \Atp\Record\Manage
    public $create_record_redirect_url;

    //public $_t_values = array();

    /**
     *
     * @param \Atp\Core $core
     */
    function __construct(\Atp\Core $core)
    {
        $this->core = $core;
    }

    /**
     * Klasifikatoriaus / straipsnio laukai
     * @param string $type Įrašo tipas
     * @param array $arr Duomenys. $arr['ID'] Įrašo ID
     * @return boolean|array
     */
    function extra_fields_info($type, $arr)
    {
        if (in_array($type, array_keys($this->core->_prefix->item), true) === FALSE) {
            return false;
        }

        return $this->get_extra_fields($arr['ID'], $type);
    }

    /**
     * Išsaugo papildomus laukus prie klasifikatorių ir straipsnių
     * @param string $type Tipas: classificator / clause
     * @param array $arr Duomenys
     * @return type
     */
    function extra_fields_save($type, $arr)
    {
        // Suformuoja duomenis
        $arr['item'] = (int) $arr['item'];
        $arr['itype'] = strtoupper($type);
        $default_arr = array(
            'col_order' => array(),
            'col_default_field' => array(),
            'col_default_field_ws' => array(),
            'ws_relation' => array(),
            'col_label' => array(),
            'col_default' => array(),
            'col_default_id' => array(),
            'col_configuration' => array(),
            'col_sort' => array(),
            'col_text' => array(),
            'col_type' => array(),
            'col_render' => array(),
            'col_source' => array(),
            'col_ws' => array(),
//            'col_value' => array(),
//            'col_value_type' => array(),
//            'col_source_other' => array(),
//            'col_value_other' => array(),
//            'col_fill' => array(),
            'col_in_list' => array(),
            'col_relation' => array(),
            'col_mandatory' => array(),
            'col_visible' => array(),
            'col_competence' => array(),
            'col_file_cnt' => array(),
            'col_file_size_from' => array(),
            'col_file_size_to' => array()
        );
        $arr['col_configuration'] = array_filter(array_map(
                function ($value) {
                return array_filter($value);
            }, filter_input(INPUT_POST, 'col_configuration', FILTER_VALIDATE_INT, FILTER_REQUIRE_ARRAY)
        ));
        $arr['col_default_field'] = array_filter($arr['col_default_field']);
        $arr['col_default_field_ws'] = array_filter($arr['col_default_field_ws']);
        $arr = array_merge($default_arr, (array) $arr);

        /* @var $removedFields int[] */
        $removedFields = [];
        /* @var $fieldOrderMap int[] */// Field order to id map.
        $fieldOrderMap = [];

        $prefix = null;
        if (array_key_exists($arr['itype'], $this->core->_prefix->item) === TRUE) {
            $prefix = $this->core->_prefix->item[$arr['itype']];
        } else {
            trigger_error('Wrong item type', E_USER_WARNING);
            return array('type' => 'error', 'msg' => 'Įvyko klaida');
        }

        $ws_relations = array();
        $structure_values = array();

        foreach ($arr['col_label'] as $ord_id => $cols) {
            $col_key = key($cols);

            if ($arr['col_sort'][$ord_id] == 9) {
                $arr['col_type'][$ord_id] = 8;
            }
            if ($arr['col_sort'][$ord_id] == 11) {
                $arr['col_type'][$ord_id] = 7;
            }

            if ($arr['col_type'][$ord_id] != 18) {
                $arr['col_render'][$ord_id] = 0;
            }

            $values['ITEM_ID'] = $arr['item'];
            $values['ITYPE'] = $arr['itype'];
            $values['LABEL'] = $cols[$col_key];
            $values['DEFAULT'] = (!empty($arr['col_default'][$ord_id])) ? $arr['col_default'][$ord_id] : '';
            $values['DEFAULT_ID'] = (!empty($arr['col_default_id'][$ord_id])) ? $arr['col_default_id'][$ord_id] : '';
            $values['NAME'] = $prefix . $this->core->_prefix->column . $arr['item'] . '_' . $ord_id;
            $values['SORT'] = (!empty($arr['col_sort'][$ord_id])) ? $arr['col_sort'][$ord_id] : 0;
            $values['TYPE'] = (!empty($arr['col_type'][$ord_id])) ? $arr['col_type'][$ord_id] : 0;
            $values['RENDER'] = (!empty($arr['col_render'][$ord_id])) ? $arr['col_render'][$ord_id] : 0;
            $values['SOURCE'] = (!empty($arr['col_source'][$ord_id])) ? $arr['col_source'][$ord_id] : 0;
            $values['WS'] = (!empty($arr['col_ws'][$ord_id])) ? $arr['col_ws'][$ord_id] : 0;
//            $values['VALUE'] = (!empty($arr['col_value'][$ord_id])) ? $arr['col_value'][$ord_id] : 0;
//            $values['VALUE_TYPE'] = (!empty($arr['col_value_type'][$ord_id])) ? $arr['col_value_type'][$ord_id] : 0;
//            $values['OTHER_SOURCE'] = (!empty($arr['col_source_other'][$ord_id])) ? $arr['col_source_other'][$ord_id] : 0;
//            $values['OTHER_VALUE'] = (!empty($arr['col_value_other'][$ord_id])) ? $arr['col_value_other'][$ord_id] : 0;
//            $values['FILL'] = (!empty($arr['col_fill'][$ord_id])) ? 1 : 0;
            $values['IN_LIST'] = (isset($arr['col_in_list'][$ord_id])) ? 1 : 0;
            $values['RELATION'] = (!empty($arr['col_relation'][$ord_id])) ? $arr['col_relation'][$ord_id] : 0;
            $values['MANDATORY'] = (isset($arr['col_mandatory'][$ord_id])) ? 1 : 0;
            $values['VISIBLE'] = (isset($arr['col_visible'][$ord_id])) ? 1 : 0;

            $competence = \Atp\Document\Field::COMPETENCE_NONE;
            if (count($arr['col_competence'][$ord_id]) === 2) {
                $competence = \Atp\Document\Field::COMPETENCE_ALL;
            } elseif (isset($arr['col_competence'][$ord_id][0])) {
                $competence = \Atp\Document\Field::COMPETENCE_INVESTIGATE;
            } elseif (isset($arr['col_competence'][$ord_id][1])) {
                $competence = \Atp\Document\Field::COMPETENCE_EXAMINATE;
            }

            $values['COMPETENCE'] = $competence;
            $values['ORD'] = (!empty($arr['col_order'][$ord_id])) ? $arr['col_order'][$ord_id] : 0;
            $values['TEXT'] = (!empty($arr['col_text'][$ord_id])) ? $arr['col_text'][$ord_id] : 0;
            $values['UPDATE_DATE'] = date('Y-m-d H:i:s');
            $values['CREATE_DATE'] = date('Y-m-d H:i:s');

            if (empty($values['LABEL']) || empty($values['TYPE'])) {
                continue;
            }

            $col_id = (!empty($col_key) ? $col_key : 'x' . $ord_id);
            $structure_values[$col_id] = $values;

            if ($type === 'WS') {
                $ws_arr = array(
                    'WS_NAME' => $arr['col_default_field_ws'][$ord_id],
                    'FIELD_ID' => $col_id,
                    'WS_FIELD_ID' => $arr['col_default_field'][$ord_id],
                    'PARENT_FIELD_ID' => $values['ITEM_ID'],
                );
                if (strpos($ws_arr['FIELD_ID'], 'x') !== FALSE) {
                    $ws_arr['FIELD_ID'] = null;
                }

                $ws_relations[$col_id] = $ws_arr;
            }
        }

        $removedFields = array();
        $last_col_id = 0;
        $table_structure = $this->get_extra_fields($arr['item'], $type);
        if ($table_structure !== FALSE) {
            foreach ($table_structure as $structure) {
                if (!empty($structure_values[$structure['ID']])) {
                    $new = $structure_values[$structure['ID']];

                    $fieldOrderMap[$new['ORD']] = $structure['ID'];

                    $update = array(null,
                        'id' => (int) $structure['ID'],
                        'label' => $new['LABEL'],
                        'default' => $new['DEFAULT'],
                        'default_id' => $new['DEFAULT_ID'],
                        'type' => $new['TYPE'],
                        'sort' => $new['SORT'],
                        'render' => $new['RENDER'],
                        'source' => $new['SOURCE'],
                        'ws' => $new['WS'],
//                        'value' => $new['VALUE'],
//                        'value_type' => $new['VALUE_TYPE'],
//                        'other_source' => $new['OTHER_SOURCE'],
//                        'other_value' => $new['OTHER_VALUE'],
//                        'fill' => $new['FILL'],
                        'in_list' => $new['IN_LIST'],
                        'relation' => $new['RELATION'],
                        'mandatory' => $new['MANDATORY'],
                        'visible' => $new['VISIBLE'],
                        'competence' => $new['COMPETENCE'],
                        'ord' => $new['ORD'],
                        'update_date' => $new['UPDATE_DATE'],
                        'text' => $new['TEXT']);

                    $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_FIELD
						SET
							`LABEL` = :label,
							`DEFAULT` = :default,
							`DEFAULT_ID` = :default_id,
							`TYPE` = :type,
							`SORT` = :sort,
							`RENDER` = :render,
							`SOURCE` = :source,
							`WS` = :ws,
						#	`VALUE` = :value,
						#	`VALUE_TYPE` = :value_type,
						#	`OTHER_SOURCE` = :other_source,
						#	`OTHER_VALUE` = :other_value,
						#	`FILL` = :fill,
							`IN_LIST` = :in_list,
							`RELATION` = :relation,
							`MANDATORY` = :mandatory,
							`VISIBLE` = :visible,
							`COMPETENCE` = :competence,
							`ORD` = :ord,
							`UPDATE_DATE` = :update_date,
							`TEXT` = :text
						WHERE `ID` = :id
						LIMIT 1', $update);
                    unset($structure_values[$structure['ID']]);
                } else {
                    $removedFields[] = $structure['ID'];
                }
            }

            if (count($removedFields)) {
                //$this->core->db_query_fast('DELETE FROM ' . PREFIX . 'ATP_FIELD WHERE ID IN(' . join(',', $removedFields) . ')');
                //$this->delete_table_structure(array('#ID#' => 'IN(' . join(',', $delete_ids) . ')'));
                $this->delete_table_structure(array(0 => '`ID` IN(' . join(',', $removedFields) . ')'));

                if ($type === 'WS') {
                    $this->core->db_query_fast('DELETE FROM ' . PREFIX . 'ATP_WEBSERVICES WHERE PARENT_FIELD_ID = ' . (int) $arr['item'] . ' AND FIELD_ID IN(' . join(',', $removedFields) . ')');
                }
            }

            $qry = 'SELECT NAME
				FROM ' . PREFIX . 'ATP_FIELD
				WHERE
					ITEM_ID = :item AND
					ITYPE = :itype
				ORDER BY ID DESC
				LIMIT 1';
            $result = $this->core->db_query_fast($qry, array('item' => $arr['item'],
                'itype' => $arr['itype']));
            $row = $this->core->db_next($result);
            preg_match('/[0-9]+$/', $row['NAME'], $m);
            $last_col_id = $m[0];
        }

        foreach ($structure_values as $fieldKey => $field) {
            $last_col_id++;
            $field['NAME'] = $prefix . $this->core->_prefix->column . $arr['item'] . '_' . $last_col_id;
            $fieldId = $this->core->db_quickInsert(PREFIX . "ATP_FIELD", $field);

            $fieldOrderMap[$field['ORD']] = $fieldId;

            if ($type === 'WS') {
                if (isset($ws_relations[$fieldKey])) {
                    $ws_relations[$fieldKey]['FIELD_ID'] = $fieldId;
                    $this->core->db_quickInsert(PREFIX . "ATP_WEBSERVICES", $ws_relations[$fieldKey]);
                }
            }
        }


        // Į kokius web-serviso parametrus paduoti užpildyto lauko duomenis
        if ($type === 'WS') {
            $ws_parent_relation = array();
            foreach ($arr['ws_relation'] as $name => &$rel) {
                $ws_parent_relation[] = array(
                    'WS_NAME' => $name,
                    'WS_FIELD_ID' => $rel,
                    'FIELD_ID' => $arr['item']
                );
            }

            $this->core->db_query_fast('DELETE FROM ' . PREFIX . 'ATP_WEBSERVICES_FIELDS WHERE FIELD_ID = ' . (int) $arr['item']);

            if (count($ws_parent_relation))
                $this->core->db_ATP_insert(PREFIX . "ATP_WEBSERVICES_FIELDS", $ws_parent_relation);
        }


        $entityManager = $this->core->factory->Doctrine();
        /* @var $configurationRepository \Atp\Repository\FieldConfigurationRepository */
        $configurationRepository = $entityManager->getRepository(\Atp\Entity\FieldConfiguration::class);

//        $configurationRepository->removeByFields(array_merge(
//            array_values($fieldOrderMap),
//            array_values($removedFields)
//        ));
        $fieldsIds = array_unique(array_merge(
                array_values($fieldOrderMap), array_values($removedFields)
        ));
        if (count($fieldsIds)) {
            $configurations = $configurationRepository->getByFieldId($fieldsIds);
            foreach ($configurations as $configuration) {
                $entityManager->remove($configuration);
            }
        }
        if (empty($arr['col_configuration']) === false) {
            foreach ($arr['col_configuration'] as $orderKey => $configurations) {
                $fieldId = $fieldOrderMap[$orderKey];
                $field = $entityManager->getReference(\Atp\Entity\Field::class, $fieldId);
                foreach ($configurations as $configurationId) {
                    $entity = new \Atp\Entity\FieldConfiguration();
                    $entity->setType(
                        $entityManager->getReference(
                            \Atp\Entity\FieldConfigurationType::class,
                            $configurationId
                        )
                    );
//                    $entity->setFieldId($fieldId);
                    $entity->setField($field);

                    $entityManager->persist($entity);
                }
            }
        }

        $entityManager->flush();

        $adminJournal = $this->core->factory->AdminJournal();
        $adminJournal->addRecord(
            $adminJournal->getSaveRecord(
                'Papildomus laukus'
                . ' (Formos elemento tipas "' . $arr['itype'] . '";'
                . ' "Formos elemento identifikatorius "' . $arr['item'] . '").'
            )
        );

        return array('type' => 'success', 'msg' => 'Išsaugota');
    }

    public function edit_table_relations()
    {
        // Dokumentas į kurį keliauja duomenys
        $to_table = (!empty($_POST['table_id'])) ? $_POST['table_id'] : 0;
        // Dokumentas iš kurio keliauja duomenys
        $from_table = (!empty($_POST['table_path_id'])) ? $_POST['table_path_id'] : 0;

        // Sukurti ryšiai
        $relations = array();
        if (!empty($_POST['table_relation']))
            $relations = $_POST['table_relation'];
        // Sukurti ryšiai, pagal nutylėjimą
        $defaults = array();
        if (!empty($_POST['FIELD']))
            $defaults = $_POST['FIELD'];

        if (empty($to_table) || empty($from_table) || (empty($relations) && empty($defaults)))
            return false;

        $table_structure = $this->core->logic2->get_fields_all(array('ITYPE' => 'DOC',
            'ITEM_ID' => $to_table));
        // Sukurti ryšiai
        //$table_structure_relations = $this->get_table_relations($to_table, $from_table);
        $table_structure_relations = $this->get_table_relations($from_table, $to_table);

        $column_relations = array();
        foreach ($table_structure_relations as $table_structure_relation)
            $column_relations[$table_structure_relation['COLUMN_ID']] = $table_structure_relation;
        unset($table_structure_relations);

        $parent_column_structure = $this->core->logic2->get_fields_all(array('ITYPE' => 'DOC',
            'ITEM_ID' => $from_table));

        $delete_relations = array();
        foreach ($table_structure as $t_structure) {
            //if ($relations[$t_structure['ID']] != 0 || $defaults[$t_structure['ID']] != NULL) {
            if ($relations[$t_structure['ID']] != 0 || empty($defaults[$t_structure['ID']]) === FALSE) {
                $parent_column_id = $relations[$t_structure['ID']];
                $parent_column_name = $parent_column_structure[$parent_column_id]['NAME'];
                $default = $defaults[$t_structure['ID']];
                $relation = $column_relations[$t_structure['ID']];

                if ($parent_column_structure[$parent_column_id]['FIELD_TYPE'] == $t_structure['FIELD_TYPE']) {
                    if ($relation) {
                        if ($relation['COLUMN_PARENT_ID'] != $relations[$t_structure['ID']] || $relation['DEFAULT'] != $default) {
                            $COLUMN_PARENT_ID = $relations[$t_structure['ID']];
                            $COLUMN_PARENT_NAME = $t_structure['NAME'];
                            if (empty($default) === FALSE) {
                                unset($COLUMN_PARENT_ID, $COLUMN_PARENT_NAME);
                            }
                            $qry = 'UPDATE `' . PREFIX . 'ATP_TABLE_RELATIONS`
									SET
										`COLUMN_PARENT_ID` = :COLUMN_PARENT_ID,
										`COLUMN_PARENT_NAME` = :COLUMN_PARENT_NAME,
										`DEFAULT` = :DEFAULT
									WHERE
										`DOCUMENT_ID` = :TO_TABLE AND
										`DOCUMENT_PARENT_ID` = :FROM_TABLE AND
										`COLUMN_ID` = :COLUMN_ID
									LIMIT 1';
                            $this->core->db_query_fast($qry, array(
                                'COLUMN_PARENT_ID' => $COLUMN_PARENT_ID,
                                'COLUMN_PARENT_NAME' => $COLUMN_PARENT_NAME,
                                'DEFAULT' => $default,
                                'TO_TABLE' => $to_table,
                                'FROM_TABLE' => $from_table,
                                'COLUMN_ID' => $t_structure['ID'],
                            ));
                        }
                    } else {
                        $value = array(
                            'DOCUMENT_ID' => $to_table,
                            'COLUMN_ID' => $t_structure['ID'],
                            'COLUMN_NAME' => $t_structure['NAME'],
                            'DOCUMENT_PARENT_ID' => $from_table,
                            'COLUMN_PARENT_ID' => $parent_column_id,
                            'COLUMN_PARENT_NAME' => $parent_column_name,
                            'DEFAULT' => $default);
                        $this->core->db_quickInsert(PREFIX . 'ATP_TABLE_RELATIONS', $value);
                    }
                } else
                    $this->core->set_message('error', $this->core->lng('error_column_types_not_match') . ' "' . $t_structure['LABEL'] . ' -> ' . $parent_column_structure[$parent_column_id]['LABEL'] . '"');

                unset($relations[$t_structure['ID']]);
            } else {
                if (empty($t_structure['ID']) === FALSE)
                    $delete_relations[] = $t_structure['ID'];
            }
        }

        if (!empty($delete_relations)) {
            $delete_relations = implode(',', $delete_relations);
            $qry = "DELETE FROM " . PREFIX . "ATP_TABLE_RELATIONS
					WHERE
						DOCUMENT_ID={$to_table} AND
						DOCUMENT_PARENT_ID={$from_table} AND
						COLUMN_ID IN ({$delete_relations})";
            $this->core->db_query_fast($qry);
        }
//die;
        $this->core->set_message('success', $this->core->lng('success_table_relations_edited'));

        return TRUE;
    }

    /**
     * More like get record form data.
     * @param array $fieldValueMap ['lowercase_field_name' => 'form_field_value']
     * @param type $documentId
     * @param type $recordId [optional?]
     * @return array[]
     */
    public function transform_data($fieldValueMap, $documentId, $recordId = null)
    {
        $values = array();
        $extra_values = array();
        $files_fields = array();
        $clause_id = null;

        if (!empty($recordId)) {
            $valuesManager = new \Atp\Record\Values($this->core);
            $recordValues = $valuesManager->getValuesByRecord($recordId, false, true, true);
        }
        
        $fieldsManager = $this->core->factory->Field();
        $fields = $fieldsManager->getFieldsByDocument($documentId);
        $loggedInWorkerId = $this->core->factory->User()->getLoggedPersonDuty()->id;
        
        foreach ($fields as $field) {
            if (isset($fieldValueMap[strtolower($field->getName())])) {
                $value = $fieldValueMap[strtolower($field->getName())];
            } else {
                $value = $recordValues[$field->getName()];
                
                // Jei lauka redaguoti leidžiama, tačiau jis nepateiktas, tai jis yra išjungtas ir jo reikšmės nereikia.
                if (!empty($recordId)) {
                    $isEditable = $fieldsManager->isEditable($recordId, $field->getid(), $loggedInWorkerId);
                    if ($isEditable) {
                        $value = null;
                    }
                }

                if ($field->getItype() === \Atp\Document\Field::TYPE_DOCUMENT) {
                    $values[$field->getName()] = $value;
                } else {
                    $extra_values[$field->getName()] = $value;
                }
                continue;
            }

            if (in_array($field->getType(), array(1, 2, 3, 4, 5, 15, 16)) === true)
                $value = (int) $value;
            elseif (in_array($field->getType(), array(6)) === true)
                $value = (float) str_replace(',', '.', $value);
            elseif (in_array($field->getType(), array(7, 8, 9, 10, 18)) === true) {
                if ($field->getSort() == 5)
                    $value = (array) $value;
                elseif ($field->getType() == 18)
                    $value = (int) $value;
                else {
                    $value = (string) $value;
                }
            } elseif (in_array($field->getType(), array(11, 12, 13, 14)) === true) {
                // Metai
                if ($field->getType() == 11) {
                    $pattern = '#^(19|20)([0-9]{2})$#';
                    $default = 'Y';
                } else
                // Data
                if ($field->getType() == 12) {
                    $pattern = '#^(19|20)([0-9]{2})[-./](0[1-9]|1[012])[-./](0[1-9]|[12][0-9]|3[01])$#';
                    $default = 'Y-m-d';
                } else
                // Laikas
                if ($field->getType() == 13) {
                    //$pattern = '#^([01]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$#';
                    $pattern = '#^([01]?[0-9]|2[0-3]):([0-5][0-9])$#';
                    //$default = 'H:i:s';
                    $default = 'H:i';
                } else
                // Data ir laikas
                if ($field->getType() == 14) {
                    //$pattern = '#^(19|20)([0-9]{2})[-./](0[1-9]|1[012])[-./](0[1-9]|[12][0-9]|3[01]) ([01]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$#';
                    $pattern = '#^(19|20)([0-9]{2})[-./](0[1-9]|1[012])[-./](0[1-9]|[12][0-9]|3[01]) ([01]?[0-9]|2[0-3]):([0-5][0-9]))$#';
                    //$default = 'Y-m-d H:i:s';
                    $default = 'Y-m-d H:i';
                }

                $is_empty = empty($value);
                if (!preg_match($pattern, $value)) {
                    if ($is_empty === true)
                        $value = '';
                    else {
                        $value = date($default, strtotime($value));
                    }
                }
            } else
            // Failas
            if ($field->getSort() == 10 && empty($_FILES) === false) {
                // Nauji failai
                if (isset($_FILES[strtolower($field->getName())]) === true) {
                    $files_fields[$field->getId()] = $_FILES[strtolower($field->getName())];
                }
            } else
            // Straipsnis
            if ($field->getSort() == 6 && empty($value) === false) {
                $clause_id = $value;
            }

            if (in_array($field->getSort(), [5, 8]) && empty($value) === false) {
                if ($field->getSort() == 8) {
                    $value = array($value);
                }
                $value = json_encode($value);
            }

            // Jei reikšmė tuščia, bet ne 0 ar '0'
            if ($value === null || $value === '') {
                $value = null;
            }


            if ($field->getItype() === \Atp\Document\Field::TYPE_DOCUMENT) {
                $values[$field->getName()] = $value;
            } else {
                $extra_values[$field->getName()] = $value;
            }
        }

        return array(
            'values' => $values,
            'extra_values' => $extra_values,
            'files_fields' => $files_fields,
            'clause' => $clause_id
        );
    }

//    /**
//     * Transform array with conditions to SQL query
//     * Array structure:
//     * array('and' => array('id = 0', 'id2 >= 3'), 'or' => array('sec = \'www\'', 'sec2 <> \'erere\'')), etc
//     * where array key - condition (AND, OR, etc), value - condition string.
//     *
//     * @param array $condition
//     * @param string $prefix
//     * @param bool $where - True - yes, flase - not
//     * @return string
//     */
//    function _PrepareCondition($condition, $where = FALSE, $prefix = '')
//    {
//        if (!is_array($condition)) {
//            return $condition;
//        }
//        $sql = ' ';
//        $limit = ' ';
//        if (TRUE === $where) {
//            $sql .= 'WHERE ' . $prefix;
//        }
//
//        $keys = array_keys($condition);
//        for ($i = 0; $i < count($keys); $i++) {
//            if (FALSE === $where || (TRUE === $where && $i > 0)) {
//                $sql .= ' ' . strtoupper($keys[$i]) . ' ' . $prefix;
//            }
//            $sql .= implode(' ' . strtoupper($keys[$i]) . ' ' . $prefix, $condition[$keys[$i]]);
//        }
//        return $sql;
//    }

    /**
     * Pažymi, kad dokumenoto įrašo statusas - Ištrintas
     * @param int $table_id
     * @param string $record_id Numeris
     * @return boolean
     */
    public function delete_record($table_id, $record_id)
    {
        if (empty($table_id) === TRUE) {
            trigger_error('Table ID is empty', E_USER_WARNING);
            return false;
        }
        if (empty($record_id) === TRUE) {
            trigger_error('Record ID is empty', E_USER_WARNING);
            return false;
        }

        $record = $this->core->logic2->get_record_relation($record_id);

        $privileges_data = array(
            'user_id' => $this->core->factory->User()->getLoggedUser()->person->id,
            'department_id' => $this->core->factory->User()->getLoggedPersonDuty()->department->id,
            'record_id' => $record['ID'],
            'table_id' => $table_id
        );

        if ($this->privileges('delete_record', $privileges_data) === FALSE) {
            return false;
        }

        $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD
            SET
                STATUS = '.\Atp\Record::STATUS_DELETED.'
            WHERE
                RECORD_ID = :record_id AND
                IS_LAST = 1
            LIMIT 1', array('record_id' => $record_id));

        return true;

        // Sename kode buvo trinami visi sukurti dokumento įrašai dokumento kelyje
    }

    /**
     * Atnaujina dokumento įrašo būklę
     * @param string $record_nr Dokumento įrašo numeris
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     */
    public function update_record_status($record)
    {
        $record = $this->core->logic2->get_record_relation($record);
        if (empty($record) === TRUE) {
            trigger_error('Record not found', E_USER_ERROR);
            exit;
        }

        $status = new \Atp\Document\Record\Status(array('RECORD' => $record), $this->core);
        //$status->setDebug(true);
        $statuses = $status->checkStatus();

        foreach ($statuses as $arr) {
            $status->SetRecordStatus($arr['RECORD'], $arr['STATUS']);
        }
    }

    /**
     * Priskiria darbuotojus prie dokumento įrašo
     * @param array $param
     * @param int $param[is_empty]
     * @param int $param[new_r_id] Naujo dokumento įrašo ATP_RECORD.ID
     * @param array $param[status] Indeksai: new, finish_investigation, finish_idle, finish_examination
     * @param int $param[clause] Straipsnio ID. $clause iš $this->transform_data()
     * @param array $param[r_relations] Ankstesnio dokumento įrašo duomenys iš ATP_RECORD
     * @param boolen $param[r_path] Ar dokumento įrašo duomenys keliauja į kitą dokumentą
     * @param int $param[table_from_id] Dokumento ID iš kurio kuriamas naujas dokumento įrašas
     * @param int $param[table_id] Naujo dokumento įrašo dokumento ID
     * @return array $current_workers
     */
    private function create_record_add_worker($param)
    {
        /*
         *
         */
//	 file_put_contents(GLOBAL_REAL_URL.'logs/add_worker'.$param['new_r_id'].'.txt', var_export($param, true));
        extract($param);

        // Saugomas dokumentas
        $record = $this->core->logic2->get_record_relation($new_r_id);
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', PHP_EOL .'RECORD "'.$record['RECORD_ID'].'" ("'.$record['ID'].'") ' . PHP_EOL, FILE_APPEND);
        // Saugomo dokumento straipsnis
        $clauseId = $this->get_record_clause($record);
		$clauseData = $this->core->logic2->get_clause(['ID' => $clauseId], true);
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'CLAUSE "'.$clauseData['NAME'].'" ("'.$clauseData['ID'].'") ' .json_encode($clause) . PHP_EOL, FILE_APPEND);
        // Ankstesnis dokumentas
        $prev_record = $this->core->logic2->get_record_relation($record['RECORD_FROM_ID']);

//        $result = $this->core->db_query_fast('
//            SELECT A.ID CNT
//			FROM ' . PREFIX . 'ATP_RECORD_UNSAVED A
//				INNER JOIN ' . PREFIX . 'ATP_RECORD B
//					ON
//						B.ID = A.RECORD_ID AND
//						B.TABLE_TO_ID = :DOCUMENT_ID AND
//						B.ID = :ID AND
//						B.IS_LAST = 1
//			WHERE
//				A.WORKER_ID = :WORKER_ID', [
//            'ID' => $record['ID'],
//            'WORKER_ID' => $this->core->factory->User()->getLoggedPersonDuty()->id,
//            'DOCUMENT_ID' => $record['TABLE_TO_ID']
//        ]);
//        $count = $this->core->db_num_rows($result);
//        $isTemporary = (bool) $count;
//        $isTemporary = (bool) $is_empty;
        //$reset = false;
        //if ($count) {
        //	$reset = true;
        //}

        $worker_tmp = [
            'RECORD_ID' => $record['ID'],
            'WORKER_ID' => null,
            'TYPE' => null
        ];

        $current_document = $this->core->logic2->get_document(['ID' => $table_id]);

        $workers = [];
        $current_workers = [];
        ;
        // Pagal dokumento tipą
        switch ($current_document['STATUS']) {
            // Nesvarbu
            case \Atp\Entity\Document::STATUS_BOTH:
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'Nesvarbu ' . PHP_EOL, FILE_APPEND);

                if (empty($status['finish_investigation']) === TRUE && empty($status['finish_idle']) === TRUE) {
                    $this->core->db_quickupdate(PREFIX . 'ATP_RECORD', ['STATUS' => \Atp\Record::STATUS_INVESTIGATING], $record['RECORD_ID'], 'ID');
                }
            //break;
            // Tiriamas
            case \Atp\Entity\Document::STATUS_INVEST:
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'Tiriamas ' . PHP_EOL, FILE_APPEND);
                /* if(isset($prev_record['ID']) && $r_path !== TRUE) {
                  $old_workers = $this->core->logic2->get_record_worker(array('A.RECORD_ID = ' . (int) $prev_record['ID']));
                  } else {
                  $old_workers = $this->core->logic2->get_record_worker(array('A.RECORD_ID = ' . $record['ID']));
                  } */


                /**
                 * Jei nevyksta perdavimas į kitą dokumentą ir tai yra nauja dokumento versija
                 * (kuriama nauja versija<del>, baigiamas tyrimas arba tarpinė būsena</del>),
                 * tai priskiriami buvusios versijos darbuotojai
                 */
                if ((
                    (isset($status['new']) && $status['new'] === TRUE)
                    /* || (isset($status['finish_investigation']) && $status['finish_investigation'] === TRUE)
                      || (isset($status['finish_idle']) && $status['finish_idle'] === TRUE) */
                    ) && $r_path !== TRUE) {


                    $prev_workers = $this->core->logic2->get_record_worker([
                        'A.RECORD_ID = ' . (int) $prev_record['ID']
                    ]);
                    /* if($status['new'] === TRUE) {
                      $record_to_get_workers_from = $prev_record['ID'];
                      } else {
                      $record_to_get_workers_from = $record['ID'];
                      }
                      $workers_to_check = $this->core->logic2->get_record_worker(array('A.RECORD_ID = ' . (int) $record_to_get_workers_from)); */

                    foreach ($prev_workers as $worker) {
                        //foreach ($workers_to_check as $worker) {

                        $tmp = $worker_tmp;

                        $tmp['WORKER_ID'] = $worker['WORKER_ID'];
                        $tmp['TYPE'] = $worker['TYPE'];

                        // Jei priskirtas numatytasis nagrinėtojas ir baigiama tarpinė būsena, tai jis numatomas kaip nagrinėtojas ir pagrindinis dokumento darbuotojas
                        if ($tmp['TYPE'] === \Atp\Record\Worker::TYPE_NEXT && isset($status['finish_idle']) && $status['finish_idle'] === TRUE) {
                            $tmp['TYPE'] = \Atp\Record\Worker::TYPE_EXAMINATOR;
                            $current_workers['main'] = $tmp['WORKER_ID'];
                        } else
                        // Kitaip tyrėjas yra pagrindinis dokumento darbuotojas
                        if ($tmp['TYPE'] === \Atp\Record\Worker::TYPE_INVESTIGATOR) {
                            $current_workers['main'] = $tmp['WORKER_ID'];
                        }

                        if (empty($tmp['WORKER_ID']) === false) {
                            $workers[] = $tmp;
                        }

                        unset($tmp);
                    }
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'Previous workers (on new) ' . json_encode($workers). PHP_EOL, FILE_APPEND);
                } else {
                    // Vykdoma saugant be naujos versijos
                    // Jei dokumentas keliauja (į tirimą), tai "next" tipo darbuotojas neliečiamas
                    /**
                     * Jei sugoma be naujos versijos
                     * (išsugoti<ins>, baigiamas tyrimas arba tarpinė būsena, baigiamas nagrinėjimas(?)</ins>),
                     * tai "next" tipo darbuotojas neliečiamas
                     */
                    // Paprastam saugojimui surenka esamą informacija
                    $record_workers = $this->core->logic2->get_record_worker([
                        'A.RECORD_ID = ' . $record['ID']
                    ]);

                    if (count($record_workers)) {
                        foreach ($record_workers as $rw) {
                            $tmp = $worker_tmp;

                            $tmp['WORKER_ID'] = $rw['WORKER_ID'];
                            $tmp['TYPE'] = $rw['TYPE'];

                            if ($tmp['TYPE'] === \Atp\Record\Worker::TYPE_INVESTIGATOR) {
                                $current_workers['main'] = $tmp['WORKER_ID'];
                            } else
                            if ($tmp['TYPE'] === \Atp\Record\Worker::TYPE_NEXT) {
                                if ($status['finish_idle'] === TRUE) {
                                    $tmp['TYPE'] = \Atp\Record\Worker::TYPE_EXAMINATOR;
                                    $current_workers[\Atp\Record\Worker::TYPE_EXAMINATOR] = $tmp['WORKER_ID'];
                                } else {
                                    $current_workers[\Atp\Record\Worker::TYPE_NEXT] = $tmp['WORKER_ID'];
                                }
                            }

                            if (empty($tmp['WORKER_ID']) === false) {
                                $workers[] = $tmp;
                            }
                        }

//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'Current workers ' . json_encode($workers). PHP_EOL, FILE_APPEND);
                    } else {
                        $tmp = $worker_tmp;

                        // Jei nera ankščiau priskirtų darbuotojų
                        // Jei registruojantis asmuo nėra tyrėjas, surasti laisviausią tyrėją ir jam priski dokumento įrašą
                        // Tikrina registruojančio darbuotojo tyrimo ir nagrinėjimo teises
                        $can_investigate = $this->can_current_worker_receive_record('INVESTIGATE_RECORD', $record, $clauseId);
                        if ((int) $current_document['STATUS'] === \Atp\Entity\Document::STATUS_BOTH) {
                            $can_examinate = $this->can_current_worker_receive_record('EXAMINATE_RECORD', $record, $clauseId);
                        } else {
                            $can_examinate = false;
                        }
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'Current user can ' . json_encode(['$can_investigate'=>$can_investigate, '$can_examinate'=>$can_examinate]). PHP_EOL, FILE_APPEND);
//                        $tmp['WORKER_ID'] = null;
                        // Ieško galinčio tirti
                        if ($can_investigate === FALSE && $can_examinate === FALSE) {
                            $tmp['TYPE'] = \Atp\Record\Worker::TYPE_INVESTIGATOR;

                            $worker = $this->get_free_investigator($current_document['ID'], $clauseId);
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'INVEST by clause "'.$clauseId.'": ' . json_encode([$current_document['ID'],$clauseId,$worker]) . PHP_EOL, FILE_APPEND);

                            if (empty($worker)) {
                                $worker = $this->get_free_investigator($current_document['ID']);
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'INVEST no clause "'.$clauseId.'": ' . json_encode([$current_document['ID'],null,$worker]) . PHP_EOL, FILE_APPEND);
                            }

                            if (empty($worker['ID']) === FALSE) {
                                $tmp['WORKER_ID'] = $worker['WORKER_ID'];
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'INVEST set: ' . json_encode($tmp) . PHP_EOL, FILE_APPEND);
                            }
                        } else {
                            if ($can_investigate === FALSE && $can_examinate === TRUE) {
                                $tmp['TYPE'] = \Atp\Record\Worker::TYPE_EXAMINATOR;
                            } else {
                                $tmp['TYPE'] = \Atp\Record\Worker::TYPE_INVESTIGATOR;
                            }

                            // Registruojantis vartotojas gali dirbti su dokumentu
                            //$worker = $this->core->logic2->get_worker_info(array('A.ID = ' . (int) $this->core->factory->User()->getLoggedPersonDuty()->id), true);
                            //if (empty($worker['ID']) === FALSE)
                            //	$tmp['WORKER_ID'] = $worker['ID'];
                            $tmp['WORKER_ID'] = $this->core->factory->User()->getLoggedPersonDuty()->id;
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'INVEST/EXAM set: ' . json_encode($tmp) . PHP_EOL, FILE_APPEND);
                        }

                        if (empty($tmp['WORKER_ID']) === false) {
                            $current_workers['main'] = $tmp['WORKER_ID'];

                            $workers[] = $tmp;
                        }

                        unset($tmp);

                        if (!$is_empty) {
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'BULL 1' . PHP_EOL, FILE_APPEND);
                            //-- BULL BEGIN --// Neįmanoma tiksliai numatyti koks bus sekantis dokumentas
                            // Jei sekantis dokumentas yra nagrainėjimo dokumentas, tai suranda galintį nagrinėti
                            $next_document = $this->core->logic2->get_document(array(
                                'PARENT_ID' => $current_document['ID']), true, null, 'ID, STATUS');

                            if ((int) $next_document['STATUS'] === \Atp\Entity\Document::STATUS_EXAM) {
                                $tmp = $worker_tmp;
                                $tmp['TYPE'] = \Atp\Record\Worker::TYPE_NEXT;

                                #// Jei registruojantis asmuo nėra nagrinėtojas, surasti laisviausią nagrinėtoją ir jam priski dokumento įrašą
                                #if ($this->check_privileges('EXAMINATE_RECORD', $check_arr) === FALSE) {
                                #} else {
                                $worker = $this->get_free_examinator($next_document['ID'], $clauseId);
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'EXAM by clause "'.$clauseId.'": ' . json_encode([$next_document['ID'],$clauseId,$worker]) . PHP_EOL, FILE_APPEND);

                                if (empty($worker)) {
                                    $worker = $this->get_free_examinator($next_document['ID']);
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'EXAM no clause "'.$clauseId.'": ' . json_encode([$next_document['ID'],null,$worker]) . PHP_EOL, FILE_APPEND);
                                }
                                if (empty($worker) === FALSE) {
                                    $tmp['WORKER_ID'] = $worker['WORKER_ID'];
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'EXAM set: ' . json_encode($tmp) . PHP_EOL, FILE_APPEND);

                                    $workers[] = $tmp;
                                    $current_workers[\Atp\Record\Worker::TYPE_NEXT] = $tmp['WORKER_ID'];
                                }
                                #}
                                unset($tmp);
                            }
                            //-- BULL END --//
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'BULL 1 END' . PHP_EOL, FILE_APPEND);
                        }
                    }
                }


                if (!$is_empty) {
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'BULL 2' . PHP_EOL, FILE_APPEND);
                    //-- BULL BEGIN --// nebepriskirinėjo numatytų nagrinėtojų, tai jei nerastas numatytas nagrinėtojas arba nagrinėjantis asmuo - priskiria
                    $next_is_set = false;
                    foreach ($workers as $worker) {
                        if ($worker['TYPE'] === \Atp\Record\Worker::TYPE_NEXT || $worker['TYPE'] === \Atp\Record\Worker::TYPE_EXAMINATOR) {
                            $next_is_set = true;
                            break;
                        }
                    }
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', '$next_is_set: ' . json_encode($next_is_set) . PHP_EOL, FILE_APPEND);
                    $next_is_set = false;
                    if ($next_is_set === FALSE) {
                        if ($current_document['ID'] == \Atp\Document::DOCUMENT_PROTOCOL) {
                            $next_document = $this->core->logic2->get_document([
                                'ID' => \Atp\Document::DOCUMENT_CASE], true, null, 'ID, STATUS');
                        } else {
                            $next_document = $this->core->logic2->get_document([
                                'PARENT_ID' => $current_document['ID']], true, null, 'ID, STATUS');
                        }

                        if (in_array($next_document['STATUS'], [\Atp\Entity\Document::STATUS_EXAM,
                                \Atp\Entity\Document::STATUS_BOTH])) {
                            $tmp = $worker_tmp;
                            $tmp['TYPE'] = \Atp\Record\Worker::TYPE_NEXT;

                            #// Jei registruojantis asmuo nėra nagrinėtojas, surasti laisviausią nagrinėtoją ir jam priski dokumento įrašą
                            #if ($this->check_privileges('EXAMINATE_RECORD', $check_arr) === FALSE) {
                            #} else {
                            $worker = $this->get_free_examinator($next_document['ID'], $clauseId);
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'EXAM by clause "'.$clauseId.'": ' . json_encode([$next_document['ID'],$clauseId,$worker]) . PHP_EOL, FILE_APPEND);

                            if (empty($worker)) {
                                $worker = $this->get_free_examinator($next_document['ID']);
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'EXAM no clause: ' . json_encode([$next_document['ID'],null,$worker]) . PHP_EOL, FILE_APPEND);
                            }
                            if (empty($worker) === FALSE) {
                                $tmp['WORKER_ID'] = $worker['WORKER_ID'];
                                #}
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'EXAM set: ' . json_encode($tmp) . PHP_EOL, FILE_APPEND);

                                $workers[] = $tmp;
                                $current_workers[\Atp\Record\Worker::TYPE_NEXT] = $tmp['WORKER_ID'];
                            }

                            unset($tmp);
                        }
                    }
                    //-- BULL END --//
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'BULL 2 END' . PHP_EOL, FILE_APPEND);
                }

                break;
            // Nagrinėjamas
            case \Atp\Entity\Document::STATUS_EXAM:
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'Nagrinėjamas ' . PHP_EOL, FILE_APPEND);
                // Paprastam saugojimui surenka esamą informacija
                $record_workers = $this->core->logic2->get_record_worker(array('A.RECORD_ID = ' . $record['ID']));
                if (count($record_workers)) {
                    foreach ($record_workers as $rw) {
                        $tmp = $worker_tmp;
                        $tmp['WORKER_ID'] = $rw['WORKER_ID'];
                        $tmp['TYPE'] = $rw['TYPE'];

                        if (empty($tmp['WORKER_ID']) === false) {
                            $workers[] = $tmp;

                            if ($tmp['TYPE'] === \Atp\Record\Worker::TYPE_INVESTIGATOR) {
                                $current_workers['main'] = $tmp['WORKER_ID'];
                            } else
                            if ($tmp['TYPE'] === \Atp\Record\Worker::TYPE_NEXT) {
                                $current_workers[\Atp\Record\Worker::TYPE_NEXT] = $tmp['WORKER_ID'];
                            }
                        }

                        unset($tmp);
                    }
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'Current workers ' . json_encode($workers). PHP_EOL, FILE_APPEND);
                } else {
                    // Jei tai yra nauja dokumento versija, tai priskiriami buvusios versijos darbuotojai
                    if ($status['new'] === TRUE && $r_path !== TRUE) {
                        $prev_workers = $this->core->logic2->get_record_worker(array(
                            'A.RECORD_ID = ' . (int) $prev_record['ID']));
                        foreach ($prev_workers as $worker) {
                            $tmp = $worker_tmp;
                            $tmp['WORKER_ID'] = $worker['WORKER_ID'];
                            $tmp['TYPE'] = $worker['TYPE'];

                            if (empty($tmp['WORKER_ID']) === false) {
                                if ($tmp['TYPE'] === \Atp\Record\Worker::TYPE_EXAMINATOR) {
                                    $current_workers['main'] = $tmp['WORKER_ID'];
                                }

                                $workers[] = $tmp;
                            }

                            unset($tmp);
                        }
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'Set OLD workers ' . json_encode($workers). PHP_EOL, FILE_APPEND);
                    } else {
                        // Jei tai naujas dokumentas arba keliauja (į tirimą), tai "next" tipo darbuotojas neliečiamas

                        $tmp = $worker_tmp;
                        $tmp['TYPE'] = \Atp\Record\Worker::TYPE_EXAMINATOR;

                        $prev_document = $this->core->logic2->get_document(array(
                            'ID' => $table_from_id), true, null, 'ID, STATUS');

                        if ((int) $prev_document['STATUS'] === \Atp\Entity\Document::STATUS_INVEST) {
                            $worker = $this->core->logic2->get_record_worker([
                                'A.RECORD_ID = ' . (int) $r_relations['ID'],
                                'A.TYPE = "next"'], true);
                            if (empty($worker) === FALSE) {
                                $tmp['WORKER_ID'] = $worker['WORKER_ID'];
                            }
                        }
                        if (empty($tmp['WORKER_ID']) === TRUE) {
                            // Jei registruojantis asmuo nėra nagrinėtojas, surasti laisviausią nagrinėtoją ir jam priski dokumento įrašą
                            if ($this->can_current_worker_receive_record('EXAMINATE_RECORD', $record, $clauseId) === FALSE) {
//                                $worker = $this->get_free_examinator($current_document['ID'], $clauseId);
                                $worker = $this->get_free_examinator($current_document['ID'], $clauseId);
                                if (empty($worker)) {
                                    $worker = $this->get_free_examinator($current_document['ID']);
                                }
                                if (empty($worker['ID']) === FALSE) {
                                    $tmp['WORKER_ID'] = $worker['WORKER_ID'];
                                }
                            } else {
                                $worker = $this->core->logic2->get_worker_info([
                                    'A.PEOPLE_ID = ' . (int) $this->core->factory->User()->getLoggedUser()->person->id,
                                    'STRUCTURE_ID' => (int) $this->core->factory->User()->getLoggedPersonDuty()->department->id
                                    ], true);
                                if (empty($worker['ID']) === FALSE) {
                                    $tmp['WORKER_ID'] = $worker['ID'];
                                }
                            }
                        }

                        if (empty($tmp['WORKER_ID']) === false) {
                            $workers[] = $tmp;
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'Set worker ' . json_encode($tmp). PHP_EOL, FILE_APPEND);

                            $current_workers['main'] = $tmp['WORKER_ID'];
                        }
                    }
                }
                break;
        }

//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'AFTER ' . PHP_EOL, FILE_APPEND);
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'Is examinator: ' . PHP_EOL, FILE_APPEND);

        foreach ($workers as $key => $worker) {
            $result = $this->can_worker_receive_record('EXAMINATE_RECORD', $record['ID'], $worker['WORKER_ID'], $clauseId);
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php',  json_encode([$result, $worker]) . PHP_EOL, FILE_APPEND);
        }
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'Is examinator END ' . PHP_EOL, FILE_APPEND);
//file_put_contents(GLOBAL_REAL_URL . '/logs/php_errors.php', 'WORKERS: ' . json_encode($workers) . PHP_EOL, FILE_APPEND);
        $workerManager = new \Atp\Record\Worker($this->core);
        $workerManager->removeRecordWorker('RECORD_ID=' . (int) $record['ID']);

        $workers = array_map(function($jsonString) {
            return json_decode($jsonString, true);
        }, array_unique(array_map('json_encode', $workers)));
/*
		$workers[] = array(
			'WORKER_ID' => $this->core->factory->User()->getLoggedPersonDuty()->id,
			'TYPE' => 'invest',
			'RECORD_ID' => $record['ID'],
		);
*/
		foreach ($workers as $worker) {
            if (empty($worker['WORKER_ID'])) {
                continue;
            }

            $workerManager->addRecordWorker($worker['RECORD_ID'], $worker['WORKER_ID'], $worker['TYPE']);
        }
// file_put_contents(GLOBAL_REAL_URL.'logs/add_worker_res_'.$param['new_r_id'].'.txt', var_export($current_workers, true).var_export($current_workers, true).var_export($workers, true));
		return $current_workers;
    }

    public function can_worker_receive_record($type, $record, $worker_id, $clause = null)
    {
        if (in_array($type, array('INVESTIGATE_RECORD', 'EXAMINATE_RECORD')) === FALSE)
            return false;

        $record = $this->core->logic2->get_record_relation($record);
        if (empty($record) === TRUE)
            return false;

        $table_id = $record['TABLE_TO_ID'];

        // Tikrina ar turi teisę tirti/nagrinėti dokumentą
        $worker_data = $this->core->logic2->get_worker_info(array('A.ID = ' . (int) $worker_id), true);
        $param = array(
            'table_id' => $table_id,
            'user_id' => $worker_data['PEOPLE_ID'],
            'department_id' => $worker_data['STRUCTURE_ID']);
        if ($this->check_privileges($type, $param) === TRUE) {
            if ($clause === NULL) {
                $clause = $this->get_record_clause($record);
            }
            if (empty($clause) === FALSE) {
                $result = $this->core->db_query_fast('
                    SELECT COUNT(ID) CNT
                    FROM ' . PREFIX . 'ATP_USER
                    WHERE
                        CLAUSE_ID = :CLAUSE_ID AND
                        WORKER_ID = :WORKER_ID', array(
                    'CLAUSE_ID' => (int) $clause,
                    'WORKER_ID' => (int) $worker_id)
                );
                $row = $this->core->db_next($result);
                if ((int) $row['CNT'] !== 0)
                    return true;
            } else {
                return true;
            }
        }

        return false;
    }

    /**
     * Tikrina ar prisijungusiam vartotojui galima priskirti dukumento įrašą
     * @param string $type 'INVESTIGATE_RECORD' arba 'EXAMINATE_RECORD'
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param int $clause [optional] Straipsnio ID. <b>Default:</b> NULL
     */
    public function can_current_worker_receive_record($type, $record, $clause = null)
    {
        return $this->can_worker_receive_record($type, $record, $this->core->factory->User()->getLoggedPersonDuty()->id, $clause);
    }

    /**
     * Gražina dokumento įrašo straipsnį
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     */
    public function get_record_clause($record)
    {

$rfile = GLOBAL_REAL_URL.'logs/get_clause_'.date("YmdHis").'.txt';
$log = 'record: '.var_export($record, true);
		
		$record = $this->core->logic2->get_record_relation($record);
        if (empty($record) === TRUE) {
            return false;
        }

        $return = null;
        $deed_fields = $this->core->logic2->get_fields(array('ITYPE' => 'DOC', 'ITEM_ID' => $record['TABLE_TO_ID'],
            'SORT' => 4, 'TYPE' => 23));
        if (count($deed_fields)) {
            $deed_field = current($deed_fields);
            //$row = $this->core->logic2->get_db(PREFIX . 'ATP_TBL_' . $record['TABLE_TO_ID'], array('RECORD_ID' => $record['RECORD_ID'], 'RECORD_LAST' => 1), true, null, false, 'A.ID, A.`' . $deed_field['NAME'] . '` VALUE');
            $row = $this->core->logic2->get_record_table($record, $fields = 'C.ID, C.`' . $deed_field['NAME'] . '` VALUE');
            $deed_value = $row['VALUE'];
//            $deed_value = $this->core->factory->Record()->getFieldValue($record['ID'], $deed_field['ID']);
            if (empty($deed_value) === FALSE) {
                // Veikos informacija
                $deed = $this->get_deed(array('ID' => $deed_value));
                // Veiką praplečia
                $ddata = $this->get_deed_extend_data(array('DEED_ID' => $deed['ID'],
                    'ITYPE' => 'CLAUSE'));

                // Surenka veiką praplečiančių elementų ID
                $clause_ids = array();
                foreach ($ddata as $arr) {
                    $clause_ids[$arr['ID']] = $arr['ITEM_ID'];
                }
                unset($ddata, $deed);


                $table = $this->core->logic2->get_document($record['TABLE_TO_ID']);
                $table_structure = $this->get_table_fields($record['TABLE_TO_ID']);
                $record_date_field_id = $this->core->factory->Document()->getOptions($record['TABLE_TO_ID'])->violation_date;
                if (empty($table_structure[$record_date_field_id]) === FALSE) {
                    $record_date_field = $table_structure[$record_date_field_id];

                    $qry = 'SELECT `' . $record_date_field['NAME'] . '`
						FROM
							' . PREFIX . 'ATP_TBL_' . $record['TABLE_TO_ID'] . ' tn
						WHERE
							tn.ID = ' . $row['ID'] . '
						LIMIT 1';
                    $result = $this->core->db_query_fast($qry);
                    $rrow = $this->core->db_next($result);
                    if (isset($rrow[$record_date_field['NAME']])) {
                        $record_date = $rrow[$record_date_field['NAME']];
                    }
                }
                if (empty($record_date) === TRUE || strpos($record_date, '0000-00-00') !== FALSE) {
                    $record_date = date('Y-m-d H:i:s');
                }
                unset($table, $table_structure, $record_date_field_id, $record_date_field, $qry, $result, $rrow);


                // Straipsnis
                if (count($clause_ids)) {
                    $clauses = $this->core->logic2->get_clause(array(
                        0 => '`ID` IN(' . join(',', $clause_ids) . ')',
                        1 => '"' . $record_date . '"' . ' > `VALID_FROM` AND "' . $record_date . '" <= `VALID_TILL`'
                    ));
                    if (count($clauses)) {
                        $clause = current($clauses);
                        $return = $clause['ID'];
                    }
                }
            }
        }
$log .= 'Result: '.var_export($return, true);
// file_put_contents($rfile, $log);
        return $return;
    }

    public function create_record_empty(Array $param = array())
    {
        $default = array(
            'WORKER_ID' => $this->core->factory->User()->getLoggedPersonDuty()->id,
            'DOCUMENT_ID' => null
        );
        $param = array_merge($default, $param);

        if (empty($param['DOCUMENT_ID']) === TRUE)
            return;

        $qry = 'SELECT A.ID, B.RECORD_ID as RECORD_NR
			FROM ' . PREFIX . 'ATP_RECORD_UNSAVED A
				INNER JOIN ' . PREFIX . 'ATP_RECORD B
					ON
						B.ID = A.RECORD_ID AND
						B.TABLE_TO_ID = :DOCUMENT_ID AND
						B.IS_LAST = 1
			WHERE
				A.WORKER_ID = :WORKER_ID';
        $result = $this->core->db_query_fast($qry, $param);
        $count = $this->core->db_num_rows($result);
        if ($count) {
            $row = $this->core->db_next($result);
            return $row['RECORD_NR'];
        }

        $values = array(
            'RECORD_MAIN_ID' => 0,
            'RECORD_VERSION' => 1,
            'RECORD_LAST' => 1,
            '#CREATE_DATE#' => 'NOW()'
        );

        if (empty($this->core->_prefix->table))
            $tbl_name = 'TBL_';
        else {
            $tbl_name = $this->core->_prefix->table;
        }

        $table_name = $tbl_name . $param['DOCUMENT_ID'];
        $param['RECORD_DOC_ID'] = $this->core->db_ATP_insert(PREFIX . 'ATP_' . $table_name, $values, 'replace');
        $param['RECORD_NR'] = $param['DOCUMENT_ID'] . 'p' . $param['RECORD_DOC_ID'];


        $documentManager = $this->core->factory->Document();
        $arr = array(
            'TABLE_FROM_ID' => $param['DOCUMENT_ID'],
            'TABLE_TO_ID' => $param['DOCUMENT_ID'],
            'IS_LAST' => 1,
            '#CREATE_DATE#' => 'NOW()',
            'DOC_STATUS' => null,
            'STATUS' => $documentManager->get($param['DOCUMENT_ID'])->getStatus(),
            'RECORD_ID' => $param['RECORD_NR']);

        $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_' . $table_name . '
			SET
				RECORD_ID = :record_id,
				RECORD_MAIN_ID = :record_id
			WHERE ID = :new_id', array(
            'new_id' => $param['RECORD_DOC_ID'],
            'record_id' => $param['RECORD_NR']));

        // Sukuria sąryšių įrašą
        $param['RECORD_REL_ID'] = $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD', $arr, 'replace');

        $this->core->db_query_fast(
            'INSERT INTO ' . PREFIX . 'ATP_RECORD_X_RELATION (`DOCUMENT_ID`, `RECORD_ID`, `RELATION_ID`)
				VALUES (:table_id, :tbl_id, :relation_id)', array(
            'table_id' => $param['DOCUMENT_ID'],
            'tbl_id' => $param['RECORD_DOC_ID'],
            'relation_id' => $param['RECORD_REL_ID']));

        $current_workers = $this->create_record_add_worker([
            'is_empty' => true,
            'new_r_id' => $param['RECORD_REL_ID'],
            'status' => array(),
            'clause' => null,
            'r_relations' => $r_relations,
            'r_path' => false,
            'table_from_id' => $arr['TABLE_FROM_ID'],
            'table_id' => $param['DOCUMENT_ID']
        ]);

        $arr = array(
            'WORKER_ID' => $param['WORKER_ID'],
            'RECORD_ID' => $param['RECORD_REL_ID'],
            'RECORD_NR' => $param['RECORD_NR']
        );
        $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD_UNSAVED', $arr);

        return $param['RECORD_NR'];
    }

    // Teisės tikrinamos prieš kviečiant metodą
    public function create_record($type, $param_arg = null, $recordNr = null)
    {
/*
$mylog = '';
$mylog .= '----- this ----';
$mylog .= var_Export($this, true);
$mylog .= '----- type ----';
$mylog .= var_export($type, true);
$mylog .= '----- param ----';
$mylog .= var_export($param_arg, true);
$mylog .= '----- recordNr ----';
$mylog .= var_export($recordNr, true);
$mylog .= '----- REQUEST ----';
$mylog .= var_export($_REQUEST, true);

 file_put_contents(GLOBAL_REAL_URL.'logs/add_worker'.date("YmdHis").'.txt', $mylog);
*/

		$tmpRecordParentId = false;

		if ($type === 'submit_path') {
            $this->core->debug = true;
        }
        $isPaid = (bool) $_POST['paid'];
        
        if (is_array($param_arg) === FALSE) {
            $param_arg = (array) $param_arg;
        }

        if (empty($this->_r_table_id) === TRUE || empty($this->_r_table_to_id) === TRUE) {
            return false;
        }

        // Iš kur dokumentas keliauja // ?
        $this->_r_table_from_id = $this->_r_table_id;


        $documentManager = $this->core->factory->Document();
        $valuesManager = new \Atp\Record\Values($this->core);
        $fieldsManager = $this->core->factory->Field();
        $workerManager = new \Atp\Record\Worker($this->core);
        /* @var $document \Atp\Entity\Document */
        $document = $documentManager->get($this->_r_table_id);

//        $recordNr = $this->_r_record_id;
//        $this->_r_record_id = $recordNr;

        // ? // TODO: Kam nors naudojama? // TODO: kuriant naują dokumentą, jis prasideda tyrimo stadijoje ar toje pačioje, kaip ankstesnis?
        if (empty($recordNr)) {
            //$this->_r_record_status = 2; // tiriamo statusas+
            // Pradinis statusas
            $this->_r_record_status = $document->getStatus();
			if (empty($this->_r_record_status)) {
				$this->_r_record_status = '1';
			}

//            $documentStatus = $document->getStatus();
//            $workerManager->getWorkerTypesByDocumentStatus($documentStatus);
        } else {
            // Gauna dabartinį statusą
            $r_relations = $this->core->logic2->get_record_relations([
                'RECORD_ID' => $recordNr,
                'IS_LAST' => 1
                ], true);
            $this->_r_record_status = $r_relations['STATUS'];
			if (empty($this->_r_record_status)) {
				$this->_r_record_status = '1';
			}
		}
        if(empty($this->_r_r_id)) {
		}
		
		
		if(!empty($this->_r_r_id)) {
            $record = $this->core->logic2->get_record_relation($this->_r_r_id);
            $this->_r_record_status = $record['STATUS'];
			if (empty($this->_r_record_status)) {
				$this->_r_record_status = '1';
			}
		}

        if ($type === 'submit_path') {
            if ($this->_r_path) {
                $this->_r_table_id = $this->_r_table_to_id;
                //$this->_r_record_from_id = $recordNr;
                $document = $documentManager->get($this->_r_table_id);
            }
        }

        // Kai "Pažeidimo duomenys SISP" perduodamas į "Pažeidimo duomenys"
        if ($type === 'submit_path') {
            if ((int) $this->_r_table_from_id === \Atp\Document::DOCUMENT_INFRACTION_SISP) {
                if ((int) $this->_r_table_id === \Atp\Document::DOCUMENT_INFRACTION) {
                    // "Pažeidimo tipas": "Parkavimo (rinkliavos) pažeidimas"
                    $this->_r_values['col_56'] = 100;
                    // "Liudytojas": "Pareigūnas"
                    $this->_r_values['col_50'] = 115;
                }
            }
        }


        if ($type === 'submit_path') {
            $data = $this->transform_data($this->_r_values, $this->_r_table_id);
        } else {
            $data = $this->transform_data($this->_r_values, $this->_r_table_id, $this->_r_r_id);
        }
        $values = $data['values'];
        $extra_values = $data['extra_values'];
        $files_fields = $data['files_fields'];
        $clause = $data['clause'];
        unset($data);

        
        $r_action = $type;

        $status = array();
        if (in_array($type, ['finish_investigation', 'finish_examination', 'finish_idle'], TRUE) === TRUE) {
            $status[$type] = true;

            if (in_array($type, array('finish_investigation', 'finish_idle'), TRUE) === TRUE) {
                $type = 'save';
            } else {
                $type = 'save_new';
                $status['new'] = TRUE;
            }
        }

        $temp_var = $this->core->logic2->get_record_table($this->_r_r_id);
        $recordMainId = $temp_var['RECORD_MAIN_ID'];
        unset($temp_var);
        if ($type === 'submit_path') {
            if ($this->_r_path) {
                $from_record = $this->core->logic2->get_record_table($this->_r_r_id, 'C.RECORD_MAIN_ID, A.RECORD_ID, A.ID RID');
                $recordMainId = $values['RECORD_MAIN_ID'] = $from_record['RECORD_MAIN_ID'];
                $type = 'save_new';
            }
        }

        if (empty($this->core->_prefix->table))
            $tbl_name = 'TBL_';
        else {
            $tbl_name = $this->core->_prefix->table;
        }

        $table_name = $tbl_name . $this->_r_table_id;
        switch ($type) {
            case 'save_new':
                $status['new'] = true;
            case 'save':
                // <editor-fold defaultstate="expanded" desc="Surenka pradinius duomenis">
                // Surenka pradinius duomenis
                if (empty($this->_r_r_id) === FALSE && $this->_r_path !== TRUE) {
                    $record = $this->core->logic2->get_record_relation($this->_r_r_id);
                    if (empty($record) === TRUE) {
                        trigger_error('Record not found', E_USER_ERROR);
                        exit;
                    }
                    $rid = $record['ID'];
                    $doc_status = $record['DOC_STATUS'];

                    // Ankščiau išsaugoti papildomi laukai
                    $extra_data = $valuesManager->getValuesByRecord($this->_r_r_id, false, true);
                    $data = $this->core->logic2->get_record_table($record);
                    if (empty($data) === TRUE) {
                        trigger_error('Record data not found', E_USER_ERROR);
                        exit;
                    }


                    // Jei kuriamas naujas įrašas, atnaujinama sukūrimo data ir nustatoma versija
                    if ($status['new'] === TRUE && $data !== FALSE) {
                        unset($data['ID'], $data['CREATE_DATE']);

                        // Jei tai ne paskutinė versija, tai gauna paskutinę
                        if ($data['RECORD_LAST'] !== '1') {
                            $last_record_table = $this->core->logic2->get_record_table($data['RECORD_ID']);
                            $data['RECORD_VERSION'] = $last_record_table['RECORD_VERSION'];
                            unset($last_record_table);
                        }

                        $data['RECORD_VERSION'] = ++$data['RECORD_VERSION'];
                        $data['#CREATE_DATE#'] = 'NOW()';
                    }


                    // <editor-fold defaultstate="expanded" desc="Jei pirmą kartą saugomas laikinas įrašas, atnaujinama sukūrimo data (1/2)">
                    // Jei pirmą kartą saugomas laikinas įrašas, atnaujinama sukūrimo data (1/2)
                    $result = $this->core->db_query_fast('
                        SELECT ID
						FROM ' . PREFIX . 'ATP_RECORD_UNSAVED
						WHERE
							RECORD_ID = :RECORD_ID', ['RECORD_ID' => $record['ID']]);
                    $count = $this->core->db_num_rows($result);
                    if ($count) {
                        // Dokumento lentelėje
                        unset($data['CREATE_DATE']);
                        $data['#CREATE_DATE#'] = 'NOW()';
                    }// </editor-fold>

                    unset($record);
                } else {
                    $extra_data = array();
                    $data = array(
                        'RECORD_MAIN_ID' => 0,
                        'RECORD_VERSION' => 1,
                        'RECORD_LAST' => 1,
                        '#CREATE_DATE#' => 'NOW()'
                    );
                }// </editor-fold>
                //
                // <editor-fold defaultstate="expanded" desc="Sujungia pradinius ir naujus duomenis">
                // Sujungia pradinius ir naujus duomenis.
                // Jei baigiama tarpinė būsena, tai pakeitimai nesaugomi.
                if (isset($status['finish_idle']) && $status['finish_idle'] === TRUE) {
                    $values = $data;
                    $extra_values = $extra_data;
                } else {
                    $values = array_merge((array) $data, $values);
                }// </editor-fold>


                // <editor-fold defaultstate="expanded" desc="Pašalinami dokumente neegzistuojantys laukai. Tarkim kai dokumento administravime pašalinamas laukas, bet kuriant naują dokumento versiją senoje versijoje laukas vis dar egzistuoja.">
                $documentFields = $fieldsManager->getFieldsMapByDocument($document->getId(), 'assoc');
                foreach ($values as $fieldName => $_) {
                    if (strpos($fieldName, $this->core->_prefix->column) === false) {
                        continue;
                    }
                    if (isset($documentFields[$fieldName]) === false) {
                        unset($extra_data[$fieldName]);
                    }
                }
                foreach ($extra_data as $fieldName => $_) {
                    if (isset($documentFields[$fieldName]) === false) {
                        unset($extra_data[$fieldName]);
                    }
                }// </editor-fold>

                
                // <editor-fold defaultstate="expanded" desc="Failų saugojimas (1/2 dalis) [Failai iš formos laukų]">
                // Failų saugojimas (1/2 dalis)
                // Failai iš formos laukų
                if (empty($files_fields) === FALSE) {
                    foreach ($files_fields as $fieldId => $files) {
                        /* @var $field \Atp\Entity\Field */
                        $field = $fieldsManager->getField($fieldId);
                        if(!$field) {
                            throw new \Exception('Form field not found');
                        }
                        
                        foreach ($files['name'] as $key => $name) {
                            // Klaida
                            if ($files['error'][$key] !== 0) {
                                // neprisegtas joks failas
                                if ($files['error'][$key] == 4) {
                                    continue;
                                }

                                if (in_array('disable_messages', $param_arg) === FALSE) {
                                    $this->core->set_message('error', 'Nepavyko įkelti failo: ' . $name);
                                }
                                continue;
                            }

                            // Įkelia failą
                            $info = $this->core->action->uploadFile(array(
                                'name' => $files['name'][$key],
                                'type' => $files['type'][$key],
                                'size' => $files['size'][$key],
                                'error' => $files['error'][$key],
                                'tmp_name' => $files['tmp_name'][$key],
                            ));

                            // Sukuria failo įrašą
                            if ($info) {
                                $fileId = $this->core->action->saveFile(array(
                                    'TYPE' => $info['type'],
                                    'SIZE' => $info['size'],
                                    'NAME' => $info['filename'],
                                    'PATH' => $info['path']
                                ));
//                                $this->_r_files[$field_id][] = $fileId;
                                $this->_r_files[$field->getId()][] = $fileId;

                                // Inforamcija apie formos lauką
//                                $field = $this->core->logic2->get_fields(array('ID' => $field_id))
//                                $field = $this->core->logic2->get_fields(['ID' => $field->getId()]);

                                // Nurodoma, kad laukas turi failų
                                $pos = strpos($field->getName(), $this->core->_prefix->column);
                                if ($pos === FALSE) {
                                    continue;
                                }

                                $reference = &$values;
                                if ($pos > 0) {
                                    $reference = &$extra_values;
                                }

                                $reference[$field->getName()] = 1;
                            }
                        }
                    }
                }// </editor-fold>
                // Sujungia pradinius papildomus ir naujus duomenis
                $extra_values = array_merge((array) $extra_data, (array) $extra_values);

                if ((isset($status['finish_investigation']) && $status['finish_investigation'] === TRUE) || (isset($status['finish_idle']) && $status['finish_idle'] === TRUE)) {
                    $exp_record_id = explode('p', $recordNr);
                    $values['ID'] = $exp_record_id[1];
                    $status['new'] = FALSE;
                    $rid = $this->_r_r_id;
                    
                    $result = $this->core->db_query_fast('
                        SELECT MAX(RECORD_VERSION) AS version
                        FROM ' . PREFIX . 'ATP_' . $table_name . '
                        WHERE
                            RECORD_ID = :RECORD_ID', array('RECORD_ID' => $recordNr));
                    if ($this->core->db_num_rows($result) === 1) {
                        $row = $this->core->db_next($result);
                        $values['RECORD_VERSION'] = $row['version'];
                    } else {
                        $values['RECORD_VERSION'] = 1;
                    }
                } elseif ($this->_r_table_to_id == \Atp\Document::DOCUMENT_PROTOCOL && $r_action === 'submit_path') {
                    $result = $this->core->db_query_fast('
                        SELECT MAX(RECORD_VERSION) AS version
                        FROM ' . PREFIX . 'ATP_' . $table_name . '
                        WHERE
                            RECORD_MAIN_ID = :RECORD_MAIN_ID', array('RECORD_MAIN_ID' => $values['RECORD_MAIN_ID']));
                    if ($this->core->db_num_rows($result) === 1) {
                        $row = $this->core->db_next($result);
                        $values['RECORD_VERSION'] = $row['version'];
                    }
                    $values['RECORD_VERSION'] ++;
                }

                // <editor-fold defaultstate="expanded" desc="Išsaugo laukų reikšmes">
                // Išsaugo dokumento įrašo duomenis (1/2 dalis)
                $values = array_map(function($item) {
                    return (string) $item;
                }, $values);
                $new_id = $this->core->db_ATP_insert(
                    PREFIX . 'ATP_' . $table_name, $values, 'replace'
                );

                // Allocate RECORD
                if (empty($values['ID'])) {
                    $allocatedRecordId = $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD', []);
                } else {
                    $allocatedRecordId = $this->_r_r_id;
                }
                
                $extra_rows = array();
                if (empty($extra_values) === FALSE) {
                    // TODO: $extra_rows, kai tenkina $status['finish_idle'] === TRUE
                    foreach ($extra_values as $name => $val) {

                        // Kad tiesioginių dokumento laukų nesaugotu kaip papildomų.
                        if(strpos($name, $this->core->_prefix->column) === 0) {
                            continue;
                        }

                        $data = array();
                        $data['ID'] = null;
//                        $data['FIELD_ID'] = $this->get_extra_field_id_by_name($name);
                        $data['FIELD_ID'] = $fieldsManager->getFieldByName($document->getId(), $name)->getId();
                        $data['RECORD_ID'] = $allocatedRecordId;
                        $data['VALUE'] = $val;
                        $extra_rows[] = $data;
                    }
                }

                // <editor-fold defaultstate="expanded" desc="Web-servisu gauti laukai">
                // Web-servisu gauti laukai

                $fields = $fieldsManager->getFieldsByDocument($this->_r_table_id);
                foreach ($fields as $field) {
                    if ($field->getWs()) {
                        if (isset($this->_r_values['ws_field'][$field->getId()]) === false) {
                            $this->_r_values['ws_field'][$field->getId()] = [];
                        }
                    }
                }

				if (empty($tmpRecordParentId) == TRUE) {
					$pfield_row = false;
					$pfield_result = false;
					$pfield_qry = "SELECT * FROM ".PREFIX."ATP_RECORD WHERE ID = '".$allocatedRecordId."'";
					$pfield_result = $this->core->db_query_fast($pfield_qry, array());
		            $pfield_row = $this->core->db_next($pfield_result);
					$tmpRecordParentId = $pfield_row['RECORD_FROM_ID'];
					if (empty($tmpRecordParentId) == TRUE) {
						$tmpRecordParentId = $_POST['record_id'];
					}
					if (empty($tmpRecordParentId) == TRUE) {
						$tmpRecordParentId = $from_record['RID'];
					}
					if (empty($tmpRecordParentId) == TRUE) {
						$tmpRecordParentId = $rid;
					}
//					file_put_contents(GLOBAL_REAL_URL . '/logs/reg_case.php', 'parent: ' .$pfield_qry.var_export($pfield_row, true).' - '.$tmpRecordParentId. PHP_EOL, FILE_APPEND);
//					file_put_contents(GLOBAL_REAL_URL . '/logs/reg_case.php', 'POST: '.var_export($_POST, true). PHP_EOL, FILE_APPEND);
				}				

                if (empty($this->_r_values['ws_field']) === FALSE) {
//                    // Jei magic_quotes_gpc įšjungtas lokaliai, bet įjungtas globaliai, tai visteik naudoja globalų nustatymą
//                    $php_ini = ini_get_all();
//                    if (isset($php_ini['magic_quotes_gpc']) === TRUE && in_array(strtolower($php_ini['magic_quotes_gpc']['global_value']), array(
//                            'on', '1')) === TRUE) {
//                        $ws_field = stripslashes($this->_r_values['ws_field']);
//                    } else {
//                        $ws_field = $this->_r_values['ws_field'];
//                    }
//                    $ws_field = json_decode($ws_field, true);
//                    /*
//                      Formatas:
//                      [ 0: { 1948: { 1955: '348646348', 1956: 'VALDAS', 1957: 'PUČKIS' } },
//                      1: { 1949: { 1958: '13246687', 1959: 'UAB "Plytos"' } } ]
//                     */
//                    foreach ($ws_field as $inputField) {
//                        $dataFields = current($inputField);
//                        $inputFieldId = key($inputField);
                    $ws_field = $this->_r_values['ws_field'];
                    foreach ($ws_field as $inputFieldId => $dataFields) {

                        $fieldsWsXRef = $this->core->logic2->get_webservices([
                            'PARENT_FIELD_ID' => $inputFieldId
                        ]);

                        foreach ($fieldsWsXRef as $fieldXRef) {
                            $fieldId = $fieldXRef['FIELD_ID'];
                            if (isset($dataFields[$fieldId]) === false) {
                                $dataFields[$fieldId] = null;
                            }
                        }

                        foreach ($dataFields as $fieldId => $value) {
                            $data = array();
                            $data['ID'] = null;
                            $data['FIELD_ID'] = $fieldId;
                            $data['RECORD_ID'] = $allocatedRecordId;
                            $data['VALUE'] = $value;

							if (empty($value) == TRUE) {
								$xvalue_result = false;
								$xvalue_row = false;
								$xfield_result = false;
								$xfield_row = false;

								$xfield_qry = "SELECT A.*, B.WS_NAME FROM ".PREFIX."ATP_FIELD A 
								INNER JOIN ".PREFIX."ATP_WEBSERVICES_FIELDS B ON A.ITEM_ID = B.FIELD_ID 
								WHERE A.ID = '".$fieldId."'";
//								file_put_contents(GLOBAL_REAL_URL . '/logs/reg_case.php', 'query: ' .$xfield_qry. PHP_EOL, FILE_APPEND);
								$xfield_result = $this->core->db_query_fast($xfield_qry, array());
					            $xfield_row = $this->core->db_next($xfield_result);
//								file_put_contents(GLOBAL_REAL_URL . '/logs/reg_case.php', 'val1: ' .var_export($xfield_row, true). PHP_EOL, FILE_APPEND);
								$check_for_personcode_field = false;
								if ($xfield_row['WS_NAME'] == 'JAR' OR $xfield_row['WS_NAME'] == 'GRT' OR $xfield_row['WS_NAME'] == 'SDR') {
									$check_for_personcode_field = strpos(strtolower($xfield_row['LABEL']), 'asmens kodas');
								}
								if ($xfield_row['ITYPE'] == 'WS' AND $xfield_row['VISIBLE'] == '1' AND $check_for_personcode_field === FALSE) {
									$xvalue_qry = "SELECT * FROM ".PREFIX."ATP_FIELDS_VALUES WHERE VALUE != '' AND RECORD_ID = '".$tmpRecordParentId."' AND FIELD_ID IN (
									SELECT TA.ID FROM ".PREFIX."ATP_FIELD TA 
									INNER JOIN ".PREFIX."ATP_FIELD TB ON TA.LABEL = TB.LABEL AND TB.ID = '".$fieldId."' AND TA.ITYPE = 'WS' AND TB.ITYPE = 'WS')";
//									file_put_contents(GLOBAL_REAL_URL . '/logs/reg_case.php', 'query: ' .$xvalue_qry. PHP_EOL, FILE_APPEND);
									$xvalue_result = $this->core->db_query_fast($xvalue_qry, array());
						            $xvalue_row = $this->core->db_next($xvalue_result);
//									file_put_contents(GLOBAL_REAL_URL . '/logs/reg_case.php', 'val2: ' .var_export($xvalue_row, true). PHP_EOL, FILE_APPEND);
									if (empty($xvalue_row['VALUE']) === FALSE) {
										$data['VALUE'] = $xvalue_row['VALUE'];
									}
								}
							}
							$extra_rows[] = $data;
                        }
                    }
                }// </editor-fold>
                //

				// Išsaugo laukus į failo kešą, tam kad rodyti sąrašuose
				$cacheArray = array();
				$wsCacheFile = GLOBAL_REAL_URL.'subsystems/atp/files/wscache/'.$extra_rows[0]['RECORD_ID'].'.txt';
				foreach ($extra_rows as $key => $val) {
					$cacheArray['data'][$extra_rows[$key]['FIELD_ID']] = $extra_rows[$key]['VALUE'];
				}
				file_put_contents($wsCacheFile, json_encode($cacheArray));
				
				// Išsaugo dokumento įrašo duomenis (2/2 dalis)
                if (count($extra_rows) > 0 && $status['finish_investigation'] !== TRUE && $status['finish_idle'] !== TRUE)
                    $this->core->db_ATP_insert(
                        PREFIX . 'ATP_FIELDS_VALUES', $extra_rows, 'replace'
                    );
                // </editor-fold>
                // 
                // Surenka duomenis sąryšio sukūrimui
                if (empty($recordNr) === TRUE || $status['new'] === TRUE || $status['finish_investigation'] === TRUE || $status['finish_idle'] === TRUE) {
                    //$uprivileges_data = array('table_id' => $this->_r_table_id, 'user_id' => $this->core->factory->User()->getLoggedUser()->person->id, 'department_id' => $this->core->factory->User()->getLoggedPersonDuty()->department->id);
                    $arr = array(
                        'TABLE_FROM_ID' => $this->_r_table_from_id,
                        'TABLE_TO_ID' => $this->_r_table_to_id,
                        'IS_LAST' => 1,
                        '#CREATE_DATE#' => 'NOW()',
                        'DOC_STATUS' => isset($this->_r_values['document_status']) ? $this->_r_values['document_status'] : null);

                    if (isset($status['new']) && $status['new'] === TRUE && $this->_r_path !== TRUE) {
                        // jei nauja versija
                        $arr['STATUS'] = $r_relations['STATUS'];
//						if ($arr['STATUS'] == '0') {
//							$arr['STATUS'] = '2';
//						}
						$arr['RECORD_ID'] = $r_relations['RECORD_ID'];
                    } else {
                        // jei nauja versijai, kai yra perduodama į kitą dokumentą
                        unset($arr['DOC_STATUS']);

                        $arr['STATUS'] = $document->getStatus();
//						if ($arr['STATUS'] == '0') {
//							$arr['STATUS'] = '2';
//						}
                        $arr['RECORD_ID'] = $this->_r_table_id . 'p' . $new_id;
                    }

                    // Baigus protokolo ar nutarimo tyrima, prideti nauja irasa i tvarkarasty,
                    // jei nebuvo apmoketa
                    $idle_days = $this->get_document_status_idle_days($this->_r_table_id);
                    if ($status['finish_investigation'] === true && ($this->_r_table_id == \Atp\Document::DOCUMENT_PROTOCOL || $this->_r_table_id == \Atp\Document::DOCUMENT_CASE) && $idle_days !== false && $isPaid !== TRUE) {
                        $sch = prepare_schedule();
                        $events = $sch->logic->getEventsBySoft(array('ITEM' => $recordNr,
                            'ITEM_TYPE' => 'expire'));
                        if (count($events) === 0) {
                            $worker = $this->core->logic2->get_record_worker(array(
                                'F.RECORD_ID = "' . $recordNr . '"',
                                'A.TYPE = "' . 'invest' . '"'
                                ), true
                            );

                            $prefix = parse_url($this->core->config['GLOBAL_SITE_URL'], PHP_URL_PATH);
                            $url = $prefix . 'subsystems/atp/index.php?m=5&id=' . $recordNr;
                            $current_document = $this->core->logic2->get_document(array(
                                'ID' => $this->_r_table_id));

                            $from = date('Y-m-d', time() + $idle_days * (24 * 60 * 60));

                            $event_id = $sch->logic->saveEvent(array(
                                'FROM' => $from,
                                'TILL' => $from,
                                'NAME' => 'ATP: Kontroliuoti AN',
                                'DETAILS' => 'ATP: Kontroliuoti AN.<br/>'
                                . 'Dokumentas: ' . $current_document['NAME'] . '<br/>'
                                . 'Adresas: <a href="' . $url . '">' . $recordNr . '</a>'
                                ), $worker['PEOPLE_ID'], array(
                                'SOFT' => 'atp',
                                'TYPE' => 'expire',
                                'ITEM' => $recordNr
                                ), $this->core->factory->User()->getLoggedUser()->person->id
                            );
                        }
                    }

                    // Pakeičia tipą į nagrinėjimą
                    if ((isset($status['finish_investigation']) && $status['finish_investigation'] === TRUE) || (isset($status['finish_idle']) && $status['finish_idle'] === TRUE)) {
//						unset($arr['DOC_STATUS']);
                        // $t duomenys yra $this->logic->table
                        //$t = $this->core->logic2->get_document($this->_r_table_id);
                        
                        if ($document->getCanIdle() && $status['finish_idle'] !== TRUE) {
                            $arr['STATUS'] = \Atp\Record::STATUS_IDLE;
                        } else {
                            // Nagrinėjimo statusas
                            $arr['STATUS'] = \Atp\Record::STATUS_EXAMINING;

                            // Jei tyrėjas neturi teisės nagrinėti, suranda laisviausią nagrinėtoją ir jam priskiria dokumento įrašą
                            if ($status['finish_idle'] !== TRUE) {

                            } else {

                                // Jei bauda apmokėta - įrašui priskiriamas finished (Baigtas) statusas, pašalinamas nagrinėtojas ir įrašas niekur nesiunčiamas
                                if ($isPaid === TRUE) {
                                    $arr['STATUS'] = \Atp\Record::STATUS_FINISHED;


                                    $workers = $this->core->logic2->get_record_worker(array(
                                        'A.RECORD_ID' => $rid
                                    ));
                                    if (empty($workers) === FALSE) {
                                        $workerManager = new \Atp\Record\Worker($this->core);
                                        foreach ($workers as &$worker) {
                                            if (in_array($worker['TYPE'], array(
                                                    'exam', 'next'), true) === TRUE) {
                                                $workerManager->removeRecordWorker('ID=' . (int) $worker);
                                            }
                                        }
                                    }
                                } else {

                                }

                                //$arr['IS_PAID'] = $isPaid;
                            }
                        }
                    }
                    // Pakeičia tipą į baigta // TODO: persųsti, kam?
                    elseif (isset($status['finish_examination']) && $status['finish_examination'] === TRUE) {
                        unset($arr['DOC_STATUS']);
                        $arr['STATUS'] = \Atp\Record::STATUS_EXAMINED;
                    }
                }


                // Jei perduodama į kitą dokumentą
                if ($this->_r_path === TRUE) {
                    unset($arr['DOC_STATUS']);
                    $arr['RECORD_FROM_ID'] = $from_record['RID'];
//                    $recordNr = $this->_r_record_id = $arr['RECORD_ID'];
                    $recordNr = $arr['RECORD_ID'];


                    // Atnaujina RECORD_ID
                    $args = array(
                        'ID' => $new_id,
                        'RECORD_ID' => $recordNr);
                    $this->core->db_query_fast('UPDATE
							' . PREFIX . 'ATP_' . $table_name . '
						SET
							RECORD_ID = :RECORD_ID
 						WHERE ID = :ID', $args);


                    // Sukuria sąryšių įrašą
                    $arr['ID'] = $allocatedRecordId;
                    $new_r_id = $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD', $arr, 'replace');
                    $new_r_id = $allocatedRecordId;
                    $this->_r_r_id = $new_r_id;


                    // UPDATE old versions of same document
                    $this->core->db_query_fast('
                        UPDATE
                            ' . PREFIX . 'ATP_RECORD A
                                INNER JOIN ' . PREFIX . 'ATP_RECORD_X_RELATION B ON
                                    B.RELATION_ID = A.ID AND
                                    B.DOCUMENT_ID = "' . $arr['TABLE_TO_ID'] . '" # just in case
                                INNER JOIN ' . PREFIX . 'ATP_' . $table_name . ' C ON
                                    C.ID = B.RECORD_ID
                        SET
                            A.IS_LAST = 0,
                            C.RECORD_LAST = 0
                        WHERE
                            C.RECORD_MAIN_ID = "' . $recordMainId . '" AND
                            C.RECORD_ID != "' . $arr['RECORD_ID'] . '"
                    ');
                    
                    if (in_array('disable_messages', $param_arg) === false) {
                        $this->core->set_message('success', 'Iš ' . '"<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $from_record['RID'] . '">' . $from_record['RECORD_ID'] . '</a>"' . ' sukurtas naujas dokumentas: "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $recordNr . '</a>"');
                    }
                    $redirect_url = $this->core->config['SITE_URL'] . 'index.php?m=5&id=' . $this->_r_r_id;
                } else
                // Jei buvo įrašyti nauji duomenys
                if (empty($recordNr) === TRUE) {
                    // <editor-fold defaultstate="expanded" desc="Jei buvo įrašyti nauji duomenys">
//                    $recordNr = $this->_r_record_id = $arr['RECORD_ID'];
                    $recordNr = $arr['RECORD_ID'];

                    // Atnaujina RECORD_ID
                    $args = array(
                        'ID' => $new_id,
                        'RECORD_ID' => $recordNr
                    );
                    $this->core->db_query_fast('UPDATE
							' . PREFIX . 'ATP_' . $table_name . '
						SET
							RECORD_ID = :RECORD_ID,
							RECORD_MAIN_ID = :RECORD_ID
						WHERE ID = :ID', $args);

                    $arr['DOC_STATUS'] = isset($this->_r_values['document_status']) ? $this->_r_values['document_status'] : 2;
                    // Sukuria sąryšių įrašą
                    $arr['ID'] = $allocatedRecordId;
                    $new_r_id = $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD', $arr, 'replace');
                    $new_r_id = $allocatedRecordId;
                    $this->_r_r_id = $new_r_id;

                    if (in_array('disable_messages', $param_arg) === FALSE)
                        $this->core->set_message('success', 'Sukurtas naujas dokumentas: <a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $recordNr . '</a>'); // </editor-fold>
                }
                else
                // Jei saugoma, kaip nauja versija
                if ($status['new'] === TRUE) {
                    // <editor-fold defaultstate="expanded" desc="Jei saugoma, kaip nauja versija">
                    // Senus įrašus nurodo kaip nebe paskutinius
                    $args = array(
                        'RECORD_ID' => $recordNr,
                        'ID' => $new_id
                    );
                    $this->core->db_query_fast('
                        UPDATE
							' . PREFIX . 'ATP_' . $table_name . '
						SET RECORD_LAST = 0
						WHERE
							RECORD_ID = :RECORD_ID AND
							RECORD_LAST = 1 AND
							ID != :ID', $args);
                    $this->core->db_query_fast('
                        UPDATE
							' . PREFIX . 'ATP_RECORD
						SET IS_LAST = 0
						WHERE
							RECORD_ID = :RECORD_ID AND
							IS_LAST = 1', $args);

                    $arr['RECORD_FROM_ID'] = $rid;

                    if (empty($this->_r_values['document_status'])) {
                        $arr['DOC_STATUS'] = $doc_status;
                    } else {
                        $arr['DOC_STATUS'] = $this->_r_values['document_status'];
                    }
//					if (empty($arr['DOC_STATUS'])) {
//						$arr['DOC_STATUS'] = '2';
//					}

//                    $recordNr = $this->_r_record_id = $arr['RECORD_ID'];
                    $recordNr = $arr['RECORD_ID'];

                    // Sukuria sąryšių įrašą
                    $arr['ID'] = $allocatedRecordId;
                    $new_r_id = $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD', $arr, 'replace');
                    $new_r_id = $allocatedRecordId;
                    $this->_r_r_id = $new_r_id;

                    if ($status['finish_investigation'] === TRUE || $status['finish_idle'] === TRUE) {
                        if ((int) $arr['STATUS'] === \Atp\Record::STATUS_IDLE) {
                            if (in_array('disable_messages', $param_arg) === FALSE)
                                $this->core->set_message('success', 'Dokumentas "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $recordNr . '</a>" perkeltas į tarpinę būseną');
                        } elseif ($isPaid === TRUE && (int) $arr['STATUS'] === \Atp\Record::STATUS_FINISHED) {
                            if (in_array('disable_messages', $param_arg) === FALSE)
                                $this->core->set_message('success', 'Dokumento "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $recordNr . '</a>" nagrinėjimas baigtas');
                        } else {
                            if (in_array('disable_messages', $param_arg) === FALSE)
                                $this->core->set_message('success', 'Dokumentas "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $recordNr . '</a>" perduotas nagrinėjimui');
                        }
                    } elseif ($status['finish_examination'] === TRUE) {
                        if (in_array('disable_messages', $param_arg) === FALSE)
                            $this->core->set_message('success', 'Dokumento "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $recordNr . '</a>" nagrinėjimas baigtas');
                    } else {
                        if (in_array('disable_messages', $param_arg) === FALSE)
                            $this->core->set_message('success', 'Išsaugota nauja dokumento "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $recordNr . '</a>" versija');
                    }

                    $redirect_url = $this->core->config['SITE_URL'] . 'index.php?m=5&record_id=' . $this->_r_r_id; // </editor-fold>
                } else {
                    $param = array();

                    // Jei pirmą kartą saugomas laikinas įrašas, atnaujinama sukūrimo data (2/2)
                    $qry = 'SELECT ID
						FROM ' . PREFIX . 'ATP_RECORD_UNSAVED
						WHERE
							RECORD_ID = :RECORD_ID';
                    $result = $this->core->db_query_fast($qry, array('RECORD_ID' => $rid));
                    $count = $this->core->db_num_rows($result);
                    if ($count) {
                        $param['CREATE_DATE'] = date('Y-m-d H:i:s');
                    }

                    if (empty($this->_r_values['document_status'])) {
                        $param['DOC_STATUS'] = $doc_status;
                    } else {
                        $param['DOC_STATUS'] = $this->_r_values['document_status'];
                    }
//					if (empty($param['DOC_STATUS'])) {
//						$param['DOC_STATUS'] = '2';
//					}

                    if ($status['finish_investigation'] === TRUE || $status['finish_idle'] === TRUE) {
                        $param['STATUS'] = $arr['STATUS'];
                        $redirect_url = $this->core->config['SITE_URL'] . 'index.php?m=5&record_id=' . $this->_r_r_id;
                    }

                    $this->core->db_quickupdate(PREFIX . 'ATP_RECORD', $param, $rid, 'ID');
                    if (in_array('disable_messages', $param_arg) === FALSE)
                        $this->core->set_message('success', 'Dokumentas "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $recordNr . '</a>" atnaujintas');
                }

                // TODO: WHAAAAT?!
                // Nes registruojant dokumentą per webservisus, nebuna automatiškai sukurto tuščio dokumento
				$r_relations = array();
				$r_relations = $this->core->logic2->get_record_relations(array($this->_r_r_id), true);


                // TODO: naudoti \Atp\Document\Record\Payment::SetRecordPaymentOption() // Ar papildomas veiksmas nieko negadins? (t.y. versijavimas)
                // Apmokėjimo atnaujinimas // Jei pažymėta, kad baigtas
                if ((int) $arr['STATUS'] === \Atp\Record::STATUS_FINISHED) {
                    $payment_status = \Atp\Document\Record\Payment::STATUS_NOT_PAID;
                    if ($isPaid === TRUE) {
                        $payment_status = \Atp\Document\Record\Payment::STATUS_PAID;
                    }
                    $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD_PAID', array(
                        'RECORD_ID' => $this->_r_r_id, 'PAYMENT_STATUS' => $payment_status), 2);
                }


                // <editor-fold defaultstate="expanded" desc="Failų saugojimas (2/2 dalis) [Sukuria išsaugotų failų ryšius su dokumento įrašu]">
                // Failų saugojimas (2/2 dalis)
                // Sukuria išsaugotų failų ryšius su dokumento įrašu
                if (isset($this->_r_files) && count($this->_r_files)) {
                    /* if (empty($new_r_id) === TRUE) {
                      $record = $this->core->logic2->get_record_relation($this->_r_r_id);
                      $record_id = $record['ID'];
                      } else {
                      $record_id = $new_r_id;
                      } */

                    $rfile = new \Atp\File\RecordFilesHandler($this->core);
                    $record_id = $this->_r_r_id;
                    foreach ($this->_r_files as $field_id => &$ids) {
                        foreach ($ids as &$id) {
                            /* $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD_FILES', array(
                              'FILE_ID' => $id,
                              'RECORD_ID' => $record_id,
                              'FIELD_ID' => $field_id
                              )); */
                            $rfile->save((object) array(
                                    'id' => $id,
                                    'record_id' => $record_id,
                                    'field_id' => $field_id
                            ));
                        }
                    }
                }// </editor-fold>


				$r_relations = array();
				$r_relations = $this->core->logic2->get_record_relations(array($this->_r_r_id), true);

                /**
                 * Sukuria ryšį tarp dokumento įrašo duomenų ATP_TBL_[0-9]+ ir ATP_RECORD_RELATION
                 */
                $this->core->db_query_fast(
                    'INSERT INTO ' . PREFIX . 'ATP_RECORD_X_RELATION (`DOCUMENT_ID`, `RECORD_ID`, `RELATION_ID`) VALUES (:DOCUMENT_ID, :RECORD_ID, :RELATION_ID)', array(
                    'DOCUMENT_ID' => $this->_r_table_id, 'RECORD_ID' => $new_id,
                    'RELATION_ID' => $this->_r_r_id));

                if (empty($this->_r_r_id) === FALSE) {
                    $current_workers = $this->create_record_add_worker(array(
                        'new_r_id' => $this->_r_r_id,
                        'status' => $status,
                        'clause' => $clause,
                        '	' => $r_relations,
                        'r_path' => $this->_r_path,
                        'table_from_id' => $this->_r_table_from_id,
                        'table_id' => $this->_r_table_id));
                } elseif (empty($this->_r_r_id) && empty($recordNr) === FALSE) {
                    die('This wasn\'t supposed to happen');
                    $record = $this->core->logic2->get_record_relation($recordNr);
                    $current_workers = $this->create_record_add_worker(array(
                        'new_r_id' => $record['ID'],
                        'status' => $record['STATUS'],
                        'clause' => $clause,
                        'r_relations' => $r_relations,
                        'r_path' => $this->_r_path,
                        'table_from_id' => $this->_r_table_from_id,
                        'table_id' => $this->_r_table_id));
                }


                if ($this->_r_table_id == \Atp\Document::DOCUMENT_CASE) {
                    $current_record = $this->core->logic2->get_record_relation($this->_r_r_id);
                    if (empty($current_record['RECORD_FROM_ID']) === FALSE) {
                        $previous_record = $this->core->logic2->get_record_relation($current_record['RECORD_FROM_ID']);

                        if ((int) $previous_record['TABLE_TO_ID'] !== (int) $current_record['TABLE_TO_ID']) {
                            $current_person = $this->core->logic2->get_record_worker(array(
                                'A.RECORD_ID = ' . (int) $current_record['ID']), true, null, 'A.ID, A.WORKER_ID, A.TYPE');

                            $previous_person = $this->core->logic2->get_record_worker(array(
                                'A.TYPE IN ("exam","next")',
                                'A.RECORD_ID = ' . (int) $previous_record['ID']), true, null, 'A.ID, A.WORKER_ID, A.TYPE');

                            if ((int) $current_person['WORKER_ID'] !== (int) $previous_person['WORKER_ID']) {
                                if (empty($current_person['ID'])) {
                                    try {
                                        throw new \Exception('Missing record - worker relation id.');
                                    } catch (\Exception $e) {
                                        error_log($e);
                                    }
                                }

                                if (empty($current_person['ID']) === false) {
                                    $this->core->db_query_fast('
                                        UPDATE
                                            ' . PREFIX . 'ATP_RECORD_X_WORKER
                                        SET
                                            WORKER_ID = ' . (int) $previous_person['WORKER_ID'] . '
                                        WHERE
                                            ID = ' . $current_person['ID']);
                                }
                            }
                        }
                    }
                }

                // pašalina laikino įrašo žymą
                if (empty($this->_r_r_id) === FALSE) {
                    $this->core->db_query_fast('DELETE
						FROM ' . PREFIX . 'ATP_RECORD_UNSAVED
						WHERE
							WORKER_ID = :WORKER_ID AND
							#RECORD_NR = :RECORD_NR
							RECORD_ID = :RECORD_ID', [
                        'WORKER_ID' => $this->core->factory->User()->getLoggedPersonDuty()->id,
                        'RECORD_ID' => $this->_r_r_id
                    ]);
                }

                // Lentelės įrašą tiesiogiai suriša su įrašo "sąryšio" lentelės įrašu
                if (empty($this->_r_r_id) === FALSE) {
                    /*
                     * Automatinis laukų užpildymas
                     */
                    $current_document = $this->core->logic2->get_document(['ID' => $this->_r_table_id]);
                    // Visų dokumento įrašo laukų reikšmės
//                    $record_values = $this->get_fields_values_by_type_and_id2(['RECORD' => $this->_r_r_id]);
                    $record_values = $valuesManager->getValuesByRecord($this->_r_r_id, false, true, true);

                    // Tesiama tik tada, kai nagrinėtojo vardo ir pavardės laukas nėra užpildytas
                    $set_examinator_fields = false;

                    $examinator_field_id = $documentManager->getOptions($current_document['ID'])->examinator;
                    if (empty($examinator_field_id) === FALSE) {
                        $field = $this->core->logic2->get_fields(array('ID' => $examinator_field_id));
                        // Praleidžia, jei dokumentas neturi tokio lauko
                        if (array_key_exists($field['NAME'], $record_values) === TRUE) {
                            // Praleidžia, jei dokumento laukas jau yra užpildytas
                            $record_values[$field['NAME']] = trim($record_values[$field['NAME']]);
                            if (empty($record_values[$field['NAME']]) === TRUE) {
                                $set_examinator_fields = true;
                            }
                        }
                    }

                    if ($set_examinator_fields === TRUE) {
                        // Numatytas nagrinėtojas
//                        $examinator = $this->core->logic2->get_record_worker([
//                            'A.TYPE IN("exam", "next")',
//                            'A.RECORD_ID = ' . (int) $this->_r_r_id], true, null, 'A.ID, A.WORKER_ID');
                        $workerManager = new \Atp\Record\Worker($this->core);

                        $examinator = $workerManager->getRecordWorker([
                            'A.TYPE IN("exam", "next")',
                            'A.RECORD_ID = ' . (int) $this->_r_r_id
                            ], [
                            'LIMIT' => true,
                            'SELECT' => 'A.ID, A.WORKER_ID'
                            ]
                        );

                        if (empty($examinator) === FALSE) {
                            // Surenka nagrinėtojo informaciją
//                            $fields = 'A.ID,B.FIRST_NAME, B.LAST_NAME,
//								A.ROOM, B.PHONE_MOBILE, B.PHONE_FULL, B.EMAIL,
//								A.STRUCTURE_ID, C.PARENT_ID as "STRUCTURE_PARENT_ID", C.SHOWS AS "STRUCTURE_NAME",
//								C.TYPE_ID as "STRUCTURE_TYPE_ID"';
//                            $examinator = $this->core->logic2->get_worker_info(array(
//                                'A.ID = ' . (int) $examinator['WORKER_ID']), true, null, $fields);

                            $examinator = $workerManager->getWorkerInfo([
                                'A.ID = ' . (int) $examinator['WORKER_ID']
                                ], [
                                'LIMIT' => true,
                                'SELECT' => 'A.ID,B.FIRST_NAME, B.LAST_NAME,
                                    A.ROOM, B.PHONE_MOBILE, B.PHONE_FULL, B.EMAIL,
                                    A.STRUCTURE_ID, C.PARENT_ID as "STRUCTURE_PARENT_ID", C.SHOWS AS "STRUCTURE_NAME",
                                    C.TYPE_ID as "STRUCTURE_TYPE_ID"'
                                ]
                            );

                            // Suformuoja dokumento įrašo laukų reikšmes
                            $fields_data = [];

                            // Kai tyrėjas priskirtas poskyriui, nustatomas poskyris ir skyrius
                            if ($examinator['STRUCTURE_TYPE_ID'] === '8') {
                                $fields_data['EXAMINATOR_SUBUNIT'] = $examinator['STRUCTURE_NAME'];

                                $parent_structure = $this->core->logic2->get_db(PREFIX . 'MAIN_STRUCTURE', [
                                    'ID' => $examinator['STRUCTURE_PARENT_ID']], true, null, false, 'A.ID, A.SHOWS, A.TYPE_ID');
                                if (empty($parent_structure) === FALSE && $parent_structure['TYPE_ID'] === '7')
                                    $fields_data['EXAMINATOR_UNIT'] = $parent_structure['SHOWS'];
                            } else
                            // Kai tyrėjas priskirtas skyriui, nustatomas tik skyrius
                            if ($examinator['STRUCTURE_TYPE_ID'] === '7') {
                                $fields_data['EXAMINATOR_UNIT'] = $examinator['STRUCTURE_NAME'];
                            }

                            // Institucijos informacija
                            $institution = $this->get_parent_structure($examinator['STRUCTURE_PARENT_ID'], 12);
                            if (empty($institution) === FALSE) {
                                $institution = $this->core->logic2->get_db(PREFIX . 'MAIN_STRUCTURE', [
                                    'ID' => $institution], true, null, false, 'A.ID, A.SHOWS, A.ADDRESS');
                                // Institucijos pavadinimas
                                $fields_data['EXAMINATOR_INSTITUTION_NAME'] = $institution['SHOWS'];
                                // Institucijos adresas
                                $fields_data['EXAMINATOR_INSTITUTION_ADDRESS'] = $institution['ADDRESS'];
                            }
                            // Nagrinėtojo vardas ir pavardė
                            $fields_data['EXAMINATOR'] = join(' ', [$examinator['FIRST_NAME'],
                                $examinator['LAST_NAME']]);
                            // Kabinetas
                            $fields_data['EXAMINATOR_CABINET'] = $examinator['ROOM'];
                            // Telefonas
                            if (empty($examinator['PHONE_FULL']) === FALSE)
                                $phone = $examinator['PHONE_FULL'];
                            elseif (empty($examinator['PHONE_MOBILE']) === FALSE)
                                $phone = $examinator['PHONE_MOBILE'];
                            $fields_data['EXAMINATOR_PHONE'] = $phone;
                            // El. paštas
                            $fields_data['EXAMINATOR_EMAIL'] = $examinator['EMAIL'];

                            // Pradiniai nagrinėjimo laikai
                            $examinator_free_slot = $this->get_document_person_free_time([
                                'RECORD' => $this->_r_r_id,
                                'DATETIME' => date('Y-m-d H:i:s'),
                                'RESULT_COUNT' => 1
                            ]);
                            $fields_data['EXAMINATOR_DATE'] = $examinator_free_slot[0]['FROM']['DATE'];
                            $fields_data['EXAMINATOR_START_TIME'] = $examinator_free_slot[0]['FROM']['TIME'];
                            $fields_data['EXAMINATOR_FINISH_TIME'] = $examinator_free_slot[0]['TILL']['TIME'];

                            // Automatiškai pildomi laukai
                            $update_data = [];
                            $fields_arr = [
                                'EXAMINATOR',
                                'EXAMINATOR_CABINET',
                                'EXAMINATOR_PHONE',
                                'EXAMINATOR_EMAIL',
                                'EXAMINATOR_INSTITUTION_NAME',
                                'EXAMINATOR_UNIT',
                                'EXAMINATOR_SUBUNIT',
                                'EXAMINATOR_INSTITUTION_ADDRESS',
                                'EXAMINATOR_DATE',
                                'EXAMINATOR_START_TIME',
                                'EXAMINATOR_FINISH_TIME'
                            ];

//                            $valuesManager = new \Atp\Record\Values($this->core);
                            foreach ($fields_arr as $field_name) {
                                $optionName = strtolower($field_name);
                                $fieldId = $documentManager->getOptions($current_document['ID'])->$optionName;
                                if (empty($fieldId)) {
                                    continue;
                                }

                                $field = $this->core->logic2->get_fields(['ID' => $fieldId]);
                                // Praleidžia, jei dokumentas neturi tokio lauko
                                if (array_key_exists($field['NAME'], $record_values) === FALSE) {
                                    continue;
                                }

                                $valuesManager->setValue($allocatedRecordId, $fieldId, $fields_data[$field_name]);
                            }

                            $valuesManager->flushValues();
                        }
                    }


                    /**
                     * Sukuria pranešimus: dokumento termino pabaiga, ATP priminimai.
                     * ATP dokumento pabaiga.
                     * ATP priminimas.
                     */
                    $qry = 'SELECT COUNT(ID) CNT
						FROM `' . PREFIX . 'ATP_RECORD`
						WHERE `RECORD_ID` = :RECORD_ID';
                    $result = $this->core->db_query_fast($qry, array('RECORD_ID' => $recordNr));
                    $count = $this->core->db_next($result);
                    $count = $count['CNT'];

                    if ($count === '1') {
                        $worker = $this->core->logic2->get_worker_info(array('A.ID = ' . (int) $current_workers['main']), true);
                        $term = $this->get_record_term($this->_r_r_id, null, false);

                        //$prefix = trim(end(explode($_SERVER['HTTP_HOST'], $this->core->config['GLOBAL_SITE_URL'], 2)), '/');
                        $prefix = parse_url($this->core->config['GLOBAL_SITE_URL'], PHP_URL_PATH);
                        $url = $prefix . 'subsystems/atp/index.php?m=5&id=' . $recordNr;

                        $current_document = $this->core->logic2->get_document(array(
                            'ID' => $this->_r_table_id));
                        $sch = prepare_schedule();

                        if (empty($term) === FALSE) {
                            $events = $sch->logic->getEventsBySoft(array('ITEM' => $recordNr,
                                'ITEM_TYPE' => 'expire'));
                            if (count($events) === 0) {
                                $event_id = $sch->logic->saveEvent(array(
                                    'FROM' => $term,
                                    'TILL' => $term,
                                    'NAME' => 'ATP dokumento pabaiga',
                                    'DETAILS' => 'ATP termino pabaiga.<br/>'
                                    . 'Dokumentas: ' . $current_document['NAME'] . '<br/>'
                                    . 'Adresas: <a href="' . $url . '">' . $recordNr . '</a>'
                                    ), $worker['PEOPLE_ID'], array(
                                    'SOFT' => 'atp',
                                    'TYPE' => 'expire',
                                    'ITEM' => $recordNr // TODO: RECORD_ID | ar čia reikia paduot RR.ID?
                                    ), $this->core->factory->User()->getLoggedUser()->person->id);
                            }
                        }

                        $holidays = $sch->logic->getHolidays($worker['PEOPLE_ID']);
                        $terms = $this->get_record_other_terms($recordNr, (array) $holidays);

                        if ($terms !== FALSE) {
                            $events = $sch->logic->getEventsBySoft(array('ITEM' => $recordNr,
                                'ITEM_TYPE' => 'remind'));
                            if (count($events) === 0) {
                                foreach ($terms as &$term) {
                                    $event_id = $sch->logic->saveEvent(array(
                                        'FROM' => $term,
                                        'TILL' => $term,
                                        'NAME' => 'ATP priminimas',
                                        'DETAILS' => 'ATP priminimas.<br/>'
                                        . 'Dokumentas: ' . $current_document['NAME'] . '<br/>'
                                        . 'Adresas: <a href="' . $url . '">' . $recordNr . '</a>'
                                        ), $worker['PEOPLE_ID'], array(
                                        'SOFT' => 'atp',
                                        'TYPE' => 'remind',
                                        'ITEM' => $recordNr // TODO: RECORD_ID | ar čia reikia paduot RR.ID?
                                        ), $this->core->factory->User()->getLoggedUser()->person->id);
                                }
                            }
                        }
                    }
                }


                // TODO: tikrinti ne pagal prisijugusį vartotoją, o pagal senus įrašo duomenis
                // TODO: papildomai tikrinit pagal skyrių
                // Jei dokumento tyrimas / nagrinėjimas perduotas kitam vartotojui, praneša kam jis buvo perduotas
                if ($current_workers['main'] != $this->core->factory->User()->getLoggedPersonDuty()->id) {
                    if (in_array($arr['STATUS'], [\Atp\Record::STATUS_INVESTIGATING,
                            \Atp\Record::STATUS_EXAMINING, \Atp\Record::STATUS_IDLE]) === TRUE) {
                        $worker = $this->core->logic2->get_worker_info(['A.ID = ' . (int) $current_workers['main']], true);
                        if (in_array('disable_messages', $param_arg) === FALSE) {
                            $this->core->set_message(
                                'success', 'Dokumentas perduotas į "' . $worker['STRUCTURE_NAME'] . '" skyrių<br>' .
                                'Atsakingas asmuo: ' . $worker['FIRST_NAME'] . ' ' . $worker['LAST_NAME']
                            );
                        }
                        $redirect_url = $this->core->config['SITE_URL'] . 'index.php?m=4&id=' . $this->_r_table_id;
                    }
                }
                break;
            case 'finish_proccess':
                if (empty($this->_r_r_id) === FALSE) {
                    /* $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD
                      SET
                      STATUS = 5 #,
                      #DOC_STATUS = NULL
                      WHERE
                      RECORD_ID = :record_id AND
                      IS_LAST = 1
                      LIMIT 1', array('record_id' => $recordNr)); */
                    $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD
						SET
							STATUS = '.\Atp\Record::STATUS_FINISHED.'
						WHERE
							ID = :ID
						LIMIT 1', array('ID' => $this->_r_r_id));


                    $sch = prepare_schedule();
                    $events = $sch->logic->getEventsBySoft([
                        'SOFT' => 'atp',
                        'ITEM' => $recordNr
                    ]);
                    foreach ($events as &$event) {
                        $sch->logic->disableEvent($event['ID']);
                    }

                    if (in_array('disable_messages', $param_arg) === FALSE) {
                        $this->core->set_message('success', 'Dokumento "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $recordNr . '">' . $recordNr . '</a>" procesas baigtas');
                    }
                } else {
                    if (in_array('disable_messages', $param_arg) === FALSE)
                        $this->core->set_message('error', 'Dokumentas nerastas');
                }
                break;
        }

        $this->update_record_status($this->_r_r_id);

        if (empty($redirect_url) === TRUE) {
            $redirect_url = $this->core->config['SITE_URL'] . 'index.php?m=4&id=' . $this->_r_table_id;
        }

        $this->create_record_redirect_url = $redirect_url;

        return $this->_r_r_id;
    }

    /**
     * Suranda tėvinę organizacinę struktūrą, kuri atitinka ieškomą struktūros tipą
     * @param int $structure_id Organizacinės struktūros ID nuo kurios pradedama paieška
     * @param type $parent_structure_type Ieškomo strukturos tipo ID
     * @return array|null
     */
    public function get_parent_structure($structure_id, $parent_structure_type, Array &$old_ids = array())
    {
        // Apribojimas jei pasiekiama uždata kilpa
        $old_ids[$structure_id] ++;
        if (in_array(5, $old_ids, true))
            return false;

        $structure = $this->core->logic2->get_db(PREFIX . 'MAIN_STRUCTURE', array(
            'ID' => $structure_id), true, null, false, 'A.ID, A.TYPE_ID, A.PARENT_ID');

        if ((int) $structure['TYPE_ID'] === (int) $parent_structure_type)
            return $structure['ID'];
        elseif (empty($structure) === TRUE || empty($structure['PARENT_ID']) === TRUE)
            return null;


        return $this->get_parent_structure($structure['PARENT_ID'], $parent_structure_type, $old_ids);
    }
    /* Dokumentų teisių saugojimas */

    public function edit_rights($department, $users, $privileges)
    {
        // Susiformuoja pradinius naudojamus duomenis
        $arg = array(
            'department' => (int) $department,
            'users' => empty($users) ? array() : (array) $users,
            'privileges' => empty($privileges) ? array() : $privileges
        );

        // Reikia skyriaus ID
        if (empty($arg['department']) === TRUE) {
            return false;
        }

        // Jei nėra vartotojo ID, bet yra skyriaus ID, surenkam visus vartotojus iš skyriaus
        if (empty($arg['users']) === TRUE && empty($arg['department']) === FALSE) {
            $arg['users'] = array_keys($this->get_m_users(0, $arg['department'], 'mp.ID'));
        }

        // Reikia, kad būtų bent vienas vartotojas
        if (count($arg['users']) === 0) {
            return false;
        }

        // Saugo vartotojų susiejimą su dokumento teise
        $in_rows = array();
        $del_rows = array();
        for ($i = 0; $i < count($arg['users']); $i++) {
            $del_rows[] = '(M_PEOPLE_ID = ' . (int) $arg['users'][$i] . ' AND M_DEPARTMENT_ID = ' . $arg['department'] . ')';
            foreach ($arg['privileges'] as $table_id => $privilege) {
                $in_rows[] = '('
                    . (int) $arg['users'][$i] . ','
                    . (int) $arg['department'] . ','
                    . (int) $table_id . ','
                    . (int) $privilege['view_record'] . ','
                    . (int) $privilege['edit_record'] . ','
                    . (int) $privilege['delete_record'] . ','
                    . (int) $privilege['investigate_record'] . ','
                    . (int) $privilege['examinate_record'] . ','
                    . (int) $privilege['end_record'] . '
				)';
            }
        }
        if (count($del_rows)) {
            $qry = 'DELETE FROM ' . PREFIX . 'ATP_USER_PRIVILEGES
				WHERE ' . join('OR ', $del_rows);
            $this->core->db_query($qry);
        }
        if (count($in_rows)) {
            $qry = 'INSERT INTO ' . PREFIX . 'ATP_USER_PRIVILEGES
					(`M_PEOPLE_ID`, `M_DEPARTMENT_ID`, `DOCUMENT_ID`, `VIEW_RECORD`,
						`EDIT_RECORD`, `DELETE_RECORD`, `INVESTIGATE_RECORD`, `EXAMINATE_RECORD`,
						`END_RECORD`)
				VALUES ' . join(',', $in_rows);
            $this->core->db_query($qry);
        }

        return true;
    }

    /**
     * Kompetencijų saugojimas
     * @todo Too many POST fields
     * @author Martynas Boika
     * @return boolen FALSE
     */
    public function edit_competence()
    {
        // Susiformuoja pradinius naudojamus duomenis
        $arg = array(
            'department' => (int) $_POST['user_department_id'],
            'users' => empty($_POST['user_admin_id']) ? array() : (array) $_POST['user_admin_id'],
            'clauses' => empty($_POST['clauses']) ? array() : $_POST['clauses']
        );

        // Reikia skyriaus ID
        if (empty($arg['department']) === TRUE) {
            return false;
        }

        // Jei nėra vartotojo ID, bet yra skyriaus ID, surenkam visus vartotojus iš skyriaus
        if (empty($arg['users']) === TRUE && empty($arg['department']) === FALSE) {
            $arg['users'] = array_keys($this->get_m_users(0, $arg['department'], 'mp.ID'));
        }



        // Reikia, kad būtų bent vienas vartotojas
        if (count($arg['users']) === 0) {
            return false;
        }
        // Saugo vartotojų susiejimą su skyriais
        $in_rows = array();
        $del_rows = array();
        for ($i = 0; $i < count($arg['users']); $i++) {
            // TODO: šalinti `M_PEOPLE_ID`, `M_DEPARTMENT_ID`
            $del_rows[] = '(M_PEOPLE_ID = ' . (int) $arg['users'][$i] . ' AND M_DEPARTMENT_ID = ' . $arg['department'] . ')';
            for ($j = 0; $j < count($arg['clauses']); $j++) {
                $worker = $this->core->logic2->get_worker_info(array('STRUCTURE_ID' => $arg['department'],
                    'PEOPLE_ID' => $arg['users'][$i]), true, null, 'A.ID');
                // TODO: šalinti `M_PEOPLE_ID`, `M_DEPARTMENT_ID`
                $in_rows[] = '(' . (int) $arg['clauses'][$j] . ', ' . $worker['ID'] . ', ' . (int) $arg['users'][$i] . ', ' . $arg['department'] . ')';
            }
        }
        if (count($del_rows)) {
            $qry = 'DELETE FROM ' . PREFIX . 'ATP_USER WHERE ' . join('OR ', $del_rows);
            $this->core->db_query($qry);
        }

        // TODO: šalinti `M_PEOPLE_ID`, `M_DEPARTMENT_ID`
        if (count($in_rows)) {
            $qry = 'INSERT INTO ' . PREFIX . 'ATP_USER (`CLAUSE_ID`, `WORKER_ID`, `M_PEOPLE_ID`, `M_DEPARTMENT_ID`) VALUES ' . join(',', $in_rows);
            $this->core->db_query($qry);
        }

        $this->core->set_message('success', 'Sėkmingai atnaujinta');

        return true;
    }

    public function get_record_before_relation($table_from_id, $record_id)
    {
        if (empty($table_from_id) && empty($record_id))
            return FALSE;

        $sql = "SELECT * FROM " . PREFIX . "ATP_RECORD WHERE TABLE_FROM_ID={$table_from_id} AND RECORD_ID=:record_id ORDER BY ID DESC LIMIT 1";
        $qry = $this->core->db_query_fast($sql, array('name' => $record_id));

        return $this->core->db_next($qry);
    }

    /**
     *
     * @deprecated removed
     * @param type $record_id
     * @param type $fields
     * @return type
     */
    public function get_records_by_id($record_id, $fields = '*')
    {
        throw new \Exception('Function removed');
// <editor-fold defaultstate="collapsed" desc="comment">
//        $qry = 'SELECT ' . $fields . '
//			FROM ' . PREFIX . 'ATP_RECORD
//			WHERE
//				RECORD_ID = :record_id';
//        $result = $this->core->db_query_fast($qry,
//            array('record_id' => $record_id));
//
//        $ret = array();
//        while ($row = $this->core->db_next($result))
//            $ret[] = $row;
//
//        return $ret;// </editor-fold>
    }

    public function get_records_by_condition($condition = '', $fields = '*', $post = '')
    {
        $qry = 'SELECT ' . $fields . '
			FROM ' . PREFIX . 'ATP_RECORD WHERE 1 = 1 ' . $this->_PrepareCondition($condition) . ' ' . $post;
        $result = $this->core->db_query_fast($qry, array('record_id' => $record_id));

        $ret = array();
        while ($row = $this->core->db_next($result))
            $ret[] = $row;
        $this->core->db_free($result);

        return $ret;
    }

    /**
     * Gražina straipsnius, į kuriuos darbuotojas turi teisę
     * @param int|array $arr Parametrai arba <i>$arr['WORKER_ID']</i>
     * @param int $arr['WORKER_ID'] [optional] Darbuotojo ID
     * @param int $arr['PEOPLE_ID'] [optional] Vartotojo ID
     * @param int $arr['STRUCTURE_ID'] [optional] Skyriaus ID
     * @example get_user_clauses(array('WORKER_ID' => $worker_id));<br>get_user_clauses($worker_id);<br>get_user_clauses(array('PEOPLE_ID' => $user_id, 'STRUCTURE_ID' => $department_id));
     * @return boolean|array
     */
    public function get_user_clauses($arr)
    {
        if (is_array($arr) === FALSE) {
            $worker_id = $arr;
        } else
        if (empty($arr['WORKER_ID']) === FALSE) {
            $worker_id = $arr['WORKER_ID'];
        } else
        if (empty($arr['PEOPLE_ID']) === FALSE && empty($arr['STRUCTURE_ID']) === FALSE) {
            $worker = $this->core->logic2->get_worker_info(array('STRUCTURE_ID' => $arr['STRUCTURE_ID'],
                'PEOPLE_ID' => $arr['PEOPLE_ID']), true, null, 'A.ID');
            $worker_id = $worker['ID'];
        }
        if (empty($worker_id) === TRUE) {
            return false;
        }

        $qry = 'SELECT B.*
			FROM
				' . PREFIX . 'ATP_USER A
					INNER JOIN ' . PREFIX . 'ATP_CLAUSES B ON B.ID = A.CLAUSE_ID
			WHERE
				WORKER_ID = :WORKER_ID';

        $result = $this->core->db_query_fast($qry, array('WORKER_ID' => (int) $worker_id));
        $return = array();
        while ($row = $this->core->db_next($result))
            $return[$row['ID']] = $row;

        return $return;
    }

    // TODO: Su departamento id išselektina straipsnius kuriems neturi kompetencijos
    public function search_clauses($arr, $fields = 'A.*', $order_by = 'CLAUSE, SECTION, PARAGRAPH, SUBPARAGRAPH')
    {
        $where = array();
        $args = array();

        if (is_array($fields))
            $fields = join(',', $fields);
        if (count($arr)) {
            foreach ($arr as $key => $val) {
                if (empty($val) === TRUE)
                    continue;
                if ($key === 'NAME') {
                    $where[] = 'A.`' . $key . '` LIKE(\'%\' + :' . $key . ' + \'%\')';
                } else {
                    $where[] = 'A.`' . $key . '` = :' . $key;
                }
                // Skyriaus ir straipsnio sąryšis panaikintas
                /* elseif ($key === 'M_DEPARTMENT_ID') {
                  $where[] = 'B.`' . $key . '`  = :' . $key . '';
                  } */
                $args[] = array('name' => $key, 'value' => $val);
            }
            array_unshift($args, array());
        }
        $qry = 'SELECT ' . $fields . '
			FROM ' . PREFIX . 'ATP_CLAUSES A
			' . (count($where) ? 'WHERE ' . join(' AND ', $where) : '')
            . (empty($order_by) ? '' : ' ORDER BY ' . $order_by);
        $qry = $this->core->db_query($qry, $args);

        $users = array();
        while ($res = $this->core->db_next($qry)) {
            $users[] = $res;
        }

        return $users;
    }

    /**
     *
     * @param null|string|string[] $fields Fields to select.
     * @param array $arr At least one of folowing.
     * @param int $arr['ID'] Record ID.
     * @param string $arr['LABEL'] Regexp to mach against.
     * @param boolean $arr['WITH_PARENTS'] Find all parents.
     * @return boolean|array
     */
    public function search_classificator($fields, $arr)
    {
        $where = array();
        $withParents = false;
        foreach ($arr as $key => $val) {
            switch ($key) {
                case 'ID':
                    if (strlen($val) > 0) {
                        $where[] = 'ID = "' . $this->core->db_escape($val) . '"';
                    }
                    break;
                case 'LABEL':
                    if (strlen($val) > 0) {
                        $to = array();
                        $escape = array('\\', '/', '^', '$', '.', '|', '?', '*',
                            '+', '(', ')', '[', ']', '{', '}', '<', '>');
                        foreach ($escape as $k => $v) {
                            $to[$k] = '\\' . $v;
                        }

                        $val = str_replace($escape, $to, $val);

                        $where[] = 'LABEL REGEXP "' . $this->core->db_escape($val) . '"';
                    }
                    break;
                case 'WITH_PARENTS':
                    $withParents = (bool) $val;
                    break;
            }
        }

        if (empty($fields)) {
            $fields = '*';
        } elseif (is_array($fields)) {
            if ($withParents) {
                if (in_array('ID', $fields) === false) {
                    $fields[] = 'ID';
                }
                if (in_array('PARENT_ID', $fields) === false) {
                    $fields[] = 'PARENT_ID';
                }
            }

            $fields = join(',', $fields);
        }


        if (count($where)) {
            $where = 'WHERE ' . join(' AND ', $where);
        }

        $sql = 'SELECT ' . $fields . ' FROM ' . PREFIX . 'ATP_CLASSIFICATORS ' . $where . ' ORDER BY LABEL';
        $qry = $this->core->db_query($sql, $b_data);

        $result = array();
        $nodesMap = array();
        while ($data = $this->core->db_next($qry)) {
            if (isset($nodesMap[$data['ID']])) {
                continue;
            }

            $nodesMap[$data['ID']] ++;

            $result[] = $data;
        }

        if ($withParents) {
            foreach ($result as $data) {
                if (empty($data['PARENT_ID'])) {
                    continue;
                }
                if (isset($nodesMap[$data['PARENT_ID']])) {
                    continue;
                }

                $parents = $this->search_classificator($fields, array_merge($arr, [
                    'ID' => $data['PARENT_ID'],
                    'LABEL' => null
                ]));


                if (empty($parents) === false) {
                    foreach ($parents as $parent) {
                        if (isset($nodesMap[$parent['ID']])) {
                            continue;
                        }

                        $nodesMap[$parent['ID']] ++;

                        $parent['EXTRACTED_AS_PARENT'] = true;
                        $result[] = $parent;
                    }
                }
            }
        }

        if (!empty($result)) {
            return $result;
        }

        return FALSE;
    }

    public function search_clause($fields, $where)
    {
        if (empty($fields))
            $fields = '*';
        elseif (is_array($fields)) {
            $fields = join(',', $fields);
        }
        if (count($where))
            $where = 'WHERE ' . join(' AND ', $where);
        $sql = 'SELECT ' . $fields . '
			FROM ' . PREFIX . 'ATP_CLAUSES
			' . $where . '
			ORDER BY CLAUSE, SECTION, PARAGRAPH, SUBPARAGRAPH';
        $qry = $this->core->db_query($sql, $b_data);
        while ($res = $this->core->db_next($qry))
            $clauses[] = $res;
        if (!empty($clauses))
            return $clauses;

        return FALSE;
    }

    public function search_users($arr, $fields = 'A.*, B.STRUCTURE_ID, C.SHOWS as DEPARTMENT_TITLE', $order_by = 'A.SHOWS ASC')
    {
        $where = array();
        $args = array();

        if (is_array($fields))
            $fields = join(',', $fields);

        if (count($arr)) {
            foreach ($arr as $key => $val) {
                if ($key === 'SHOWS') {
                    $where[] = 'A.`' . $key . '` LIKE(\'%\' + :' . $key . ' + \'%\')';
                } elseif ($key === 'STRUCTURE_ID') {
                    $where[] = 'B.`' . $key . '`  = :' . $key . '';
                }
                $args[] = array('name' => $key, 'value' => $val);
            }
            array_unshift($args, array());
        }

        $qry = 'SELECT ' . $fields . '
			FROM ' . PREFIX . 'MAIN_WORKERS B
				INNER JOIN ' . PREFIX . 'MAIN_PEOPLE A ON A.ID = B.PEOPLE_ID
				INNER JOIN ' . PREFIX . 'MAIN_STRUCTURE C ON C.ID = B.STRUCTURE_ID
			' . (count($where) ? 'WHERE ' . join(' AND ', $where) : '')
            . (empty($order_by) ? '' : ' ORDER BY ' . $order_by);
        $qry = $this->core->db_query($qry, $args);

        $users = array();
        while ($res = $this->core->db_next($qry)) {
            $users[] = $res;
        }

        return $users;
    }

    public function search_departments($arr, $fields = 'B.*', $order_by = 'B.SHOWS ASC')
    {
        $where = array();
        $args = array();

        if (is_array($fields))
            $fields = join(',', $fields);

        if (count($arr)) {
            foreach ($arr as $key => $val) {
                $where[] = '`' . $key . '` LIKE(\'%\' + :' . $key . ' + \'%\')';
                $args[] = array('name' => $key, 'value' => $val);
            }
            array_unshift($args, array());
        }

        $qry = 'SELECT ' . $fields . '
			FROM ' . PREFIX . 'MAIN_STRUCTURE B
			' . (count($where) ? 'WHERE ' . join(' AND ', $where) : '')
            . (empty($order_by) ? '' : ' ORDER BY ' . $order_by);
        $qry = $this->core->db_query($qry, $args);

        $departments = array();
        while ($res = $this->core->db_next($qry))
            $departments[] = $res;
        return $departments;
    }

    /**
     *
     * @deprecated nenaudojamas
     * @param int $department_id
     * @param boolen $admin_none [optional] Default: FALSE
     * @return boolean/array
     */
    public function get_department($department_id = 0, $admin_none = FALSE)
    {
        trigger_error('get_department() is deprecated', E_USER_DEPRECATED);
        $where = (!empty($department_id)) ? "WHERE d.MS_ID={$department_id}" : "WHERE ms.PARENT_ID=490";
        // TODO: kam tikrinamas 'STATUS' ? DONE: STATUS = 1 yra matymo teisė; 2 - ir redagavimo teisė
        // TODO: vistiek, kam tai tikrinama ?
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('STATUS') > 1) {
            // TODO: iš kur tas 'ADMIN_DEPARTMENT_ID' ?
            if (!empty($this->core->factory->User()->getLoggedPersonDutyStatus('ADMIN_DEPARTMENT_ID')) && empty($department_id))
                $where = "WHERE
						(
							d.MS_ID = {$this->core->factory->User()->getLoggedPersonDutyStatus('ADMIN_DEPARTMENT_ID')} OR
							ms.PARENT_ID = {$this->core->factory->User()->getLoggedPersonDutyStatus('ADMIN_DEPARTMENT_ID')}
						)";
        } elseif ($admin_none === FALSE)
            $where = '';

        $departments = array();
        $qry = $this->core->db_query('SELECT ms.*
				FROM ' . PREFIX . 'ATP_DEPARTMENT d
					INNER JOIN ' . PREFIX . 'MAIN_STRUCTURE ms ON ms.ID = d.MS_ID ' .
            $where);
        while ($res = $this->core->db_next($qry))
            $departments[$res['ID']] = $res;

        if (!empty($departments))
            return $departments;

        return FALSE;
    }

    public function get_m_department($department_id)
    {
        if (empty($department_id))
            return FALSE;

        $qry = $this->core->db_query("SELECT * FROM " . PREFIX . "MAIN_STRUCTURE WHERE ID={$department_id}");
        return $this->core->db_next($qry);
    }

    public function get_m_department_list($parent_id, $level = 0)
    {
        $where = ($level == 0 ? " ID={$parent_id} LIMIT 1" : " PARENT_ID={$parent_id}");

        $level += 1;
        $result = array();
        $qry = $this->core->db_query("SELECT * FROM " . PREFIX . "MAIN_STRUCTURE WHERE {$where}");
        while ($res = $this->core->db_next($qry)) {
            $res['LEVEL'] = $level;
            $result[] = $res;
            $result_chld = $this->get_m_department_list($res['ID'], $level);
            foreach ($result_chld as $chld)
                $result[] = $chld;
        }

        return $result;
    }

    public function get_m_user($user_id = 0, $department_id = 0)
    {
        if (empty($user_id))
            return FALSE;

        $where = array('mw.PEOPLE_ID = :PEOPLE_ID');
        $arg = array('PEOPLE_ID' => $user_id);
        if (empty($department_id) === FALSE)
            $where[] = 'mw.STRUCTURE_ID = :STRUCTURE_ID';
        $arg['STRUCTURE_ID'] = $department_id;

        $users = array();
        $qry = $this->core->db_query_fast('SELECT mp.*,mw.STRUCTURE_ID,ms.SHOWS as DEPARTMENT_TITLE
			FROM ' . PREFIX . 'MAIN_WORKERS mw
                INNER JOIN ' . PREFIX . 'MAIN_PEOPLE mp ON mp.ID = mw.PEOPLE_ID
                INNER JOIN ' . PREFIX . 'MAIN_STRUCTURE ms ON ms.ID = mw.STRUCTURE_ID
            WHERE ' . join(' AND ', $where), $arg);
        $users = $this->core->db_next($qry);

//        // TODO: WTF per IF'as?
//        if ($this->core->_admin_id == $this->core->factory->User()->getLoggedUser()->person->id && $this->core->factory->User()->getLoggedUser()->person->id == $user_id && empty($department_id))
//            $users['DEPARTMENT_TITLE'] = 'ATP sistemos administratorius';

        if (!empty($users))
            return $users;

        return FALSE;
    }

    public function get_m_users($user_id = 0, $department_id = 0, $fields = null)
    {
        if (empty($department_id))
            return FALSE;

        $where = '';
        if (!empty($user_id))
            $where = "mw.PEOPLE_ID={$user_id}";
        if (!empty($department_id)) {
            if (!empty($where))
                $where .= " AND ";
            $where .= "mw.STRUCTURE_ID={$department_id}";
        }
        if (empty($fields) === TRUE)
            $fields = 'mp.*,mw.STRUCTURE_ID,ms.SHOWS as DEPARTMENT_TITLE';

        $qry = $this->core->db_query('SELECT ' . $fields . '  FROM ' . PREFIX . 'MAIN_WORKERS mw
                INNER JOIN ' . PREFIX . 'MAIN_PEOPLE mp ON (mp.ID=mw.PEOPLE_ID)
                INNER JOIN ' . PREFIX . 'MAIN_STRUCTURE ms ON (ms.ID=mw.STRUCTURE_ID)
            WHERE ' . $where . ' ORDER BY mp.LAST_NAME');

        $users = array();
        while ($res = $this->core->db_next($qry))
            $users[$res['ID']] = $res;

        if (!empty($users))
            return $users;

        return FALSE;
    }

    /**
     * @author Justinas Malūkas
     * @return boolen|array
     */
    function clause_action($data, $type)
    {
        switch ($type) {
            case 'delete':
                if (empty($data)) {
                    return false;
                }

                $clause = $this->core->logic2->get_clause(array('ID' => $data));

                //$ids = $this->get_clause_branch_ids($data);
                $ids = $this->get_branch_ids($clause['ID'], 'clause');

                if (count($ids) === 0)
                    return false;

                $this->core->db_query('DELETE FROM `' . PREFIX . 'ATP_CLAUSES` WHERE `ID` IN (' . join(',', $ids) . ')');
                //$this->core->db_query('DELETE FROM `' . PREFIX . 'ATP_FIELD` WHERE `ITYPE` = "CLAUSE" AND `ITEM_ID` IN (' . join(',', $ids) . ')');
                //$this->delete_table_structure(array('ITYPE' => 'CLAUSE', '#ITEM_ID#' => 'IN (' . join(',', $ids) . ')'));
                $this->delete_table_structure(array('ITYPE' => 'CLAUSE', 0 => '`ITEM_ID` IN (' . join(',', $ids) . ')'));
                $this->delete_deed_extends(array('ITYPE' => 'CLAUSE', 0 => '`ITEM_ID` IN (' . join(',', $ids) . ')'));

                $result = $this->core->db_query('SELECT COUNT(`ID`) COUNT FROM `' . PREFIX . 'ATP_CLAUSES` WHERE `ID` = ' . (int) $clause['ID']);
                $row = $this->core->db_next($result);


                $adminJournal = $this->core->factory->AdminJournal();
                $adminJournal->addRecord(
                    $adminJournal->getDeleteRecord(
                        'Straipsnį "' . $clause['NAME'] . '"'
                    )
                );


                return (int) $row['COUNT'] ? false : $ids;
                break;
        }
    }

    /**
     * @author Justinas Malūkas
     * @return boolen|array
     */
    function classificator_action($data, $type)
    {
        switch ($type) {
            case 'save':
                $classificatorManager = new \Atp\Classificator($this->core);
                $idOrFalse = $classificatorManager->save($data);

                return $idOrFalse;
            case 'delete':
                $classificatorManager = new \Atp\Classificator($this->core);
                $idsOrFalse = $classificatorManager->delete($data);

                return $idsOrFalse;
                break;
            case 'search':
                return $this->search_classificator('*', $data);
                break;
        }
    }

    /**
     * Veikų veiksmai
     * @author Justinas Malūkas
     * @return boolen|array
     */
    function deed_action($data, $type)
    {
        switch ($type) {
            case 'editform':
                return $this->core->manage->deed_editform(true);
                break;
            case 'save':
                $extends = $data['extends'];
                $data = $data['data'];
                $extended = array();

                $update = false;
                $create = false;

                if (empty($data['ID']) === FALSE) {
                    $update = true;

                    $qry = 'UPDATE `' . PREFIX . 'ATP_DEEDS`
						SET
							`LABEL` = :LABEL
						WHERE `ID` = :ID';

                    // Surenka senus papildomus įrašus, kad veliau įrašinėtu tik naujus įrašus
                    $result = $this->core->db_query_fast('SELECT `ITEM_ID`, `ITYPE` FROM `' . PREFIX . 'ATP_DEED_EXTENDS` WHERE `DEED_ID` = :ID AND `VALID` = 1', $data);
                    while ($row = $this->core->db_next($result)) {
                        $extended[$row['ITYPE']][$row['ITEM_ID']] = 1;
                    }
                } elseif (isset($data['PARENT_ID']) === TRUE) {
                    $create = true;

                    $qry = 'INSERT INTO `' . PREFIX . 'ATP_DEEDS`
							(`LABEL`, `PARENT_ID`)
						VALUES
							(:LABEL, :PARENT_ID)';
                } else {
                    $this->core->set_message('error', 'Klaida');
                    return false;
                }
                $result = $this->core->db_query_fast($qry, $data);

                if (isset($data['PARENT_ID']) === TRUE) {
                    $data['ID'] = $result = (int) $this->core->db_last_id();
                }

                $i = 0;
                $ins = array();
                $bind['DEED_ID'] = (int) $data['ID'];
                foreach ($extends as $type => $arr) {
                    if ($type === 'acts') {
                        $type = 'ACT';
                    } elseif ($type === 'clauses') {
                        $type = 'CLAUSE';
                    } else {
                        trigger_error('Undefined type "' . $type . '"', E_USER_WARNING);
                        continue;
                    }

                    foreach ($arr as $item) {
                        if (empty($item) === TRUE || isset($extended[$type][$item]) === TRUE) {
                            unset($extended[$type][$item]);
                            continue;
                        }
                        //$ins[] = '(' . (int) $data['ID']. ', ' . (int) $item . ', "' . $type . '")';
                        $ins[] = '(:DEED_ID, :' . $i . 'ITEM_ID, :' . $i . 'ITYPE)';
                        $bind = array_merge($bind, array($i . 'ITEM_ID' => $item,
                            $i . 'ITYPE' => $type));
                        //$bind[$i . 'ITEM_ID'] = $item;
                        //$bind[$i . 'ITYPE'] = $type;
                        $i++;
                    }
                }
                if (count($ins) > 0)
                    $this->core->db_query_fast('INSERT INTO `' . PREFIX . 'ATP_DEED_EXTENDS` (`DEED_ID`, `ITEM_ID`, `ITYPE`)
						VALUES ' . join(',', $ins), $bind);
                //$this->core->db_ATP_insert(PREFIX . 'ATP_DEED_EXTENDS', $bind);
                //$i = 0;
                $del = array();
                $bind['DEED_ID'] = (int) $data['ID'];
                foreach ($extended as $type => $arr) {
                    foreach ($arr as $item => $v) {
                        $del[] = '(`ITEM_ID` = ' . (int) $item . ' AND `ITYPE` = "' . $type . '")';
                        //$del[] = '(`ITEM_ID` = :' . $i . 'ITEM_ID AND `ITYPE` = :' . $i . 'ITYPE)';
                        //$bind = array_merge($bind, array($i . 'ITEM_ID' => $item, $i . 'ITYPE' => $type));
                        //$i++;
                    }
                }
                if (count($del) > 0) {
                    $this->delete_deed_extends(array('DEED_ID' => (int) $data['ID'],
                        0 => '(' . join(' OR ', $del) . ')'));
                }


                if ($create) {
                    $adminJournal = $this->core->factory->AdminJournal();
                    $adminJournal->addRecord(
                        $adminJournal->getCreateRecord(
                            'Veika "' . $data['LABEL'] . '"'
                        )
                    );

                    $this->core->set_message('success', 'Veika sukurta');
                } elseif ($update) {
                    $adminJournal = $this->core->factory->AdminJournal();
                    $adminJournal->addRecord(
                        $adminJournal->getEditRecord(
                            'Veika "' . $data['LABEL'] . '"'
                        )
                    );

                    $this->core->set_message('success', 'Veika atnaujinta');
                }


                return $result;
                break;
            case 'delete':
                if (empty($data)) {
                    return false;
                }

                $deed = $this->logic->get_deed(array('ID' => $data));

                $ids = $this->get_branch_ids($deed['ID'], 'deed');

                if (count($ids) === 0) {
                    return false;
                }

                $this->core->db_query_fast('DELETE FROM `' . PREFIX . 'ATP_DEEDS` WHERE `ID` IN (' . join(',', $ids) . ')');
                $this->delete_deed_extends(array(0 => '`DEED_ID` IN (' . join(',', $ids) . ')'));
                //$this->core->db_query('DELETE FROM `' . PREFIX . 'ATP_FIELD` WHERE `ITYPE` = "DEED" AND `ITEM_ID` IN (' . join(',', $ids) . ')');
                //$this->delete_table_structure(array('ITYPE' => 'DEED', '#ITEM_ID#' => 'IN (' . join(',', $ids) . ')'));
                $this->delete_table_structure(array('ITYPE' => 'DEED', 0 => '`ITEM_ID` IN (' . join(',', $ids) . ')'));
                $result = $this->core->db_query_fast('SELECT COUNT(`ID`) COUNT FROM `' . PREFIX . 'ATP_DEEDS` WHERE `ID` = ' . (int) $deed['ID']);
                $row = $this->core->db_next($result);

                if ((int) $row['COUNT'] == FALSE) {
                    $adminJournal = $this->core->factory->AdminJournal();
                    $adminJournal->addRecord(
                        $adminJournal->getDeleteRecord(
                            'Veika "' . $deed['LABEL'] . '"'
                        )
                    );


                    $this->core->set_message('success', 'Veika ištrinta');
                } else {
                    $this->core->set_message('error', 'Veika neištrinta');
                }

                return (int) $row['COUNT'] ? false : $ids;
                break;
        }
    }

    /**
     * Gauna visų vaikų ID su jų tėvų ID
     * @author Justinas Malūkas
     * @param int $id Įrašo ID, nuo kurio pradėti paiešką
     * @param string $type Įrašo tipas ('act', 'deed', 'class' arba 'clause')
     * @param array $ret
     * @return boolean
     */
    function get_branch_ids($id, $type, &$ret = array())
    {
        if (empty($id) === TRUE) {
            trigger_error('ID is missing', E_USER_WARNING);
            return false;
        }
        if (in_array($type, array('act', 'deed', 'class', 'clause')) === FALSE) {
            trigger_error('Type is missing or wrong', E_USER_WARNING);
            return false;
        }

        $tables = array(
            'act' => 'ATP_ACTS',
            'deed' => 'ATP_DEEDS',
            'class' => 'ATP_CLASSIFICATORS',
            'clause' => 'ATP_CLAUSES',
        );

        $where = '';
        if ($type === 'act') {
            $where = ' AND `VALID` = 1';
        }

        if (empty($ret))
            $qry = 'SELECT `ID`, `PARENT_ID` FROM `' . PREFIX . $tables[$type] . '` WHERE `ID` = ' . (int) $id . $where;
        else {
            $qry = 'SELECT `ID`, `PARENT_ID` FROM `' . PREFIX . $tables[$type] . '` WHERE `PARENT_ID` = ' . (int) $id . $where;
        }
        $result = $this->core->db_query($qry);
        while ($row = $this->core->db_next($result)) {
            $ret[] = (int) $row['ID'];
            $this->get_branch_ids($row['ID'], $type, $ret);
        }

        return $ret;
    }

    public function get_deed_search_values($params = array())
    {
        $values = array();

        // Veikos
        $classType = false;
        if (empty($params['CLASS_TYPE']) === FALSE) {
            $classType = (int) $params['CLASS_TYPE'];
		}
		$deeds = $this->get_deed(array(), false, $classType);
		if (count($deeds)) {
            foreach ($deeds as $key => $arr) {
                $values[] = array(
                    'LABEL' => $arr['LABEL'],
                    'VALUE' => $arr['ID'],
                    'ID' => $arr['ID'],
                    'TYPE' => 'DEED',
                    'DATA' => $arr
                );
            }
        } else {
            return true;
        }


        // Straipsniai ir teisės aktų punktai
        $param = array();
        $classType = null;
        if (empty($params['CLASS_TYPE']) === FALSE) {
            $classType = (int) $params['CLASS_TYPE'];
//            $param[] = 'D.CLASSIFICATOR_ID = ' . (int) $params['CLASS_TYPE'];
        }

        $ddata = $this->get_deed_extend_data($param, $classType, true);

        // Filtruojama pagal Pažeidimo tipą ir gražina tik veikas
/*
        if (empty($params['CLASS_TYPE']) === FALSE) {
            $deed_ids = array();
            foreach ($ddata as $arr) {
                $deed_ids[$arr['DEED_ID']] = $arr['DEED_ID'];
            }


            foreach ($values as $key => &$value) {
                if (in_array($value['ID'], $deed_ids) === FALSE) {
//                  unset($values[$key]);
				}
            }
            return $values;
        }
*/

        foreach ($ddata as $arr) {
            $values[] = array(
                'LABEL' => $arr['INAME'],
                'VALUE' => $arr['DEED_ID'],
                'ID' => $arr['ITEM_ID'],
                'TYPE' => $arr['ITYPE'],
                'DATA' => $arr
            );
        }


        // Pagal laukų reikšmes ?
        foreach ($values as $arr) {
            $extra = $this->get_table_fields_w_extra2r($arr['ID'], array('ITYPE' => $arr['TYPE']));
            foreach ($extra as $a) {
                if (empty($a['DEFAULT']) === TRUE && empty($a['DEFAULT_ID']) === TRUE)
                    continue;

                $values[] = array(
                    'LABEL' => $a['DEFAULT'],
                    'VALUE' => $arr['VALUE'],
                    'ID' => $a['ID'],
                    'TYPE' => 'FIELD',
                    'DATA' => $a
                );
            }
        }

        return $values;
    }

    public function get_deeds()
    {
        return $this->get_deed();
    }

    /**
     * Gauna veikų informaciją
     * @author Justinas Malūkas
     * @param array $params parametrų masyvas
     * @param boolen $limit
     * @return array
     */
    public function get_deed($params = array(), $limit = FALSE, $class_id = false)
    {
        $where = array();
        $arg = array();
        foreach ($params as $key => $value) {
            $where[] = '`' . $key . '`' . ' = :' . $key;
            $arg[$key] = $value;
            if ($key === 'ID')
                $limit = true;
        }

		$class_sql = '';
		if (empty($class_id) === FALSE) {
			$where[] = 'ID IN (SELECT DEED_ID FROM '.PREFIX.'ATP_DEED_EXTENDS WHERE ITEM_ID IN (SELECT CLAUSE_ID FROM '.PREFIX.'ATP_CLAUSE_CLASSIFICATOR_XREF WHERE CLASSIFICATOR_ID = ' . (int) $class_id.')) ';
		}

		$where = count($where) ? ' WHERE ' . join(' AND ', $where) : '';

        $qry = 'SELECT * FROM ' . PREFIX . 'ATP_DEEDS' . $where . ($limit ? ' LIMIT 1' : '');
        $result = $this->core->db_query_fast($qry, $arg);

        $ret = array();
        while ($row = $this->core->db_next($result)) {
            if ($limit === TRUE)
                return $row;
            $ret[$row['ID']] = $row;
        }

        return $ret;
    }

    /**
     *
     * @param array $param Parametrų masyvas
     * @param array $classType Parametrų masyvas
     * @return array
     */
    public function get_deed_extend_data($params = array(), $classType = null, $showAll = false)
    {

        $where = array();
        $arg = array();

        $join = null;
        if (empty($classType) === FALSE) {
			if ($showAll == false) {
				$params[] = 'D.CLASSIFICATOR_ID = ' . (int) $classType;
	            $join = ' LEFT JOIN ' . PREFIX . 'ATP_CLAUSE_CLASSIFICATOR_XREF D ON D.CLAUSE_ID = C.ID ';
			} else {
				$params[] = 'D.CLASSIFICATOR_ID = ' . (int) $classType;
	            $join = ' LEFT JOIN ' . PREFIX . 'ATP_CLAUSE_CLASSIFICATOR_XREF D ON D.CLAUSE_ID = C.ID ';
			}
        }

        foreach ($params as $key => $value) {
            if (is_int($key) === TRUE) {
                $where[] = ':' . $key;
                $key = '#' . $key . '#';
            } else {
                $bind = trim($key, '#');
                if (strpos($key, '.') !== FALSE) {
                    list($table, $column) = explode('.', $bind);
                } else {
                    $table = 'A';
                    $column = $key;
                }
                $bind = $table . '_' . $column;

                $condition = (empty($table) ? '' : '`' . $table . '`.')
                    . '`' . $column . '`' . ' = :' . $bind;

                $where[] = $condition;
                $key = $bind;
            }
            $arg[$key] = $value;
        }

        $where[] = 'A.`VALID` = 1';

        $where = count($where) ? 'WHERE ' . join(' AND ', $where) : '';

        // IID nereikia, yra A.ITEM_ID
        $qry = 'SELECT A.*, IF(B.ID IS NULL, C.ID, B.ID) IID, IF(B.ID IS NULL, C.NAME, B.LABEL) INAME
			FROM
				' . PREFIX . 'ATP_DEED_EXTENDS A
					LEFT JOIN ' . PREFIX . 'ATP_ACTS B ON B.ID = A.ITEM_ID AND A.ITYPE = "ACT" AND B.`VALID` = 1
					LEFT JOIN ' . PREFIX . 'ATP_CLAUSES C ON C.ID = A.ITEM_ID AND A.ITYPE = "CLAUSE"
					' . $join . '
			' . $where;

        $result = $this->core->db_query_fast($qry, $arg);

        $ret = array();
        while ($row = $this->core->db_next($result)) {
            $ret[$row['ID']] = $row;
        }

        return $ret;
    }
    /**
     * Gauna klasifikatorių duomenis pagal nurodytus parametrus
     * @author Justinas Malūkas
     * @param array $params Parametrų masyvas
     * @param boolen $limit [optional] Gražina tik pirmą įrašą. <b>Default: FALSE</b>
     * @return array
     */
    /* public function get_classificator($params = array(), $limit = FALSE) {
      $where = array();
      $arg = array();
      foreach ($params as $key => $value) {
      if (is_int($key) === TRUE) {
      $where[] = ':' . $key;
      $key = '#' . $key . '#';
      } else
      $where[] = '`' . trim($key, '#') . '`' . ' = :' . trim($key, '#');
      $arg[$key] = $value;

      if ($key === 'ID')
      $limit = true;
      }
      $where = count($where) ? ' WHERE ' . join(' AND ', $where) : '';

      $qry = 'SELECT * FROM ' . PREFIX . 'ATP_CLASSIFICATORS' . $where . ($limit ? ' LIMIT 1' : '');
      $result = $this->core->db_query_fast($qry, $arg);
      $ret = array();
      while ($row = $this->core->db_next($result)) {
      if ($limit === TRUE)
      return $row;
      $ret[$row['ID']] = $row;
      }

      return $ret;
      } */

    // TODO: naudoti $this->get_classificator()
    /**
     * Gražina klasifikatorių duomenis pagal nurodytus parametrus
     * @authors Martynas Boika, Justinas Malūkas
     * @param int $id Klasifikatoriaus ID
     * @param int $parent_id Klasifikatoriaus tėvinis ID
     * @param array $opt Nustatymai. Pvz.: array('ORDER' => 'ID DESC')
     * @return boolean|array
     */
    /* 	public function get_classification($id = null, $parent_id = null, $opt = null) {
      if (empty($id) === FALSE) {
      $where = ' WHERE ID = ' . (int) $id;
      } elseif (isset($parent_id) === TRUE) {
      $where = ' WHERE PARENT_ID = ' . (int) $parent_id;
      }

      if (empty($opt['ORDER']) === FALSE) {
      $order = ' ORDER BY ' . $opt['ORDER'];
      } elseif (empty($where) === TRUE || isset($parent_id) === TRUE) {
      $order = ' ORDER BY ID';
      }

      $limit = (!empty($where) && !empty($id)) ? ' LIMIT 1' : '';

      $return = array();
      $result = $this->core->db_query('SELECT * FROM ' . PREFIX . 'ATP_CLASSIFICATORS' . $where . $order . $limit);
      while ($row = $this->core->db_next($result))
      $return[$row['ID']] = $row;

      if (!empty($return))
      return $return;

      return FALSE;
      } */

    /**
     *
     * @param type $classification_list
     * @deprecated Nenaudojamas. Naudoti $this->get_classificator()
     * @return boolean
     */
    public function get_classification_list($classification_list = array())
    {
        throw new \Exception('Function removed');
// <editor-fold defaultstate="collapsed" desc="comment">
//        if (empty($classification_list))
//            return FALSE;
//
//        $classification_list = implode(',', $classification_list);
//
//        $classifications = array();
//        $qry = $this->core->db_query("SELECT * FROM " . PREFIX . "ATP_CLASSIFICATORS WHERE ID IN ({$classification_list})");
//        while ($res = $this->core->db_next($qry))
//            $classifications[$res['ID']] = $res;
//
//        if (!empty($classifications))
//            return $classifications;
//
//        return FALSE;// </editor-fold>
    }

    public function edit_classification()
    {
        $classification_parent = (!empty($_POST['classification_parent'])) ? $_POST['classification_parent'] : array();
        $classification_label = (!empty($_POST['classification_label'])) ? $_POST['classification_label'] : '';

        $parent_id = 0;
        foreach ($classification_parent as $parent) {
            if (!empty($parent)) {
                $parent_id = $parent;
            }
        }

        $delete = array();
        $update = array();
        $insert = array();
        //$classifications = $this->get_classification(null, $parent_id);
        $classifications = $this->core->logic2->get_classificator(array('PARENT_ID' => $parent_id));

        foreach ($classification_label as $order => $label) {
            $found = FALSE;
            foreach ($classifications as $classification) {
                if (key($label) == $classification['ID']) {
                    $found = TRUE;
                    $value = $label[key($label)];
                    if (!empty($value))
                        $update[$classification['ID']] = $value;
                    else {
                        $delete[] = $classification['ID'];
                    }

                    break;
                }
            }
            if (!$found) {
                $value = $label[key($label)];
                if (!empty($value)) {
                    $insert[] = $value;
                }
            }
        }

        if (!empty($delete)) {
            /*
             * TODO: reikia padarytį, kad ir šakas ištrintų
             * */
            $delete = implode(',', $delete);
            $this->core->db_query("DELETE FROM " . PREFIX . "ATP_CLASSIFICATORS WHERE ID IN ({$delete}) AND PARENT_ID={$parent_id}");
        }
        if (!empty($update)) {
            foreach ($update as $id => $label) {
                $this->core->db_query("UPDATE " . PREFIX . "ATP_CLASSIFICATORS SET LABEL=:label WHERE ID={$id} AND PARENT_ID={$parent_id} LIMIT 1", array(
                    0, array('name' => 'label', 'value' => $label)));
            }
        }
        if (!empty($insert)) {
            foreach ($insert as $label) {
                $this->core->db_quickInsert(PREFIX . "ATP_CLASSIFICATORS", array(
                    'PARENT_ID' => $parent_id, 'LABEL' => $label, 'VALID' => 1));
            }
        }

        $this->core->set_message('success', $this->core->lng('success_edited'));

        return FALSE;
    }

    /**
     * Vartotojų administravimo teisių saugojimas
     * @authors Martynas Boika, Justinas Malūkas
     * @return boolean
     */
    public function edit_status()
    {

        // Duomenys
        $arg = array(
            'department' => (int) $_POST['user_department_id'],
            'users' => empty($_POST['user_admin_id']) ? array() : (array) $_POST['user_admin_id']
        );

        // Privalomas skyriaus ID
        if (empty($arg['department']) === TRUE) {
            trigger_error('Department ID is missing', E_USER_WARNING);
            return false;
        }

        // Jei nėra vartotojo ID, bet yra skyriaus ID, surenkam visus vartotojus iš skyriaus
        if (empty($arg['users']) === TRUE && empty($arg['department']) === FALSE)
            $arg['users'] = array_keys($this->get_m_users(0, $arg['department'], 'mp.ID'));

        // Privalo būti bent vienas vartotojas
        if (count($arg['users']) === 0) {
            trigger_error('No users to add', E_USER_WARNING);
            return false;
        }

        // Saugo vartotojų susiejimą su teisėmis
        $in_rows = array();
        $del_rows = array();
        for ($i = 0; $i < count($arg['users']); $i++) {
            $del_rows[] = '(M_PEOPLE_ID = ' . (int) $arg['users'][$i] . ' AND M_DEPARTMENT_ID = ' . $arg['department'] . ')';

            $in_rows[] = '('
                . (int) $arg['users'][$i] . ','
                . $arg['department'] . ','
                . 2 . ',' // statusas // 1 yra matymo teisė; 2 - ir redagaimo teisė
                . (($_POST['view_table'] || $_POST['view_relation'] || $_POST['view_classification'] || $_POST['view_competence'] || $_POST['view_document'] || $_POST['view_clause'] || $_POST['view_department']) ? 1 : 0) . ','
                . (int) $_POST['view_table'] . ','
                . (int) $_POST['view_relation'] . ','
                . (int) $_POST['view_classification'] . ','
                . (int) $_POST['view_competence'] . ','
                . (int) $_POST['view_competence'] . ','
                . (int) $_POST['view_competence'] . ','
                . (int) $_POST['view_competence'] . ','
                . (int) $_POST['view_document'] . ','
                . (int) $_POST['view_clause'] . ','
                . 0 . ',' /* (int) $_POST['view_competence'] */ // Skyrių linko nereikia, bet ji kolkas pasiliekam
                . (int) $_POST['view_act'] . ','
                . (int) $_POST['view_deed'] . ','
                . (int) $_POST['view_status'] . ''
                . ')';
        }
        if (count($del_rows)) {
            $qry = 'DELETE FROM ' . PREFIX . 'ATP_USER_STATUS
				WHERE ' . join('OR ', $del_rows);
            $this->core->db_query($qry);
        }
        if (count($in_rows)) {
            $qry = 'INSERT INTO ' . PREFIX . 'ATP_USER_STATUS
				(`M_PEOPLE_ID`, `M_DEPARTMENT_ID`, `STATUS`, `VIEW_ADMIN`, `A_TABLE`,
					`A_RELATION`, `A_CLASSIFICATION`, `A_COMPETENCE`, `A_COMPETENCE_ADMIN`,
					`A_COMPETENCE_COMPETENCIES`, `A_COMPETENCE_USER_RIGHTS`, `A_DOCUMENT`,
					`A_CLAUSE`, `A_DEPARTMENT`, `A_ACT`, `A_DEED`, `A_STATUS`)
				VALUES ' . join(',', $in_rows);
            $this->core->db_query($qry);
        }

        $this->core->set_message('success', $this->core->lng('success_edited'));

        return true;
    }
//    /**
//     * Dokumento įrašo informacija
//     * @deprecated Naudojamas <b>\Atp\Logic2::get_record_table()</b>
//     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
//     * @param string $fields [optional] Laukai, kuriuos gražinti
//     * @return array Dokumento įrašo informacija arba FALSE
//     */
//    public function get_table_record($record, $fields = 'C.*,A.M_PEOPLE_ID,A.M_DEPARTMENT_ID,A.STATUS')
//    {
//        $fields = str_replace(array('tn.', 'rr.', 'tn`.', 'rr`.'), array('C.', 'A.',
//            'C`.', 'A`.'), $fields);
//        $fields = str_replace(array('A.M_PEOPLE_ID', 'A.M_DEPARTMENT_ID'), array(
//            'E.PEOPLE_ID as M_PEOPLE_ID', 'E.STRUCTURE_ID as M_DEPARTMENT_ID'), $fields);
//        $fields = str_replace(array('A.M_PEOPLE_NAME', 'A.M_PEOPLE_SURNAME'), array(
//            'F.FIRST_NAME as M_PEOPLE_NAME', 'F.LAST_NAME as M_PEOPLE_SURNAME'), $fields);
//
//        return $this->core->logic2->get_record_table($record, $fields);
//
//
//
//        $record = $this->core->logic2->get_record_relation($record);
//        if (empty($record) === TRUE) {
//            trigger_error('Record not found', E_USER_ERROR);
//            exit;
//        }
//
//        $table_record = array();
//        $table_name = $this->core->_prefix->table . $record['TABLE_TO_ID'];
//        $fields = str_replace(array('tn.', 'rr.', 'tn`.', 'rr`.'), array('A.', 'B.',
//            'A`.', 'B`.'), $fields);
//        $fields = str_replace(array('B.M_PEOPLE_ID', 'B.M_DEPARTMENT_ID'), array(
//            'D.PEOPLE_ID as M_PEOPLE_ID', 'D.STRUCTURE_ID as M_DEPARTMENT_ID'), $fields);
//        $fields = str_replace(array('B.M_PEOPLE_NAME', 'B.M_PEOPLE_SURNAME'), array(
//            'E.FIRST_NAME as M_PEOPLE_NAME', 'E.LAST_NAME as M_PEOPLE_SURNAME'), $fields);
//
//        $joins = array(
//            'B' => 'INNER JOIN `' . PREFIX . 'ATP_RECORD` B ON B.RECORD_ID = A.RECORD_ID AND B.IS_LAST = 1',
//            'C' => 'INNER JOIN `' . PREFIX . 'ATP_RECORD_X_WORKER` C ON C.RECORD_ID = B.ID',
//            'D' => 'INNER JOIN `' . PREFIX . 'MAIN_WORKERS` D ON D.ID = C.WORKER_ID',
//            'E' => 'INNER JOIN `' . PREFIX . 'MAIN_PEOPLE` E ON E.ID = D.PEOPLE_ID'
//        );
//
//        $params = array(
//            0 => 'A.RECORD_ID = "' . $record['RECORD_ID'] . '"',
//            1 => 'B.TABLE_TO_ID = ' . (int) $record['TABLE_TO_ID'],
//            2 => 'A.RECORD_LAST = 1'
//        );
//        $table_record = $this->core->logic2->get_db(PREFIX . 'ATP_' . $table_name, $params, $limit = true, $order, false, $fields, $joins);
//
//        if (!empty($table_record)) {
//            return $table_record;
//        }
//
//        return FALSE;
//    }

    /**
     * Dokumento įrašo informacija su įvestomis laukų reikšmėmis, pagal priskirtą vartotoją. // TODO: nėra webserviso lauko laukų. O reikia?
     * @param array $arg Array
     * @param int $arg[DOC_ID] Dokumento ID
     * @param mixed $arg[RECORD_NR] [optional] Dokumento įrašo numeris
     * @param int $arg[START] [optional] Nuo kelinto įrašo gražinti rezultatą
     * @param int $arg[OFFSET] [optional] Kiek įrašų gražinti rezultate
     * @param boolen $arg[WITH_EXTRA] [optional] TRUE - pridėti papildomų laukų įrašo reikšmes, kitaip FALSE.<br>Default: TRUE
     * @param boolen $arg[WITH_TEMP] [optional] <p>Ar išrinkti laikinus įrašus <b>Default:</b> TRUE</p>
     * @param boolen $arg[INCLUDE_OWNER] [optional] <p>Ar išrinti informaciją tenkinant vartotojų teises <b>Default:</b> TRUE</p>
     * @param boolen $arg[LAST] [optional] <p>Ar turi tenkinti sąlygą IS_LAST = 1<b>Default:</b> FALSE</p>
     * @return boolen|array
     * @see This is bullshit. don't use
     */
    public function get_table_records($arg)
    {
        $default = array(
            'DOC_ID' => null,
            'RECORD_NR' => null,
            'START' => null,
            'OFFSET' => null,
            'WITH_EXTRA' => true,
            'WITH_TEMP' => true,
            'INCLUDE_OWNER' => true,
            'LAST' => false
        );
        $arg = array_merge($default, $arg);

        $record = $this->core->logic2->get_record_relation($arg['RECORD_NR']);
        if (empty($record) === FALSE) {
            $arg['DOC_ID'] = $record['TABLE_TO_ID'];
            $arg['RECORD_NR'] = $record['RECORD_ID'];
            $arg['RECORD_ID'] = $record['ID'];
        } elseif (empty($arg['DOC_ID']) === TRUE) {
            trigger_error('Document ID is not set', E_USER_WARNING);
            return false;
        }


        $table_records = array();
        $table_name = $this->core->_prefix->table . $arg['DOC_ID'];
        //$fields = 'tn.*, rr.STATUS, rr.IS_PAID, rr.DOC_STATUS, rr.ID RID';
        $fields = 'tn.*, rr.STATUS, rr.DOC_STATUS, rr.ID RID';
        if ($arg['INCLUDE_OWNER'] === TRUE) {
            $fields .= ',rr.M_PEOPLE_ID,rr.M_DEPARTMENT_ID';
        }

        $fields = str_replace(array('tn.', 'rr.', 'tn`.', 'rr`.'), array('G.', 'A.',
            'G`.', 'A`.'), $fields);
        $fields = str_replace(array('A.M_PEOPLE_ID', 'A.M_DEPARTMENT_ID'), array(
            'D.PEOPLE_ID as M_PEOPLE_ID', 'D.STRUCTURE_ID as M_DEPARTMENT_ID'), $fields);

        $joins = array(
            'F' => 'INNER JOIN `' . PREFIX . 'ATP_RECORD_X_RELATION` F ON F.RELATION_ID = A.ID',
            //'B' => 'INNER JOIN `' . PREFIX . 'ATP_RECORD` B ON B.RECORD_ID = A.RECORD_ID AND B.IS_LAST = 1 ',
            'G' => 'INNER JOIN `' . PREFIX . 'ATP_TBL_' . (int) $arg['DOC_ID'] . '` G ON G.ID = F.RECORD_ID ',
            'C' => 'INNER JOIN `' . PREFIX . 'ATP_RECORD_X_WORKER` C ON	C.RECORD_ID = A.ID',
            'D' => 'INNER JOIN `' . PREFIX . 'MAIN_WORKERS` D ON D.ID = C.WORKER_ID',
            'E' => 'LEFT JOIN `' . PREFIX . 'ATP_RECORD_UNSAVED` E ON E.RECORD_NR = A.RECORD_ID'
        );

        $params = array(
            0 => 'A.TABLE_TO_ID = ' . (int) $arg['DOC_ID']//,
            //1 => 'A.IS_LAST = 1'
        );
        if ($arg['LAST'] === TRUE) {
            $params[] = 'A.IS_LAST = 1';
        }

        if ($arg['INCLUDE_OWNER'] === TRUE) {
            $params[2] = 'D.ID = ' . (int) $this->core->factory->User()->getLoggedPersonDuty()->id;
        }

        // Kad neišrinkinėtu laikinų
        if ($arg['WITH_TEMP'] === FALSE) {
            $params[3] = 'E.RECORD_NR IS NULL';
            $fields .= ' , E.RECORD_NR';
        }
        if (!empty($arg['RECORD_ID'])) {
            $params[4] = 'A.ID = ' . (int) $arg['RECORD_ID'] . '';
            $params[4] .= ' LIMIT 1';
        } elseif (!empty($arg['OFFSET'])) {
            $params[4] = '1 = 1';
            $params[4] .= ' LIMIT ' . $arg['START'] . ', ' . $arg['OFFSET'];
        }

        $rows = $this->core->logic2->get_db(PREFIX . 'ATP_RECORD', $params, $limit = false, $order = null, false, $fields, $joins);
        foreach ($rows as $row) {
            // TODO: this is bull
            //
            // Jei f-jos parametruose nenurodytas RECORD_NR, tai nesurenka papildomu lauku reiksmiu, o turetu surinkti
            //if ($arg['WITH_EXTRA'] && !empty($arg['RECORD_NR'])) {
            if ($arg['WITH_EXTRA']) {
                $fields = $this->get_table_fields($arg['DOC_ID']);
                // Surenka visus papildomus laukus, kurie gali būti dokumente
                $extra_fields = $this->get_extra_fields_by_table($arg['DOC_ID'], 'DOC', 'by_doc');
                // Atrenkami papildomi laukai pridedami prie strukturos
                foreach ($fields as $structure) {
                    if ($structure['SORT'] === '4' && $structure['TYPE'] === '18') {
                        $class_val = $row[$structure['NAME']];

                        // Reikia, kad laukas būtų užpildytas ir kai nepasirinktas klasifikatorius
                        if (empty($class_val) === FALSE) {
                            foreach ((array) $extra_fields['CLASS'][$structure['SOURCE']][$class_val] as $key => $arr) {
//                                $row[$arr['NAME']] = $this->get_extra_field_value($row['RID'], $arr['ID']);
                                $row[$arr['NAME']] = $this->core->factory->Record()->getFieldValue($row['RID'], $arr['ID']);
                            }
                        } else {
                            foreach ((array) $extra_fields['CLASS'][$structure['SOURCE']] as $id => $class) {
                                foreach ($class as $key => $arr) {
//                                    $row[$arr['NAME']] = $this->get_extra_field_value($arg['DOC_ID'], $row['ID'], $arr['ID']);
                                    $row[$arr['NAME']] = $this->core->factory->Record()->getFieldValue($row['RID'], $arr['ID']);
                                }
                            }
                        }
                    } elseif ($structure['SORT'] === '4' && $structure['TYPE'] === '23') {
                        $class_val = $row[$structure['NAME']];
                        // Reikia, kad laukas būtų užpildytas ir kai nepasirinktas klasifikatorius
                        if (empty($class_val) === FALSE && isset($extra_fields['DEED'])) {
                            foreach ((array) $extra_fields['DEED'][$class_val] as $key => $arr) {
//                                $row[$arr['NAME']] = $this->get_extra_field_value($arg['DOC_ID'], $row['ID'], $arr['ID']);
                                $row[$arr['NAME']] = $this->core->factory->Record()->getFieldValue($row['RID'], $arr['ID']);
                            }
                        } else {
                            if (isset($extra_fields['DEED'])) {
                                foreach ((array) $extra_fields['DEED'] as $id => $class) {
                                    foreach ($class as $key => $arr) {
//                                        $row[$arr['NAME']] = $this->get_extra_field_value($arg['DOC_ID'], $row['ID'], $arr['ID']);
                                        $row[$arr['NAME']] = $this->core->factory->Record()->getFieldValue($row['RID'], $arr['ID']);
                                    }
                                }
                            }
                        }
                    } elseif ($structure['SORT'] === '6' && $structure['TYPE'] === '19') {
                        foreach ($extra_fields['CLAUSE'] as $key => $arr) {
                            foreach ($arr as $a => $r) {
//                                $row[$r['NAME']] = $this->get_extra_field_value($arg['DOC_ID'], $row['ID'], $r['ID']);
                                $row[$r['NAME']] = $this->core->factory->Record()->getFieldValue($row['RID'], $r['ID']);
                            }
                        }
                    }
                }
            }

            // Jei AN ivykdytas - praleidziam
            if (isset($row['STATUS']) && (int) $row['STATUS'] === \Atp\Record::STATUS_FINISHED && $row['M_PEOPLE_ID'] === $this->core->factory->User()->getLoggedUser()->person->id) {
                continue;
            }
            $table_records[] = $row;
        }

        return $table_records;
    }

    /**
     * Surenka dokumentus, kuriais gali judėti dokumento įrašai.<br/>
     * Rezultate: array('prev_parth' => array($document_id_arr...), 'next_path' => array($document_id_arr...))
     * @param int $id Judančio dokumento ID
     * @param boolean $return_ancestors Should "prev_path" be returned. Default: false
     * @return boolean|array Dokumentų duomenys ['prev_path' => [ [ID=>...], [ID=>...] ... ], 'next_path' => [ [ID=>...], [ID=>...] ... ]]
     */
    public function get_table_path($id, $return_ancestors = false)
    {
        if (empty($id) === TRUE) {
            trigger_error('Document ID is empty');
            return FALSE;
        }
        $docs = $this->core->logic2->get_document();

        $path = array();
        if ($return_ancestors) {
            $path['prev_path'] = array();

            $cnt = 0;
            $go = TRUE;
            $parent_id = $id;
            while ($go && empty($parent_id) === FALSE) {
                if (++$cnt > 1000) {
                    trigger_error('Infinity', E_USER_ERROR);
                    break;
                } else
                if ($docs[$parent_id]) {
                    $parent_id = $docs[$parent_id]['PARENT_ID'];
                    if ($parent_id > 0)
                        array_unshift($path['prev_path'], $docs[$parent_id]);
                } else {
                    $go = FALSE;
                }
            };

            if (empty($path['prev_path'])) {
                unset($path['prev_path']);
            }
        }

        $cnt = 0;
        $go = TRUE;
        $level = 0;
        $parent_id = array();
        while ($go) {
            if (++$cnt > 1000) {
                trigger_error('Infinity', E_USER_ERROR);
                break;
            }

            $found_match = FALSE;
            foreach ($docs as &$doc) {
                if ($doc['PARENT_ID'] == $id) {
                    $level++;
                    $found_match = TRUE;

                    $parent_id[$level] = $doc['ID'];
                    $path['next_path'][] = $doc;

                    $doc = NULL;

                    break;
                }
            }

            if (!$found_match) {
                if (!empty($parent_id[$level])) {
                    $id = $parent_id[$level];
                    $parent_id[$level] = 0;
                    $level--;
                } else {
                    $go = FALSE;
                }
            }
        }

        return $path;
    }

    /**
     *
     * @param mixed $record  Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @return null|array
     */
    public function get_record_path($record)
    {
        $record = $this->core->logic2->get_record_relation($record);
        // Gauna kelyje esančius dokumentų įrašus
        $records = $this->record_path($record['TABLE_TO_ID'], $record);

        if (count($records) === 0) {
            return;
        }

        $workerManager = new \Atp\Record\Worker($this->core);

        // Surenka papildomus duomenis
        foreach ($records as $key => $record) {
            $qry = 'SELECT A.ID, A.RECORD_ID, A.RECORD_FROM_ID, A.TABLE_TO_ID, A.CREATE_DATE, A.STATUS, A.DOC_STATUS,
					C.RECORD_MAIN_ID, C.RECORD_VERSION,
					D.STATUS as DOCUMENT_STATUS
				FROM
					' . PREFIX . 'ATP_RECORD A
						INNER JOIN ' . PREFIX . 'ATP_RECORD_X_RELATION B
							ON
								B.RELATION_ID = A.ID AND
								B.DOCUMENT_ID = :DOCUMENT_ID
						INNER JOIN ' . PREFIX . ':TABLE_NAME C ON C.ID = B.RECORD_ID
						INNER JOIN ' . PREFIX . 'ATP_DOCUMENTS D ON D.ID = A.TABLE_TO_ID
				WHERE
					A.ID = :ID';

            $result = $this->core->db_query_fast($qry, array(
                '#TABLE_NAME#' => 'ATP_TBL_' . $record['TABLE_TO_ID'],
                'DOCUMENT_ID' => $record['TABLE_TO_ID'],
                'ID' => $record['ID']));

            if (!$this->core->db_num_rows($result)) {
                unset($records[$key]);
                continue;
            }

            $row = $this->core->db_next($result);
            $records[$key] = array_merge($record, (array) $row);
        }

        return $records;
    }

    /**
     *
     * @param type $record
     * @return type
     */
    function record_path_last($record)
    {
        // Jei paduodamas dokumento numeris, tai ims ne pradinę įrašo versiją, o aktualią (paskutinę)
        $record = $this->core->logic2->get_record_relation($record);
        if (empty($record) === TRUE)
            return;

        $path = $this->get_record_path($record);
        if (empty($path) === TRUE)
            return;

        usort($path, function($a, $b) {
            return strtotime($a['CREATE_DATE']) < strtotime($b['CREATE_DATE']);
        });
        $last_path = current($path);
        return $this->core->logic2->get_record_relation($last_path['ID']);
    }

    /**
     * Dokumento įrašo kelyje esantys dokumentų įrašai
     * @param int $document_id Dokumento ID
     * @param mixed $record  Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @return null|array Dokumentų įrašų informacija.<br/>Min.: array(ID, TABLE_FROM_ID, TABLE_TO_ID)
     */
    function record_path($document_id, $record)
    {
        $record = $this->core->logic2->get_record_relation($record);
        // Gauna įrašo pagrindini (pradinį) numerį, kuris nekinta ryšiu susietuose dokumento įrašuose
        $qry = 'SELECT C.RECORD_MAIN_ID
				FROM
					' . PREFIX . 'ATP_RECORD A
						INNER JOIN ' . PREFIX . 'ATP_RECORD_X_RELATION B ON B.RELATION_ID = A.ID AND B.DOCUMENT_ID = :DOCUMENT_ID
						INNER JOIN ' . PREFIX . ':TABLE_NAME C ON C.ID = B.RECORD_ID
				WHERE
					A.ID = :ID
				LIMIT 1';
        $result = $this->core->db_query_fast($qry, array(
            '#TABLE_NAME#' => 'ATP_TBL_' . $document_id,
            'DOCUMENT_ID' => $document_id,
            'ID' => $record['ID']));
        $row = $this->core->db_next($result);
        if (empty($row) === TRUE)
            return;

        // Pradinio dokumento informacija
        // Blogai: gauna tik aktualią pradinio įrašo versiją
        //$first_record = $this->core->logic2->get_record_relation($row['RECORD_MAIN_ID']);
        // Gali būti blogai: rikiuoja pagal sukūrimo datą, kai reikia rikiuoti pagal versiją
        //$first_record = $this->core->logic2->get_record_relations(array('RECORD_ID' => $row['RECORD_MAIN_ID']), true, 'CREATE_DATE DESC');
        $qry = 'SELECT A.ID, A.TABLE_FROM_ID, A.TABLE_TO_ID
				FROM
					' . PREFIX . 'ATP_RECORD A
						INNER JOIN ' . PREFIX . 'ATP_RECORD_X_RELATION B ON B.RELATION_ID = A.ID AND B.DOCUMENT_ID = :DOCUMENT_ID
						INNER JOIN ' . PREFIX . ':TABLE_NAME C ON C.ID = B.RECORD_ID
				WHERE
					A.RECORD_ID = :RECORD_ID
				ORDER BY C.RECORD_VERSION ASC
				LIMIT 1';
        $document_id = current(explode('p', $row['RECORD_MAIN_ID']));
        $result = $this->core->db_query_fast($qry, array(
            '#TABLE_NAME#' => 'ATP_TBL_' . $document_id,
            'DOCUMENT_ID' => $document_id,
            'RECORD_ID' => $row['RECORD_MAIN_ID']));
        $first_record = $this->core->db_next($result);

        // Nuo pradinio dokumento surenka visus ryšiu susietus dokumento įrašs
        $data = $this->record_path_forward($first_record['ID']);
        $data[$first_record['ID']] = $first_record;

        return $data;
    }

    /**
     * Suranda vėlesnius ryšiu susijusius dokumentų įrašus
     * @param int $recordId Dokumento įrašo ID
     * @return array Dokumentų įrašų informacija.<br/>Min.: array(ID, TABLE_FROM_ID, TABLE_TO_ID)
     */
    public function record_path_forward($recordId, &$ret = array())
    {
        if (empty($ret) === TRUE) {
            $this->loop = 0;
        }

        $result = $this->core->db_query_fast('
            SELECT A.ID, A.TABLE_FROM_ID, A.TABLE_TO_ID
            FROM
                ' . PREFIX . 'ATP_RECORD A
            WHERE
                A.RECORD_FROM_ID = :RECORD_FROM_ID', ['RECORD_FROM_ID' => $recordId]);
        while ($row = $this->core->db_next($result)) {
            if ($this->loop > 500) {
                trigger_error('Fatal error: Terminated due to stacked loop', E_USER_ERROR);
            }
            $this->loop++;

            $ret[$row['ID']] = $row;

            $this->record_path_forward($row['ID'], $ret);
        }

        return $ret;
    }

    /**
     * Suranda ryšiu susijusius dokumentų įrašus. Pirma suranda visus iki pradinį ir tada nuo pradinio perieško į priekį
     * @deprecated Overkill. Nebenaudojamas ir nenaudoti. Kadangi ieškoma į priekį iki pat galo, rezultatas nebeatitinka pavadinimo.
     * @param int $record_id Dokumento įrašo ID
     * @return array
     */
    private function record_path_backward($record_id, &$ret = array())
    {
        $qry = 'SELECT A.ID, A.RECORD_FROM_ID, A.TABLE_FROM_ID, A.TABLE_TO_ID
			FROM
				' . PREFIX . 'ATP_RECORD A
			WHERE
				A.ID = :ID';
        $result = $this->core->db_query_fast($qry, array('ID' => $record_id));

        while ($row = $this->core->db_next($result)) {
            if ($this->i > 500) {
                trigger_error('Fatal error: Terminated due to stacked loop', E_USER_ERROR);
            }
            $this->i++;

            $row['parent_id'] = $row['RECORD_FROM_ID'];
            $ret[$row['ID']] = $row;
            if (empty($row['RECORD_FROM_ID']))
                continue;

            $this->record_path_backward($row['RECORD_FROM_ID'], $ret);
        }

        // Kai reikia pereit per visus atbuline tvarka rastus dokumentus, nes kiekvienas iš jų gali turėti vaikų // Overkill
        $last = end($ret);
        $this->record_path_forward($last['ID'], $ret);

        return $ret;
    }

    /**
     * Gauna lauko sąryšio tipą
     * @author Martynas Boika
     * @param int $column_id Lauko ID
     * @param array $column_relations Sąryšių masyvas <b>$this->managa->table_column_relation</b>
     * @return int 1 - Tėvinis laukas. 2 - Nėra sąryšio.
     * @return boolean FALSE - vaikas
     */
    public function get_column_relation_status($column_id, $column_relations)
    {
        $is_parent = FALSE;
        $is_child = FALSE;
        foreach ($column_relations as $relation) {
            if ($relation['COLUMN_PARENT_ID'] == $column_id)
                $is_parent = TRUE;
            elseif ($relation['COLUMN_ID'] == $column_id)
                $is_child = TRUE;

            if ($is_child)
                break;
        }

        if ($is_parent && !$is_child)
            return 1;
        elseif (!$is_parent && !$is_child)
            return 2;

        return FALSE;
    }

    /**
     *
     * @author Martynas Boika
     * @param int $column_id
     * @param type $column_relations
     * @return type
     */
    public function get_column_relation_order($column_id, $column_relations)
    {
        $relations_arr = array();
        foreach ($column_relations as $relation) {
            if ($relation['COLUMN_PARENT_ID'] == $column_id) {
                $column_relation = $this->get_column_relation_order($relation['COLUMN_ID'], $column_relations);
                $relations_arr[$relation['COLUMN_ID']] = $column_relation;
            }
        }

        return $relations_arr;
    }

    /**
     * @deprecated Nenaudojamas
     * @return array
     */
    public function get_field_types()
    {
        trigger_error('get_field_types() is deprecated', E_USER_DEPRECATED);
        $field_types = array();
        $sql = "SELECT * FROM " . PREFIX . "ATP_FIELD_TYPE";
        $qry = $this->core->db_query($sql);
        while ($res = $this->core->db_next($qry))
            $field_types[$res['ID']] = $res;

        return $field_types;
    }

    // TODO: nenaudojamas. Ryšių kolkas nebereikia
    public function get_clause_structures()
    {
        $clause_structures = array(
            array('ID' => 1, 'LABEL' => 'Dalis'),
            array('ID' => 2, 'LABEL' => 'Punktas'),
            array('ID' => 3, 'LABEL' => 'Papunktis'),
            array('ID' => 4, 'LABEL' => 'Pavadinimas'),
            array('ID' => 5, 'LABEL' => 'Bauda (nuo - iki)'),
            array('ID' => 6, 'LABEL' => 'Skiriamas įspėjimas'),
            array('ID' => 7, 'LABEL' => 'Įspėjimo tekstas'),
            array('ID' => 8, 'LABEL' => 'Terminas')
        );

        return $clause_structures;
    }

    public function get_field_sort()
    {
        $field_sort = array(
            1 => array('ID' => 1, 'LABEL' => 'Skaitiniai tipai', 'TYPE' => array(
                    1, 2, 3, 4, 5, 6)),
            2 => array('ID' => 2, 'LABEL' => 'Tekstiniai tipai', 'TYPE' => array(
                    7, 8, 9, 10)),
//            3 => array('ID' => 3, 'LABEL' => 'Web servisas', 'TYPE' => array(1, 2,
//                    3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16/* , 17 */), 'SORT' => 'webservice'),
            4 => array('ID' => 4, 'LABEL' => 'Iškrentantis sąrašas', 'TYPE' => array(
                    18, 22, 23)),
            5 => array('ID' => 5, 'LABEL' => 'Varnelė', 'TYPE' => array(18)),
            6 => array('ID' => 6, 'LABEL' => 'Straipsnis', 'TYPE' => array(19), 'HIDE_TYPE' => true,
                'DEFAULT_TYPE' => 19), //,20,21,22,23,24,25,26,27,28,29)),
            7 => array('ID' => 7, 'LABEL' => 'Datos ir laiko tipai', 'TYPE' => array(
                    11, 12, 13, 14)),
            /* 8 => array('ID' => 8, 'LABEL' => 'Loginis pasirinkimas', 'TYPE' => array(18)), */ // Radiobox
            9 => array('ID' => 9, 'LABEL' => 'Antraštė', 'TYPE' => array(8)),
            10 => array('ID' => 10, 'LABEL' => 'Failas', 'TYPE' => array(20, 21)),
            11 => array('ID' => 11, 'LABEL' => 'Geografinė padėtis', 'TYPE' => array(
                    7))
        );

        return $field_sort;
    }

    public function get_field_type()
    {
        $field_type = array(
            1 => array('ID' => 1, 'LABEL' => 'Smulkus sveikasis skaičius (3 skait.)',
                'VALUE' => 'tinyint'), //3
            2 => array('ID' => 2, 'LABEL' => 'Mažas sveikasis skaičius (5 skait.)',
                'VALUE' => 'smallint'), //5
            3 => array('ID' => 3, 'LABEL' => 'Vidutinis sveikasis skaičius (7 skait.)',
                'VALUE' => 'mediumint'), //7
            4 => array('ID' => 4, 'LABEL' => 'Sveikasis skaičius (11 skait.)', 'VALUE' => 'int'), //11
            5 => array('ID' => 5, 'LABEL' => 'Didelis sveikasis skaičius (19 skait.)',
                'VALUE' => 'bigint'), //19
            6 => array('ID' => 6, 'LABEL' => 'Skaičius su kableliu', 'VALUE' => 'float'),
            7 => array('ID' => 7, 'LABEL' => 'Smulkus tekstas (256 simb.)', 'VALUE' => 'tinytext'), //256
            8 => array('ID' => 8, 'LABEL' => 'Tekstas (65 tūkst. simb.)', 'VALUE' => 'text'), //65,535
            9 => array('ID' => 9, 'LABEL' => 'Vidutinis tekstas (16 mil. simb.)',
                'VALUE' => 'mediumtext'), //16,777,215
            10 => array('ID' => 10, 'LABEL' => 'Ilgas tekstas (4 mlrd. simb.)', 'VALUE' => 'longtext'), //4,294,967,295
            11 => array('ID' => 11, 'LABEL' => 'Metai', 'VALUE' => 'year'),
            12 => array('ID' => 12, 'LABEL' => 'Data', 'VALUE' => 'date'),
            13 => array('ID' => 13, 'LABEL' => 'Laikas', 'VALUE' => 'time'),
            14 => array('ID' => 14, 'LABEL' => 'Data ir laikas', 'VALUE' => 'datetime'),
            15 => array('ID' => 15, 'LABEL' => 'Laiko žyma', 'VALUE' => 'timestamp'),
            16 => array('ID' => 16, 'LABEL' => 'Loginis pasirinkimas', 'VALUE' => 'boolean'),
            //17=>array('ID'=>17,'LABEL'=>'Straipsnis','VALUE'=>'tinytext'),
            18 => array('ID' => 18, 'LABEL' => 'Klasifikatorius', 'VALUE' => 'tinytext'),
            19 => array('ID' => 19, 'LABEL' => 'Straipsnis', 'VALUE' => 'tinytext'),
            20 => array('ID' => 20, 'LABEL' => 'Nuotrauka', 'VALUE' => 'int'),
            21 => array('ID' => 21, 'LABEL' => 'Dokumentas', 'VALUE' => 'int'),
            22 => array('ID' => 22, 'LABEL' => 'Teisės aktas', 'VALUE' => 'tinytext'),
            23 => array('ID' => 23, 'LABEL' => 'Veika', 'VALUE' => 'tinytext')
        );

        return $field_type;
    }

    public function get_web_services()
    {
        $webservices = $this->core->logic2->get_ws()->get_infos();

        $ret = array();
        foreach ($webservices as $webservice) {
            $ret[$webservice['id']] = array(
                'ID' => $webservice['id'],
                'LABEL' => $webservice['label']
            );
        }

        return $ret;
    }

    /**
     * Dokumento įrašų statusai<br>ID, SHORT ir NAME privalo būti unikalūs
     * @authors Martynas Boika, Justinas Malūkas
     * @return array
     */
    public function get_document_statuses()
    {
        return array(
            0 => array('ID' => 0, 'SHORT' => 'del', 'NAME' => 'deleted', 'LABEL' => $this->core->lng('status_deleted')),
            1 => array('ID' => 1, 'SHORT' => 'disc', 'NAME' => 'discontinued', 'LABEL' => $this->core->lng('status_discontinued')),
            2 => array('ID' => 2, 'SHORT' => 'invest', 'NAME' => 'investigating',
                'LABEL' => $this->core->lng('status_investigating')),
            3 => array('ID' => 3, 'SHORT' => 'exam', 'NAME' => 'examining', 'LABEL' => $this->core->lng('status_examining')),
            4 => array('ID' => 4, 'SHORT' => 'examd', 'NAME' => 'examined', 'LABEL' => $this->core->lng('status_examined')),
            5 => array('ID' => 5, 'SHORT' => 'fin', 'NAME' => 'finished', 'LABEL' => $this->core->lng('status_finished')),
            6 => array('ID' => 6, 'SHORT' => 'idle', 'NAME' => 'idle', 'LABEL' => $this->core->lng('status_idle'))//,
            //999 => array('ID' => 999, 'LABEL' => 'Baigta')
        );
    }

    /**
     * Suranda dokumento statusą
     * @author Justinas Malūkas
     * @param string|array $opt Paieškos parametras (-ai)<br><b>string</b> - reikšmė paieškai pagal 'SHORT' stuleplį<br><b>array</b> - pagal ID, SHORT arba NAME
     * @param null|string $return <b>null</b> - gražina visus duomenis<br><b>string</b> - jei randa, grąžina nurodytą lauką
     * @example find_document_status(array('ID' => 1));<br>find_document_status(array('SHORT' => 'invest'));<br> find_document_status('invest');
     * @return boolean <b>FALSE</b> klaidos atveju
     * @return null Statusas nerastas
     * @return array Rasto statuso duomenys
     */
    public function find_document_status($opt, $return = null)
    {
        if (empty($opt) === TRUE) {
            trigger_error('Search parameter is empty', E_USER_WARNING);
            return false;
        }
        if (is_array($opt) === TRUE) {
            if (count($opt) > 1)
                trigger_error('Only first element of supplied parameter is used', E_USER_WARNING);

            $val = reset($opt);
            $key = strtoupper(key($opt));
            if (in_array($key, array('ID', 'SHORT', 'NAME')) === FALSE)
                return false;
        } else {
            $val = $opt;
            $key = 'SHORT';
        }

        $statuses = $this->get_document_statuses();

        if (in_array($return = strtoupper($return), array_keys($statuses[1])) === FALSE)
            $return = null;

        foreach ($statuses as $status) {
            if (isset($status[$key]) && $status[$key] == $val)
                return $return === null ? $status : $status[$return];
        }
        return null;
    }

    /**
     *
     * @param array $check
     * @param array $data
     * @param int $data[table_id]
     * @param int $data[worker_id] If provided, user_id and department_id is not needed
     * @param int $data[user_id]
     * @param int $data[department_id]
     * @return boolean
     */
    public function check_privileges($check, $data)
    {
        if (empty($check) || (isset($check['AND']) && empty($check['AND']) ) || (isset($check['OR']) && empty($check['OR']))) {
            trigger_error('Rights not supplied or supplied incorrectly', E_USER_WARNING);
            return false;
        }
        extract($data);
        if (empty($table_id)) {
            trigger_error('Table ID is not supplied', E_USER_WARNING);
            return false;
        }
        if (empty($worker_id) === false) {
            $userHelper = $this->core->factory->User();
            /* @var $user \Atp\Entity\User */
            $user = $userHelper->getUserDataByPersonDutyId($worker_id);
            $personDuty = $userHelper->getPersonDuty($worker_id);

            $user_id = $user->person->id;
            $department_id = $personDuty->department->id;
        }
        if (empty($user_id)) {
            trigger_error('User ID is not supplied', E_USER_WARNING);
            return false;
        }
        if (empty($department_id)) {
            trigger_error('Department ID is not supplied', E_USER_WARNING);
            return false;
        }

        // Gauna vartotojo - dokumento teises
        $privileges = null;
        if ($user_id == $this->core->factory->User()->getLoggedUser()->person->id && $department_id == $this->core->factory->User()->getLoggedPersonDuty()->department->id) {
            if (!empty($this->core->factory->User()->getLoggedPersonDutyPrivileges($table_id))) {
                $privileges = $this->core->factory->User()->getLoggedPersonDutyPrivileges($table_id);
            }
        } else {
            $privileges = $this->core->factory->User()->getUserPrivileges($user_id, $table_id, $department_id);
        }

        // Jei nėra, tai nėra
        if (empty($privileges)) {
            return false;
        }

        // Patikrina ar turi nurodytas teises
        if (is_array($check) === false) {
            return (bool) $privileges[$check];
        }

        // Sudaro taisykles
        if (isset($check['AND']) === false && isset($check['OR']) === false) {
            $check = ['AND' => $check];
        }

        // Suteikia teises pagal taisykles
        $valid = ['AND' => true, 'OR' => true];

        // Tikrina kiekvieną taisyklę
        foreach ($check as $type => $rights) {
            $valid[$type] = 0; //  atema pradinę teisę
            $count = count($check[$type]);
            for ($i = 0; $i < $count; $i++) {
                if ((bool) $privileges[$rights[$i]] === true) {
                    $valid[$type] ++;
                    if ($type === 'OR') {
                        break;
                    }
                }
            }
            if ($type === 'AND') {
                $valid[$type] = ($valid[$type] === $count);
            } elseif ($type === 'OR')
                $valid[$type] = (bool) $valid[$type];
        }

        return $valid['AND'] === TRUE && $valid['OR'] === TRUE;
    }

    /**
     * Pagal tipą tikrinamos vartotojo teisės
     * @author Justinas Malūkas
     * @param string $type Tipo pavadinimas
     * @param int $table_id Dokumento ID<br>Jei parametras nepaduotas, jis ieškomas $this->logic->_r_table_id
     * @param int $user_id Varotojo ID<br>Jei parametras nepaduotas, jis ieškomas $this->core->factory->User()->getLoggedUser()->person->id
     * @example <b>privileges('table_records', 9, 1);</b> - visi parametrai paduoti<br><b>privileges('table_records');</b> - ieškos nenurodytų parametrų<br><b>privileges('table_records', null, null);</b> - nenaudos parametrų pagal nutylėjima, kadangi parametrai yra paduoti kaip NULL'ai
     * @return function Teisių tikrinimo rezultatas
     */
    //public function privileges($type, $table_id = null, $user_id = null, $department_id = null) {
    /**
     * Pagal tipą tikrinamos vartotojo teisės
     * @author Justinas Malūkas
     * @param string $type Tipo pavadinimas
     * @param array $data
     * @param int $data[table_id] Dokumento ID<br>Jei parametras nepaduotas, jis ieškomas $this->logic->_r_table_id
     * @param int $data[user_id] Varotojo ID<br>Jei parametras nepaduotas, jis ieškomas $this->core->factory->User()->getLoggedUser()->person->id
     * @param int $data[department_id] Skyriaus ID<br>Jei parametras nepaduotas, jis ieškomas $this->core->department->id
     * @param int $data[record] [removed]
     * @param int $data[record_id]
     * @param int $data[table] [removed]
     * @example <b>privileges('table_records', array('table_id' => 9, 'user_id' => 1, 'department_id' => 1));</b> - visi parametrai paduoti<br><b>privileges('table_records');</b> - ieškos nenurodytų parametrų<br><b>privileges('table_records', array('table_id' => null, 'user_id' => null, 'department_id' => null));</b> - nenaudos parametrų pagal nutylėjima, kadangi parametrai yra nurodyti
     * @return function Teisių tikrinimo rezultatas
     */
    public function privileges($type, $data = array())
    {
        if (empty($type) === TRUE) {
            trigger_error('Type is empty ', E_USER_WARNING);
            return false;
        }
        extract($data);
        // Jei parametrai visiškai nepaduodamai, tai pagal nutylėjimą nurodomos jų reikšmės
        //$args = func_get_args();
        //if (array_key_exists(1, $args) === FALSE && empty($this->_r_table_id) === FALSE)

//        if (array_key_exists('table_id', $data) === FALSE && empty($this->_r_table_id) === FALSE) {
//            $data['table_id'] = $table_id = $this->_r_table_id;
//        }
//        //if (array_key_exists(2, $args) === FALSE && empty($this->core->factory->User()->getLoggedUser()->person->id) === FALSE) {
//        if (array_key_exists('user_id', $data) === FALSE && empty($this->core->factory->User()->getLoggedUser()->person->id) === FALSE) {
//            $data['user_id'] = $user_id = $this->core->factory->User()->getLoggedUser()->person->id;
//        }
//        //if (array_key_exists(3, $args) === FALSE && empty($this->core->factory->User()->getLoggedPersonDuty()->department->id) === FALSE) {
//        if (array_key_exists('department_id', $data) === FALSE && empty($this->core->factory->User()->getLoggedPersonDuty()->department->id) === FALSE) {
//            $data['department_id'] = $department_id = $this->core->factory->User()->getLoggedPersonDuty()->department->id;
//        }
////        if (array_key_exists('record', $data) === FALSE && empty($this->core->manage->table_record) === FALSE) {
////            $data['record'] = $record = $this->core->manage->table_record;
////        }
//        if(array_key_exists('record', $data) === false) {
//            throw new \Exception('Method depends on removed parameter');
//        }
////        if (array_key_exists('table', $data) === FALSE && empty($this->core->manage->table) === FALSE) {
////            $data['table'] = $table = $this->core->manage->table;
////        }
//        if(array_key_exists('table', $data) === false) {
//            throw new \Exception('Method depends on removed parameter');
//        }
//         if (array_key_exists('record_id', $data) === FALSE && empty($this->_r_r_id) === FALSE) {
//            $data['record_id'] = $record_id = $this->_r_record_id;
//        }

        if(empty($table_id)) {
            throw new \Exception('Missing document ID.');
        }
        if(empty($user_id)) {
            throw new \Exception('Missing user ID.');
        }
        if(empty($department_id)) {
            throw new \Exception('Missing department ID.');
        }

        $table = $this->core->logic2->get_document($table_id);

        $r = null;
        $extra = true;
        switch ($type) {
            // Dokumentų sąraše pasirinkus dokumentą (įrašų sąrašas)
            case 'table_records':
            // Šoniniame meniu dokumentas ties dokumentų sąrašu
            case 'atp_menu_see_records_table_link':
                $r = array('OR' => array('VIEW_RECORD', 'EDIT_RECORD', 'DELETE_RECORD',
                        'INVESTIGATE_RECORD', 'EXAMINATE_RECORD', 'END_RECORD'));
                break;
            // Dokumentų sąraše pasirinkus dokumentą (įrašų sąrašas) mygtukas "Sukurti naują"
            case 'table_records_button_new':
                $r = array('OR' => array('EDIT_RECORD', 'INVESTIGATE_RECORD'));
                break;
            // Dokumentų sąraše pasirinkus dokumentą (įrašų sąrašas) lentelės mygtukas "Trinti"
            case 'table_record_button_delete':
            case 'delete_record':
                $r = 'DELETE_RECORD';
                break;
            // Dokumentų sąraše pasirinkus dokumentą (įrašų sąrašas) lentelės mygtukas "Peržiūrėti"
            case 'table_record_button_view':
            // Dokumento įrašo peržiura
            case 'table_record_edit':
                $r = array('OR' => array('VIEW_RECORD', 'EDIT_RECORD', 'INVESTIGATE_RECORD',
                        'EXAMINATE_RECORD', 'END_RECORD'));
                break;
            // Dokumentų sąraše pasirinkus dokumentą (įrašų sąrašas) lentelės mygtukas "Atnaujinti"
            case 'table_record_button_update':
            // Dokumento įrašo apmokėjimų peržiūros / atnaujinimo langas
            case 'table_record_update':
                $r = array('OR' => array('EDIT_RECORD', 'INVESTIGATE_RECORD', 'EXAMINATE_RECORD'));
                // Leidžiama ne tik tarpinėje būsenoje
                //$extra = ((int) $data['record']['STATUS'] === \Atp\Record::STATUS_IDLE);
                break;
            // Dokumento įraše mygtukas "Išsaugoti"
            case 'table_record_edit_button_save':
            case 'table_record_edit_save':
                if(empty($record_id)) {
                    throw new \Exception('Missing record ID');
                }

                $record = $this->core->logic2->get_record_relation($record_id);
                if ((int) $record['STATUS'] === \Atp\Record::STATUS_INVESTIGATING) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_INVESTIGATOR];
                } else
                // jei nagrinėjimo dokumentas, ieško nagrinėtojo
                if ((int) $record['STATUS'] === \Atp\Record::STATUS_EXAMINING) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_EXAMINATOR];
                } else
                // TODO: Kai statusas "nutrauktas" ?
                if ((int) $record['STATUS'] === \Atp\Record::STATUS_DISCONTINUED) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_INVESTIGATOR, \Atp\Record\Worker::TYPE_EXAMINATOR];
                }

                if (empty($workerTypes) === FALSE) {
                    $workers = $this->core->logic2->get_record_worker([
                        'A.RECORD_ID' => $record['ID'],
                        'A.TYPE IN("' . join('","', $workerTypes) . '")',
                        'A.WORKER_ID' => $this->core->factory->User()->getLoggedPersonDuty()->id
                    ]);
                } else {
                    $workers = null;
                }

                $valid = false;
                if (empty($workers) === false) {
                    $valid = true;
                }

                $r = array('OR' => array('EDIT_RECORD', 'INVESTIGATE_RECORD', 'EXAMINATE_RECORD'));
                // TODO: Kai statusas "nutrauktas" ?
                $extra = in_array($record['STATUS'], array(null, \Atp\Record::STATUS_DISCONTINUED,
                        \Atp\Record::STATUS_INVESTIGATING, \Atp\Record::STATUS_EXAMINING)) === TRUE && $valid;
                break;
            // Dokumento įraše mygtukas naujos versijos išsaugojimui
            case 'table_record_edit_button_save_new':
            case 'table_record_edit_save_new':
                if(empty($record_id)) {
                    throw new \Exception('Missing record ID');
                }
                $record = $this->core->logic2->get_record_relation($record_id);

                if ((int) $record['STATUS'] === \Atp\Record::STATUS_INVESTIGATING) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_INVESTIGATOR];
                } else
                // jei nagrinėjimo dokumentas, ieško nagrinėtojo
                if ((int) $record['STATUS'] === \Atp\Record::STATUS_EXAMINING) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_EXAMINATOR];
                } else
                // TODO: Kai statusas "nutrauktas" ?
                if ((int) $record['STATUS'] === \Atp\Record::STATUS_DISCONTINUED) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_INVESTIGATOR, \Atp\Record\Worker::TYPE_EXAMINATOR];
                }

                if (empty($workerTypes) === FALSE) {
                    $workers = $this->core->logic2->get_record_worker([
                        'A.RECORD_ID' => $record['ID'],
                        'A.TYPE IN("' . join('","', $workerTypes) . '")',
                        'A.WORKER_ID' => $this->core->factory->User()->getLoggedPersonDuty()->id
                    ]);
                } else {
                    $workers = null;
                }

                $valid = false;
                if (empty($workers) === false) {
                    $valid = true;
                }

                $r = array('OR' => array('EDIT_RECORD', 'INVESTIGATE_RECORD', 'EXAMINATE_RECORD'));
                // TODO: Kai statusas "nutrauktas" ?
                $extra = in_array($record['STATUS'], array(null, \Atp\Record::STATUS_DISCONTINUED,
                        \Atp\Record::STATUS_INVESTIGATING, \Atp\Record::STATUS_EXAMINING)) === TRUE && $valid;
                break;
            // Dokumento įraše mygtukas "Baigti tyrimą"
            case 'table_record_edit_button_finish_investigation':
            case 'table_record_edit_finish_investigation':
                if(empty($record_id)) {
                    throw new \Exception('Missing record ID');
                }
                $record = $this->core->logic2->get_record_relation($record_id);

                // TODO: Kai statusas "nutrauktas" ?
                // Tyrimą gali baigti tik tyrėjas ir tik tyrimo stadijoje
                if ((int) $record['STATUS'] === \Atp\Record::STATUS_INVESTIGATING || (int) $record['STATUS'] === \Atp\Record::STATUS_DISCONTINUED) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_INVESTIGATOR];
                }

                if (empty($workerTypes) === FALSE) {
                    $workers = $this->core->logic2->get_record_worker([
                        'A.RECORD_ID' => $record['ID'],
                        'A.TYPE IN("' . join('","', $workerTypes) . '")',
                        'A.WORKER_ID' => $this->core->factory->User()->getLoggedPersonDuty()->id
                    ]);
                } else {
                    $workers = null;
                }

                $valid = false;
                if (empty($workers) === false) {
                    $valid = true;
                }

                $r = 'INVESTIGATE_RECORD';
                // TODO: Kai statusas "nutrauktas" ?
                $extra = in_array($record['STATUS'], array(\Atp\Record::STATUS_DISCONTINUED,
                        \Atp\Record::STATUS_INVESTIGATING)) === TRUE && $valid;

                break;
            // Dokumento įraše mygtukas "Baigti tarpinę stadiją"
            case 'table_record_edit_button_finish_idle':
            case 'table_record_edit_finish_idle':
                if(empty($record_id)) {
                    throw new \Exception('Missing record ID');
                }
                $record = $this->core->logic2->get_record_relation($record_id);

                /*
                 *  TODO: yra vienas skyrius, kuriame tyrėjai tikrina vmi ir baigia tarpinę stadiją,
                 * tačiau yra kitas skyrius, kuriame tarpinę stadiją turi baigti nagrinėtojas.
                 * Jiems turi būti nustaatyta, kokius mygtukus rodyti
                 */
                if ((int) $record['STATUS'] === \Atp\Record::STATUS_INVESTIGATING) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_INVESTIGATOR];
                } else
                // jei nagrinėjimo dokumentas, ieško nagrinėtojo
                if ((int) $record['STATUS'] === \Atp\Record::STATUS_EXAMINING) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_EXAMINATOR];
                } else {
                    $workerTypes = [\Atp\Record\Worker::TYPE_INVESTIGATOR];
                }
                $workers = $this->core->logic2->get_record_worker([
                    'A.RECORD_ID' => $record['ID'],
                    'A.TYPE IN("' . join('","', $workerTypes) . '")',
                    'A.WORKER_ID' => $this->core->factory->User()->getLoggedPersonDuty()->id
                ]);

                $valid = false;
                if (empty($workers) === false) {
                    $valid = true;
                }

                $r = 'INVESTIGATE_RECORD';
                $extra = in_array($record['STATUS'], array(\Atp\Record::STATUS_IDLE)) === TRUE && $valid;
                break;
            // Dokumento įraše mygtukas "Baigti nagrinėjimą"
            case 'table_record_edit_button_finish_examination':
            case 'table_record_edit_finish_examination':
                if(empty($record_id)) {
                    throw new \Exception('Missing record ID');
                }
                $record = $this->core->logic2->get_record_relation($record_id);

                if ((int) $record['STATUS'] === \Atp\Record::STATUS_INVESTIGATING) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_INVESTIGATOR];
                } else
                // jei nagrinėjimo dokumentas, ieško nagrinėtojo
                if ((int) $record['STATUS'] === \Atp\Record::STATUS_EXAMINING) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_EXAMINATOR];
                } else
                // TODO: Kai statusas "nutrauktas" ?
                if ((int) $record['STATUS'] === \Atp\Record::STATUS_DISCONTINUED) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_EXAMINATOR];
                }

                if (empty($workerTypes) === FALSE) {
                    $workers = $this->core->logic2->get_record_worker([
                        'A.RECORD_ID' => $record['ID'],
                        'A.TYPE IN("' . join('","', $workerTypes) . '")',
                        'A.WORKER_ID' => $this->core->factory->User()->getLoggedPersonDuty()->id
                    ]);
                } else {
                    $workers = null;
                }

                if (empty($workers) === false) {
                    $valid = true;
                }

                $r = 'EXAMINATE_RECORD';
                // TODO: Kai statusas "nutrauktas" ?
                $extra = in_array($record['STATUS'], array(\Atp\Record::STATUS_DISCONTINUED,
                        \Atp\Record::STATUS_EXAMINING)) === TRUE && $valid;
                break;
            // Dokumento įraše mygtukas "Baigti procesą"
            case 'table_record_edit_button_finish_proccess':
            case 'table_record_edit_finish_proccess':
                if(empty($record_id)) {
                    throw new \Exception('Missing record ID');
                }
                $record = $this->core->logic2->get_record_relation($record_id);

                if ((int) $record['STATUS'] === \Atp\Record::STATUS_INVESTIGATING) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_INVESTIGATOR];
                } else
                // jei nagrinėjimo dokumentas, ieško nagrinėtojo
                if ((int) $record['STATUS'] === \Atp\Record::STATUS_EXAMINING) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_EXAMINATOR];
                } else
                // TODO: Kai statusas "nutrauktas" ?
                if ((int) $record['STATUS'] === \Atp\Record::STATUS_DISCONTINUED) {
                    $workerTypes = [\Atp\Record\Worker::TYPE_INVESTIGATOR, \Atp\Record\Worker::TYPE_EXAMINATOR];
                }

                if (empty($workerTypes) === FALSE) {
                    $workers = $this->core->logic2->get_record_worker([
                        'A.RECORD_ID' => $record['ID'],
                        'A.TYPE IN("' . join('","', $workerTypes) . '")',
                        'A.WORKER_ID' => $this->core->factory->User()->getLoggedPersonDuty()->id
                    ]);
                } else {
                    $workers = null;
                }

                $valid = false;
                if (empty($workers) === false) {
                    $valid = true;
                }

                $r = 'END_RECORD';
                $extra = in_array($record['STATUS'], array(\Atp\Record::STATUS_EXAMINED)) === TRUE && $table['CAN_END'] === '1' && $valid;
                break;
            // Dokumento įraše mygtukas "Sukurti"
            case 'table_record_edit_button_submit_path':
            case 'table_record_edit_submit_path':
                $r = array('OR' => array('EDIT_RECORD', 'INVESTIGATE_RECORD', 'EXAMINATE_RECORD'));
                break;
        }

        if ($extra === FALSE) {
            return $extra;
        }

        return $this->check_privileges($r, $data) && $extra;
    }

    /**
     * @deprecated nebenaudoajams
     * @return array
     */
    public function get_web_service_type()
    {
        trigger_error('get_web_service_type() is deprecated', E_USER_ERROR);
        $web_services = array(
            1 => array('ID' => 1, 'LABEL' => 'Web serviso Filtras'),
            2 => array('ID' => 2, 'LABEL' => 'Kito web serviso filtras'));

        return $web_services;
    }

    /**
     * Gauna laisvą tirėją
     * @author Justinas Malūkas
     * @param int $table_id Dokumento ID
     * @return function
     */
    public function get_free_investigator($table_id, $clause = null)
    {
        return $this->get_free_person($table_id, 1, $clause);
    }

    /**
     * Gauna laisvą nagrinėtoją
     * @author Justinas Malūkas
     * @param int $table_id Dokumento ID
     * @param int $clause [optional] Straipsnio ID
     * @return function
     */
    public function get_free_examinator($table_id, $clause = null)
    {
        return $this->get_free_person($table_id, 2, $clause);
    }

    /**
     * Gauna laisviausią tirėją / nagrinėtoją
     * @author Justinas Malūkas
     * @param int $table_id Dokumento ID
     * @param int $type 1 (tyrėjas) arba 2 (nagrinėtojas)
     * @param int $clause [optional] Straipsnio ID
     * @return array
     */
    public function get_free_person($table_id, $type, $clause = null)
    {
        if (empty($table_id) === TRUE || in_array($type, array(1, 2)) === FALSE)
            return false;

        $extra = null;
        $arg = array('DOCUMENT_ID' => $table_id);
        if (empty($clause) === FALSE && ctype_digit($clause) === TRUE) {
            $extra = 'INNER JOIN ' . PREFIX . 'ATP_USER U
				ON
					U.WORKER_ID = W.ID AND
					U.CLAUSE_ID = :CLAUSE_ID';
            $arg['CLAUSE_ID'] = (int) $clause;
        }
        if ($type === 1) {
            $type = 'INVESTIGATE_RECORD';
        } elseif ($type === 2) {
            $type = 'EXAMINATE_RECORD';
        }
        $arg['#TYPE_COLUMN#'] = $type;


        // TODO: šalinti M_PEOPLE_ID, M_DEPARTMENT_ID ir naudoti WORKER_ID
        $result = $this->core->db_query_fast('SELECT W.ID as WORKER_ID, MP.*, MS.ID AS DEPARTMENT_ID, MS.SHOWS as DEPARTMENT_TITLE,
			(
				SELECT COUNT(A.ID)
				FROM
					' . PREFIX . 'ATP_RECORD A
						INNER JOIN `' . PREFIX . 'ATP_RECORD_X_WORKER` B ON B.RECORD_ID = A.ID
						INNER JOIN `' . PREFIX . 'MAIN_WORKERS` C ON C.ID = B.WORKER_ID
				WHERE
					A.TABLE_TO_ID = P.DOCUMENT_ID AND
					A.IS_LAST = 1 AND
					A.STATUS < '.\Atp\Record::STATUS_EXAMINED.' AND
					C.ID = W.ID
			) AS TEMP_COUNT
			FROM
				' . PREFIX . 'ATP_USER_PRIVILEGES P
					INNER JOIN ' . PREFIX . 'MAIN_PEOPLE MP ON MP.ID = P.M_PEOPLE_ID
					INNER JOIN ' . PREFIX . 'MAIN_STRUCTURE MS ON MS.ID = P.M_DEPARTMENT_ID
					INNER JOIN ' . PREFIX . 'MAIN_WORKERS W ON W.PEOPLE_ID = MP.ID AND W.STRUCTURE_ID = MS.ID
					' . $extra . '
			WHERE
				P.:TYPE_COLUMN = 1 AND
				P.DOCUMENT_ID = :DOCUMENT_ID
			ORDER BY TEMP_COUNT ASC
			LIMIT 1', $arg);
        $row = $this->core->db_next($result);
        if (empty($row) === TRUE)
            return false;

        return $row;
    }

    // TODO: nenaudojamas
    /**
     * Gauna vartotojų sąrašą, kurie turi visas nurodytas teises
     * @author Justinas Malūkas
     * @param array $arr Tesių masyvas
     * @param int $table_id Dokumento ID
     * @param sting $fields Selekto laukai
     * @return array
     */
    public function get_users_by_privileges($arr, $table_id = null, $fields = 'A.*, P.M_DEPARTMENT_ID AS DEPARTMENT_ID, C.SHOWS as DEPARTMENT_TITLE')
    {
        if (empty($arr) === TRUE)
            return false;

        $where = array();
        for ($i = 0; $i < count($arr); $i++) {
            $where[] = 'P.' . strtoupper($arr[$i]) . ' = 1';
        }

        if (empty($table_id) === FALSE)
            $where[] = 'P.DOCUMENT_ID = ' . (int) $table_id;

        $result = $this->core->db_query('SELECT ' . $fields . '
			FROM
				' . PREFIX . 'ATP_USER_PRIVILEGES P
					INNER JOIN ' . PREFIX . 'MAIN_PEOPLE A ON A.ID = P.M_PEOPLE_ID
					INNER JOIN ' . PREFIX . 'MAIN_STRUCTURE C ON C.ID = P.M_DEPARTMENT_ID
			WHERE ' . join(' AND ', $where));

        $return = array();
        while ($row = $this->core->db_next($result))
            $return[] = $row;

        return $return;
    }

    public function get_user_department($m_people_id)
    {
        $departments = array();
        $qry = $this->core->db_query('
			SELECT ms.*
				FROM ' . PREFIX . 'MAIN_WORKERS mw
					INNER JOIN ' . PREFIX . 'MAIN_STRUCTURE ms ON ms.ID = mw.STRUCTURE_ID
				WHERE mw.PEOPLE_ID = ' . (int) $m_people_id);
        while ($res = $this->core->db_next($qry))
            $departments[$res['ID']] = $res;

        return $departments;
    }

    // Skyriaus teisės į dokumentus // Nenaudojama. Pridedant teises prie skyriaus, teisės sudedamos kiekvienam varotojui
    public function get_default_privileges($m_department_id = 0)
    {
        // TODO: change to WORKER ID
        $where .= " M_PEOPLE_ID=0 AND M_DEPARTMENT_ID={$m_department_id}";
        $qry = $this->core->db_query("SELECT * FROM " . PREFIX . "ATP_USER_PRIVILEGES WHERE {$where}");
        while ($res = $this->core->db_next($qry))
            $user_privileges[$res['TABLE_ID']] = $res;

        return $user_privileges;
    }

    // TODO: buvo naudojama ryšiams. Kolkas nebereikia
    public function get_clause_relation($params = array())
    {
        $where = '';
        if (!empty($params['CLAUSE_ID']))
            $where = " CLAUSE_ID={$params['CLAUSE_ID']}";

        $clause_relation = array();
        $sql = "SELECT * FROM " . PREFIX . "ATP_CLAUSE_RELATION WHERE {$where}";
        $qry = $this->core->db_query($sql);
        while ($res = $this->core->db_next($qry))
            $clause_relation[$res['ID']] = $res;

        return $clause_relation;
    }

    /**
     *
     * @param int $table_id
     * @return array
     */
    public function get_table_parent($table_id)
    {
        $sql = "SELECT p.*
			FROM
				" . PREFIX . "ATP_DOCUMENTS c,
				" . PREFIX . "ATP_DOCUMENTS p
			WHERE
				c.ID=" . (int) $table_id . " AND
				p.ID=c.PARENT_ID LIMIT 1";
        $qry = $this->core->db_query($sql);

        return $this->core->db_next($qry);
    }

    // TODO: funkcionalumas neatitinka pavadinimo :D tiesiog get_field()
    /**
     * Gauna lauko informaciją pagal jo ID
     * @author Justinas Malūkas
     * @param int $id Lauko ID
     * @return boolean|array
     */
    public function get_extra_field($id)
    {
        if (empty($id) === TRUE) {
            trigger_error('Field ID is empty', E_USER_WARNING);
            return false;
        }

        return $this->get_extra_fields($id, null, 'by_id');
    }

    // TODO: doumento įrašo kūrimo formoje laukų indeksus keisti iš strukturos 'NAME' į 'ID'. Bus mažiau nervų
    public function get_extra_field_id_by_name($name)
    {
        $qry = 'SELECT A.*
			FROM ' . PREFIX . 'ATP_FIELD A
			WHERE NAME = :name';
        $result = $this->core->db_query_fast($qry, array('name' => $name));
        $row = $this->core->db_next($result);
        return $row['ID'];
    }

    // TODO: Ten, kur naudojamas 3 parametrų variantas, pakeisti į 2 parametrų variantą
    /**
     * Gauna dokumento įrašo lauko reikšmę
     * @deprecated use \Atp\Record::getFieldValue()
     * @param $ Jei 3 parametrai. <b>DEPRECATED</b><br><b>TODO:</b> Ten kur naudojama pakeisti į 2 parametrų variantą
     * @param int $table_id Dokumento ID
     * @param int|string $record Dokumento įrašo ID iš dokumento lentelės arba dokumento įrašo numeris
     * @param int $field_id Lauko ID
     * @param $ Jei 2 parametrai
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param int $field_id Lauko ID
     * @return string
     */
    public function get_extra_field_value()
    {
        throw new \Exception('Implementation removed');
        $count = func_num_args();
        if ($count === 3) {

            list($table_id, $record, $field_id) = func_get_args();
            if (strpos($record, 'p') !== FALSE) {
                $expl = explode('p', $record);
                $params = array(
                    'RECORD_ID' => $record,
                    'RECORD_LAST' => 1,
                );
                $row = $this->core->logic2->get_db(PREFIX . 'ATP_TBL_' . $expl[0], $params, true, null, false, $fields);
                $record_id = $row['ID'];
            } else {
                $record_id = $record;
            }
        } elseif ($count === 2) {
            $record = func_get_arg(0);
            $field_id = func_get_arg(1);

            $record = $this->core->logic2->get_record_relation($record);

            // todo: use record manager
//            $recordManager = new \Atp\Record($this->core);
//            $record = $recordManager->get($record);
//            $record_table = $this->core->logic2->get_record_table($record);
//            if (empty($record_table) === TRUE) {
//                trigger_error('Record not found', E_USER_ERROR);
//                exit;
//            }
//            $record_id = $record_table['ID'];
//            $table_id = $record['TABLE_TO_ID'];
            $record_id = $record['ID'];
        } else {
            trigger_error('Wrong arguments count', E_USER_ERROR);
        }

        $qry = 'SELECT VALUE
			FROM ' . PREFIX . 'ATP_FIELDS_VALUES
			WHERE
			#	TABLE_ID = ' . (int) $table_id . ' AND
			#	RECORD_ID = ' . (int) $record_id . ' AND
				RECORD_ID = ' . (int) $record_id . ' AND
				FIELD_ID = ' . (int) $field_id;
        $result = $this->core->db_query_fast($qry);
        $row = $this->core->db_next($result);
        return $row['VALUE'];
    }

    // TODO: kur naudoajama, negalima pakeist į ką nors kitą? galbūt $this->get_extra_fields() ?
    public function get_extra_fields_by_table($item_id, $type, $extra_type = null, $included = false)
    {
        $type = 'DOC';
        $extra_type = 'by_doc';

        if (empty($item_id) === TRUE) {
            trigger_error('Item ID is empty', E_USER_WARNING);
            return false;
        }

        // Pradiniai užklausos duomenys
        $sql = array(
            'tables' => array(PREFIX . 'ATP_FIELD A'),
            'where' => array('1 = 1'),
            'order' => null,
            'limit' => null);

        // Papildomi užklausos duomenys pagal papildomą tipą
        switch ($extra_type) {
            //Gauna vaikų laukų informaciją pagal tėvo įrašo ID ir tipą
            case 'parent':
                $sql['order'] = array('A.ITEM_ID ASC', 'A.ORD ASC');
                // TODO: truksta DEED ir ACT (veikų ir teisės aktų)
                // TODO: dokumento vaikų laukų tikrai nereikia. Tai galimi tipai tik 'CLASS' ir 'CLAUSE'
                switch ($type) {
                    case 'DOC':
                        trigger_error($type . ' unavailable', E_USER_ERROR);
                        break;
                    case 'CLASS':
                        $sql['tables'][] = 'INNER JOIN ' . PREFIX . 'ATP_CLASSIFICATORS B ON B.ID = A.ITEM_ID AND ' .
                            ($included ? '(B.PARENT_ID = ' . (int) $item_id . ' OR B.ID = ' . (int) $item_id . ')' : 'B.PARENT_ID = ' . (int) $item_id);
                        break;
                    case 'CLAUSE':
                        // TODO: kaip ir beveik tas pats kaip 'CLASS'. Priėjus prie šios dalies - patikrint
                        trigger_error($type . ' still not supported', E_USER_ERROR);
                        $sql['tables'][] = 'INNER JOIN ' . PREFIX . 'ATP_CLAUSES B ON B.ID = A.ITEM_ID AND ' .
                            ($included ? '(B.PARENT_ID = ' . (int) $item_id . ' OR B.ID = ' . (int) $item_id . ')' : 'B.PARENT_ID = ' . (int) $item_id);

                        break;
                    default:
                        trigger_error('Wrong type', E_USER_WARNING);
                        return false;
                }
                break;
            // Gauna lauko informaciją pagal jo ID
            case 'by_id':
                $sql['where'][] = ' AND A.ID = ' . (int) $item_id;
                $sql['limit'] = '1';
                break;
            // Visi galimi papildomi laukai pagal Dokumento ID
            case 'by_doc':
                // Dokumente esantys klasifikatoriai
                // TODO: ir straipsniai
                $qry = 'SELECT SOURCE, SORT, TYPE
					FROM
						`' . PREFIX . 'ATP_FIELD` A
					WHERE
						ITEM_ID = ' . (int) $item_id . ' AND
						ITYPE = "DOC" AND
						( SORT = 4 OR SORT = 6 )';
                $result = $this->core->db_query_fast($qry);
                while ($row = $this->core->db_next($result)) {
                    /* if($row['TYPE'] === '23') {
                      $type = 'DEED';
                      //$fields[$type] = $this->get_all_extra_fields_by_type($type);
                      $fields[$type] = (array) $this->get_table_fields_w_extra2r($deed['ID'], array('ITYPE' => 'DEED'));

                      $act = array();
                      if(count($act_ids)) {
                      $act = $this->core->logic2->get_act(array(
                      0 => '`ID` IN(' . join(',', $act_ids) . ')',
                      1 => 'NOW() > `VALID_FROM` AND NOW() <= `VALID_TILL`'
                      ));
                      if(count($act)) {
                      $act = array_shift($act);
                      $fields[$type] = array_merge((array) $fields[$type], (array) $this->get_table_fields_w_extra2r($act['ID'], array('ITYPE' => 'ACT')));
                      }
                      }

                      $clauses = array();
                      if(count($clause_ids)) {
                      $clauses = $this->get_clause(array(
                      0 => '`ID` IN(' . join(',', $clause_ids) . ')'
                      ));
                      if(count($clauses)) {
                      foreach($clauses as $arr) {
                      $fields[$type] = array_merge((array) $fields[$type], (array) $this->get_table_fields_w_extra2r($arr['ID'], array('ITYPE' => 'CLAUSE')));
                      }
                      }
                      }
                      } else */
                    // Gauna klasifikatoriaus vaikų laukus
                    //if ($row['SORT'] === '4') {
                    if ($row['TYPE'] === '18') {
                        if (empty($row['SOURCE'])) {
                            continue;
                        }

                        $type = 'CLASS';
                        $fields[$type][$row['SOURCE']] = $this->get_extra_fields($row['SOURCE'], $type, 'parent');
                    } elseif ($row['TYPE'] === '19') {
                        $type = 'CLAUSE';
                        $fields[$type] = $this->get_all_extra_fields_by_type($type);
                    }
                }
                return $fields;
                break;
            // Gauna laukų informaciją pagal įrašo ID
            default:
                $sql['where'][] = ' AND A.ITEM_ID = ' . (int) $item_id;
                $sql['order'] = array('A.ORD ASC');
        }

        // Nurodomas tipas // Kai $extra_type == 'by_id', A.ITYPE gali būti nereikalingas
        if (in_array($type, array('DOC', 'CLASS', 'CLAUSE'), true) && $extra_type !== 'by_id')
        //if (in_array($type, array_keys($this->core->_prefix->item), true) && $extra_type !== 'by_id')
            $sql['where'][] = 'AND A.ITYPE = "' . $type . '"';

        // Surenka užklausos duomenis į tekstus
        $sql['tables'] = join(' ', $sql['tables']);
        $sql['where'] = count($sql['where']) ? ' WHERE ' . join(PHP_EOL, $sql['where']) : '';
        $sql['order'] = count($sql['order']) ? ' ORDER BY ' . join(', ', $sql['order']) : '';
        $sql['limit'] = empty($sql['limit']) ? '' : ' LIMIT ' . $sql['limit'];

        // Vykdoma užklausa
        $qry = 'SELECT A.* FROM ' . $sql['tables'] . $sql['where'] . $sql['order'] . $sql['limit'];
        $result = $this->core->db_query($qry);

        $return = array();
        while ($row = $this->core->db_next($result)) {
            switch ($extra_type) {
                case 'parent':
                    // Rezultatas tik dviejų lygmenų
                    $return[$row['ITEM_ID']][$row['ID']] = $row;
                    break;
                case 'by_id':
                    return $row;
                    break;
                default:
                    $return[$row['ID']] = $row;
            }
        }

        return $return;
    }

    public function get_all_extra_fields_by_type($type, $order = null)
    {
        if (in_array($type, array_keys($this->core->_prefix->item), true) === FALSE)
            return false;

        if ($type == 'CLASS') {
            $qry = 'SELECT A.*
				FROM ' . PREFIX . 'ATP_FIELD A
					INNER JOIN ' . PREFIX . 'ATP_CLASSIFICATORS B ON B.ID = A.ITEM_ID
				WHERE ITYPE = :itype';
        } elseif ($type == 'CLAUSE') {
            $qry = 'SELECT A.*
				FROM ' . PREFIX . 'ATP_FIELD A
					INNER JOIN ' . PREFIX . 'ATP_CLAUSES B ON B.ID = A.ITEM_ID
				WHERE ITYPE = :itype';
        } elseif ($type === 'DEED') {
            $qry = 'SELECT A.*
				FROM ' . PREFIX . 'ATP_FIELD A
					INNER JOIN ' . PREFIX . 'ATP_DEEDS B ON B.ID = A.ITEM_ID
				WHERE ITYPE = :itype';
        } elseif ($type === 'ACT') {
            $qry = 'SELECT A.*
				FROM ' . PREFIX . 'ATP_FIELD A
					INNER JOIN ' . PREFIX . 'ATP_ACTS B ON B.ID = A.ITEM_ID AND B.`VALID` = 1
				WHERE ITYPE = :itype';
        } else {
            return false;
        }

        if (empty($order) === FALSE)
            $qry .= ' ORDER BY ' . $order;

        $result = $this->core->db_query_fast($qry, array('itype' => $type));

        $return = array();
        while ($row = $this->core->db_next($result)) {
            $return[$row['ITEM_ID']][$row['ID']] = $row;
        }
        return $return;
    }

    // TODO: dalinai funkcionalumas neatitinka pavadinimo :D tiesiog get_fields()
    /**
     * Gauna laukų informaciją
     * @author Justinas Malūkas
     * @param int $item_id Įrašo ID
     * @param string $type Įrašo tipas
     * @param string $extra_type 'parent' - $item_id yra tevinis ID, 'by_id' - $item_id yra lauko ID
     * @param boolen $included [not strict] Išrinkti ir tėvinio įrašo laukus
     * @return array
     * @throws \Exception
     */
    public function get_extra_fields($item_id, $type, $extra_type = null, $included = false)
    {
        if (empty($item_id) === TRUE) {
//            return false;
            throw new \Exception('Item ID is empty');
        }

        // Pradiniai užklausos duomenys
        $sql = array(
            'fields' => array('A.*'),
            'tables' => array(PREFIX . 'ATP_FIELD A'),
            'where' => array('1 = 1'),
            'order' => null,
            'limit' => null);

        // Papildomi užklausos duomenys pagal papildomą tipą
        switch ($extra_type) {
            //Gauna vaikų laukų informaciją pagal tėvo įrašo ID ir tipą
            case 'parent':
                $sql['order'] = array('A.ITEM_ID ASC', 'A.ORD ASC');

                // TODO: dokumento vaikų laukų tikrai nereikia. Tai galimi tipai tik 'CLASS' ir 'CLAUSE'
                switch ($type) {
                    case 'DOC':
                        trigger_error($type . ' unavailable', E_USER_ERROR);
                        break;
                    case 'CLASS':
                        $sql['tables'][] = 'INNER JOIN ' . PREFIX . 'ATP_CLASSIFICATORS B ON B.ID = A.ITEM_ID AND ' .
                            ($included ? '(B.PARENT_ID = ' . (int) $item_id . ' OR B.ID = ' . (int) $item_id . ')' : 'B.PARENT_ID = ' . (int) $item_id);
                        break;
                    case 'CLAUSE':
                        // TODO: kaip ir beveik tas pats kaip 'CLASS'. Priėjus prie šios dalies - patikrint
                        trigger_error($type . ' still not supported', E_USER_ERROR);
                        $sql['tables'][] = 'INNER JOIN ' . PREFIX . 'ATP_CLAUSES B ON B.ID = A.ITEM_ID AND ' .
                            ($included ? '(B.PARENT_ID = ' . (int) $item_id . ' OR B.ID = ' . (int) $item_id . ')' : 'B.PARENT_ID = ' . (int) $item_id);
                        break;
                    case 'DEED':
                        $sql['tables'][] = 'INNER JOIN ' . PREFIX . 'ATP_DEEDS B ON B.ID = A.ITEM_ID AND ' .
                            ($included ? '(B.PARENT_ID = ' . (int) $item_id . ' OR B.ID = ' . (int) $item_id . ')' : 'B.PARENT_ID = ' . (int) $item_id);
                        break;
                    case 'ACT':
                        $sql['tables'][] = 'INNER JOIN ' . PREFIX . 'ATP_ACTS B ON B.ID = A.ITEM_ID AND B.`VALID` = 1 AND ' .
                            ($included ? '(B.PARENT_ID = ' . (int) $item_id . ' OR B.ID = ' . (int) $item_id . ')' : 'B.PARENT_ID = ' . (int) $item_id);
                        break;
                    case 'WS':
                        /* $sql['tables'][] = 'INNER JOIN ' . PREFIX . 'ATP_ACTS B ON B.ID = A.ITEM_ID AND B.`VALID` = 1 AND ' .
                          ($included ? '(B.PARENT_ID = ' . (int) $item_id . ' OR B.ID = ' . (int) $item_id . ')' : 'B.PARENT_ID = ' . (int) $item_id); */
                        break;
                    default:
//                        trigger_error('Wrong type', E_USER_WARNING);
//                        return false;
                        throw new \Exception('Wrong type');
                }
                break;
            // Gauna lauko informaciją pagal jo ID
            case 'by_id':
                $sql['where'][] = ' AND A.ID = ' . (int) $item_id;
                $sql['limit'] = '1';
                break;
            // Gauna laukų informaciją pagal įrašo ID
            default:
                $sql['where'][] = ' AND A.ITEM_ID = ' . (int) $item_id;
                $sql['order'] = array('A.ORD ASC');
        }

        // Nurodomas tipas // Kai $extra_type == 'by_id', A.ITYPE gali būti nereikalingas
        if (in_array($type, array_keys($this->core->_prefix->item), true) && $extra_type !== 'by_id') {
            $sql['where'][] = 'AND A.ITYPE = "' . $type . '"';
        }



        if ($type === 'WS') {
            $sql['fields'][] = '`WS`.`WS_NAME`, `WS`.`WS_FIELD_ID`';
            $sql['tables'][] = 'LEFT JOIN ' . PREFIX . 'ATP_WEBSERVICES WS ON WS.FIELD_ID = A.ID';
        }

        // Surenka užklausos duomenis į tekstus
        $sql['fields'] = join(',', $sql['fields']);
        $sql['tables'] = join(' ', $sql['tables']);
        $sql['where'] = count($sql['where']) ? ' WHERE ' . join(PHP_EOL, $sql['where']) : '';
        $sql['order'] = count($sql['order']) ? ' ORDER BY ' . join(', ', $sql['order']) : '';
        $sql['limit'] = empty($sql['limit']) ? '' : ' LIMIT ' . $sql['limit'];

        /* if($type === 'ACT') {
          $fields = $this->core->logic2->get_act_default_fields_structure();
          $fields_tmp = array();
          foreach($fields as &$field) {
          $fields_tmp[$field['ID']] = $field;
          }
          return $fields_tmp;
          } */

        // Vykdoma užklausa
        $qry = 'SELECT ' . $sql['fields'] . ' FROM ' . $sql['tables'] . $sql['where'] . $sql['order'] . $sql['limit'];
        $result = $this->core->db_query($qry);

        $return = array();
        while ($row = $this->core->db_next($result)) {
            switch ($extra_type) {
                case 'parent':
                    // Rezultatas tik dviejų lygmenų
                    $return[$row['ITEM_ID']][$row['ID']] = $row;
                    break;
                case 'by_id':
                    return $row;
                    break;
                default:
                    $return[$row['ID']] = $row;
            }
        }

        return $return;
    }

    // TODO: This is BullS. Reikia alternatyvos arba keist kodą, kur naudojamas šis metodas
    public function get_table_fields_w_extra2r($item_id, $opt = null, &$fields = null)
    {
        //$fields = $this->get_table_fields($item_id, $opt);
        $fields = array_merge((array) $fields, (array) $this->get_table_fields($item_id, $opt));
        // Surenka visus papildomus laukus, kurie gali būti dokumente
        //if(empty($opt['ITYPE']) || $opt['ITYPE'] === 'DOC')
        $extra_fields = $this->get_extra_fields_by_table($item_id, 'DOC', 'by_doc');
        //	$extra_fields = $this->get_extra_fields($item_id, $opt['ITYPE'], 'by_id');
        // Atrenkami papildomi laukai pridedami prie strukturos
        foreach ($fields as $structure) {
            if ($structure['SORT'] === '4' && $structure['TYPE'] === '18') {
                foreach ((array) $extra_fields['CLASS'] as $a => $aa) {
                    foreach ((array) $aa as $b => $bb) {
                        foreach ((array) $bb as $c => $cc) {
                            $fields[$cc['ID']] = $cc;
                            //return $this->get_table_fields_w_extra2r($cc['ITEM_ID'], array('ITYPE' => $cc['ITYPE']), $fields);
                        }
                    }
                }
            } else
            if ($structure['SORT'] === '4' && $structure['TYPE'] === '23') {
                $items = array();
                $deeds = $this->get_deeds();

                $ids = array();
                foreach ($deeds as $key => $arr) {
                    $ids['DEED'][$arr['ID']] = $arr['ID'];
                    $items[] = array(
                        'ID' => $arr['ID'],
                        'TYPE' => 'DEED'
                    );
                }
                if (count($ids['DEED'])) {
                    $ddata = $this->get_deed_extend_data(array(0 => '`DEED_ID` IN(' . join(',', $ids['DEED']) . ')'));
                    foreach ($ddata as $arr) {
                        $items[] = array(
                            'ID' => $arr['ITEM_ID'],
                            'TYPE' => $arr['ITYPE']
                        );
                    }

                    foreach ($items as $arr) {
                        $extra = $this->get_table_fields_w_extra2r($arr['ID'], array(
                            'ITYPE' => $arr['TYPE']));
                        foreach ((array) $extra as $a) {
                            $fields[$a['ID']] = $a;
                        }
                    }
                }
            } else
            if ($structure['SORT'] === '6' && $structure['TYPE'] === '19') {
                foreach ($extra_fields['CLAUSE'] as $key => $arr) {
                    foreach ($arr as $a => $r) {
                        $fields[$r['ID']] = $r;
                        //$this->get_table_fields_w_extra2r($r['ITEM_ID'], array('ITYPE' => $r['ITYPE']), $fields);
                    }
                }
            }
        }
        return $fields;
    }

//    /**
//     * Pagal nurodytus parametrus gauna dokumento įrašo reikšmes
//     * @deprecated denaudojamas
//     * @example UNTESTED <b>$this->get_fields_values_by_type_and_id(array('RECORD_ID' => 257, 'ID' => 1, 'TYPE' => 'DOC'));</b> - gauna visų galimų dokumento laukų reikšmes
//     * @example UNTESTED <b>$this->get_fields_values_by_type_and_id(array('RECORD_ID' => 257, 'ID' => 1, 'TYPE' => 'CLASS'));</b> - gauna visų galimų klasifikatoriaus laukų reikšmes
//     * @example <b>$this->get_fields_values_by_type_and_id(array('RECORD_ID' => 257, 'ID' => 1, 'TYPE' => 'DEED'));</b> - gauna visų galimų veikos laukų reikšmes
//     * @example UNTESTED <b>$this->get_fields_values_by_type_and_id(array('RECORD_ID' => 257, 'ID' => 1, 'TYPE' => 'CLAUSE'));</b> - gauna visų galimų straipsnio laukų reikšmes
//     * @example UNTESTED <b>$this->get_fields_values_by_type_and_id(array('RECORD_ID' => 257, 'ID' => 1, 'TYPE' => 'ACT'));</b> - gauna visų galimų teisės akto laukų reikšmes
//     * @param array $param Parametrai
//     * @param int $param['ID'] Įrašo grupės ID (Dokumento / Klasifikatoriaus / Straipsnio / Teisės akto / Veikos ID)
//     * @param string $param['TYPE'] Įrašo tipas (DOC / CLASS / CLAUSE / ACT / DEED)
//     * @param string $param['RECORD_ID'] Dokumento įrašo ID<br>(Formatas: {Dokumento_ID}p{Įrašo_ID} Pvz.: "9p257")
//     * @param array $data
//     * @return array
//     */
//    public function get_fields_values_by_type_and_id($param = array(), &$data = array())
//    {
//        //return $this->get_fields_values_by_type_and_id2($param, $data);
//
//        $fields = $this->get_fields_by_type_and_id($param);
//        foreach ($fields as $arr) {
//            if (isset($data[$arr['NAME']]))
//                continue;
//
//            // Visų dokumento laukų reikšmės pagal įrašo gupes
//            switch ($arr['ITYPE']) {
//                case 'DOC':
//                    $table_name = $this->core->_prefix->table . (int) $param['ID'];
//                    //$qry = 'SELECT tn.*,rr.M_PEOPLE_ID,rr.M_DEPARTMENT_ID,rr.M_PEOPLE_ID_2,rr.M_DEPARTMENT_ID_2,rr.STATUS,rr.IS_PAID
//                    $qry = 'SELECT tn.*,rr.M_PEOPLE_ID,rr.M_DEPARTMENT_ID,rr.M_PEOPLE_ID_2,rr.M_DEPARTMENT_ID_2,rr.STATUS
//						FROM
//							' . PREFIX . 'ATP_' . $table_name . ' tn
//								INNER JOIN ' . PREFIX . 'ATP_RECORD rr
//									ON
//										rr.RECORD_ID = tn.RECORD_ID AND
//										rr.IS_LAST = 1
//										#AND
//										#(
//										#	(	rr.M_PEOPLE_ID = :user_id AND
//										#		rr.M_DEPARTMENT_ID = :department_id )
//										#	OR
//										#	(
//										#		rr.STATUS = '.\Atp\Record::STATUS_IDLE.' AND
//										#		rr.M_PEOPLE_ID_2 = :user_id AND
//										#		rr.M_DEPARTMENT_ID_2 = :department_id )
//										#)
//						WHERE
//							rr.TABLE_TO_ID = :table_id AND
//							tn.RECORD_LAST = 1 AND
//							tn.RECORD_ID = :record_id
//						LIMIT 1';
//                    $result = $this->core->db_query_fast($qry, array(
//                        'table_id' => $param['ID'],
//                        'record_id' => $param['ID'] . 'p' . end(explode('p', $param['RECORD_ID'])),
//                        ), true);
//                    while ($row = $this->core->db_next($result)) {
//                        $data = array_merge((array) $data, (array) $row);
//                        //$data[$row['NAME']] = $row;
//                    }
//                    break;
//                case 'CLASS':
//                case 'CLAUSE':
//                case 'DEED':
//                case 'ACT':
//                    $expl = explode('p', $param['RECORD_ID']);
//
//                    // Gauna įrašo ID iš pačio dokumento
//                    $result = $this->core->db_query_fast('SELECT A.ID, B.RELATION_ID RID
//						FROM
//							' . PREFIX . 'ATP_TBL_' . (int) $expl[0] . ' A
//                                INNER JOIN ' . PREFIX . 'ATP_RECORD_X_RELATION B ON
//                                    B.RECORD_ID = A.ID AND
//                                    B.DOCUMENT_ID = ' . (int) $expl[0] . '
//						WHERE
//							A.RECORD_ID = :record_id AND
//							A.RECORD_LAST = 1', array('record_id' => $param['RECORD_ID']));
//                    $row = $this->core->db_next($result);
////                    $expl[1] = $row['ID'];
//                    //$data[$arr['NAME']] = $this->get_extra_field_value($expl[0], $expl[1], $arr['ID']);
////                    $data[$arr['NAME']] = $this->get_extra_field_value($expl[0], $param['RECORD_ID'], $arr['ID']);
//                    $row[$r['NAME']] = $this->core->factory->Record()->getFieldValue($row['RID'], $arr['ID']);
//                    break;
//            }
//        }
//        return $data;
//    }
//
//    /**
//     * Pagal nurodytus parametrus gauna dokumento įrašo reikšmes
//     * @todo DCOL (DEED) nenaudojamas?
//     * @deprecated Nenaudoti
//     * @param array $param Parametrai
//     * @param mixed $param['RECORD'] Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
//     * @param int $param['ID'] [optional]<p>Įrašo grupės ID (Dokumento / Klasifikatoriaus / Straipsnio / Teisės akto / Veikos ID) <b>Default:</b> <i>$param['RECORD']</i> dokumentas</p>
//     * @param string $param['TYPE'] [optional]<p>Įrašo tipas (DOC / CLASS / CLAUSE / ACT / DEED) <b>Default:</b> 'DOC'</p>
//     * @return array
//     */
//    public function get_fields_values_by_type_and_id2($param)
//    {
//        trigger_error(__METHOD__ . ' is deprecated', E_USER_DEPRECATED);
//
//        $record = $this->core->logic2->get_record_relation($param['RECORD']);
//        if (empty($record) === TRUE) {
//            trigger_error('Record not found', E_USER_ERROR);
//            exit;
//        }
//
//        $default = array(
//            'ID' => $record['TABLE_TO_ID'],
//            'TYPE' => 'DOC'
//        );
//        $param = array_merge($default, $param);
//
//        $arg = array(
//            'ID' => $param['ID'],
//            'TYPE' => $param['TYPE']
//        );
//        $fields = $this->get_fields_by_type_and_id($arg);
//
//        $result = array();
//        foreach ($fields as $arr) {
//            // Jei formoje dubliuojasi laukai, saugomas pirmasis laukas. Nėra jokio reikalo ieškoti besikartojančių laukų reikšmių, vistiek išsaugotu tik paskutinį
//            if (isset($result[$arr['NAME']]))
//                continue;
//
//            // Visų dokumento laukų reikšmės pagal įrašo gupes
//            switch ($arr['ITYPE']) {
//                case 'DOC':
//                    /* $table_name = $this->core->_prefix->table . (int) $record['TABLE_TO_ID'];
//
//                      $joins = array(
//                      'B' => 'INNER JOIN `' . PREFIX . 'ATP_RECORD_X_RELATION` B ON B.RELATION_ID = A.ID',
//                      'C' => 'INNER JOIN `' . PREFIX . 'ATP_' . $table_name . '` C ON C.ID = B.RECORD_ID'
//                      );
//                      $params = array(
//                      0 => 'A.ID = "' . $record['ID'] . '"'
//                      );
//                      $row = $this->core->logic2->get_db(PREFIX . 'ATP_RECORD', $params, $limit = true, null, false, 'C.*', $joins);
//                     */
//                    $row = $this->core->logic2->get_record_table($record, $fields = 'C.*');
//                    // Reikalingi tik formos laukų stulpeliai
//                    foreach ($row as $column => &$value) {
//                        if (strpos($column, $this->core->_prefix->column) !== 0)
//                            unset($row[$column]);
//                    }
//
//                    $result = array_merge((array) $result, (array) $row);
//                    break;
//                case 'CLASS':
//                case 'CLAUSE':
//                case 'DEED':
//                case 'ACT':
//                    //$result[$arr['NAME']] = $this->get_extra_field_value($record['TABLE_TO_ID'], $record['RECORD_ID'], $arr['ID']);
////                    $result[$arr['NAME']] = $this->get_extra_field_value($record, $arr['ID']);
//                    $result[$arr['NAME']] = $this->core->factory->Record()->getFieldValue($record['ID'], $arr['ID']);
//                    break;
//            }
//        }
//        return $result;
//    }

    // TODO: Klasifikatoriui ir veikai gražina ir pagrindinio (tėvinio) įrašo laukus. Šiaip to nereikia.
    /**
     * Pagal nurodytus parametrus gauna visų galimų laukų informaciją
     * @todo DCOL (DEED) nenaudojamas?
     * @author Justinas Malūkas
     * @example <b>$this->get_fields_by_type_and_id(array('ID' => 1, 'TYPE' => 'DOC'));</b> - gauna visus galimus dokumento laukus
     * @example <b>$this->get_fields_by_type_and_id(array('ID' => 1, 'TYPE' => 'CLASS'));</b> - gauna visus galimus klasifikatoriaus laukus
     * @example <b>$this->get_fields_by_type_and_id(array('ID' => 1, 'TYPE' => 'DEED'));</b> - gauna visus galimus veikos laukus
     * @example <b>$this->get_fields_by_type_and_id(array('ID' => 1, 'TYPE' => 'CLAUSE'));</b> - gauna visus galimus straipsnio laukus
     * @example <b>$this->get_fields_by_type_and_id(array('ID' => 1, 'TYPE' => 'ACT'));</b> - gauna visus galimus teisės akto laukus
     * @param array $param Parametrai
     * @param int $param['ID'] Įrašo grupės ID (Dokumento / Klasifikatoriaus / Straipsnio / Teisės akto / Veikos ID)
     * @param string $param['TYPE'] Įrašo tipas (DOC / CLASS / CLAUSE / ACT / DEED)
     * @param array $fields
     * @return array
     */
    public function get_fields_by_type_and_id($param = array(), &$fields = array())
    {
        //$par['ITYPE'] = $param['TYPE'];
        //$par['ITEM_ID'] = $par['ID'];
        if ($param['TYPE'] === 'DOC') { // Dokumentas // DOC
            // Gauna laukus
            $qry = 'SELECT A.*
				FROM
					' . PREFIX . 'ATP_FIELD A
				WHERE
					A.`ITYPE` = "DOC" AND
					A.`ITEM_ID` = ' . (int) $param['ID'] . '
				ORDER BY A.`ORD` ASC';
        } elseif ($param['TYPE'] === 'CLASS') { // Klasifikatorius // CLASS
            // Surenka vaikus ir jų laukus
            //$children = $this->get_classification(null, $param['ID']);
            $children = $this->core->logic2->get_classificator(array('PARENT_ID' => $param['ID']));
            if (empty($children) === FALSE)
                foreach ($children as $arr) {
                    $this->get_fields_by_type_and_id(array('ID' => $arr['ID'], 'TYPE' => 'CLASS'), $fields);
                }

            // Gauna laukus
            $qry = 'SELECT A.*
				FROM
					' . PREFIX . 'ATP_FIELD A
				WHERE
					A.`ITYPE` = "CLASS" AND
					A.`ITEM_ID` = ' . (int) $param['ID'] . '
				ORDER BY A.`ORD` ASC';
        } elseif ($param['TYPE'] === 'CLAUSE') { // Straipsnis // CLAUSE
            // Gauna laukus
            /* $qry = 'SELECT A.*
              FROM
              ' . PREFIX . 'ATP_FIELD A
              WHERE
              A.`ITYPE` = "CLAUSE"
              ' . (isset($param['ID']) ? ' AND A.`ITEM_ID` = ' . (int) $param['ID'] : '') . '
              ORDER BY A.`ORD` ASC'; */
            $qry = null;
            $rows = $this->core->logic2->get_fields(array('ITYPE' => $param['TYPE'],
                'ITEM_ID' => $param['ID']));
            foreach ($rows as $row) {
                $fields[$row['ID']] = $row;
            }
        } elseif ($param['TYPE'] === 'ACT') { // Teisės aktas // ACT
            // Gauna laukus
            /* 	$qry = 'SELECT A.*
              FROM
              ' . PREFIX . 'ATP_FIELD A
              WHERE
              A.`ITYPE` = "ACT"
              ' . (isset($param['ID']) ? ' AND A.`ITEM_ID` = ' . (int) $param['ID'] : '') . '
              ORDER BY A.`ORD` ASC';
             */
            $qry = null;
            $rows = $this->core->logic2->get_fields(array('ITYPE' => $param['TYPE'],
                'ITEM_ID' => $param['ID']));
            foreach ($rows as $row) {
                $fields[$row['ID']] = $row;
            }
        } elseif ($param['TYPE'] === 'DEED') { // Veika // DEED
            // Gauna veikas
            if (isset($param['ID'])) { // Jei tiesiogiai bandoma gauti veikos laukus
                $deeds[] = $this->get_deed(array('ID' => $param['ID']));
            } else { // Kitaip rastas veikos laukas ir gali būti pasirenkama bet kuri veika, tai susirenka jas visas
                $deeds = $this->get_deeds();
            }
            // Gauna įrašus, kurie praplečia veikos formą, ir jų laukus
            foreach ($deeds as $arr) {
                $deed_extends = $this->get_deed_extend_data(array('DEED_ID' => $arr['ID']));
                foreach ($deed_extends as $extends) {
                    // Jei išpečia teisės aktas, tai tikrina ar jis galioja
                    if ($extends['ITYPE'] === 'ACT' && empty($param['RECORD_DATE']) === FALSE) {
                        $act = $this->core->logic2->get_act(array('ID' => $extends['ITEM_ID']));
                        if ($param['RECORD_DATE'] < $act['VALID_FROM'] || $param['RECORD_DATE'] > $act['VALID_TILL'])
                            continue;
                    }
                    $this->get_fields_by_type_and_id(array('ID' => $extends['ITEM_ID'],
                        'TYPE' => $extends['ITYPE']), $fields);
                }
            }

            // TODO: Pati veika neturi savo laukų (ji tik apjungia straipsnius ir teisės aktų punktus)
            // Gauna laukus
            $qry = 'SELECT A.*
				FROM
					' . PREFIX . 'ATP_FIELD A
				WHERE
					A.`ITYPE` = "DEED"
					' . (isset($param['ID']) ? ' AND A.`ITEM_ID` = ' . (int) $param['ID'] : '') . '
				ORDER BY A.`ORD` ASC';
        }

        // Išsaugo gautus laukus, tikrina ar gauti laukai gali turėti papildomų laukų
        if (empty($qry) === FALSE) {
            $result = $this->core->db_query_fast($qry);
            //$fields_temp = array();
            while ($row = $this->core->db_next($result)) {
                $fields[$row['ID']] = $row;
                // TODO: Galimėtu tikrinti tik 'TYPE'
                // Iškrentantis sąrašas
                if ($row['SORT'] === '4') {
                    // Klasifikatorius // CLASS
                    if ($row['TYPE'] === '18') {
                        $this->get_fields_by_type_and_id(array('ID' => $row['SOURCE'],
                            'TYPE' => 'CLASS'), $fields);
                    } else
                    // Veika // DEED
                    if ($row['TYPE'] === '23') {
                        $this->get_fields_by_type_and_id(array('TYPE' => 'DEED'), $fields);
                    }
                } else
                // Straipsnis // CLAUSE
                if ($row['SORT'] === '6' && $row['TYPE'] === '19') {
                    $this->get_fields_by_type_and_id(array('TYPE' => 'CLAUSE'), $fields);
                } // Teisės akto (ACT) tipo lauko nėra
            }
        }
        return $fields;
    }

    // TODO: nebenaudoti get_table_fields() metodo?
    /**
     * Gauna dokumento laukų informaciją
     * @deprecated Nebenaudoti šio metodo?
     * @authors Martynas Boika, Justinas Malūkas
     * @param int $item_id Įrašo ID
     * @param array $opt Papildomi nurodymai
     * @return array|boolen
     */
    public function get_table_fields($item_id, $opt = null)
    {
        $where = '';

        // Tipas pagal nutyėjimą
        if (empty($opt['ITYPE']) === TRUE)
            $opt['ITYPE'] = 'DOC';

        if (in_array($opt['ITYPE'], array_keys($this->core->_prefix->item), true))
            $where .= ' AND A.ITYPE = "' . $opt['ITYPE'] . '"';
        else {
            trigger_error('Unknown item type', E_USER_WARNING);
            return false;
        }

        if (empty($opt['IN_LIST']) === false) {
            $where .= ' AND A.IN_LIST = 1';
        }

        // TODO: pašalinti alias, greičiau paaiškės kas liko nepakeista
        $qry = 'SELECT A.*
			FROM
				' . PREFIX . 'ATP_FIELD A
			WHERE
				A.ITEM_ID = ' . (int) $item_id
            . $where . '
			ORDER BY A.ORD ASC';
        $result = $this->core->db_query($qry);

        $return = array();
        while ($row = $this->core->db_next($result)) {

            // TODO: Nereikia ir tiems, kurie neturi tiesioginio ryšio su dokumentu?
            if ((int) $row['SORT'] === 10 && $row['ITYPE'] === 'DOC') {
                $file_params = $this->get_file_params(array('DOCUMENT_ID' => $row['ITEM_ID'],
                    'STRUCTURE_ID' => $row['ID'], 'LIMIT' => 1));
                if (!empty($file_params)) {
                    $file_params = array_shift($file_params);
                    $row['FILE_CNT'] = $file_params['CNT'];
                    $row['FILE_SIZE_FROM'] = $file_params['SIZE_FROM'];
                    $row['FILE_SIZE_TO'] = $file_params['SIZE_TO'];
                }
            }

            $return[$row['ID']] = $row;
        }

        return $return;
    }

//    /**
//     * @param type $table_id
//     * @deprecated nenaudojamas
//     * @return array
//     */
//    public function get_table_column_relation($table_id)
//    {
//        trigger_error('get_table_column_relation() is deprecated', E_USER_DEPRECATED);
//        $column_relations = array();
//        $sql = "SELECT * FROM " . PREFIX . "ATP_RELATION_COLUMN WHERE DOCUMENT_ID={$table_id}";
//        $qry = $this->core->db_query($sql);
//        while ($res = $this->core->db_next($qry))
//            $column_relations[$res['COLUMN_ID']] = $res;
//
//        return $column_relations;
//    }

    /**
     * Dokumento ryšiai arba ryšiai tarp dokumentų
     * @author Martynas Boika
     * @param int $document_id Dokumento ID iš kurio eina ryšys
     * @param int $parent_id [optional] Dokumento ID į kurį eina ryšys
     * @return array Ryšių duomenys
     */
    //public function get_table_relations($document_id, $parent_id = 0) {
    /**
     * Dokumento ryšiai arba ryšiai tarp dokumentų
     * @author Martynas Boika
     * @param int $from_document Dokumento ID iš kurio eina ryšys
     * @param int $to_document [optional] Dokumento ID į kurį eina ryšys
     * @return array Ryšių duomenys
     */
    public function get_table_relations($from_document, $to_document = 0)
    {
        $where = null;
        if (empty($from_document) === FALSE) {
            $where .= ' AND A.DOCUMENT_PARENT_ID = ' . (int) $from_document;
        }

        $arr = array();
        $qry = 'SELECT A.*
				FROM ' . PREFIX . 'ATP_TABLE_RELATIONS A
				WHERE
					A.DOCUMENT_ID = ' . (int) $to_document
            . $where;
        $result = $this->core->db_query_fast($qry);
        while ($row = $this->core->db_next($result))
            $arr[] = $row;

        return $arr;
    }

    public function get_file_params($params = array())
    {
        $where = '';
        if (!empty($params['DOCUMENT_ID']))
            $where = " DOCUMENT_ID={$params['DOCUMENT_ID']}";
        if (!empty($where))
            $where .= ' AND';
        if (!empty($params['STRUCTURE_ID']))
            $where = " STRUCTURE_ID={$params['STRUCTURE_ID']}";
        $limit = (!empty($params['LIMIT']) ? ' LIMIT ' . $params['LIMIT'] : '');

        $file_params = array();
        $sql = "SELECT * FROM " . PREFIX . "ATP_FILE_PARAMS WHERE {$where} {$limit}";
        $qry = $this->core->db_query($sql);
        while ($res = $this->core->db_next($qry))
            $file_params[$res['ID']] = $res;

        return $file_params;
    }

    /**
     * Nustato hierarchijos lygį
     * @author Justinas Malūkas
     * @param array $arr Hierarchijos masyvas
     * @param int $id Hierarchijos masyvo elemento ID
     * @param int $level
     * @return int Hierarchijos elemento lygis
     */
    function get_table_level($arr, $id, $level = 0)
    {
        if ($arr[$id]['PARENT_ID'] === $id) {
            trigger_error('Item is parent to itself', E_USER_WARNING);
            return null;
        } else
        if ($level > 500) {
            trigger_error('Loop overflow', E_USER_WARNING);
            return null;
        }

        return empty($arr[$id]['PARENT_ID']) ? $level : $this->get_table_level($arr, $arr[$id]['PARENT_ID'], ++$level);
    }
    private $cacheTableHasChildren = array();

    /**
     * Patikrina ar hierarchijos elementas turi vaiką
     * @deprecated When $dataType is 'CLASSIFICATORS' use nested set tree.
     * @author Justinas Malūkas
     * @param array $arr Hierarchijos masyvas
     * @param int $id Hierarchijos masyvo elemento ID
     * @param string $dataType [optional] One of following: 'CLASSIFICATORS', 'ACTS', 'DEEDS', 'DOCUMENTS', 'CLAUSES'
     * @return boolen
     * @throws \Exception
     */
    function table_has_children($arr, $id, $dataType = null)
    {
        if ($dataType === 'CLASSIFICATORS') {
            throw new \Exception('Deprecated for "CLASSIFICATORS". Use nested set tree.');
        }

        $validTypes = ['CLASSIFICATORS', 'ACTS', /* 'STATUS', */ 'DEEDS', 'DOCUMENTS',
            'CLAUSES'];
        if (empty($dataType) === FALSE && in_array($dataType, $validTypes)) {
            if (empty($this->cacheTableHasChildren[$dataType])) {
                $result = $this->core->db_query_fast('
                    SELECT ID, PARENT_ID
                    FROM
                        ' . PREFIX . 'ATP_' . $dataType
                );
                while ($row = $this->core->db_next($result)) {
                    if (empty($row['PARENT_ID']) === FALSE) {
                        $this->cacheTableHasChildren[$dataType][$row['PARENT_ID']][] = $row['ID'];
                    }
                }
            }

            if (isset($this->cacheTableHasChildren[$dataType][$id])) {
                return true;
            }

            return false;
        }

        foreach ($arr as $b) {
            if ($b['PARENT_ID'] == $id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Perrikuoja įrašus, kiekvienas įrašas pastatomas po jo tėviniu įrašu.
     * @author web
     * @bug Jei įrašui neranda tėvinio ID - jį pameta
     * @param array $elements Įrašai
     * @param string $id_field ID elemento pavadinimas
     * @param string $parent_id_field Tėvinio elemento pavadinimas
     * @param int $parent_id [optional] Tėvinis ID
     * @param array $result [optional] Perrikiuotas masyvas
     * @param int $depth [optional] Gylis
     * @return array Perrikuotas masyvas
     */
    function sort_children_to_parent($elements, $id_field, $parent_id_field, $parent_id = 0, &$result = array(), &$depth = 0)
    {
        foreach ((array) $elements as $key => $value) {
            if ($value[$parent_id_field] == $parent_id) {
                $value['depth'] = $depth;
                array_push($result, $value);
                unset($elements[$key]);
                $old_parent = $parent_id;
                $parent_id = $value[$id_field];
                $depth++;
                $res = $this->sort_children_to_parent($elements, $id_field, $parent_id_field, $parent_id, $result, $depth);
                $parent_id = $old_parent;
                $depth--;
            }
        }
        return $result;
    }

    /**
     * Gražina visų dokumento įrašų apskaičiuotą terminą.
     * @author Justinas Malūkas
     * @param int $id Dokumento ID
     * @param array $table Dokumento duomenys
     * @return call
     */
    function get_records_terms_by_table_id($id, $table = null)
    {
        return $this->get_record_term($id, $table, true);
    }

    /**
     * Gražina dokumento įrašo (-ų) apskaičiuotą terminą.
     * @author Justinas Malūkas
     * @param mixed $id Dokumento ID | Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param null|array $table [optional]<p>Dokumento duomenys. <b>Default</b>: NULL</p>
     * @param boolen $by_table [optional]<p><b>TRUE</b> jei ieškoma pagal dokumento ID. <b>Default</b>: FALSE</p>
     * @return boolean|null|string Jei nustatyta data - datos tekstas, jei nėra termino - NULL, kitaip FALSE
     */
    function get_record_term($id, $table = null, $by_table = false)
    {
        if (empty($id) === TRUE) {
            trigger_error(($by_table ? 'Table' : 'Record') . ' ID is empty', E_USER_WARNING);
            return false;
        }

        if (empty($table) === TRUE && $by_table === FALSE) {
            $record = $this->core->logic2->get_record_relation($id);
            $table = $this->core->logic2->get_document($record['TABLE_TO_ID']);
        }
        if (empty($table['CAN_TERM']) === TRUE)
            return null;

        $format = 'Y-m-d';
        $group = null;

        if ($table['CAN_TERM'] === '1') {
            //$condition = array('AND' => array('STATUS = ' . \Atp\Record::STATUS_INVESTIGATING));
            // TODO: Kai statusas "nutrauktas" ?
            $condition = array('AND' => array('STATUS IN (' . \Atp\Record::STATUS_DISCONTINUED . ',' . \Atp\Record::STATUS_INVESTIGATING . ')'));
        } elseif ($table['CAN_TERM'] === '2') {
            $condition = array('AND' => array('STATUS = ' . \Atp\Record::STATUS_EXAMINING));
        }

        if ($by_table === TRUE) {
            $condition['AND'][] = 'TABLE_TO_ID = "' . $id . '"';
            $fields = 'MIN(CREATE_DATE) as CREATE_DATE, RECORD_ID';
            $group = 'GROUP BY RECORD_ID';
        } else {
            $condition['AND'][] = 'ID = "' . $record['ID'] . '"';
            $fields = 'MIN(CREATE_DATE) as CREATE_DATE';
        }
        $data = $this->get_records_by_condition($condition, $fields, $group);

        if ($by_table === TRUE) {
            $ret = array();
            foreach ($data as $key => $arr) {
                if (empty($arr['CREATE_DATE']) === FALSE)
                    $ret[$arr['RECORD_ID']] = date($format, strtotime($arr['CREATE_DATE'] . ' + ' . $table['TERM_DAYS'] . 'days'));
            }
            return $ret;
        } else {
            if (empty($data[0]['CREATE_DATE']) === FALSE)
                return date($format, strtotime($data[0]['CREATE_DATE'] . ' + ' . $table['TERM_DAYS'] . 'days'));
        }

        return null;
    }

    /**
     * Apkaičiuoja priminimų laikus
     * @param string $record_nr Dokumento įrašo numeris
     * @param string $holidays Išeidinės dienos
     * @return boolean/array
     */
    function get_record_other_terms($record_nr, Array $holidays = array())
    {
        $document_id = current(explode('p', $record_nr));

        $documentManager = $this->core->factory->Document();
        $document_terms = $documentManager->GetTerms(array('DOCUMENT_ID' => $document_id));

        $fields = $this->core->logic2->get_fields_all(array(
            'ITEM_ID' => $document_id,
            'ITYPE' => 'DOC'
        ));
        //$record = $this->get_table_records($document_id, $record_nr);
        $record = $this->get_table_records(array(
            'DOC_ID' => $document_id,
            'RECORD_NR' => $record_nr
        ));
        $record = array_shift($record);

        if (count($document_terms)) {
            $term_dates = array();
            foreach ($document_terms as $term) {
                $field = &$fields[$term['FIELD_ID']];
                if (isset($field) === FALSE)
                    continue;

                // Data arba data ir laikas
                if (in_array($field['TYPE'], array(12, 14)) === FALSE)
                    continue;

                $curent_date = $record[$field['NAME']];
                $append_days = (int) $term['DAYS'];

                // Skaičiuojamos visos dienos
                if ($term['TYPE'] === '1') {
                    $term_dates[$term['ID']] = date('Y-m-d H:i:s', strtotime($curent_date . ' +' . $append_days . ' day'));
                } else
                // TODO: reikėju jungti tvarkaraščio posistemę
                // Skaičiuojamos darbo dienos
                if ($term['TYPE'] === '2') {
                    $term_dates[$term['ID']] = $this->add_business_days($curent_date, $append_days, $holidays, 'Y-m-d H:i:s');
                }
            }
            if (count($term_dates))
                return $term_dates;
        }
        return false;
    }

    function add_business_days($date, $days, $holidays, $dateformat = 'Y-m-d')
    {
        $date = strtotime($date);

        foreach ($holidays as &$val) {
            $val = date('Y-m-d', strtotime($val));
        }
        for ($i = 1; $i <= intval($days); $i++) { //Loops each day count
            //First, find the next available weekday because this might be a weekend/holiday
            while (date('N', $date) >= 6 || in_array(date('Y-m-d', $date), $holidays)) {
                $date = strtotime(date('Y-m-d', $date) . ' +1 day');
            }

            //Now that we know we have a business day, add 1 day to it
            $date = strtotime(date('Y-m-d', $date) . ' +1 day');

            //If this day that was previously added falls on a weekend/holiday, then find the next business day
            while (date('N', $date) >= 6 || in_array(date('Y-m-d', $date), $holidays)) {
                $date = strtotime(date('Y-m-d', $date) . ' +1 day');
            }
        }
        return date($dateformat, $date);
    }

    public function delete_table_structure($params = array())
    {
        $where = array();
        $arg = array();
        foreach ($params as $key => $value) {
            //if(substr($value, 0, 2) === 'IN')
            //	$where[] = '`' . trim($key, '#') . '`' . ' :' . trim($key, '#');
            if (is_int($key) === TRUE) {
                //$where[] = ':' . trim($key, '#');
                $where[] = ':' . $key;
                $key = '#' . $key . '#';
            } else {
                $where[] = '`' . trim($key, '#') . '`' . ' = :' . trim($key, '#');
            }
            $arg[$key] = $value;
        }
        $where = count($where) ? ' WHERE ' . join(' AND ', $where) : '';

        $qry = 'DELETE FROM ' . PREFIX . 'ATP_FIELD ' . $where;

        $this->core->db_query_fast($qry, $arg);

        #
        # Vartotojas lentelės informaciją.
        #
		$sp = array('<br/>Vartotojas lentelės informaciją. RawInfo - ' . json_encode($params));
        $this->core->manage->writeATPactionLog($sp);

        return true;
    }

    public function delete_deed_extends($params = array()/* , $bind = array() */)
    {
        $where = array();
        $arg = array();
        foreach ($params as $key => $value) {
            if (is_int($key) === TRUE) {
                //$where[] = ':' . trim($key, '#');
                $where[] = ':' . $key;
                $key = '#' . $key . '#';
            } else {
                $where[] = '`' . trim($key, '#') . '`' . ' = :' . trim($key, '#');
            }
            $arg[$key] = $value;
        }
        $where = count($where) ? ' WHERE ' . join(' AND ', $where) : '';

        //$qry = 'DELETE FROM ' . PREFIX . 'ATP_DEED_EXTENDS ' . $where;
        $qry = 'UPDATE ' . PREFIX . 'ATP_DEED_EXTENDS SET `VALID` = 0 ' . $where;

        //$arg = array_merge($bind, $arg);
        $this->core->db_query_fast($qry, $arg);
        /* */
//		#
        # Vartotojas pašalino .
        #
		//$sp = array('<br/>Vartotojas lentelės informaciją. RawInfo - ' . json_encode($params));
        //$this->core->manage->writeATPactionLog($sp);
        return true;
    }

    /**
     * Informacinių laukų informacija
     * @param array $params
     * @param int $params['ID'] Elemento ID
     * @param string $params['TYPE'] CLAUSE / ACT
     * @param boolen $all [optional]<p> Gražinti visus laukus. <b>Default:</b> FALSE</p>
     * @return array
     */
    public function get_info_fields($params, $all = false)
    {
        $ret = array();
        switch ($params['TYPE']) {
            case 'CLAUSE':
                // TODO: [done] Dokumento įrašo formoje nereikalingi laukai "Straipsnis" ir "Pavadinimas"
                $ret = array(
                    'header' => array(
                        4 => $this->core->lng('atp_clause_name'),
                        1 => $this->core->lng('atp_clause_section'),
                        2 => $this->core->lng('atp_clause_paragraph'),
                        3 => $this->core->lng('atp_clause_subparagraph'),
                        6 => $this->core->lng('atp_clause_penalty_min'),
                        7 => $this->core->lng('atp_clause_penalty_max'),
                        8 => $this->core->lng('atp_clause_notice'),
                        9 => $this->core->lng('atp_clause_notice_label'),
                        10 => $this->core->lng('atp_clause_term'),
                        11 => $this->core->lng('atp_clause_codex')
                    ),
                    'column' => array(
                        4 => 'NAME',
                        1 => 'SECTION',
                        2 => 'PARAGRAPH',
                        3 => 'SUBPARAGRAPH',
                        6 => 'PENALTY_MIN',
                        7 => 'PENALTY_MAX',
                        8 => 'NOTICE',
                        9 => 'NOTICE_LABEL',
                        10 => 'TERM',
                        11 => 'CODEX'
                    )
                );
                if ($all === TRUE) {
                    $ret['header'][0] = $this->core->lng('atp_clause_clause');
                    $ret['header'][5] = $this->core->lng('atp_clause_label');
                    $ret['column'][0] = 'CLAUSE';
                    $ret['column'][5] = 'LABEL';
                }

                if (isset($params['ID'])) {
                    $data = $this->core->logic2->get_clause(array('ID' => $params['ID']), true);
                    if (count($data)) {
                        $ret['data'] = array(
                            4 => $data['NAME'],
                            1 => $data['SECTION'],
                            2 => $data['PARAGRAPH'],
                            3 => $data['SUBPARAGRAPH'],
                            6 => $data['PENALTY_MIN'],
                            7 => $data['PENALTY_MAX'],
                            8 => $data['NOTICE'],
                            9 => $data['NOTICE_LABEL'],
                            10 => $data['TERM'],
                            11 => $data['CODEX']
                        );
                        $ret['value'] = array(
                            4 => $data['NAME'],
                            1 => (empty($data['SECTION']) ? '' : $data['SECTION'] . ' ' . $this->_prefix->se),
                            2 => (empty($data['PARAGRAPH']) ? '' : $data['PARAGRAPH'] . ' ' . $this->_prefix->pa),
                            3 => (empty($data['SUBPARAGRAPH']) ? '' : $data['SUBPARAGRAPH'] . ' ' . $this->_prefix->su),
                            6 => $data['PENALTY_MIN'] . ' EUR',
                            7 => $data['PENALTY_MAX'] . ' EUR',
                            8 => (empty($data['NOTICE']) ? 'Ne' : 'Taip'),
                            9 => $data['NOTICE_LABEL'],
                            10 => $data['TERM'] . ' d. d.',
                            11 => $data['CODEX']
                        );

                        if ($all === TRUE) {
                            $ret['data'][0] = $data['CLAUSE'];
                            $ret['data'][5] = $data['LABEL'];
                            $ret['value'][0] = $data['CLAUSE'] . ' ' . $this->_prefix->cl;
                            $ret['value'][5] = $data['LABEL'];
                        }
                    }
                }
                break;
            case 'ACT':
                $ret = array(
                    'header' => array(
                        //0 => $this->core->lng('atp_act_label'),
                        1 => $this->core->lng('atp_act_valid_from'),
                        2 => $this->core->lng('atp_act_valid_till'),
                        3 => $this->core->lng('atp_act_number'),
                        4 => $this->core->lng('atp_act_classificator')
                    ),
                    'column' => array(
                        //0 => 'LABEL',
                        1 => 'VALID_FROM',
                        2 => 'VALID_TILL',
                        3 => 'NR',
                        4 => 'CLASSIFICATOR'
                    )
                );

                if ($all === TRUE) {
                    $ret['header'][0] = $this->core->lng('atp_act_label');
                    $ret['column'][0] = 'LABEL';
                };
                if (isset($params['ID'])) {
                    $data = $this->core->logic2->get_act(array('ID' => $params['ID']));
                    if (count($data)) {
                        $ret['data'] = array(
                            1 => $data['VALID_FROM'],
                            2 => $data['VALID_TILL'],
                            3 => $data['NR'],
                            4 => $data['CLASSIFICATOR']
                        );
                        $classificator = $this->core->logic2->get_classificator(array(
                            'ID' => $data['CLASSIFICATOR']), true);
                        if (empty($classificator) === FALSE) {
                            $data['CLASSIFICATOR'] = $classificator['LABEL'];
                        }
                        $ret['value'] = array(
                            1 => date('Y-m-d', strtotime($data['VALID_FROM'])),
                            2 => date('Y-m-d', strtotime($data['VALID_TILL'])),
                            3 => $data['NR'],
                            4 => $data['CLASSIFICATOR']
                        );
                        if ($all === TRUE) {
                            $ret['data'][0] = $data['LABEL'];
                            $ret['value'][0] = $data['LABEL'];
                        }
                    }
                }
        }

        if (count($ret) === 0)
            return false;

        return $ret;
    }

    /**
     * Išsaugo dokumento įrašo mokėjimo ryšius
     * @author Justinas Malūkas
     */
    public function record_payments_save()
    {
        $param = array(
            'status' => (int) $_POST['status'],
            'payment' => $_POST['payment'],
            'check' => $_POST['check'],
            'paid' => 0
        );

        // Dokumento įrašo duomenys
        $record = $this->core->logic2->get_record_relation($_GET['id']);
        if (empty($record) === TRUE) {
            trigger_error('Record not found', E_USER_ERROR);
            exit;
        }

        $paymentManager = new \Atp\Document\Record\Payment($this->core);

        // Dokumento duomenys
        $document = $this->core->logic2->get_document($record['TABLE_TO_ID']);

        // Prie įrašo išsaugoti mokėjimai
        $payments = $this->core->logic2->get_payments(array(
            'DOCUMENT_ID' => $document['ID'],
            'RECORD_ID' => $record['ID']
        ));
        // Iš naujai saugomų pašalinau jau išsaugotus mokėjimų ryšius ir surenka senus mokėjimus trynimui
        $delete = array();
        $update = array();
        if (count($payments)) {
            foreach ($payments as &$payment) {
                if (isset($param['payment'][$payment['PAYMENT_ID']]) === TRUE) {
                    $arr = $payment;
                    $arr['PAID'] = $param['check'][$payment['PAYMENT_ID']];
                    $update[] = $arr;
                    unset($param['payment'][$payment['PAYMENT_ID']]);
                } else {
                    $delete[] = $payment['ID'];
                }
            }
        }

        // Suformuoja duomenis saugojimui
        $ins = array();
        $ins_vmi_data = array();
        if (count($param['payment'])) {
            foreach ($param['payment'] as $key => &$payment) {
                $ins[] = array(
                    'DOCUMENT_ID' => $document['ID'],
                    'RECORD_ID' => $record['ID'],
                    'PAYMENT_ID' => $key,
                    'PAID' => $param['check'][$key]
                );

                // Mokėjimo duomenys iš VMI registro
                $data = array('registry' => 'vmi', 'SEARCH_ID' => $key);
                $ins_vmi_data[] = $this->core->getRDBWsData(
                        'getVMISearchResult', $data
                    )[0];
            }
        }

        // Ištrina senus nereikalingus ryšius
        $delete = array_filter($delete);
        if (count($delete) > 0) {
            $paymentManager->removePaymentData(array(
                0 => '`ID` IN(' . join(',', $delete) . ')'
            ));
        }
        // Įšsaugo naujus ryšius
        if (count($ins) > 0) {
            $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD_PAYMENTS', $ins);
            $this->core->db_ATP_insert(PREFIX . 'ATP_PAYMENTS_DATA', $ins_vmi_data, 2);
        }
        // Atnaujina senus ryšius
        if (count($update) > 0) {
            $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD_PAYMENTS', $update, 2);
        }

        // Pažymimas apmokėjimas
        $result = $paymentManager->SetRecordPaymentOption(
            new \Atp\Document\Record($record['ID'], $this->core), $param['status']
        );

        // Atnaujinamos būsenos
        $this->update_record_status($record['ID']);

        return true;
    }

    /**
     * Pagal šablono kintamąjį randa kelią iki dokumento lauko
     * @param string $variable Kintamasis
     * @param array $relation [optional] Masyvas kelio nurodymui
     * @return array
     */
    function parse_variable($variable, $relation = array())
    {
        if (empty($variable))
            return $relation;

        $variable = str_replace(array('${', '}'), '', $variable);
        if (preg_match('/T([I]*)([^A-Z]+)/i', $variable, $m)) {
            $type = array_search('', $this->core->_prefix->item);
            $item = ($m[1] === '' ? 'FIELD' : 'INFO');
            $field_id = $m[2];
        } else {

            // Web-servisas
            if (preg_match('/^(WS)([^' . $this->core->_prefix->item['WS'] . ']+)(' . $this->core->_prefix->item['WS'] . ')([0-9]+)/i', $variable, $m)) {
                $type = array_search($m[3], $this->core->_prefix->item);
                $item = $m[2];
                $field_id = $m[4];
            } else
            // Teisės akto punktas / straipsnis
            if (preg_match('/^(' . $this->core->_prefix->item['ACT'] . '|' . $this->core->_prefix->item['CLAUSE'] . ')([I]*)([0-9]+)/i', $variable, $m)) {
                $type = array_search($m[1], $this->core->_prefix->item);
                $item = ($m[2] === '' ? 'FIELD' : 'INFO');
                $field_id = $m[3];
            } else
            // Klasifikatorius
            if (preg_match('/^(' . $this->core->_prefix->item['CLASS'] . ')([0-9]+)' . $this->core->_prefix->item['CLASS'] . '([0-9]+)/i', $variable, $m)) {
                $type = array_search($m[1], $this->core->_prefix->item);
                $item = $m[2];
                $field_id = $m[3];
            }
        }

        if (isset($field_id)) {
            $relation[$type][$item][$field_id] = null;
            $ret = &$relation[$type][$item][$field_id];

            $pos = strpos($variable, $m[0]);
            $variable = substr($variable, $pos + strlen($m[0]));

            if ($variable !== FALSE) {
                $ret = $this->parse_variable($variable, $arr);
            }
        }

        return $relation;
    }

    /**
     * Gauna lauko informaciją pagal šablono kintamojo kelią
     * @param type $varpath
     * @param type $document_id
     * @param type $field
     * @return boolean|array
     */
    function get_field_info_by_varpath($varpath, $document_id, $field = array())
    {
        $key = key($varpath);
        switch ($key) {
            // Dokumentas
            case 'DOC':
                $item = key($varpath[$key]);
                $field_id = key($varpath[$key][$item]);

                if ($item !== 'INFO')
                    $field = $this->core->logic2->get_fields(array('ITYPE' => $key,
                        'ITEM_ID' => $document_id,
                        'NAME' => $this->core->_prefix->column . $field_id), true);

                $r = $varpath[$key][$item][$field_id];
                break;
            // Webservisas
            case 'WS':
                $item = key($varpath[$key]);
                $field_id = key($varpath[$key][$item]);

                $field = $this->core->logic2->get_fields(array('ITYPE' => $key, 'ITEM_ID' => $field['ID'],
                    'NAME' => 'WCOL_' . $field['ID'] . '_' . $field_id), true);

                $r = $varpath[$key][$item][$field_id];
                break;
            // Klasifikatorius
            case 'CLASS':
                $item = key($varpath[$key]);
                $field_id = key($varpath[$key][$item]);

                $field = $this->core->logic2->get_fields(array('ITYPE' => $key, 'ITEM_ID' => $item,
                    'NAME' => 'KCOL_' . $item . '_' . $field_id), true);

                $r = $varpath[$key][$item][$field_id];
                break;
            // Teisės akto punktas
            case 'ACT':
                $item = key($varpath[$key]);
                $field_id = key($varpath[$key][$item]);

                if ($item === 'FIELD') {
                    $field = $this->core->logic2->get_fields(array('ITYPE' => $key,
                        'ITEM_ID' => $field['ID'],
                        'NAME' => 'ACOL_' . $field_id), true);
                }

                $r = $varpath[$key][$item][$field_id];
                break;
            // Straipsnis
            case 'CLAUSE':
                $item = key($varpath[$key]);
                $field_id = key($varpath[$key][$item]);

                if ($item === 'FIELD') {
                    $field = $this->core->logic2->get_fields(array('ITYPE' => $key,
                        'ITEM_ID' => $field['ID'],
                        'NAME' => 'SCOL_' . $field_id), true);
                }

                $r = $varpath[$key][$item][$field_id];
                break;
            default:
                return false;
        }

        if (empty($r) === FALSE)
            $field = $this->get_field_info_by_varpath($r, $document_id, $field);

        return $field;
    }

    /**
     *
     * @param array $varpath Lauko kelio masyvas. Pagal kintamąjį sugeneruojamas \Atp\Logic::parse_variable() rezultatas
     * @param int $document_id Dokumento ID
     * @param int $record_id Dokumento įrašo ID iš TBL_{dokumento_id}
     * @param array $field [optional] Duomenų rinkimui metodo viduje
     * @return boolean
     */
    function get_field_by_varpath($varpath, $document_id, $record_id, $globalRecordId, $field = array())
    {
        $key = key($varpath);
        switch ($key) {
            // Dokumentas
            case 'DOC':
                $item = key($varpath[$key]);
                $field_id = key($varpath[$key][$item]);
                if ($item === 'INFO') {
                    $doc_record = $this->core->logic2->get_db(PREFIX . 'ATP_TBL_' . $document_id, array(
                        'ID' => $record_id), true, null, false, 'ID, RECORD_ID');
                    //$record = $this->get_table_records($document_id, $doc_record['RECORD_ID'], 0, 1);
                    $fields = $this->record_worker_info_to_fields($doc_record['RECORD_ID']/* , $record[0]['STATUS'] */);
                    $fields2 = $this->record_post_info_to_fields($doc_record['RECORD_ID']/* , $record[0]['STATUS'] */);
                    $fields3 = $this->record_penalty_info_to_fields($doc_record['RECORD_ID']);

                    foreach ($fields2 as $key => $val) {
                        $fields[$key] = $val;
                    }
                    foreach ($fields3 as $key => $val) {
                        $fields[$key] = $val;
                    }

                    if (isset($fields[$field_id]['VALUE'])) {
                        $field['INFO'] = null;
                        $field['VALUE'] = $fields[$field_id]['VALUE'];
                    } else {
                        $field['INFO'] = null;
                        $field['VALUE'] = false;
                    }
                } else {
                    $field['INFO'] = $this->core->logic2->get_fields(array('ITYPE' => $key,
                        'ITEM_ID' => $document_id, 'NAME' => $this->core->_prefix->column . $field_id), true);

//                    $table_name = $this->core->_prefix->table . (int) $document_id;
//                    $qry = 'SELECT `' . $field['INFO']['NAME'] . '`
//							FROM
//								' . PREFIX . 'ATP_' . $table_name . ' tn
//							WHERE
//								tn.ID = ' . $record_id . '
//							LIMIT 1';
//                    $result = $this->core->db_query_fast($qry);
//                    $row = $this->core->db_next($result);
//                    if (isset($row[$field['INFO']['NAME']]))
//                        $field['VALUE'] = $row[$field['INFO']['NAME']];
//                    else {
//                        $field['VALUE'] = false;
//                    }

                    $field['VALUE'] = $this->core->factory->Record()->getFieldValue($globalRecordId, $field['INFO']['ID']);
                }

                $r = $varpath[$key][$item][$field_id];
                break;
            // Webservisas
            case 'WS':
                $item = key($varpath[$key]);
                $field_id = key($varpath[$key][$item]);
                $field['INFO'] = $this->core->logic2->get_fields(array('ITYPE' => $key,
                    'ITEM_ID' => $field['INFO']['ID'], 'NAME' => 'WCOL_' . $field['INFO']['ID'] . '_' . $field_id), true);
//                $field['VALUE'] = $this->get_extra_field_value($document_id, $record_id, $field['INFO']['ID']);
                $field['VALUE'] = $this->core->factory->Record()->getFieldValue($globalRecordId, $field['INFO']['ID']);

                $r = $varpath[$key][$item][$field_id];
                break;
            // Klasifikatorius
            case 'CLASS':
                $item = key($varpath[$key]);
                $field_id = key($varpath[$key][$item]);
                $field['INFO'] = $this->core->logic2->get_fields(array('ITYPE' => $key,
                    'ITEM_ID' => $item, 'NAME' => 'KCOL_' . $item . '_' . $field_id), true);
//                $field['VALUE'] = $this->get_extra_field_value($document_id, $record_id, $field['INFO']['ID']);
                $field['VALUE'] = $this->core->factory->Record()->getFieldValue($globalRecordId, $field['INFO']['ID']);

                $r = $varpath[$key][$item][$field_id];
                break;
            // Teisės akto punktas
            case 'ACT':
                $item = key($varpath[$key]);
                $field_id = key($varpath[$key][$item]);

                // Gauna dokumento įraše pasirinktą teisės akto punktą
                if ($field['INFO']['ITYPE'] !== 'DOC') {
//                    $act_id = $this->get_extra_field_value($document_id, $record_id, $field_id);
                    $act_id = $this->core->factory->Record()->getFieldValue($globalRecordId, $field_id);
                } else {
                    $table_name = $this->core->_prefix->table . (int) $document_id;
//                    $qry = 'SELECT `' . $field['INFO']['NAME'] . '`
//							FROM
//								' . PREFIX . 'ATP_' . $table_name . ' tn
//							WHERE
//								tn.ID = ' . $record_id . '
//							LIMIT 1';
//
//                    $result = $this->core->db_query_fast($qry);
//                    $row = $this->core->db_next($result);
//                    if (isset($row[$field['INFO']['NAME']])) {
//                        $id = $row[$field['INFO']['NAME']];
//                    }
                    $id = $this->core->factory->Record()->getFieldValue($globalRecordId, $field['INFO']['ID']);


                    // Jei tėvinis laukas yra veika, tai išrenka galiojantį straipsnį
                    if ($field['INFO']['TYPE'] == 23) {
                        // Veika
                        $deed = $this->get_deed(array('ID' => $id));
                        // Veiką praplečia
                        $ddata = $this->get_deed_extend_data(array('DEED_ID' => $deed['ID']));

                        // Surenka veikos praplečiančių elementų ID
                        $act_ids = array();
                        foreach ($ddata as $arr) {
                            if ($arr['ITYPE'] === 'ACT') {
                                $act_ids[$arr['ID']] = $arr['ITEM_ID'];
                            }
                        }

                        // Data veikai
//                        $table = $this->core->logic2->get_document($document_id);
//                        $table_structure = $this->get_table_fields($document_id);
//                        $record_date_field = $table_structure[$table['VIOLATION_DATE']];
                        $documentOptions = $this->core->factory->Document()->getOptions($document_id);
                        if (empty($documentOptions->violation_date) === false) {
//                            $qry = 'SELECT `' . $record_date_field['NAME'] . '`
//									FROM
//										' . PREFIX . 'ATP_' . $table_name . ' tn
//									WHERE
//										tn.ID = ' . $record_id . '
//									LIMIT 1';
//                            $result = $this->core->db_query_fast($qry);
//                            $rrow = $this->core->db_next($result);
//                            if (isset($rrow[$record_date_field['NAME']])) {
//                                $record_date = $rrow[$record_date_field['NAME']];
//                            }
                            $record_date = $this->core->factory->Record()->getFieldValue($globalRecordId, $documentOptions->violation_date);



                            // Straipsnio laukai
                            if (count($act_ids)) {
                                $data = $this->core->logic2->get_act(array(
                                    0 => '`ID` IN(' . join(',', $act_ids) . ')',
                                    1 => '"' . $record_date . '"' . ' > `VALID_FROM` AND "' . $record_date . '" <= `VALID_TILL`'
                                ));
                                $data = array_values($data);
                                $act_id = $data[0]['ID'];
                            }
                        }
                    } else {
                        $act_id = $id;
                    }
                }

                if (empty($act_id) === TRUE)
                    return false;

                if ($item === 'FIELD') {
                    $field['INFO'] = $this->core->logic2->get_fields(array('ITYPE' => $key,
                        'ITEM_ID' => $field['INFO']['ID'], 'NAME' => 'ACOL_' . $field_id), true);
//                    $field['VALUE'] = $this->get_extra_field_value($document_id, $record_id, $field['INFO']['ID']);
                    $field['VALUE'] = $this->core->factory->Record()->getFieldValue($globalRecordId, $field['INFO']['ID']);
                } elseif ($item === 'INFO') {
                    $fields = $this->get_info_fields(array('TYPE' => $key, 'ID' => $act_id));

                    if (isset($fields['value'][$field_id])) {
                        $field['INFO'] = null;
                        $field['VALUE'] = $fields['value'][$field_id];
                    } else {
                        $field['INFO'] = null;
                        $field['VALUE'] = false;
                    }
                } else {
                    $field['INFO'] = null;
                    $field['VALUE'] = false;
                }

                $r = $varpath[$key][$item][$field_id];
                break;
            // Straipsnis
            case 'CLAUSE':
                $item = key($varpath[$key]);
                $field_id = key($varpath[$key][$item]);

                // Gauna dokumento įraše pasirinktą teisės akto punktą
                if ($field['INFO']['ITYPE'] !== 'DOC') {
//                    $clause_id = get_extra_field_value($document_id, $record_id, $field_id);
                    $clause_id = $this->core->factory->Record()->getFieldValue($globalRecordId, $field_id);
                } else {
                    $table_name = $this->core->_prefix->table . (int) $document_id;
                    $qry = 'SELECT `' . $field['INFO']['NAME'] . '`
							FROM
								' . PREFIX . 'ATP_' . $table_name . ' tn
							WHERE
								tn.ID = ' . $record_id . '
							LIMIT 1';

                    $result = $this->core->db_query_fast($qry);
                    $row = $this->core->db_next($result);
                    if (isset($row[$field['INFO']['NAME']]))
                        $id = $row[$field['INFO']['NAME']];


                    // Jei tėvinis laukas yra veika, tai išrenka galiojantį straipsnį
                    if ($field['INFO']['TYPE'] == 23) {
                        // Veika
                        $deed = $this->get_deed(array('ID' => $id));
                        // Veiką praplečia
                        $ddata = $this->get_deed_extend_data(array('DEED_ID' => $deed['ID']));

                        // Surenka veikos praplečiančių elementų ID
                        $clause_ids = array();
                        foreach ($ddata as $arr) {
                            if ($arr['ITYPE'] === 'CLAUSE')
                                $clause_ids[$arr['ID']] = $arr['ITEM_ID'];
                        }

                        // Data veikai
//                        $table = $this->core->logic2->get_document($document_id);
                        $table_structure = $this->get_table_fields($document_id);
                        $record_date_field_id = $this->core->factory->Document()->getOptions($document_id)->violation_date;
                        $record_date_field = $table_structure[$record_date_field_id];
                        if (empty($record_date_field) === FALSE) {
                            $qry = 'SELECT `' . $record_date_field['NAME'] . '`
								FROM
									' . PREFIX . 'ATP_' . $table_name . ' tn
								WHERE
									tn.ID = ' . $record_id . '
								LIMIT 1';
                            $result = $this->core->db_query_fast($qry);
                            $rrow = $this->core->db_next($result);
                            if (isset($rrow[$record_date_field['NAME']]))
                                $record_date = $rrow[$record_date_field['NAME']];



                            // Straipsnio laukai
                            if (count($clause_ids)) {
                                $data = $this->core->logic2->get_clause(array(
                                    0 => '`ID` IN(' . join(',', $clause_ids) . ')',
                                    1 => '"' . $record_date . '"' . ' > `VALID_FROM` AND "' . $record_date . '" <= `VALID_TILL`'
                                ));
                                $data = array_values($data);
                                $clause_id = $data[0]['ID'];
                            }
                        }
                    } else {
                        $clause_id = $id;
                    }
                }

                if (empty($clause_id) === TRUE) {
                    return false;
                }

                if ($item === 'FIELD') {
                    $field['INFO'] = $this->core->logic2->get_fields(array('ITYPE' => $key,
                        'ITEM_ID' => $field['INFO']['ID'], 'NAME' => 'SCOL_' . $field_id), true);
//                    $field['VALUE'] = $this->get_extra_field_value($document_id, $record_id, $field['INFO']['ID']);
                    $field['VALUE'] = $this->core->factory->Record()->getFieldValue($globalRecordId, $field['INFO']['ID']);
                } elseif ($item === 'INFO') {
                    $fields = $this->get_info_fields(array('TYPE' => $key, 'ID' => $clause_id));

                    if (isset($fields['value'][$field_id])) {
                        $field['INFO'] = null;
                        $field['VALUE'] = $fields['value'][$field_id];
                    } else {
                        $field['INFO'] = null;
                        $field['VALUE'] = false;
                    }
                } else {
                    $field['INFO'] = null;
                    $field['VALUE'] = false;
                }

                $r = $varpath[$key][$item][$field_id];
                break;
            default:
                return false;
        }

        if (empty($r) === FALSE)
            $field = $this->get_field_by_varpath($r, $document_id, $record_id, $globalRecordId, $field);

        return $field;
    }

    /**
     * Dokumento tarpines busenos laikotarpis dienomis
     * @param type $document_id Dokumento ID
     * @return boolean|int false - nerastas dokumentas arba neturi tarpines busenos; int - tarpines busenso dienu skaicius
     */
    function get_document_status_idle_days($document_id)
    {
        $documentOptions = $this->core->factory->Document()->getOptions($document_id);
        if (empty($documentOptions) === FALSE && $documentOptions->can_idle === 1)
            return $documentOptions->can_idle;

        return false;
    }

    /**
     * Gauna laisvą 5 min. intervalą darbuotojo tvarkaraštyje, kuriam priskirtas dokumento įrašas
     * @param array $param	Parametrai
     * @param mixed $param['RECORD'] Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param string $param['DATETIME'] Unix timestamp. Default: time()
     * @param int $param['RESULT_COUNT'] Default: 1
     * @return array
     */
    function get_document_person_free_time($data)
    {
        $default = array(
            'DATETIME' => date('Y-m-d H:i:s'),
            'RESULT_COUNT' => 1
        );
        $data = array_merge($default, $data);

        //if (preg_match('/[0-9]+p[0-9]+/', $data['RECORD']) !== 1)
        //	return;
        //
        // Dokumento įrašo inforamcija
        $record = $this->core->logic2->get_record_relation($data['RECORD']);
        if (ctype_digit($record['ID']) === FALSE)
            return;

        // Dokumento įraše nurodytas sekantis nagrinėtojas
        $worker = $this->core->logic2->get_record_worker(array('A.RECORD_ID = ' . (int) $record['ID'],
            'A.TYPE = "next"'), true, null, 'A.ID, D.PEOPLE_ID, G.ID as USER_ID');
        if (empty($worker) === TRUE)
            return;

        // Tikrinami pateikti duomenys
        $parsed_date = date_parse($data['DATETIME']);
        if ($parsed_date['error_count'] === 0) {
            $from_date = $data['DATETIME'];
        } else {
            $from_date = $default['DATETIME'];
        }
        if ((int) $data['RESULT_COUNT'] < 1) {
            $data['RESULT_COUNT'] = $default['RESULT_COUNT'];
        }

        $result = array();
        $param = array(
            'USER_ID' => $worker['USER_ID'],
            'PERSON_ID' => $worker['PEOPLE_ID'],
            'FROM_DATE' => strtotime($from_date),
            'DURATION' => 300);


        $sch = prepare_schedule();
        for ($i = 0; $i < $data['RESULT_COUNT']; $i++) {
            // Laisvas darbo grafiko intervalas
            $free_datetime = $sch->logic->getFreeTime($param, 0);

            //$free_datetime['TILL'] = strtotime('+ ' . $param['DURATION'] . ' seconds', $free_datetime['FROM']);
            $result[] = array(
                'FROM' => array(
                    'DATE' => date('Y-m-d', $free_datetime['FROM']),
                    'TIME' => date('H:i:s', $free_datetime['FROM'])
                ),
                'TILL' => array(
                    'DATE' => date('Y-m-d', $free_datetime['TILL']),
                    'TIME' => date('H:i:s', $free_datetime['TILL'])
                )
            );

            // Sekantčio rezultato ieškoti nuo rezultato sekančios dienos pradžios
            //$param['FROM_DATE'] = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime('+ 1 day', $free_datetime['FROM']))));
            $param['FROM_DATE'] = strtotime(date('Y-m-d', strtotime('+ 1 day', $free_datetime['FROM'])));
        }
        return $result;
    }

    /**
     * ??
     * <br>Rezervuoti pavadinimai TICOL_20 - TICOL_30
     * @param mixed $record [optional]<p>Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>. <b>Default:</b> NULL</p>
     * @return array
     */
    function record_post_info_to_fields($record = null)
    {
        /* Laukai private $data = array('NAME' =>'','SURNAME' =>'','COMPANY' =>'','MUNICIPALITY' =>'','CITY' =>'','STREET' =>'','HOUSE_NO' =>'','FLAT_NO' =>'','POST_CODE' =>'','ACQ_CONFIRMATION' =>'','DOC_ID' =>''); */
        $fields = array(
            '20' => array('LABEL' => /* NAME */'Gavėjo vardas'/* $this->core->lng('record_investigator') */,
                'NAME' => 'TICOL_20', 'TYPE' => 7, 'SORT' => 2),
            '21' => array('LABEL' => /* SURNAME */'Gavėjo pavardė'/* $this->core->lng('record_phone') */,
                'NAME' => 'TICOL_21', 'TYPE' => 7, 'SORT' => 2),
            '22' => array('LABEL' => /* COMPANY */'Gavejo juridinis pavadinimas'/* $this->core->lng('record_email') */,
                'NAME' => 'TICOL_22', 'TYPE' => 7, 'SORT' => 2),
            '23' => array('LABEL' => /* CITY */'Gavėjo gyvenvietė'/* $this->core->lng('record_date') */,
                'NAME' => 'TICOL_23', 'TYPE' => 7, 'SORT' => 2),
            '24' => array('LABEL' => /* STREET */'Gavėjo gatvė'/* $this->core->lng('record_investigator_first_name') */,
                'NAME' => 'TICOL_24', 'TYPE' => 7, 'SORT' => 2),
            '25' => array('LABEL' => /* HOUSE_NO */'Gavėjo namas'/* $this->core->lng('record_investigator_last_name') */,
                'NAME' => 'TICOL_25', 'TYPE' => 7, 'SORT' => 2),
            '26' => array('LABEL' => /* FLAT_NO */'Gavėjo butas'/* $this->core->lng('record_investigator_position') */,
                'NAME' => 'TICOL_26', 'TYPE' => 7, 'SORT' => 2),
            '27' => array('LABEL' => /* POST_CODE */'Gavėjo pašto kodas'/* $this->core->lng('record_investigator_cabinet') */,
                'NAME' => 'TICOL_27', 'TYPE' => 7, 'SORT' => 2),
            '28' => array('LABEL' => /* RN */'Gavėjo RN kodas'/* $this->core->lng('record_investigator_cabinet') */,
                'NAME' => 'TICOL_28', 'TYPE' => 7, 'SORT' => 2),
            '29' => array('LABEL' => /* RN_img */'Gavėjo RN paveiksliukas'/* $this->core->lng('record_investigator_cabinet') */,
                'NAME' => 'TICOL_29', 'TYPE' => 7, 'SORT' => 2),
            '30' => array('LABEL' => /* RN_img */'Gavėjo laukas'/* $this->core->lng('record_investigator_cabinet') */,
                'NAME' => 'TICOL_30', 'TYPE' => 7, 'SORT' => 2)
        );

        // Jei paduotas dokumento įrašas, užpildo laukus įrašo duomenimis
        if ($record !== null) {
            $record = $this->core->logic2->get_record_relation($record);
            $qry = 'SELECT NAME, SURNAME, COMPANY, CITY, STREET, HOUSE_NO, FLAT_NO, POST_CODE, RN FROM ' . PREFIX . 'ATP_POST_RN_REGISTRY WHERE DOC_ID=' . $record['ID'];
            $data = $this->core->db_next($this->core->db_query_fast($qry));
            if (empty($data['COMPANY']) === false) {
                $receiver_field = $data['COMPANY'];
            } else {
                $receiver_field = $data['NAME'] . ' ' . $data['SURNAME'];
            }
            $values = array(
                '20' => $data['NAME'],
                '21' => $data['SURNAME'],
                '22' => $data['COMPANY'],
                '23' => $data['CITY'],
                '24' => $data['STREET'],
                '25' => $data['HOUSE_NO'],
                '26' => (empty($data['FLAT_NO']) === FALSE ? '-' . $data['FLAT_NO'] : ""),
                '27' => $data['POST_CODE'],
                '28' => $data['RN'],
                '29' => $this->core->config['GLOBAL_REAL_URL'] . 'subsystems/atp/class/Post/Barcode/Images/' . $data['RN'] . '.gif',
                '30' => $receiver_field
            );
            foreach ($fields as $key => &$arr) {
                $arr['VALUE'] = $values[$key];
            }
        }

        return $fields;
    }

    /**
     * Baustumo laukų informacija, ir reikšmės, jei pateikas <i>$record</i>.<br>Rezervuoti pavadinimai TICOL_40 - TICOL_??
     * @param mixed $record [optional]<p>Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>. <b>Default:</b> NULL</p>
     * @return array
     */
    function record_penalty_info_to_fields($record = null)
    {
        $fields = array(
            '40' => array('LABEL' => 'Baustumo straipsnis', 'NAME' => 'TICOL_40',
                'TYPE' => 7, 'SORT' => 2),
            '41' => array('LABEL' => 'Baustumo teisės akto informacija', 'NAME' => 'TICOL_41',
                'TYPE' => 7, 'SORT' => 2),
            '42' => array('LABEL' => 'Baustumo teisės aktas', 'NAME' => 'TICOL_42',
                'TYPE' => 7, 'SORT' => 2),
            '43' => array('LABEL' => 'Baustumo tekstas', 'NAME' => 'TICOL_43', 'TYPE' => 7,
                'SORT' => 2),
            '44' => array('LABEL' => 'Baustumo teisės akto numeris', 'NAME' => 'TICOL_44',
                'TYPE' => 7, 'SORT' => 2),
            '45' => array('LABEL' => 'Baustumo rūšis', 'NAME' => 'TICOL_45', 'TYPE' => 7,
                'SORT' => 2),
            '46' => array('LABEL' => 'Baustumo automobilio numeris', 'NAME' => 'TICOL_46',
                'TYPE' => 7, 'SORT' => 2)
        );


        // Jei paduotas dokumento įrašas, užpildo laukus įrašo duomenimis
        /* if ($record !== null) {
          $record = $this->core->logic2->get_record_relation($record);
          if((int) $record['TABLE_TO_ID'] !== \Atp\Document::DOCUMENT_INFRACTION) {
          // TODO: surasti 52 dokumento irasa
          }
          $qry = 'SELECT NAME FROM ' . PREFIX . 'ATP_KAZKAS_LENTELE WHERE KAZKAS_ID = ' . $record['ID'];
          $data = $this->core->db_next($this->core->db_query_fast($qry));
          if (empty($data['COMPANY']) === false) {
          $receiver_field = $data['COMPANY'];
          } else {
          $receiver_field = $data['NAME'] . ' ' . $data['SURNAME'];
          }
          $values = array(
          '40' => 'baustumassssss'
          );
          foreach ($fields as $key => &$arr) {
          $arr['VALUE'] = $values[$key];
          }
          } */

        return $fields;
    }

    /**
     * Tiriančio asmens laukų informacija, ir reikšmės, jei pateikas <i>$record</i>.<br>Rezervuoti pavadinimai TICOL_0 - TICOL_11
     * @param mixed $record [optional]<p>Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>. <b>Default:</b> NULL</p>
     * @return array
     */
    function record_worker_info_to_fields($record = null)
    {
        // Laukai
        $fields = array(
            0 => array('LABEL' => $this->core->lng('record_investigator'), 'NAME' => 'TICOL_0',
                'TYPE' => 7, 'SORT' => 2),
            1 => array('LABEL' => $this->core->lng('record_phone'), 'NAME' => 'TICOL_1',
                'TYPE' => 7, 'SORT' => 2),
            2 => array('LABEL' => $this->core->lng('record_email'), 'NAME' => 'TICOL_2',
                'TYPE' => 7, 'SORT' => 2),
            3 => array('LABEL' => $this->core->lng('record_date'), 'NAME' => 'TICOL_3',
                'TYPE' => 7, 'SORT' => 2),
            4 => array('LABEL' => $this->core->lng('record_investigator_first_name'),
                'NAME' => 'TICOL_4',
                'TYPE' => 7, 'SORT' => 2),
            5 => array('LABEL' => $this->core->lng('record_investigator_last_name'),
                'NAME' => 'TICOL_5',
                'TYPE' => 7, 'SORT' => 2),
            6 => array('LABEL' => $this->core->lng('record_investigator_position'),
                'NAME' => 'TICOL_6',
                'TYPE' => 7, 'SORT' => 2),
            7 => array('LABEL' => $this->core->lng('record_investigator_cabinet'),
                'NAME' => 'TICOL_7',
                'TYPE' => 7, 'SORT' => 2),
            8 => array('LABEL' => $this->core->lng('record_investigator_institution'),
                'NAME' => 'TICOL_8', 'TYPE' => 7, 'SORT' => 2),
            9 => array('LABEL' => $this->core->lng('record_investigator_unit'), 'NAME' => 'TICOL_9',
                'TYPE' => 7, 'SORT' => 2),
            10 => array('LABEL' => $this->core->lng('record_investigator_subunit'),
                'NAME' => 'TICOL_10',
                'TYPE' => 7, 'SORT' => 2),
            11 => array('LABEL' => $this->core->lng('record_investigator_institution_address'),
                'NAME' => 'TICOL_11', 'TYPE' => 7, 'SORT' => 2)
        );

        // Jei paduotas dokumento įrašas, užpildo laukus įrašo duomenimis
        if ($record !== null) {
            $record = $this->core->logic2->get_record_relation($record);
            if (empty($record) === TRUE) {
                trigger_error('Record not found', E_USER_ERROR);
                exit;
            }

            if ((int) $record['STATUS'] === \Atp\Record::STATUS_INVESTIGATING) {
                $workerTypes = [\Atp\Record\Worker::TYPE_INVESTIGATOR];
            } else
                // TODO: Kai statusas "nutrauktas" ?
            if ((int) $record['STATUS'] === \Atp\Record::STATUS_DISCONTINUED) {
                $workerTypes = [\Atp\Record\Worker::TYPE_INVESTIGATOR, \Atp\Record\Worker::TYPE_EXAMINATOR];
            } else
            // jei nagrinėjimo dokumentas, ieško nagrinėtojo
            if ((int) $record['STATUS'] === \Atp\Record::STATUS_EXAMINING) {
                $workerTypes = [\Atp\Record\Worker::TYPE_EXAMINATOR];
            } else {
                $workerTypes = [\Atp\Record\Worker::TYPE_INVESTIGATOR, \Atp\Record\Worker::TYPE_EXAMINATOR];
            }

            $worker = $this->core->logic2->get_record_worker(array(
                'F.RECORD_ID = "' . $record['RECORD_ID'] . '"',
                'A.TYPE IN("' . join('","', $workerTypes) . '")'
                ), true);

            // Surenka darbuotojo informaciją
            $select = 'A.ID,B.FIRST_NAME, B.LAST_NAME, F.SHOWS as "POSITION",
			A.ROOM, B.PHONE_MOBILE, B.PHONE_FULL, B.PHONE_SHORT, B.EMAIL,
			A.STRUCTURE_ID, C.PARENT_ID as "STRUCTURE_PARENT_ID", C.SHOWS AS "STRUCTURE_NAME", C.ADDRESS as "STRUCTURE_ADDRESS", C.PHONE as "STRUCTURE_PHONE", C.EMAIL as "STRUCTURE_EMAIL",
			C.TYPE_ID as "STRUCTURE_TYPE_ID"';
            $worker = $this->core->logic2->get_worker_info(array(
                'A.ID = ' . (int) $worker['WORKER_ID']
                ), true, null, $select);

            // Suformuoja dokumento įrašo laukų reikšmes
            $data = array();

            // Padalinys (skyrius)
            $data['EXAMINATOR_SUBUNIT_ID'] = $this->get_parent_structure($worker['STRUCTURE_ID'], 7);
            if (empty($data['EXAMINATOR_SUBUNIT_ID']) === FALSE) {
                $arr = $this->core->logic2->get_db(PREFIX . 'MAIN_STRUCTURE', array(
                    'ID' => $data['EXAMINATOR_SUBUNIT_ID']), true, null, false, 'A.ID, A.SHOWS, A.ADDRESS');
                $data['EXAMINATOR_SUBUNIT'] = $arr['SHOWS'];
            }
            // Dalinys (departamentas)
            $data['EXAMINATOR_UNIT_ID'] = $this->get_parent_structure($worker['STRUCTURE_ID'], 6);
            if (empty($data['EXAMINATOR_UNIT_ID']) === FALSE) {
                $arr = $this->core->logic2->get_db(PREFIX . 'MAIN_STRUCTURE', array(
                    'ID' => $data['EXAMINATOR_UNIT_ID']), true, null, false, 'A.ID, A.SHOWS, A.ADDRESS');
                $data['EXAMINATOR_UNIT'] = $arr['SHOWS'];
            }
            // Institucija
            $data['EXAMINATOR_INSTITUTION_ID'] = $this->get_parent_structure(
                $worker['STRUCTURE_ID'], 12
            );
            if (empty($data['EXAMINATOR_INSTITUTION_ID']) === FALSE) {
                $arr = $this->core->logic2->get_db(PREFIX . 'MAIN_STRUCTURE', array(
                    'ID' => $data['EXAMINATOR_INSTITUTION_ID']), true, null, false, 'A.ID, A.SHOWS, A.ADDRESS');
                // Institucijos pavadinimas
                $data['EXAMINATOR_INSTITUTION_NAME'] = $arr['SHOWS'];
                // Institucijos adresas
                $data['EXAMINATOR_INSTITUTION_ADDRESS'] = $arr['ADDRESS'];
            }
            // Nagrinėtojo vardas ir pavardė
            $data['EXAMINATOR'] = join(' ', array_filter(array($worker['FIRST_NAME'],
                $worker['LAST_NAME'])));
            // Kabinetas
            $data['EXAMINATOR_CABINET'] = $worker['ROOM'];
            // El. paštas
            $data['EMAIL'] = current(array_filter(array($worker['EMAIL'], $worker['STRUCTURE_EMAIL'])));
            // Telefonas
            $data['PHONE'] = current(array_filter(
                    array(
                        $worker['PHONE_MOBILE'],
                        $worker['PHONE_FULL'],
                        $worker['PHONE_SHORT'], // TODO: trumpasis numeris greičiausiai tinka
                        $worker['STRUCTURE_PHONE']
                    )
            ));
            // Įrašo ir tiriančio asmens duomenys
            $data['PARTY'] = join(', ', array_filter(
                    array(
                        $data['EXAMINATOR'],
                        $worker['STRUCTURE_NAME'],
                        current(explode(',', $worker['STRUCTURE_ADDRESS']))
            )));

            $values = array(
                0 => $data['PARTY'],
                1 => $data['PHONE'],
                2 => $data['EMAIL'],
                3 => $record['CREATE_DATE'],
                4 => $worker['FIRST_NAME'],
                5 => $worker['LAST_NAME'],
                6 => $worker['POSITION'],
                7 => $data['EXAMINATOR_CABINET'],
                8 => $data['EXAMINATOR_INSTITUTION_NAME'],
                9 => $data['EXAMINATOR_UNIT'],
                10 => $data['EXAMINATOR_SUBUNIT'],
                11 => $data['EXAMINATOR_INSTITUTION_ADDRESS']
            );
            foreach ($fields as $key => &$arr) {
                $arr['VALUE'] = $values[$key];
            }
        }

        return $fields;
    }

    /**
     * Tikrina ar iki dokumento įrašo tarpinės stadijos pabaigos liko 1 diena arba mažiau.
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param string|int|null $now [optional] <b>Default: null</b><p>
     * Einamasis laikas. Timestamp, arba formatai iš "Date and Time Formats".<br/>
     * Jei įgyja NULL reikšmė - naudojama time() f-jos gražinta reikšmė.
     * </p>
     * @param array|int|null $document [optional]  <b>Default: null</b><p>
     * Dokumento ID arba DOCUMENTS lentelės duomenys.<br/>
     * Jei masyvas, tai ID elementas privalo sutapti su dokumento įraše nurodytu dokumento ID.<br/>
     * Jei įgyja NULL reikšmę, tai dokumentas gaunamas iš <i>record</i> duomenų.
     * <p>
     * @param bool|null $deadline [optional]  <b>Default: FALSE</b><p>
     * Nurodo ar tikrinti pagal galutinį terminą.<br/>
     * <b>TRUE</b> - galutinė termino pabaiga, <b>FALSE</b> - artėjanti termino pabaiga
     * <p>
     * @return boolen <b>TRUE</b> jei <i>now</i> pasiekė arba praėjo termino pabaigą, kitaip <b>FALSE</b>
     */
    public function idle_period_end($record, $now = null, $document = null, $deadline = false)
    {
        // Apdoroja $now parametrą
        if (empty($now) === TRUE) {
            $now = time();
        } else
        if (ctype_digit((string) $now) === FALSE) {
            $now = strtotime($now);
        }

        // Apdoroja $record parametrą
        /* if (is_array($record) === FALSE) {
          $params = array(
          'RECORD_ID' => $record,
          'IS_LAST' => 1);

          $record = $this->core->logic2->get_record_relations($params, true, null, 'A.*');
          } */
        $record = $this->core->logic2->get_record_relation($record);
        if (empty($record) === TRUE) {
            trigger_error('Record not found', E_USER_ERROR);
            exit;
        }

        // Apdoroja $document parametrą
        if (is_array($document) === TRUE && $document['ID'] !== $record['TABLE_TO_ID']) {
            unset($document);
        }
        if (empty($document) === TRUE) {
            $document_id = $record['TABLE_TO_ID'];
            unset($document);
        } else
        if (ctype_digit((string) $document) === TRUE) {
            $document_id = $document;
            unset($document);
        }
        if (isset($document_id) === TRUE) {
            $document = $this->core->logic2->get_document(array('ID' => $document_id,
                'CAN_IDLE' => 1), true, null, 'ID, IDLE_DAYS');
        }

        // Tikrina ar baigiasi dokumento įrašo tarpinė stadija
        if (empty($document) === FALSE) {
            if ($deadline === TRUE)
                $add_days = (int) $document['IDLE_DAYS'];
            else {
                $add_days = (int) $document['IDLE_DAYS'] - 1;
            }

            if ($add_days <= 0)
                return true;

            $limit = strtotime(' + ' . $add_days . ' days', strtotime($record['CREATE_DATE']));
            if ($limit <= $now)
                return true;
        }

        return false;
    }
}
