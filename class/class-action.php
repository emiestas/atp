<?php

namespace Atp;

class Action
{
    /**
     * @var \Atp\Core
     */
    public $core;

    /**
     *
     * @param \Atp\Core $core
     */
    public function __construct(\Atp\Core $core)
    {
        $this->core = $core;
    }

    /**
     * Dokumentų būsenų veiksmai
     * @author Justinas Malūkas
     * @return boolen|array
     */
    public function status($data, $type)
    {
        switch ($type) {
            // Būsenos forma
            case 'editstatus':
                return $this->core->manage->status_editstatus();
                break;
            // Būsenos saugojimas
            case 'savestatus':
                $param = array(
                    'LABEL' => $data['LABEL'],
                    'VALID' => 1
                );
                if (empty($data['ID']) === false) {
                    $param[0] = 'ID != ' . (int) $data['ID'];
                }
                $status = $this->core->logic2->get_status($param);
                if (count($status) > 0) {
                    $this->core->set_message('error', 'Būsena tokiu pavadinimu jau egzistuoja');
                    return false;
                }

                $create = false;
                $update = false;

                if (empty($data['ID']) === false) {
                    $update = true;

                    $qry = 'UPDATE `' . PREFIX . 'ATP_STATUS`
						SET
							`LABEL` = :LABEL
						WHERE `ID` = ' . (int) $data['ID'];
                } elseif (isset($data['PARENT_ID']) === true) {
                    $create = true;

                    $qry = 'INSERT INTO `' . PREFIX . 'ATP_STATUS`
							(`ID`, `LABEL`, `VALID`)
						VALUES
							(:ID, :LABEL, 1)';
                } else {
                    $this->core->set_message('error', 'Klaida');
                    return false;
                }
                $result = $this->core->db_query_fast($qry, $data);

                if ($create) {
                    $adminJournal = $this->core->factory->AdminJournal();
                    $adminJournal->addRecord(
                        $adminJournal->getCreateRecord(
                            'Dokumento būseną "' . $data['LABEL'] . '"'
                        )
                    );


                    $this->core->set_message('success', 'Būsena sukurta');
                } elseif ($update) {
                    $adminJournal = $this->core->factory->AdminJournal();
                    $adminJournal->addRecord(
                        $adminJournal->getEditRecord(
                            'Dokumento būseną "' . $data['LABEL'] . '"'
                        )
                    );
                    
                    $this->core->set_message('success', 'Būsena atnaujinta');
                }

                return $result;
                break;
            // Būsenos trinimas
            case 'deletestatus':
                if (empty($data)) {
                    return false;
                }

                $status = $this->core->logic2->get_status(array('ID' => $data));

                $this->core->db_query_fast('
                    UPDATE  ' . PREFIX . 'ATP_STATUS
                    SET     VALID = 0
                    WHERE   ID = ' . (int) $status['ID']);
                $this->core->db_query_fast('
                    UPDATE  ' . PREFIX . 'ATP_STATUS_X_DOCUMENT
                    SET     VALID = 0
                    WHERE   STATUS_ID = ' . (int) $status['ID']);
                

                $adminJournal = $this->core->factory->AdminJournal();
                $adminJournal->addRecord(
                    $adminJournal->getDeleteRecord(
                        'Dokumento būseną "' . $status['LABEL'] . '"'
                    )
                );

                $this->core->set_message('success', 'Būsena ištrinta');

                return $data;
                break;
            // Dokumento būsenos ryšių forma
            case 'editrelation':
                return $this->core->manage->status_editrelation();
                break;
            // Dokumento būsenos ryšių saugojimas
            case 'saverelation':
                // Atnaujinami ryšiai
                if (empty($data['RELATIONS']) === false) {
                    $rel = &$data['RELATIONS'];

                    // Trinami panaikinti ryšiai
                    if (count($rel['delete'])) {
                        foreach ($rel['delete'] as $key => $id) {
                            list($atp_status, $status) = explode('-', $id);
                            $arg = array(
                                'DOCUMENT_ID' => $data['DOCUMENT_ID'],
                                'ATP_STATUS_ID' => $atp_status,
                                'STATUS_ID' => $status
                            );
                            $qry = 'UPDATE
									`' . PREFIX . 'ATP_STATUS_X_DOCUMENT`
								SET `VALID` = 0
								WHERE
									`DOCUMENT_ID` = :DOCUMENT_ID AND
									`ATP_STATUS_ID` = :ATP_STATUS_ID AND
									`STATUS_ID` = :STATUS_ID';
                            $this->core->db_query_fast($qry, $arg);
                            unset($rel['delete'][$key]);
                        }
                    }

                    // Pridedami nauji ryšiai
                    if (count($rel['add'])) {
                        $arg = array();
                        foreach ($rel['add'] as $key => $id) {
                            list($atp_status, $status) = explode('-', $id);
                            $arg[$key]['DOCUMENT_ID'] = $data['DOCUMENT_ID'];
                            $arg[$key]['ATP_STATUS_ID'] = $atp_status;
                            $arg[$key]['STATUS_ID'] = $status;
                            // TODO: Jei norima tiksliai nurodyti, tai reikėtu tikrinti ar būsena nėra išjungta
                            $arg[$key]['VALID'] = 1;
                        }
                        // Laukų ryšiai
                        if (count($arg)) {
                            $this->core->db_ATP_insert(PREFIX . 'ATP_STATUS_X_DOCUMENT', $arg);
                        }
                    }
                }

                
                $adminJournal = $this->core->factory->AdminJournal();
                $adminJournal->addRecord(
                    $adminJournal->getSaveRecord(
                        'Dokumento "' . $this->core->factory->Document()->get($data['DOCUMENT_ID'])->getLabel() . '"'
                        . ' būsenų ryšius.'
                    )
                );

                $this->core->set_message('success', 'Dokumento būsenos ryšiai išsaugoti');

                return $result;
                break;
        }
    }

    /**
     * Teisės aktų veiksmai
     * @author Justinas Malūkas
     * @param type $data
     * @param type $type
     * @return boolen|array
     */
    public function act($data, $type)
    {
        switch ($type) {
            case 'editform':
                return $this->core->manage->act_editform(true);
                break;
            case 'save':

                // Įvesties lauke "Tekstas" reikšmė nukopijuoja į informacinį lauką "LABEL"
//                $values = unserialize($data['FIELDS_VALUES']);
                $data['LABEL'] = $values['VALUE_6'];

                if (empty($data['ID']) === false) {
                    $qry = 'UPDATE `' . PREFIX . 'ATP_ACTS`
						SET
							`NR` = :NR,
							`LABEL` = :LABEL,
							`VALID_FROM` = :VALID_FROM,
							`VALID_TILL` = :VALID_TILL,
							`CLASSIFICATOR` = :CLASSIFICATOR,
						#	`FIELDS_VALUES` = :xxFIELDS_VALUES,
							`VALUE_1` = :VALUE_1,
							`VALUE_2` = :VALUE_2,
							`VALUE_3` = :VALUE_3,
							`VALUE_4` = :VALUE_4,
							`VALUE_6` = :VALUE_6
						WHERE `ID` = ' . (int) $data['ID'];
                } elseif (isset($data['PARENT_ID']) === true) {
                    $qry = 'INSERT INTO `' . PREFIX . 'ATP_ACTS`
							(
                                `NR`, `LABEL`, `PARENT_ID`, `VALID_FROM`, `VALID_TILL`,
                                `CLASSIFICATOR`,
                            #    `xxFIELDS_VALUES`,
                                `VALUE_1`, `VALUE_2`, `VALUE_3`, `VALUE_4`, `VALUE_6`,
                                `VALID`
                            )
						VALUES
							(
                                :NR, :LABEL, :PARENT_ID, :VALID_FROM, :VALID_TILL,
                                :CLASSIFICATOR,
                            #    :xxFIELDS_VALUES,
                                :VALUE_1, :VALUE_2, :VALUE_3, :VALUE_4, :VALUE_6,
                                1
                            )';
                } else {
                    $this->core->set_message('error', 'Klaida');
                    return false;
                }
                $result = $this->core->db_query_fast($qry, $data);

                if (empty($data['ID']) === false) {
                    $adminJournal = $this->core->factory->AdminJournal();
                    $adminJournal->addRecord(
                        $adminJournal->getEditRecord(
                            'Teisės aktą "' . $data['LABEL'] . '"'
                        )
                    );


                    $this->core->set_message('success', 'Teisės aktas atnaujintas');
                } else {
                    $adminJournal = $this->core->factory->AdminJournal();
                    $adminJournal->addRecord(
                        $adminJournal->getCreateRecord(
                            'Teisės aktą "' . $data['LABEL'] . '"'
                        )
                    );

                    
                    $this->core->set_message('success', 'Teisės aktas sukurtas');
                }

                if (isset($data['PARENT_ID']) === true) {
                    return (int) $this->core->db_last_id();
                }
                return $result;
                break;
            case 'delete':
                if (empty($data)) {
                    return false;
                }

                $act = $this->logic2->get_act(array('ID' => $data));
                
                $ids = $this->core->logic->get_branch_ids($act['ID'], 'act');

                if (count($ids) === 0) {
                    return false;
                }

                //$this->core->db_query('DELETE FROM `' . PREFIX . 'ATP_ACTS` WHERE `ID` IN (' . join(',', $ids) . ')');
                $this->core->db_query_fast('
                    UPDATE  ' . PREFIX . 'ATP_ACTS
                    SET     VALID = 0
                    WHERE   ID IN (' . join(',', $ids) . ')');
                // Hardcodinti laukai
                $this->core->logic->delete_deed_extends([
                    'ITYPE' => 'ACT',
                    0 => '`ITEM_ID` IN (' . join(',', $ids) . ')']);

                $result = $this->core->db_query('
                    SELECT COUNT(`ID`) COUNT
                    FROM ' . PREFIX . 'ATP_ACTS
                    WHERE
                        ID IN (' . join(',', $ids) . ') AND
                        VALID = 1');
                $row = $this->core->db_next($result);

                if ((int) $row['COUNT'] == false) {
                    $adminJournal = $this->core->factory->AdminJournal();
                    $adminJournal->addRecord(
                        $adminJournal->getDeleteRecord(
                            'Teisės aktą "' . $act['LABEL'] . '"'
                        )
                    );


                    $this->core->set_message('success', 'Teisės aktas ištrintas');
                } else {
                    $this->core->set_message('error', 'Teisės aktas neištrintas');
                }

                return (int) $row['COUNT'] ? false : $ids;
                break;
            case 'search':
                $where = array();
                foreach ($data as $key => $val) {
                    switch ($key) {
                        case 'LABEL':
                            if (strlen($val) > 0) {
                                $where[] = 'LABEL REGEXP "' . addslashes($val) . '"';
                            }
                            break;
                    }
                }
                $where[] = '`VALID` = 1';
                if (count($where)) {
                    $where = 'WHERE ' . join(' AND ', $where);
                }

                $sql = 'SELECT * FROM ' . PREFIX . 'ATP_ACTS ' . $where . ' ORDER BY LABEL';
                $qry = $this->core->db_query($sql/* , $b_data */);

                $ret = array();
                while ($res = $this->core->db_next($qry)) {
                    $ret[] = $res;
                }

                if (empty($ret) === false) {
                    return $ret;
                }

                return false;
                break;
        }
    }

    /**
     * Paieškos laukų veiksmai
     * @author Justinas Malūkas
     * @param mixed $data Duomenys
     * @param string $type Veiksmo tipas
     * @param array $extra [optional]
     * @param string $extra[from] [optional] Ieškoti dokumentų, kurių sukūrimo
     *      data yra ne ankstesnė nei nurodyta data.
     * @param string $extra[till] [optional] Ieškoti dokumentų, kurių sukūrimo
     *      data yra ne vėlesnė nei nurodyta data.
     * @param boolean $extra[quicksearch] [optional] Default: false
     * @return boolen|array
     */
    public function searchFields($data, $type, $extra = null)
    {
        switch ($type) {
            case 'editform':
                return $this->core->manage->search_fields_editform(true);
                break;
            // Saugomi paieškos lauko duomenys
            case 'save':
                if (empty($data['USE_IN_QUICK_SEARCH'])) {
                    $data['USE_IN_QUICK_SEARCH'] = 0;
                }

                $create = false;
                $update = false;

                // Saugoma paieškos lauko informacija
                if (empty($data['ID']) === false) {
                    $update = true;

                    $qry = 'UPDATE `' . PREFIX . 'ATP_SEARCH_FIELDS`
							SET
								`NAME` = :NAME,
								`TYPE` = :TYPE,
								`USE_IN_QUICK_SEARCH` = :USE_IN_QUICK_SEARCH,
								`UPDATE_DATE` = NOW()
							WHERE `ID` = :ID';
                } else {
                    $create = true;

                    $qry = 'INSERT INTO `' . PREFIX . 'ATP_SEARCH_FIELDS`
							(`NAME`, `TYPE`, `USE_IN_QUICK_SEARCH`, `VALID`, `CREATE_DATE`)
						VALUES
							(:NAME, :TYPE, :USE_IN_QUICK_SEARCH, 1, NOW())';
                }
                $result = $this->core->db_query_fast($qry, $data);

                if (empty($data['ID']) === true) {
                    $data['ID'] = (int) $this->core->db_last_id();
                }

                // Atnaujinami pririšti laukai
                if (empty($data['FIELDS']) === false) {
                    // Informaciniu laukų tipai
                    $itypes = array(
                        'SI' => 'CLAUSE',
                        'AI' => 'ACT'
                    );

                    // Trinami panaikinti laukai
                    if (count($data['FIELDS']['delete'])) {
                        foreach ($data['FIELDS']['delete'] as $key => $id) {
                            $type = substr($id, 0, 2);
                            if (isset($itypes[$type])) {
                                $arg = array(
                                    'ITYPE' => $itypes[$type],
                                    'SEARCH_FIELD_ID' => $data['ID'],
                                    'FIELD_ID' => substr($id, 2)
                                );
                                $qry = 'DELETE FROM `' . PREFIX . 'ATP_SEARCH_FIELDS_X_IFIELDS`
										WHERE
                                            `SEARCH_FIELD_ID` = :SEARCH_FIELD_ID AND
                                            `FIELD_ID` = :FIELD_ID AND
                                            `ITYPE` = :ITYPE';
                                $this->core->db_query_fast($qry, $arg);
                                unset($data['FIELDS']['delete'][$key]);
                            }
                        }
                        // Laukų ryšiai
                        if (count($data['FIELDS']['delete'])) {
                            $qry = 'DELETE FROM `' . PREFIX . 'ATP_SEARCH_FIELDS_X_FIELDS`
									WHERE
                                        `SEARCH_FIELD_ID` = :ID AND
                                        `FIELD_ID` IN(' . join(',', $data['FIELDS']['delete']) . ')';
                            $this->core->db_query_fast($qry, $data);
                        }
                    }
                    // Pridedami nauji laukai
                    if (count($data['FIELDS']['add'])) {
                        $arg = array();
                        $info_arg = array();
                        foreach ($data['FIELDS']['add'] as $key => $id) {
                            $type = substr($id, 0, 2);
                            if (isset($itypes[$type])) {
                                $info_arg[$key]['ITYPE'] = $itypes[$type];
                                $info_arg[$key]['SEARCH_FIELD_ID'] = $data['ID'];
                                $info_arg[$key]['FIELD_ID'] = substr($id, 2);
                            } else {
                                $arg[$key]['SEARCH_FIELD_ID'] = $data['ID'];
                                $arg[$key]['FIELD_ID'] = $id;
                            }
                        }
                        // Laukų ryšiai
                        if (count($arg)) {
                            $this->core->db_ATP_insert(PREFIX . 'ATP_SEARCH_FIELDS_X_FIELDS', $arg);
                        }
                        // Informacinių laukų ryšiai
                        if (count($info_arg)) {
                            $this->core->db_ATP_insert(PREFIX . 'ATP_SEARCH_FIELDS_X_IFIELDS', $info_arg);
                        }
                    }
                }

                if ($create) {
                    $adminJournal = $this->core->factory->AdminJournal();
                    $adminJournal->addRecord(
                        $adminJournal->getCreateRecord(
                            'Paieškos laukas "' . $data['NAME'] . '"'
                        )
                    );
                    
                    
                    $this->core->set_message('success', 'Paieškos laukas sukurtas');
                } elseif ($update) {
                    $adminJournal = $this->core->factory->AdminJournal();
                    $adminJournal->addRecord(
                        $adminJournal->getEditRecord(
                            'Paieškos laukas "' . $data['NAME'] . '"'
                        )
                    );

                    
                    $this->core->set_message('success', 'Paieškos laukas atnaujintas');
                }


                return $result;
                break;
            case 'delete':
                if (empty($data)) {
                    return false;
                }

                $fieldData = $this->core->logic2->get_search_field(array('ID' => $data));

                $this->core->db_query_fast('
                    UPDATE  ' . PREFIX . 'ATP_SEARCH_FIELDS
                    SET     VALID = 0
                    WHERE   ID = ' . (int) $fieldData['ID']);
                $result = $this->core->db_query('SELECT COUNT(`ID`) COUNT
						FROM `' . PREFIX . 'ATP_ACTS`
						WHERE
							`ID` = ' . (int) $fieldData['ID'] . ' AND
							`VALID` = 1');
                $row = $this->core->db_next($result);

                if ((int) $row['COUNT'] == false) {
                    $adminJournal = $this->core->factory->AdminJournal();
                    $adminJournal->addRecord(
                        $adminJournal->getDeleteRecord(
                            'Paieškos laukas "' . $fieldData['NAME'] . '"'
                        )
                    );
                    

                    $this->core->set_message('success', 'Teisės aktas ištrintas');
                } else {
                    $this->core->set_message('error', 'Teisės aktas neištrintas');
                }

                return (int) $row['COUNT'] ? false : $fieldData['ID'];
                break;
            case 'save_report':
                if (empty($data) === false) {
                    $res = $this->core->db_ATP_insert(PREFIX . 'ATP_USER_REPORTS', array(
                        'NAME' => $data,
                        'PEOPLE_ID' => $this->core->factory->User()->getLoggedUser()->person->id,
                        'DEPARTMENT_ID' => $this->core->factory->User()->getLoggedPersonDuty()->department->id,
                        'PARAMETERS' => serialize($data['field']),
                    ));

                    if ($res > 0) {
                        $adminJournal = $this->core->factory->AdminJournal();
                        $adminJournal->addRecord(
                            $adminJournal->getSaveRecord(
                                'Ataskaitą "' . $data . '"'
                            )
                        );
                    }

                    return $res;
                }
                return false;
                break;
            case 'search':
                /** using $extra */

                // Salyga ataskaitos saugojimui. Nėra puslapio, $data['field']
                // ne masyvas (t.y. ne greitoji paieška) ir $data['field']['as_report']
                // (ataskaitos pavadinimas) ne tuščias
                if (isset($_GET['_page']) === false && empty($data['field']['as_report']) === false) {
                    $new_id = $this->searchFields($data['field']['as_report'], 'save_report');
                    unset($data['field']['as_report']);
                }

                if (empty($data['field'])) {
                    break;
                }

                $qry = array();
                $fields = array();
                $ifields = array();
                $search_params = array();
                // Greitoji paieška ieško OR jungtimi + ieškoma dokumento numerio
                $quickSearch = false;

                // Get searchable fields
                $criteria = ['VALID' => 1];
                if (isset($extra['quicksearch']) && $extra['quicksearch'] === true) {
                    $criteria['USE_IN_QUICK_SEARCH'] = 1;
                }

                $searchFields = $this->core->logic2->get_search_field($criteria, null, null, 'ID, NAME');
                if (is_array($data['field']) === false) {
                    $searchValue = $data['field'];
                    unset($data['field']);
                    foreach ($searchFields as $field_id => &$field) {
                        $data['field'][$field_id] = $searchValue;
                    }

                    $qry[] = '(SELECT NULL VALUE, NULL TYPE, RECORD_ID, NULL PARAM, CREATE_DATE'
                        . ' FROM `' . PREFIX . 'ATP_RECORD`'
                        . ' WHERE `RECORD_ID` LIKE "%' . $this->core->db_escape($searchValue) . '%" AND '
                        . ' IS_LAST = 1)';
                    $quickSearch = true;
                }

                if (empty($data['field']) === false) {
                    foreach ($data['field'] as $fieldId => &$field) {
                        if (empty($searchFields[$fieldId]) || empty($field)) {
                            unset($data['field'][$fieldId]);
                            continue;
                        }

                        $fields[$fieldId] = $this->core->logic2->get_search_field_form_fields([
                            'SEARCH_FIELD_ID' => $fieldId
                        ]);
                        foreach ($fields[$fieldId] as $sfield) {
                            $search_params[$fieldId][] = $sfield['FIELD_ID'];
                        }

                        // TODO
                        $ifields[$fieldId] = $this->core->logic2->get_search_field_form_ifields([
                            'SEARCH_FIELD_ID' => $fieldId
                        ]);
                        foreach ($ifields[$fieldId] as $sfield) {
                            $isearch_params[$fieldId][$sfield['ITYPE']][] = $sfield['FIELD_ID'];
                        }
                    }
                }
                unset($searchFields);

                if (empty($search_params) === false || empty($isearch_params) === false) {
                    //$arg = array();
                    if (empty($search_params) === false) {
                        // Surenka laukų informaciją, pagal kuriuos yra vykdoma paieška
                        //$all_fields = $this->core->logic2->get_fields();
                        $searchFields_ids = array();
                        foreach ($search_params as $key => &$params) {
                            foreach ($params as &$param) {
                                $searchFields_ids[] = $param;
                            }
                        }
                        $searchFields = $this->core->logic2->get_fields([
                            'ID IN(' . join(',', (array) $searchFields_ids) . ')'
                        ]);

                        foreach ($search_params as $key => &$params) {
                            foreach ($params as &$param) {
                                //$field = &$all_fields[$param];
                                $field = &$searchFields[$param];

                                if (empty($field) === true) {
                                    unset($param);
                                    continue;
                                }

                                $value = $data['field'][$key];

                                // TODO: Jei į lauką įvedamas klasifikatorius,
                                // reikia, kad paieška vyktu tarp klasifikatorių
                                // pavadinimų, o ne ID
                                if ($field['ITYPE'] === 'DOC') {
                                    $qry[] = '( SELECT
                                            `' . $field['NAME'] . '` VALUE,
                                            "DOC" TYPE,
                                            RECORD_ID, "' . $key . '" PARAM,
                                            CREATE_DATE
                                        FROM `' . PREFIX . 'ATP_TBL_' . $field['ITEM_ID'] . '`
                                        WHERE
                                            `' . $field['NAME'] . '` LIKE "%' . $this->core->db_escape($value) . '%"
                                            AND RECORD_LAST = 1 )';
                                } elseif ($field['ITYPE'] === 'WS') {
                                    //$parent_field = $all_fields[$field['ITEM_ID']];
                                    $parent_field = $this->core->logic2->get_fields(array(
                                        'ID' => (int) $field['ITEM_ID']));
                                    if (empty($parent_field) === false) {
                                        if ($parent_field['ITYPE'] === 'DOC') {
                                            $qry[] = '(
                                                SELECT
                                                    A.VALUE VALUE,
                                                    "DOC" TYPE,
                                                    B.RECORD_ID,
                                                    "' . $key . '" PARAM,
                                                    B.CREATE_DATE
                                                FROM
                                                    ' . PREFIX . 'ATP_FIELDS_VALUES A
                                                        INNER JOIN '.PREFIX .'ATP_RECORD_X_RELATION C ON
                                                            C.RELATION_ID = A.RECORD_ID
                                                        INNER JOIN '.PREFIX.'ATP_TBL_' . $parent_field['ITEM_ID'] . ' B ON
                                                #            B.ID = A.RECORD_ID AND
                                                #            B.RECORD_LAST = 1
                                                            B.ID = C.RECORD_ID
                                                WHERE
                                                #    A.TABLE_ID = ' . $parent_field['ITEM_ID'] . ' AND
                                                    C.DOCUMENT_ID = ' . $parent_field['ITEM_ID'] . ' AND
                                                    A.FIELD_ID = ' . $field['ID'] . ' AND
                                                    A.VALUE LIKE "%' . $this->core->db_escape($value) . '%"
                                            )';
                                        }
                                    }

                                } elseif (in_array($field['ITYPE'], ['CLASS', 'ACT','CLAUSE'])) {
                                    $q = 'SELECT
                                                B.DOCUMENT_ID,
                                                A.VALUE,
                                                B.RECORD_ID
											FROM
                                                ' . PREFIX . 'ATP_FIELDS_VALUES A
                                                    INNER JOIN ' .PREFIX . 'ATP_RECORD_X_RELATION B ON
                                                        B.RELATION_ID = A.RECORD_ID
											WHERE
												A.FIELD_ID = ' . $field['ID'] . ' AND
												A.VALUE LIKE "%' . $this->core->db_escape($value) . '%"';
                                    $r = $this->core->db_query_fast($q);
                                    $q2 = array();
                                    while ($row = $this->core->db_next($r)) {
                                        // RECORD_ID ir CREATE_DATE negali buti NULL
                                        $q2[] = '(SELECT
                                                    COUNT(*) CNT,
                                                    "' . $row['DOCUMENT_ID'] . '" DOC,
                                                    COALESCE(RECORD_ID ,"") "RECORD_ID",
                                                    "' . $this->core->db_escape($row['VALUE']) . '" VALUE,
                                                    COALESCE(CREATE_DATE, "1970-01-01") "CREATE_DATE"
												FROM ' . PREFIX . 'ATP_TBL_' . $row['DOCUMENT_ID'] . '
												WHERE
													ID = ' . $row['RECORD_ID'] . ' AND
													RECORD_LAST = 1)';
                                    }
                                    if (count($q2)) {
                                        $q2 = 'SELECT * FROM (' . join(' UNION ALL ', $q2) . ') t
												WHERE CNT > 0
												GROUP BY RECORD_ID';
                                        $r2 = $this->core->db_query_fast($q2);
                                        while ($row = $this->core->db_next($r2)) {
                                            $qry[] = '(SELECT
												"' . $this->core->db_escape($row['VALUE']) . '" VALUE,
												"DOC" TYPE,
												"' . $row['RECORD_ID'] . '" RECORD_ID,
												"' . $key . '" PARAM,
												"' . $row['CREATE_DATE'] . '" CREATE_DATE)';
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (empty($isearch_params) === false) {
                        foreach ($isearch_params as $key => &$arr) {
                            $value = $data['field'][$key];
                            foreach ($arr as $type => &$params) {
                                foreach ($params as &$param) {
                                    if ($type === 'CLAUSE') {
                                        // Laukas pagal kurį ieška straipsnių
                                        $ifields = $this->core->logic->get_info_fields(array(
                                            'TYPE' => $type), true);
                                        $col_name = $ifields['column'][$param];

                                        // Pagal paieškos parametrą rasti straipsniai
                                        $clauses = $this->core->logic2->get_clause(array(
                                            0 => '`' . $col_name . '` LIKE "%' . $this->core->db_escape($value) . '%"'
                                        ));
                                        // Jei nerasta tarp straipsnių, su šiuo parametru nebėra ką veikt
                                        if (count($clauses) === 0) {
                                            continue;
                                        }

                                        $clauses_ids = array();
                                        foreach ($clauses as $id => $clause) {
                                            $clauses_ids[] = $clause['ID'];
                                        }

                                        /*
                                         * Paieška laukuose, į kurios įrašomas straipsnis
                                         */
                                        // Gaunami straipsnio laukai
                                        $fields = $this->core->logic2->get_fields(array(
                                            'SORT' => '6'));
                                        // Ieškoma ar straipsnio lauke yra nurodytas bent vienas iš rastų straipsnių
                                        if (count($fields)) {
                                            // Atskiriami laukų ID pagal dokumento tipą
                                            $fields_ids = array();
                                            foreach ($fields as $id => $field) {
                                                if ($field['ITYPE'] === 'DOC') {
                                                    $fields_ids[$field['ITYPE']][] = $field['ID'];
                                                } else {
                                                    $fields_ids['EXTRA'][] = $field['ID'];
                                                }
                                            }

                                            // Ieškoma straipsnio dokumento laukuose
                                            if (count($fields_ids['DOC'])) {
                                                foreach ($fields_ids['DOC'] as $field) {
                                                    $field = $fields[$field];
                                                    $qry[] = '(
                                            SELECT
                                                `' . $field['NAME'] . '` VALUE,
                                                "DOC" TYPE,
                                                RECORD_ID, "' . $key . '" PARAM,
                                                CREATE_DATE
                                            FROM `' . PREFIX . 'ATP_TBL_' . $field['ITEM_ID'] . '`
                                            WHERE
                                                `' . $field['NAME'] . '` IN (' . join(',', $clauses_ids) . ') AND
                                                RECORD_LAST = 1
                                        )';
                                                }
                                            }
                                            // Ieškoma straipsnio papildomuose laukuose
                                            if (count($fields_ids['EXTRA'])) {
                                                $q = 'SELECT
                                                            B.DOCUMENT_ID,
                                                            A.VALUE,
                                                            B.RECORD_ID
														FROM
                                                            ' . PREFIX . 'ATP_FIELDS_VALUES A
                                                                INNER JOIN ' .PREFIX . 'ATP_RECORD_X_RELATION B ON
                                                                    B.RELATION_ID = A.RECORD_ID
														WHERE
															A.FIELD_ID = ' . $field['ID'] . ' AND
															A.VALUE IN (' . join(',', $clauses_ids) . ')';
                                                $r = $this->core->db_query_fast($q);
                                                $q2 = array();
                                                while ($row = $this->core->db_next($r)) {
                                                    $q2[] = '(SELECT
                                                                COUNT(*) CNT,
                                                                "' . $row['DOCUMENT_ID'] . '" DOC,
                                                                RECORD_ID,
                                                                "' . $this->core->db_escape($row['VALUE']) . '" VALUE,
                                                                CREATE_DATE
															FROM ' . PREFIX . 'ATP_TBL_' . $row['DOCUMENT_ID'] . '
															WHERE
																ID = ' . $row['RECORD_ID'] . ' AND
																RECORD_LAST = 1)';
                                                }
                                                if (count($q2)) {
                                                    $q2 = 'SELECT * FROM (' . join(' UNION ALL ', $q2) . ') t
															WHERE CNT > 0
															GROUP BY RECORD_ID';
                                                    $r2 = $this->core->db_query_fast($q2);
                                                    while ($row = $this->core->db_next($r2)) {
                                                        $qry[] = '(SELECT
															"' . $this->core->db_escape($row['VALUE']) . '" VALUE,
															"DOC" TYPE,
															"' . $row['RECORD_ID'] . '" RECORD_ID,
															"' . $key . '" PARAM,
															"' . $row['CREATE_DATE'] . '" CREATE_DATE)';
                                                    }
                                                }
                                            }
                                        }
                                        /*
                                         * Paieška veikose
                                         */
                                        if (1 === 1) {
                                            // Veikos naudojančio straipsnį
                                            $extends = $this->core->logic->get_deed_extend_data(array(
                                                0 => 'ITEM_ID IN(' . join(',', $clauses_ids) . ')',
                                                'ITYPE' => 'CLAUSE'));
                                            if (count($extends) === 0) {
                                                continue;
                                            }

                                            $deeds_ids = array();
                                            foreach ($extends as $extend) {
                                                $deeds_ids[$extend['DEED_ID']] = $extend['DEED_ID'];
                                            }
                                            $deeds_ids = array_values($deeds_ids);

                                            /*
                                             * Paieška laukuose, į kurios įrašoma veika
                                             */
                                            // Gaunami veikos laukai
                                            $fields = $this->core->logic2->get_fields(array(
                                                'SORT' => '4', 'TYPE' => '23'));
                                            // Ieškoma ar straipsnio lauke yra nurodytas
                                            // bent vienas iš rastų straipsnių
                                            if (count($fields)) {
                                                // Atskiriami laukų ID pagal dokumento tipą
                                                $fields_ids = array();
                                                foreach ($fields as $id => $field) {
                                                    if ($field['ITYPE'] === 'DOC') {
                                                        $fields_ids[$field['ITYPE']][] = $field['ID'];
                                                    } else {
                                                        $fields_ids['EXTRA'][] = $field['ID'];
                                                    }
                                                }

                                                // Ieškoma straipsnio dokumento laukuose
                                                if (count($fields_ids['DOC'])) {
                                                    foreach ($fields_ids['DOC'] as $field) {
                                                        $field = $fields[$field];
                                                        $qry[] = '(
                                                SELECT
                                                    `' . $field['NAME'] . '` VALUE,
                                                    "DOC" TYPE,
                                                    RECORD_ID,
                                                    "' . $key . '" PARAM,
                                                    CREATE_DATE
                                                FROM `' . PREFIX . 'ATP_TBL_' . $field['ITEM_ID'] . '`
                                                WHERE
                                                    `' . $field['NAME'] . '` IN (' . join(',', $deeds_ids) . ') AND
                                                    RECORD_LAST = 1
                                            )';
                                                    }
                                                }
                                                // Ieškoma straipsnio papildomuose laukuose
                                                if (count($fields_ids['EXTRA'])) {
                                                    $q = 'SELECT
                                                            B.DOCUMENT_ID,
                                                            A.VALUE,
                                                            B.RECORD_ID
														FROM
                                                            ' . PREFIX . 'ATP_FIELDS_VALUES A
                                                                INNER JOIN ' .PREFIX . 'ATP_RECORD_X_RELATION B ON
                                                                    B.RELATION_ID = A.RECORD_ID
														WHERE
															A.FIELD_ID = ' . $field['ID'] . ' AND
															A.VALUE IN (' . join(',', $deeds_ids) . ')';
                                                    $r = $this->core->db_query_fast($q);
                                                    $q2 = array();
                                                    while ($row = $this->core->db_next($r)) {
                                                        $q2[] = '(SELECT
                                                                COUNT(*) CNT,
                                                                "' . $row['DOCUMENT_ID'] . '" DOC,
                                                                RECORD_ID,
                                                                "' . $this->core->db_escape($row['VALUE']) . '" VALUE,
                                                                CREATE_DATE
															FROM ' . PREFIX . 'ATP_TBL_' . $row['DOCUMENT_ID'] . '
															WHERE
																ID = ' . $row['RECORD_ID'] . ' AND
																RECORD_LAST = 1)';
                                                    }
                                                    if (count($q2)) {
                                                        $q2 = 'SELECT * FROM (' . join(' UNION ALL ', $q2) . ') t
															WHERE CNT > 0
															GROUP BY RECORD_ID';
                                                        $r2 = $this->core->db_query_fast($q2);
                                                        while ($row = $this->core->db_next($r2)) {
                                                            $qry[] = '(SELECT
															"' . $this->core->db_escape($row['VALUE']) . '" VALUE,
															"DOC" TYPE,
															"' . $row['RECORD_ID'] . '" RECORD_ID,
															"' . $key . '" PARAM,
															"' . $row['CREATE_DATE'] . '" CREATE_DATE)';
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } elseif ($type === 'ACT') {
                                        // Laukas pagal kurį ieška straipsnių
                                        $ifields = $this->core->logic->get_info_fields(array(
                                            'TYPE' => $type), true);
                                        $col_name = $ifields['column'][$param];

                                        // Pagal paieškos parametrą rasti aktai
                                        $acts = $this->core->logic2->get_act([
                                            0 => '`' . $col_name . '` LIKE "%' . $this->core->db_escape($value) . '%"'
                                        ]);
                                        // Jei nerasta tarp straipsnių, su šiuo parametru nebėra ką veikt
                                        if (count($acts) === 0) {
                                            continue;
                                        }

                                        $acts_ids = array();
                                        foreach ($acts as $id => $act) {
                                            $acts_ids[] = $act['ID'];
                                        }

                                        /*
                                         * Paieška laukuose, į kurios įrašomas teisės aktas
                                         */
                                        // Nors teisės akto lauko sukurymas yra,
                                        // bet nebuvo numatyta, kad toks laukas gali būti kuriamas.
                                        // Gaunami akto laukai
                                        $fields = $this->core->logic2->get_fields(array(
                                            'SORT' => '4', 'TYPE' => '22'));
                                        // Ieškoma ar akto lauke yra nurodytas bent vienas iš rastų aktų
                                        if (count($fields)) {
                                            // Atskiriami laukų ID pagal dokumento tipą
                                            $fields_ids = array();
                                            foreach ($fields as $id => $field) {
                                                if ($field['ITYPE'] === 'DOC') {
                                                    $fields_ids[$field['ITYPE']][] = $field['ID'];
                                                } else {
                                                    $fields_ids['EXTRA'][] = $field['ID'];
                                                }
                                            }
                                            // Ieškoma akto dokumento laukuose
                                            if (count($fields_ids['DOC'])) {
                                                foreach ($fields_ids['DOC'] as $field) {
                                                    $field = $fields[$field];
                                                    $qry[] = '( SELECT
                                                        `' . $field['NAME'] . '` VALUE,
                                                        "DOC" TYPE,
                                                        RECORD_ID,
                                                        "' . $key . '" PARAM,
                                                        CREATE_DATE
													FROM `' . PREFIX . 'ATP_TBL_' . $field['ITEM_ID'] . '`
													WHERE
														`' . $field['NAME'] . '` IN (' . join(',', $acts_ids) . ') AND
														RECORD_LAST = 1 )';
                                                }
                                            }
                                            // Ieškoma akto papildomuose laukuose
                                            if (count($fields_ids['EXTRA'])) {
                                                $q = 'SELECT
                                                            B.DOCUMENT_ID,
                                                            A.VALUE,
                                                            B.RECORD_ID
														FROM
                                                            ' . PREFIX . 'ATP_FIELDS_VALUES A
                                                                INNER JOIN ' .PREFIX . 'ATP_RECORD_X_RELATION B ON
                                                                    B.RELATION_ID = A.RECORD_ID
														WHERE
															A.FIELD_ID = ' . $field['ID'] . ' AND
															A.VALUE IN (' . join(',', $acts_ids) . ')';
                                                $r = $this->core->db_query_fast($q);
                                                $q2 = array();
                                                while ($row = $this->core->db_next($r)) {
                                                    $q2[] = '(SELECT
                                                                COUNT(*) CNT,
                                                                "' . $row['DOCUMENT_ID'] . '" DOC,
                                                                RECORD_ID,
                                                                "' . $this->core->db_escape($row['VALUE']) . '" VALUE,
                                                                CREATE_DATE
															FROM ' . PREFIX . 'ATP_TBL_' . $row['DOCUMENT_ID'] . '
															WHERE
																ID = ' . $row['RECORD_ID'] . ' AND
																RECORD_LAST = 1)';
                                                }
                                                if (count($q2)) {
                                                    $q2 = 'SELECT * FROM (' . join(' UNION ALL ', $q2) . ') t
															WHERE CNT > 0
															GROUP BY RECORD_ID';
                                                    $r2 = $this->core->db_query_fast($q2);
                                                    while ($row = $this->core->db_next($r2)) {
                                                        $qry[] = '(SELECT
															"' . $this->core->db_escape($row['VALUE']) . '" VALUE,
															"DOC" TYPE,
															"' . $row['RECORD_ID'] . '" RECORD_ID,
															"' . $key . '" PARAM,
															"' . $row['CREATE_DATE'] . '" CREATE_DATE)';
                                                    }
                                                }
                                            }
                                        }
                                        /*
                                         * Paieška veikose
                                         */
                                        if (1 === 1) {
                                            // Veikos naudojančio ieškomus teisės aktus
                                            $extends = $this->core->logic->get_deed_extend_data(array(
                                                0 => 'ITEM_ID IN(' . join(',', $acts_ids) . ')',
                                                'ITYPE' => 'ACT'));
                                            if (count($extends) === 0) {
                                                continue;
                                            }

                                            $deeds_ids = array();
                                            foreach ($extends as $extend) {
                                                $deeds_ids[$extend['DEED_ID']] = $extend['DEED_ID'];
                                            }
                                            $deeds_ids = array_values($deeds_ids);

                                            /*
                                             * Paieška laukuose, į kurios įrašoma veika
                                             */
                                            // Gaunami veikos laukai
                                            $fields = $this->core->logic2->get_fields(array(
                                                'SORT' => '4', 'TYPE' => '23'));
                                            // Ieškoma ar straipsnio lauke yra
                                            // nurodytas bent vienas iš rastų straipsnių
                                            if (count($fields)) {
                                                // Atskiriami laukų ID pagal dokumento tipą
                                                $fields_ids = array();
                                                foreach ($fields as $id => $field) {
                                                    if ($field['ITYPE'] === 'DOC') {
                                                        $fields_ids[$field['ITYPE']][] = $field['ID'];
                                                    } else {
                                                        $fields_ids['EXTRA'][] = $field['ID'];
                                                    }
                                                }

                                                // Ieškoma straipsnio dokumento laukuose
                                                if (count($fields_ids['DOC'])) {
                                                    foreach ($fields_ids['DOC'] as $field) {
                                                        $field = $fields[$field];
                                                        $qry[] = '( SELECT
                                                            `' . $field['NAME'] . '` VALUE,
                                                            "DOC" TYPE,
                                                            RECORD_ID,
                                                            "' . $key . '" PARAM,
                                                            CREATE_DATE
													FROM `' . PREFIX . 'ATP_TBL_' . $field['ITEM_ID'] . '`
													WHERE
														`' . $field['NAME'] . '` IN (' . join(',', $deeds_ids) . ') AND
														RECORD_LAST = 1 )';
                                                    }
                                                }
                                                // Ieškoma straipsnio papildomuose laukuose
                                                if (count($fields_ids['EXTRA'])) {
                                                    $q = 'SELECT
                                                            B.DOCUMENT_ID,
                                                            A.VALUE,
                                                            B.RECORD_ID
                                                        FROM
                                                            ' . PREFIX . 'ATP_FIELDS_VALUES A
                                                                INNER JOIN ' .PREFIX . 'ATP_RECORD_X_RELATION B ON
                                                                    B.RELATION_ID = A.RECORD_ID
														WHERE
															A.FIELD_ID = ' . $field['ID'] . ' AND
															A.VALUE IN (' . join(',', $deeds_ids) . ')';
                                                    $r = $this->core->db_query_fast($q);
                                                    $q2 = array();
                                                    while ($row = $this->core->db_next($r)) {
                                                        $q2[] = '(SELECT
                                                                COUNT(*) CNT,
                                                                "' . $row['DOCUMENT_ID'] . '" DOC,
                                                                RECORD_ID,
                                                                "' . $this->core->db_escape($row['VALUE']) . '" VALUE,
                                                                CREATE_DATE
															FROM ' . PREFIX . 'ATP_TBL_' . $row['DOCUMENT_ID'] . '
															WHERE
																ID = ' . $row['RECORD_ID'] . ' AND
																RECORD_LAST = 1)';
                                                    }
                                                    if (count($q2)) {
                                                        $q2 = 'SELECT * FROM (' . join(' UNION ALL ', $q2) . ') t
															WHERE CNT > 0
															GROUP BY RECORD_ID';
                                                        $r2 = $this->core->db_query_fast($q2);
                                                        while ($row = $this->core->db_next($r2)) {
                                                            $qry[] = '(SELECT
															"' . $this->core->db_escape($row['VALUE']) . '" VALUE,
															"DOC" TYPE,
															"' . $row['RECORD_ID'] . '" RECORD_ID,
															"' . $key . '" PARAM,
															"' . $row['CREATE_DATE'] . '" CREATE_DATE)';
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $res = array();
                if (count($qry)) {
                    $where = array();
                    if (empty($extra['from']) === false) {
                        $where[] = ' CREATE_DATE >= DATE("' . $extra['from'] . '")';
                    }
                    if (empty($extra['till']) === false) {
                        $where[] = ' CREATE_DATE < DATE("' . $extra['till'] . '")';
                    }
                    if (count($where)) {
                        $where = ' WHERE ' . JOIN(' AND ', $where);
                    } else {
                        $where = '';
                    }

                    $group = 'GROUP BY RECORD_ID, PARAM';
                    $having = 'HAVING COUNT(RECORD_ID) = ' . count($data['field']) . '';
                    if ($quickSearch === true) {
                        $group = '';
                        $having = '';
                    }

                    $sql = 'SELECT RECORD_ID/*, COUNT(RECORD_ID)*/
                            FROM (
                                SELECT *
                                FROM (' . join(' UNION ALL ', $qry) . ') t
                                ' . $group . '
                            ) t2
                            ' . $where . '
                            GROUP BY RECORD_ID
                            ' . $having . '
                            ORDER BY CREATE_DATE DESC';
                    $result = $this->core->db_query_fast($sql);
                    while ($row = $this->core->db_next($result)) {
                        $res[] = $row['RECORD_ID'];
                    }
                }
                return $res;
                break;
        }
    }

    public function clause()
    {
        $codex = (!empty($_POST['codex']) ? $_POST['codex'] : '');
        $clause = (!empty($_POST['clause']) ? $_POST['clause'] : '');
        $clause_parent = (!empty($_POST['parent_id']) ? $_POST['parent_id'] : 0);
        $clause_section = (!empty($_POST['clause_section']) ? $_POST['clause_section'] : '');
        $clause_paragraph = (!empty($_POST['clause_paragraph']) ? $_POST['clause_paragraph'] : '');
        $clause_subparagraph = (!empty($_POST['clause_subparagraph']) ? $_POST['clause_subparagraph'] : '');
        $clause_label = (!empty($_POST['clause_label']) ? $_POST['clause_label'] : '');
        $valid_from = (!empty($_POST['valid_from']) ? $_POST['valid_from'] : null);
        $valid_till = (!empty($_POST['valid_till']) ? $_POST['valid_till'] : null);
        $clause_notice = (!empty($_POST['clause_notice']) ? 1 : 0);
        $clause_notice_label = (!empty($_POST['clause_notice_label']) ? $_POST['clause_notice_label'] : '');
        $clause_penalty_min = (!empty($_POST['clause_penalty_min']) ? $_POST['clause_penalty_min'] : 0);
        $clause_penalty_max = (!empty($_POST['clause_penalty_max']) ? $_POST['clause_penalty_max'] : 0);
        $clause_penalty_first = (!empty($_POST['clause_penalty_first']) ? $_POST['clause_penalty_first'] : 0);
        $clause_term = (!empty($_POST['clause_term']) ? $_POST['clause_term'] : 0);
//        $classificatorsIds = (!empty($_POST['clause_class_type']) ? $_POST['clause_class_type'] : 0);
        $classificatorsIds = array_filter(
            filter_input(INPUT_POST, 'clause_class_type', FILTER_VALIDATE_INT, FILTER_REQUIRE_ARRAY)
        );
        $clause_group = (!empty($_POST['clause_group']) ? $_POST['clause_group'] : 0);
        $field = (!empty($_POST['field']) ? $_POST['field'] : array());



        if (!empty($clause)) {
            $type = 0;
            $where = '';
            $where_chk = '';
            $arg = array();
            $arg_chk = array();
            if (!empty($clause)) {
                $type = 1;
                $type_chk = 1;
                $where .= " AND CLAUSE = :CLAUSE";
                $where_chk .= " AND CLAUSE = :CLAUSE";
                $arg['CLAUSE'] = $clause;
                $arg_chk['CLAUSE'] = $clause;
            }
            if (!empty($clause_section)) {
                $type = 2;
                $type_chk = 1;
                $where .= " AND SECTION = :SECTION";
                $arg['SECTION'] = $clause_section;
                $arg_chk['CLAUSE'] = $clause;
            }
            if (!empty($clause_paragraph)) {
                $type = 3;
                $type_chk = 2;
                $where .= " AND PARAGRAPH = :PARAGRAPH";
                $where_chk .= " AND SECTION = :SECTION";
                $arg['PARAGRAPH'] = $clause_paragraph;
                $arg_chk['SECTION'] = $clause_section;
            }
            if (!empty($clause_subparagraph)) {
                $type = 4;
                $type_chk = 3;
                $where .= " AND SUBPARAGRAPH = :SUBPARAGRAPH";
                $where_chk .= " AND PARAGRAPH = :PARAGRAPH";
                $arg['SUBPARAGRAPH'] = $clause_subparagraph;
                $arg_chk['PARAGRAPH'] = $clause_paragraph;
            }

            $qry_chk = 'SELECT *
                FROM ' . PREFIX . 'ATP_CLAUSES
                WHERE TYPE = ' . (int) $type_chk . ' ' . $where_chk . '
                LIMIT 1';
            $result_chk = $this->core->db_query_fast($qry_chk, $arg_chk);


            $name = $clause . $this->core->_prefix->cl;
            if ($type === 1) {
            } elseif ($type === 2) {
                $name .= ' ' . $clause_section . $this->core->_prefix->se;
            } elseif ($type === 3) {
                $name .= ' ' . $clause_section . $this->core->_prefix->se
                    . ' ' . $clause_paragraph . $this->core->_prefix->pa;
            } elseif ($type == 4) {
                $name .= ' ' . $clause_section . $this->core->_prefix->se
                    . ' ' . $clause_paragraph . ' ' . $this->core->_prefix->pa
                    . $clause_subparagraph . $this->core->_prefix->su;
            } else {
                $name = '';
            }

            if ($this->core->db_select_count($result_chk) != 0 || $type == 1) {
            } else {
                if ($type == 1) {
                    $this->core->set_message(
                        'error',
                        'Straipsnis "' . $clause . ' str." jau yra sukurtas.'
                    );
                } else {
                    $this->core->set_message(
                        'error',
                        'Straipsnis "' . $name . '" nesukurtas, nes nėra sukurto tėvinio straipsnio.'
                    );
                }

                header('Location: ' . $_SERVER['HTTP_REFERER']);
                exit;
            }

            $qry = 'SELECT * FROM ' . PREFIX . 'ATP_CLAUSES WHERE TYPE = ' . (int) $type . ' ' . $where . ' LIMIT 1';
            $result = $this->core->db_query_fast($qry, $arg);
            $data = $this->core->db_next($result);

            if (empty($_POST['parent_id']) === false) {
                $values['PARENT_ID'] = (int) $_POST['parent_id'];
            }
            $values['TYPE'] = $type;
            $values['CODEX'] = $codex;
            $values['CLAUSE'] = $clause;
            $values['SECTION'] = $clause_section;
            $values['PARAGRAPH'] = $clause_paragraph;
            $values['SUBPARAGRAPH'] = $clause_subparagraph;
            $values['NAME'] = $name;
            $values['VALID_FROM'] = $valid_from;
            $values['VALID_TILL'] = $valid_till;
            $values['LABEL'] = $clause_label;
            $values['NOTICE'] = $clause_notice;
            $values['NOTICE_LABEL'] = $clause_notice_label;
            $values['PENALTY_MIN'] = $clause_penalty_min;
            $values['PENALTY_MAX'] = $clause_penalty_max;
            $values['PENALTY_FIRST'] = $clause_penalty_first;
            $values['TERM'] = $clause_term;
//            $values['CLASS_TYPE'] = $classificatorsIds;
            $values['GROUP'] = $clause_group;
            $values['FIELDS_VALUES'] = serialize($field);

            $clauseManager = new \Atp\Document\Clause($this->core);

            if ($this->core->db_select_count($result) === 0) {
                $clauseId = $this->core->db_ATP_insert(PREFIX . 'ATP_CLAUSES', $values);
                
                $clauseManager->setClassificators($clauseId, $classificatorsIds);

                $adminJournal = $this->core->factory->AdminJournal();
                $adminJournal->addRecord(
                    $adminJournal->getCreateRecord(
                        'Straipsnį "' . $values['NAME'] . '"'
                    )
                );

                $this->core->set_message('success', 'Straipsnis "' . $name . '" sukurtas sėkmingai.');
            } elseif (!empty($_GET['id']) && $_GET['action'] === 'save' && empty($values['PARENT_ID'])) {
                $values = array_merge($data, $values, array('ID' => (int) $_GET['id']));

                $onDuplicate = array();
                foreach ($values as $key => $value) {
                    $onDuplicate[] = '`' . $key . '` = VALUES(`' . $key . '`)';
                }

                $clauseId = $this->core->db_ATP_insert(PREFIX . 'ATP_CLAUSES', $values, 1, $onDuplicate);
                
                $clauseManager->setClassificators($values['ID'], $classificatorsIds);

                
                $adminJournal = $this->core->factory->AdminJournal();
                $adminJournal->addRecord(
                    $adminJournal->getEditRecord(
                        'Straipsnį "' . $values['NAME'] . '" ("' . $data['NAME'] . '")'
                    )
                );
                

                $this->core->set_message('success', 'Straipsnis "' . $name . '" atnaujintas sėkmingai.');
            } else {
                $this->core->set_message('error', 'Straipsnis "' . $name . ' str." jau yra sukurtas.');
            }

            header('Location: index.php?m=11');
            exit;
        }
    }

    /**
     * Sugeneruoja piktogramą
     * @param string $image Kelis į originalų paveiksliuką
     * @param string $thumb_dir Piktogramos saugojimo katalogas
     * @param boolen $return_existing [optional] Nurodo ar gražinti piktogramą, jei ji egzistuoja
     * @return boolean|string Piktograms failo vardas
     */
    public function createThumbnail($image, $thumb_dir, $return_existing = true)
    {
        if (is_file($image) === false) {
            return false;
        }

        if (is_dir($thumb_dir) === false) {
            $oldumask = umask(0);
            mkdir($thumb_dir, 0777, true);
            umask($oldumask);
        }

        $image_size = getimagesize($image);
        if (is_file($image) === false || $image_size === false || is_dir($thumb_dir) === false) {
            return false;
        }

        $thumb_width = 170;
        $thumb_height = 100;
        $thumb_prefix = "_thumb_";

        $path_info = pathinfo($image);
        $thumb_name = $thumb_prefix . $path_info['basename'];

        $thumb_file = $thumb_dir . $thumb_name;
        if (is_file($thumb_file)) {
            if ($return_existing === true) {
                return $thumb_name;
            } else {
                unlink($thumb_file);
            }
        }

        $width = $image_size[0];
        $height = $image_size[1];

        if ($width > $height) {
            $new_width = $thumb_width;
            $new_height = intval($height * $new_width / $width);
        } else {
            $new_height = $thumb_height;
            $new_width = intval($width * $new_height / $height);
        }

        $dest_x = intval(($thumb_width - $new_width) / 2);
        $dest_y = intval(($thumb_height - $new_height) / 2);

        if ($image_size[2] == 1) {
            $imgt = "ImageGIF";
            $imgcreatefrom = "ImageCreateFromGIF";
        } elseif ($image_size[2] == 2) {
            $imgt = "ImageJPEG";
            $imgcreatefrom = "ImageCreateFromJPEG";
        } elseif ($image_size[2] == 3) {
            $imgt = "ImagePNG";
            $imgcreatefrom = "ImageCreateFromPNG";
        }

        if ($imgt) {
            $old_image = $imgcreatefrom($image);
            $new_image = imagecreatetruecolor($thumb_width, $thumb_height);
            imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $width, $height);

            $oldumask = umask(0);
            $imgt($new_image, $thumb_file);
            chmod($thumb_file, 0644);
            umask($oldumask);
        }
        return $thumb_name;
    }

    public function report()
    {
        $type = $_GET['action'];
        switch ($type) {
            case 'download':
                require_once($this->core->config['REAL_URL'] . 'class/Reports/Reports.php');
                $data = $_GET;
                $report_id = &$data['id'];
                $report_type = &$data['type'];

                $rp = new \Atpreports($this->core);
                $method = 'generate' . strtoupper($report_type);
                if (method_exists($rp, $method)) {
                    $report = $this->core->logic2->get_reports(array('ID' => $report_id), false, null, '*');
                    if (empty($report)) {
                        break;
                    }

                    $params['field'] = unserialize($report['PARAMETERS']);
                    $interval = array(
                        'from' => &$data['from'],
                        'till' => &$data['till']
                    );
                    $records = $this->searchFields($params, 'search', $interval);

                    $arr = array(
                        'name' => &$report['NAME'],
                        'interval' => &$interval,
                        'records' => &$records
                    );
                    $rp->$method($arr);
                }
                break;
        }
    }

    /**
     * @deprecated Perkėlus saugojimą galima trinti
     * @param type $file
     * @return boolean
     */
    public function uploadFile($file)
    {
        $oldumask = umask(0);

        // Failo saugojimo vieta
        $atp_uploads_path = 'files/uploads/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $uploads_path = $this->core->config['REAL_URL'] . $atp_uploads_path;
        if (is_dir($uploads_path) === false) {
            mkdir($uploads_path, 0777, true);
        }

        // Performuojamas failo pavadinimas
        $slug = new \Slug();
        $info = pathinfo($file['name']);
        $new_name = $slug->makeSlugs($info['filename']) . '.' . $info['extension'];
        while (is_file($uploads_path . $new_name) === true) {
            $new_name = $slug->makeSlugs($info['filename'])
                . '-['
                . join('-', array_reverse(explode(' ', str_replace('0.', '', microtime())))) . '].'
                . $info['extension'];
        }
        $new_path = $atp_uploads_path . $new_name;
        $new_full_path = $uploads_path . $new_name;

        // Perkeliamas failas
        $is_moved = move_uploaded_file($file['tmp_name'], $new_full_path);
        if ($is_moved === false) {
            $is_moved = rename($file['tmp_name'], $new_full_path);
        }
        chmod($new_full_path, 0644);

        umask($oldumask);

        if (is_file($new_full_path)) {
            #
            # Išsaugotas failas
            #
            $sp = array('<br/>Vartotojas išsaugojo "failą". Pavadinimas - ' . $new_name);
            $this->core->manage->writeATPactionLog($sp);
            return array(
                'path' => $new_path,
                'size' => $file['size'],
                'type' => $file['type'],
                'filename' => $new_name
            );
        }
        return false;
    }

    /**
     * @deprecated Perkėlus saugojimą galima trinti
     * @param type $file_arg
     * @return type
     */
    public function saveFile($file_arg)
    {
        return $this->core->db_ATP_insert(PREFIX . 'ATP_FILES', $file_arg, 'replace');
    }
}
