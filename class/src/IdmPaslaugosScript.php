<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosScript
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_SCRIPT", uniqueConstraints={@ORM\UniqueConstraint(name="_SERVICE_ID", columns={"SERVICE_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosScript
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SCRIPT_NAME", type="string", length=256, nullable=false)
     */
    private $scriptName;

    /**
     * @var \IdmPaslaugos
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SERVICE_ID", referencedColumnName="ID")
     * })
     */
    private $service;


}

