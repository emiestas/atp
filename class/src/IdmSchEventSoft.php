<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmSchEventSoft
 *
 * @ORM\Table(name="IDM_IDM_SCH_EVENT_SOFT", indexes={@ORM\Index(name="fk_IDM_SCH_EVENT_SOFT_IDM_SCH_EVENTS1_idx", columns={"EVENT_ID"})})
 * @ORM\Entity
 */
class IdmSchEventSoft
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="EVENT_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $eventId;

    /**
     * @var string
     *
     * @ORM\Column(name="SOFT", type="string", length=25, nullable=false)
     */
    private $soft;

    /**
     * @var string
     *
     * @ORM\Column(name="ITEM_TYPE", type="string", length=50, nullable=false)
     */
    private $itemType;

    /**
     * @var string
     *
     * @ORM\Column(name="ITEM", type="string", length=25, nullable=false)
     */
    private $item;


}

