<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsDeputy
 *
 * @ORM\Table(name="IDM_IDM_DHS_DEPUTY")
 * @ORM\Entity
 */
class IdmDhsDeputy
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=false)
     */
    private $userId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="DEPUTY_ID", type="integer", nullable=false)
     */
    private $deputyId = '0';


}

