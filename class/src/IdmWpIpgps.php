<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWpIpgps
 *
 * @ORM\Table(name="IDM_IDM_WP_IPGPS", indexes={@ORM\Index(name="IP", columns={"IP"})})
 * @ORM\Entity
 */
class IdmWpIpgps
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var string
     *
     * @ORM\Column(name="IP", type="string", length=20, nullable=true)
     */
    private $ip = '';

    /**
     * @var string
     *
     * @ORM\Column(name="GISPOINT", type="string", length=255, nullable=true)
     */
    private $gispoint = '';

    /**
     * @var string
     *
     * @ORM\Column(name="GISLOCATION", type="string", length=255, nullable=true)
     */
    private $gislocation = '';


}

