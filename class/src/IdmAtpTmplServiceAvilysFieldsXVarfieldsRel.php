<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTmplServiceAvilysFieldsXVarfieldsRel
 *
 * @ORM\Table(name="IDM_IDM_ATP_TMPL_SERVICE_AVILYS_FIELDS_X_VARFIELDS_REL", uniqueConstraints={@ORM\UniqueConstraint(name="AVILYS_FIELD_ID", columns={"AVILYS_FIELD_ID", "TEMPLATE_ID"})})
 * @ORM\Entity
 */
class IdmAtpTmplServiceAvilysFieldsXVarfieldsRel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="AVILYS_FIELD_ID", type="integer", nullable=false)
     */
    private $avilysFieldId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TEMPLATE_ID", type="integer", nullable=false)
     */
    private $templateId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TEMPLATE_VAR_ID", type="integer", nullable=false)
     */
    private $templateVarId;


}

