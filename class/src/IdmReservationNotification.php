<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmReservationNotification
 *
 * @ORM\Table(name="IDM_IDM_RESERVATION_NOTIFICATION", indexes={@ORM\Index(name="REZERVATION_ID", columns={"RESERVATION_ID", "RECIPIENT"})})
 * @ORM\Entity
 */
class IdmReservationNotification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="RESERVATION_ID", type="integer", nullable=true)
     */
    private $reservationId;

    /**
     * @var string
     *
     * @ORM\Column(name="MESSAGE", type="text", length=65535, nullable=false)
     */
    private $message;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECIPIENT", type="integer", nullable=false)
     */
    private $recipient;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATED", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="VIEWED", type="datetime", nullable=false)
     */
    private $viewed;


}

