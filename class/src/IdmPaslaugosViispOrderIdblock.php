<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosViispOrderIdblock
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_VIISP_ORDER_IDBLOCK", indexes={@ORM\Index(name="fk_VIISP_ID", columns={"VIISP_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosViispOrderIdblock
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="MSG_ID", type="string", length=64, nullable=false)
     */
    private $msgId;

    /**
     * @var string
     *
     * @ORM\Column(name="MESSAGE_CREATION_DATE", type="string", length=25, nullable=false)
     */
    private $messageCreationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="VIISP_SERVICE_CODE", type="string", length=256, nullable=false)
     */
    private $viispServiceCode;

    /**
     * @var string
     *
     * @ORM\Column(name="VIISP_PROCESS_ID", type="string", length=64, nullable=false)
     */
    private $viispProcessId;

    /**
     * @var string
     *
     * @ORM\Column(name="VIISP_APPLICATION_ID", type="string", length=64, nullable=false)
     */
    private $viispApplicationId;

    /**
     * @var \IdmPaslaugosViispOrder
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosViispOrder")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="VIISP_ID", referencedColumnName="ID")
     * })
     */
    private $viisp;


}

