<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosFiles
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_FILES")
 * @ORM\Entity
 */
class IdmPaslaugosFiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=500, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=200, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="SIZE", type="string", length=50, nullable=false)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="PATH", type="string", length=5000, nullable=false)
     */
    private $path;


}

