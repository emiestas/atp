<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmIdbUserreports
 *
 * @ORM\Table(name="IDM_IDM_IDB_USERREPORTS", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmIdbUserreports
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="REPORT_ID", type="integer", nullable=false)
     */
    private $reportId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=false)
     */
    private $userId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="REPORT_DATA", type="text", nullable=false)
     */
    private $reportData;


}

