<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmRepReportfolder
 *
 * @ORM\Table(name="IDM_IDM_REP_REPORTFOLDER")
 * @ORM\Entity
 */
class IdmRepReportfolder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="FOLDER_NAME", type="string", length=255, nullable=true)
     */
    private $folderName = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="POSITION", type="integer", nullable=false)
     */
    private $position = '0';


}

