<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmReservationObject
 *
 * @ORM\Table(name="IDM_IDM_RESERVATION_OBJECT")
 * @ORM\Entity
 */
class IdmReservationObject
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE_ID", type="integer", nullable=false)
     */
    private $typeId;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="TIME_LIMIT", type="integer", nullable=true)
     */
    private $timeLimit;

    /**
     * @var integer
     *
     * @ORM\Column(name="APPROVABLE_BY", type="integer", nullable=true)
     */
    private $approvableBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ENABLED", type="boolean", nullable=true)
     */
    private $enabled = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="SPECIAL_DATA", type="text", length=65535, nullable=true)
     */
    private $specialData;

    /**
     * @var string
     *
     * @ORM\Column(name="INFO", type="text", length=65535, nullable=true)
     */
    private $info;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DELETED", type="datetime", nullable=true)
     */
    private $deleted;


}

