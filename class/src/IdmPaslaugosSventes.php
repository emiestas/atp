<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosSventes
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_SVENTES")
 * @ORM\Entity
 */
class IdmPaslaugosSventes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FREE_DATE", type="date", nullable=false)
     */
    private $freeDate;


}

