<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpUserPrivileges
 *
 * @ORM\Table(name="IDM_IDM_ATP_USER_PRIVILEGES", indexes={@ORM\Index(name="TABLE_ID", columns={"TABLE_ID"})})
 * @ORM\Entity
 */
class IdmAtpUserPrivileges
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="M_PEOPLE_ID", type="integer", nullable=false)
     */
    private $mPeopleId;

    /**
     * @var integer
     *
     * @ORM\Column(name="M_DEPARTMENT_ID", type="integer", nullable=false)
     */
    private $mDepartmentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TABLE_ID", type="integer", nullable=false)
     */
    private $tableId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VIEW_RECORD", type="boolean", nullable=false)
     */
    private $viewRecord;

    /**
     * @var boolean
     *
     * @ORM\Column(name="EDIT_RECORD", type="boolean", nullable=false)
     */
    private $editRecord;

    /**
     * @var boolean
     *
     * @ORM\Column(name="DELETE_RECORD", type="boolean", nullable=false)
     */
    private $deleteRecord;

    /**
     * @var boolean
     *
     * @ORM\Column(name="INVESTIGATE_RECORD", type="boolean", nullable=false)
     */
    private $investigateRecord;

    /**
     * @var boolean
     *
     * @ORM\Column(name="EXAMINATE_RECORD", type="boolean", nullable=false)
     */
    private $examinateRecord;

    /**
     * @var boolean
     *
     * @ORM\Column(name="END_RECORD", type="boolean", nullable=false)
     */
    private $endRecord;


}

