<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsDocs
 *
 * @ORM\Table(name="IDM_IDM_PHS_DOCS", indexes={@ORM\Index(name="STATUS_ID", columns={"STATUS_ID"}), @ORM\Index(name="FOLDER_ID", columns={"FOLDER_ID"}), @ORM\Index(name="PROJECT_ID", columns={"PROJECT_ID"}), @ORM\Index(name="DHSDOC_ID", columns={"DHSDOC_ID"}), @ORM\Index(name="NOTE_ID", columns={"NOTE_ID"}), @ORM\Index(name="PROCESS_ID", columns={"NOTE_ID"}), @ORM\Index(name="STAGE_ID", columns={"STAGE_ID"})})
 * @ORM\Entity
 */
class IdmPhsDocs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=65535, nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_DATA", type="text", length=65535, nullable=false)
     */
    private $docData;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_ACCOMPLISH", type="text", length=65535, nullable=false)
     */
    private $docAccomplish;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", length=65535, nullable=true)
     */
    private $receiverInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", length=65535, nullable=true)
     */
    private $receiverShort;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_INFO", type="text", length=65535, nullable=true)
     */
    private $senderInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_SHORT", type="text", length=65535, nullable=true)
     */
    private $senderShort;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXPIRE_DATE", type="datetime", nullable=true)
     */
    private $expireDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ACCOMPLISH_DATE", type="datetime", nullable=true)
     */
    private $accomplishDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CHECK_DATE", type="datetime", nullable=true)
     */
    private $checkDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="REGISTER_DATE", type="datetime", nullable=true)
     */
    private $registerDate = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="PROJECT_ID", type="integer", nullable=true)
     */
    private $projectId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS_ID", type="integer", nullable=true)
     */
    private $statusId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="STAGE_ID", type="integer", nullable=true)
     */
    private $stageId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PROCESS_ID", type="integer", nullable=true)
     */
    private $processId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="FOLDER_ID", type="integer", nullable=false)
     */
    private $folderId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="DHSDOC_ID", type="integer", nullable=false)
     */
    private $dhsdocId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="NOTE_ID", type="integer", nullable=false)
     */
    private $noteId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENTTYPE", type="string", length=100, nullable=false)
     */
    private $contenttype = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENT", type="text", nullable=true)
     */
    private $content;


}

