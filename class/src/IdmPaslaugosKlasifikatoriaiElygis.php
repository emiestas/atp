<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosKlasifikatoriaiElygis
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_KLASIFIKATORIAI_ELYGIS")
 * @ORM\Entity
 */
class IdmPaslaugosKlasifikatoriaiElygis
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="FOREIGN_ID", type="integer", nullable=true)
     */
    private $foreignId;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPTION", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="NO", type="string", length=15, nullable=false)
     */
    private $no;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=false)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=false)
     */
    private $modifyIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=20, nullable=false)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=20, nullable=false)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=false)
     */
    private $modifyDate = '2007-01-01 00:00:00';


}

