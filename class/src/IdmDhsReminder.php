<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsReminder
 *
 * @ORM\Table(name="IDM_IDM_DHS_REMINDER", indexes={@ORM\Index(name="DOC_ID", columns={"DOC_ID"})})
 * @ORM\Entity
 */
class IdmDhsReminder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     */
    private $docId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="MAILTYPE", type="string", length=50, nullable=true)
     */
    private $mailtype;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_SENT", type="string", length=10, nullable=false)
     */
    private $isSent = '';


}

