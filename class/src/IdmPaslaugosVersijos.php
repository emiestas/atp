<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosVersijos
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_VERSIJOS")
 * @ORM\Entity
 */
class IdmPaslaugosVersijos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=5, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="PASLAUGOS_ID", type="integer", nullable=false)
     */
    private $paslaugosId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="APRASYMAS", type="text", nullable=true)
     */
    private $aprasymas;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=20, nullable=true)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=20, nullable=true)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=true)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=true)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=true)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=true)
     */
    private $modifyIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SPRENDIMAS", type="text", nullable=false)
     */
    private $sprendimas;

    /**
     * @var integer
     *
     * @ORM\Column(name="VERSIJOS_NR", type="integer", nullable=false)
     */
    private $versijosNr = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="OLDDATA", type="text", nullable=true)
     */
    private $olddata;


}

