<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmCrmContactInfo
 *
 * @ORM\Table(name="IDM_IDM_CRM_CONTACT_INFO", indexes={@ORM\Index(name="SUBJECT", columns={"SUBJECT"}), @ORM\Index(name="CONTACT_ID", columns={"CONTACT_ID"}), @ORM\Index(name="CONTACTER_ID", columns={"CONTACTER_ID"}), @ORM\Index(name="ORGANIZATION_ID", columns={"ORGANIZATION_ID"}), @ORM\Index(name="INFO", columns={"INFO_LOG"})})
 * @ORM\Entity
 */
class IdmCrmContactInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SUBJECT", type="string", length=255, nullable=false)
     */
    private $subject;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="INFO", type="text", length=16777215, nullable=false)
     */
    private $info;

    /**
     * @var string
     *
     * @ORM\Column(name="INFO_LOG", type="text", length=16777215, nullable=false)
     */
    private $infoLog;

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE_ID", type="integer", nullable=false)
     */
    private $typeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORGANIZATION_ID", type="integer", nullable=true)
     */
    private $organizationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTACT_ID", type="integer", nullable=true)
     */
    private $contactId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTACTER_ID", type="integer", nullable=false)
     */
    private $contacterId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="COMPLETED", type="boolean", nullable=false)
     */
    private $completed = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATED_BY", type="integer", nullable=false)
     */
    private $createdBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="STATUS", type="boolean", nullable=false)
     */
    private $status;


}

