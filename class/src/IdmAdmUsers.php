<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAdmUsers
 *
 * @ORM\Table(name="IDM_IDM_ADM_USERS", indexes={@ORM\Index(name="USERNAME", columns={"USERNAME"})})
 * @ORM\Entity
 */
class IdmAdmUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="USERNAME", type="string", length=100, nullable=true)
     */
    private $username = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PASSWORD", type="string", length=32, nullable=true)
     */
    private $password = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="GROUP_ID", type="integer", nullable=false)
     */
    private $groupId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="FNAME", type="string", length=100, nullable=true)
     */
    private $fname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="LNAME", type="string", length=100, nullable=true)
     */
    private $lname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="EMAIL", type="string", length=100, nullable=true)
     */
    private $email = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ADMIN_ACCESS", type="string", length=5, nullable=true)
     */
    private $adminAccess = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=true)
     */
    private $peopleId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SKIN", type="string", length=50, nullable=false)
     */
    private $skin = '';

    /**
     * @var string
     *
     * @ORM\Column(name="DESKTOP", type="string", length=50, nullable=false)
     */
    private $desktop = '';

    /**
     * @var string
     *
     * @ORM\Column(name="HELP", type="string", length=10, nullable=false)
     */
    private $help = '';

    /**
     * @var string
     *
     * @ORM\Column(name="LAST_URL", type="string", length=255, nullable=false)
     */
    private $lastUrl = '';

    /**
     * @var string
     *
     * @ORM\Column(name="BLOCKED", type="string", length=2, nullable=false)
     */
    private $blocked = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="BLOCKED_TILL", type="datetime", nullable=false)
     */
    private $blockedTill = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="SEND_EMAILS", type="string", length=10, nullable=false)
     */
    private $sendEmails;

    /**
     * @var string
     *
     * @ORM\Column(name="SELECT_TYPE", type="string", length=10, nullable=false)
     */
    private $selectType;

    /**
     * @var string
     *
     * @ORM\Column(name="DHSFILTER_DATA", type="text", length=16777215, nullable=false)
     */
    private $dhsfilterData;

    /**
     * @var string
     *
     * @ORM\Column(name="ACTIVEDIRECTORY", type="string", length=255, nullable=false)
     */
    private $activedirectory;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LAST_PASS_CHANGE", type="date", nullable=true)
     */
    private $lastPassChange;


}

