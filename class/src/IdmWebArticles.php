<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWebArticles
 *
 * @ORM\Table(name="IDM_IDM_WEB_ARTICLES")
 * @ORM\Entity
 */
class IdmWebArticles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="MENU_ID", type="integer", nullable=false)
     */
    private $menuId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="TEXT_DATA", type="text", nullable=true)
     */
    private $textData;


}

