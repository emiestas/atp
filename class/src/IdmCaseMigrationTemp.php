<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmCaseMigrationTemp
 *
 * @ORM\Table(name="IDM_IDM_CASE_MIGRATION_TEMP")
 * @ORM\Entity
 */
class IdmCaseMigrationTemp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCUMENT_ID", type="integer", nullable=false)
     */
    private $documentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CASE_INTEGER", type="integer", nullable=false)
     */
    private $caseInteger;

    /**
     * @var integer
     *
     * @ORM\Column(name="CASE_FRACTION", type="integer", nullable=false)
     */
    private $caseFraction;

    /**
     * @var integer
     *
     * @ORM\Column(name="CASE_TYPE", type="integer", nullable=false)
     */
    private $caseType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CASE_YEAR", type="datetime", nullable=false)
     */
    private $caseYear;

    /**
     * @var integer
     *
     * @ORM\Column(name="CASE_ID", type="integer", nullable=false)
     */
    private $caseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PLAN_ID", type="integer", nullable=false)
     */
    private $planId;

    /**
     * @var integer
     *
     * @ORM\Column(name="MAIN_DOCUMENT_ID", type="integer", nullable=false)
     */
    private $mainDocumentId;


}

