<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpUser
 *
 * @ORM\Table(name="IDM_IDM_ATP_USER", indexes={@ORM\Index(name="CLAUSE_ID", columns={"CLAUSE_ID", "WORKER_ID"})})
 * @ORM\Entity
 */
class IdmAtpUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="CLAUSE_ID", type="integer", nullable=false)
     */
    private $clauseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="WORKER_ID", type="integer", nullable=false)
     */
    private $workerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="M_PEOPLE_ID", type="integer", nullable=false)
     */
    private $mPeopleId;

    /**
     * @var integer
     *
     * @ORM\Column(name="M_DEPARTMENT_ID", type="integer", nullable=false)
     */
    private $mDepartmentId;


}

