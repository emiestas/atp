<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmFvasXxFinInt201GsSaskaitosV
 *
 * @ORM\Table(name="IDM_IDM_FVAS_XX_FIN_INT201_GS_SASKAITOS_V", indexes={@ORM\Index(name="_KLIENTO_KODAS", columns={"KLIENTO_KODAS"})})
 * @ORM\Entity
 */
class IdmFvasXxFinInt201GsSaskaitosV
{
    /**
     * @var integer
     *
     * @ORM\Column(name="SASKAITOS_ID", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $saskaitosId;

    /**
     * @var string
     *
     * @ORM\Column(name="SASKAITOS_SERIJA", type="string", length=150, nullable=true)
     */
    private $saskaitosSerija;

    /**
     * @var string
     *
     * @ORM\Column(name="SASKAITOS_NUMERIS", type="string", length=20, nullable=false)
     */
    private $saskaitosNumeris;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="SASKAITOS_DATA", type="datetime", nullable=false)
     */
    private $saskaitosData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="APMOKEJIMO_TERMINAS", type="datetime", nullable=true)
     */
    private $apmokejimoTerminas;

    /**
     * @var string
     *
     * @ORM\Column(name="VALIUTA", type="string", length=15, nullable=true)
     */
    private $valiuta;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORG_ID", type="bigint", nullable=true)
     */
    private $orgId;

    /**
     * @var string
     *
     * @ORM\Column(name="KLIENTO_KODAS", type="string", length=30, nullable=false)
     */
    private $klientoKodas;

    /**
     * @var string
     *
     * @ORM\Column(name="KLIENTO_PAVADINIMAS", type="string", length=360, nullable=true)
     */
    private $klientoPavadinimas;

    /**
     * @var string
     *
     * @ORM\Column(name="KLIENTO_ADRESAS", type="string", length=4000, nullable=true)
     */
    private $klientoAdresas;

    /**
     * @var string
     *
     * @ORM\Column(name="SASKAITOS_TIPAS", type="string", length=20, nullable=true)
     */
    private $saskaitosTipas;

    /**
     * @var string
     *
     * @ORM\Column(name="SASKAITOS_SUMA", type="decimal", precision=14, scale=4, nullable=true)
     */
    private $saskaitosSuma;

    /**
     * @var string
     *
     * @ORM\Column(name="APMOKETA_SUMA", type="decimal", precision=14, scale=4, nullable=true)
     */
    private $apmoketaSuma;

    /**
     * @var string
     *
     * @ORM\Column(name="ATSISKAITOMOJI_SASKAITA", type="string", length=30, nullable=true)
     */
    private $atsiskaitomojiSaskaita;

    /**
     * @var string
     *
     * @ORM\Column(name="BANKAS", type="string", length=320, nullable=true)
     */
    private $bankas;

    /**
     * @var string
     *
     * @ORM\Column(name="SWIFT", type="string", length=12, nullable=true)
     */
    private $swift;


}

