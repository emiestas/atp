<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosFolder
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_FOLDER", uniqueConstraints={@ORM\UniqueConstraint(name="_NAME", columns={"NAME"})})
 * @ORM\Entity
 */
class IdmPaslaugosFolder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=25, nullable=false)
     */
    private $name;


}

