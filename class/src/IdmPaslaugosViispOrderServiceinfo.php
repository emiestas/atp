<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosViispOrderServiceinfo
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_VIISP_ORDER_SERVICEINFO", indexes={@ORM\Index(name="_VIISP_ID", columns={"VIISP_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosViispOrderServiceinfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SERVICE_CODE", type="string", length=256, nullable=false)
     */
    private $serviceCode;

    /**
     * @var string
     *
     * @ORM\Column(name="SERVICE_SUBTYPE", type="string", length=256, nullable=false)
     */
    private $serviceSubtype;

    /**
     * @var string
     *
     * @ORM\Column(name="SERVICE_SORT", type="string", length=256, nullable=false)
     */
    private $serviceSort;

    /**
     * @var string
     *
     * @ORM\Column(name="SERVICE_AUXTYPE", type="string", length=256, nullable=false)
     */
    private $serviceAuxtype;

    /**
     * @var \IdmPaslaugosViispOrder
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosViispOrder")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="VIISP_ID", referencedColumnName="ID")
     * })
     */
    private $viisp;


}

