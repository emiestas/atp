<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsParents
 *
 * @ORM\Table(name="IDM_IDM_DHS_PARENTS", indexes={@ORM\Index(name="DOC_ID", columns={"DOC_ID"})})
 * @ORM\Entity
 */
class IdmDhsParents
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=false)
     */
    private $parentId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     */
    private $docId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="LAWTYPE", type="integer", nullable=true)
     */
    private $lawtype;

    /**
     * @var string
     *
     * @ORM\Column(name="REL_NAME", type="string", length=100, nullable=false)
     */
    private $relName;


}

