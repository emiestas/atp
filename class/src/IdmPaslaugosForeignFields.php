<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosForeignFields
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_FOREIGN_FIELDS", indexes={@ORM\Index(name="_FIELD_ID__TYPE", columns={"FIELD_ID", "TYPE"}), @ORM\Index(name="_TYPE", columns={"TYPE"}), @ORM\Index(name="_FIELD_ID", columns={"FIELD_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosForeignFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="FIELD_ID", type="integer", nullable=false)
     */
    private $fieldId;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=20, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=50, nullable=false)
     */
    private $name;


}

