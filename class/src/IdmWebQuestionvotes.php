<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWebQuestionvotes
 *
 * @ORM\Table(name="IDM_IDM_WEB_QUESTIONVOTES", indexes={@ORM\Index(name="CONTACT_ID", columns={"CONTACT_ID"}), @ORM\Index(name="QUESTION_ID", columns={"QUESTION_ID"})})
 * @ORM\Entity
 */
class IdmWebQuestionvotes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=255, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTACT_ID", type="string", length=100, nullable=false)
     */
    private $contactId = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="QUESTION_ID", type="integer", nullable=false)
     */
    private $questionId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="VOTE_DATA", type="text", nullable=true)
     */
    private $voteData;

    /**
     * @var string
     *
     * @ORM\Column(name="CONTACT_IP", type="text", nullable=true)
     */
    private $contactIp;


}

