<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpFieldDefault
 *
 * @ORM\Table(name="IDM_IDM_ATP_FIELD_DEFAULT")
 * @ORM\Entity
 */
class IdmAtpFieldDefault
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="FIELD_ID", type="integer", nullable=false)
     */
    private $fieldId;

    /**
     * @var string
     *
     * @ORM\Column(name="VALUE", type="string", length=5000, nullable=true)
     */
    private $value;


}

