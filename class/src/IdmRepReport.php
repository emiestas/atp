<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmRepReport
 *
 * @ORM\Table(name="IDM_IDM_REP_REPORT")
 * @ORM\Entity
 */
class IdmRepReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="REPORT_NAME", type="string", length=255, nullable=true)
     */
    private $reportName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SYS_NAME", type="string", length=255, nullable=false)
     */
    private $sysName = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="FOLDER_ID", type="integer", nullable=false)
     */
    private $folderId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="POSITION", type="integer", nullable=false)
     */
    private $position = '0';


}

