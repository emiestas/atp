<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsDocumentCaseNumber
 *
 * @ORM\Table(name="IDM_IDM_DHS_DOCUMENT_CASE_NUMBER", indexes={@ORM\Index(name="fk_IDM_DHS_DOCUMENT_CASE_NUMBER__DOCUMENTS2_idx", columns={"MAIN_DOCUMENT_ID"}), @ORM\Index(name="fk_IDM_DHS_DOCUMENT_CASE_NUMBER__DOCUMENTATION_PLAN1_idx", columns={"PLAN_ID"})})
 * @ORM\Entity
 */
class IdmDhsDocumentCaseNumber
{
    /**
     * @var integer
     *
     * @ORM\Column(name="INTEGER", type="integer", nullable=false)
     */
    private $integer;

    /**
     * @var integer
     *
     * @ORM\Column(name="FRACTION", type="integer", nullable=false)
     */
    private $fraction;

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE_ID", type="integer", nullable=false)
     */
    private $typeId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="INSERT_DATE", type="datetime", nullable=false)
     */
    private $insertDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="UPDATE_DATE", type="datetime", nullable=false)
     */
    private $updateDate;

    /**
     * @var \IdmDhsDocumentationPlan
     *
     * @ORM\ManyToOne(targetEntity="IdmDhsDocumentationPlan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PLAN_ID", referencedColumnName="ID")
     * })
     */
    private $plan;

    /**
     * @var \IdmDhsDocuments
     *
     * @ORM\ManyToOne(targetEntity="IdmDhsDocuments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="MAIN_DOCUMENT_ID", referencedColumnName="ID")
     * })
     */
    private $mainDocument;

    /**
     * @var \IdmDhsDocuments
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="IdmDhsDocuments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DOCUMENT_ID", referencedColumnName="ID")
     * })
     */
    private $document;


}

