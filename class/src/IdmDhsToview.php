<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsToview
 *
 * @ORM\Table(name="IDM_IDM_DHS_TOVIEW", indexes={@ORM\Index(name="DOC_ID", columns={"DOC_ID"}), @ORM\Index(name="EMPLOYER_ID", columns={"EMPLOYER_ID"})})
 * @ORM\Entity
 */
class IdmDhsToview
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     */
    private $docId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="EMPLOYER_ID", type="integer", nullable=false)
     */
    private $employerId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="EMPLOYER_TYPE", type="integer", nullable=false)
     */
    private $employerType = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="VIEW_ISNEW", type="string", length=5, nullable=false)
     */
    private $viewIsnew;


}

