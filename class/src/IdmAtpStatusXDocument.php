<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpStatusXDocument
 *
 * @ORM\Table(name="IDM_IDM_ATP_STATUS_X_DOCUMENT", indexes={@ORM\Index(name="fk_ATP_STATUS_X_DOCUMENT_ATP_STATUS1_idx", columns={"STATUS_ID"}), @ORM\Index(name="fk_IDM_ATP_STATUS_X_DOCUMENT_IDM_ATP_DOCUMENTS1", columns={"DOCUMENT_ID"})})
 * @ORM\Entity
 */
class IdmAtpStatusXDocument
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ATP_STATUS_ID", type="integer", nullable=false)
     */
    private $atpStatusId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VALID", type="boolean", nullable=false)
     */
    private $valid = '1';

    /**
     * @var \IdmAtpStatus
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="IdmAtpStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="STATUS_ID", referencedColumnName="ID")
     * })
     */
    private $status;

    /**
     * @var \IdmAtpDocuments
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="IdmAtpDocuments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DOCUMENT_ID", referencedColumnName="ID")
     * })
     */
    private $document;


}

