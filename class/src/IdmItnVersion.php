<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmItnVersion
 *
 * @ORM\Table(name="IDM_IDM_ITN_VERSION", indexes={@ORM\Index(name="ITEM_ID", columns={"ITEM_ID"})})
 * @ORM\Entity
 */
class IdmItnVersion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="VERSION", type="string", length=100, nullable=false)
     */
    private $version = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="ITEM_ID", type="integer", nullable=false)
     */
    private $itemId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="VERSION_URL", type="text", length=16777215, nullable=true)
     */
    private $versionUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="VERSION_DATA", type="text", nullable=true)
     */
    private $versionData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="VERSION_DATE", type="datetime", nullable=true)
     */
    private $versionDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';


}

