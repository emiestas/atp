<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpClassificators
 *
 * @ORM\Table(name="IDM_IDM_ATP_CLASSIFICATORS", indexes={@ORM\Index(name="VALID", columns={"VALID"}), @ORM\Index(name="PARENT_ID", columns={"PARENT_ID", "VALID"})})
 * @ORM\Entity
 */
class IdmAtpClassificators
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=false)
     */
    private $parentId;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=5000, nullable=false)
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VALID", type="boolean", nullable=true)
     */
    private $valid = '1';


}

