<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderRevisionFields
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_REVISION_FIELDS", indexes={@ORM\Index(name="_REVISION_ID", columns={"REVISION_ID"}), @ORM\Index(name="_FIELD_ID", columns={"FIELD_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderRevisionFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="FIELD_ID", type="integer", nullable=false)
     */
    private $fieldId;

    /**
     * @var string
     *
     * @ORM\Column(name="NOTE", type="text", length=16777215, nullable=true)
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="OLD_VALUE", type="text", length=16777215, nullable=true)
     */
    private $oldValue;

    /**
     * @var string
     *
     * @ORM\Column(name="NEW_VALUE", type="text", length=16777215, nullable=true)
     */
    private $newValue;

    /**
     * @var \IdmPaslaugosOrderRevisions
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosOrderRevisions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="REVISION_ID", referencedColumnName="ID")
     * })
     */
    private $revision;


}

