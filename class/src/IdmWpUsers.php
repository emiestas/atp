<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWpUsers
 *
 * @ORM\Table(name="IDM_IDM_WP_USERS", indexes={@ORM\Index(name="USER_ID", columns={"USER_ID"})})
 * @ORM\Entity
 */
class IdmWpUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=true)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="SOFT_ID", type="integer", nullable=true)
     */
    private $softId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=true)
     */
    private $userId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="STRUCTURE_ID", type="integer", nullable=true)
     */
    private $structureId = '0';


}

