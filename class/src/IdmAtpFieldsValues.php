<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpFieldsValues
 *
 * @ORM\Table(name="IDM_IDM_ATP_FIELDS_VALUES", uniqueConstraints={@ORM\UniqueConstraint(name="FIELD_ID", columns={"FIELD_ID", "TABLE_ID", "RECORD_ID"})}, indexes={@ORM\Index(name="TABLE_ID", columns={"TABLE_ID", "RECORD_ID"}), @ORM\Index(name="FIELD_ID_2", columns={"FIELD_ID"})})
 * @ORM\Entity
 */
class IdmAtpFieldsValues
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="FIELD_ID", type="integer", nullable=false)
     */
    private $fieldId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TABLE_ID", type="integer", nullable=false)
     */
    private $tableId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_ID", type="integer", nullable=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="VALUE", type="text", length=65535, nullable=true)
     */
    private $value;


}

