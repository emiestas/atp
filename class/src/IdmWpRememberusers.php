<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWpRememberusers
 *
 * @ORM\Table(name="IDM_IDM_WP_REMEMBERUSERS", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmWpRememberusers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=false)
     */
    private $userId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="USERNAME", type="string", length=255, nullable=false)
     */
    private $username = '';

    /**
     * @var string
     *
     * @ORM\Column(name="USER_IP", type="string", length=255, nullable=false)
     */
    private $userIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="USER_BROWSER", type="string", length=255, nullable=false)
     */
    private $userBrowser = '';

    /**
     * @var string
     *
     * @ORM\Column(name="USER_HASH", type="string", length=255, nullable=false)
     */
    private $userHash = '';

    /**
     * @var string
     *
     * @ORM\Column(name="USER_COOKIE", type="string", length=255, nullable=false)
     */
    private $userCookie = '';

    /**
     * @var string
     *
     * @ORM\Column(name="USER_SIGNATURE", type="text", length=16777215, nullable=false)
     */
    private $userSignature;


}

