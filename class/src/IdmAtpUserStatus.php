<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpUserStatus
 *
 * @ORM\Table(name="IDM_IDM_ATP_USER_STATUS")
 * @ORM\Entity
 */
class IdmAtpUserStatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="M_PEOPLE_ID", type="integer", nullable=false)
     */
    private $mPeopleId;

    /**
     * @var integer
     *
     * @ORM\Column(name="M_DEPARTMENT_ID", type="integer", nullable=false)
     */
    private $mDepartmentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VIEW_ADMIN", type="boolean", nullable=false)
     */
    private $viewAdmin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="A_TABLE", type="boolean", nullable=false)
     */
    private $aTable;

    /**
     * @var boolean
     *
     * @ORM\Column(name="A_RELATION", type="boolean", nullable=false)
     */
    private $aRelation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="A_CLASSIFICATION", type="boolean", nullable=false)
     */
    private $aClassification;

    /**
     * @var boolean
     *
     * @ORM\Column(name="A_COMPETENCE", type="boolean", nullable=false)
     */
    private $aCompetence;

    /**
     * @var boolean
     *
     * @ORM\Column(name="A_COMPETENCE_ADMIN", type="boolean", nullable=false)
     */
    private $aCompetenceAdmin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="A_COMPETENCE_COMPETENCIES", type="boolean", nullable=false)
     */
    private $aCompetenceCompetencies;

    /**
     * @var boolean
     *
     * @ORM\Column(name="A_COMPETENCE_USER_RIGHTS", type="boolean", nullable=false)
     */
    private $aCompetenceUserRights;

    /**
     * @var boolean
     *
     * @ORM\Column(name="A_DEPARTMENT", type="boolean", nullable=true)
     */
    private $aDepartment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="A_DOCUMENT", type="boolean", nullable=false)
     */
    private $aDocument;

    /**
     * @var boolean
     *
     * @ORM\Column(name="A_CLAUSE", type="boolean", nullable=false)
     */
    private $aClause;

    /**
     * @var boolean
     *
     * @ORM\Column(name="A_ACT", type="boolean", nullable=false)
     */
    private $aAct;

    /**
     * @var boolean
     *
     * @ORM\Column(name="A_DEED", type="boolean", nullable=false)
     */
    private $aDeed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="A_STATUS", type="boolean", nullable=false)
     */
    private $aStatus;


}

