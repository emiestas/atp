<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTmplVarfields
 *
 * @ORM\Table(name="IDM_IDM_ATP_TMPL_VARFIELDS")
 * @ORM\Entity
 */
class IdmAtpTmplVarfields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCUMENT_ID", type="integer", nullable=false)
     */
    private $documentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="FIELD_ID", type="integer", nullable=true)
     */
    private $fieldId;

    /**
     * @var string
     *
     * @ORM\Column(name="FIELD_VARNAME", type="string", length=100, nullable=false)
     */
    private $fieldVarname;

    /**
     * @var string
     *
     * @ORM\Column(name="FIELD_VARPATH", type="string", length=300, nullable=false)
     */
    private $fieldVarpath;


}

