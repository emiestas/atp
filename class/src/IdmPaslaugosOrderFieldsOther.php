<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderFieldsOther
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_FIELDS_OTHER", indexes={@ORM\Index(name="ORDER_ID", columns={"ORDER_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderFieldsOther
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORDER_ID", type="integer", nullable=false)
     */
    private $orderId;

    /**
     * @var string
     *
     * @ORM\Column(name="DATA", type="text", length=65535, nullable=false)
     */
    private $data;


}

