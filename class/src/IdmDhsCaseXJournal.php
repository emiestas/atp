<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsCaseXJournal
 *
 * @ORM\Table(name="IDM_IDM_DHS_CASE_X_JOURNAL")
 * @ORM\Entity
 */
class IdmDhsCaseXJournal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="CASE_ID", type="integer", nullable=false)
     */
    private $caseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId;


}

