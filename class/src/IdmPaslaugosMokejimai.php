<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosMokejimai
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_MOKEJIMAI", indexes={@ORM\Index(name="PASLAUGOS_ID", columns={"PASLAUGOS_ID"}), @ORM\Index(name="CONTACT_ID", columns={"CONTACT_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosMokejimai
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PASLAUGOS_ID", type="integer", nullable=false)
     */
    private $paslaugosId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTACT_ID", type="integer", nullable=false)
     */
    private $contactId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ORDER_DATA", type="text", length=65535, nullable=false)
     */
    private $orderData;

    /**
     * @var string
     *
     * @ORM\Column(name="BACK_DATA", type="text", length=65535, nullable=false)
     */
    private $backData;

    /**
     * @var string
     *
     * @ORM\Column(name="STATE", type="string", length=50, nullable=false)
     */
    private $state;


}

