<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderFiles
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_FILES", indexes={@ORM\Index(name="_ORDER__FILE", columns={"ORDER_ID", "FILE_ID"}), @ORM\Index(name="_ORDER_ID", columns={"ORDER_ID"}), @ORM\Index(name="_FILE_ID", columns={"FILE_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderFiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \IdmPaslaugosFiles
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosFiles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FILE_ID", referencedColumnName="ID")
     * })
     */
    private $file;

    /**
     * @var \IdmPaslaugosUzsakymai
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosUzsakymai")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ORDER_ID", referencedColumnName="ID")
     * })
     */
    private $order;


}

