<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmSchSchedulePeople
 *
 * @ORM\Table(name="IDM_IDM_SCH_SCHEDULE_PEOPLE", indexes={@ORM\Index(name="fk_IDM_SCH_SCHEDULE_PEOPLE_IDM_SCH_SCHEDULES_idx", columns={"SCHEDULE_ID"})})
 * @ORM\Entity
 */
class IdmSchSchedulePeople
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="SCHEDULE_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $scheduleId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PERSON_ID", type="integer", nullable=false)
     */
    private $personId;


}

