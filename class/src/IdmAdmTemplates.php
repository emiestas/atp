<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAdmTemplates
 *
 * @ORM\Table(name="IDM_IDM_ADM_TEMPLATES")
 * @ORM\Entity
 */
class IdmAdmTemplates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="TEXT_DATA", type="text", nullable=true)
     */
    private $textData;

    /**
     * @var integer
     *
     * @ORM\Column(name="OBJECT_ID", type="integer", nullable=false)
     */
    private $objectId = '0';


}

