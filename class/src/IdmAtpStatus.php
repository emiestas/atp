<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpStatus
 *
 * @ORM\Table(name="IDM_IDM_ATP_STATUS")
 * @ORM\Entity
 */
class IdmAtpStatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=500, nullable=false)
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VALID", type="boolean", nullable=false)
     */
    private $valid = '1';


}

