<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmIdbCategory
 *
 * @ORM\Table(name="IDM_IDM_IDB_CATEGORY", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmIdbCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=100, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ICON_OPEN", type="string", length=255, nullable=false)
     */
    private $iconOpen = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ICON_CLOSE", type="string", length=255, nullable=false)
     */
    private $iconClose = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="POSITION", type="integer", nullable=false)
     */
    private $position = '0';


}

