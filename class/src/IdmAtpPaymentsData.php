<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpPaymentsData
 *
 * @ORM\Table(name="IDM_IDM_ATP_PAYMENTS_DATA")
 * @ORM\Entity
 */
class IdmAtpPaymentsData
{
    /**
     * @var string
     *
     * @ORM\Column(name="ID", type="string", length=38, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="DOK_NUMERIS", type="string", length=60, nullable=true)
     */
    private $dokNumeris;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="OPERACIJOS_DATA", type="date", nullable=false)
     */
    private $operacijosData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DOK_DATA", type="date", nullable=false)
     */
    private $dokData;

    /**
     * @var string
     *
     * @ORM\Column(name="MOKA_MM_KODAS", type="string", length=90, nullable=true)
     */
    private $mokaMmKodas;

    /**
     * @var string
     *
     * @ORM\Column(name="MOKA_MM_PAVADINIMAS", type="string", length=750, nullable=true)
     */
    private $mokaMmPavadinimas;

    /**
     * @var string
     *
     * @ORM\Column(name="MOKESCIO_MOKETOJAI_KODAS", type="string", length=90, nullable=true)
     */
    private $mokescioMoketojaiKodas;

    /**
     * @var string
     *
     * @ORM\Column(name="MOKESCIO_MOKETOJAI_PAV", type="string", length=600, nullable=true)
     */
    private $mokescioMoketojaiPav;

    /**
     * @var integer
     *
     * @ORM\Column(name="SAVIVALDYBES_ID", type="integer", nullable=true)
     */
    private $savivaldybesId;

    /**
     * @var integer
     *
     * @ORM\Column(name="IMOKOS_KODAS", type="integer", nullable=true)
     */
    private $imokosKodas;

    /**
     * @var string
     *
     * @ORM\Column(name="OP_TIPAS", type="string", length=9, nullable=true)
     */
    private $opTipas;

    /**
     * @var integer
     *
     * @ORM\Column(name="SUMA_BV", type="integer", nullable=false)
     */
    private $sumaBv;

    /**
     * @var integer
     *
     * @ORM\Column(name="SUMA", type="integer", nullable=false)
     */
    private $suma;

    /**
     * @var string
     *
     * @ORM\Column(name="VALIUTA", type="string", length=9, nullable=false)
     */
    private $valiuta;

    /**
     * @var string
     *
     * @ORM\Column(name="MOKEJIMO_PASKIRTIS", type="string", length=1200, nullable=true)
     */
    private $mokejimoPaskirtis;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="IRS_UPD_DATA", type="date", nullable=false)
     */
    private $irsUpdData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="IRS_INS_DATA", type="date", nullable=false)
     */
    private $irsInsData;


}

