<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMainOffice
 *
 * @ORM\Table(name="IDM_IDM_MAIN_OFFICE", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmMainOffice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=true)
     */
    private $parentId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=true)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS_EN", type="string", length=255, nullable=true)
     */
    private $showsEn = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS_F", type="string", length=255, nullable=true)
     */
    private $showsF = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="RANK", type="integer", nullable=true)
     */
    private $rank = '0';


}

