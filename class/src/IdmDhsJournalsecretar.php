<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsJournalsecretar
 *
 * @ORM\Table(name="IDM_IDM_DHS_JOURNALSECRETAR", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsJournalsecretar
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SECRETAR_ID", type="integer", nullable=false)
     */
    private $secretarId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SECRETAR_TYPE", type="integer", nullable=false)
     */
    private $secretarType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SECRETAR_SHORT", type="text", nullable=false)
     */
    private $secretarShort;

    /**
     * @var string
     *
     * @ORM\Column(name="SECRETAR_INFO", type="text", nullable=false)
     */
    private $secretarInfo;


}

