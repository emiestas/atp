<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMetReceivers
 *
 * @ORM\Table(name="IDM_IDM_MET_RECEIVERS", indexes={@ORM\Index(name="MEETING_ID", columns={"QUESTION_ID"})})
 * @ORM\Entity
 */
class IdmMetReceivers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="QUESTION_ID", type="integer", nullable=false)
     */
    private $questionId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="MEETING_ID", type="integer", nullable=false)
     */
    private $meetingId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ID", type="integer", nullable=false)
     */
    private $receiverId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", nullable=true)
     */
    private $receiverInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", nullable=true)
     */
    private $receiverShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_TYPE", type="integer", nullable=false)
     */
    private $receiverType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ORGNODE_ID", type="integer", nullable=false)
     */
    private $receiverOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="STATE", type="string", length=15, nullable=true)
     */
    private $state = '';


}

