<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpRecord
 *
 * @ORM\Table(name="IDM_IDM_ATP_RECORD", indexes={@ORM\Index(name="RECORD_ID", columns={"RECORD_ID"}), @ORM\Index(name="TABLE_TO_ID", columns={"TABLE_TO_ID"}), @ORM\Index(name="RECORD_FROM_ID", columns={"RECORD_FROM_ID", "TABLE_TO_ID"}), @ORM\Index(name="TABLE_TO_ID_2", columns={"TABLE_TO_ID", "WORKER_ID"})})
 * @ORM\Entity
 */
class IdmAtpRecord
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="MAIN_ID", type="integer", nullable=false)
     */
    private $mainId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $recordId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_FROM_ID", type="integer", nullable=true)
     */
    private $recordFromId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TABLE_FROM_ID", type="integer", nullable=false)
     */
    private $tableFromId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TABLE_TO_ID", type="integer", nullable=false)
     */
    private $tableToId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_LAST", type="boolean", nullable=false)
     */
    private $isLast;

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_PAID", type="boolean", nullable=false)
     */
    private $isPaid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_STATUS", type="integer", nullable=true)
     */
    private $docStatus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PUNISHMENT_CHECKED", type="boolean", nullable=false)
     */
    private $punishmentChecked;

    /**
     * @var integer
     *
     * @ORM\Column(name="REGISTERED_TEMPLATE_ID", type="integer", nullable=false)
     */
    private $registeredTemplateId;

    /**
     * @var integer
     *
     * @ORM\Column(name="WORKER_ID", type="integer", nullable=false)
     */
    private $workerId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="IdmAtpAtprAtp", inversedBy="record")
     * @ORM\JoinTable(name="IDM_idm_atp_atpr_atp_record_xref",
     *   joinColumns={
     *     @ORM\JoinColumn(name="RECORD_ID", referencedColumnName="ID")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="ROIK", referencedColumnName="ROIK")
     *   }
     * )
     */
    private $roik;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roik = new \Doctrine\Common\Collections\ArrayCollection();
    }

}

