<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpFieldType
 *
 * @ORM\Table(name="IDM_IDM_ATP_FIELD_TYPE")
 * @ORM\Entity
 */
class IdmAtpFieldType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=100, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="RELATION", type="string", length=20, nullable=false)
     */
    private $relation;


}

