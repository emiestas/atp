<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpRecordXRelation
 *
 * @ORM\Table(name="IDM_IDM_ATP_RECORD_X_RELATION", indexes={@ORM\Index(name="RECORD_ID", columns={"RECORD_ID"}), @ORM\Index(name="RELATION_ID", columns={"RELATION_ID"})})
 * @ORM\Entity
 */
class IdmAtpRecordXRelation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCUMENT_ID", type="integer", nullable=false)
     */
    private $documentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_ID", type="integer", nullable=false)
     */
    private $recordId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RELATION_ID", type="integer", nullable=false)
     */
    private $relationId;


}

