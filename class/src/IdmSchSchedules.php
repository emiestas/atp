<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmSchSchedules
 *
 * @ORM\Table(name="IDM_IDM_SCH_SCHEDULES")
 * @ORM\Entity
 */
class IdmSchSchedules
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=500, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FROM", type="date", nullable=false)
     */
    private $from;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TILL", type="date", nullable=false)
     */
    private $till;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATED", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VALID", type="boolean", nullable=false)
     */
    private $valid = '1';


}

