<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsFiles
 *
 * @ORM\Table(name="IDM_IDM_DHS_FILES", indexes={@ORM\Index(name="DOC_ID", columns={"DOC_ID"})})
 * @ORM\Entity
 */
class IdmDhsFiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS_NEW", type="string", length=255, nullable=false)
     */
    private $showsNew;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS_OLD", type="text", length=16777215, nullable=false)
     */
    private $showsOld;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     */
    private $docId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENTTYPE", type="string", length=100, nullable=false)
     */
    private $contenttype = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENT", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="FOLDER", type="string", length=255, nullable=false)
     */
    private $folder = '';

    /**
     * @var string
     *
     * @ORM\Column(name="USER_LABEL", type="string", length=255, nullable=false)
     */
    private $userLabel;

    /**
     * @var integer
     *
     * @ORM\Column(name="VERSION", type="integer", nullable=false)
     */
    private $version;

    /**
     * @var string
     *
     * @ORM\Column(name="NEW_SHOWS", type="string", length=5000, nullable=false)
     */
    private $newShows;


}

