<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWebMain
 *
 * @ORM\Table(name="IDM_IDM_WEB_MAIN", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmWebMain
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="HEADER_ID", type="integer", nullable=false)
     */
    private $headerId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="TEMPLATE_ID", type="integer", nullable=false)
     */
    private $templateId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="ACTIVE_ID", type="integer", nullable=true)
     */
    private $activeId = '0';


}

