<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMainCrontab
 *
 * @ORM\Table(name="IDM_IDM_MAIN_CRONTAB")
 * @ORM\Entity
 */
class IdmMainCrontab
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="FILENAME", type="string", length=100, nullable=false)
     */
    private $filename = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PLAN", type="string", length=100, nullable=false)
     */
    private $plan = '';

    /**
     * @var string
     *
     * @ORM\Column(name="STATE", type="string", length=100, nullable=false)
     */
    private $state = '';

    /**
     * @var string
     *
     * @ORM\Column(name="JOBTYPE", type="string", length=100, nullable=false)
     */
    private $jobtype = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LASTDATE", type="datetime", nullable=false)
     */
    private $lastdate = '2007-01-01 00:00:00';


}

