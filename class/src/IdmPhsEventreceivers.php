<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsEventreceivers
 *
 * @ORM\Table(name="IDM_IDM_PHS_EVENTRECEIVERS", indexes={@ORM\Index(name="EVENT_ID", columns={"EVENT_ID"}), @ORM\Index(name="RECEIVER_ID", columns={"RECEIVER_ID"})})
 * @ORM\Entity
 */
class IdmPhsEventreceivers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="PROJECT_ID", type="integer", nullable=false)
     */
    private $projectId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="EVENT_ID", type="integer", nullable=false)
     */
    private $eventId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ID", type="integer", nullable=false)
     */
    private $receiverId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", length=65535, nullable=true)
     */
    private $receiverInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", length=65535, nullable=true)
     */
    private $receiverShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_TYPE", type="integer", nullable=false)
     */
    private $receiverType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ORGNODE_ID", type="integer", nullable=false)
     */
    private $receiverOrgnodeId = '0';


}

