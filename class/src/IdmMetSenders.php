<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMetSenders
 *
 * @ORM\Table(name="IDM_IDM_MET_SENDERS", indexes={@ORM\Index(name="QUESTION_ID", columns={"QUESTION_ID"})})
 * @ORM\Entity
 */
class IdmMetSenders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="QUESTION_ID", type="integer", nullable=false)
     */
    private $questionId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="MEETING_ID", type="integer", nullable=false)
     */
    private $meetingId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SENDER_ID", type="integer", nullable=false)
     */
    private $senderId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_INFO", type="text", nullable=true)
     */
    private $senderInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_SHORT", type="text", nullable=true)
     */
    private $senderShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="SENDER_TYPE", type="integer", nullable=false)
     */
    private $senderType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SENDER_ORGNODE_ID", type="integer", nullable=false)
     */
    private $senderOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="STATE", type="string", length=15, nullable=true)
     */
    private $state = '';


}

