<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpMessageReceiver
 *
 * @ORM\Table(name="IDM_IDM_ATP_MESSAGE_RECEIVER", indexes={@ORM\Index(name="IDX_C937EF8060A848F6", columns={"MESSAGE_ID"})})
 * @ORM\Entity
 */
class IdmAtpMessageReceiver
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ID", type="integer", nullable=false)
     */
    private $receiverId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_READ", type="boolean", nullable=false)
     */
    private $isRead;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RECEIVE_DATE", type="datetime", nullable=false)
     */
    private $receiveDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="READ_DATE", type="datetime", nullable=true)
     */
    private $readDate;

    /**
     * @var \IdmAtpMessage
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpMessage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="MESSAGE_ID", referencedColumnName="ID")
     * })
     */
    private $message;


}

