<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWebdavTemp
 *
 * @ORM\Table(name="IDM_IDM_WEBDAV_TEMP")
 * @ORM\Entity
 */
class IdmWebdavTemp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="FILE_ID", type="integer", nullable=false)
     */
    private $fileId;

    /**
     * @var string
     *
     * @ORM\Column(name="OLD_PATH", type="string", length=500, nullable=false)
     */
    private $oldPath;

    /**
     * @var string
     *
     * @ORM\Column(name="OLD_DATA", type="text", nullable=false)
     */
    private $oldData;

    /**
     * @var integer
     *
     * @ORM\Column(name="ITEM_ID", type="integer", nullable=false)
     */
    private $itemId;

    /**
     * @var string
     *
     * @ORM\Column(name="ITEM_TYPE", type="string", length=50, nullable=false)
     */
    private $itemType;


}

