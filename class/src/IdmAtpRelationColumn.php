<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpRelationColumn
 *
 * @ORM\Table(name="IDM_IDM_ATP_RELATION_COLUMN")
 * @ORM\Entity
 */
class IdmAtpRelationColumn
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="TABLE_ID", type="integer", nullable=false)
     */
    private $tableId;

    /**
     * @var integer
     *
     * @ORM\Column(name="COLUMN_ID", type="integer", nullable=false)
     */
    private $columnId;

    /**
     * @var integer
     *
     * @ORM\Column(name="COLUMN_PARENT_ID", type="integer", nullable=false)
     */
    private $columnParentId;


}

