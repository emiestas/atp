<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTbl48
 *
 * @ORM\Table(name="IDM_IDM_ATP_TBL_48", indexes={@ORM\Index(name="RECORD_LAST", columns={"RECORD_LAST"}), @ORM\Index(name="RECORD_MAIN_ID", columns={"RECORD_MAIN_ID"})})
 * @ORM\Entity
 */
class IdmAtpTbl48
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_MAIN_ID", type="string", length=25, nullable=false)
     */
    private $recordMainId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_VERSION", type="integer", nullable=false)
     */
    private $recordVersion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RECORD_LAST", type="boolean", nullable=false)
     */
    private $recordLast;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_0", type="date", nullable=false)
     */
    private $col0;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_1", type="text", length=255, nullable=false)
     */
    private $col1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_9", type="date", nullable=false)
     */
    private $col9;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_10", type="time", nullable=false)
     */
    private $col10;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_11", type="text", length=255, nullable=false)
     */
    private $col11;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_12", type="text", length=255, nullable=false)
     */
    private $col12;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_13", type="text", length=255, nullable=false)
     */
    private $col13;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_14", type="text", length=255, nullable=false)
     */
    private $col14;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_21", type="text", length=255, nullable=false)
     */
    private $col21;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_41", type="text", length=65535, nullable=false)
     */
    private $col41;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_42", type="text", length=65535, nullable=false)
     */
    private $col42;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_43", type="text", length=65535, nullable=false)
     */
    private $col43;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_44", type="text", length=65535, nullable=false)
     */
    private $col44;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_45", type="text", length=255, nullable=false)
     */
    private $col45;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_46", type="text", length=255, nullable=false)
     */
    private $col46;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_47", type="text", length=255, nullable=false)
     */
    private $col47;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_48", type="text", length=255, nullable=false)
     */
    private $col48;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_49", type="text", length=255, nullable=false)
     */
    private $col49;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_50", type="text", length=255, nullable=false)
     */
    private $col50;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_51", type="date", nullable=false)
     */
    private $col51;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_52", type="text", length=255, nullable=false)
     */
    private $col52;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_53", type="text", length=255, nullable=false)
     */
    private $col53;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;


}

