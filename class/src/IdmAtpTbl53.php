<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTbl53
 *
 * @ORM\Table(name="IDM_IDM_ATP_TBL_53", indexes={@ORM\Index(name="RECORD_LAST", columns={"RECORD_LAST"}), @ORM\Index(name="RECORD_MAIN_ID", columns={"RECORD_MAIN_ID"})})
 * @ORM\Entity
 */
class IdmAtpTbl53
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_MAIN_ID", type="string", length=25, nullable=false)
     */
    private $recordMainId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_VERSION", type="integer", nullable=false)
     */
    private $recordVersion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RECORD_LAST", type="boolean", nullable=false)
     */
    private $recordLast;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_1", type="date", nullable=false)
     */
    private $col1;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_2", type="text", length=255, nullable=false)
     */
    private $col2;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_10", type="text", length=255, nullable=false)
     */
    private $col10;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_20", type="date", nullable=false)
     */
    private $col20;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_21", type="text", length=255, nullable=false)
     */
    private $col21;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_22", type="text", length=255, nullable=false)
     */
    private $col22;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_23", type="text", length=255, nullable=false)
     */
    private $col23;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_24", type="text", length=255, nullable=false)
     */
    private $col24;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_25", type="text", length=65535, nullable=false)
     */
    private $col25;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_28", type="text", length=65535, nullable=false)
     */
    private $col28;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_29", type="text", length=65535, nullable=false)
     */
    private $col29;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_30", type="text", length=255, nullable=false)
     */
    private $col30;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_31", type="text", length=255, nullable=false)
     */
    private $col31;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_32", type="text", length=255, nullable=false)
     */
    private $col32;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_33", type="text", length=255, nullable=false)
     */
    private $col33;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;


}

