<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderHistory
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_HISTORY", indexes={@ORM\Index(name="_CREATE_DATE", columns={"CREATE_DATE"}), @ORM\Index(name="_ORDER_ID", columns={"ORDER_ID"}), @ORM\Index(name="_ID__DATE", columns={"ORDER_ID", "CREATE_DATE"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORDER_ID", type="integer", nullable=false)
     */
    private $orderId;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPTION", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = 'CURRENT_TIMESTAMP';


}

