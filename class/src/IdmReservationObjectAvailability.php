<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmReservationObjectAvailability
 *
 * @ORM\Table(name="IDM_IDM_RESERVATION_OBJECT_AVAILABILITY")
 * @ORM\Entity
 */
class IdmReservationObjectAvailability
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="OBJECT_ID", type="integer", nullable=false)
     */
    private $objectId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="DAY", type="boolean", nullable=false)
     */
    private $day;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TIME_FROM", type="time", nullable=false)
     */
    private $timeFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TIME_TO", type="time", nullable=false)
     */
    private $timeTo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ENABLED", type="boolean", nullable=true)
     */
    private $enabled = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DELETED", type="datetime", nullable=true)
     */
    private $deleted;


}

