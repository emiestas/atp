<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWpSoft
 *
 * @ORM\Table(name="IDM_IDM_WP_SOFT", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmWpSoft
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SOFT_DATA", type="text", nullable=false)
     */
    private $softData;

    /**
     * @var integer
     *
     * @ORM\Column(name="STYPE", type="integer", nullable=true)
     */
    private $stype = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="POSITION", type="integer", nullable=true)
     */
    private $position = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="DB_DATA", type="text", nullable=true)
     */
    private $dbData;

    /**
     * @var string
     *
     * @ORM\Column(name="DB_WHERE", type="text", nullable=true)
     */
    private $dbWhere;

    /**
     * @var string
     *
     * @ORM\Column(name="SOFT_DESC", type="text", nullable=true)
     */
    private $softDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="SOFT_DATA2", type="text", nullable=true)
     */
    private $softData2;

    /**
     * @var string
     *
     * @ORM\Column(name="VISIBLE", type="string", length=2, nullable=true)
     */
    private $visible;


}

