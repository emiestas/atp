<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWebMenu
 *
 * @ORM\Table(name="IDM_IDM_WEB_MENU", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmWebMenu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=true)
     */
    private $parentId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="URL", type="string", length=255, nullable=true)
     */
    private $url = '';

    /**
     * @var string
     *
     * @ORM\Column(name="TARGET", type="string", length=50, nullable=true)
     */
    private $target = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_VISIBLE", type="string", length=5, nullable=true)
     */
    private $isVisible = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="POSITION", type="integer", nullable=true)
     */
    private $position = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PAGE_ID", type="integer", nullable=false)
     */
    private $pageId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="LANGUAGE_ID", type="integer", nullable=false)
     */
    private $languageId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="HOME_PAGE", type="string", length=5, nullable=true)
     */
    private $homePage = '';


}

