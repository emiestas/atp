<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMetMeetings
 *
 * @ORM\Table(name="IDM_IDM_MET_MEETINGS")
 * @ORM\Entity
 */
class IdmMetMeetings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=16777215, nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="AVILIO_ID", type="string", length=64, nullable=false)
     */
    private $avilioId;

    /**
     * @var integer
     *
     * @ORM\Column(name="GROUP_ID", type="integer", nullable=false)
     */
    private $groupId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="MEET_NR", type="string", length=255, nullable=false)
     */
    private $meetNr = '';

    /**
     * @var string
     *
     * @ORM\Column(name="RECURSIVE", type="string", length=5, nullable=false)
     */
    private $recursive = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MEETING_DATE", type="datetime", nullable=false)
     */
    private $meetingDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATA_DATE", type="datetime", nullable=false)
     */
    private $dataDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="NEXT_STRING", type="string", length=255, nullable=false)
     */
    private $nextString = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATOR_ID", type="integer", nullable=true)
     */
    private $creatorId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATOR_TYPE", type="string", length=10, nullable=false)
     */
    private $creatorType = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATOR_ORGNODE_ID", type="integer", nullable=true)
     */
    private $creatorOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATOR_INFO", type="text", nullable=true)
     */
    private $creatorInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATOR_SHORT", type="text", nullable=true)
     */
    private $creatorShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="OPERATOR_ID", type="integer", nullable=false)
     */
    private $operatorId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="OPERATOR_ORGNODE_ID", type="integer", nullable=false)
     */
    private $operatorOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="OPERATOR_INFO", type="text", nullable=false)
     */
    private $operatorInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="OPERATOR_SHORT", type="text", nullable=false)
     */
    private $operatorShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS_ID", type="integer", nullable=true)
     */
    private $statusId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="HEAD_ID", type="integer", nullable=true)
     */
    private $headId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="HEAD_TYPE", type="string", length=10, nullable=false)
     */
    private $headType = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="HEAD_ORGNODE_ID", type="integer", nullable=true)
     */
    private $headOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="HEAD_INFO", type="text", nullable=true)
     */
    private $headInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="HEAD_SHORT", type="text", nullable=true)
     */
    private $headShort;

    /**
     * @var string
     *
     * @ORM\Column(name="PARTICIPANT_SHORT", type="text", nullable=false)
     */
    private $participantShort;

    /**
     * @var string
     *
     * @ORM\Column(name="PARTICIPANT_INFO", type="text", nullable=false)
     */
    private $participantInfo;

    /**
     * @var integer
     *
     * @ORM\Column(name="BEFORE_DOC_ID", type="integer", nullable=false)
     */
    private $beforeDocId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="BEFORE_STORE_DHS", type="string", length=5, nullable=false)
     */
    private $beforeStoreDhs = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="BEFORE_JOURNAL_ID", type="integer", nullable=true)
     */
    private $beforeJournalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="AFTER_DOC_ID", type="integer", nullable=false)
     */
    private $afterDocId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="AFTER_STORE_DHS", type="string", length=5, nullable=false)
     */
    private $afterStoreDhs = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="AFTER_JOURNAL_ID", type="integer", nullable=true)
     */
    private $afterJournalId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SECRET", type="string", length=5, nullable=false)
     */
    private $secret = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_PUBLIC", type="string", length=5, nullable=false)
     */
    private $isPublic = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MEET_HASH", type="string", length=32, nullable=false)
     */
    private $meetHash;

    /**
     * @var string
     *
     * @ORM\Column(name="FOREIGNMEET_ID", type="string", length=255, nullable=false)
     */
    private $foreignmeetId;

    /**
     * @var string
     *
     * @ORM\Column(name="NOW_ACTIVE", type="string", length=5, nullable=false)
     */
    private $nowActive;

    /**
     * @var string
     *
     * @ORM\Column(name="PLACE", type="string", length=255, nullable=false)
     */
    private $place = '';


}

