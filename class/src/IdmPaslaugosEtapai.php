<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosEtapai
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ETAPAI", indexes={@ORM\Index(name="_PASLAUGOS_ID", columns={"PASLAUGOS_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosEtapai
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=16777215, nullable=false)
     */
    private $shows;

    /**
     * @var integer
     *
     * @ORM\Column(name="ADMINAS_ID", type="integer", nullable=true)
     */
    private $adminasId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=true)
     */
    private $parentId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PASLAUGOS_ID", type="integer", nullable=true)
     */
    private $paslaugosId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="DIENOS_ID", type="integer", nullable=true)
     */
    private $dienosId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ETALDIENOS_ID", type="integer", nullable=false)
     */
    private $etaldienosId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="KURATORIUS_ID", type="integer", nullable=true)
     */
    private $kuratoriusId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="TEXT_DATA", type="text", length=16777215, nullable=true)
     */
    private $textData;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=20, nullable=false)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=20, nullable=false)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=false)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=false)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=false)
     */
    private $modifyIp = '';


}

