<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmSchEventUsers
 *
 * @ORM\Table(name="IDM_IDM_SCH_EVENT_USERS", indexes={@ORM\Index(name="fk_IDM_SCH_EVENT_USERS_IDM_SCH_EVENTS1_idx", columns={"EVENT_ID"})})
 * @ORM\Entity
 */
class IdmSchEventUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="EVENT_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $eventId;

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=false)
     */
    private $userId;


}

