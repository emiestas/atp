<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpAtprMessage
 *
 * @ORM\Table(name="IDM_IDM_ATP_ATPR_MESSAGE", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_30C3C261902161C9", columns={"ATP_MESSAGE_ID"})}, indexes={@ORM\Index(name="IDX_30C3C261FB33BE9C", columns={"ROIK"})})
 * @ORM\Entity
 */
class IdmAtpAtprMessage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="MESSAGE_ID", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $messageId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="BUSENOS_PASIKEITIMO_DATA", type="datetime", nullable=false)
     */
    private $busenosPasikeitimoData;

    /**
     * @var string
     *
     * @ORM\Column(name="BUSENOS_TIPAS", type="string", length=255, nullable=false)
     */
    private $busenosTipas;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_READ", type="boolean", nullable=false)
     */
    private $isRead;

    /**
     * @var \IdmAtpMessage
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpMessage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ATP_MESSAGE_ID", referencedColumnName="ID")
     * })
     */
    private $atpMessage;

    /**
     * @var \IdmAtpAtprAtp
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpAtprAtp")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ROIK", referencedColumnName="ROIK")
     * })
     */
    private $roik;


}

