<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTmplXFile
 *
 * @ORM\Table(name="IDM_IDM_ATP_TMPL_X_FILE", indexes={@ORM\Index(name="fk_IDM_ATP_TMPL_X_FILE_IDM_ATP_TABLE_TEMPLATE1_idx", columns={"TMPL_ID"})})
 * @ORM\Entity
 */
class IdmAtpTmplXFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="FILE", type="string", length=25, nullable=true)
     */
    private $file;

    /**
     * @var \IdmAtpTableTemplate
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpTableTemplate")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TMPL_ID", referencedColumnName="ID")
     * })
     */
    private $tmpl;


}

