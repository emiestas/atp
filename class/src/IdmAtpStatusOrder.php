<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpStatusOrder
 *
 * @ORM\Table(name="IDM_IDM_ATP_STATUS_ORDER", uniqueConstraints={@ORM\UniqueConstraint(name="NR_UNIQUE", columns={"NR", "STATUS_ID", "PARENT_NR"})}, indexes={@ORM\Index(name="fk_IDM_ATP_STATUS_ORDER_IDM_ATP_STATUS1_idx", columns={"STATUS_ID"}), @ORM\Index(name="fk_IDM_ATP_STATUS_ORDER_IDM_ATP_DOCUMENTS1_idx", columns={"DOCUMENT_ID"})})
 * @ORM\Entity
 */
class IdmAtpStatusOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="NR", type="integer", nullable=false)
     */
    private $nr;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_NR", type="integer", nullable=false)
     */
    private $parentNr;

    /**
     * @var \IdmAtpDocuments
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="IdmAtpDocuments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DOCUMENT_ID", referencedColumnName="ID")
     * })
     */
    private $document;

    /**
     * @var \IdmAtpStatus
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="IdmAtpStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="STATUS_ID", referencedColumnName="ID")
     * })
     */
    private $status;


}

