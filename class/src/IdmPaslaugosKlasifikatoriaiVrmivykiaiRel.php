<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosKlasifikatoriaiVrmivykiaiRel
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_KLASIFIKATORIAI_VRMIVYKIAI_REL")
 * @ORM\Entity
 */
class IdmPaslaugosKlasifikatoriaiVrmivykiaiRel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=false)
     */
    private $shows;

    /**
     * @var integer
     *
     * @ORM\Column(name="PASLAUGOS_ID", type="integer", nullable=true)
     */
    private $paslaugosId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="VRMIVYKIAI_ID", type="integer", nullable=true)
     */
    private $vrmivykiaiId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=false)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=false)
     */
    private $modifyIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=20, nullable=false)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=20, nullable=false)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=false)
     */
    private $modifyDate = '2007-01-01 00:00:00';


}

