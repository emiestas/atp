<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsMymenu
 *
 * @ORM\Table(name="IDM_IDM_DHS_MYMENU", indexes={@ORM\Index(name="EMPLOYER_ID", columns={"EMPLOYER_ID"})})
 * @ORM\Entity
 */
class IdmDhsMymenu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="MENU_ITEM", type="string", length=50, nullable=false)
     */
    private $menuItem = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="POSITION", type="integer", nullable=false)
     */
    private $position = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="EMPLOYER_ID", type="integer", nullable=false)
     */
    private $employerId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ITEM_NAME", type="string", length=100, nullable=true)
     */
    private $itemName;


}

