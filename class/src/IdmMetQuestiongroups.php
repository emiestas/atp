<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMetQuestiongroups
 *
 * @ORM\Table(name="IDM_IDM_MET_QUESTIONGROUPS")
 * @ORM\Entity
 */
class IdmMetQuestiongroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=16777215, nullable=false)
     */
    private $shows;

    /**
     * @var integer
     *
     * @ORM\Column(name="POSITION", type="integer", nullable=false)
     */
    private $position = '0';


}

