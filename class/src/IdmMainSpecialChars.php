<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMainSpecialChars
 *
 * @ORM\Table(name="IDM_IDM_MAIN_SPECIAL_CHARS")
 * @ORM\Entity
 */
class IdmMainSpecialChars
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="CHARACTER", type="string", length=3, nullable=false)
     */
    private $character;

    /**
     * @var string
     *
     * @ORM\Column(name="HEX", type="string", length=6, nullable=false)
     */
    private $hex;

    /**
     * @var string
     *
     * @ORM\Column(name="NUMERIC", type="string", length=7, nullable=false)
     */
    private $numeric;

    /**
     * @var string
     *
     * @ORM\Column(name="HTML", type="string", length=9, nullable=false)
     */
    private $html;

    /**
     * @var string
     *
     * @ORM\Column(name="ESCAPE", type="string", length=6, nullable=false)
     */
    private $escape;

    /**
     * @var string
     *
     * @ORM\Column(name="URI", type="string", length=9, nullable=false)
     */
    private $uri;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPTION", type="string", length=150, nullable=false)
     */
    private $description;


}

