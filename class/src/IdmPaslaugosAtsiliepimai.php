<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosAtsiliepimai
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ATSILIEPIMAI")
 * @ORM\Entity
 */
class IdmPaslaugosAtsiliepimai
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=true)
     */
    private $shows;

    /**
     * @var integer
     *
     * @ORM\Column(name="PASLAUGOS_ID", type="integer", nullable=false)
     */
    private $paslaugosId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ATS1_ID", type="integer", nullable=false)
     */
    private $ats1Id = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ATS2_ID", type="integer", nullable=false)
     */
    private $ats2Id = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ATS3_ID", type="integer", nullable=false)
     */
    private $ats3Id = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=20, nullable=true)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=20, nullable=true)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=true)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=true)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=true)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=true)
     */
    private $modifyIp = '';


}

