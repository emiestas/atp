<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosAvilys
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_AVILYS", uniqueConstraints={@ORM\UniqueConstraint(name="_SERVICE_ID", columns={"SERVICE_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosAvilys
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="WEB_AVILYS_ID", type="string", length=256, nullable=true)
     */
    private $webAvilysId;

    /**
     * @var string
     *
     * @ORM\Column(name="PVS_AVILYS_ID", type="string", length=256, nullable=true)
     */
    private $pvsAvilysId;

    /**
     * @var string
     *
     * @ORM\Column(name="WEB_JOURNAL_ID", type="string", length=256, nullable=true)
     */
    private $webJournalId;

    /**
     * @var string
     *
     * @ORM\Column(name="PVS_JOURNAL_ID", type="string", length=256, nullable=true)
     */
    private $pvsJournalId;

    /**
     * @var string
     *
     * @ORM\Column(name="WEB_CASE_ID", type="string", length=256, nullable=true)
     */
    private $webCaseId;

    /**
     * @var string
     *
     * @ORM\Column(name="PVS_CASE_ID", type="string", length=256, nullable=true)
     */
    private $pvsCaseId;

    /**
     * @var string
     *
     * @ORM\Column(name="WEB_DOCUMENT_ID", type="string", length=256, nullable=true)
     */
    private $webDocumentId;

    /**
     * @var string
     *
     * @ORM\Column(name="PVS_DOCUMENT_ID", type="string", length=256, nullable=true)
     */
    private $pvsDocumentId;

    /**
     * @var string
     *
     * @ORM\Column(name="TASK_ID", type="string", length=256, nullable=true)
     */
    private $taskId;

    /**
     * @var \IdmPaslaugos
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SERVICE_ID", referencedColumnName="ID")
     * })
     */
    private $service;


}

