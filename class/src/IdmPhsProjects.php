<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsProjects
 *
 * @ORM\Table(name="IDM_IDM_PHS_PROJECTS", indexes={@ORM\Index(name="STATUS_ID", columns={"STATUS_ID"}), @ORM\Index(name="CREATE_DATE", columns={"CREATE_DATE"})})
 * @ORM\Entity
 */
class IdmPhsProjects
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PROGRAM_ID", type="integer", nullable=false)
     */
    private $programId;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PROJECT_DESC", type="text", length=65535, nullable=true)
     */
    private $projectDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="KEYWORDS", type="text", length=65535, nullable=false)
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="PAUSE_DESC", type="text", length=65535, nullable=false)
     */
    private $pauseDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="PROJECT_NOTES", type="text", length=65535, nullable=false)
     */
    private $projectNotes;

    /**
     * @var string
     *
     * @ORM\Column(name="PROLONG_DESC", type="text", length=65535, nullable=true)
     */
    private $prolongDesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="VERSION", type="integer", nullable=false)
     */
    private $version = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SECRET", type="string", length=1, nullable=false)
     */
    private $secret = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="PROGRESS", type="integer", nullable=false)
     */
    private $progress = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ISLATE", type="string", length=5, nullable=true)
     */
    private $islate;

    /**
     * @var string
     *
     * @ORM\Column(name="WASLATE", type="string", length=1, nullable=true)
     */
    private $waslate = '';

    /**
     * @var string
     *
     * @ORM\Column(name="NOEXPIREDATE", type="string", length=1, nullable=true)
     */
    private $noexpiredate = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXPIRE_DATE", type="datetime", nullable=true)
     */
    private $expireDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="START_DATE", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CHECK_DATE", type="datetime", nullable=false)
     */
    private $checkDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ACCOMPLISH_DATE", type="datetime", nullable=true)
     */
    private $accomplishDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="PARTICIPANT_INFO", type="text", length=65535, nullable=true)
     */
    private $participantInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="PARTICIPANT_SHORT", type="text", length=65535, nullable=true)
     */
    private $participantShort;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_INFO", type="text", length=65535, nullable=true)
     */
    private $senderInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_SHORT", type="text", length=65535, nullable=true)
     */
    private $senderShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS_ID", type="integer", nullable=false)
     */
    private $statusId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PRIORITY_ID", type="integer", nullable=false)
     */
    private $priorityId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATOR_ID", type="integer", nullable=true)
     */
    private $creatorId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATOR_ORGNODE_ID", type="integer", nullable=true)
     */
    private $creatorOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATOR_INFO", type="text", length=65535, nullable=true)
     */
    private $creatorInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATOR_SHORT", type="text", length=65535, nullable=true)
     */
    private $creatorShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="HEAD_ID", type="integer", nullable=false)
     */
    private $headId;

    /**
     * @var integer
     *
     * @ORM\Column(name="HEAD_ORGNODE_ID", type="integer", nullable=false)
     */
    private $headOrgnodeId;

    /**
     * @var string
     *
     * @ORM\Column(name="HEAD_INFO", type="text", length=65535, nullable=false)
     */
    private $headInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="HEAD_SHORT", type="text", length=65535, nullable=false)
     */
    private $headShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTROL_TYPE", type="integer", nullable=true)
     */
    private $controlType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CURATOR_ID", type="integer", nullable=true)
     */
    private $curatorId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CURATOR_ORGNODE_ID", type="integer", nullable=true)
     */
    private $curatorOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CURATOR_INFO", type="text", length=65535, nullable=true)
     */
    private $curatorInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="CURATOR_SHORT", type="text", length=65535, nullable=true)
     */
    private $curatorShort;

    /**
     * @var string
     *
     * @ORM\Column(name="PRICE", type="decimal", precision=9, scale=2, nullable=false)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="FINANCE_PERCENT", type="string", length=100, nullable=false)
     */
    private $financePercent;

    /**
     * @var string
     *
     * @ORM\Column(name="ARCHIVE", type="string", length=2, nullable=false)
     */
    private $archive = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ISPUBLIC", type="string", length=2, nullable=false)
     */
    private $ispublic;

    /**
     * @var string
     *
     * @ORM\Column(name="TMPFIELD", type="string", length=255, nullable=false)
     */
    private $tmpfield = '';


}

