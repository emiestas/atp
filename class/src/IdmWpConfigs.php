<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWpConfigs
 *
 * @ORM\Table(name="IDM_IDM_WP_CONFIGS")
 * @ORM\Entity
 */
class IdmWpConfigs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SYSTEM", type="string", length=50, nullable=false)
     */
    private $system;

    /**
     * @var string
     *
     * @ORM\Column(name="PREFIX", type="string", length=255, nullable=false)
     */
    private $prefix;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="VALUE", type="text", length=65535, nullable=false)
     */
    private $value;


}

