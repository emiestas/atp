<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTbl55
 *
 * @ORM\Table(name="IDM_IDM_ATP_TBL_55", indexes={@ORM\Index(name="RECORD_LAST", columns={"RECORD_LAST"}), @ORM\Index(name="RECORD_MAIN_ID", columns={"RECORD_MAIN_ID"})})
 * @ORM\Entity
 */
class IdmAtpTbl55
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_MAIN_ID", type="string", length=25, nullable=false)
     */
    private $recordMainId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_VERSION", type="integer", nullable=false)
     */
    private $recordVersion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RECORD_LAST", type="boolean", nullable=false)
     */
    private $recordLast;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_9", type="text", length=255, nullable=false)
     */
    private $col9;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_12", type="text", length=255, nullable=false)
     */
    private $col12;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_13", type="date", nullable=false)
     */
    private $col13;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_14", type="time", nullable=false)
     */
    private $col14;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_15", type="text", length=255, nullable=false)
     */
    private $col15;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_16", type="text", length=255, nullable=false)
     */
    private $col16;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_17", type="date", nullable=false)
     */
    private $col17;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_28", type="text", length=255, nullable=false)
     */
    private $col28;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_31", type="text", length=65535, nullable=false)
     */
    private $col31;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_32", type="text", length=65535, nullable=false)
     */
    private $col32;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_34", type="text", length=255, nullable=false)
     */
    private $col34;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_35", type="text", length=255, nullable=false)
     */
    private $col35;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_36", type="text", length=255, nullable=false)
     */
    private $col36;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_37", type="text", length=65535, nullable=false)
     */
    private $col37;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_38", type="text", length=255, nullable=false)
     */
    private $col38;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_39", type="text", length=255, nullable=false)
     */
    private $col39;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;


}

