<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsJournalvisas
 *
 * @ORM\Table(name="IDM_IDM_DHS_JOURNALVISAS", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsJournalvisas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="VISAS_ID", type="integer", nullable=false)
     */
    private $visasId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="VISAS_TYPE", type="integer", nullable=false)
     */
    private $visasType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="VISAS_SHORT", type="text", nullable=false)
     */
    private $visasShort;

    /**
     * @var string
     *
     * @ORM\Column(name="VISAS_INFO", type="text", nullable=false)
     */
    private $visasInfo;


}

