<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsJournalsignature
 *
 * @ORM\Table(name="IDM_IDM_DHS_JOURNALSIGNATURE", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsJournalsignature
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SIGNATURE_ID", type="integer", nullable=false)
     */
    private $signatureId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SIGNATURE_TYPE", type="integer", nullable=false)
     */
    private $signatureType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SIGNATURE_SHORT", type="text", nullable=false)
     */
    private $signatureShort;

    /**
     * @var string
     *
     * @ORM\Column(name="SIGNATURE_INFO", type="text", nullable=false)
     */
    private $signatureInfo;


}

