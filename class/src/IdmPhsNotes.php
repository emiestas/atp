<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsNotes
 *
 * @ORM\Table(name="IDM_IDM_PHS_NOTES", indexes={@ORM\Index(name="PROJECT_ID", columns={"PROJECT_ID"}), @ORM\Index(name="TASK_ID", columns={"TASK_ID"}), @ORM\Index(name="STAGE_ID", columns={"STAGE_ID"})})
 * @ORM\Entity
 */
class IdmPhsNotes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=false)
     */
    private $parentId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SYS_ID", type="string", length=255, nullable=false)
     */
    private $sysId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE_ID", type="integer", nullable=false)
     */
    private $typeId;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=65535, nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="NOTE_DATA", type="text", length=65535, nullable=false)
     */
    private $noteData;

    /**
     * @var string
     *
     * @ORM\Column(name="NOTE_SEARCH", type="text", length=65535, nullable=false)
     */
    private $noteSearch;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_INFO", type="text", length=65535, nullable=true)
     */
    private $senderInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_SHORT", type="text", length=65535, nullable=true)
     */
    private $senderShort;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", length=65535, nullable=true)
     */
    private $receiverInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", length=65535, nullable=true)
     */
    private $receiverShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="PROJECT_ID", type="integer", nullable=true)
     */
    private $projectId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="STAGE_ID", type="integer", nullable=true)
     */
    private $stageId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="TASK_ID", type="integer", nullable=true)
     */
    private $taskId = '0';


}

