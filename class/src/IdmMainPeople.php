<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMainPeople
 *
 * @ORM\Table(name="IDM_IDM_MAIN_PEOPLE", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmMainPeople
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=true)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="FIRST_NAME", type="string", length=100, nullable=true)
     */
    private $firstName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="LAST_NAME", type="string", length=100, nullable=true)
     */
    private $lastName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=50, nullable=true)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=50, nullable=true)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=true)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=true)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=true)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=true)
     */
    private $modifyIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="EMAIL", type="string", length=100, nullable=true)
     */
    private $email = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PHONE_MOBILE", type="string", length=100, nullable=true)
     */
    private $phoneMobile;

    /**
     * @var string
     *
     * @ORM\Column(name="PHONE_SHORT", type="string", length=100, nullable=true)
     */
    private $phoneShort = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PHONE_FULL", type="string", length=150, nullable=true)
     */
    private $phoneFull = '';

    /**
     * @var string
     *
     * @ORM\Column(name="LOGIN", type="string", length=50, nullable=true)
     */
    private $login = '';

    /**
     * @var string
     *
     * @ORM\Column(name="GENDER", type="string", length=1, nullable=true)
     */
    private $gender = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CODE", type="string", length=11, nullable=false)
     */
    private $code = '12345678901';

    /**
     * @var string
     *
     * @ORM\Column(name="PHOTO", type="string", length=255, nullable=false)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="SALARY", type="decimal", precision=9, scale=2, nullable=false)
     */
    private $salary;


}

