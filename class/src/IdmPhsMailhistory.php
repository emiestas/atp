<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsMailhistory
 *
 * @ORM\Table(name="IDM_IDM_PHS_MAILHISTORY", indexes={@ORM\Index(name="REGPATH", columns={"REGPATH"})})
 * @ORM\Entity
 */
class IdmPhsMailhistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="REGPATH", type="string", length=255, nullable=false)
     */
    private $regpath;

    /**
     * @var string
     *
     * @ORM\Column(name="REGTYPE", type="string", length=255, nullable=false)
     */
    private $regtype;

    /**
     * @var string
     *
     * @ORM\Column(name="REGSTATE", type="string", length=2, nullable=false)
     */
    private $regstate;

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCHPART", type="string", length=255, nullable=false)
     */
    private $searchpart;

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCHEMAIL", type="string", length=255, nullable=false)
     */
    private $searchemail;

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCHMSG", type="string", length=255, nullable=false)
     */
    private $searchmsg;

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCHSUBJECT", type="string", length=255, nullable=false)
     */
    private $searchsubject;

    /**
     * @var integer
     *
     * @ORM\Column(name="MAILBOX_ID", type="integer", nullable=false)
     */
    private $mailboxId;

    /**
     * @var integer
     *
     * @ORM\Column(name="FOLDER_ID", type="integer", nullable=false)
     */
    private $folderId;


}

