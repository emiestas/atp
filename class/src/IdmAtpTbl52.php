<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTbl52
 *
 * @ORM\Table(name="IDM_IDM_ATP_TBL_52", indexes={@ORM\Index(name="RECORD_LAST", columns={"RECORD_LAST"}), @ORM\Index(name="RECORD_MAIN_ID", columns={"RECORD_MAIN_ID"})})
 * @ORM\Entity
 */
class IdmAtpTbl52
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_MAIN_ID", type="string", length=25, nullable=false)
     */
    private $recordMainId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_VERSION", type="integer", nullable=false)
     */
    private $recordVersion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RECORD_LAST", type="boolean", nullable=false)
     */
    private $recordLast;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_11", type="date", nullable=true)
     */
    private $col11;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_12", type="time", nullable=true)
     */
    private $col12;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_13", type="text", length=255, nullable=true)
     */
    private $col13;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_30", type="text", length=65535, nullable=true)
     */
    private $col30;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_31", type="text", length=65535, nullable=true)
     */
    private $col31;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_36", type="text", length=255, nullable=true)
     */
    private $col36;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_49", type="text", length=255, nullable=true)
     */
    private $col49;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_50", type="text", length=255, nullable=true)
     */
    private $col50;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_51", type="text", length=255, nullable=true)
     */
    private $col51;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_55", type="text", length=255, nullable=true)
     */
    private $col55;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_56", type="text", length=255, nullable=true)
     */
    private $col56;

    /**
     * @var integer
     *
     * @ORM\Column(name="COL_57", type="integer", nullable=true)
     */
    private $col57;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_58", type="text", length=255, nullable=false)
     */
    private $col58;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_59", type="text", length=255, nullable=false)
     */
    private $col59;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_60", type="text", length=255, nullable=false)
     */
    private $col60;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_61", type="text", length=255, nullable=false)
     */
    private $col61;

    /**
     * @var integer
     *
     * @ORM\Column(name="COL_62", type="integer", nullable=false)
     */
    private $col62;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_63", type="text", length=255, nullable=false)
     */
    private $col63;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_64", type="text", length=255, nullable=false)
     */
    private $col64;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_65", type="text", length=255, nullable=false)
     */
    private $col65;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_66", type="text", length=255, nullable=false)
     */
    private $col66;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;


}

