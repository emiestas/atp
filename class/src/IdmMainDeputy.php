<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMainDeputy
 *
 * @ORM\Table(name="IDM_IDM_MAIN_DEPUTY")
 * @ORM\Entity
 */
class IdmMainDeputy
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="DEPUTY_ID", type="integer", nullable=false)
     */
    private $deputyId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="FIRST_NAME", type="string", length=255, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="LAST_NAME", type="string", length=255, nullable=false)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="STRUCTURE", type="string", length=255, nullable=false)
     */
    private $structure;

    /**
     * @var string
     *
     * @ORM\Column(name="OFFICE", type="string", length=255, nullable=false)
     */
    private $office;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FROM_DATE", type="datetime", nullable=false)
     */
    private $fromDate;

    /**
     * @var string
     *
     * @ORM\Column(name="XML_OFFICE", type="string", length=255, nullable=false)
     */
    private $xmlOffice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TILL_DATE", type="datetime", nullable=false)
     */
    private $tillDate;

    /**
     * @var string
     *
     * @ORM\Column(name="DHS_BUTTONS", type="text", length=16777215, nullable=false)
     */
    private $dhsButtons;

    /**
     * @var string
     *
     * @ORM\Column(name="VISIBLE", type="string", length=5, nullable=false)
     */
    private $visible;


}

