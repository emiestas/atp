<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTableTemplate
 *
 * @ORM\Table(name="IDM_IDM_ATP_TABLE_TEMPLATE", indexes={@ORM\Index(name="fk_IDM_ATP_TABLE_TEMPLATE_IDM_ATP_DOCUMENTS1_idx", columns={"TABLE_ID"})})
 * @ORM\Entity
 */
class IdmAtpTableTemplate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="SERVICE_ID", type="integer", nullable=true)
     */
    private $serviceId;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=255, nullable=false)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="HANDLER", type="string", length=25, nullable=false)
     */
    private $handler;

    /**
     * @var \IdmAtpDocuments
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpDocuments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TABLE_ID", referencedColumnName="ID")
     * })
     */
    private $table;


}

