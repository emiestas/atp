<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTmplServiceAvilysFields
 *
 * @ORM\Table(name="IDM_IDM_ATP_TMPL_SERVICE_AVILYS_FIELDS")
 * @ORM\Entity
 */
class IdmAtpTmplServiceAvilysFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=50, nullable=false)
     */
    private $name;


}

