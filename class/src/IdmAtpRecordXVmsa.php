<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpRecordXVmsa
 *
 * @ORM\Table(name="IDM_IDM_ATP_RECORD_X_VMSA", uniqueConstraints={@ORM\UniqueConstraint(name="PAZEIDIMAS_ID", columns={"PAZEIDIMAS_ID", "PROTOKOLAS_ID", "NUTARIMAS_ID"})}, indexes={@ORM\Index(name="RECORD_ID", columns={"RECORD_ID"})})
 * @ORM\Entity
 */
class IdmAtpRecordXVmsa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PAZEIDIMAS_ID", type="integer", nullable=false)
     */
    private $pazeidimasId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PROTOKOLAS_ID", type="integer", nullable=false)
     */
    private $protokolasId;

    /**
     * @var integer
     *
     * @ORM\Column(name="NUTARIMAS_ID", type="integer", nullable=false)
     */
    private $nutarimasId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATA", type="datetime", nullable=true)
     */
    private $data;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_ID", type="integer", nullable=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_TYPE", type="string", length=50, nullable=true)
     */
    private $recordType;


}

