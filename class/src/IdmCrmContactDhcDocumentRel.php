<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmCrmContactDhcDocumentRel
 *
 * @ORM\Table(name="IDM_IDM_CRM_CONTACT_DHC_DOCUMENT_REL")
 * @ORM\Entity
 */
class IdmCrmContactDhcDocumentRel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="CONTACT_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $contactId;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $docId;


}

