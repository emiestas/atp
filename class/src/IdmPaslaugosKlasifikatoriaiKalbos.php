<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosKlasifikatoriaiKalbos
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_KLASIFIKATORIAI_KALBOS")
 * @ORM\Entity
 */
class IdmPaslaugosKlasifikatoriaiKalbos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="FOREIGN_ID", type="string", length=255, nullable=false)
     */
    private $foreignId;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=65535, nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="ORIGINAL_NAME", type="string", length=255, nullable=false)
     */
    private $originalName;


}

