<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosViisp
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_VIISP", indexes={@ORM\Index(name="_SERVICE_ID", columns={"SERVICE_ID"}), @ORM\Index(name="_PROVIDER_CODE", columns={"PROVIDER_CODE"}), @ORM\Index(name="_VIISP_SERVICE", columns={"SERVICE_CODE", "SERVICE_SUBTYPE", "SERVICE_SORT", "SERVICE_AUXTYPE"})})
 * @ORM\Entity
 */
class IdmPaslaugosViisp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="SERVICE_ID", type="integer", nullable=false)
     */
    private $serviceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PROVIDER_CODE", type="integer", nullable=true)
     */
    private $providerCode;

    /**
     * @var string
     *
     * @ORM\Column(name="PROVIDER_NAME", type="string", length=256, nullable=true)
     */
    private $providerName;

    /**
     * @var string
     *
     * @ORM\Column(name="SERVICE_CODE", type="string", length=256, nullable=false)
     */
    private $serviceCode;

    /**
     * @var string
     *
     * @ORM\Column(name="SERVICE_SUBTYPE", type="string", length=256, nullable=false)
     */
    private $serviceSubtype;

    /**
     * @var string
     *
     * @ORM\Column(name="SERVICE_SORT", type="string", length=256, nullable=false)
     */
    private $serviceSort;

    /**
     * @var string
     *
     * @ORM\Column(name="SERVICE_AUXTYPE", type="string", length=256, nullable=false)
     */
    private $serviceAuxtype;


}

