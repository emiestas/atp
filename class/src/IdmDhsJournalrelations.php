<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsJournalrelations
 *
 * @ORM\Table(name="IDM_IDM_DHS_JOURNALRELATIONS", indexes={@ORM\Index(name="SEND_JOURNAL_ID", columns={"SEND_JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsJournalrelations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var integer
     *
     * @ORM\Column(name="SEND_JOURNAL_ID", type="integer", nullable=false)
     */
    private $sendJournalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVE_JOURNAL_ID", type="integer", nullable=false)
     */
    private $receiveJournalId = '0';


}

