<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugos
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS", indexes={@ORM\Index(name="TEMOS_ID", columns={"TEMOS_ID"}), @ORM\Index(name="IS_RODOMA", columns={"IS_RODOMA"}), @ORM\Index(name="VADOVAS_ID", columns={"VADOVAS_ID"}), @ORM\Index(name="ADMINAS_ID", columns={"ADMINAS_ID"}), @ORM\Index(name="ISTAIGOS_ID", columns={"ISTAIGOS_ID"}), @ORM\Index(name="LIPVADOVAS_ID", columns={"LIPVADOVAS_ID"}), @ORM\Index(name="DIENOS_ID", columns={"DIENOS_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORDER", type="integer", nullable=false)
     */
    private $order;

    /**
     * @var string
     *
     * @ORM\Column(name="PRASYMAS_ID", type="text", length=65535, nullable=true)
     */
    private $prasymasId;

    /**
     * @var integer
     *
     * @ORM\Column(name="WWWDIENOS_ID", type="integer", nullable=false)
     */
    private $wwwdienosId;

    /**
     * @var integer
     *
     * @ORM\Column(name="GRIZFORMA_ID", type="integer", nullable=true)
     */
    private $grizformaId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="RODIKLIAI", type="text", nullable=true)
     */
    private $rodikliai;

    /**
     * @var string
     *
     * @ORM\Column(name="UZPILDYTA", type="string", length=100, nullable=true)
     */
    private $uzpildyta = '';

    /**
     * @var string
     *
     * @ORM\Column(name="FORMOS_ID", type="text", nullable=true)
     */
    private $formosId;

    /**
     * @var string
     *
     * @ORM\Column(name="IDOCFORMOS_ID", type="text", nullable=true)
     */
    private $idocformosId;

    /**
     * @var integer
     *
     * @ORM\Column(name="KURATORIUS_ID", type="integer", nullable=true)
     */
    private $kuratoriusId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CURATOR_ID", type="integer", nullable=false)
     */
    private $curatorId;

    /**
     * @var integer
     *
     * @ORM\Column(name="KONTROLE_ID", type="integer", nullable=false)
     */
    private $kontroleId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ETAPAI_ID", type="integer", nullable=false)
     */
    private $etapaiId;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=true)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="SHORT", type="text", nullable=true)
     */
    private $short;

    /**
     * @var integer
     *
     * @ORM\Column(name="TEMOS_ID", type="integer", nullable=true)
     */
    private $temosId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PAY_FORMULA", type="text", length=16777215, nullable=false)
     */
    private $payFormula;

    /**
     * @var string
     *
     * @ORM\Column(name="IVYKIAI_ID", type="text", nullable=true)
     */
    private $ivykiaiId;

    /**
     * @var string
     *
     * @ORM\Column(name="TEIKVIETOS_ID", type="text", nullable=true)
     */
    private $teikvietosId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ISTAIGOS_ID", type="integer", nullable=true)
     */
    private $istaigosId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="VADOVAS_ID", type="integer", nullable=true)
     */
    private $vadovasId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ADMINAS_ID", type="integer", nullable=true)
     */
    private $adminasId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="DIENOS_ID", type="integer", nullable=true)
     */
    private $dienosId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ETALDIENOS_ID", type="integer", nullable=false)
     */
    private $etaldienosId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="INTERESANTAI_ID", type="integer", nullable=true)
     */
    private $interesantaiId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PASIS_INTERESANTAI_ID", type="string", length=255, nullable=true)
     */
    private $pasisInteresantaiId;

    /**
     * @var string
     *
     * @ORM\Column(name="VRMTEMOS_ID", type="string", length=255, nullable=true)
     */
    private $vrmtemosId;

    /**
     * @var string
     *
     * @ORM\Column(name="VRMIVYKIAI_ID", type="string", length=255, nullable=true)
     */
    private $vrmivykiaiId;

    /**
     * @var string
     *
     * @ORM\Column(name="VRMTIPAS_ID", type="string", length=255, nullable=true)
     */
    private $vrmtipasId;

    /**
     * @var string
     *
     * @ORM\Column(name="IRESFORMOS_ID", type="string", length=255, nullable=true)
     */
    private $iresformosId;

    /**
     * @var string
     *
     * @ORM\Column(name="LOGIN_ID", type="text", length=16777215, nullable=false)
     */
    private $loginId;

    /**
     * @var string
     *
     * @ORM\Column(name="DOCTEISAKTAI_ID", type="text", length=16777215, nullable=false)
     */
    private $docteisaktaiId;

    /**
     * @var string
     *
     * @ORM\Column(name="REZULTATAI_ID", type="text", length=16777215, nullable=false)
     */
    private $rezultataiId;

    /**
     * @var string
     *
     * @ORM\Column(name="ELPROCPRIEDAS_ID", type="string", length=255, nullable=true)
     */
    private $elprocpriedasId;

    /**
     * @var string
     *
     * @ORM\Column(name="ES_REIKALAVIMAI_ID", type="string", length=255, nullable=true)
     */
    private $esReikalavimaiId;

    /**
     * @var string
     *
     * @ORM\Column(name="PASLAUGA_RES_ID", type="string", length=255, nullable=true)
     */
    private $paslaugaResId;

    /**
     * @var string
     *
     * @ORM\Column(name="REKVIZITAI_ID", type="string", length=255, nullable=true)
     */
    private $rekvizitaiId;

    /**
     * @var string
     *
     * @ORM\Column(name="ELKAINA_ID", type="string", length=255, nullable=true)
     */
    private $elkainaId;

    /**
     * @var string
     *
     * @ORM\Column(name="KAINA_ID", type="string", length=255, nullable=true)
     */
    private $kainaId;

    /**
     * @var string
     *
     * @ORM\Column(name="KAINININKAS_ID", type="string", length=255, nullable=true)
     */
    private $kainininkasId;

    /**
     * @var string
     *
     * @ORM\Column(name="EBANK_ID", type="string", length=255, nullable=true)
     */
    private $ebankId;

    /**
     * @var string
     *
     * @ORM\Column(name="EPOST_ID", type="string", length=255, nullable=true)
     */
    private $epostId;

    /**
     * @var string
     *
     * @ORM\Column(name="REGISTRAI_ID", type="string", length=255, nullable=true)
     */
    private $registraiId;

    /**
     * @var string
     *
     * @ORM\Column(name="EPASLAUGOS_URL", type="string", length=255, nullable=true)
     */
    private $epaslaugosUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="ELYGIS_ID", type="string", length=255, nullable=true)
     */
    private $elygisId;

    /**
     * @var string
     *
     * @ORM\Column(name="PASIS_ISTAIGOS_ID", type="string", length=255, nullable=true)
     */
    private $pasisIstaigosId;

    /**
     * @var string
     *
     * @ORM\Column(name="CONTACT_INFO", type="text", length=16777215, nullable=true)
     */
    private $contactInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="INIT_FORM", type="text", length=16777215, nullable=true)
     */
    private $initForm;

    /**
     * @var string
     *
     * @ORM\Column(name="PROCESAS_DESC_NOE", type="text", length=16777215, nullable=true)
     */
    private $procesasDescNoe;

    /**
     * @var string
     *
     * @ORM\Column(name="VIISP_CONTRACT", type="string", length=255, nullable=true)
     */
    private $viispContract;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_ESLICENCE", type="string", length=255, nullable=true)
     */
    private $isEslicence;

    /**
     * @var string
     *
     * @ORM\Column(name="PASLAUGA_RESULT", type="text", length=16777215, nullable=true)
     */
    private $paslaugaResult;

    /**
     * @var string
     *
     * @ORM\Column(name="PASLAUGA_RES_KITA", type="text", length=16777215, nullable=true)
     */
    private $paslaugaResKita;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_FEE", type="string", length=255, nullable=true)
     */
    private $isFee;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_PAID_IN_PLACE", type="string", length=255, nullable=true)
     */
    private $isPaidInPlace;

    /**
     * @var string
     *
     * @ORM\Column(name="GAVEJO_KODAS", type="string", length=255, nullable=true)
     */
    private $gavejoKodas;

    /**
     * @var string
     *
     * @ORM\Column(name="RAKTINIAI_ZODZIAI", type="text", length=16777215, nullable=true)
     */
    private $raktiniaiZodziai;

    /**
     * @var string
     *
     * @ORM\Column(name="PSL_URL", type="text", length=16777215, nullable=true)
     */
    private $pslUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="TAPATYBE", type="string", length=255, nullable=true)
     */
    private $tapatybe;

    /**
     * @var string
     *
     * @ORM\Column(name="LOGIN_URL", type="text", length=16777215, nullable=true)
     */
    private $loginUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="INSTRUKCIJA", type="text", length=16777215, nullable=true)
     */
    private $instrukcija;

    /**
     * @var string
     *
     * @ORM\Column(name="INSTRUKCIJA_URL", type="string", length=255, nullable=true)
     */
    private $instrukcijaUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="REIKALAVIMAI_KITA", type="text", length=16777215, nullable=true)
     */
    private $reikalavimaiKita;

    /**
     * @var string
     *
     * @ORM\Column(name="PASIS_PRASYMAS_ID", type="string", length=255, nullable=true)
     */
    private $pasisPrasymasId;

    /**
     * @var string
     *
     * @ORM\Column(name="PASIS_VALD_DESC", type="text", length=16777215, nullable=true)
     */
    private $pasisValdDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="PASIS_GAVEJAS", type="string", length=255, nullable=true)
     */
    private $pasisGavejas;

    /**
     * @var string
     *
     * @ORM\Column(name="PROCESAS_DESC_E", type="text", length=16777215, nullable=true)
     */
    private $procesasDescE;

    /**
     * @var string
     *
     * @ORM\Column(name="INSTRUKCIJOS_ATT_ID", type="string", length=255, nullable=true)
     */
    private $instrukcijosAttId;

    /**
     * @var string
     *
     * @ORM\Column(name="REIKALAVIMAI_ID", type="string", length=255, nullable=true)
     */
    private $reikalavimaiId;

    /**
     * @var string
     *
     * @ORM\Column(name="NEELPROCPRIEDAS_ID", type="string", length=255, nullable=true)
     */
    private $neelprocpriedasId;

    /**
     * @var string
     *
     * @ORM\Column(name="SUSIJE_PASLAUGOS_ID", type="text", nullable=true)
     */
    private $susijePaslaugosId;

    /**
     * @var string
     *
     * @ORM\Column(name="BYLA_AVILYS_ID", type="text", nullable=true)
     */
    private $bylaAvilysId;

    /**
     * @var string
     *
     * @ORM\Column(name="TASK_AVILYS_ID", type="string", length=64, nullable=false)
     */
    private $taskAvilysId;

    /**
     * @var string
     *
     * @ORM\Column(name="BYLA2_AVILYS_ID", type="string", length=64, nullable=false)
     */
    private $byla2AvilysId;

    /**
     * @var string
     *
     * @ORM\Column(name="PASLAUGA_DESC", type="text", nullable=true)
     */
    private $paslaugaDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="PRIEDAI_ID", type="text", nullable=true)
     */
    private $priedaiId;

    /**
     * @var string
     *
     * @ORM\Column(name="VYKD_DESC", type="text", nullable=true)
     */
    private $vykdDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="TEISAKTAI_ID", type="text", nullable=true)
     */
    private $teisaktaiId;

    /**
     * @var string
     *
     * @ORM\Column(name="OTHER_URL", type="string", length=255, nullable=false)
     */
    private $otherUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="VALD_DESC", type="text", nullable=true)
     */
    private $valdDesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="ATNAUJINIMAS_ID", type="integer", nullable=true)
     */
    private $atnaujinimasId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PASTABOS", type="text", nullable=true)
     */
    private $pastabos;

    /**
     * @var string
     *
     * @ORM\Column(name="INFORMACIJA", type="text", nullable=false)
     */
    private $informacija;

    /**
     * @var string
     *
     * @ORM\Column(name="BANKAI", type="string", length=255, nullable=true)
     */
    private $bankai = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SASKAITA", type="string", length=50, nullable=true)
     */
    private $saskaita = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IMOK_KODAS", type="string", length=50, nullable=true)
     */
    private $imokKodas = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SUMA_DESC", type="text", nullable=true)
     */
    private $sumaDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=20, nullable=true)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=20, nullable=true)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=true)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=true)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=true)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=true)
     */
    private $modifyIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="VALD_DESC2", type="text", nullable=true)
     */
    private $valdDesc2;

    /**
     * @var string
     *
     * @ORM\Column(name="AVILYS_ID", type="string", length=100, nullable=true)
     */
    private $avilysId;

    /**
     * @var string
     *
     * @ORM\Column(name="AVILYS2_ID", type="string", length=64, nullable=false)
     */
    private $avilys2Id;

    /**
     * @var integer
     *
     * @ORM\Column(name="SYS_ID", type="integer", nullable=true)
     */
    private $sysId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="GAVEJAS", type="text", nullable=true)
     */
    private $gavejas;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_VILNIETIS", type="string", length=1, nullable=true)
     */
    private $isVilnietis = '';

    /**
     * @var string
     *
     * @ORM\Column(name="DOCUMENT_AVILYS_ID", type="string", length=100, nullable=true)
     */
    private $documentAvilysId;

    /**
     * @var string
     *
     * @ORM\Column(name="DOCUMENT2_AVILYS_ID", type="string", length=100, nullable=true)
     */
    private $document2AvilysId;

    /**
     * @var string
     *
     * @ORM\Column(name="JOURNAL2_AVILYS_ID", type="string", length=64, nullable=false)
     */
    private $journal2AvilysId;

    /**
     * @var string
     *
     * @ORM\Column(name="VALD_DESC3", type="text", nullable=true)
     */
    private $valdDesc3;

    /**
     * @var integer
     *
     * @ORM\Column(name="LIPVADOVAS_ID", type="integer", nullable=false)
     */
    private $lipvadovasId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="VERSIJA", type="string", length=5, nullable=true)
     */
    private $versija = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_RODOMA", type="string", length=10, nullable=true)
     */
    private $isRodoma = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_NOTASK", type="string", length=5, nullable=false)
     */
    private $isNotask;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_VPSIS", type="string", length=5, nullable=false)
     */
    private $isVpsis;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_LANGELIAI", type="string", length=1, nullable=true)
     */
    private $isLangeliai = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_ONLINE", type="string", length=1, nullable=true)
     */
    private $isOnline = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_DOC", type="boolean", nullable=false)
     */
    private $isDoc = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_AUTOCOMPLEETE", type="string", length=5, nullable=false)
     */
    private $isAutocompleete;

    /**
     * @var string
     *
     * @ORM\Column(name="JOURNAL_AVILYS_ID", type="string", length=100, nullable=true)
     */
    private $journalAvilysId;

    /**
     * @var string
     *
     * @ORM\Column(name="HTML", type="text", nullable=false)
     */
    private $html;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_DARBO", type="string", length=5, nullable=false)
     */
    private $isDarbo;

    /**
     * @var string
     *
     * @ORM\Column(name="GAVEJAS_KODAS", type="string", length=255, nullable=false)
     */
    private $gavejasKodas;

    /**
     * @var integer
     *
     * @ORM\Column(name="BANKAI_ID", type="integer", nullable=false)
     */
    private $bankaiId;

    /**
     * @var string
     *
     * @ORM\Column(name="REG_DESC", type="text", length=16777215, nullable=false)
     */
    private $regDesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="PRASYMAI_ID", type="integer", nullable=false)
     */
    private $prasymaiId;

    /**
     * @var string
     *
     * @ORM\Column(name="NO_WEBINPUTFIELDS", type="string", length=5, nullable=false)
     */
    private $noWebinputfields;

    /**
     * @var string
     *
     * @ORM\Column(name="SK_KAINA", type="decimal", precision=8, scale=2, nullable=false)
     */
    private $skKaina;

    /**
     * @var string
     *
     * @ORM\Column(name="SV_KAINA", type="decimal", precision=8, scale=2, nullable=false)
     */
    private $svKaina;

    /**
     * @var integer
     *
     * @ORM\Column(name="SK_LYGIS", type="integer", nullable=false)
     */
    private $skLygis;

    /**
     * @var string
     *
     * @ORM\Column(name="IK_LYGIS", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $ikLygis;

    /**
     * @var string
     *
     * @ORM\Column(name="AFFECTO_ID", type="string", length=50, nullable=false)
     */
    private $affectoId;

    /**
     * @var string
     *
     * @ORM\Column(name="TMPFIELD", type="string", length=5, nullable=false)
     */
    private $tmpfield;

    /**
     * @var string
     *
     * @ORM\Column(name="UZS_INFO", type="text", length=16777215, nullable=false)
     */
    private $uzsInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="RF005A", type="decimal", precision=8, scale=2, nullable=false)
     */
    private $rf005a;

    /**
     * @var string
     *
     * @ORM\Column(name="RF005B", type="decimal", precision=8, scale=2, nullable=false)
     */
    private $rf005b;

    /**
     * @var string
     *
     * @ORM\Column(name="RF005C", type="decimal", precision=8, scale=2, nullable=false)
     */
    private $rf005c;

    /**
     * @var string
     *
     * @ORM\Column(name="ORDER_ACTIONS_ID", type="string", length=255, nullable=false)
     */
    private $orderActionsId = '';


}

