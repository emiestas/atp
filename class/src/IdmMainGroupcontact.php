<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMainGroupcontact
 *
 * @ORM\Table(name="IDM_IDM_MAIN_GROUPCONTACT", indexes={@ORM\Index(name="GROUP_ID", columns={"GROUP_ID"})})
 * @ORM\Entity
 */
class IdmMainGroupcontact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="GROUP_ID", type="integer", nullable=false)
     */
    private $groupId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTACT_ID", type="integer", nullable=false)
     */
    private $contactId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTACT_TYPE", type="integer", nullable=false)
     */
    private $contactType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTACT_SHORT", type="text", nullable=false)
     */
    private $contactShort;

    /**
     * @var string
     *
     * @ORM\Column(name="CONTACT_INFO", type="text", nullable=false)
     */
    private $contactInfo;


}

