<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMainClienttypes
 *
 * @ORM\Table(name="IDM_IDM_MAIN_CLIENTTYPES")
 * @ORM\Entity
 */
class IdmMainClienttypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';


}

