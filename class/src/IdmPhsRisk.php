<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsRisk
 *
 * @ORM\Table(name="IDM_IDM_PHS_RISK")
 * @ORM\Entity
 */
class IdmPhsRisk
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RISK_NAME", type="string", length=255, nullable=false)
     */
    private $riskName;

    /**
     * @var string
     *
     * @ORM\Column(name="DEFAULT_VALUE", type="string", length=255, nullable=false)
     */
    private $defaultValue;


}

