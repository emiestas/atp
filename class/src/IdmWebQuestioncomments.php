<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWebQuestioncomments
 *
 * @ORM\Table(name="IDM_IDM_WEB_QUESTIONCOMMENTS", indexes={@ORM\Index(name="QUESTION_ID", columns={"QUESTION_ID"})})
 * @ORM\Entity
 */
class IdmWebQuestioncomments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="CONTACT_ID", type="string", length=100, nullable=false)
     */
    private $contactId = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COMMENT_DATE", type="datetime", nullable=false)
     */
    private $commentDate = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="QUESTION_ID", type="integer", nullable=false)
     */
    private $questionId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="TEXT_DATA", type="text", nullable=true)
     */
    private $textData;

    /**
     * @var string
     *
     * @ORM\Column(name="CONTACT_IP", type="text", nullable=true)
     */
    private $contactIp;


}

