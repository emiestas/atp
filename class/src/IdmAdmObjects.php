<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAdmObjects
 *
 * @ORM\Table(name="IDM_IDM_ADM_OBJECTS", indexes={@ORM\Index(name="OBJECT_PARENT_ID", columns={"OBJECT_PARENT_ID"})})
 * @ORM\Entity
 */
class IdmAdmObjects
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ITEM", type="string", length=100, nullable=false)
     */
    private $item = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="OBJECT_PARENT_ID", type="integer", nullable=false)
     */
    private $objectParentId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="OBJECT_NAME", type="string", length=100, nullable=false)
     */
    private $objectName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="OBJECT_FILE", type="string", length=100, nullable=true)
     */
    private $objectFile = '';

    /**
     * @var string
     *
     * @ORM\Column(name="OBJECT_TABLE", type="string", length=100, nullable=true)
     */
    private $objectTable = '';

    /**
     * @var string
     *
     * @ORM\Column(name="OBJECT_ADMIN_FILE", type="string", length=100, nullable=true)
     */
    private $objectAdminFile = '';

    /**
     * @var string
     *
     * @ORM\Column(name="OBJECT_DESC", type="string", length=100, nullable=true)
     */
    private $objectDesc = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_HISTORY", type="string", length=5, nullable=true)
     */
    private $isHistory = '';

    /**
     * @var string
     *
     * @ORM\Column(name="WHERE_SQL", type="string", length=255, nullable=true)
     */
    private $whereSql = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="OBJECTTYPE_ID", type="integer", nullable=false)
     */
    private $objecttypeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ORDER_VAR", type="string", length=255, nullable=true)
     */
    private $orderVar = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_ORDERING", type="string", length=5, nullable=true)
     */
    private $isOrdering = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_TEMPLATE", type="string", length=5, nullable=true)
     */
    private $isTemplate = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ADMIN_TYPE", type="string", length=100, nullable=true)
     */
    private $adminType = '';

    /**
     * @var string
     *
     * @ORM\Column(name="B_ADD", type="string", length=50, nullable=true)
     */
    private $bAdd = '';

    /**
     * @var string
     *
     * @ORM\Column(name="B_SAVE", type="string", length=50, nullable=true)
     */
    private $bSave = '';

    /**
     * @var string
     *
     * @ORM\Column(name="B_DEL", type="string", length=50, nullable=true)
     */
    private $bDel = '';

    /**
     * @var string
     *
     * @ORM\Column(name="B_RESET", type="string", length=50, nullable=true)
     */
    private $bReset = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PRIMARY_KEY", type="string", length=50, nullable=false)
     */
    private $primaryKey = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ORDER_SUBVAR", type="string", length=255, nullable=true)
     */
    private $orderSubvar = '';

    /**
     * @var string
     *
     * @ORM\Column(name="QUICKSEARCH", type="string", length=5, nullable=true)
     */
    private $quicksearch = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="POSITION", type="integer", nullable=true)
     */
    private $position = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="VISIBLE", type="string", length=1, nullable=false)
     */
    private $visible = '';


}

