<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsScenario
 *
 * @ORM\Table(name="IDM_IDM_DHS_SCENARIO")
 * @ORM\Entity
 */
class IdmDhsScenario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCCONTROLER_ID", type="integer", nullable=false)
     */
    private $doccontrolerId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCRECEIVER_ID", type="integer", nullable=false)
     */
    private $docreceiverId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="DAYS", type="integer", nullable=false)
     */
    private $days = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SYSTYPE", type="string", length=255, nullable=false)
     */
    private $systype;

    /**
     * @var string
     *
     * @ORM\Column(name="USEABLE", type="string", length=1, nullable=false)
     */
    private $useable = '';

    /**
     * @var string
     *
     * @ORM\Column(name="DONTREGISTER", type="string", length=5, nullable=false)
     */
    private $dontregister;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDRECEIVE", type="string", length=50, nullable=false)
     */
    private $sendreceive;


}

