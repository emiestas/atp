<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmSchEvents
 *
 * @ORM\Table(name="IDM_IDM_SCH_EVENTS")
 * @ORM\Entity
 */
class IdmSchEvents
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=5000, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FROM", type="datetime", nullable=false)
     */
    private $from;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TILL", type="datetime", nullable=false)
     */
    private $till;

    /**
     * @var string
     *
     * @ORM\Column(name="DETAILS", type="text", length=65535, nullable=false)
     */
    private $details;

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VALID", type="boolean", nullable=false)
     */
    private $valid = '1';


}

