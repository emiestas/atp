<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsJournalauthor
 *
 * @ORM\Table(name="IDM_IDM_DHS_JOURNALAUTHOR", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsJournalauthor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="AUTHOR_ID", type="integer", nullable=false)
     */
    private $authorId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="AUTHOR_TYPE", type="integer", nullable=false)
     */
    private $authorType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="AUTHOR_SHORT", type="text", nullable=false)
     */
    private $authorShort;

    /**
     * @var string
     *
     * @ORM\Column(name="AUTHOR_INFO", type="text", nullable=false)
     */
    private $authorInfo;


}

