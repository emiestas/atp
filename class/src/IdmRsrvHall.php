<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmRsrvHall
 *
 * @ORM\Table(name="IDM_IDM_RSRV_HALL")
 * @ORM\Entity
 */
class IdmRsrvHall
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="HALL_NAME", type="string", length=255, nullable=false)
     */
    private $hallName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="HALL_DESC", type="text", length=16777215, nullable=true)
     */
    private $hallDesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_COUNT", type="integer", nullable=false)
     */
    private $userCount = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_PROJECTOR", type="string", length=10, nullable=false)
     */
    private $isProjector = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="ADMIN_ID", type="integer", nullable=false)
     */
    private $adminId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ADMIN2_ID", type="integer", nullable=false)
     */
    private $admin2Id = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SUPORT_ID", type="integer", nullable=false)
     */
    private $suportId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="STRUCTURE_ID", type="integer", nullable=false)
     */
    private $structureId;

    /**
     * @var string
     *
     * @ORM\Column(name="IMG_URL1", type="string", length=255, nullable=false)
     */
    private $imgUrl1 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IMG_URL2", type="string", length=255, nullable=false)
     */
    private $imgUrl2 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IMG_URL3", type="string", length=255, nullable=false)
     */
    private $imgUrl3 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="USEABLE", type="string", length=10, nullable=false)
     */
    private $useable = '';


}

