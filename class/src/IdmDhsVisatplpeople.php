<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsVisatplpeople
 *
 * @ORM\Table(name="IDM_IDM_DHS_VISATPLPEOPLE", indexes={@ORM\Index(name="VISATEMPLATE_ID", columns={"VISATEMPLATE_ID"}), @ORM\Index(name="PEOPLE_ID", columns={"PEOPLE_ID"})})
 * @ORM\Entity
 */
class IdmDhsVisatplpeople
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="VISATEMPLATE_ID", type="integer", nullable=false)
     */
    private $visatemplateId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_TYPE", type="integer", nullable=false)
     */
    private $peopleType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PEOPLE_SHORT", type="text", nullable=false)
     */
    private $peopleShort;

    /**
     * @var string
     *
     * @ORM\Column(name="PEOPLE_INFO", type="text", nullable=false)
     */
    private $peopleInfo;


}

