<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmItnItempeople
 *
 * @ORM\Table(name="IDM_IDM_ITN_ITEMPEOPLE", indexes={@ORM\Index(name="ITEM_ID", columns={"ITEM_ID"})})
 * @ORM\Entity
 */
class IdmItnItempeople
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ITEM_ID", type="integer", nullable=false)
     */
    private $itemId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_TYPE", type="integer", nullable=false)
     */
    private $peopleType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PEOPLE_SHORT", type="text", nullable=false)
     */
    private $peopleShort;

    /**
     * @var string
     *
     * @ORM\Column(name="PEOPLE_INFO", type="text", nullable=false)
     */
    private $peopleInfo;


}

