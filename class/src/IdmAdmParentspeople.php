<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAdmParentspeople
 *
 * @ORM\Table(name="IDM_IDM_ADM_PARENTSPEOPLE", indexes={@ORM\Index(name="PARENT_ID", columns={"PARENT_ID"}), @ORM\Index(name="PEOPLE_ID", columns={"PEOPLE_ID"})})
 * @ORM\Entity
 */
class IdmAdmParentspeople
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=false)
     */
    private $parentId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_TYPE", type="integer", nullable=false)
     */
    private $peopleType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PEOPLE_SHORT", type="text", nullable=false)
     */
    private $peopleShort;

    /**
     * @var string
     *
     * @ORM\Column(name="PEOPLE_INFO", type="text", nullable=false)
     */
    private $peopleInfo;


}

