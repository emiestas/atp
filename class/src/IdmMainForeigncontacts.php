<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMainForeigncontacts
 *
 * @ORM\Table(name="IDM_IDM_MAIN_FOREIGNCONTACTS", indexes={@ORM\Index(name="SHOWS", columns={"SHOWS"}), @ORM\Index(name="CODE", columns={"CODE"})})
 * @ORM\Entity
 */
class IdmMainForeigncontacts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="OLD_ID", type="integer", nullable=false)
     */
    private $oldId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=500, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CODE", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="BIRTHDATE", type="date", nullable=true)
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(name="PVMCODE", type="text", nullable=true)
     */
    private $pvmcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="CITY_ID", type="integer", nullable=false)
     */
    private $cityId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CITY", type="string", length=255, nullable=false)
     */
    private $city = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="COUNTRY_ID", type="integer", nullable=false)
     */
    private $countryId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="COUNTRY", type="string", length=255, nullable=false)
     */
    private $country = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ADDRESS", type="text", nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="EMAIL", type="text", nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="WEB", type="string", length=255, nullable=false)
     */
    private $web = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MSN", type="string", length=255, nullable=false)
     */
    private $msn = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ICQ", type="string", length=255, nullable=false)
     */
    private $icq = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SKYPE", type="string", length=255, nullable=false)
     */
    private $skype = '';

    /**
     * @var string
     *
     * @ORM\Column(name="EDELIVERY", type="string", length=30, nullable=false)
     */
    private $edelivery;

    /**
     * @var string
     *
     * @ORM\Column(name="PHONE_FULL", type="text", nullable=true)
     */
    private $phoneFull;

    /**
     * @var string
     *
     * @ORM\Column(name="PHONE_MOBILE", type="text", nullable=true)
     */
    private $phoneMobile;

    /**
     * @var string
     *
     * @ORM\Column(name="PHONE_FAX", type="text", nullable=true)
     */
    private $phoneFax;

    /**
     * @var string
     *
     * @ORM\Column(name="FNAME", type="text", nullable=true)
     */
    private $fname;

    /**
     * @var string
     *
     * @ORM\Column(name="LNAME", type="text", nullable=true)
     */
    private $lname;

    /**
     * @var string
     *
     * @ORM\Column(name="OFFICE", type="text", nullable=true)
     */
    private $office;

    /**
     * @var string
     *
     * @ORM\Column(name="BANK", type="text", nullable=true)
     */
    private $bank;

    /**
     * @var string
     *
     * @ORM\Column(name="ACCOUNT", type="text", nullable=true)
     */
    private $account;

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE", type="integer", nullable=false)
     */
    private $type = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PASS", type="string", length=32, nullable=true)
     */
    private $pass = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SHORT", type="text", nullable=false)
     */
    private $short;

    /**
     * @var string
     *
     * @ORM\Column(name="ENTERPRISE", type="text", nullable=true)
     */
    private $enterprise;

    /**
     * @var string
     *
     * @ORM\Column(name="IMAGE_URL", type="text", length=16777215, nullable=false)
     */
    private $imageUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_HIDDEN", type="string", length=5, nullable=false)
     */
    private $isHidden;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = 'CURRENT_TIMESTAMP';


}

