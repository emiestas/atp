<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpSearchFieldsXFields
 *
 * @ORM\Table(name="IDM_IDM_ATP_SEARCH_FIELDS_X_FIELDS", indexes={@ORM\Index(name="fk_IDM_ATP_SEARCH_FIELDS_X_FIELDS_IDM_ATP_SEARCH_FIELDS1_idx", columns={"SEARCH_FIELD_ID"}), @ORM\Index(name="fk_IDM_ATP_SEARCH_FIELDS_X_FIELDS_IDM_ATP_TABLE_STRUCTURE1_idx", columns={"FIELD_ID"})})
 * @ORM\Entity
 */
class IdmAtpSearchFieldsXFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var \IdmAtpSearchFields
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="IdmAtpSearchFields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SEARCH_FIELD_ID", referencedColumnName="ID")
     * })
     */
    private $searchField;

    /**
     * @var \IdmAtpTableStructure
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="IdmAtpTableStructure")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FIELD_ID", referencedColumnName="ID")
     * })
     */
    private $field;


}

