<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpRecordUnsaved
 *
 * @ORM\Table(name="IDM_IDM_ATP_RECORD_UNSAVED", indexes={@ORM\Index(name="fk_IDM_ATP_RECORD_UNSAVED_IDM_ATP_RECORD_RELATIONS1_idx", columns={"RECORD_ID"}), @ORM\Index(name="RECORD_NR", columns={"RECORD_NR"})})
 * @ORM\Entity
 */
class IdmAtpRecordUnsaved
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_ID", type="integer", nullable=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_NR", type="string", length=100, nullable=false)
     */
    private $recordNr;

    /**
     * @var integer
     *
     * @ORM\Column(name="WORKER_ID", type="integer", nullable=false)
     */
    private $workerId;


}

