<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWebMyproblems
 *
 * @ORM\Table(name="IDM_IDM_WEB_MYPROBLEMS")
 * @ORM\Entity
 */
class IdmWebMyproblems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="CONTACT_ID", type="string", length=100, nullable=false)
     */
    private $contactId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_ID", type="string", length=32, nullable=false)
     */
    private $docId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="GID_ID", type="string", length=255, nullable=false)
     */
    private $gidId = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS_ID", type="integer", nullable=false)
     */
    private $statusId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_DESC", type="text", nullable=true)
     */
    private $docDesc;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MY_DATE", type="datetime", nullable=false)
     */
    private $myDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=255, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SOLUTION", type="string", length=255, nullable=false)
     */
    private $solution;

    /**
     * @var string
     *
     * @ORM\Column(name="SCENARIO", type="string", length=255, nullable=false)
     */
    private $scenario;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXPIRE_DATE", type="datetime", nullable=false)
     */
    private $expireDate;


}

