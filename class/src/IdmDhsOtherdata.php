<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsOtherdata
 *
 * @ORM\Table(name="IDM_IDM_DHS_OTHERDATA", indexes={@ORM\Index(name="DOC_ID", columns={"DOC_ID"})})
 * @ORM\Entity
 */
class IdmDhsOtherdata
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     */
    private $docId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CL1_ID", type="string", length=255, nullable=true)
     */
    private $cl1Id = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CL2_ID", type="string", length=100, nullable=true)
     */
    private $cl2Id = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CL3_ID", type="string", length=100, nullable=true)
     */
    private $cl3Id = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CL4_ID", type="string", length=100, nullable=true)
     */
    private $cl4Id = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CL5_ID", type="string", length=100, nullable=true)
     */
    private $cl5Id = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CL6_ID", type="string", length=100, nullable=false)
     */
    private $cl6Id;

    /**
     * @var string
     *
     * @ORM\Column(name="CL7_ID", type="string", length=100, nullable=false)
     */
    private $cl7Id;

    /**
     * @var string
     *
     * @ORM\Column(name="CL8_ID", type="string", length=100, nullable=false)
     */
    private $cl8Id;

    /**
     * @var string
     *
     * @ORM\Column(name="CL9_ID", type="string", length=100, nullable=false)
     */
    private $cl9Id;

    /**
     * @var string
     *
     * @ORM\Column(name="CL10_ID", type="string", length=100, nullable=false)
     */
    private $cl10Id;

    /**
     * @var string
     *
     * @ORM\Column(name="CL11_ID", type="string", length=100, nullable=false)
     */
    private $cl11Id;

    /**
     * @var string
     *
     * @ORM\Column(name="CL12_ID", type="string", length=100, nullable=false)
     */
    private $cl12Id;

    /**
     * @var string
     *
     * @ORM\Column(name="CL13_ID", type="string", length=100, nullable=false)
     */
    private $cl13Id;

    /**
     * @var string
     *
     * @ORM\Column(name="CL14_ID", type="string", length=100, nullable=false)
     */
    private $cl14Id;

    /**
     * @var string
     *
     * @ORM\Column(name="CL15_ID", type="string", length=100, nullable=false)
     */
    private $cl15Id;

    /**
     * @var string
     *
     * @ORM\Column(name="CL16_ID", type="string", length=100, nullable=false)
     */
    private $cl16Id;

    /**
     * @var string
     *
     * @ORM\Column(name="CL17_ID", type="string", length=100, nullable=false)
     */
    private $cl17Id;

    /**
     * @var string
     *
     * @ORM\Column(name="CL18_ID", type="string", length=100, nullable=false)
     */
    private $cl18Id;

    /**
     * @var string
     *
     * @ORM\Column(name="CL19_ID", type="string", length=100, nullable=false)
     */
    private $cl19Id;

    /**
     * @var string
     *
     * @ORM\Column(name="CL20_ID", type="string", length=100, nullable=false)
     */
    private $cl20Id;


}

