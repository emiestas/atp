<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmRsrvEvents
 *
 * @ORM\Table(name="IDM_IDM_RSRV_EVENTS")
 * @ORM\Entity
 */
class IdmRsrvEvents
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ITEM_ID", type="integer", nullable=false)
     */
    private $itemId;

    /**
     * @var string
     *
     * @ORM\Column(name="ITEM_TYPE", type="string", length=255, nullable=false)
     */
    private $itemType = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EVENT_START", type="datetime", nullable=true)
     */
    private $eventStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EVENT_END", type="datetime", nullable=true)
     */
    private $eventEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="EVENT_DESC", type="text", length=16777215, nullable=true)
     */
    private $eventDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="EVENT_PURPOSE", type="text", length=16777215, nullable=true)
     */
    private $eventPurpose;

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATOR_ID", type="integer", nullable=false)
     */
    private $creatorId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ADMIN_ID", type="integer", nullable=false)
     */
    private $adminId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ORGANIZER_ID", type="integer", nullable=false)
     */
    private $organizerId;

    /**
     * @var string
     *
     * @ORM\Column(name="VALID", type="string", length=10, nullable=false)
     */
    private $valid = '';


}

