<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMetQuestions
 *
 * @ORM\Table(name="IDM_IDM_MET_QUESTIONS", indexes={@ORM\Index(name="MEETING_ID", columns={"MEETING_ID"})})
 * @ORM\Entity
 */
class IdmMetQuestions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=16777215, nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="AVILIO_ID", type="string", length=64, nullable=false)
     */
    private $avilioId;

    /**
     * @var string
     *
     * @ORM\Column(name="QUESTION_DESC", type="text", nullable=false)
     */
    private $questionDesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="NR", type="integer", nullable=false)
     */
    private $nr = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="MEETING_ID", type="integer", nullable=false)
     */
    private $meetingId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="GROUP_ID", type="integer", nullable=false)
     */
    private $groupId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE_ID", type="integer", nullable=false)
     */
    private $typeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="VOTE_DATA", type="text", length=16777215, nullable=false)
     */
    private $voteData;

    /**
     * @var integer
     *
     * @ORM\Column(name="VOTE_COUNT", type="integer", nullable=false)
     */
    private $voteCount = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_TASK", type="string", length=5, nullable=false)
     */
    private $isTask = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     */
    private $docId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_DATA", type="text", length=16777215, nullable=false)
     */
    private $docData;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_TYPE", type="string", length=5, nullable=false)
     */
    private $createType = '';

    /**
     * @var string
     *
     * @ORM\Column(name="INVITEE_TEXT", type="text", length=16777215, nullable=false)
     */
    private $inviteeText;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATOR_ID", type="integer", nullable=false)
     */
    private $creatorId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATOR_ORGNODE_ID", type="integer", nullable=false)
     */
    private $creatorOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATOR_INFO", type="text", nullable=false)
     */
    private $creatorInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATOR_SHORT", type="text", nullable=false)
     */
    private $creatorShort;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXPIRE_DATE", type="datetime", nullable=false)
     */
    private $expireDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", nullable=true)
     */
    private $receiverInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_INFO", type="string", length=255, nullable=false)
     */
    private $senderInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", nullable=true)
     */
    private $receiverShort;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_SHORT", type="string", length=255, nullable=false)
     */
    private $senderShort;

    /**
     * @var string
     *
     * @ORM\Column(name="INVITEE_SHORT", type="text", length=16777215, nullable=false)
     */
    private $inviteeShort;

    /**
     * @var string
     *
     * @ORM\Column(name="INVITEE_INFO", type="text", length=16777215, nullable=false)
     */
    private $inviteeInfo;

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTROL_TYPE", type="integer", nullable=true)
     */
    private $controlType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTROLER_ID", type="integer", nullable=true)
     */
    private $controlerId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTROLER_ORGNODE_ID", type="integer", nullable=true)
     */
    private $controlerOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTROLER_INFO", type="text", nullable=true)
     */
    private $controlerInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="CONTROLER_SHORT", type="text", nullable=true)
     */
    private $controlerShort;

    /**
     * @var string
     *
     * @ORM\Column(name="APROVED", type="string", length=5, nullable=false)
     */
    private $aproved = '';

    /**
     * @var string
     *
     * @ORM\Column(name="NOW_ACTIVE", type="string", length=5, nullable=false)
     */
    private $nowActive = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="LATER_ACTIVE", type="boolean", nullable=false)
     */
    private $laterActive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="NEXT_ACTIVE", type="boolean", nullable=false)
     */
    private $nextActive;

    /**
     * @var string
     *
     * @ORM\Column(name="NOW_VOTING", type="string", length=5, nullable=false)
     */
    private $nowVoting = '';

    /**
     * @var string
     *
     * @ORM\Column(name="FOREIGN_ID", type="string", length=255, nullable=false)
     */
    private $foreignId;

    /**
     * @var string
     *
     * @ORM\Column(name="LAW_ID", type="string", length=255, nullable=false)
     */
    private $lawId;

    /**
     * @var string
     *
     * @ORM\Column(name="QUESTION_HASH", type="string", length=32, nullable=false)
     */
    private $questionHash;

    /**
     * @var string
     *
     * @ORM\Column(name="STATUS_DESC", type="string", length=255, nullable=false)
     */
    private $statusDesc;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="VOTE_STARTED", type="datetime", nullable=true)
     */
    private $voteStarted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="VOTE_ENDED", type="datetime", nullable=true)
     */
    private $voteEnded;


}

