<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosRegistry
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_REGISTRY", uniqueConstraints={@ORM\UniqueConstraint(name="_SERVICE__CODE", columns={"SERVICE_ID", "CHECK_CODE"})}, indexes={@ORM\Index(name="_SERVICE_ID", columns={"SERVICE_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosRegistry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="CHECK_CODE", type="string", length=64, nullable=false)
     */
    private $checkCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="REGISTER_ORG_CODE", type="integer", nullable=false)
     */
    private $registerOrgCode;

    /**
     * @var string
     *
     * @ORM\Column(name="REGISTER_ORG_TITLE", type="string", length=256, nullable=false)
     */
    private $registerOrgTitle;

    /**
     * @var \IdmPaslaugos
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SERVICE_ID", referencedColumnName="ID")
     * })
     */
    private $service;


}

