<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsAutoaction
 *
 * @ORM\Table(name="IDM_IDM_DHS_AUTOACTION", indexes={@ORM\Index(name="PEOPLE_ID", columns={"PEOPLE_ID"})})
 * @ORM\Entity
 */
class IdmDhsAutoaction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SIGN", type="string", length=5, nullable=false)
     */
    private $sign = '';

    /**
     * @var string
     *
     * @ORM\Column(name="VISA", type="string", length=5, nullable=false)
     */
    private $visa = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ACCEPT", type="string", length=5, nullable=false)
     */
    private $accept = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ACCOMPLISH", type="string", length=5, nullable=false)
     */
    private $accomplish = '';

    /**
     * @var string
     *
     * @ORM\Column(name="REGISTER", type="string", length=5, nullable=false)
     */
    private $register = '';


}

