<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmIdbWindowusers
 *
 * @ORM\Table(name="IDM_IDM_IDB_WINDOWUSERS")
 * @ORM\Entity
 */
class IdmIdbWindowusers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="REPORT_ID", type="integer", nullable=false)
     */
    private $reportId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=false)
     */
    private $userId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="WINDOW_STATUS", type="string", length=100, nullable=false)
     */
    private $windowStatus = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENT", type="text", nullable=false)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="FIELD_POS", type="string", length=255, nullable=false)
     */
    private $fieldPos = '';

    /**
     * @var string
     *
     * @ORM\Column(name="TOPLEFT", type="string", length=255, nullable=false)
     */
    private $topleft = '';

    /**
     * @var string
     *
     * @ORM\Column(name="WIDTHHEIGHT", type="string", length=255, nullable=false)
     */
    private $widthheight = '';

    /**
     * @var string
     *
     * @ORM\Column(name="NEW_WINDOW", type="string", length=1, nullable=false)
     */
    private $newWindow = '';


}

