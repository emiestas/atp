<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosRekvizitai
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_REKVIZITAI")
 * @ORM\Entity
 */
class IdmPaslaugosRekvizitai
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="BANKO_SASKAITA", type="string", length=255, nullable=true)
     */
    private $bankoSaskaita;

    /**
     * @var string
     *
     * @ORM\Column(name="BANKO_PAVADINIMAS", type="string", length=255, nullable=true)
     */
    private $bankoPavadinimas;

    /**
     * @var string
     *
     * @ORM\Column(name="APMOKEJIMO_GAVEJAS", type="text", length=65535, nullable=true)
     */
    private $apmokejimoGavejas;

    /**
     * @var string
     *
     * @ORM\Column(name="GAVEJO_KODAS", type="string", length=50, nullable=true)
     */
    private $gavejoKodas;

    /**
     * @var string
     *
     * @ORM\Column(name="IMOKOS_KODAS", type="string", length=50, nullable=true)
     */
    private $imokosKodas;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=20, nullable=false)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=20, nullable=false)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=false)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=false)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=false)
     */
    private $modifyIp = '';


}

