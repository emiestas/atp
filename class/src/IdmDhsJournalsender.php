<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsJournalsender
 *
 * @ORM\Table(name="IDM_IDM_DHS_JOURNALSENDER", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsJournalsender
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SENDER_ID", type="integer", nullable=false)
     */
    private $senderId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SENDER_TYPE", type="integer", nullable=false)
     */
    private $senderType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_SHORT", type="text", nullable=false)
     */
    private $senderShort;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_INFO", type="text", nullable=false)
     */
    private $senderInfo;


}

