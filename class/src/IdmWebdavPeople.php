<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWebdavPeople
 *
 * @ORM\Table(name="IDM_IDM_WEBDAV_PEOPLE")
 * @ORM\Entity
 */
class IdmWebdavPeople
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="FILE_ID", type="integer", nullable=false)
     */
    private $fileId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId;


}

