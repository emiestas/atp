<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTbl9
 *
 * @ORM\Table(name="IDM_IDM_ATP_TBL_9", indexes={@ORM\Index(name="RECORD_LAST", columns={"RECORD_LAST"}), @ORM\Index(name="RECORD_MAIN_ID", columns={"RECORD_MAIN_ID"})})
 * @ORM\Entity
 */
class IdmAtpTbl9
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_MAIN_ID", type="string", length=25, nullable=false)
     */
    private $recordMainId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_VERSION", type="integer", nullable=false)
     */
    private $recordVersion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RECORD_LAST", type="boolean", nullable=false)
     */
    private $recordLast;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_18", type="date", nullable=false)
     */
    private $col18;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_19", type="time", nullable=false)
     */
    private $col19;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_20", type="text", length=255, nullable=false)
     */
    private $col20;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_24", type="text", length=255, nullable=false)
     */
    private $col24;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_25", type="date", nullable=false)
     */
    private $col25;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_29", type="text", length=255, nullable=false)
     */
    private $col29;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_30", type="text", length=255, nullable=false)
     */
    private $col30;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_31", type="text", length=255, nullable=false)
     */
    private $col31;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_34", type="date", nullable=false)
     */
    private $col34;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_35", type="text", length=255, nullable=false)
     */
    private $col35;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_42", type="text", length=65535, nullable=false)
     */
    private $col42;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_43", type="text", length=65535, nullable=false)
     */
    private $col43;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_44", type="text", length=65535, nullable=false)
     */
    private $col44;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_45", type="text", length=65535, nullable=false)
     */
    private $col45;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_46", type="text", length=255, nullable=false)
     */
    private $col46;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_47", type="text", length=255, nullable=false)
     */
    private $col47;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_51", type="text", length=255, nullable=false)
     */
    private $col51;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_52", type="text", length=255, nullable=false)
     */
    private $col52;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_53", type="text", length=255, nullable=false)
     */
    private $col53;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_54", type="text", length=255, nullable=false)
     */
    private $col54;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_55", type="text", length=255, nullable=false)
     */
    private $col55;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_56", type="date", nullable=false)
     */
    private $col56;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;

    /**
     * @var string
     *
     * @ORM\Column(name="REGNUMBER", type="text", length=255, nullable=false)
     */
    private $regnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="RDATE", type="text", length=255, nullable=false)
     */
    private $rdate;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDERID", type="text", length=255, nullable=false)
     */
    private $senderid;

    /**
     * @var string
     *
     * @ORM\Column(name="PENALTYID", type="text", length=255, nullable=false)
     */
    private $penaltyid;

    /**
     * @var string
     *
     * @ORM\Column(name="ENDID", type="text", length=255, nullable=false)
     */
    private $endid;

    /**
     * @var string
     *
     * @ORM\Column(name="NOTE", type="text", length=255, nullable=false)
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="NDATE", type="text", length=255, nullable=false)
     */
    private $ndate;

    /**
     * @var string
     *
     * @ORM\Column(name="TELNUM", type="text", length=255, nullable=false)
     */
    private $telnum;

    /**
     * @var string
     *
     * @ORM\Column(name="DUTY", type="text", length=255, nullable=false)
     */
    private $duty;

    /**
     * @var string
     *
     * @ORM\Column(name="COURTID", type="text", length=255, nullable=false)
     */
    private $courtid;

    /**
     * @var string
     *
     * @ORM\Column(name="APPEALID", type="text", length=255, nullable=false)
     */
    private $appealid;

    /**
     * @var string
     *
     * @ORM\Column(name="RULE", type="text", length=255, nullable=false)
     */
    private $rule;


}

