<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMetNotes
 *
 * @ORM\Table(name="IDM_IDM_MET_NOTES", indexes={@ORM\Index(name="MEETING_ID", columns={"QUESTION_ID"})})
 * @ORM\Entity
 */
class IdmMetNotes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=16777215, nullable=false)
     */
    private $shows;

    /**
     * @var integer
     *
     * @ORM\Column(name="QUESTION_ID", type="integer", nullable=false)
     */
    private $questionId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="MEETING_ID", type="integer", nullable=false)
     */
    private $meetingId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PEOPLE_INFO", type="text", nullable=true)
     */
    private $peopleInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="PEOPLE_SHORT", type="text", nullable=true)
     */
    private $peopleShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_TYPE", type="integer", nullable=false)
     */
    private $peopleType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ORGNODE_ID", type="integer", nullable=false)
     */
    private $peopleOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PEOPLE_IP", type="string", length=255, nullable=false)
     */
    private $peopleIp = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COMMENT_DATE", type="datetime", nullable=false)
     */
    private $commentDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="USER_TYPE", type="string", length=5, nullable=true)
     */
    private $userType;


}

