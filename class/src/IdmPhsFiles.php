<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsFiles
 *
 * @ORM\Table(name="IDM_IDM_PHS_FILES", indexes={@ORM\Index(name="FOLDER_ID", columns={"FOLDER_ID"}), @ORM\Index(name="PROJECT_ID", columns={"PROJECT_ID"}), @ORM\Index(name="DOC_ID", columns={"TASK_ID"}), @ORM\Index(name="NOTE_ID", columns={"NOTE_ID"}), @ORM\Index(name="PROCESS_ID", columns={"NOTE_ID"}), @ORM\Index(name="STAGE_ID", columns={"STAGE_ID"})})
 * @ORM\Entity
 */
class IdmPhsFiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=65535, nullable=false)
     */
    private $shows;

    /**
     * @var integer
     *
     * @ORM\Column(name="PROJECT_ID", type="integer", nullable=true)
     */
    private $projectId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="STAGE_ID", type="integer", nullable=true)
     */
    private $stageId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PROCESS_ID", type="integer", nullable=true)
     */
    private $processId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="FOLDER_ID", type="integer", nullable=false)
     */
    private $folderId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="TASK_ID", type="integer", nullable=false)
     */
    private $taskId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="NOTE_ID", type="integer", nullable=false)
     */
    private $noteId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENTTYPE", type="string", length=100, nullable=false)
     */
    private $contenttype = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENT", type="text", nullable=true)
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATOR_ID", type="integer", nullable=false)
     */
    private $creatorId;


}

