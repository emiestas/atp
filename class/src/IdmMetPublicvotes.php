<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMetPublicvotes
 *
 * @ORM\Table(name="IDM_IDM_MET_PUBLICVOTES", indexes={@ORM\Index(name="MEETING_ID", columns={"QUESTION_ID"})})
 * @ORM\Entity
 */
class IdmMetPublicvotes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=16777215, nullable=false)
     */
    private $shows;

    /**
     * @var integer
     *
     * @ORM\Column(name="QUESTION_ID", type="integer", nullable=false)
     */
    private $questionId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="MEETING_ID", type="integer", nullable=false)
     */
    private $meetingId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="VOTING_ID", type="integer", nullable=false)
     */
    private $votingId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="VOTING_INFO", type="text", nullable=true)
     */
    private $votingInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="VOTING_SHORT", type="text", nullable=true)
     */
    private $votingShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="VOTING_TYPE", type="integer", nullable=false)
     */
    private $votingType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="VOTING_ORGNODE_ID", type="integer", nullable=false)
     */
    private $votingOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="VOTING_IP", type="string", length=255, nullable=false)
     */
    private $votingIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="VOTING_LIP", type="string", length=255, nullable=false)
     */
    private $votingLip = '';

    /**
     * @var string
     *
     * @ORM\Column(name="VOTE", type="string", length=255, nullable=true)
     */
    private $vote = '';

    /**
     * @var string
     *
     * @ORM\Column(name="REG_USER", type="string", length=5, nullable=true)
     */
    private $regUser = '';


}

