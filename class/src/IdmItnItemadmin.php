<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmItnItemadmin
 *
 * @ORM\Table(name="IDM_IDM_ITN_ITEMADMIN", indexes={@ORM\Index(name="ITEM_ID", columns={"ITEM_ID"})})
 * @ORM\Entity
 */
class IdmItnItemadmin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ITEM_ID", type="integer", nullable=false)
     */
    private $itemId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ADMIN_ID", type="integer", nullable=false)
     */
    private $adminId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ADMIN_TYPE", type="integer", nullable=false)
     */
    private $adminType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ADMIN_SHORT", type="text", nullable=false)
     */
    private $adminShort;

    /**
     * @var string
     *
     * @ORM\Column(name="ADMIN_INFO", type="text", nullable=false)
     */
    private $adminInfo;


}

