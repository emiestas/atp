<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsCase
 *
 * @ORM\Table(name="IDM_IDM_DHS_CASE", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID_REM"})})
 * @ORM\Entity
 */
class IdmDhsCase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="DEFAULTSET", type="string", length=5, nullable=false)
     */
    private $defaultset;

    /**
     * @var string
     *
     * @ORM\Column(name="PREFIX", type="string", length=255, nullable=false)
     */
    private $prefix = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID_REM", type="integer", nullable=false)
     */
    private $journalIdRem = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="LAST_CASE_ID", type="integer", nullable=false)
     */
    private $lastCaseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PERIOD", type="integer", nullable=true)
     */
    private $period;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     */
    private $docId;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_ID_URL", type="string", length=5000, nullable=false)
     */
    private $docIdUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="NOTE", type="text", length=65535, nullable=false)
     */
    private $note;

    /**
     * @var integer
     *
     * @ORM\Column(name="ACTIVITY_ID", type="integer", nullable=false)
     */
    private $activityId;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_IN_ADDITION", type="string", length=1, nullable=false)
     */
    private $isInAddition;

    /**
     * @var string
     *
     * @ORM\Column(name="USABLE", type="string", length=1, nullable=false)
     */
    private $usable = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="CASE_DAYS", type="integer", nullable=false)
     */
    private $caseDays;

    /**
     * @var integer
     *
     * @ORM\Column(name="ARCHIVE_YEAR", type="integer", nullable=false)
     */
    private $archiveYear;

    /**
     * @var integer
     *
     * @ORM\Column(name="DELETE_YEAR", type="integer", nullable=false)
     */
    private $deleteYear;

    /**
     * @var integer
     *
     * @ORM\Column(name="ADMIN_ORG_ID", type="integer", nullable=false)
     */
    private $adminOrgId;


}

