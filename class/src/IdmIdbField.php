<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmIdbField
 *
 * @ORM\Table(name="IDM_IDM_IDB_FIELD", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmIdbField
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=255, nullable=false)
     */
    private $name = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="REPORT_ID", type="integer", nullable=false)
     */
    private $reportId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=100, nullable=false)
     */
    private $type = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCH_START", type="text", length=16777215, nullable=false)
     */
    private $searchStart;

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCH_END", type="text", length=16777215, nullable=false)
     */
    private $searchEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="START_TAG", type="text", length=16777215, nullable=false)
     */
    private $startTag;

    /**
     * @var string
     *
     * @ORM\Column(name="END_TAG", type="text", length=16777215, nullable=false)
     */
    private $endTag;

    /**
     * @var string
     *
     * @ORM\Column(name="START_HTML", type="text", length=16777215, nullable=true)
     */
    private $startHtml;

    /**
     * @var string
     *
     * @ORM\Column(name="END_HTML", type="text", length=16777215, nullable=true)
     */
    private $endHtml;

    /**
     * @var string
     *
     * @ORM\Column(name="ADD_START", type="string", length=1, nullable=true)
     */
    private $addStart;

    /**
     * @var string
     *
     * @ORM\Column(name="ADD_END", type="string", length=1, nullable=true)
     */
    private $addEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="JUNK_START", type="text", length=16777215, nullable=true)
     */
    private $junkStart;

    /**
     * @var string
     *
     * @ORM\Column(name="JUNK_END", type="text", length=16777215, nullable=true)
     */
    private $junkEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="JUNK_DATA", type="text", length=16777215, nullable=false)
     */
    private $junkData;

    /**
     * @var integer
     *
     * @ORM\Column(name="PART_NR", type="integer", nullable=false)
     */
    private $partNr = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ROW_NR", type="string", length=5, nullable=false)
     */
    private $rowNr = '';

    /**
     * @var string
     *
     * @ORM\Column(name="DEFAULT_VALUE", type="string", length=255, nullable=false)
     */
    private $defaultValue = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="POSITION", type="integer", nullable=false)
     */
    private $position = '0';


}

