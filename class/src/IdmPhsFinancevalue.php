<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsFinancevalue
 *
 * @ORM\Table(name="IDM_IDM_PHS_FINANCEVALUE")
 * @ORM\Entity
 */
class IdmPhsFinancevalue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var string
     *
     * @ORM\Column(name="INPUT_VALUE", type="string", length=50, nullable=false)
     */
    private $inputValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="PROJECT_ID", type="integer", nullable=false)
     */
    private $projectId = '0';


}

