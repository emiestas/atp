<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsJournaltemplate
 *
 * @ORM\Table(name="IDM_IDM_DHS_JOURNALTEMPLATE", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsJournaltemplate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=255, nullable=false)
     */
    private $shows = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="XML_TEMPLATE", type="string", length=255, nullable=false)
     */
    private $xmlTemplate = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId = '0';


}

