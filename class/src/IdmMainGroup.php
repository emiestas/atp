<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMainGroup
 *
 * @ORM\Table(name="IDM_IDM_MAIN_GROUP")
 * @ORM\Entity
 */
class IdmMainGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="GROUP_NAME", type="string", length=255, nullable=false)
     */
    private $groupName = '0';


}

