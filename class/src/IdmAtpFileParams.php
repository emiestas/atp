<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpFileParams
 *
 * @ORM\Table(name="IDM_IDM_ATP_FILE_PARAMS")
 * @ORM\Entity
 */
class IdmAtpFileParams
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="TABLE_ID", type="integer", nullable=false)
     */
    private $tableId;

    /**
     * @var integer
     *
     * @ORM\Column(name="STRUCTURE_ID", type="integer", nullable=false)
     */
    private $structureId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CNT", type="integer", nullable=false)
     */
    private $cnt;

    /**
     * @var integer
     *
     * @ORM\Column(name="SIZE_FROM", type="integer", nullable=false)
     */
    private $sizeFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="SIZE_TO", type="integer", nullable=false)
     */
    private $sizeTo;


}

