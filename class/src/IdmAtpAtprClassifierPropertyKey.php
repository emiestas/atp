<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpAtprClassifierPropertyKey
 *
 * @ORM\Table(name="IDM_IDM_ATP_ATPR_CLASSIFIER_PROPERTY_KEY")
 * @ORM\Entity
 */
class IdmAtpAtprClassifierPropertyKey
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=255, nullable=false)
     */
    private $name;


}

