<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmItnToknow
 *
 * @ORM\Table(name="IDM_IDM_ITN_TOKNOW", indexes={@ORM\Index(name="ITEM_ID", columns={"ITEM_ID"})})
 * @ORM\Entity
 */
class IdmItnToknow
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var integer
     *
     * @ORM\Column(name="ITEM_ID", type="integer", nullable=false)
     */
    private $itemId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ACTION_DATE", type="datetime", nullable=true)
     */
    private $actionDate;


}

