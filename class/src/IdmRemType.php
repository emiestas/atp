<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmRemType
 *
 * @ORM\Table(name="IDM_IDM_REM_TYPE")
 * @ORM\Entity
 */
class IdmRemType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=255, nullable=false)
     */
    private $type = '';

    /**
     * @var string
     *
     * @ORM\Column(name="REMIND_ACTION", type="string", length=255, nullable=false)
     */
    private $remindAction = '';


}

