<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsTemplate
 *
 * @ORM\Table(name="IDM_IDM_DHS_TEMPLATE", indexes={@ORM\Index(name="CREATE_ID", columns={"CREATE_ID"})})
 * @ORM\Entity
 */
class IdmDhsTemplate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="TPL_NAME", type="string", length=255, nullable=false)
     */
    private $tplName = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SCENARIO_ID", type="integer", nullable=false)
     */
    private $scenarioId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ID", type="integer", nullable=false)
     */
    private $receiverId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SENDER_ID", type="integer", nullable=false)
     */
    private $senderId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_TYPE", type="integer", nullable=false)
     */
    private $receiverType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SENDER_TYPE", type="integer", nullable=false)
     */
    private $senderType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTROLER_ID", type="integer", nullable=false)
     */
    private $controlerId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTROL_TYPE", type="integer", nullable=false)
     */
    private $controlType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SECRET", type="string", length=2, nullable=false)
     */
    private $secret = '';

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_DATA", type="text", nullable=true)
     */
    private $docData;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_NOTES", type="text", nullable=true)
     */
    private $docNotes;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=false)
     */
    private $parentId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATE_ID", type="integer", nullable=false)
     */
    private $createId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_TYPE", type="string", length=255, nullable=false)
     */
    private $createType = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATEMODIFY_DATE", type="datetime", nullable=false)
     */
    private $createmodifyDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="VISAS_ID", type="string", length=50, nullable=false)
     */
    private $visasId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SIGNATURE_ID", type="string", length=50, nullable=false)
     */
    private $signatureId = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="CASE_ID", type="integer", nullable=false)
     */
    private $caseId = '0';


}

