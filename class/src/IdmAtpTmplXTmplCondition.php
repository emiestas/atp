<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTmplXTmplCondition
 *
 * @ORM\Table(name="IDM_IDM_ATP_TMPL_X_TMPL_CONDITION", indexes={@ORM\Index(name="TMPL_ID", columns={"TMPL_ID", "CONDITION_ID"})})
 * @ORM\Entity
 */
class IdmAtpTmplXTmplCondition
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="TMPL_ID", type="integer", nullable=false)
     */
    private $tmplId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CONDITION_ID", type="integer", nullable=false)
     */
    private $conditionId;


}

