<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTbl57
 *
 * @ORM\Table(name="IDM_IDM_ATP_TBL_57", indexes={@ORM\Index(name="RECORD_LAST", columns={"RECORD_LAST"}), @ORM\Index(name="RECORD_MAIN_ID", columns={"RECORD_MAIN_ID"})})
 * @ORM\Entity
 */
class IdmAtpTbl57
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_MAIN_ID", type="string", length=25, nullable=false)
     */
    private $recordMainId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_VERSION", type="integer", nullable=false)
     */
    private $recordVersion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RECORD_LAST", type="boolean", nullable=false)
     */
    private $recordLast;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_1", type="text", length=65535, nullable=false)
     */
    private $col1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_2", type="date", nullable=false)
     */
    private $col2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_3", type="time", nullable=false)
     */
    private $col3;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_4", type="text", length=255, nullable=false)
     */
    private $col4;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_5", type="text", length=255, nullable=false)
     */
    private $col5;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_6", type="text", length=255, nullable=false)
     */
    private $col6;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_7", type="text", length=255, nullable=false)
     */
    private $col7;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_8", type="text", length=255, nullable=false)
     */
    private $col8;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_9", type="text", length=255, nullable=false)
     */
    private $col9;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_10", type="text", length=65535, nullable=false)
     */
    private $col10;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_11", type="text", length=255, nullable=false)
     */
    private $col11;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_12", type="text", length=65535, nullable=false)
     */
    private $col12;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_13", type="date", nullable=false)
     */
    private $col13;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_14", type="text", length=255, nullable=false)
     */
    private $col14;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_15", type="date", nullable=false)
     */
    private $col15;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_16", type="text", length=255, nullable=false)
     */
    private $col16;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_20", type="text", length=255, nullable=false)
     */
    private $col20;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_22", type="text", length=255, nullable=false)
     */
    private $col22;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_23", type="text", length=255, nullable=false)
     */
    private $col23;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_24", type="text", length=255, nullable=false)
     */
    private $col24;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_25", type="text", length=255, nullable=false)
     */
    private $col25;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_27", type="text", length=255, nullable=false)
     */
    private $col27;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_34", type="text", length=255, nullable=false)
     */
    private $col34;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_37", type="text", length=255, nullable=false)
     */
    private $col37;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_42", type="text", length=255, nullable=false)
     */
    private $col42;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_43", type="date", nullable=false)
     */
    private $col43;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_44", type="text", length=255, nullable=false)
     */
    private $col44;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_45", type="date", nullable=false)
     */
    private $col45;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_46", type="text", length=65535, nullable=false)
     */
    private $col46;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_47", type="text", length=65535, nullable=false)
     */
    private $col47;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_48", type="text", length=65535, nullable=false)
     */
    private $col48;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_49", type="date", nullable=false)
     */
    private $col49;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_50", type="date", nullable=false)
     */
    private $col50;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_51", type="text", length=65535, nullable=false)
     */
    private $col51;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_52", type="text", length=255, nullable=false)
     */
    private $col52;

    /**
     * @var integer
     *
     * @ORM\Column(name="COL_53", type="integer", nullable=false)
     */
    private $col53;

    /**
     * @var integer
     *
     * @ORM\Column(name="COL_54", type="integer", nullable=false)
     */
    private $col54;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_55", type="date", nullable=false)
     */
    private $col55;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_56", type="time", nullable=false)
     */
    private $col56;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_57", type="time", nullable=false)
     */
    private $col57;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_58", type="text", length=255, nullable=false)
     */
    private $col58;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_59", type="text", length=255, nullable=false)
     */
    private $col59;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_61", type="text", length=255, nullable=false)
     */
    private $col61;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_62", type="text", length=65535, nullable=false)
     */
    private $col62;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_63", type="text", length=65535, nullable=false)
     */
    private $col63;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_64", type="text", length=255, nullable=false)
     */
    private $col64;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_65", type="text", length=255, nullable=false)
     */
    private $col65;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;


}

