<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosRegistryFields
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_REGISTRY_FIELDS", uniqueConstraints={@ORM\UniqueConstraint(name="_REGISTRY__NAME", columns={"REGISTRY_ID", "NAME"})}, indexes={@ORM\Index(name="_REGISTRY_ID", columns={"REGISTRY_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosRegistryFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="VALUE_TYPE", type="string", length=30, nullable=false)
     */
    private $valueType;

    /**
     * @var string
     *
     * @ORM\Column(name="LIST_ITEM_TYPE", type="string", length=30, nullable=false)
     */
    private $listItemType;

    /**
     * @var \IdmPaslaugosRegistry
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosRegistry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="REGISTRY_ID", referencedColumnName="ID")
     * })
     */
    private $registry;


}

