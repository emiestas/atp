<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsProcess
 *
 * @ORM\Table(name="IDM_IDM_PHS_PROCESS", indexes={@ORM\Index(name="PROJECT_ID", columns={"PROJECT_ID"}), @ORM\Index(name="STAGE_ID", columns={"STAGE_ID"}), @ORM\Index(name="PARENT_ID", columns={"PARENT_ID"})})
 * @ORM\Entity
 */
class IdmPhsProcess
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=false)
     */
    private $parentId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=65535, nullable=false)
     */
    private $shows;

    /**
     * @var integer
     *
     * @ORM\Column(name="PROJECT_ID", type="integer", nullable=true)
     */
    private $projectId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="STAGE_ID", type="integer", nullable=true)
     */
    private $stageId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ISLATE", type="string", length=5, nullable=true)
     */
    private $islate = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ISDONE", type="string", length=5, nullable=true)
     */
    private $isdone = '';

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", length=65535, nullable=true)
     */
    private $receiverShort;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", length=65535, nullable=true)
     */
    private $receiverInfo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=true)
     */
    private $createDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXPIRE_DATE", type="datetime", nullable=true)
     */
    private $expireDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CHECK_DATE", type="datetime", nullable=false)
     */
    private $checkDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ACCOMPLISH_DATE", type="datetime", nullable=true)
     */
    private $accomplishDate = '0000-00-00 00:00:00';


}

