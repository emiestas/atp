<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpActs
 *
 * @ORM\Table(name="IDM_IDM_ATP_ACTS", indexes={@ORM\Index(name="VALID", columns={"VALID"})})
 * @ORM\Entity
 */
class IdmAtpActs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=false)
     */
    private $parentId;

    /**
     * @var string
     *
     * @ORM\Column(name="NR", type="string", length=100, nullable=false)
     */
    private $nr;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=5000, nullable=false)
     */
    private $label;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="VALID_FROM", type="datetime", nullable=false)
     */
    private $validFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="VALID_TILL", type="datetime", nullable=false)
     */
    private $validTill;

    /**
     * @var integer
     *
     * @ORM\Column(name="CLASSIFICATOR", type="integer", nullable=false)
     */
    private $classificator;

    /**
     * @var string
     *
     * @ORM\Column(name="FIELDS_VALUES", type="text", length=65535, nullable=false)
     */
    private $fieldsValues;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VALID", type="boolean", nullable=false)
     */
    private $valid = '1';


}

