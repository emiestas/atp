<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderStatus
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_STATUS", indexes={@ORM\Index(name="_ORDER_ID__LAST", columns={"ORDER_ID", "IS_LAST"}), @ORM\Index(name="_STATUS_ID__LAST", columns={"STATUS_ID", "IS_LAST"}), @ORM\Index(name="ORDER_ID", columns={"ORDER_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderStatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORDER_ID", type="integer", nullable=false)
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS_ID", type="integer", nullable=false)
     */
    private $statusId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_LAST", type="boolean", nullable=false)
     */
    private $isLast;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = 'CURRENT_TIMESTAMP';


}

