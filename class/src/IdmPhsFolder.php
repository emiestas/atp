<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsFolder
 *
 * @ORM\Table(name="IDM_IDM_PHS_FOLDER")
 * @ORM\Entity
 */
class IdmPhsFolder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=65535, nullable=false)
     */
    private $shows;

    /**
     * @var integer
     *
     * @ORM\Column(name="POSITION", type="integer", nullable=false)
     */
    private $position = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="USEABLE", type="string", length=100, nullable=false)
     */
    private $useable = '';


}

