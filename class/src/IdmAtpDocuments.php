<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpDocuments
 *
 * @ORM\Table(name="IDM_IDM_ATP_DOCUMENTS", indexes={@ORM\Index(name="_PARENT_ID", columns={"PARENT_ID"}), @ORM\Index(name="_VALID", columns={"VALID"})})
 * @ORM\Entity
 */
class IdmAtpDocuments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=255, nullable=false)
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="STATUS", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CAN_EDIT", type="boolean", nullable=false)
     */
    private $canEdit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CAN_END", type="boolean", nullable=false)
     */
    private $canEnd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CAN_NEXT", type="boolean", nullable=false)
     */
    private $canNext;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CAN_IDLE", type="boolean", nullable=false)
     */
    private $canIdle;

    /**
     * @var integer
     *
     * @ORM\Column(name="IDLE_DAYS", type="integer", nullable=false)
     */
    private $idleDays;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CAN_TERM", type="boolean", nullable=false)
     */
    private $canTerm;

    /**
     * @var integer
     *
     * @ORM\Column(name="TERM_DAYS", type="integer", nullable=false)
     */
    private $termDays;

    /**
     * @var integer
     *
     * @ORM\Column(name="VIOLATION_DATE", type="integer", nullable=false)
     */
    private $violationDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="PERSON_CODE", type="integer", nullable=false)
     */
    private $personCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="PERSON_ADDRESS", type="integer", nullable=false)
     */
    private $personAddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="COMPANY_ADDRESS", type="integer", nullable=false)
     */
    private $companyAddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="OTHER_ADDRESS", type="integer", nullable=false)
     */
    private $otherAddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="PERSON_NAME", type="integer", nullable=false)
     */
    private $personName;

    /**
     * @var integer
     *
     * @ORM\Column(name="PERSON_SURNAME", type="integer", nullable=false)
     */
    private $personSurname;

    /**
     * @var integer
     *
     * @ORM\Column(name="COMPANY_NAME", type="integer", nullable=false)
     */
    private $companyName;

    /**
     * @var integer
     *
     * @ORM\Column(name="FILL_DATE", type="integer", nullable=false)
     */
    private $fillDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="AVILYS_DATE", type="integer", nullable=false)
     */
    private $avilysDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="AVILYS_NUMBER", type="integer", nullable=false)
     */
    private $avilysNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="PENALTY_SUM", type="integer", nullable=false)
     */
    private $penaltySum;

    /**
     * @var integer
     *
     * @ORM\Column(name="CLASS_TYPE", type="integer", nullable=false)
     */
    private $classType;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR", type="integer", nullable=false)
     */
    private $examinator;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_CABINET", type="integer", nullable=false)
     */
    private $examinatorCabinet;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_PHONE", type="integer", nullable=false)
     */
    private $examinatorPhone;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_EMAIL", type="integer", nullable=false)
     */
    private $examinatorEmail;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_INSTITUTION_NAME", type="integer", nullable=false)
     */
    private $examinatorInstitutionName;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_UNIT", type="integer", nullable=false)
     */
    private $examinatorUnit;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_SUBUNIT", type="integer", nullable=false)
     */
    private $examinatorSubunit;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_INSTITUTION_ADDRESS", type="integer", nullable=false)
     */
    private $examinatorInstitutionAddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_DATE", type="integer", nullable=false)
     */
    private $examinatorDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_START_TIME", type="integer", nullable=false)
     */
    private $examinatorStartTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXAMINATOR_FINISH_TIME", type="integer", nullable=false)
     */
    private $examinatorFinishTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="DECISION_DATE", type="integer", nullable=false)
     */
    private $decisionDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="APPEALED_DATE", type="integer", nullable=false)
     */
    private $appealedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="APPEAL_STC_DATE", type="integer", nullable=false)
     */
    private $appealStcDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="APPEAL_CCD_DATE", type="integer", nullable=false)
     */
    private $appealCcdDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="VIOLATION_TIME", type="integer", nullable=false)
     */
    private $violationTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="VIOLATION_PLACE", type="integer", nullable=false)
     */
    private $violationPlace;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="UPDATE_DATE", type="datetime", nullable=false)
     */
    private $updateDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VALID", type="boolean", nullable=false)
     */
    private $valid = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="WITNESS_NAME", type="integer", nullable=false)
     */
    private $witnessName;

    /**
     * @var integer
     *
     * @ORM\Column(name="WITNESS_SURNAME", type="integer", nullable=false)
     */
    private $witnessSurname;

    /**
     * @var integer
     *
     * @ORM\Column(name="WITNESS_INSTITUTION", type="integer", nullable=false)
     */
    private $witnessInstitution;

    /**
     * @var \IdmAtpDocuments
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpDocuments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PARENT_ID", referencedColumnName="ID")
     * })
     */
    private $parent;


}

