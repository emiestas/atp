<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosWebpartner
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_WEBPARTNER", uniqueConstraints={@ORM\UniqueConstraint(name="_SERVICE_ID", columns={"SERVICE_ID"})}, indexes={@ORM\Index(name="_JOURNAL_ID", columns={"JOURNAL_ID"}), @ORM\Index(name="_CASE_ID", columns={"CASE_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosWebpartner
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=true)
     */
    private $journalId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CASE_ID", type="integer", nullable=true)
     */
    private $caseId;

    /**
     * @var \IdmPaslaugos
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SERVICE_ID", referencedColumnName="ID")
     * })
     */
    private $service;


}

