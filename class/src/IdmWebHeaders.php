<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWebHeaders
 *
 * @ORM\Table(name="IDM_IDM_WEB_HEADERS", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmWebHeaders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="HEADER_DATA", type="text", nullable=false)
     */
    private $headerData;


}

