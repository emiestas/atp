<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsAutoregmail
 *
 * @ORM\Table(name="IDM_IDM_DHS_AUTOREGMAIL", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsAutoregmail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RULENAME", type="string", length=255, nullable=false)
     */
    private $rulename;

    /**
     * @var integer
     *
     * @ORM\Column(name="MAILBOX_ID", type="integer", nullable=false)
     */
    private $mailboxId;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_DATA", type="text", nullable=false)
     */
    private $docData;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_NOTES", type="text", nullable=false)
     */
    private $docNotes;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CASE_ID", type="integer", nullable=false)
     */
    private $caseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="SCENARIO_ID", type="integer", nullable=false)
     */
    private $scenarioId;

    /**
     * @var integer
     *
     * @ORM\Column(name="REGISTRATOR_ID", type="integer", nullable=false)
     */
    private $registratorId;

    /**
     * @var string
     *
     * @ORM\Column(name="USEABLE", type="string", length=2, nullable=false)
     */
    private $useable;

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCHPART", type="string", length=255, nullable=false)
     */
    private $searchpart;

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCHEMAIL", type="string", length=255, nullable=false)
     */
    private $searchemail;

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCHSUBJECT", type="string", length=255, nullable=false)
     */
    private $searchsubject;

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCHMSG", type="string", length=255, nullable=false)
     */
    private $searchmsg;

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS_ID", type="integer", nullable=false)
     */
    private $statusId;


}

