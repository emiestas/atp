<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpDocumentTerms
 *
 * @ORM\Table(name="IDM_IDM_ATP_DOCUMENT_TERMS")
 * @ORM\Entity
 */
class IdmAtpDocumentTerms
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCUMENT_ID", type="integer", nullable=false)
     */
    private $documentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="FIELD_ID", type="integer", nullable=false)
     */
    private $fieldId;

    /**
     * @var integer
     *
     * @ORM\Column(name="DAYS", type="integer", nullable=false)
     */
    private $days;

    /**
     * @var boolean
     *
     * @ORM\Column(name="TYPE", type="boolean", nullable=false)
     */
    private $type;


}

