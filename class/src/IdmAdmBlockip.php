<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAdmBlockip
 *
 * @ORM\Table(name="IDM_IDM_ADM_BLOCKIP")
 * @ORM\Entity
 */
class IdmAdmBlockip
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="IP", type="string", length=32, nullable=false)
     */
    private $ip = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="BLOCKED_TILL", type="datetime", nullable=false)
     */
    private $blockedTill = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="REASON", type="string", length=255, nullable=false)
     */
    private $reason = '';


}

