<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsPayments
 *
 * @ORM\Table(name="IDM_IDM_PHS_PAYMENTS", indexes={@ORM\Index(name="PROJECT_ID", columns={"PROJECT_ID"})})
 * @ORM\Entity
 */
class IdmPhsPayments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PROJECT_ID", type="integer", nullable=false)
     */
    private $projectId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=65535, nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="PRICE", type="decimal", precision=9, scale=2, nullable=false)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="PAY_ID", type="integer", nullable=false)
     */
    private $payId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="PAY_DATE", type="datetime", nullable=false)
     */
    private $payDate = '2007-01-01 00:00:00';


}

