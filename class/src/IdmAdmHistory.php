<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAdmHistory
 *
 * @ORM\Table(name="IDM_IDM_ADM_HISTORY", indexes={@ORM\Index(name="OBJECT_ID", columns={"OBJECT_ID"})})
 * @ORM\Entity
 */
class IdmAdmHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="OBJECT_ID", type="integer", nullable=false)
     */
    private $objectId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ITEM_DATA", type="text", nullable=true)
     */
    private $itemData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=false)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="ITEM_ID", type="integer", nullable=false)
     */
    private $itemId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="VERSION", type="integer", nullable=false)
     */
    private $version = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=50, nullable=false)
     */
    private $modifyBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=32, nullable=false)
     */
    private $modifyIp = '';


}

