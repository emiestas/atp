<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpDeedExtends
 *
 * @ORM\Table(name="IDM_IDM_ATP_DEED_EXTENDS", indexes={@ORM\Index(name="VALID", columns={"VALID"}), @ORM\Index(name="DEED_ID", columns={"DEED_ID", "ITYPE", "VALID"})})
 * @ORM\Entity
 */
class IdmAtpDeedExtends
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="DEED_ID", type="integer", nullable=false)
     */
    private $deedId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ITEM_ID", type="integer", nullable=false)
     */
    private $itemId;

    /**
     * @var string
     *
     * @ORM\Column(name="ITYPE", type="string", length=10, nullable=false)
     */
    private $itype;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VALID", type="boolean", nullable=false)
     */
    private $valid = '1';


}

