<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWpSoftusers
 *
 * @ORM\Table(name="IDM_IDM_WP_SOFTUSERS", indexes={@ORM\Index(name="ID", columns={"ID_OLD"}), @ORM\Index(name="USER_ID", columns={"USER_ID"})})
 * @ORM\Entity
 */
class IdmWpSoftusers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=true)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="SOFT_ID", type="integer", nullable=true)
     */
    private $softId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=true)
     */
    private $userId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="USER_SHORT", type="text", length=16777215, nullable=false)
     */
    private $userShort;

    /**
     * @var string
     *
     * @ORM\Column(name="USER_INFO", type="text", length=16777215, nullable=false)
     */
    private $userInfo;

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_TYPE", type="integer", nullable=false)
     */
    private $userType;

    /**
     * @var integer
     *
     * @ORM\Column(name="STRUCTURE_ID", type="integer", nullable=true)
     */
    private $structureId = '0';


}

