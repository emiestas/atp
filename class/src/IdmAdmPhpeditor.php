<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAdmPhpeditor
 *
 * @ORM\Table(name="IDM_IDM_ADM_PHPEDITOR", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmAdmPhpeditor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="PHP_DATA", type="text", nullable=true)
     */
    private $phpData;


}

