<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsStagetype
 *
 * @ORM\Table(name="IDM_IDM_PHS_STAGETYPE")
 * @ORM\Entity
 */
class IdmPhsStagetype
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=65535, nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="USEABLE", type="string", length=100, nullable=false)
     */
    private $useable = '';


}

