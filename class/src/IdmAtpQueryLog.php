<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpQueryLog
 *
 * @ORM\Table(name="IDM_IDM_ATP_QUERY_LOG", indexes={@ORM\Index(name="time", columns={"time"})})
 * @ORM\Entity
 */
class IdmAtpQueryLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="site", type="integer", nullable=false)
     */
    private $site = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="start", type="float", precision=10, scale=0, nullable=false)
     */
    private $start;

    /**
     * @var float
     *
     * @ORM\Column(name="end", type="float", precision=10, scale=0, nullable=false)
     */
    private $end;

    /**
     * @var float
     *
     * @ORM\Column(name="time", type="float", precision=10, scale=0, nullable=false)
     */
    private $time;

    /**
     * @var string
     *
     * @ORM\Column(name="query", type="text", length=65535, nullable=false)
     */
    private $query;

    /**
     * @var string
     *
     * @ORM\Column(name="backtrace", type="text", length=65535, nullable=false)
     */
    private $backtrace;

    /**
     * @var string
     *
     * @ORM\Column(name="request", type="text", length=65535, nullable=false)
     */
    private $request;


}

