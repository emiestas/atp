<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWpMailurl
 *
 * @ORM\Table(name="IDM_IDM_WP_MAILURL")
 * @ORM\Entity
 */
class IdmWpMailurl
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="URL", type="text", length=16777215, nullable=false)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="URLDATE", type="datetime", nullable=false)
     */
    private $urldate;


}

