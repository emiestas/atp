<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMainWorkers
 *
 * @ORM\Table(name="IDM_IDM_MAIN_WORKERS", indexes={@ORM\Index(name="PEOPLE_ID", columns={"PEOPLE_ID"})})
 * @ORM\Entity
 */
class IdmMainWorkers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="STRUCTURE_ID", type="integer", nullable=false)
     */
    private $structureId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="OFFICE_ID", type="integer", nullable=false)
     */
    private $officeId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTACT_ID", type="integer", nullable=false)
     */
    private $contactId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=true)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="AVILIO_ID", type="string", length=64, nullable=false)
     */
    private $avilioId;

    /**
     * @var string
     *
     * @ORM\Column(name="ROOM", type="string", length=50, nullable=true)
     */
    private $room = '';

    /**
     * @var string
     *
     * @ORM\Column(name="WORKER_DESC", type="text", nullable=true)
     */
    private $workerDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="VISIBLE", type="string", length=5, nullable=false)
     */
    private $visible;

    /**
     * @var string
     *
     * @ORM\Column(name="CITY", type="string", length=500, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="STREET", type="string", length=500, nullable=false)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="POST_CODE", type="string", length=50, nullable=false)
     */
    private $postCode;

    /**
     * @var string
     *
     * @ORM\Column(name="KAZKAS_ANT_VOKO", type="string", length=5000, nullable=false)
     */
    private $kazkasAntVoko;


}

