<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMetInvitees
 *
 * @ORM\Table(name="IDM_IDM_MET_INVITEES", indexes={@ORM\Index(name="QUESTION_ID", columns={"QUESTION_ID"})})
 * @ORM\Entity
 */
class IdmMetInvitees
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="QUESTION_ID", type="integer", nullable=false)
     */
    private $questionId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="MEETING_ID", type="integer", nullable=false)
     */
    private $meetingId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="INVITEE_ID", type="integer", nullable=false)
     */
    private $inviteeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="INVITEE_INFO", type="text", nullable=true)
     */
    private $inviteeInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="INVITEE_SHORT", type="text", nullable=true)
     */
    private $inviteeShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="INVITEE_TYPE", type="integer", nullable=false)
     */
    private $inviteeType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="INVITEE_ORGNODE_ID", type="integer", nullable=false)
     */
    private $inviteeOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="STATE", type="string", length=15, nullable=true)
     */
    private $state = '';


}

