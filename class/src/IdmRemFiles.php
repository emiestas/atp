<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmRemFiles
 *
 * @ORM\Table(name="IDM_IDM_REM_FILES")
 * @ORM\Entity
 */
class IdmRemFiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=255, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="REMIND_ID", type="integer", nullable=false)
     */
    private $remindId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENTTYPE", type="string", length=100, nullable=false)
     */
    private $contenttype = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENT", type="text", nullable=true)
     */
    private $content;


}

