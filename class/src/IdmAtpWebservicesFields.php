<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpWebservicesFields
 *
 * @ORM\Table(name="IDM_IDM_ATP_WEBSERVICES_FIELDS", uniqueConstraints={@ORM\UniqueConstraint(name="WS_NAME", columns={"WS_NAME", "WS_FIELD_ID", "FIELD_ID"})})
 * @ORM\Entity
 */
class IdmAtpWebservicesFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="WS_NAME", type="string", length=25, nullable=false)
     */
    private $wsName;

    /**
     * @var integer
     *
     * @ORM\Column(name="WS_FIELD_ID", type="integer", nullable=false)
     */
    private $wsFieldId;

    /**
     * @var integer
     *
     * @ORM\Column(name="FIELD_ID", type="integer", nullable=false)
     */
    private $fieldId;


}

