<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpRecordXWorker
 *
 * @ORM\Table(name="IDM_IDM_ATP_RECORD_X_WORKER", indexes={@ORM\Index(name="fk_IDM_ATP_RECORD_X_PEOPLE_IDM_ATP_RECORD_RELATIONS1_idx", columns={"RECORD_ID"}), @ORM\Index(name="WORKER_ID", columns={"WORKER_ID"}), @ORM\Index(name="RECORD_ID", columns={"RECORD_ID", "WORKER_ID"})})
 * @ORM\Entity
 */
class IdmAtpRecordXWorker
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_ID", type="integer", nullable=false)
     */
    private $recordId;

    /**
     * @var integer
     *
     * @ORM\Column(name="WORKER_ID", type="integer", nullable=false)
     */
    private $workerId;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=45, nullable=true)
     */
    private $type;


}

