<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmCrmContactType
 *
 * @ORM\Table(name="IDM_IDM_CRM_CONTACT_TYPE")
 * @ORM\Entity
 */
class IdmCrmContactType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ACTIVE", type="boolean", nullable=false)
     */
    private $active = '1';


}

