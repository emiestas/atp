<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderPaymentRequestXref
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_PAYMENT_REQUEST_XREF", indexes={@ORM\Index(name="_PAYMENT_REQUEST_ID", columns={"PAYMENT_REQUEST_ID"}), @ORM\Index(name="_PAYMENT_ID", columns={"PAYMENT_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderPaymentRequestXref
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \IdmPaslaugosOrderPaymentRequest
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosOrderPaymentRequest")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PAYMENT_REQUEST_ID", referencedColumnName="ID")
     * })
     */
    private $paymentRequest;

    /**
     * @var \IdmPaslaugosOrderPayments
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosOrderPayments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PAYMENT_ID", referencedColumnName="ID")
     * })
     */
    private $payment;


}

