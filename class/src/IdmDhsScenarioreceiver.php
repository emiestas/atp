<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsScenarioreceiver
 *
 * @ORM\Table(name="IDM_IDM_DHS_SCENARIORECEIVER", indexes={@ORM\Index(name="SCENARIO_ID", columns={"SCENARIO_ID"})})
 * @ORM\Entity
 */
class IdmDhsScenarioreceiver
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="SCENARIO_ID", type="integer", nullable=false)
     */
    private $scenarioId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ID", type="integer", nullable=false)
     */
    private $receiverId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_TYPE", type="integer", nullable=false)
     */
    private $receiverType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", nullable=false)
     */
    private $receiverShort;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", nullable=false)
     */
    private $receiverInfo;


}

