<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAdmObjectsTypes
 *
 * @ORM\Table(name="IDM_IDM_ADM_OBJECTS_TYPES")
 * @ORM\Entity
 */
class IdmAdmObjectsTypes
{
    /**
     * @var string
     *
     * @ORM\Column(name="TEMPL_ICON", type="string", length=100, nullable=true)
     */
    private $templIcon = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ITEMS_ICON", type="string", length=100, nullable=true)
     */
    private $itemsIcon = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=true)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ITEM_ICON", type="string", length=100, nullable=true)
     */
    private $itemIcon = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ADD_ICON", type="string", length=100, nullable=true)
     */
    private $addIcon = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SAVE_ICON", type="string", length=100, nullable=true)
     */
    private $saveIcon = '';

    /**
     * @var string
     *
     * @ORM\Column(name="DEL_ICON", type="string", length=100, nullable=true)
     */
    private $delIcon = '';

    /**
     * @var string
     *
     * @ORM\Column(name="RESET_ICON", type="string", length=100, nullable=true)
     */
    private $resetIcon = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCH_ICON", type="string", length=100, nullable=true)
     */
    private $searchIcon = '';

    /**
     * @var string
     *
     * @ORM\Column(name="POS_ICON", type="string", length=100, nullable=true)
     */
    private $posIcon = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PREVIEW_ICON", type="string", length=100, nullable=true)
     */
    private $previewIcon = '';


}

