<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpSispTowXRecord
 *
 * @ORM\Table(name="IDM_IDM_ATP_SISP_TOW_X_RECORD")
 * @ORM\Entity
 */
class IdmAtpSispTowXRecord
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="TOW_ID", type="integer", nullable=false)
     */
    private $towId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_NR", type="string", length=20, nullable=false)
     */
    private $recordNr;


}

