<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpDeeds
 *
 * @ORM\Table(name="IDM_IDM_ATP_DEEDS")
 * @ORM\Entity
 */
class IdmAtpDeeds
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=false)
     */
    private $parentId;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=255, nullable=false)
     */
    private $label;


}

