<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMetVotes
 *
 * @ORM\Table(name="IDM_IDM_MET_VOTES", indexes={@ORM\Index(name="MEETING_ID", columns={"QUESTION_ID"})})
 * @ORM\Entity
 */
class IdmMetVotes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=16777215, nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="VOTE_DESC", type="string", length=255, nullable=false)
     */
    private $voteDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="DESC_FILE", type="string", length=255, nullable=false)
     */
    private $descFile;

    /**
     * @var integer
     *
     * @ORM\Column(name="QUESTION_ID", type="integer", nullable=false)
     */
    private $questionId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="MEETING_ID", type="integer", nullable=false)
     */
    private $meetingId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="VOTING_ID", type="integer", nullable=false)
     */
    private $votingId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="VOTING_INFO", type="text", nullable=true)
     */
    private $votingInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="VOTING_SHORT", type="text", nullable=true)
     */
    private $votingShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="VOTING_TYPE", type="integer", nullable=false)
     */
    private $votingType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="VOTING_ORGNODE_ID", type="integer", nullable=false)
     */
    private $votingOrgnodeId = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="VOTE_DATE", type="datetime", nullable=false)
     */
    private $voteDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="VOTE", type="string", length=255, nullable=true)
     */
    private $vote = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SYS_ID", type="string", length=255, nullable=false)
     */
    private $sysId;


}

