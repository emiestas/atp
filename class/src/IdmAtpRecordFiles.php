<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpRecordFiles
 *
 * @ORM\Table(name="IDM_IDM_ATP_RECORD_FILES", indexes={@ORM\Index(name="IDX_4065E3DD31722D03", columns={"RECORD_ID"}), @ORM\Index(name="FIELD_ID", columns={"FIELD_ID"}), @ORM\Index(name="IDX_4065E3DD94C3C196", columns={"FILE_ID"})})
 * @ORM\Entity
 */
class IdmAtpRecordFiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="FIELD_ID", type="integer", nullable=false)
     */
    private $fieldId;

    /**
     * @var \IdmAtpRecord
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpRecord")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="RECORD_ID", referencedColumnName="ID")
     * })
     */
    private $record;

    /**
     * @var \IdmAtpFiles
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpFiles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FILE_ID", referencedColumnName="ID")
     * })
     */
    private $file;


}

