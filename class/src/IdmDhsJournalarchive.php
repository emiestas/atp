<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsJournalarchive
 *
 * @ORM\Table(name="IDM_IDM_DHS_JOURNALARCHIVE", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsJournalarchive
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ARCHIVE_ID", type="integer", nullable=false)
     */
    private $archiveId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ARCHIVE_TYPE", type="integer", nullable=false)
     */
    private $archiveType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ARCHIVE_SHORT", type="text", nullable=false)
     */
    private $archiveShort;

    /**
     * @var string
     *
     * @ORM\Column(name="ARCHIVE_INFO", type="text", nullable=false)
     */
    private $archiveInfo;


}

