<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTmplConditionXConditionPram
 *
 * @ORM\Table(name="IDM_IDM_ATP_TMPL_CONDITION_X_CONDITION_PRAM", indexes={@ORM\Index(name="CONDITION_ID", columns={"CONDITION_ID"})})
 * @ORM\Entity
 */
class IdmAtpTmplConditionXConditionPram
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="CONDITION_ID", type="integer", nullable=false)
     */
    private $conditionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARAMETER_ID", type="integer", nullable=false)
     */
    private $parameterId;


}

