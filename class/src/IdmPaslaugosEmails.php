<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosEmails
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_EMAILS")
 * @ORM\Entity
 */
class IdmPaslaugosEmails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="EMAIL", type="string", length=100, nullable=false)
     */
    private $email;


}

