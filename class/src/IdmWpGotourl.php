<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWpGotourl
 *
 * @ORM\Table(name="IDM_IDM_WP_GOTOURL", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmWpGotourl
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="URL", type="text", length=16777215, nullable=false)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="TARGET", type="text", length=16777215, nullable=false)
     */
    private $target;

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=false)
     */
    private $userId = '0';


}

