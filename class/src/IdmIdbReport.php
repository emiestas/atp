<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmIdbReport
 *
 * @ORM\Table(name="IDM_IDM_IDB_REPORT", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmIdbReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=100, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SYSNAME", type="string", length=255, nullable=false)
     */
    private $sysname = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="PERIOD", type="integer", nullable=false)
     */
    private $period = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="NEW_FILE_PERIOD", type="integer", nullable=false)
     */
    private $newFilePeriod = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FILE_DATE", type="datetime", nullable=false)
     */
    private $fileDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="NEXT_UPDATE", type="datetime", nullable=false)
     */
    private $nextUpdate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LAST_UPDATE", type="datetime", nullable=false)
     */
    private $lastUpdate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CHECK_DATE", type="datetime", nullable=false)
     */
    private $checkDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="ITEM_ICON", type="string", length=255, nullable=false)
     */
    private $itemIcon = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="CATEGORY_ID", type="integer", nullable=false)
     */
    private $categoryId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="HTMLTABLE_ID", type="integer", nullable=false)
     */
    private $htmltableId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="HEADER_INFO", type="text", nullable=false)
     */
    private $headerInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="MAXPARTS", type="string", length=5, nullable=false)
     */
    private $maxparts = '';

    /**
     * @var string
     *
     * @ORM\Column(name="XML_FILENAME", type="string", length=255, nullable=false)
     */
    private $xmlFilename = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="MAX_LENGTH", type="integer", nullable=false)
     */
    private $maxLength = '0';


}

