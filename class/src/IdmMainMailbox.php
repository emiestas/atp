<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMainMailbox
 *
 * @ORM\Table(name="IDM_IDM_MAIN_MAILBOX")
 * @ORM\Entity
 */
class IdmMainMailbox
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="MAILSERVER", type="string", length=255, nullable=false)
     */
    private $mailserver;

    /**
     * @var string
     *
     * @ORM\Column(name="MAILPORT", type="string", length=4, nullable=false)
     */
    private $mailport;

    /**
     * @var string
     *
     * @ORM\Column(name="MAILUSER", type="string", length=255, nullable=false)
     */
    private $mailuser;

    /**
     * @var string
     *
     * @ORM\Column(name="MAILPASS", type="string", length=255, nullable=false)
     */
    private $mailpass;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CHECKED_DATE", type="datetime", nullable=false)
     */
    private $checkedDate;


}

