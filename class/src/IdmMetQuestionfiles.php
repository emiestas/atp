<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMetQuestionfiles
 *
 * @ORM\Table(name="IDM_IDM_MET_QUESTIONFILES", indexes={@ORM\Index(name="MEETING_ID", columns={"QUESTION_ID"})})
 * @ORM\Entity
 */
class IdmMetQuestionfiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=false)
     */
    private $shows;

    /**
     * @var integer
     *
     * @ORM\Column(name="QUESTION_ID", type="integer", nullable=false)
     */
    private $questionId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="MEETING_ID", type="integer", nullable=false)
     */
    private $meetingId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENTTYPE", type="string", length=100, nullable=false)
     */
    private $contenttype = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENT", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="FOLDER", type="string", length=255, nullable=false)
     */
    private $folder = '';

    /**
     * @var string
     *
     * @ORM\Column(name="USER_LABEL", type="string", length=255, nullable=false)
     */
    private $userLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="FILE_DESC", type="string", length=255, nullable=false)
     */
    private $fileDesc = '';


}

