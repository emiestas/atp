<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpClauseRelation
 *
 * @ORM\Table(name="IDM_IDM_ATP_CLAUSE_RELATION")
 * @ORM\Entity
 */
class IdmAtpClauseRelation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="CLAUSE_ID", type="integer", nullable=false)
     */
    private $clauseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CLAUSE_STRUCTURE_ID", type="integer", nullable=false)
     */
    private $clauseStructureId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TABLE_ID", type="integer", nullable=false)
     */
    private $tableId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TABLE_STRUCTURE_ID", type="integer", nullable=false)
     */
    private $tableStructureId;


}

