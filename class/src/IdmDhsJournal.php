<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsJournal
 *
 * @ORM\Table(name="IDM_IDM_DHS_JOURNAL", indexes={@ORM\Index(name="OFFICECASE_ID", columns={"OFFICECASE_ID"})})
 * @ORM\Entity
 */
class IdmDhsJournal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ADMIN_ORG_ID", type="integer", nullable=false)
     */
    private $adminOrgId;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=16777215, nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="REGISTER_TYPE", type="string", length=50, nullable=false)
     */
    private $registerType = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SHORT", type="string", length=255, nullable=false)
     */
    private $short;

    /**
     * @var integer
     *
     * @ORM\Column(name="FROMORGNODE_ID", type="integer", nullable=false)
     */
    private $fromorgnodeId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCSECRETAR_ID", type="integer", nullable=false)
     */
    private $docsecretarId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="OFFICECASE_ID", type="integer", nullable=false)
     */
    private $officecaseId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS_ID", type="integer", nullable=false)
     */
    private $statusId = '60';

    /**
     * @var integer
     *
     * @ORM\Column(name="FINALSTATUS_ID", type="integer", nullable=false)
     */
    private $finalstatusId;

    /**
     * @var string
     *
     * @ORM\Column(name="SYSTYPE", type="string", length=5, nullable=false)
     */
    private $systype;

    /**
     * @var integer
     *
     * @ORM\Column(name="LAST_ID", type="integer", nullable=false)
     */
    private $lastId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCCONTROLER_ID", type="integer", nullable=false)
     */
    private $doccontrolerId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCCONTROL_TYPE", type="integer", nullable=false)
     */
    private $doccontrolType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="DAYS", type="integer", nullable=true)
     */
    private $days = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PREFIX", type="string", length=100, nullable=false)
     */
    private $prefix = '';

    /**
     * @var string
     *
     * @ORM\Column(name="NR_TEMPLATE", type="text", nullable=false)
     */
    private $nrTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="XML_TEMPLATE", type="text", nullable=true)
     */
    private $xmlTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_TEMPLATE", type="text", nullable=true)
     */
    private $docTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="REL_TEXT", type="text", length=16777215, nullable=false)
     */
    private $relText;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDRECEIVE", type="string", length=10, nullable=false)
     */
    private $sendreceive = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATOR_TEXT", type="string", length=100, nullable=false)
     */
    private $creatorText;

    /**
     * @var string
     *
     * @ORM\Column(name="EXPIRE_TEXT", type="string", length=100, nullable=false)
     */
    private $expireText;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_TEXT", type="string", length=100, nullable=false)
     */
    private $receiverText;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_TEXT", type="string", length=100, nullable=false)
     */
    private $senderText;

    /**
     * @var string
     *
     * @ORM\Column(name="SECRET", type="string", length=1, nullable=false)
     */
    private $secret = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_PUBLIC", type="string", length=1, nullable=false)
     */
    private $isPublic;

    /**
     * @var string
     *
     * @ORM\Column(name="USEABLE", type="string", length=2, nullable=false)
     */
    private $useable;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_PUBLIC_SET", type="string", length=2, nullable=false)
     */
    private $isPublicSet;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_VERSIONABLE", type="string", length=1, nullable=false)
     */
    private $isVersionable;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_BACKREG", type="string", length=5, nullable=false)
     */
    private $isBackreg;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_NUMBERCONT", type="string", length=5, nullable=false)
     */
    private $isNumbercont;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_NOFILES", type="string", length=5, nullable=false)
     */
    private $isNofiles;

    /**
     * @var integer
     *
     * @ORM\Column(name="POSITION", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOW_REGISTRATOR", type="string", length=50, nullable=false)
     */
    private $showRegistrator;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOW_CREATOR", type="string", length=50, nullable=false)
     */
    private $showCreator;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOW_SENDER", type="string", length=50, nullable=false)
     */
    private $showSender;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOW_RECEIVER", type="string", length=50, nullable=false)
     */
    private $showReceiver;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOW_EXPIREDATE", type="string", length=50, nullable=false)
     */
    private $showExpiredate;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOW_ORIG", type="string", length=50, nullable=false)
     */
    private $showOrig;


}

