<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsAutoregmailsender
 *
 * @ORM\Table(name="IDM_IDM_DHS_AUTOREGMAILSENDER", indexes={@ORM\Index(name="MAIL_ID", columns={"MAIL_ID"})})
 * @ORM\Entity
 */
class IdmDhsAutoregmailsender
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="MAIL_ID", type="integer", nullable=false)
     */
    private $mailId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SENDER_ID", type="integer", nullable=false)
     */
    private $senderId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SENDER_TYPE", type="integer", nullable=false)
     */
    private $senderType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_SHORT", type="text", nullable=false)
     */
    private $senderShort;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_INFO", type="text", nullable=false)
     */
    private $senderInfo;


}

