<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsDocuments
 *
 * @ORM\Table(name="IDM_IDM_DHS_DOCUMENTS", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"}), @ORM\Index(name="CREATE_DATE", columns={"CREATE_DATE"}), @ORM\Index(name="STATUS_ID", columns={"STATUS_ID"}), @ORM\Index(name="ARCHIVE", columns={"ARCHIVE"}), @ORM\Index(name="EXPIRE_DATE", columns={"EXPIRE_DATE"}), @ORM\Index(name="_CASE_ID", columns={"CASE_ID"})})
 * @ORM\Entity
 */
class IdmDhsDocuments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="TEMP_ID", type="integer", nullable=false)
     */
    private $tempId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CASE_NUMBER", type="string", length=10, nullable=false)
     */
    private $caseNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_NR", type="integer", nullable=false)
     */
    private $docNr;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_DATA", type="text", nullable=true)
     */
    private $docData;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_DESC", type="text", nullable=true)
     */
    private $docDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_ACCOMPLISH", type="text", nullable=true)
     */
    private $docAccomplish;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_RETURN", type="text", length=16777215, nullable=false)
     */
    private $docReturn;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_NOTES", type="text", nullable=false)
     */
    private $docNotes;

    /**
     * @var string
     *
     * @ORM\Column(name="PROLONG_DESC", type="text", nullable=true)
     */
    private $prolongDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="ISNEW", type="string", length=1, nullable=false)
     */
    private $isnew = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="VERSION", type="integer", nullable=false)
     */
    private $version = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SECRET", type="string", length=1, nullable=false)
     */
    private $secret = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="ALLOW_DOC_EDIT_DURING_COORDINATION", type="integer", nullable=false)
     */
    private $allowDocEditDuringCoordination = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PROGRESS", type="integer", nullable=false)
     */
    private $progress = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="FOREIGN_NR", type="string", length=255, nullable=false)
     */
    private $foreignNr = '';

    /**
     * @var string
     *
     * @ORM\Column(name="FOREIGN_DATE", type="string", length=10, nullable=false)
     */
    private $foreignDate = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PLACE", type="string", length=255, nullable=true)
     */
    private $place = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="ISLATE", type="boolean", nullable=true)
     */
    private $islate;

    /**
     * @var string
     *
     * @ORM\Column(name="WASLATE", type="string", length=1, nullable=true)
     */
    private $waslate = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXPIRE_DATE", type="datetime", nullable=false)
     */
    private $expireDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="PROLONG_DATE", type="datetime", nullable=true)
     */
    private $prolongDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="REGISTER_DATE", type="datetime", nullable=false)
     */
    private $registerDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ACCOMPLISH_DATE", type="datetime", nullable=false)
     */
    private $accomplishDate;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", nullable=true)
     */
    private $receiverInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", nullable=true)
     */
    private $receiverShort;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_INFO", type="text", nullable=true)
     */
    private $senderInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_SHORT", type="text", nullable=true)
     */
    private $senderShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="CASE_ID", type="integer", nullable=false)
     */
    private $caseId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE_ID", type="integer", nullable=false)
     */
    private $typeId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="OLDSTATUS_ID", type="integer", nullable=true)
     */
    private $oldstatusId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SCENARIO_ID", type="integer", nullable=false)
     */
    private $scenarioId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS_ID", type="integer", nullable=false)
     */
    private $statusId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PRIORITY_ID", type="integer", nullable=false)
     */
    private $priorityId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATOR_ID", type="integer", nullable=true)
     */
    private $creatorId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATOR_ORGNODE_ID", type="integer", nullable=true)
     */
    private $creatorOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATOR_INFO", type="text", nullable=true)
     */
    private $creatorInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATOR_SHORT", type="text", nullable=true)
     */
    private $creatorShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTROL_TYPE", type="integer", nullable=true)
     */
    private $controlType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTROLER_ID", type="integer", nullable=true)
     */
    private $controlerId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTROLER_ORGNODE_ID", type="integer", nullable=true)
     */
    private $controlerOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTROLER_INFO", type="text", nullable=true)
     */
    private $controlerInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="CONTROLER_SHORT", type="text", nullable=true)
     */
    private $controlerShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="REGISTRATOR_ID", type="integer", nullable=true)
     */
    private $registratorId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="REGISTRATOR_ORGNODE_ID", type="integer", nullable=true)
     */
    private $registratorOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="REGISTRATOR_INFO", type="text", nullable=true)
     */
    private $registratorInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="REGISTRATOR_SHORT", type="text", nullable=true)
     */
    private $registratorShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORIG_ID", type="integer", nullable=false)
     */
    private $origId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORIG_ORGNODE_ID", type="integer", nullable=false)
     */
    private $origOrgnodeId;

    /**
     * @var string
     *
     * @ORM\Column(name="ORIG_INFO", type="text", length=16777215, nullable=false)
     */
    private $origInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="ORIG_SHORT", type="text", length=16777215, nullable=false)
     */
    private $origShort;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ARCHIVE", type="boolean", nullable=false)
     */
    private $archive = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="IN_ARCHIVE", type="string", length=2, nullable=false)
     */
    private $inArchive;

    /**
     * @var string
     *
     * @ORM\Column(name="IS_PUBLIC", type="string", length=2, nullable=false)
     */
    private $isPublic;

    /**
     * @var string
     *
     * @ORM\Column(name="EDOC", type="string", length=2, nullable=false)
     */
    private $edoc;

    /**
     * @var integer
     *
     * @ORM\Column(name="HEAD_PRIORITY", type="integer", nullable=false)
     */
    private $headPriority;

    /**
     * @var string
     *
     * @ORM\Column(name="FIRSTPARENT_SENDER", type="string", length=255, nullable=false)
     */
    private $firstparentSender;

    /**
     * @var string
     *
     * @ORM\Column(name="FIRSTPARENT_SHOWS", type="string", length=255, nullable=false)
     */
    private $firstparentShows;

    /**
     * @var string
     *
     * @ORM\Column(name="FIRSTPARENT_DOC_DATA", type="string", length=255, nullable=false)
     */
    private $firstparentDocData;

    /**
     * @var integer
     *
     * @ORM\Column(name="FIRSTPARENT_ID", type="integer", nullable=false)
     */
    private $firstparentId;

    /**
     * @var string
     *
     * @ORM\Column(name="DIGITAL_TIMESTAMP", type="string", length=5, nullable=false)
     */
    private $digitalTimestamp;

    /**
     * @var string
     *
     * @ORM\Column(name="TMPFIELD", type="string", length=255, nullable=false)
     */
    private $tmpfield = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="RTYPE_ID", type="integer", nullable=false)
     */
    private $rtypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="FOREIGN_ID", type="string", length=50, nullable=true)
     */
    private $foreignId;


}

