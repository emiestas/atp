<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsDocforwarding
 *
 * @ORM\Table(name="IDM_IDM_DHS_DOCFORWARDING", indexes={@ORM\Index(name="DOC_ID", columns={"DOC_ID"})})
 * @ORM\Entity
 */
class IdmDhsDocforwarding
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var string
     *
     * @ORM\Column(name="RESOLUTION_TEXT", type="string", length=100, nullable=false)
     */
    private $resolutionText = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FORWARD_DATE", type="datetime", nullable=false)
     */
    private $forwardDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXPIRE_DATE", type="datetime", nullable=false)
     */
    private $expireDate = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     */
    private $docId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="FORWARD_TEXT", type="string", length=20, nullable=true)
     */
    private $forwardText;

    /**
     * @var integer
     *
     * @ORM\Column(name="FORWARD_ID", type="integer", nullable=false)
     */
    private $forwardId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="FORWARD_INFO", type="text", length=65535, nullable=true)
     */
    private $forwardInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="FORWARD_SHORT", type="text", length=65535, nullable=true)
     */
    private $forwardShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="FORWARD_TYPE", type="integer", nullable=false)
     */
    private $forwardType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="FORWARD_ORGNODE_ID", type="integer", nullable=false)
     */
    private $forwardOrgnodeId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="FROM_ID", type="integer", nullable=false)
     */
    private $fromId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="FROM_INFO", type="text", length=65535, nullable=true)
     */
    private $fromInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="FROM_SHORT", type="text", length=65535, nullable=true)
     */
    private $fromShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="FROM_TYPE", type="integer", nullable=false)
     */
    private $fromType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="FROM_ORGNODE_ID", type="integer", nullable=false)
     */
    private $fromOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ACCOMPLISH_DESC", type="text", length=65535, nullable=false)
     */
    private $accomplishDesc;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ACCOMPLISH_DATE", type="datetime", nullable=false)
     */
    private $accomplishDate;


}

