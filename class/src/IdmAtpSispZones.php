<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpSispZones
 *
 * @ORM\Table(name="IDM_IDM_ATP_SISP_ZONES", uniqueConstraints={@ORM\UniqueConstraint(name="Zone", columns={"Zone"})})
 * @ORM\Entity
 */
class IdmAtpSispZones
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ZoneId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $zoneid;

    /**
     * @var string
     *
     * @ORM\Column(name="Zone", type="string", length=250, nullable=false)
     */
    private $zone;

    /**
     * @var string
     *
     * @ORM\Column(name="Tollage", type="decimal", precision=20, scale=2, nullable=false)
     */
    private $tollage = '0.00';


}

