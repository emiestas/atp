<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsClassification
 *
 * @ORM\Table(name="IDM_IDM_DHS_CLASSIFICATION", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsClassification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_MANDATORY", type="string", length=5, nullable=false)
     */
    private $isMandatory;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDRECEIVE", type="string", length=5, nullable=false)
     */
    private $sendreceive;

    /**
     * @var string
     *
     * @ORM\Column(name="ON_ACTION", type="string", length=100, nullable=false)
     */
    private $onAction = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CL_VALUES", type="text", length=65535, nullable=true)
     */
    private $clValues;

    /**
     * @var string
     *
     * @ORM\Column(name="CL_TYPE", type="string", length=100, nullable=false)
     */
    private $clType;

    /**
     * @var string
     *
     * @ORM\Column(name="CL_SYSNAME", type="string", length=100, nullable=false)
     */
    private $clSysname;

    /**
     * @var string
     *
     * @ORM\Column(name="CL_DEFAULT", type="string", length=255, nullable=false)
     */
    private $clDefault;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=true)
     */
    private $journalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CASE_ID", type="integer", nullable=true)
     */
    private $caseId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SCENARIO_ID", type="integer", nullable=true)
     */
    private $scenarioId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="JOURNAL_SEARCH", type="text", length=65535, nullable=false)
     */
    private $journalSearch;


}

