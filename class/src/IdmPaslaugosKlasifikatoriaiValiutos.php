<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosKlasifikatoriaiValiutos
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_KLASIFIKATORIAI_VALIUTOS", uniqueConstraints={@ORM\UniqueConstraint(name="FOREIGN_ID", columns={"FOREIGN_ID"})}, indexes={@ORM\Index(name="ID", columns={"ID_OLD"})})
 * @ORM\Entity
 */
class IdmPaslaugosKlasifikatoriaiValiutos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var string
     *
     * @ORM\Column(name="FOREIGN_ID", type="string", length=50, nullable=false)
     */
    private $foreignId;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=255, nullable=false)
     */
    private $shows;

    /**
     * @var integer
     *
     * @ORM\Column(name="DISABLED", type="integer", nullable=false)
     */
    private $disabled;


}

