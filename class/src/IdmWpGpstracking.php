<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWpGpstracking
 *
 * @ORM\Table(name="IDM_IDM_WP_GPSTRACKING", indexes={@ORM\Index(name="USER_ID", columns={"USER_ID"})})
 * @ORM\Entity
 */
class IdmWpGpstracking
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=true)
     */
    private $userId = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TRACKDATE", type="datetime", nullable=true)
     */
    private $trackdate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="IP", type="string", length=20, nullable=true)
     */
    private $ip = '';

    /**
     * @var string
     *
     * @ORM\Column(name="GISPOINT", type="string", length=255, nullable=true)
     */
    private $gispoint = '';

    /**
     * @var string
     *
     * @ORM\Column(name="GISLOCATION", type="string", length=255, nullable=true)
     */
    private $gislocation = '';


}

