<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsJournalregistrator
 *
 * @ORM\Table(name="IDM_IDM_DHS_JOURNALREGISTRATOR", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsJournalregistrator
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="REGISTRATOR_ID", type="integer", nullable=false)
     */
    private $registratorId = '0';


}

