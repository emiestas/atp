<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWpSettings
 *
 * @ORM\Table(name="IDM_IDM_WP_SETTINGS", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmWpSettings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SOFT_ID", type="integer", nullable=false)
     */
    private $softId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="POSITION", type="integer", nullable=true)
     */
    private $position = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="VISIBLE", type="string", length=5, nullable=true)
     */
    private $visible = '';


}

