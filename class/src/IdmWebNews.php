<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWebNews
 *
 * @ORM\Table(name="IDM_IDM_WEB_NEWS", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmWebNews
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=false)
     */
    private $shows;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="NEWS_DATE", type="datetime", nullable=false)
     */
    private $newsDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="TEXT_DATA", type="text", nullable=false)
     */
    private $textData;

    /**
     * @var integer
     *
     * @ORM\Column(name="LANGUAGE_ID", type="integer", nullable=true)
     */
    private $languageId = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FROM_DATE", type="datetime", nullable=true)
     */
    private $fromDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXP_DATE", type="datetime", nullable=true)
     */
    private $expDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="IMAGE_URL", type="string", length=255, nullable=true)
     */
    private $imageUrl = '';

    /**
     * @var string
     *
     * @ORM\Column(name="VISIBLE", type="string", length=5, nullable=true)
     */
    private $visible = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SEND", type="string", length=1, nullable=true)
     */
    private $send = '';


}

