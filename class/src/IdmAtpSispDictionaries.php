<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpSispDictionaries
 *
 * @ORM\Table(name="IDM_IDM_ATP_SISP_DICTIONARIES")
 * @ORM\Entity
 */
class IdmAtpSispDictionaries
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=20, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="Text", type="string", length=200, nullable=false)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="ParentName", type="string", length=20, nullable=true)
     */
    private $parentname;

    /**
     * @var integer
     *
     * @ORM\Column(name="didParent", type="integer", nullable=true)
     */
    private $didparent;

    /**
     * @var integer
     *
     * @ORM\Column(name="SeqNum", type="integer", nullable=true)
     */
    private $seqnum;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="ShortText", type="string", length=20, nullable=true)
     */
    private $shorttext;

    /**
     * @var string
     *
     * @ORM\Column(name="TelNr", type="string", length=20, nullable=true)
     */
    private $telnr;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Banklinkable", type="boolean", nullable=false)
     */
    private $banklinkable;


}

