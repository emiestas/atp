<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWpSignature
 *
 * @ORM\Table(name="IDM_IDM_WP_SIGNATURE")
 * @ORM\Entity
 */
class IdmWpSignature
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="HASH", type="text", length=16777215, nullable=false)
     */
    private $hash;


}

