<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsUserrole
 *
 * @ORM\Table(name="IDM_IDM_DHS_USERROLE", indexes={@ORM\Index(name="USER_ID", columns={"USER_ID"})})
 * @ORM\Entity
 */
class IdmDhsUserrole
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="HIDDEN", type="string", length=10, nullable=false)
     */
    private $hidden;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=true)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=false)
     */
    private $userId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ROLE_ID", type="integer", nullable=false)
     */
    private $roleId = '0';


}

