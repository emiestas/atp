<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsTasks
 *
 * @ORM\Table(name="IDM_IDM_PHS_TASKS", indexes={@ORM\Index(name="STATUS_ID", columns={"STATUS_ID"}), @ORM\Index(name="TASK_ID", columns={"PARENT_ID"}), @ORM\Index(name="PROJECT_ID", columns={"PROJECT_ID"}), @ORM\Index(name="DHSDOC_ID", columns={"DHSDOC_ID"}), @ORM\Index(name="NOTE_ID", columns={"NOTE_ID"}), @ORM\Index(name="STAGE_ID", columns={"STAGE_ID"})})
 * @ORM\Entity
 */
class IdmPhsTasks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=65535, nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_DATA", type="text", length=65535, nullable=false)
     */
    private $docData;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_NOTES", type="text", length=65535, nullable=false)
     */
    private $docNotes;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_ACCOMPLISH", type="text", length=65535, nullable=false)
     */
    private $docAccomplish;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ID", type="integer", nullable=true)
     */
    private $receiverId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ORGNODE_ID", type="integer", nullable=true)
     */
    private $receiverOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", length=65535, nullable=true)
     */
    private $receiverInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", length=65535, nullable=true)
     */
    private $receiverShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATOR_ID", type="integer", nullable=true)
     */
    private $creatorId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATOR_ORGNODE_ID", type="integer", nullable=true)
     */
    private $creatorOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATOR_INFO", type="text", length=65535, nullable=true)
     */
    private $creatorInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATOR_SHORT", type="text", length=65535, nullable=true)
     */
    private $creatorShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTROLER_ID", type="integer", nullable=true)
     */
    private $controlerId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTROLER_ORGNODE_ID", type="integer", nullable=true)
     */
    private $controlerOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTROLER_INFO", type="text", length=65535, nullable=true)
     */
    private $controlerInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="CONTROLER_SHORT", type="text", length=65535, nullable=true)
     */
    private $controlerShort;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXPIRE_DATE", type="datetime", nullable=true)
     */
    private $expireDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ACCOMPLISH_DATE", type="datetime", nullable=true)
     */
    private $accomplishDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CHECK_DATE", type="datetime", nullable=true)
     */
    private $checkDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=true)
     */
    private $createDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="PROJECT_ID", type="integer", nullable=true)
     */
    private $projectId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS_ID", type="integer", nullable=true)
     */
    private $statusId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="STAGE_ID", type="integer", nullable=true)
     */
    private $stageId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=true)
     */
    private $parentId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PROCESS_ID", type="integer", nullable=true)
     */
    private $processId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="DHSDOC_ID", type="string", length=255, nullable=false)
     */
    private $dhsdocId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="DHSDOC_NO", type="string", length=100, nullable=false)
     */
    private $dhsdocNo;

    /**
     * @var integer
     *
     * @ORM\Column(name="NOTE_ID", type="integer", nullable=false)
     */
    private $noteId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PRIORITY_ID", type="integer", nullable=false)
     */
    private $priorityId;

    /**
     * @var string
     *
     * @ORM\Column(name="ISLATE", type="string", length=5, nullable=false)
     */
    private $islate;

    /**
     * @var string
     *
     * @ORM\Column(name="WASLATE", type="string", length=5, nullable=false)
     */
    private $waslate;


}

