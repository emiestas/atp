<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTbl36
 *
 * @ORM\Table(name="IDM_IDM_ATP_TBL_36", indexes={@ORM\Index(name="RECORD_LAST", columns={"RECORD_LAST"}), @ORM\Index(name="RECORD_MAIN_ID", columns={"RECORD_MAIN_ID"})})
 * @ORM\Entity
 */
class IdmAtpTbl36
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_MAIN_ID", type="string", length=25, nullable=false)
     */
    private $recordMainId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_VERSION", type="integer", nullable=false)
     */
    private $recordVersion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RECORD_LAST", type="boolean", nullable=false)
     */
    private $recordLast;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_0", type="date", nullable=false)
     */
    private $col0;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_1", type="text", length=255, nullable=false)
     */
    private $col1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_37", type="date", nullable=false)
     */
    private $col37;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_38", type="time", nullable=false)
     */
    private $col38;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_39", type="text", length=255, nullable=false)
     */
    private $col39;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_60", type="text", length=65535, nullable=false)
     */
    private $col60;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_61", type="text", length=65535, nullable=false)
     */
    private $col61;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_62", type="text", length=65535, nullable=false)
     */
    private $col62;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_66", type="text", length=255, nullable=false)
     */
    private $col66;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_80", type="text", length=255, nullable=false)
     */
    private $col80;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_81", type="text", length=255, nullable=false)
     */
    private $col81;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_82", type="text", length=255, nullable=false)
     */
    private $col82;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_86", type="text", length=255, nullable=false)
     */
    private $col86;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_87", type="text", length=255, nullable=false)
     */
    private $col87;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_88", type="text", length=255, nullable=false)
     */
    private $col88;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_89", type="date", nullable=false)
     */
    private $col89;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_90", type="time", nullable=false)
     */
    private $col90;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_91", type="text", length=255, nullable=false)
     */
    private $col91;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_92", type="time", nullable=false)
     */
    private $col92;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_93", type="text", length=255, nullable=false)
     */
    private $col93;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_94", type="text", length=255, nullable=false)
     */
    private $col94;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_95", type="text", length=255, nullable=false)
     */
    private $col95;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_96", type="text", length=255, nullable=false)
     */
    private $col96;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_97", type="text", length=255, nullable=false)
     */
    private $col97;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_98", type="text", length=255, nullable=false)
     */
    private $col98;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_99", type="text", length=255, nullable=false)
     */
    private $col99;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_100", type="text", length=255, nullable=false)
     */
    private $col100;

    /**
     * @var integer
     *
     * @ORM\Column(name="COL_101", type="integer", nullable=false)
     */
    private $col101;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_102", type="text", length=255, nullable=false)
     */
    private $col102;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_103", type="text", length=255, nullable=false)
     */
    private $col103;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_104", type="text", length=255, nullable=false)
     */
    private $col104;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_105", type="text", length=255, nullable=false)
     */
    private $col105;

    /**
     * @var integer
     *
     * @ORM\Column(name="COL_106", type="integer", nullable=false)
     */
    private $col106;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_107", type="text", length=255, nullable=false)
     */
    private $col107;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_108", type="text", length=255, nullable=false)
     */
    private $col108;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;


}

