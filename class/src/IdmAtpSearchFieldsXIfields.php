<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpSearchFieldsXIfields
 *
 * @ORM\Table(name="IDM_IDM_ATP_SEARCH_FIELDS_X_IFIELDS", indexes={@ORM\Index(name="fk_IDM_ATP_SEARCH_FIELDS_X_FIELDS_IDM_ATP_SEARCH_FIELDS1_idx", columns={"SEARCH_FIELD_ID"})})
 * @ORM\Entity
 */
class IdmAtpSearchFieldsXIfields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="FIELD_ID", type="integer", nullable=false)
     */
    private $fieldId;

    /**
     * @var string
     *
     * @ORM\Column(name="ITYPE", type="string", length=20, nullable=false)
     */
    private $itype;

    /**
     * @var \IdmAtpSearchFields
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="IdmAtpSearchFields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SEARCH_FIELD_ID", referencedColumnName="ID")
     * })
     */
    private $searchField;


}

