<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmCrmEventNotification
 *
 * @ORM\Table(name="IDM_IDM_CRM_EVENT_NOTIFICATION", indexes={@ORM\Index(name="OWNER", columns={"OWNER"})})
 * @ORM\Entity
 */
class IdmCrmEventNotification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="OWNER", type="integer", nullable=false)
     */
    private $owner;

    /**
     * @var integer
     *
     * @ORM\Column(name="EVENT_ID", type="integer", nullable=false)
     */
    private $eventId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATED", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="VIEWED", type="datetime", nullable=true)
     */
    private $viewed;


}

