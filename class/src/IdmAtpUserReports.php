<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpUserReports
 *
 * @ORM\Table(name="IDM_IDM_ATP_USER_REPORTS")
 * @ORM\Entity
 */
class IdmAtpUserReports
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId;

    /**
     * @var integer
     *
     * @ORM\Column(name="DEPARTMENT_ID", type="integer", nullable=false)
     */
    private $departmentId;

    /**
     * @var string
     *
     * @ORM\Column(name="PARAMETERS", type="text", length=65535, nullable=true)
     */
    private $parameters;


}

