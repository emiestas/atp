<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmReservationObjectPhoto
 *
 * @ORM\Table(name="IDM_IDM_RESERVATION_OBJECT_PHOTO", indexes={@ORM\Index(name="NAME", columns={"NAME"}), @ORM\Index(name="OBJECT_ID", columns={"OBJECT_ID"})})
 * @ORM\Entity
 */
class IdmReservationObjectPhoto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="OBJECT_ID", type="integer", nullable=false)
     */
    private $objectId;


}

