<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpAtprAtpDocumentXref
 *
 * @ORM\Table(name="IDM_IDM_ATP_ATPR_ATP_DOCUMENT_XREF", indexes={@ORM\Index(name="IDX_595801A8FB33BE9C", columns={"ROIK"}), @ORM\Index(name="IDX_595801A894C3C196", columns={"FILE_ID"})})
 * @ORM\Entity
 */
class IdmAtpAtprAtpDocumentXref
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \IdmAtpFiles
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpFiles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FILE_ID", referencedColumnName="ID")
     * })
     */
    private $file;

    /**
     * @var \IdmAtpAtprAtp
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpAtprAtp")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ROIK", referencedColumnName="ROIK")
     * })
     */
    private $roik;


}

