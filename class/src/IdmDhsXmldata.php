<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsXmldata
 *
 * @ORM\Table(name="IDM_IDM_DHS_XMLDATA")
 * @ORM\Entity
 */
class IdmDhsXmldata
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="XML_DATA", type="text", nullable=false)
     */
    private $xmlData;


}

