<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTmplConditionParam
 *
 * @ORM\Table(name="IDM_IDM_ATP_TMPL_CONDITION_PARAM")
 * @ORM\Entity
 */
class IdmAtpTmplConditionParam
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="COMPARISON_TYPE", type="string", length=6, nullable=false)
     */
    private $comparisonType;

    /**
     * @var integer
     *
     * @ORM\Column(name="ACTION_PARAMETER_ID", type="integer", nullable=false)
     */
    private $actionParameterId;

    /**
     * @var string
     *
     * @ORM\Column(name="VALUE", type="string", length=45, nullable=true)
     */
    private $value;


}

