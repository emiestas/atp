<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMainCity
 *
 * @ORM\Table(name="IDM_IDM_MAIN_CITY")
 * @ORM\Entity
 */
class IdmMainCity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="COUNTRY_ID", type="integer", nullable=false)
     */
    private $countryId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CITY", type="string", length=100, nullable=false)
     */
    private $city = '';


}

