<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAdmPerms
 *
 * @ORM\Table(name="IDM_IDM_ADM_PERMS", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmAdmPerms
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=50, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PREAD", type="string", length=1, nullable=false)
     */
    private $pread = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PINSERT", type="string", length=1, nullable=false)
     */
    private $pinsert = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PUPDATE", type="string", length=1, nullable=false)
     */
    private $pupdate = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PDELETE", type="string", length=1, nullable=false)
     */
    private $pdelete = '';


}

