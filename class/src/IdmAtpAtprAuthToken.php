<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpAtprAuthToken
 *
 * @ORM\Table(name="IDM_IDM_ATP_ATPR_AUTH_TOKEN", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_8591E60A2C39FB69FB33BE9C", columns={"ATPR_VARTOTOJO_VARDAS", "ROIK"})}, indexes={@ORM\Index(name="IDX_8591E60AFB33BE9C", columns={"ROIK"})})
 * @ORM\Entity
 */
class IdmAtpAtprAuthToken
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ATPR_VARTOTOJO_VARDAS", type="string", length=255, nullable=false)
     */
    private $atprVartotojoVardas;

    /**
     * @var string
     *
     * @ORM\Column(name="PRE_AUTH_TOKEN", type="string", length=255, nullable=false)
     */
    private $preAuthToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="GALIOJIMO_PABAIGOS_DATA", type="datetime", nullable=true)
     */
    private $galiojimoPabaigosData;

    /**
     * @var string
     *
     * @ORM\Column(name="ATPR_ISORINIS_URL", type="string", length=500, nullable=true)
     */
    private $atprIsorinisUrl;

    /**
     * @var \IdmAtpAtprAtp
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpAtprAtp")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ROIK", referencedColumnName="ROIK")
     * })
     */
    private $roik;


}

