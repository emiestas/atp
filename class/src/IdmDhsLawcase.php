<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsLawcase
 *
 * @ORM\Table(name="IDM_IDM_DHS_LAWCASE")
 * @ORM\Entity
 */
class IdmDhsLawcase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     */
    private $docId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ITEM_NO", type="string", length=255, nullable=false)
     */
    private $itemNo = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CASE_NO", type="string", length=255, nullable=false)
     */
    private $caseNo = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ACCOMPLISH_DATE", type="datetime", nullable=false)
     */
    private $accomplishDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXPIRE_DATE", type="datetime", nullable=false)
     */
    private $expireDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="REASON", type="string", length=255, nullable=false)
     */
    private $reason = '';


}

