<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsFinancecriteria
 *
 * @ORM\Table(name="IDM_IDM_PHS_FINANCECRITERIA")
 * @ORM\Entity
 */
class IdmPhsFinancecriteria
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=255, nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="NO", type="string", length=255, nullable=false)
     */
    private $no;

    /**
     * @var string
     *
     * @ORM\Column(name="PROGRAM_COST", type="string", length=255, nullable=false)
     */
    private $programCost;

    /**
     * @var string
     *
     * @ORM\Column(name="PROGRAM_DESC", type="text", length=65535, nullable=false)
     */
    private $programDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="PROGRAM_VALUE", type="string", length=255, nullable=false)
     */
    private $programValue;

    /**
     * @var string
     *
     * @ORM\Column(name="INPUT_TYPE", type="string", length=50, nullable=false)
     */
    private $inputType;

    /**
     * @var integer
     *
     * @ORM\Column(name="ADD_VALUE", type="integer", nullable=false)
     */
    private $addValue = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="MULTIPLYER", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $multiplyer = '0.00';


}

