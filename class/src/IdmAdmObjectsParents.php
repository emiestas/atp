<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAdmObjectsParents
 *
 * @ORM\Table(name="IDM_IDM_ADM_OBJECTS_PARENTS")
 * @ORM\Entity
 */
class IdmAdmObjectsParents
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PARENT_DESC", type="string", length=255, nullable=true)
     */
    private $parentDesc = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ICON_OPEN", type="string", length=255, nullable=true)
     */
    private $iconOpen = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ICON_CLOSE", type="string", length=255, nullable=true)
     */
    private $iconClose = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_OPEN", type="string", length=5, nullable=true)
     */
    private $isOpen = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PREFIX", type="string", length=255, nullable=true)
     */
    private $prefix = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="POSITION", type="integer", nullable=true)
     */
    private $position = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="VISIBLE", type="string", length=1, nullable=false)
     */
    private $visible = '';


}

