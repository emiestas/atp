<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsMystar
 *
 * @ORM\Table(name="IDM_IDM_DHS_MYSTAR", indexes={@ORM\Index(name="DOC_ID", columns={"DOC_ID"}), @ORM\Index(name="PEOPLE_ID", columns={"PEOPLE_ID"})})
 * @ORM\Entity
 */
class IdmDhsMystar
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     */
    private $docId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';


}

