<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTableStructure
 *
 * @ORM\Table(name="IDM_IDM_ATP_TABLE_STRUCTURE", indexes={@ORM\Index(name="TABLE_ID", columns={"ITEM_ID"}), @ORM\Index(name="ITEM_ID", columns={"ITEM_ID", "ITYPE"}), @ORM\Index(name="ITYPE", columns={"ITYPE"}), @ORM\Index(name="ITYPE_2", columns={"ITYPE", "ORD"}), @ORM\Index(name="ITEM_ID_2", columns={"ITEM_ID", "ITYPE", "NAME"})})
 * @ORM\Entity
 */
class IdmAtpTableStructure
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ITEM_ID", type="integer", nullable=false)
     */
    private $itemId;

    /**
     * @var string
     *
     * @ORM\Column(name="ITYPE", type="string", length=25, nullable=false)
     */
    private $itype = 'DOC';

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=75, nullable=false)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=25, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="SORT", type="integer", nullable=false)
     */
    private $sort;

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="SOURCE", type="integer", nullable=false)
     */
    private $source;

    /**
     * @var boolean
     *
     * @ORM\Column(name="WS", type="boolean", nullable=false)
     */
    private $ws = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="VALUE", type="integer", nullable=false)
     */
    private $value;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VALUE_TYPE", type="boolean", nullable=false)
     */
    private $valueType;

    /**
     * @var integer
     *
     * @ORM\Column(name="OTHER_SOURCE", type="integer", nullable=false)
     */
    private $otherSource;

    /**
     * @var integer
     *
     * @ORM\Column(name="OTHER_VALUE", type="integer", nullable=false)
     */
    private $otherValue;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FILL", type="boolean", nullable=false)
     */
    private $fill;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VISIBILITY", type="boolean", nullable=false)
     */
    private $visibility;

    /**
     * @var boolean
     *
     * @ORM\Column(name="SEARCHABLE", type="boolean", nullable=true)
     */
    private $searchable;

    /**
     * @var integer
     *
     * @ORM\Column(name="RELATION", type="integer", nullable=false)
     */
    private $relation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="MANDATORY", type="boolean", nullable=false)
     */
    private $mandatory;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VISIBLE", type="boolean", nullable=false)
     */
    private $visible;

    /**
     * @var boolean
     *
     * @ORM\Column(name="COMPETENCE", type="boolean", nullable=false)
     */
    private $competence;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORD", type="integer", nullable=false)
     */
    private $ord;

    /**
     * @var string
     *
     * @ORM\Column(name="TEXT", type="text", length=65535, nullable=false)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="DEFAULT", type="string", length=1000, nullable=false)
     */
    private $default;

    /**
     * @var integer
     *
     * @ORM\Column(name="DEFAULT_ID", type="integer", nullable=true)
     */
    private $defaultId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="UPDATE_DATE", type="datetime", nullable=false)
     */
    private $updateDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;


}

