<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmIdbValue
 *
 * @ORM\Table(name="IDM_IDM_IDB_VALUE", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmIdbValue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="FIELD_ID", type="integer", nullable=false)
     */
    private $fieldId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="HTML_VALUE", type="text", nullable=false)
     */
    private $htmlValue;

    /**
     * @var string
     *
     * @ORM\Column(name="VCH_VALUE", type="text", nullable=true)
     */
    private $vchValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="INT_VALUE", type="integer", nullable=true)
     */
    private $intValue;

    /**
     * @var binary
     *
     * @ORM\Column(name="DECIMAL_VALUE", type="binary", nullable=true)
     */
    private $decimalValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_VALUE", type="date", nullable=true)
     */
    private $dateValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TIME_VALUE", type="time", nullable=true)
     */
    private $timeValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ITEM_DATE", type="datetime", nullable=false)
     */
    private $itemDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="ITEM_HASH", type="string", length=32, nullable=false)
     */
    private $itemHash = '';


}

