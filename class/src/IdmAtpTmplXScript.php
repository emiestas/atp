<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTmplXScript
 *
 * @ORM\Table(name="IDM_IDM_ATP_TMPL_X_SCRIPT", indexes={@ORM\Index(name="fk_IDM_ATP_TMPL_X_SCRIPT_IDM_ATP_TABLE_TEMPLATE1_idx", columns={"TMPL_ID"})})
 * @ORM\Entity
 */
class IdmAtpTmplXScript
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SCRIPT_NAME", type="string", length=50, nullable=false)
     */
    private $scriptName;

    /**
     * @var \IdmAtpTableTemplate
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpTableTemplate")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TMPL_ID", referencedColumnName="ID")
     * })
     */
    private $tmpl;


}

