<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmSchScheduleIntervals
 *
 * @ORM\Table(name="IDM_IDM_SCH_SCHEDULE_INTERVALS", indexes={@ORM\Index(name="fk_IDM_SCH_SCHEDULE_INTERVALS_IDM_SCH_SCHEDULES1_idx", columns={"SCHEDULE_ID"})})
 * @ORM\Entity
 */
class IdmSchScheduleIntervals
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="SCHEDULE_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $scheduleId;

    /**
     * @var string
     *
     * @ORM\Column(name="ITEM", type="string", length=45, nullable=false)
     */
    private $item;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FROM", type="time", nullable=false)
     */
    private $from;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TILL", type="time", nullable=false)
     */
    private $till;


}

