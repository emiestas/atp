<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmRemFolder
 *
 * @ORM\Table(name="IDM_IDM_REM_FOLDER")
 * @ORM\Entity
 */
class IdmRemFolder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="FOLDER", type="string", length=255, nullable=false)
     */
    private $folder = '';


}

