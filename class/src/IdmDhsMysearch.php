<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsMysearch
 *
 * @ORM\Table(name="IDM_IDM_DHS_MYSEARCH", indexes={@ORM\Index(name="PEOPLE_ID", columns={"PEOPLE_ID"})})
 * @ORM\Entity
 */
class IdmDhsMysearch
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCH_TEXT", type="text", length=16777215, nullable=false)
     */
    private $searchText;

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCH_URL", type="text", length=16777215, nullable=false)
     */
    private $searchUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ADDED_DATE", type="datetime", nullable=false)
     */
    private $addedDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="REMEMBER", type="string", length=5, nullable=false)
     */
    private $remember = '';

    /**
     * @var string
     *
     * @ORM\Column(name="HASH", type="string", length=32, nullable=false)
     */
    private $hash;


}

