<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosStatusai
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_STATUSAI", uniqueConstraints={@ORM\UniqueConstraint(name="_NAME", columns={"NAME"})})
 * @ORM\Entity
 */
class IdmPaslaugosStatusai
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=50, nullable=false)
     */
    private $name;


}

