<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpSispTows
 *
 * @ORM\Table(name="IDM_IDM_ATP_SISP_TOWS", uniqueConstraints={@ORM\UniqueConstraint(name="Tows_TowDocNum_Unique", columns={"TowDocNum"})}, indexes={@ORM\Index(name="IX_Tows_PlateNumber", columns={"PlateNumber"}), @ORM\Index(name="IX_Tows_RegisterTime", columns={"RegisterTime"})})
 * @ORM\Entity
 */
class IdmAtpSispTows
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="TowDocNum", type="string", length=10, nullable=true)
     */
    private $towdocnum;

    /**
     * @var integer
     *
     * @ORM\Column(name="didTowStatus", type="integer", nullable=true)
     */
    private $didtowstatus = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RegisterTime", type="datetime", nullable=true)
     */
    private $registertime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ContactTime", type="datetime", nullable=true)
     */
    private $contacttime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ReleaseTime", type="datetime", nullable=true)
     */
    private $releasetime;

    /**
     * @var string
     *
     * @ORM\Column(name="PlateNumber", type="string", length=50, nullable=true)
     */
    private $platenumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="didCarModel", type="integer", nullable=true)
     */
    private $didcarmodel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AllowRelease", type="boolean", nullable=false)
     */
    private $allowrelease = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="didAddressCity", type="integer", nullable=true)
     */
    private $didaddresscity;

    /**
     * @var string
     *
     * @ORM\Column(name="AddressStreet", type="string", length=100, nullable=true)
     */
    private $addressstreet;

    /**
     * @var string
     *
     * @ORM\Column(name="AddressNumber", type="string", length=100, nullable=true)
     */
    private $addressnumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="didTowTruck", type="integer", nullable=true)
     */
    private $didtowtruck;

    /**
     * @var integer
     *
     * @ORM\Column(name="didTowDriver", type="integer", nullable=true)
     */
    private $didtowdriver;

    /**
     * @var string
     *
     * @ORM\Column(name="Fine", type="decimal", precision=19, scale=4, nullable=true)
     */
    private $fine;

    /**
     * @var string
     *
     * @ORM\Column(name="OwnerName", type="string", length=100, nullable=true)
     */
    private $ownername;

    /**
     * @var string
     *
     * @ORM\Column(name="ScannedDocPath", type="string", length=200, nullable=true)
     */
    private $scanneddocpath;

    /**
     * @var integer
     *
     * @ORM\Column(name="idLastChangeUser", type="integer", nullable=true)
     */
    private $idlastchangeuser;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IsStolen", type="boolean", nullable=true)
     */
    private $isstolen;

    /**
     * @var string
     *
     * @ORM\Column(name="StolenExplanation", type="string", length=200, nullable=true)
     */
    private $stolenexplanation;

    /**
     * @var string
     *
     * @ORM\Column(name="CarComment", type="text", length=65535, nullable=true)
     */
    private $carcomment;

    /**
     * @var string
     *
     * @ORM\Column(name="FaultCode1", type="string", length=10, nullable=true)
     */
    private $faultcode1;

    /**
     * @var string
     *
     * @ORM\Column(name="FaultCode2", type="string", length=10, nullable=true)
     */
    private $faultcode2;

    /**
     * @var string
     *
     * @ORM\Column(name="Coordinates", type="string", length=50, nullable=true)
     */
    private $coordinates;

    /**
     * @var string
     *
     * @ORM\Column(name="txtSelectedFaultCode", type="string", length=100, nullable=true)
     */
    private $txtselectedfaultcode;

    /**
     * @var string
     *
     * @ORM\Column(name="PrasymasDocPath", type="text", length=65535, nullable=true)
     */
    private $prasymasdocpath;

    /**
     * @var string
     *
     * @ORM\Column(name="AtsakymasDocPath", type="text", length=65535, nullable=true)
     */
    private $atsakymasdocpath;

    /**
     * @var string
     *
     * @ORM\Column(name="CoordinatesLat", type="string", length=50, nullable=true)
     */
    private $coordinateslat;

    /**
     * @var string
     *
     * @ORM\Column(name="CoordinatesLon", type="string", length=50, nullable=true)
     */
    private $coordinateslon;

    /**
     * @var integer
     *
     * @ORM\Column(name="ZoneId", type="integer", nullable=true)
     */
    private $zoneid;


}

