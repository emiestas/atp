<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsVisatemplate
 *
 * @ORM\Table(name="IDM_IDM_DHS_VISATEMPLATE")
 * @ORM\Entity
 */
class IdmDhsVisatemplate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=255, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CONTROL_TYPE", type="string", length=32, nullable=false)
     */
    private $controlType;


}

