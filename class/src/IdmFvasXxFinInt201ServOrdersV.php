<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmFvasXxFinInt201ServOrdersV
 *
 * @ORM\Table(name="IDM_IDM_FVAS_XX_FIN_INT201_SERV_ORDERS_V")
 * @ORM\Entity
 */
class IdmFvasXxFinInt201ServOrdersV
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ORDER_INT_ID", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $orderIntId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORDER_ID", type="integer", nullable=true)
     */
    private $orderId;

    /**
     * @var string
     *
     * @ORM\Column(name="ORDER_DATE", type="string", length=4000, nullable=true)
     */
    private $orderDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="SERVICE_ID", type="integer", nullable=true)
     */
    private $serviceId;

    /**
     * @var string
     *
     * @ORM\Column(name="PAYER_CODE", type="string", length=4000, nullable=true)
     */
    private $payerCode;

    /**
     * @var string
     *
     * @ORM\Column(name="AMOUNT", type="decimal", precision=14, scale=4, nullable=true)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="CURRENCY_CODE", type="string", length=4000, nullable=true)
     */
    private $currencyCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="REQUEST_ID", type="integer", nullable=true)
     */
    private $requestId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATION_DATE", type="datetime", nullable=true)
     */
    private $creationDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATED_BY", type="integer", nullable=true)
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LAST_UPDATE_DATE", type="datetime", nullable=true)
     */
    private $lastUpdateDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="LAST_UPDATED_BY", type="integer", nullable=true)
     */
    private $lastUpdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="LAST_UPDATE_LOGIN", type="integer", nullable=true)
     */
    private $lastUpdateLogin;


}

