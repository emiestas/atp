<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsStages
 *
 * @ORM\Table(name="IDM_IDM_PHS_STAGES", indexes={@ORM\Index(name="PARENT_ID", columns={"PARENT_ID"}), @ORM\Index(name="PROJECT_ID", columns={"PROJECT_ID"})})
 * @ORM\Entity
 */
class IdmPhsStages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=true)
     */
    private $parentId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="STAGETYPE_ID", type="integer", nullable=false)
     */
    private $stagetypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="STAGENO", type="string", length=100, nullable=false)
     */
    private $stageno;

    /**
     * @var integer
     *
     * @ORM\Column(name="PROJECT_ID", type="integer", nullable=true)
     */
    private $projectId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=65535, nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", length=65535, nullable=true)
     */
    private $receiverInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", length=65535, nullable=true)
     */
    private $receiverShort;

    /**
     * @var string
     *
     * @ORM\Column(name="EXPIRE_FORMULA", type="string", length=255, nullable=true)
     */
    private $expireFormula = '';

    /**
     * @var string
     *
     * @ORM\Column(name="EXPIRE_TYPE", type="string", length=100, nullable=true)
     */
    private $expireType = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXPIRE_DATE", type="datetime", nullable=true)
     */
    private $expireDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXPIRE_PLANDATE", type="datetime", nullable=false)
     */
    private $expirePlandate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="START_FORCEDATE", type="datetime", nullable=true)
     */
    private $startForcedate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CHECK_DATE", type="datetime", nullable=false)
     */
    private $checkDate = '2007-01-01 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATOR_ID", type="integer", nullable=true)
     */
    private $creatorId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATOR_ORGNODE_ID", type="integer", nullable=true)
     */
    private $creatorOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATOR_INFO", type="text", length=65535, nullable=true)
     */
    private $creatorInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATOR_SHORT", type="text", length=65535, nullable=true)
     */
    private $creatorShort;

    /**
     * @var string
     *
     * @ORM\Column(name="RECURSION_FORMULA", type="string", length=100, nullable=true)
     */
    private $recursionFormula = '';

    /**
     * @var string
     *
     * @ORM\Column(name="RECURSION_TYPE", type="string", length=100, nullable=true)
     */
    private $recursionType = '';

    /**
     * @var string
     *
     * @ORM\Column(name="RECURSION_TIME", type="string", length=100, nullable=true)
     */
    private $recursionTime = '';

    /**
     * @var string
     *
     * @ORM\Column(name="TEXT_DATA", type="text", length=65535, nullable=true)
     */
    private $textData;

    /**
     * @var integer
     *
     * @ORM\Column(name="PROCESSCLASS_ID", type="integer", nullable=true)
     */
    private $processclassId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_START", type="string", length=100, nullable=true)
     */
    private $isStart = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_LATE", type="string", length=100, nullable=true)
     */
    private $isLate = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IS_END", type="string", length=100, nullable=true)
     */
    private $isEnd = '';


}

