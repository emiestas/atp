<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmApiToken
 *
 * @ORM\Table(name="IDM_IDM_API_TOKEN", uniqueConstraints={@ORM\UniqueConstraint(name="TOKEN", columns={"TOKEN"})})
 * @ORM\Entity
 */
class IdmApiToken
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="TOKEN", type="string", length=32, nullable=false)
     */
    private $token;

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FOREIGN_CONTACT", type="boolean", nullable=false)
     */
    private $foreignContact = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="REQUESTED_SYSTEM", type="string", length=32, nullable=false)
     */
    private $requestedSystem;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXPIRE_TIME", type="integer", nullable=false)
     */
    private $expireTime;


}

