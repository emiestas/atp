<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderPayments
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_PAYMENTS", indexes={@ORM\Index(name="_ORDER_ID", columns={"ORDER_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderPayments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="PAID_SUM", type="decimal", precision=14, scale=2, nullable=false)
     */
    private $paidSum;

    /**
     * @var string
     *
     * @ORM\Column(name="PAID_CURRENCY", type="string", length=3, nullable=false)
     */
    private $paidCurrency;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="PAYMENT_DATE", type="datetime", nullable=false)
     */
    private $paymentDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="PAYMENT_CODE", type="integer", nullable=true)
     */
    private $paymentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="PAYMENT_NAME", type="string", length=256, nullable=true)
     */
    private $paymentName;

    /**
     * @var string
     *
     * @ORM\Column(name="PAYMENT_DETAILS", type="string", length=256, nullable=true)
     */
    private $paymentDetails;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_ACCOUNT_NO", type="string", length=32, nullable=true)
     */
    private $receiverAccountNo;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_NAME", type="string", length=256, nullable=true)
     */
    private $receiverName;

    /**
     * @var string
     *
     * @ORM\Column(name="PAYER_ACCOUNT_NO", type="string", length=32, nullable=true)
     */
    private $payerAccountNo;

    /**
     * @var string
     *
     * @ORM\Column(name="PAYER_NAME", type="string", length=256, nullable=true)
     */
    private $payerName;

    /**
     * @var string
     *
     * @ORM\Column(name="PAYER_CODE", type="string", length=64, nullable=true)
     */
    private $payerCode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PAYMENT_STATUS", type="boolean", nullable=false)
     */
    private $paymentStatus;

    /**
     * @var \IdmPaslaugosUzsakymai
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosUzsakymai")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ORDER_ID", referencedColumnName="ID")
     * })
     */
    private $order;


}

