<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpRecordPayments
 *
 * @ORM\Table(name="IDM_IDM_ATP_RECORD_PAYMENTS")
 * @ORM\Entity
 */
class IdmAtpRecordPayments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCUMENT_ID", type="integer", nullable=false)
     */
    private $documentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_ID", type="integer", nullable=false)
     */
    private $recordId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PAYMENT_ID", type="integer", nullable=false)
     */
    private $paymentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PAID", type="integer", nullable=false)
     */
    private $paid;


}

