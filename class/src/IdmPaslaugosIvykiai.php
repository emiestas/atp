<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosIvykiai
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_IVYKIAI")
 * @ORM\Entity
 */
class IdmPaslaugosIvykiai
{
    /**
     * @var integer
     *
     * @ORM\Column(name="MENU_ID", type="integer", nullable=true)
     */
    private $menuId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="TEXT_DATA", type="text", nullable=true)
     */
    private $textData;

    /**
     * @var string
     *
     * @ORM\Column(name="CLASS", type="string", length=100, nullable=true)
     */
    private $class = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ICO", type="string", length=100, nullable=true)
     */
    private $ico = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IVYKIAI_DESC", type="text", nullable=true)
     */
    private $ivykiaiDesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=20, nullable=false)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=20, nullable=false)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=false)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=false)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=false)
     */
    private $modifyIp = '';


}

