<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsOfficecase
 *
 * @ORM\Table(name="IDM_IDM_DHS_OFFICECASE", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmDhsOfficecase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PREFIX", type="string", length=100, nullable=false)
     */
    private $prefix = '';


}

