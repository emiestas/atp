<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosFolderFiles
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_FOLDER_FILES", uniqueConstraints={@ORM\UniqueConstraint(name="_FILE_ID", columns={"FILE_ID"})}, indexes={@ORM\Index(name="_FOLDER__FILE", columns={"FOLDER_ID", "FILE_ID"}), @ORM\Index(name="_FOLDER_ID", columns={"FOLDER_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosFolderFiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \IdmPaslaugosFolder
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosFolder")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FOLDER_ID", referencedColumnName="ID")
     * })
     */
    private $folder;

    /**
     * @var \IdmPaslaugosFiles
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosFiles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FILE_ID", referencedColumnName="ID")
     * })
     */
    private $file;


}

