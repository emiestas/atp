<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpMessage
 *
 * @ORM\Table(name="IDM_IDM_ATP_MESSAGE", indexes={@ORM\Index(name="IDX_CAF872AD31722D03", columns={"RECORD_ID"})})
 * @ORM\Entity
 */
class IdmAtpMessage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SUBJECT", type="string", length=255, nullable=false)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="BODY", type="text", nullable=true)
     */
    private $body;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;

    /**
     * @var \IdmAtpRecord
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpRecord")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="RECORD_ID", referencedColumnName="ID")
     * })
     */
    private $record;


}

