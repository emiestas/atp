<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosKlasifikatoriaiMainStructure
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_KLASIFIKATORIAI_MAIN_STRUCTURE")
 * @ORM\Entity
 */
class IdmPaslaugosKlasifikatoriaiMainStructure
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=true)
     */
    private $parentId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=255, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="AVILIO_ID", type="string", length=64, nullable=false)
     */
    private $avilioId;

    /**
     * @var string
     *
     * @ORM\Column(name="ADDRESS", type="string", length=255, nullable=true)
     */
    private $address = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PHONE", type="string", length=255, nullable=true)
     */
    private $phone = '';

    /**
     * @var string
     *
     * @ORM\Column(name="EMAIL", type="string", length=255, nullable=true)
     */
    private $email = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE_ID", type="integer", nullable=true)
     */
    private $typeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPTION_EN", type="text", length=16777215, nullable=false)
     */
    private $descriptionEn;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPTION", type="text", length=16777215, nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="RANK", type="integer", nullable=true)
     */
    private $rank = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="FAX", type="string", length=255, nullable=true)
     */
    private $fax = '';

    /**
     * @var string
     *
     * @ORM\Column(name="BANK", type="string", length=255, nullable=true)
     */
    private $bank = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ACCOUNT", type="string", length=255, nullable=true)
     */
    private $account = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CODE", type="string", length=255, nullable=true)
     */
    private $code = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PVMCODE", type="string", length=255, nullable=true)
     */
    private $pvmcode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CITY", type="string", length=255, nullable=true)
     */
    private $city = '';

    /**
     * @var string
     *
     * @ORM\Column(name="COUNTRY", type="string", length=255, nullable=true)
     */
    private $country = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MOBILE", type="string", length=255, nullable=true)
     */
    private $mobile = '';

    /**
     * @var string
     *
     * @ORM\Column(name="FOREIGN_ID", type="string", length=255, nullable=false)
     */
    private $foreignId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CHECK_DATE", type="datetime", nullable=false)
     */
    private $checkDate;

    /**
     * @var string
     *
     * @ORM\Column(name="URL", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="WORKDAYS", type="string", length=255, nullable=false)
     */
    private $workdays;

    /**
     * @var integer
     *
     * @ORM\Column(name="DISABLED", type="integer", nullable=false)
     */
    private $disabled = '0';


}

