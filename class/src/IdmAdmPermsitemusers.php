<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAdmPermsitemusers
 *
 * @ORM\Table(name="IDM_IDM_ADM_PERMSITEMUSERS", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmAdmPermsitemusers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=false)
     */
    private $shows;

    /**
     * @var integer
     *
     * @ORM\Column(name="ITEM_ID", type="integer", nullable=false)
     */
    private $itemId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="OBJECT_ID", type="integer", nullable=false)
     */
    private $objectId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=false)
     */
    private $userId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PERMISSION_ID", type="integer", nullable=false)
     */
    private $permissionId = '0';


}

