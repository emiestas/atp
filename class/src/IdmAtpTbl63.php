<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTbl63
 *
 * @ORM\Table(name="IDM_IDM_ATP_TBL_63", indexes={@ORM\Index(name="RECORD_LAST", columns={"RECORD_LAST"}), @ORM\Index(name="RECORD_MAIN_ID", columns={"RECORD_MAIN_ID"})})
 * @ORM\Entity
 */
class IdmAtpTbl63
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_MAIN_ID", type="string", length=25, nullable=false)
     */
    private $recordMainId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_RELATION_ID", type="integer", nullable=false)
     */
    private $recordRelationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_VERSION", type="integer", nullable=false)
     */
    private $recordVersion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RECORD_LAST", type="boolean", nullable=false)
     */
    private $recordLast;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_1", type="date", nullable=false)
     */
    private $col1;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_2", type="text", length=255, nullable=false)
     */
    private $col2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_3", type="date", nullable=false)
     */
    private $col3;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_4", type="text", length=255, nullable=false)
     */
    private $col4;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_5", type="text", length=255, nullable=false)
     */
    private $col5;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_6", type="date", nullable=false)
     */
    private $col6;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_7", type="date", nullable=false)
     */
    private $col7;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_11", type="text", length=255, nullable=false)
     */
    private $col11;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_12", type="text", length=65535, nullable=false)
     */
    private $col12;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_13", type="text", length=65535, nullable=false)
     */
    private $col13;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_14", type="text", length=65535, nullable=false)
     */
    private $col14;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_15", type="text", length=65535, nullable=false)
     */
    private $col15;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_16", type="text", length=65535, nullable=false)
     */
    private $col16;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_17", type="text", length=255, nullable=false)
     */
    private $col17;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;


}

