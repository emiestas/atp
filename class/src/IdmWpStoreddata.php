<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWpStoreddata
 *
 * @ORM\Table(name="IDM_IDM_WP_STOREDDATA", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmWpStoreddata
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="STOREDDATA", type="text", nullable=false)
     */
    private $storeddata;

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=false)
     */
    private $userId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SOFT_ID", type="integer", nullable=false)
     */
    private $softId = '0';


}

