<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosNeighborhood
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_NEIGHBORHOOD", indexes={@ORM\Index(name="_CODE", columns={"CODE"})})
 * @ORM\Entity
 */
class IdmPaslaugosNeighborhood
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="CODE", type="string", length=256, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=255, nullable=false)
     */
    private $label;


}

