<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsDocumentationPlan
 *
 * @ORM\Table(name="IDM_IDM_DHS_DOCUMENTATION_PLAN")
 * @ORM\Entity
 */
class IdmDhsDocumentationPlan
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=5000, nullable=false)
     */
    private $label;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FROM", type="datetime", nullable=false)
     */
    private $from;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TILL", type="datetime", nullable=false)
     */
    private $till;

    /**
     * @var integer
     *
     * @ORM\Column(name="ADMIN_ORG_ID", type="integer", nullable=false)
     */
    private $adminOrgId;


}

