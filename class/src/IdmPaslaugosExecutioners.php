<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosExecutioners
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_EXECUTIONERS", indexes={@ORM\Index(name="_SERVICE_ID", columns={"SERVICE_ID"}), @ORM\Index(name="_PERSON_ID", columns={"PERSON_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosExecutioners
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \IdmPaslaugos
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SERVICE_ID", referencedColumnName="ID")
     * })
     */
    private $service;

    /**
     * @var \IdmMainPeople
     *
     * @ORM\ManyToOne(targetEntity="IdmMainPeople")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PERSON_ID", referencedColumnName="ID")
     * })
     */
    private $person;


}

