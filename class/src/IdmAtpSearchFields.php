<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpSearchFields
 *
 * @ORM\Table(name="IDM_IDM_ATP_SEARCH_FIELDS")
 * @ORM\Entity
 */
class IdmAtpSearchFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORD", type="integer", nullable=false)
     */
    private $ord;

    /**
     * @var boolean
     *
     * @ORM\Column(name="VALID", type="boolean", nullable=false)
     */
    private $valid = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="UPDATE_DATE", type="datetime", nullable=false)
     */
    private $updateDate;


}

