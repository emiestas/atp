<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMetParticipants
 *
 * @ORM\Table(name="IDM_IDM_MET_PARTICIPANTS", indexes={@ORM\Index(name="MEETING_ID", columns={"MEETING_ID"})})
 * @ORM\Entity
 */
class IdmMetParticipants
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="MEETING_ID", type="integer", nullable=false)
     */
    private $meetingId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PARTICIPANT_ID", type="integer", nullable=false)
     */
    private $participantId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PARTICIPANT_INFO", type="text", nullable=true)
     */
    private $participantInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="PARTICIPANT_SHORT", type="text", nullable=true)
     */
    private $participantShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARTICIPANT_TYPE", type="integer", nullable=false)
     */
    private $participantType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PARTICIPANT_ORGNODE_ID", type="integer", nullable=false)
     */
    private $participantOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="STATE", type="string", length=15, nullable=true)
     */
    private $state = '';


}

