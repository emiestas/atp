<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmRemTasks
 *
 * @ORM\Table(name="IDM_IDM_REM_TASKS")
 * @ORM\Entity
 */
class IdmRemTasks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="REMIND", type="string", length=255, nullable=false)
     */
    private $remind = '';

    /**
     * @var string
     *
     * @ORM\Column(name="REMIND_DESC", type="text", length=16777215, nullable=false)
     */
    private $remindDesc;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2008-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="REMIND_DATE", type="datetime", nullable=false)
     */
    private $remindDate = '2008-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXPIRE_DATE", type="datetime", nullable=false)
     */
    private $expireDate = '2008-01-01 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="FOLDER_ID", type="integer", nullable=false)
     */
    private $folderId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE_ID", type="integer", nullable=false)
     */
    private $typeId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ITEM_ID", type="integer", nullable=false)
     */
    private $itemId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="REMIND_VALUE", type="integer", nullable=false)
     */
    private $remindValue = '0';


}

