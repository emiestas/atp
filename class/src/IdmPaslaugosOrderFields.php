<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderFields
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_FIELDS", indexes={@ORM\Index(name="ORDER_ID", columns={"ORDER_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORDER_ID", type="integer", nullable=false)
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(name="FIELD_ID", type="integer", nullable=false)
     */
    private $fieldId;

    /**
     * @var string
     *
     * @ORM\Column(name="VALUE", type="text", length=65535, nullable=false)
     */
    private $value;


}

