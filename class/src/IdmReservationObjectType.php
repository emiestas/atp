<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmReservationObjectType
 *
 * @ORM\Table(name="IDM_IDM_RESERVATION_OBJECT_TYPE")
 * @ORM\Entity
 */
class IdmReservationObjectType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ENABLED", type="boolean", nullable=true)
     */
    private $enabled = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="PUBLIC", type="boolean", nullable=true)
     */
    private $public = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SPECIAL_ATTRIBUTES", type="text", length=65535, nullable=true)
     */
    private $specialAttributes;

    /**
     * @var string
     *
     * @ORM\Column(name="INFO", type="text", length=65535, nullable=true)
     */
    private $info;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DELETED", type="datetime", nullable=true)
     */
    private $deleted;


}

