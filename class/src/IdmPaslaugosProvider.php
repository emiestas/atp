<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosProvider
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_PROVIDER", indexes={@ORM\Index(name="_CODE", columns={"CODE"})})
 * @ORM\Entity
 */
class IdmPaslaugosProvider
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="CODE", type="integer", nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=256, nullable=true)
     */
    private $label;


}

