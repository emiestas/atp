<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsProjectrisk
 *
 * @ORM\Table(name="IDM_IDM_PHS_PROJECTRISK", indexes={@ORM\Index(name="PROJECT_ID", columns={"PROJECT_ID"}), @ORM\Index(name="RISK_ID", columns={"RISK_ID"})})
 * @ORM\Entity
 */
class IdmPhsProjectrisk
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RISK_NAME", type="string", length=255, nullable=false)
     */
    private $riskName;

    /**
     * @var integer
     *
     * @ORM\Column(name="PROJECT_ID", type="integer", nullable=false)
     */
    private $projectId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RISK_ID", type="integer", nullable=false)
     */
    private $riskId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="RISK_VALUE", type="string", length=255, nullable=false)
     */
    private $riskValue;


}

