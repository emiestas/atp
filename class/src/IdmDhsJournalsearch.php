<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsJournalsearch
 *
 * @ORM\Table(name="IDM_IDM_DHS_JOURNALSEARCH", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsJournalsearch
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SEARCH_ID", type="integer", nullable=false)
     */
    private $searchId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SEARCH_TYPE", type="integer", nullable=false)
     */
    private $searchType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCH_SHORT", type="text", nullable=false)
     */
    private $searchShort;

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCH_INFO", type="text", nullable=false)
     */
    private $searchInfo;


}

