<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosInterimStatus
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_INTERIM_STATUS", uniqueConstraints={@ORM\UniqueConstraint(name="_SERVICE__STATUS", columns={"SERVICE_ID", "STATUS_ID"})}, indexes={@ORM\Index(name="_SERVICE_ID", columns={"SERVICE_ID"}), @ORM\Index(name="_STATUS_ID", columns={"STATUS_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosInterimStatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \IdmPaslaugos
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SERVICE_ID", referencedColumnName="ID")
     * })
     */
    private $service;

    /**
     * @var \IdmPaslaugosStatusai
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosStatusai")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="STATUS_ID", referencedColumnName="ID")
     * })
     */
    private $status;


}

