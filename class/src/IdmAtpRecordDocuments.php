<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpRecordDocuments
 *
 * @ORM\Table(name="IDM_IDM_ATP_RECORD_DOCUMENTS", uniqueConstraints={@ORM\UniqueConstraint(name="FILE_ID", columns={"FILE_ID"})}, indexes={@ORM\Index(name="RECORD_ID", columns={"RECORD_ID"}), @ORM\Index(name="TEMPLATE_ID", columns={"TEMPLATE_ID"})})
 * @ORM\Entity
 */
class IdmAtpRecordDocuments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="FILE_ID", type="integer", nullable=false)
     */
    private $fileId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_ID", type="integer", nullable=false)
     */
    private $recordId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TEMPLATE_ID", type="integer", nullable=false)
     */
    private $templateId;


}

