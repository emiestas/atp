<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmSchHolidays
 *
 * @ORM\Table(name="IDM_IDM_SCH_HOLIDAYS", uniqueConstraints={@ORM\UniqueConstraint(name="DATE", columns={"DATE", "USER_ID"})})
 * @ORM\Entity
 */
class IdmSchHolidays
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE", type="datetime", nullable=false)
     */
    private $date;


}

