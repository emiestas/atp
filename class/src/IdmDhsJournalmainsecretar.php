<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsJournalmainsecretar
 *
 * @ORM\Table(name="IDM_IDM_DHS_JOURNALMAINSECRETAR", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsJournalmainsecretar
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="MAINSECRETAR_ID", type="integer", nullable=false)
     */
    private $mainsecretarId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="MAINSECRETAR_TYPE", type="integer", nullable=false)
     */
    private $mainsecretarType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="MAINSECRETAR_SHORT", type="text", nullable=false)
     */
    private $mainsecretarShort;

    /**
     * @var string
     *
     * @ORM\Column(name="MAINSECRETAR_INFO", type="text", nullable=false)
     */
    private $mainsecretarInfo;


}

