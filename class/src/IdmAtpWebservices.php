<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpWebservices
 *
 * @ORM\Table(name="IDM_IDM_ATP_WEBSERVICES", uniqueConstraints={@ORM\UniqueConstraint(name="WS_ID", columns={"WS_NAME", "FIELD_ID", "WS_FIELD_ID", "PARENT_FIELD_ID"})})
 * @ORM\Entity
 */
class IdmAtpWebservices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="WS_NAME", type="string", length=25, nullable=false)
     */
    private $wsName;

    /**
     * @var integer
     *
     * @ORM\Column(name="FIELD_ID", type="integer", nullable=false)
     */
    private $fieldId;

    /**
     * @var integer
     *
     * @ORM\Column(name="WS_FIELD_ID", type="integer", nullable=false)
     */
    private $wsFieldId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_FIELD_ID", type="integer", nullable=false)
     */
    private $parentFieldId;


}

