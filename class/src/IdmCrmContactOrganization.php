<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmCrmContactOrganization
 *
 * @ORM\Table(name="IDM_IDM_CRM_CONTACT_ORGANIZATION", indexes={@ORM\Index(name="NAME", columns={"NAME"}), @ORM\Index(name="REGISTRATION_NUMBER", columns={"REGISTRATION_NUMBER"})})
 * @ORM\Entity
 */
class IdmCrmContactOrganization
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="REGISTRATION_NUMBER", type="bigint", nullable=false)
     */
    private $registrationNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="Address", type="string", length=255, nullable=false)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="PHONE", type="string", length=30, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="EMAIL", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="INFO", type="text", length=16777215, nullable=true)
     */
    private $info;


}

