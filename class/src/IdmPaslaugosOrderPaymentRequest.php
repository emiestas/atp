<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderPaymentRequest
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_PAYMENT_REQUEST", indexes={@ORM\Index(name="_ORDER_ID", columns={"ORDER_ID"}), @ORM\Index(name="_IS_ACTIVE", columns={"IS_ACTIVE"}), @ORM\Index(name="_IS_DONE", columns={"IS_DONE"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderPaymentRequest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="PAYMENT_SUM", type="decimal", precision=14, scale=2, nullable=false)
     */
    private $paymentSum;

    /**
     * @var string
     *
     * @ORM\Column(name="PAYMENT_CURRENCY", type="string", length=3, nullable=false)
     */
    private $paymentCurrency;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DUE_DATE", type="datetime", nullable=false)
     */
    private $dueDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="REQUEST_DATE", type="datetime", nullable=true)
     */
    private $requestDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RESPONSE_DATE", type="datetime", nullable=true)
     */
    private $responseDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_ACTIVE", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_CANCELED", type="boolean", nullable=false)
     */
    private $isCanceled;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_DONE", type="boolean", nullable=false)
     */
    private $isDone;

    /**
     * @var string
     *
     * @ORM\Column(name="CANCEL_REASON", type="text", length=16777215, nullable=false)
     */
    private $cancelReason;

    /**
     * @var \IdmPaslaugosUzsakymai
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosUzsakymai")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ORDER_ID", referencedColumnName="ID")
     * })
     */
    private $order;


}

