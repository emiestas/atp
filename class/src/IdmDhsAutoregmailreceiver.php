<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsAutoregmailreceiver
 *
 * @ORM\Table(name="IDM_IDM_DHS_AUTOREGMAILRECEIVER", indexes={@ORM\Index(name="MAIL_ID", columns={"MAIL_ID"})})
 * @ORM\Entity
 */
class IdmDhsAutoregmailreceiver
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="MAIL_ID", type="integer", nullable=false)
     */
    private $mailId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ID", type="integer", nullable=false)
     */
    private $receiverId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_TYPE", type="integer", nullable=false)
     */
    private $receiverType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", nullable=false)
     */
    private $receiverShort;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", nullable=false)
     */
    private $receiverInfo;


}

