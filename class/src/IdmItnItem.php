<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmItnItem
 *
 * @ORM\Table(name="IDM_IDM_ITN_ITEM")
 * @ORM\Entity
 */
class IdmItnItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=255, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ITEMTEXT", type="text", length=16777215, nullable=false)
     */
    private $itemtext;

    /**
     * @var string
     *
     * @ORM\Column(name="CATNAMES", type="text", length=16777215, nullable=false)
     */
    private $catnames;

    /**
     * @var string
     *
     * @ORM\Column(name="CATVALUES", type="text", length=16777215, nullable=false)
     */
    private $catvalues;

    /**
     * @var string
     *
     * @ORM\Column(name="CLASSTYPE", type="string", length=50, nullable=false)
     */
    private $classtype = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="VERSION", type="integer", nullable=false)
     */
    private $version = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="SITE_URL", type="text", length=16777215, nullable=false)
     */
    private $siteUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="REAL_URL", type="text", length=16777215, nullable=false)
     */
    private $realUrl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ITEM_DATE", type="datetime", nullable=true)
     */
    private $itemDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="TILL_DATE", type="datetime", nullable=true)
     */
    private $tillDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="POSITION", type="integer", nullable=false)
     */
    private $position = '0';


}

