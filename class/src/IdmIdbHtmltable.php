<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmIdbHtmltable
 *
 * @ORM\Table(name="IDM_IDM_IDB_HTMLTABLE", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmIdbHtmltable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=100, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="URI", type="text", nullable=false)
     */
    private $uri;

    /**
     * @var string
     *
     * @ORM\Column(name="START_TAG", type="text", nullable=false)
     */
    private $startTag;

    /**
     * @var string
     *
     * @ORM\Column(name="END_TAG", type="text", nullable=false)
     */
    private $endTag;

    /**
     * @var string
     *
     * @ORM\Column(name="JUNK_DATA", type="string", length=255, nullable=false)
     */
    private $junkData = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="PART_NR", type="integer", nullable=false)
     */
    private $partNr = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="HTML_TABLE", type="string", length=255, nullable=false)
     */
    private $htmlTable = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LAST_UPDATE", type="datetime", nullable=false)
     */
    private $lastUpdate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="ENCODING", type="string", length=50, nullable=false)
     */
    private $encoding = '';


}

