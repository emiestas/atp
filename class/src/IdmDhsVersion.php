<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsVersion
 *
 * @ORM\Table(name="IDM_IDM_DHS_VERSION", indexes={@ORM\Index(name="DOC_ID", columns={"DOC_ID"})})
 * @ORM\Entity
 */
class IdmDhsVersion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="VERSION", type="string", length=100, nullable=false)
     */
    private $version = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     */
    private $docId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="VERSION_DATA", type="text", nullable=true)
     */
    private $versionData;


}

