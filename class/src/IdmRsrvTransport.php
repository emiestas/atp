<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmRsrvTransport
 *
 * @ORM\Table(name="IDM_IDM_RSRV_TRANSPORT")
 * @ORM\Entity
 */
class IdmRsrvTransport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="AUTO_NAME", type="string", length=255, nullable=false)
     */
    private $autoName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="AUTO_DESC", type="text", length=16777215, nullable=true)
     */
    private $autoDesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_COUNT", type="integer", nullable=false)
     */
    private $userCount = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ADMIN_ID", type="integer", nullable=false)
     */
    private $adminId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ADMIN2_ID", type="integer", nullable=false)
     */
    private $admin2Id = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="DRIVER_ID", type="integer", nullable=false)
     */
    private $driverId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="TRIP_DEST", type="text", length=16777215, nullable=true)
     */
    private $tripDest;

    /**
     * @var integer
     *
     * @ORM\Column(name="STRUCTURE_ID", type="integer", nullable=false)
     */
    private $structureId;

    /**
     * @var string
     *
     * @ORM\Column(name="IMG_URL1", type="string", length=255, nullable=false)
     */
    private $imgUrl1 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IMG_URL2", type="string", length=255, nullable=false)
     */
    private $imgUrl2 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IMG_URL3", type="string", length=255, nullable=false)
     */
    private $imgUrl3 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="USEABLE", type="string", length=10, nullable=false)
     */
    private $useable = '';


}

