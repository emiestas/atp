<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderResult
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_RESULT", uniqueConstraints={@ORM\UniqueConstraint(name="_ORDER_ID", columns={"ORDER_ID"})}, indexes={@ORM\Index(name="_ORDER__TYPE", columns={"ORDER_ID", "TYPE"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderResult
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCUMENT_ID", type="integer", nullable=false)
     */
    private $documentId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="TYPE", type="boolean", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="NOTE", type="text", length=65535, nullable=true)
     */
    private $note;

    /**
     * @var \IdmPaslaugosUzsakymai
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosUzsakymai")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ORDER_ID", referencedColumnName="ID")
     * })
     */
    private $order;


}

