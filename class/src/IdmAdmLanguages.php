<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAdmLanguages
 *
 * @ORM\Table(name="IDM_IDM_ADM_LANGUAGES")
 * @ORM\Entity
 */
class IdmAdmLanguages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="LANGUAGE", type="string", length=2, nullable=false)
     */
    private $language = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';


}

