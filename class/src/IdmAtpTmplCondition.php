<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTmplCondition
 *
 * @ORM\Table(name="IDM_IDM_ATP_TMPL_CONDITION")
 * @ORM\Entity
 */
class IdmAtpTmplCondition
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ACTION_ID", type="integer", nullable=false)
     */
    private $actionId;


}

