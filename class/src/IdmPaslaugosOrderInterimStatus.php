<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderInterimStatus
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_INTERIM_STATUS", indexes={@ORM\Index(name="_ORDER_ID", columns={"ORDER_ID"}), @ORM\Index(name="_STATUS_ID", columns={"STATUS_ID"}), @ORM\Index(name="_IS_ACTIVE", columns={"IS_ACTIVE"}), @ORM\Index(name="_IS_DONE", columns={"IS_DONE"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderInterimStatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DUE_DATE", type="datetime", nullable=false)
     */
    private $dueDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="REQUEST_DATE", type="datetime", nullable=true)
     */
    private $requestDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RESPONSE_DATE", type="datetime", nullable=true)
     */
    private $responseDate;

    /**
     * @var string
     *
     * @ORM\Column(name="RESONSE_COMMENT", type="text", length=16777215, nullable=true)
     */
    private $resonseComment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_ACTIVE", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_CANCELED", type="boolean", nullable=false)
     */
    private $isCanceled;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_DONE", type="boolean", nullable=false)
     */
    private $isDone;

    /**
     * @var \IdmPaslaugosUzsakymai
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosUzsakymai")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ORDER_ID", referencedColumnName="ID")
     * })
     */
    private $order;

    /**
     * @var \IdmPaslaugosStatusai
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosStatusai")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="STATUS_ID", referencedColumnName="ID")
     * })
     */
    private $status;


}

