<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsDocumentationPlanItems
 *
 * @ORM\Table(name="IDM_IDM_DHS_DOCUMENTATION_PLAN_ITEMS")
 * @ORM\Entity
 */
class IdmDhsDocumentationPlanItems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PLAN_ID", type="integer", nullable=false)
     */
    private $planId;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=7, nullable=false)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="ITEM", type="integer", nullable=false)
     */
    private $item;

    /**
     * @var string
     *
     * @ORM\Column(name="LABEL", type="string", length=1000, nullable=false)
     */
    private $label;


}

