<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmCrmFile
 *
 * @ORM\Table(name="IDM_IDM_CRM_FILE", indexes={@ORM\Index(name="NAME", columns={"NAME"})})
 * @ORM\Entity
 */
class IdmCrmFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=128, nullable=false)
     */
    private $name;


}

