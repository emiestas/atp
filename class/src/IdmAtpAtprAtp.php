<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpAtprAtp
 *
 * @ORM\Table(name="IDM_IDM_ATP_ATPR_ATP", uniqueConstraints={@ORM\UniqueConstraint(name="_ROIK", columns={"ROIK"})})
 * @ORM\Entity
 */
class IdmAtpAtprAtp
{
    /**
     * @var string
     *
     * @ORM\Column(name="ROIK", type="string", length=15, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $roik;

    /**
     * @var string
     *
     * @ORM\Column(name="PILNA_NUORODA_I_ATPR", type="string", length=500, nullable=true)
     */
    private $pilnaNuorodaIAtpr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = 'CURRENT_TIMESTAMP';

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="IdmAtpRecord", mappedBy="roik")
     */
    private $record;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->record = new \Doctrine\Common\Collections\ArrayCollection();
    }

}

