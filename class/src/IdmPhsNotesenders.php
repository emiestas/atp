<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsNotesenders
 *
 * @ORM\Table(name="IDM_IDM_PHS_NOTESENDERS", indexes={@ORM\Index(name="NOTE_ID", columns={"NOTE_ID"}), @ORM\Index(name="SENDER_ID", columns={"SENDER_ID"})})
 * @ORM\Entity
 */
class IdmPhsNotesenders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="PROJECT_ID", type="integer", nullable=false)
     */
    private $projectId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="NOTE_ID", type="integer", nullable=false)
     */
    private $noteId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SENDER_ID", type="integer", nullable=false)
     */
    private $senderId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_INFO", type="text", length=65535, nullable=true)
     */
    private $senderInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="SENDER_SHORT", type="text", length=65535, nullable=true)
     */
    private $senderShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="SENDER_TYPE", type="integer", nullable=false)
     */
    private $senderType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="SENDER_ORGNODE_ID", type="integer", nullable=false)
     */
    private $senderOrgnodeId = '0';


}

