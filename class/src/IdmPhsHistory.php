<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsHistory
 *
 * @ORM\Table(name="IDM_IDM_PHS_HISTORY", indexes={@ORM\Index(name="PROJECT_ID", columns={"PROJECT_ID"}), @ORM\Index(name="PROCESS_ID", columns={"PROCESS_ID"}), @ORM\Index(name="EMPLOYER_ID", columns={"EMPLOYER_ID"}), @ORM\Index(name="STAGE_ID", columns={"STAGE_ID"})})
 * @ORM\Entity
 */
class IdmPhsHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="PROJECT_ID", type="integer", nullable=false)
     */
    private $projectId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="STAGE_ID", type="integer", nullable=false)
     */
    private $stageId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PROCESS_ID", type="integer", nullable=false)
     */
    private $processId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="FILE_ID", type="integer", nullable=false)
     */
    private $fileId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TASK_ID", type="integer", nullable=false)
     */
    private $taskId;

    /**
     * @var integer
     *
     * @ORM\Column(name="NOTE_ID", type="integer", nullable=false)
     */
    private $noteId;

    /**
     * @var integer
     *
     * @ORM\Column(name="EMPLOYER_ID", type="integer", nullable=false)
     */
    private $employerId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="EMPLOYER_INFO", type="text", length=65535, nullable=true)
     */
    private $employerInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="EMPLOYER_SHORT", type="text", length=65535, nullable=true)
     */
    private $employerShort;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=50, nullable=false)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=50, nullable=false)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=false)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=false)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=false)
     */
    private $modifyIp = '';


}

