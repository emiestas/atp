<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTableRelations
 *
 * @ORM\Table(name="IDM_IDM_ATP_TABLE_RELATIONS")
 * @ORM\Entity
 */
class IdmAtpTableRelations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="TABLE_ID", type="integer", nullable=false)
     */
    private $tableId;

    /**
     * @var integer
     *
     * @ORM\Column(name="COLUMN_ID", type="integer", nullable=false)
     */
    private $columnId;

    /**
     * @var string
     *
     * @ORM\Column(name="COLUMN_NAME", type="string", length=100, nullable=false)
     */
    private $columnName;

    /**
     * @var integer
     *
     * @ORM\Column(name="TABLE_PARENT_ID", type="integer", nullable=false)
     */
    private $tableParentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="COLUMN_PARENT_ID", type="integer", nullable=true)
     */
    private $columnParentId;

    /**
     * @var string
     *
     * @ORM\Column(name="COLUMN_PARENT_NAME", type="string", length=100, nullable=true)
     */
    private $columnParentName;

    /**
     * @var string
     *
     * @ORM\Column(name="DEFAULT", type="text", length=65535, nullable=true)
     */
    private $default;


}

