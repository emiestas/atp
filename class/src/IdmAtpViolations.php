<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpViolations
 *
 * @ORM\Table(name="IDM_IDM_ATP_VIOLATIONS")
 * @ORM\Entity
 */
class IdmAtpViolations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_ID", type="integer", nullable=false)
     */
    private $recordId;

    /**
     * @var integer
     *
     * @ORM\Column(name="VIOLATION_RECORD_ID", type="integer", nullable=false)
     */
    private $violationRecordId;

    /**
     * @var string
     *
     * @ORM\Column(name="VIOLATION_TYPE", type="string", length=50, nullable=false)
     */
    private $violationType;


}

