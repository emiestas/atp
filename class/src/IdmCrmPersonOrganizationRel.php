<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmCrmPersonOrganizationRel
 *
 * @ORM\Table(name="IDM_IDM_CRM_PERSON_ORGANIZATION_REL", indexes={@ORM\Index(name="ORGANIZATION_ID", columns={"ORGANIZATION_ID"}), @ORM\Index(name="PERSON_ID", columns={"PERSON_ID"})})
 * @ORM\Entity
 */
class IdmCrmPersonOrganizationRel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORGANIZATION_ID", type="integer", nullable=false)
     */
    private $organizationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PERSON_ID", type="integer", nullable=true)
     */
    private $personId;


}

