<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosResult
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_RESULT", indexes={@ORM\Index(name="_SERVICE_ID", columns={"SERVICE_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosResult
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="SERVICE_ID", type="integer", nullable=false)
     */
    private $serviceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CASE_ID", type="integer", nullable=false)
     */
    private $caseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="TEMPLATE_ID", type="integer", nullable=false)
     */
    private $templateId;


}

