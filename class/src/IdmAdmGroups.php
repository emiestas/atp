<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAdmGroups
 *
 * @ORM\Table(name="IDM_IDM_ADM_GROUPS", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmAdmGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=true)
     */
    private $parentId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=true)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="GROUP_NAME", type="string", length=100, nullable=false)
     */
    private $groupName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="GROUP_DESC", type="text", nullable=true)
     */
    private $groupDesc;


}

