<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosViispActions
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_VIISP_ACTIONS", indexes={@ORM\Index(name="_GROUP", columns={"ORDER_ID", "ACTION_ID", "STATUS"}), @ORM\Index(name="ORDER_ID", columns={"ORDER_ID"}), @ORM\Index(name="ACTION", columns={"ACTION_ID"}), @ORM\Index(name="STATUS", columns={"STATUS"})})
 * @ORM\Entity
 */
class IdmPaslaugosViispActions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORDER_ID", type="integer", nullable=false)
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ACTION_ID", type="integer", nullable=false)
     */
    private $actionId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="STATUS", type="boolean", nullable=false)
     */
    private $status = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="REQUEST", type="text", length=16777215, nullable=true)
     */
    private $request;

    /**
     * @var string
     *
     * @ORM\Column(name="RESPONSE", type="text", length=16777215, nullable=true)
     */
    private $response;

    /**
     * @var integer
     *
     * @ORM\Column(name="ERROR_CODE", type="integer", nullable=true)
     */
    private $errorCode;

    /**
     * @var string
     *
     * @ORM\Column(name="ERROR_MESSAGE", type="text", length=65535, nullable=false)
     */
    private $errorMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="ERROR", type="text", length=65535, nullable=false)
     */
    private $error;


}

