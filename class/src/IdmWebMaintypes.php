<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWebMaintypes
 *
 * @ORM\Table(name="IDM_IDM_WEB_MAINTYPES", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmWebMaintypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=20, nullable=false)
     */
    private $type = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=true)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE_DESC", type="string", length=255, nullable=true)
     */
    private $typeDesc = '';


}

