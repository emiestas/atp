<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpPostRnRegistry
 *
 * @ORM\Table(name="IDM_IDM_ATP_POST_RN_REGISTRY", uniqueConstraints={@ORM\UniqueConstraint(name="RN", columns={"RN"})}, indexes={@ORM\Index(name="DOC_ID", columns={"DOC_ID"})})
 * @ORM\Entity
 */
class IdmAtpPostRnRegistry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RN", type="string", length=25, nullable=false)
     */
    private $rn;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="SURNAME", type="string", length=255, nullable=true)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="COMPANY", type="text", length=65535, nullable=true)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="MUNICIPALITY", type="string", length=255, nullable=true)
     */
    private $municipality;

    /**
     * @var string
     *
     * @ORM\Column(name="CITY", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="STREET", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="HOUSE_NO", type="string", length=25, nullable=true)
     */
    private $houseNo;

    /**
     * @var string
     *
     * @ORM\Column(name="FLAT_NO", type="string", length=25, nullable=true)
     */
    private $flatNo;

    /**
     * @var string
     *
     * @ORM\Column(name="POST_CODE", type="string", length=25, nullable=true)
     */
    private $postCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     */
    private $docId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ACQ_CONFIRMATION", type="integer", nullable=true)
     */
    private $acqConfirmation;

    /**
     * @var integer
     *
     * @ORM\Column(name="RESERVED", type="integer", nullable=false)
     */
    private $reserved = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RESERVATION_DATE", type="date", nullable=true)
     */
    private $reservationDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="RESERVED_FOR_STRUCTURE", type="integer", nullable=true)
     */
    private $reservedForStructure;


}

