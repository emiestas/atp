<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderFormFields
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_FORM_FIELDS", indexes={@ORM\Index(name="fk_IDM_PASLAUGOS_ORDER_FORM_FIELDS_IDM_PASLAUGOS1_idx", columns={"PASLAUGOS_ID"}), @ORM\Index(name="_REVISABLE", columns={"REVISABLE"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderFormFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PASLAUGOS_ID", type="integer", nullable=false)
     */
    private $paslaugosId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ORDER", type="integer", nullable=false)
     */
    private $order;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=65535, nullable=true)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWTYPE", type="text", length=65535, nullable=true)
     */
    private $showtype;

    /**
     * @var string
     *
     * @ORM\Column(name="FIELDTYPE", type="text", length=65535, nullable=true)
     */
    private $fieldtype;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS_TIP", type="text", length=65535, nullable=true)
     */
    private $showsTip;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS_UP", type="text", length=65535, nullable=true)
     */
    private $showsUp;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS_DEFAULT", type="text", length=65535, nullable=true)
     */
    private $showsDefault;

    /**
     * @var string
     *
     * @ORM\Column(name="MANDATORY", type="text", length=65535, nullable=true)
     */
    private $mandatory;

    /**
     * @var string
     *
     * @ORM\Column(name="PUBLIC", type="text", length=65535, nullable=true)
     */
    private $public;

    /**
     * @var string
     *
     * @ORM\Column(name="AVILYS_FIELD_NAME", type="text", length=65535, nullable=false)
     */
    private $avilysFieldName;

    /**
     * @var string
     *
     * @ORM\Column(name="HTMLSIZE", type="text", length=65535, nullable=true)
     */
    private $htmlsize;

    /**
     * @var string
     *
     * @ORM\Column(name="HTMLROWS", type="text", length=65535, nullable=true)
     */
    private $htmlrows;

    /**
     * @var string
     *
     * @ORM\Column(name="MAXLENGTH", type="text", length=65535, nullable=true)
     */
    private $maxlength;

    /**
     * @var string
     *
     * @ORM\Column(name="FILESIZE", type="string", length=50, nullable=false)
     */
    private $filesize = '';

    /**
     * @var string
     *
     * @ORM\Column(name="HTMLVALUE", type="text", length=65535, nullable=true)
     */
    private $htmlvalue;

    /**
     * @var string
     *
     * @ORM\Column(name="HTMLTYPE", type="text", length=65535, nullable=true)
     */
    private $htmltype;

    /**
     * @var string
     *
     * @ORM\Column(name="VNAME", type="text", length=65535, nullable=true)
     */
    private $vname;

    /**
     * @var string
     *
     * @ORM\Column(name="FTYPE", type="text", length=65535, nullable=true)
     */
    private $ftype;

    /**
     * @var string
     *
     * @ORM\Column(name="PRESENTATION", type="text", length=65535, nullable=true)
     */
    private $presentation;

    /**
     * @var string
     *
     * @ORM\Column(name="VISIBLE", type="text", length=65535, nullable=true)
     */
    private $visible;

    /**
     * @var string
     *
     * @ORM\Column(name="EDITABLE", type="text", length=65535, nullable=true)
     */
    private $editable;

    /**
     * @var boolean
     *
     * @ORM\Column(name="REVISABLE", type="boolean", nullable=false)
     */
    private $revisable;

    /**
     * @var string
     *
     * @ORM\Column(name="OUTPUT", type="text", length=65535, nullable=true)
     */
    private $output;

    /**
     * @var string
     *
     * @ORM\Column(name="REG", type="text", length=65535, nullable=true)
     */
    private $reg;

    /**
     * @var string
     *
     * @ORM\Column(name="REG_SHOWS", type="text", length=65535, nullable=true)
     */
    private $regShows;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS_DOWN", type="text", length=65535, nullable=true)
     */
    private $showsDown;

    /**
     * @var string
     *
     * @ORM\Column(name="REL_DESC", type="text", length=65535, nullable=true)
     */
    private $relDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="VISIBILITY", type="text", length=65535, nullable=false)
     */
    private $visibility;

    /**
     * @var string
     *
     * @ORM\Column(name="ADITIONAL", type="text", length=65535, nullable=false)
     */
    private $aditional;

    /**
     * @var string
     *
     * @ORM\Column(name="AVILYS_NAME", type="text", length=65535, nullable=false)
     */
    private $avilysName;


}

