<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosKaina
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_KAINA")
 * @ORM\Entity
 */
class IdmPaslaugosKaina
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=20, nullable=false)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=20, nullable=false)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=false)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=false)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=false)
     */
    private $modifyIp = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="KAINA", type="integer", nullable=true)
     */
    private $kaina = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="VALIUTA", type="string", length=50, nullable=true)
     */
    private $valiuta;

    /**
     * @var string
     *
     * @ORM\Column(name="KAINOS_APRASYMAS", type="text", length=65535, nullable=true)
     */
    private $kainosAprasymas;

    /**
     * @var integer
     *
     * @ORM\Column(name="TRUKME", type="integer", nullable=true)
     */
    private $trukme = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="TRUKME_KOMENTARAS", type="text", length=65535, nullable=true)
     */
    private $trukmeKomentaras;


}

