<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTbl58
 *
 * @ORM\Table(name="IDM_IDM_ATP_TBL_58", indexes={@ORM\Index(name="RECORD_LAST", columns={"RECORD_LAST"}), @ORM\Index(name="RECORD_MAIN_ID", columns={"RECORD_MAIN_ID"})})
 * @ORM\Entity
 */
class IdmAtpTbl58
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_MAIN_ID", type="string", length=25, nullable=false)
     */
    private $recordMainId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_RELATION_ID", type="integer", nullable=false)
     */
    private $recordRelationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_VERSION", type="integer", nullable=false)
     */
    private $recordVersion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RECORD_LAST", type="boolean", nullable=false)
     */
    private $recordLast;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_2", type="date", nullable=false)
     */
    private $col2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_3", type="time", nullable=false)
     */
    private $col3;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_4", type="text", length=255, nullable=false)
     */
    private $col4;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_1", type="text", length=255, nullable=false)
     */
    private $col1;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_5", type="text", length=255, nullable=false)
     */
    private $col5;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_6", type="text", length=65535, nullable=false)
     */
    private $col6;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_7", type="date", nullable=false)
     */
    private $col7;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_8", type="time", nullable=false)
     */
    private $col8;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_9", type="text", length=255, nullable=false)
     */
    private $col9;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_10", type="date", nullable=false)
     */
    private $col10;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_11", type="text", length=65535, nullable=false)
     */
    private $col11;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_12", type="text", length=255, nullable=false)
     */
    private $col12;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_13", type="text", length=255, nullable=false)
     */
    private $col13;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_14", type="text", length=65535, nullable=false)
     */
    private $col14;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_15", type="text", length=65535, nullable=false)
     */
    private $col15;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_16", type="text", length=255, nullable=false)
     */
    private $col16;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_17", type="text", length=255, nullable=false)
     */
    private $col17;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_18", type="text", length=255, nullable=false)
     */
    private $col18;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_19", type="text", length=65535, nullable=false)
     */
    private $col19;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_20", type="text", length=255, nullable=false)
     */
    private $col20;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;


}

