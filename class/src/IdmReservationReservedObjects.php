<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmReservationReservedObjects
 *
 * @ORM\Table(name="IDM_IDM_RESERVATION_RESERVED_OBJECTS")
 * @ORM\Entity
 */
class IdmReservationReservedObjects
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="INITIAL", type="integer", nullable=true)
     */
    private $initial;

    /**
     * @var integer
     *
     * @ORM\Column(name="OBJECT_ID", type="integer", nullable=false)
     */
    private $objectId;

    /**
     * @var string
     *
     * @ORM\Column(name="SPECIAL_DATA", type="text", length=65535, nullable=false)
     */
    private $specialData;

    /**
     * @var integer
     *
     * @ORM\Column(name="RESERVED_BY", type="integer", nullable=false)
     */
    private $reservedBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FOREIGN_CONTACT", type="boolean", nullable=true)
     */
    private $foreignContact = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_FROM", type="datetime", nullable=false)
     */
    private $dateFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_TO", type="datetime", nullable=true)
     */
    private $dateTo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="REPEAT_TYPE", type="boolean", nullable=true)
     */
    private $repeatType = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="REPEAT_TILL", type="date", nullable=true)
     */
    private $repeatTill;

    /**
     * @var boolean
     *
     * @ORM\Column(name="STATUS", type="boolean", nullable=true)
     */
    private $status = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="INFO", type="text", length=65535, nullable=true)
     */
    private $info;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DELETED", type="datetime", nullable=true)
     */
    private $deleted;


}

