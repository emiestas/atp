<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmIdbOrder
 *
 * @ORM\Table(name="IDM_IDM_IDB_ORDER", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmIdbOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=100, nullable=false)
     */
    private $type = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ORDER_DATA", type="text", nullable=true)
     */
    private $orderData;

    /**
     * @var string
     *
     * @ORM\Column(name="MAIL_DATA", type="text", nullable=true)
     */
    private $mailData;

    /**
     * @var string
     *
     * @ORM\Column(name="Sent", type="string", length=1, nullable=false)
     */
    private $sent = '';


}

