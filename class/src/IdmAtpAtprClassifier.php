<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpAtprClassifier
 *
 * @ORM\Table(name="IDM_IDM_ATP_ATPR_CLASSIFIER", indexes={@ORM\Index(name="PARENT_EXTERNAL_ID", columns={"PARENT_EXTERNAL_ID"}), @ORM\Index(name="CLASSIFICATOR_ID", columns={"CLASSIFICATOR_ID"}), @ORM\Index(name="TYPE_ID", columns={"TYPE_ID"}), @ORM\Index(name="CLAUSE_ID", columns={"CLAUSE_ID"})})
 * @ORM\Entity
 */
class IdmAtpAtprClassifier
{
    /**
     * @var integer
     *
     * @ORM\Column(name="EXTERNAL_ID", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $externalId;

    /**
     * @var string
     *
     * @ORM\Column(name="CODE", type="string", length=255, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="SHORT_NAME", type="string", length=5000, nullable=false)
     */
    private $shortName;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=5000, nullable=false)
     */
    private $name;

    /**
     * @var \IdmAtpAtprClassifier
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpAtprClassifier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PARENT_EXTERNAL_ID", referencedColumnName="EXTERNAL_ID")
     * })
     */
    private $parentExternal;

    /**
     * @var \IdmAtpClassificators
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpClassificators")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CLASSIFICATOR_ID", referencedColumnName="ID")
     * })
     */
    private $classificator;

    /**
     * @var \IdmAtpAtprClassifierType
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpAtprClassifierType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TYPE_ID", referencedColumnName="ID")
     * })
     */
    private $type;

    /**
     * @var \IdmAtpClauses
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpClauses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CLAUSE_ID", referencedColumnName="ID")
     * })
     */
    private $clause;


}

