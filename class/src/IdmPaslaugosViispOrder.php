<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosViispOrder
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_VIISP_ORDER", indexes={@ORM\Index(name="_ORDER_ID", columns={"ORDER_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosViispOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="IDBLOCK", type="text", length=65535, nullable=false)
     */
    private $idblock;

    /**
     * @var boolean
     *
     * @ORM\Column(name="REVISEDAPPLICATION", type="boolean", nullable=false)
     */
    private $revisedapplication;

    /**
     * @var string
     *
     * @ORM\Column(name="REVISEDAPPLICATIONCOMMENTS", type="text", length=65535, nullable=false)
     */
    private $revisedapplicationcomments;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="APPLICATIONDATE", type="date", nullable=false)
     */
    private $applicationdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DELIVERYTIME", type="date", nullable=false)
     */
    private $deliverytime;

    /**
     * @var string
     *
     * @ORM\Column(name="SERVICEPROVIDER", type="text", length=65535, nullable=false)
     */
    private $serviceprovider;

    /**
     * @var string
     *
     * @ORM\Column(name="SERVICEINFO", type="text", length=65535, nullable=false)
     */
    private $serviceinfo;

    /**
     * @var string
     *
     * @ORM\Column(name="CUSTOMER", type="text", length=65535, nullable=false)
     */
    private $customer;

    /**
     * @var string
     *
     * @ORM\Column(name="APPLICANT", type="text", length=65535, nullable=false)
     */
    private $applicant;

    /**
     * @var string
     *
     * @ORM\Column(name="PAYER", type="text", length=65535, nullable=false)
     */
    private $payer;

    /**
     * @var string
     *
     * @ORM\Column(name="APPLICATIONDATA", type="blob", nullable=false)
     */
    private $applicationdata;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="INSERT_DATE", type="datetime", nullable=false)
     */
    private $insertDate = 'CURRENT_TIMESTAMP';

    /**
     * @var \IdmPaslaugosUzsakymai
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosUzsakymai")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ORDER_ID", referencedColumnName="ID")
     * })
     */
    private $order;


}

