<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsMenu
 *
 * @ORM\Table(name="IDM_IDM_DHS_MENU", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmDhsMenu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=true)
     */
    private $parentId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE_ID", type="integer", nullable=false)
     */
    private $typeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="INCLFILE", type="text", nullable=false)
     */
    private $inclfile;

    /**
     * @var integer
     *
     * @ORM\Column(name="POS", type="integer", nullable=false)
     */
    private $pos = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="VISIBLE", type="string", length=5, nullable=true)
     */
    private $visible = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=50, nullable=false)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=50, nullable=false)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=false)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=false)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=false)
     */
    private $modifyIp = '';


}

