<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderExecutors
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_EXECUTORS", indexes={@ORM\Index(name="_ORDER_ID", columns={"ORDER_ID"}), @ORM\Index(name="_PERSON_ID", columns={"PERSON_ID"}), @ORM\Index(name="_IS_ACTIVE", columns={"IS_ACTIVE"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderExecutors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_ACTIVE", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var string
     *
     * @ORM\Column(name="NOTE", type="text", length=16777215, nullable=true)
     */
    private $note;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = 'CURRENT_TIMESTAMP';

    /**
     * @var \IdmPaslaugosUzsakymai
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosUzsakymai")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ORDER_ID", referencedColumnName="ID")
     * })
     */
    private $order;

    /**
     * @var \IdmMainPeople
     *
     * @ORM\ManyToOne(targetEntity="IdmMainPeople")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PERSON_ID", referencedColumnName="ID")
     * })
     */
    private $person;


}

