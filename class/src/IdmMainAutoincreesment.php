<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMainAutoincreesment
 *
 * @ORM\Table(name="IDM_IDM_MAIN_AUTOINCREESMENT", indexes={@ORM\Index(name="TABLENAME", columns={"TABLENAME"})})
 * @ORM\Entity
 */
class IdmMainAutoincreesment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="TABLENAME", type="string", length=255, nullable=false)
     */
    private $tablename = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="AI_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $aiId;


}

