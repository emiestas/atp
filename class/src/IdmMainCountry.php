<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMainCountry
 *
 * @ORM\Table(name="IDM_IDM_MAIN_COUNTRY")
 * @ORM\Entity
 */
class IdmMainCountry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="COUNTRY", type="string", length=100, nullable=false)
     */
    private $country = '';


}

