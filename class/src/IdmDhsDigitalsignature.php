<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsDigitalsignature
 *
 * @ORM\Table(name="IDM_IDM_DHS_DIGITALSIGNATURE", indexes={@ORM\Index(name="DOC_ID", columns={"DOC_ID"})})
 * @ORM\Entity
 */
class IdmDhsDigitalsignature
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=16777215, nullable=false)
     */
    private $shows;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     */
    private $docId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="EMPLOYER_ID", type="integer", nullable=false)
     */
    private $employerId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="EMPLOYER_INFO", type="text", nullable=true)
     */
    private $employerInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="EMPLOYER_SHORT", type="text", nullable=true)
     */
    private $employerShort;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=50, nullable=false)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=50, nullable=false)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=false)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=false)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=false)
     */
    private $modifyIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="STATE", type="string", length=15, nullable=true)
     */
    private $state = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CERTIFICATE", type="text", nullable=true)
     */
    private $certificate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="VALID_FROM", type="datetime", nullable=false)
     */
    private $validFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="VALID_TO", type="datetime", nullable=false)
     */
    private $validTo;


}

