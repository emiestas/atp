<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpSispTowpictures
 *
 * @ORM\Table(name="IDM_IDM_ATP_SISP_TOWPICTURES", indexes={@ORM\Index(name="IX_TowPictures_Tows", columns={"idTows"})})
 * @ORM\Entity
 */
class IdmAtpSispTowpictures
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idTows", type="integer", nullable=false)
     */
    private $idtows;

    /**
     * @var string
     *
     * @ORM\Column(name="Path", type="string", length=200, nullable=true)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="ThumbnailPath", type="string", length=200, nullable=true)
     */
    private $thumbnailpath;


}

