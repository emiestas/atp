<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWebdavFiles
 *
 * @ORM\Table(name="IDM_IDM_WEBDAV_FILES")
 * @ORM\Entity
 */
class IdmWebdavFiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="PATH", type="string", length=5000, nullable=false)
     */
    private $path;


}

