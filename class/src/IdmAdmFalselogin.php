<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAdmFalselogin
 *
 * @ORM\Table(name="IDM_IDM_ADM_FALSELOGIN", indexes={@ORM\Index(name="USERNAME", columns={"USERNAME"})})
 * @ORM\Entity
 */
class IdmAdmFalselogin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="USERNAME", type="string", length=32, nullable=false)
     */
    private $username = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LOGIN_DATE", type="datetime", nullable=false)
     */
    private $loginDate = '0000-00-00 00:00:00';

    /**
     * @var integer
     *
     * @ORM\Column(name="USER_ID", type="integer", nullable=false)
     */
    private $userId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="IP", type="string", length=32, nullable=false)
     */
    private $ip = '';


}

