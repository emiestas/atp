<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsJournalreceiver
 *
 * @ORM\Table(name="IDM_IDM_DHS_JOURNALRECEIVER", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsJournalreceiver
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ID", type="integer", nullable=false)
     */
    private $receiverId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_TYPE", type="integer", nullable=false)
     */
    private $receiverType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", nullable=false)
     */
    private $receiverShort;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", nullable=false)
     */
    private $receiverInfo;


}

