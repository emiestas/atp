<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsAutoregfolderreceiver
 *
 * @ORM\Table(name="IDM_IDM_DHS_AUTOREGFOLDERRECEIVER", indexes={@ORM\Index(name="FOLDER_ID", columns={"FOLDER_ID"})})
 * @ORM\Entity
 */
class IdmDhsAutoregfolderreceiver
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="FOLDER_ID", type="integer", nullable=false)
     */
    private $folderId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ID", type="integer", nullable=false)
     */
    private $receiverId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_TYPE", type="integer", nullable=false)
     */
    private $receiverType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", nullable=false)
     */
    private $receiverShort;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", nullable=false)
     */
    private $receiverInfo;


}

