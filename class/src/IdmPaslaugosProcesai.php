<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosProcesai
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_PROCESAI", indexes={@ORM\Index(name="PASLAUGOS_ID", columns={"PASLAUGOS_ID", "UZSAKYMAI_ID", "ETAPAI_ID"}), @ORM\Index(name="ETAPAI_ID", columns={"ETAPAI_ID"}), @ORM\Index(name="UZSAKYMAI_ID", columns={"UZSAKYMAI_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosProcesai
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", length=65535, nullable=false)
     */
    private $shows;

    /**
     * @var integer
     *
     * @ORM\Column(name="PASLAUGOS_ID", type="integer", nullable=true)
     */
    private $paslaugosId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="UZSAKYMAI_ID", type="integer", nullable=true)
     */
    private $uzsakymaiId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="ETAPAI_ID", type="integer", nullable=true)
     */
    private $etapaiId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="FOREIGNCONTACT_ID", type="integer", nullable=true)
     */
    private $foreigncontactId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ISLATE", type="string", length=5, nullable=true)
     */
    private $islate = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ISDONE", type="string", length=5, nullable=true)
     */
    private $isdone = '';

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_ID", type="string", length=32, nullable=true)
     */
    private $docId = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ID", type="integer", nullable=true)
     */
    private $receiverId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", length=65535, nullable=true)
     */
    private $receiverShort;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", length=65535, nullable=true)
     */
    private $receiverInfo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=true)
     */
    private $createDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXPIRE_DATE", type="datetime", nullable=true)
     */
    private $expireDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ACCOMPLISH_DATE", type="datetime", nullable=true)
     */
    private $accomplishDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_RESULT", type="text", length=65535, nullable=true)
     */
    private $docResult;


}

