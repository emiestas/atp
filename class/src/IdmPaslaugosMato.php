<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosMato
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_MATO", indexes={@ORM\Index(name="PASLAUGOS_ID", columns={"PASLAUGOS_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosMato
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PASLAUGOS_ID", type="integer", nullable=false)
     */
    private $paslaugosId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_TYPE", type="integer", nullable=false)
     */
    private $peopleType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PEOPLE_SHORT", type="text", nullable=false)
     */
    private $peopleShort;

    /**
     * @var string
     *
     * @ORM\Column(name="PEOPLE_INFO", type="text", nullable=false)
     */
    private $peopleInfo;


}

