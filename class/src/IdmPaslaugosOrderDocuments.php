<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderDocuments
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_DOCUMENTS", indexes={@ORM\Index(name="_ORDER_ID", columns={"ORDER_ID"}), @ORM\Index(name="_ORDER__TYPE", columns={"ORDER_ID", "TYPE"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderDocuments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCUMENT_ID", type="integer", nullable=false)
     */
    private $documentId;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=20, nullable=false)
     */
    private $type;

    /**
     * @var \IdmPaslaugosUzsakymai
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosUzsakymai")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ORDER_ID", referencedColumnName="ID")
     * })
     */
    private $order;


}

