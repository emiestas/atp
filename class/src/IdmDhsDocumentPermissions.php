<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsDocumentPermissions
 *
 * @ORM\Table(name="IDM_IDM_DHS_DOCUMENT_PERMISSIONS", uniqueConstraints={@ORM\UniqueConstraint(name="DOC_X_PERSON", columns={"DOCUMENT_ID", "PEOPLE_ID"})}, indexes={@ORM\Index(name="DOCUMENT_ID", columns={"DOCUMENT_ID"}), @ORM\Index(name="PEOPLE_ID", columns={"PEOPLE_ID"})})
 * @ORM\Entity
 */
class IdmDhsDocumentPermissions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="DOCUMENT_ID", type="integer", nullable=false)
     */
    private $documentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId;


}

