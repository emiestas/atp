<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosOrderRevisionFiles
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_ORDER_REVISION_FILES", indexes={@ORM\Index(name="_REVISION_ID", columns={"REVISION_ID"}), @ORM\Index(name="_FILE_ID", columns={"FILE_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosOrderRevisionFiles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \IdmPaslaugosOrderRevisions
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosOrderRevisions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="REVISION_ID", referencedColumnName="ID")
     * })
     */
    private $revision;

    /**
     * @var \IdmPaslaugosFiles
     *
     * @ORM\ManyToOne(targetEntity="IdmPaslaugosFiles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FILE_ID", referencedColumnName="ID")
     * })
     */
    private $file;


}

