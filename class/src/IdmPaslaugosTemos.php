<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosTemos
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_TEMOS", indexes={@ORM\Index(name="SHOWS", columns={"SHOWS"})})
 * @ORM\Entity
 */
class IdmPaslaugosTemos
{
    /**
     * @var string
     *
     * @ORM\Column(name="VISIBLE", type="string", length=5, nullable=true)
     */
    private $visible = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=255, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=20, nullable=false)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=20, nullable=false)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=false)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=false)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=false)
     */
    private $modifyIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SHORT", type="string", length=100, nullable=true)
     */
    private $short = '';

    /**
     * @var string
     *
     * @ORM\Column(name="AVILYS_SHOWS", type="string", length=100, nullable=true)
     */
    private $avilysShows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="MENU_ID", type="integer", nullable=true)
     */
    private $menuId = '0';


}

