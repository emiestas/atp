<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosBankai
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_BANKAI")
 * @ORM\Entity
 */
class IdmPaslaugosBankai
{
    /**
     * @var string
     *
     * @ORM\Column(name="PRIVATE_KEY", type="text", nullable=true)
     */
    private $privateKey;

    /**
     * @var string
     *
     * @ORM\Column(name="KEYPASS", type="text", nullable=true)
     */
    private $keypass;

    /**
     * @var string
     *
     * @ORM\Column(name="VALIUTA", type="string", length=100, nullable=true)
     */
    private $valiuta = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ACCOUNT", type="string", length=50, nullable=true)
     */
    private $account = '';

    /**
     * @var string
     *
     * @ORM\Column(name="BANKCODE", type="string", length=50, nullable=true)
     */
    private $bankcode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ORGANIZATION", type="text", nullable=true)
     */
    private $organization;

    /**
     * @var string
     *
     * @ORM\Column(name="PHP_OUT_FILE", type="string", length=50, nullable=true)
     */
    private $phpOutFile = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PHP_IN_FILE", type="string", length=50, nullable=true)
     */
    private $phpInFile = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PUBLIC_KEY", type="text", nullable=true)
     */
    private $publicKey;

    /**
     * @var string
     *
     * @ORM\Column(name="BANK_URL", type="text", nullable=true)
     */
    private $bankUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=20, nullable=false)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=20, nullable=false)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=false)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=false)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=false)
     */
    private $modifyIp = '';


}

