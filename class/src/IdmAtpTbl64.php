<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTbl64
 *
 * @ORM\Table(name="IDM_IDM_ATP_TBL_64", indexes={@ORM\Index(name="RECORD_LAST", columns={"RECORD_LAST"}), @ORM\Index(name="RECORD_MAIN_ID", columns={"RECORD_MAIN_ID"})})
 * @ORM\Entity
 */
class IdmAtpTbl64
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_MAIN_ID", type="string", length=25, nullable=false)
     */
    private $recordMainId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_RELATION_ID", type="integer", nullable=false)
     */
    private $recordRelationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_VERSION", type="integer", nullable=false)
     */
    private $recordVersion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RECORD_LAST", type="boolean", nullable=false)
     */
    private $recordLast;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_1", type="text", length=255, nullable=false)
     */
    private $col1;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_2", type="text", length=255, nullable=false)
     */
    private $col2;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_3", type="text", length=65535, nullable=false)
     */
    private $col3;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_4", type="text", length=255, nullable=false)
     */
    private $col4;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_5", type="text", length=65535, nullable=false)
     */
    private $col5;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_6", type="text", length=255, nullable=false)
     */
    private $col6;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_7", type="text", length=65535, nullable=false)
     */
    private $col7;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_8", type="date", nullable=false)
     */
    private $col8;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_9", type="text", length=255, nullable=false)
     */
    private $col9;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_10", type="date", nullable=false)
     */
    private $col10;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_11", type="text", length=255, nullable=false)
     */
    private $col11;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_12", type="text", length=255, nullable=false)
     */
    private $col12;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;


}

