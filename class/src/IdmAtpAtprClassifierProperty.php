<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpAtprClassifierProperty
 *
 * @ORM\Table(name="IDM_IDM_ATP_ATPR_CLASSIFIER_PROPERTY", indexes={@ORM\Index(name="CLASSIFIER_ID", columns={"CLASSIFIER_ID"}), @ORM\Index(name="KEY_ID", columns={"KEY_ID"})})
 * @ORM\Entity
 */
class IdmAtpAtprClassifierProperty
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="KEY", type="string", length=255, nullable=false)
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(name="VALUE", type="string", length=255, nullable=false)
     */
    private $value;

    /**
     * @var \IdmAtpAtprClassifierPropertyKey
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpAtprClassifierPropertyKey")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="KEY_ID", referencedColumnName="ID")
     * })
     */
    private $key2;

    /**
     * @var \IdmAtpAtprClassifier
     *
     * @ORM\ManyToOne(targetEntity="IdmAtpAtprClassifier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CLASSIFIER_ID", referencedColumnName="EXTERNAL_ID")
     * })
     */
    private $classifier;


}

