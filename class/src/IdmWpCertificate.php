<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWpCertificate
 *
 * @ORM\Table(name="IDM_IDM_WP_CERTIFICATE")
 * @ORM\Entity
 */
class IdmWpCertificate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=255, nullable=false)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CERT_DATA", type="text", nullable=false)
     */
    private $certData;


}

