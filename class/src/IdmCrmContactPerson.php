<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmCrmContactPerson
 *
 * @ORM\Table(name="IDM_IDM_CRM_CONTACT_PERSON", indexes={@ORM\Index(name="LAST_NAME", columns={"LAST_NAME"}), @ORM\Index(name="NAME", columns={"NAME"})})
 * @ORM\Entity
 */
class IdmCrmContactPerson
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="LAST_NAME", type="string", length=255, nullable=false)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="POSITION", type="string", length=128, nullable=true)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="PHONE", type="string", length=30, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="EMAIL", type="string", length=128, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="INFO", type="text", length=16777215, nullable=true)
     */
    private $info;


}

