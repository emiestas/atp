<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsJournalpeople
 *
 * @ORM\Table(name="IDM_IDM_DHS_JOURNALPEOPLE", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsJournalpeople
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_ID", type="integer", nullable=false)
     */
    private $peopleId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PEOPLE_TYPE", type="integer", nullable=false)
     */
    private $peopleType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PEOPLE_SHORT", type="text", nullable=false)
     */
    private $peopleShort;

    /**
     * @var string
     *
     * @ORM\Column(name="PEOPLE_INFO", type="text", nullable=false)
     */
    private $peopleInfo;


}

