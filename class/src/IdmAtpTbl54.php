<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpTbl54
 *
 * @ORM\Table(name="IDM_IDM_ATP_TBL_54", indexes={@ORM\Index(name="RECORD_LAST", columns={"RECORD_LAST"}), @ORM\Index(name="RECORD_MAIN_ID", columns={"RECORD_MAIN_ID"})})
 * @ORM\Entity
 */
class IdmAtpTbl54
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_MAIN_ID", type="string", length=25, nullable=false)
     */
    private $recordMainId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_VERSION", type="integer", nullable=false)
     */
    private $recordVersion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="RECORD_LAST", type="boolean", nullable=false)
     */
    private $recordLast;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_1", type="date", nullable=false)
     */
    private $col1;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_2", type="text", length=255, nullable=false)
     */
    private $col2;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_10", type="text", length=255, nullable=false)
     */
    private $col10;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_31", type="text", length=255, nullable=false)
     */
    private $col31;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_32", type="text", length=65535, nullable=false)
     */
    private $col32;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_33", type="text", length=255, nullable=false)
     */
    private $col33;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_34", type="text", length=255, nullable=false)
     */
    private $col34;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_35", type="text", length=255, nullable=false)
     */
    private $col35;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_44", type="text", length=65535, nullable=false)
     */
    private $col44;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_45", type="text", length=65535, nullable=false)
     */
    private $col45;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_48", type="date", nullable=false)
     */
    private $col48;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="COL_49", type="time", nullable=false)
     */
    private $col49;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_50", type="text", length=255, nullable=false)
     */
    private $col50;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_51", type="text", length=255, nullable=false)
     */
    private $col51;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_52", type="text", length=255, nullable=false)
     */
    private $col52;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_53", type="text", length=255, nullable=false)
     */
    private $col53;

    /**
     * @var string
     *
     * @ORM\Column(name="COL_54", type="text", length=255, nullable=false)
     */
    private $col54;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate;


}

