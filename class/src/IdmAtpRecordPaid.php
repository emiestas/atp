<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpRecordPaid
 *
 * @ORM\Table(name="IDM_IDM_ATP_RECORD_PAID", uniqueConstraints={@ORM\UniqueConstraint(name="RECORD_ID", columns={"RECORD_ID"})})
 * @ORM\Entity
 */
class IdmAtpRecordPaid
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECORD_ID", type="integer", nullable=false)
     */
    private $recordId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="STATUS", type="boolean", nullable=false)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PAYMENT_STATUS", type="boolean", nullable=true)
     */
    private $paymentStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE", type="datetime", nullable=false)
     */
    private $date = 'CURRENT_TIMESTAMP';


}

