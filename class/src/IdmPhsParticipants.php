<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPhsParticipants
 *
 * @ORM\Table(name="IDM_IDM_PHS_PARTICIPANTS", indexes={@ORM\Index(name="PROJECT_ID", columns={"PROJECT_ID"})})
 * @ORM\Entity
 */
class IdmPhsParticipants
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="PROJECT_ID", type="integer", nullable=false)
     */
    private $projectId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PARTICIPANT_ID", type="integer", nullable=false)
     */
    private $participantId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="PARTICIPANT_INFO", type="text", length=65535, nullable=true)
     */
    private $participantInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="PARTICIPANT_SHORT", type="text", length=65535, nullable=true)
     */
    private $participantShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARTICIPANT_TYPE", type="integer", nullable=false)
     */
    private $participantType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="PARTICIPANT_ORGNODE_ID", type="integer", nullable=false)
     */
    private $participantOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="STATE", type="string", length=15, nullable=true)
     */
    private $state = '';


}

