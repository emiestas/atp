<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpUserLoad
 *
 * @ORM\Table(name="IDM_IDM_ATP_USER_LOAD")
 * @ORM\Entity
 */
class IdmAtpUserLoad
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="M_PEOPLE_ID", type="integer", nullable=false)
     */
    private $mPeopleId;

    /**
     * @var integer
     *
     * @ORM\Column(name="M_DEPARTMENT_ID", type="integer", nullable=false)
     */
    private $mDepartmentId;

    /**
     * @var string
     *
     * @ORM\Column(name="RECORD_ID", type="string", length=25, nullable=false)
     */
    private $recordId;


}

