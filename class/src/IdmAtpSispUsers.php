<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpSispUsers
 *
 * @ORM\Table(name="IDM_IDM_ATP_SISP_USERS")
 * @ORM\Entity
 */
class IdmAtpSispUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="UserName", type="string", length=100, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="Password", type="string", length=100, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="DefaultPage", type="string", length=200, nullable=false)
     */
    private $defaultpage = 'Tows.aspx';

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessAdmin", type="boolean", nullable=false)
     */
    private $accessadmin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessTowsView", type="boolean", nullable=false)
     */
    private $accesstowsview;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessTowsEdit", type="boolean", nullable=false)
     */
    private $accesstowsedit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessTowsRelease", type="boolean", nullable=false)
     */
    private $accesstowsrelease;

    /**
     * @var string
     *
     * @ORM\Column(name="FullName", type="string", length=100, nullable=true)
     */
    private $fullname;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessSMS", type="boolean", nullable=false)
     */
    private $accesssms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessInspection", type="boolean", nullable=false)
     */
    private $accessinspection;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessBlocks", type="boolean", nullable=false)
     */
    private $accessblocks;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessHalts", type="boolean", nullable=false)
     */
    private $accesshalts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessReports", type="boolean", nullable=false)
     */
    private $accessreports;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessStatus1", type="boolean", nullable=false)
     */
    private $accessstatus1;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessStatus2", type="boolean", nullable=false)
     */
    private $accessstatus2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessStatus3", type="boolean", nullable=false)
     */
    private $accessstatus3;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessStatus4", type="boolean", nullable=false)
     */
    private $accessstatus4;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessGyventojuLeidimai", type="boolean", nullable=false)
     */
    private $accessgyventojuleidimai;

    /**
     * @var boolean
     *
     * @ORM\Column(name="GyventojuLeidimaiEdit", type="boolean", nullable=false)
     */
    private $gyventojuleidimaiedit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="KontroleAccess", type="boolean", nullable=false)
     */
    private $kontroleaccess;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessInspectionEdit", type="boolean", nullable=false)
     */
    private $accessinspectionedit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="TowsReadOnly", type="boolean", nullable=false)
     */
    private $towsreadonly;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessStatus5", type="boolean", nullable=false)
     */
    private $accessstatus5;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessPaymentsView", type="boolean", nullable=false)
     */
    private $accesspaymentsview;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AccessPaymentsEdit", type="boolean", nullable=false)
     */
    private $accesspaymentsedit;


}

