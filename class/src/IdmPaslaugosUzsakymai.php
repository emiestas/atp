<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmPaslaugosUzsakymai
 *
 * @ORM\Table(name="IDM_IDM_PASLAUGOS_UZSAKYMAI", indexes={@ORM\Index(name="FOREIGNCONTACT_ID", columns={"FOREIGNCONTACT_ID"}), @ORM\Index(name="PASLAUGOS_ID", columns={"PASLAUGOS_ID"}), @ORM\Index(name="AVILIO_ID", columns={"AVILIO_ID"}), @ORM\Index(name="STATUSAS_ID", columns={"STATUSAS_ID"}), @ORM\Index(name="MODIFY_DATE", columns={"MODIFY_DATE"}), @ORM\Index(name="STATUSAS_ID_2", columns={"STATUSAS_ID", "PSL_END_SEND"}), @ORM\Index(name="_RESULT_DOCUMENT_ID", columns={"RESULT_DOCUMENT_ID"})})
 * @ORM\Entity
 */
class IdmPaslaugosUzsakymai
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ENTERPRISECODE", type="string", length=20, nullable=false)
     */
    private $enterprisecode;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=true)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_NUMBER", type="string", length=200, nullable=false)
     */
    private $docNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="FOREIGNCONTACT_ID", type="integer", nullable=false)
     */
    private $foreigncontactId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="EXECUTOR_ID", type="integer", nullable=false)
     */
    private $executorId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CUSTOMER_ID", type="integer", nullable=false)
     */
    private $customerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="APPLICANT_ID", type="integer", nullable=false)
     */
    private $applicantId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PAYER_ID", type="integer", nullable=true)
     */
    private $payerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PASLAUGOS_ID", type="integer", nullable=true)
     */
    private $paslaugosId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUSAS_ID", type="integer", nullable=false)
     */
    private $statusasId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="KAINA", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $kaina = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=20, nullable=true)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=20, nullable=true)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=true)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=true)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=true)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=true)
     */
    private $modifyIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_ID", type="text", nullable=false)
     */
    private $docId;

    /**
     * @var integer
     *
     * @ORM\Column(name="SYS_ID", type="integer", nullable=true)
     */
    private $sysId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="TEXT_DATA", type="text", nullable=true)
     */
    private $textData;

    /**
     * @var string
     *
     * @ORM\Column(name="PSL_START_SEND", type="string", length=10, nullable=true)
     */
    private $pslStartSend = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PSL_END_SEND", type="string", length=10, nullable=true)
     */
    private $pslEndSend = '';

    /**
     * @var string
     *
     * @ORM\Column(name="XML_REQUEST", type="text", nullable=true)
     */
    private $xmlRequest;

    /**
     * @var string
     *
     * @ORM\Column(name="VELUOJA", type="string", length=5, nullable=true)
     */
    private $veluoja = '';

    /**
     * @var string
     *
     * @ORM\Column(name="VARTOTOJAI_ID", type="string", length=255, nullable=false)
     */
    private $vartotojaiId;

    /**
     * @var string
     *
     * @ORM\Column(name="AVILIO_ID", type="string", length=255, nullable=false)
     */
    private $avilioId;

    /**
     * @var string
     *
     * @ORM\Column(name="GIS_START_SEND", type="string", length=100, nullable=false)
     */
    private $gisStartSend;

    /**
     * @var string
     *
     * @ORM\Column(name="GIS_END_SEND", type="string", length=100, nullable=false)
     */
    private $gisEndSend;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ACCOMPLISH_DATE", type="datetime", nullable=false)
     */
    private $accomplishDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="EXPIRE_DATE", type="datetime", nullable=false)
     */
    private $expireDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ANSWER_TYPE", type="boolean", nullable=true)
     */
    private $answerType;

    /**
     * @var string
     *
     * @ORM\Column(name="ANSWER_COMMENT", type="text", length=65535, nullable=true)
     */
    private $answerComment;

    /**
     * @var integer
     *
     * @ORM\Column(name="ANSWER_FILE_ID", type="integer", nullable=true)
     */
    private $answerFileId;

    /**
     * @var integer
     *
     * @ORM\Column(name="RESULT_DOCUMENT_ID", type="integer", nullable=true)
     */
    private $resultDocumentId;


}

