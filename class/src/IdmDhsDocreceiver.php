<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsDocreceiver
 *
 * @ORM\Table(name="IDM_IDM_DHS_DOCRECEIVER", indexes={@ORM\Index(name="DOC_ID", columns={"DOC_ID"})})
 * @ORM\Entity
 */
class IdmDhsDocreceiver
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false)
     */
    private $docId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ID", type="integer", nullable=false)
     */
    private $receiverId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_INFO", type="text", nullable=true)
     */
    private $receiverInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVER_SHORT", type="text", nullable=true)
     */
    private $receiverShort;

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_TYPE", type="integer", nullable=false)
     */
    private $receiverType = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="RECEIVER_ORGNODE_ID", type="integer", nullable=false)
     */
    private $receiverOrgnodeId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="STATE", type="string", length=15, nullable=true)
     */
    private $state = '';

    /**
     * @var string
     *
     * @ORM\Column(name="RECEIVE_ISNEW", type="string", length=5, nullable=false)
     */
    private $receiveIsnew;


}

