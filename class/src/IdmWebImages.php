<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmWebImages
 *
 * @ORM\Table(name="IDM_IDM_WEB_IMAGES", indexes={@ORM\Index(name="ID", columns={"ID"})})
 * @ORM\Entity
 */
class IdmWebImages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="OBJECT_ID", type="integer", nullable=false)
     */
    private $objectId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="IMAGE_TYPE", type="string", length=10, nullable=true)
     */
    private $imageType = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ORIGINAL_FILE", type="string", length=255, nullable=true)
     */
    private $originalFile = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IMAGE_TITLE", type="string", length=100, nullable=true)
     */
    private $imageTitle = '';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_BY", type="string", length=50, nullable=false)
     */
    private $createBy = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_BY", type="string", length=50, nullable=false)
     */
    private $modifyBy = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFY_DATE", type="datetime", nullable=false)
     */
    private $modifyDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=false)
     */
    private $createIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="MODIFY_IP", type="string", length=15, nullable=false)
     */
    private $modifyIp = '';

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=true)
     */
    private $shows = '';

    /**
     * @var string
     *
     * @ORM\Column(name="IMAGE_FILE", type="string", length=255, nullable=true)
     */
    private $imageFile = '';


}

