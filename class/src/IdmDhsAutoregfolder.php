<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmDhsAutoregfolder
 *
 * @ORM\Table(name="IDM_IDM_DHS_AUTOREGFOLDER", indexes={@ORM\Index(name="JOURNAL_ID", columns={"JOURNAL_ID"})})
 * @ORM\Entity
 */
class IdmDhsAutoregfolder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="RULENAME", type="string", length=255, nullable=false)
     */
    private $rulename;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="text", nullable=false)
     */
    private $shows;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_DATA", type="text", nullable=false)
     */
    private $docData;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC_NOTES", type="text", nullable=false)
     */
    private $docNotes;

    /**
     * @var integer
     *
     * @ORM\Column(name="JOURNAL_ID", type="integer", nullable=false)
     */
    private $journalId;

    /**
     * @var integer
     *
     * @ORM\Column(name="CASE_ID", type="integer", nullable=false)
     */
    private $caseId;

    /**
     * @var integer
     *
     * @ORM\Column(name="SCENARIO_ID", type="integer", nullable=false)
     */
    private $scenarioId;

    /**
     * @var integer
     *
     * @ORM\Column(name="REGISTRATOR_ID", type="integer", nullable=false)
     */
    private $registratorId;

    /**
     * @var string
     *
     * @ORM\Column(name="USEABLE", type="string", length=2, nullable=false)
     */
    private $useable;

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCHPART", type="string", length=255, nullable=false)
     */
    private $searchpart;

    /**
     * @var string
     *
     * @ORM\Column(name="DELDOC", type="string", length=5, nullable=false)
     */
    private $deldoc;

    /**
     * @var string
     *
     * @ORM\Column(name="SEARCHMSG", type="string", length=255, nullable=false)
     */
    private $searchmsg;

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS_ID", type="integer", nullable=false)
     */
    private $statusId;


}

