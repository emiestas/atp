<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmFvasXxFinInt201GsSaskEilV
 *
 * @ORM\Table(name="IDM_IDM_FVAS_XX_FIN_INT201_GS_SASK_EIL_V")
 * @ORM\Entity
 */
class IdmFvasXxFinInt201GsSaskEilV
{
    /**
     * @var integer
     *
     * @ORM\Column(name="SASKAITOS_ID", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $saskaitosId;

    /**
     * @var integer
     *
     * @ORM\Column(name="EILUTES_NUMERIS", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $eilutesNumeris;

    /**
     * @var string
     *
     * @ORM\Column(name="PASLAUGOS_PAVADINIMAS", type="string", length=281, nullable=true)
     */
    private $paslaugosPavadinimas;

    /**
     * @var string
     *
     * @ORM\Column(name="MAT_VNT", type="string", length=25, nullable=true)
     */
    private $matVnt;

    /**
     * @var integer
     *
     * @ORM\Column(name="KIEKIS", type="integer", nullable=true)
     */
    private $kiekis;

    /**
     * @var string
     *
     * @ORM\Column(name="KAINA", type="decimal", precision=14, scale=4, nullable=true)
     */
    private $kaina;

    /**
     * @var string
     *
     * @ORM\Column(name="SUMA", type="decimal", precision=14, scale=4, nullable=true)
     */
    private $suma;


}

