<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmCrmContactFileRel
 *
 * @ORM\Table(name="IDM_IDM_CRM_CONTACT_FILE_REL", indexes={@ORM\Index(name="CONTACT_ID", columns={"CONTACT_ID", "FILE_ID"}), @ORM\Index(name="FILE_ID", columns={"FILE_ID"}), @ORM\Index(name="CONTACT_ID_2", columns={"CONTACT_ID"})})
 * @ORM\Entity
 */
class IdmCrmContactFileRel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="CONTACT_ID", type="integer", nullable=false)
     */
    private $contactId;

    /**
     * @var integer
     *
     * @ORM\Column(name="FILE_ID", type="integer", nullable=false)
     */
    private $fileId;


}

