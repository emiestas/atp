<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmMetHistory
 *
 * @ORM\Table(name="IDM_IDM_MET_HISTORY", indexes={@ORM\Index(name="MEETING_ID", columns={"MEETING_ID"})})
 * @ORM\Entity
 */
class IdmMetHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_OLD", type="integer", nullable=true)
     */
    private $idOld;

    /**
     * @var string
     *
     * @ORM\Column(name="SHOWS", type="string", length=100, nullable=false)
     */
    private $shows = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="MEETING_ID", type="integer", nullable=false)
     */
    private $meetingId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="EMPLOYER_ID", type="integer", nullable=false)
     */
    private $employerId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="EMPLOYER_INFO", type="text", nullable=true)
     */
    private $employerInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="EMPLOYER_SHORT", type="text", nullable=true)
     */
    private $employerShort;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATE_DATE", type="datetime", nullable=false)
     */
    private $createDate = '2007-01-01 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="CREATE_IP", type="string", length=15, nullable=false)
     */
    private $createIp = '';


}

