<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpRecordXVmsaImportStatusTemp
 *
 * @ORM\Table(name="IDM_IDM_ATP_RECORD_X_VMSA_IMPORT_STATUS_TEMP")
 * @ORM\Entity
 */
class IdmAtpRecordXVmsaImportStatusTemp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IS_ACTIVE", type="boolean", nullable=false)
     */
    private $isActive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="LAST_ACTIVITY", type="datetime", nullable=false)
     */
    private $lastActivity = 'CURRENT_TIMESTAMP';


}

