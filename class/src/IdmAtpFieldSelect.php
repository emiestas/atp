<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * IdmAtpFieldSelect
 *
 * @ORM\Table(name="IDM_IDM_ATP_FIELD_SELECT")
 * @ORM\Entity
 */
class IdmAtpFieldSelect
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="integer", nullable=false)
     */
    private $parentId;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=255, nullable=false)
     */
    private $name;


}

