<?php

namespace Atp;

class Cache
{
    private $cache;

    /**
     * @param string $type Cache type.
     * @param string $key Cache identificator.
     * @return boolean
     */
    public function is($type, $key)
    {
        $identificator = $this->hash($key);
        return isset($this->cache->$type->$identificator);
    }

    /**
     * Get data.
     *
     * @param string $type Cache type.
     * @param string $key Cache identificator.
     * @return mixed
     */
    public function get($type, $key)
    {
        $identificator = $this->hash($key);
        if (isset($this->cache->$type->$identificator)) {
            return $this->cache->$type->$identificator;
        }

        return false;
    }

    /**
     * Set data.
     * 
     * @param string $type Cache type.
     * @param string $key Cache identificator.
     * @param mixed $data cache data.
     * @return boolean
     */
    public function set($type, $key, $data)
    {
        if (isset($this->cache->$type) === false) {
            if (isset($this->cache) === false) {
                $this->cache = new \stdClass();
            }
            $this->cache->$type = new \stdClass();
        }

        $identificator = $this->hash($key);
        $this->cache->$type->$identificator = $data;

        return true;
    }

    /**
     * @param string $type Cache type.
     * @param string $key Cache identificator.
     * @return boolean
     */
    public function delete($type, $key)
    {
        $identificator = $this->hash($key);
        if (isset($this->cache->$type->$identificator)) {
            unset($this->cache->$type->$identificator);
        }

        return true;
    }

    /**
     * Return all data.
     * 
     * @return array[]
     */
    public function dump()
    {
        $arr = array();
        foreach ($this->cache as $type => &$a) {
            $arr[$type] = array();
            foreach ($a as $identificator => $data) {
                $arr[$type][$identificator] = json_encode($data);
            }
        }

        return $arr;
    }

    /**
     * @param string $key Cache identificator.
     * @return string
     */
    private function hash($key)
    {
        return hash('sha256', $key);
    }
}
