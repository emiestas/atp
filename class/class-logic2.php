<?php

namespace Atp;

class Logic2
{

    /**
     * @var \Atp\Core
     */
    private $core;

    /**
     *
     * @param type $core
     */
    function __construct($core)
    {
        $this->core = $core;
    }

    /**
     * Gauna laukų informaciją
     * @author Justinas Malūkas
     * @param array $params [optional] Parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @param string $fields [optional] Duomenų bazės laukai, kuriuos surinkti. Default: <b>null</b>
     * @return array Laukų informacija
     */
    public function get_fields($params = null, $limit = FALSE, $order = null, $fields = null)
    {
        $ret = array();

        if (empty($params) === TRUE)
            $params = array();

        // TODO: ACT laukai vistiek turi teisingus ID, tai reikia pridėti paiešką pagal `ID`
        if (isset($params['ITYPE']) === TRUE && in_array($params['ITYPE'], array(
                'ACT', 'CLAUSE'))) {
            $ret = $this->get_db(PREFIX . 'ATP_FIELD', array_merge((array) $params, array(
                'ITEM_ID' => 0)), $limit, $order, false, $fields);

            if ($limit === TRUE)
                $ret = array($ret['ID'] => $ret);

            if (empty($params['ITEM_ID']) === FALSE) {
                if ($params['ITYPE'] === 'ACT') {
                    $values = $this->get_act_default_fields_values($params['ITEM_ID']);
                } elseif ($params['ITYPE'] === 'CLAUSE') {
                    $values = $this->get_clause_default_fields_values($params['ITEM_ID']);
                }

                foreach ($ret as &$field) {
                    $field['ITEM_ID'] = $params['ITEM_ID'];
                    $field['DEFAULT'] = $values[$field['ID']];

                    if ($limit === TRUE)
                        return $field;
                }
            }
        } else {
            return $this->get_db(PREFIX . 'ATP_FIELD', $params, $limit, $order, false, $fields);
        }
        return $ret;
    }

    /**
     * Gauna visų tolygių arba gilesnių laukų informaciją
     * @param array $params [optional] Parametrų masyvas
     * @param array $fields [otional] Rasti laukai
     * @return array Laukų informacija
     */
    public function get_fields_all($params = null, &$fields = array())
    {

        if (empty($params) === TRUE)
            $params = array();

        $arr = $this->get_fields($params);
        foreach ($arr as $field) {
            $fields[$field['ID']] = $field;
            // Klasifikatorius
            if ($field['TYPE'] === '18') {
                if (empty($field['SOURCE']) === FALSE) {
                    // Klasifikatoriaus vaikai
                    $data = $this->get_classificator(array('PARENT_ID' => (int) $field['SOURCE']));
                    foreach ($data as $arr) {
                        $this->get_fields_all(array('ITEM_ID' => $arr['ID'], 'ITYPE' => 'CLASS'), $fields);
                    }
                }
            } else
            // Straipsnis
            if ($field['TYPE'] === '19') {
                $this->get_fields_all(array('ITYPE' => 'CLAUSE'), $fields);
            } else
            // Veika
            if ($field['TYPE'] === '23') {
                //$this->get_fields_all(array('ITEM_ID' => 0, 'ITYPE' => 'DEED'), $fields);
                $this->get_fields_all(array('ITYPE' => 'ACT'), $fields);
                $this->get_fields_all(array('ITYPE' => 'CLAUSE'), $fields);
            }
            // Web-servisas
            if ($field['WS'] === '1') {
                $this->get_fields_all(array('ITEM_ID' => $field['ID'], 'ITYPE' => 'WS'), $fields);
            }
        }
        return $fields;
    }

    /**
     * Galimi veikos laukai
     * @author Justinas Malūkas
     * @return array
     */
    public function get_act_default_fields_structure()
    {
        $default = array(
            'ID' => null, 'ITEM_ID' => '', 'ITYPE' => 'ACT',
            'LABEL' => null, 'NAME' => null, 'SORT' => null, 'TYPE' => null,
            'SOURCE' => '0', /*'VALUE' => '0', 'VALUE_TYPE' => '0',
            'OTHER_SOURCE' => '0', 'OTHER_VALUE' => '0', 'FILL' => '0',*/
            'IN_LIST' => '0', 'RELATION' => '0', 'MANDATORY' => '0',
            'VISIBLE' => '1', 'COMPETENCE' => '1', 'ORD' => '1',
            'TEXT' => '', 'DEFAULT' => '', 'DEFAULT_ID' => '',
            'UPDATE_DATE' => '2014-01-01 00:00:00', 'CREATE_DATE' => '2014-01-01 00:00:00'
        );
        $custom = array(
            array(
                'ID' => 1, 'LABEL' => 'Teisės akto subjektas', 'NAME' => 'ACOL_1',
                'SORT' => '2', 'TYPE' => '7', 'ORD' => '2'),
            array(
                'ID' => 2, 'LABEL' => 'Teisės akto numeris', 'NAME' => 'ACOL_2',
                'SORT' => '2', 'TYPE' => '7', 'ORD' => '3'),
            array(
                'ID' => 3, 'LABEL' => 'Teisės akto rūšis', 'NAME' => 'ACOL_3',
                'SORT' => '2', 'TYPE' => '7', 'ORD' => '4'),
            array(
                'ID' => 4, 'LABEL' => 'Teisės akto data', 'NAME' => 'ACOL_4',
                'SORT' => '7', 'TYPE' => '12', 'ORD' => '5'),
            array(
                'ID' => 6, 'LABEL' => 'Tekstas', 'NAME' => 'ACOL_5',
                'SORT' => '2', 'TYPE' => '8', 'ORD' => '1')
        );

        foreach ($custom as &$arr) {
            $arr = array_merge($default, $arr);
        }
        return $custom;
    }

    /**
     * Gauna teisės akto laukų  reikšmes
     * @author Justinas Malūkas
     * @param int $id Teisės akto ID
     * @param array $data [optional] Teisės akto informacija
     * @return array Teisės akto laukų reikšmės
     */
    public function get_act_default_fields_values($id, &$data = array())
    {
        if (empty($id) === TRUE) {
            return null;
        }

        if (empty($data) === TRUE) {
            $data = $this->get_act(array('ID' => $id));
        }

        return array(
            1 => $data['VALUE_1'],
            2 => $data['VALUE_2'],
            3 => $data['VALUE_3'],
            4 => $data['VALUE_4'],
            6 => $data['VALUE_6']
        );

//        if (empty($data['FIELDS_VALUES']) === TRUE) {
//            return null;
//        }
//
//        return unserialize($data['FIELDS_VALUES']);
    }

    /**
     * Gauna visų teisės aktų punkų informaciją
     * @author Justinas Malūkas
     * @return call
     */
    public function get_acts()
    {
        return $this->get_act(null);
    }

    // TODO: jei $limit tekstas, tai išvesti tą tekstą
    /**
     * Gauna įrašus iš duomenų bazės
     * @param string $table Duomenų bazės lentelė
     * @param array $params [optional] Parametrų masyvas duomenų išrinkimui. <b>Default: NULL</b>
     * @param boolean $limit [optional] <b>TRUE</b> - gauti vieną įrašą, <b>FALSE</b> gauti visus. <b>Default: FALSE</b>
     * @param string $order [optional] ORDER BY sąlyga. <b>Default: NULL</b>
     * @param boolean $default_validity [optional] <b>TRUE</b> - gauti įrašą su VALID laukų reikšme '1', kitaip <b>FALSE</b>. <b>Default: TRUE</b>
     * @param string $fields [optional] Duomenų bazės laukai, kuriuos surinkti. <b>Default: NULL</b>
     * @param array $joins [optional] JOIN'ų masyvas. Pagal indeksą tikrina ar $fields yra reikalaujamas laukas ir, jei reikia, prie SQL'o prideda join'ą. <b>Default: array()</b>
     * @param bool $id_to_index [optional] Ar ID dėti į indeksą, jei jis yra užklausos rezultate. <b>Default: TRUE</b>
     * @return array
     */
    public function get_db($table, $params = null, $limit = FALSE, $order = null, $default_validity = true, $fields = null, $joins = array(), $id_to_index = true)
    {
        $where = array();
        $arg = array();

        if (empty($params) === TRUE)
            $params = array();
        foreach ($params as $key => $value) {
            if (is_int($key) === TRUE) {
                $where[] = ':' . $key;
                $key = '#' . $key . '#';
            } else {
                $key = trim($key, '#');

                // Jei indekse nurodyta lentelė
                if (strpos($key, '.') > 0) {
                    $column = str_replace('`', '', $key);
                    $column = preg_replace('/([^.]*)\.(.*)/i', '`$1`.`$2`', $column);
                    $key = str_replace('.', '__', $key);
                } else {
                    $column = '`' . $key . '`';
                }

                $where[] = $column . ' = :' . $key;
            }
            $arg[$key] = $value;

            if ($key === 'ID') {
                $limit = true;
            }
            if ($key === 'VALID') {
                $default_validity = false;
            }
        }

        if ($default_validity === TRUE)
            $where[] = '`VALID` = 1';

        $where = count($where) ? ' WHERE ' . join(' AND ', $where) : '';

        $aliases = array();
        if (preg_match_all('/`?([A-Z0-9_]+)`?\.`?[^\s,]+`?/', $fields, $matches)) {
            if (empty($matches[1]) === FALSE) {
                $aliases = array_unique($matches[1]);
            }
        }

        $exclude = array('A');
        $joins_temp = array();
        $joins = $this->get_db_joins($aliases, $joins, $exclude, $joins_temp);
        $joins = join(PHP_EOL, $joins);

        $qry = 'SELECT ' . (!$fields ? '*' : $fields)
            . ' FROM `' . $table . '` A '
            . $joins
            . $where
            . ($order ? ' ORDER BY ' . $order : '')
            . ($limit ? ' LIMIT 1' : '');
        $asd = false;
        if ($table === PREFIX . '') {
            //$asd = true;
        }
        $result = $this->core->db_query_fast($qry, $arg, $asd);
        if ($asd) {
            die;
        }
        $ret = array();
        while ($row = $this->core->db_next($result)) {
            if ($limit === TRUE)
                return $row;

            if (empty($row['ID']) || $id_to_index === FALSE)
                $ret[] = $row;
            else {
                $ret[$row['ID']] = $row;
            }
        }

        return $ret;
    }

    /**
     * Automatiškai surenka ir išrikiuoja JOIN sakinius
     * @param array $aliases Lentelių alias'ai, pagal kuriuos išrenkami JOIN sakiniai
     * @param array $joins [optional] Visi JOIN sakiniai
     * @param array $excludes [optional] Lentelių alias'ai, pagal kuriuos atmetami JOIN sakiniai
     * @param array $joins_tmp [optional] Surinkti JOIN sakiniai
     * @return array JOIN sakinių masyvas
     */
    private function get_db_joins($aliases, Array $joins = array(), Array &$excludes = array(), Array &$joins_tmp = array())
    {
        foreach ($aliases as $aliase) {
            if (in_array($aliase, $excludes))
                continue;

            $excludes[] = $aliase;

            if (isset($joins[$aliase])) {
                if (preg_match_all('/`?([A-Z0-9_]+)`?\.`?[^\s,]+`?/', $joins[$aliase], $matches)) {
                    if (empty($matches[1]) === FALSE) {
                        $aliases_tmp = array_unique($matches[1]);
                    }
                }
                if (empty($aliases_tmp) === FALSE) {
                    $this->get_db_joins($aliases_tmp, $joins, $excludes, $joins_tmp);
                }
                $joins_tmp[] = $joins[$aliase];
            }
        }
        return $joins_tmp;
    }

    /**
     * Gauna teisės aktų punkų informaciją
     * @author Justinas Malūkas
     * @param array $params [optional] Parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @return array Teisės aktų punkų informacija
     */
    public function get_act($params = null, $limit = FALSE, $order = null)
    {
        return $this->get_db(PREFIX . 'ATP_ACTS', $params, $limit, $order, true, null);
    }

    /**
     * Galimi straipsnio laukai
     * @author Justinas Malūkas
     * @return array
     */
    public function get_clause_default_fields_structure()
    {
        $default = array(
            'ID' => null, 'ITEM_ID' => '', 'ITYPE' => 'CLAUSE',
            'LABEL' => null, 'NAME' => null, 'SORT' => null, 'TYPE' => null,
            'SOURCE' => '0', /*'VALUE' => '0', 'VALUE_TYPE' => '0',
            'OTHER_SOURCE' => '0', 'OTHER_VALUE' => '0', 'FILL' => '0',*/
            'IN_LIST' => '0', 'RELATION' => '0', 'MANDATORY' => '0',
            'VISIBLE' => '1', 'COMPETENCE' => '1', 'ORD' => '1',
            'TEXT' => '', 'DEFAULT' => '', 'DEFAULT_ID' => '',
            'UPDATE_DATE' => '2014-01-01 00:00:00', 'CREATE_DATE' => '2014-01-01 00:00:00'
        );
        $custom = array(
            array(
                'ID' => 5, 'LABEL' => 'ATPK sankcijos tekstas', 'NAME' => 'SCOL_1',
                'SORT' => '2', 'TYPE' => '8', 'ORD' => '1')
        );

        foreach ($custom as &$arr) {
            $arr = array_merge($default, $arr);
        }
        return $custom;
    }

    /**
     * Gauna straipsnio laukų reikšmes
     * @author Justinas Malūkas
     * @param int $id Straipsnio ID
     * @param array $data [optional] Straipsnio informacija
     * @return array Straipsnio laukų reikšmės
     */
    public function get_clause_default_fields_values($id, &$data = array())
    {
        if (empty($id) === TRUE) {
            return null;
        }
        if (empty($data) === TRUE) {
            $data = $this->get_clause(array('ID' => $id));
        }
        if (empty($data['FIELDS_VALUES']) === TRUE) {
            return null;
        }
        return unserialize($data['FIELDS_VALUES']);
    }

    /**
     * Gauna straipsnių informaciją
     * @author Justinas Malūkas
     * @param array $params Parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>'CLAUSE, SECTION, PARAGRAPH, SUBPARAGRAPH'</b>
     * @return array Straipsnių informacija
     */
    public function get_clause($params = null, $limit = FALSE, $order = 'CLAUSE, SECTION, PARAGRAPH, SUBPARAGRAPH')
    {
        return $this->get_db(PREFIX . 'ATP_CLAUSES', $params, $limit, $order, true, null);
    }

    /**
     * Gauna klasifikatorių informaciją
     * @author Justinas Malūkas
     * @param array $params Parametrų masyvas
     * @param boolean $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @param string $fields [optional] Duomenų bazės laukai, kuriuos surinkti. Default: <b>null</b>
     * @param boolean $validity [optional] Default: <b>true</b>
     * @return array Klasifikatorių informacija
     */
    public function get_classificator($params = null, $limit = FALSE, $order = null, $fields = null, $validity = true)
    {
        return $this->get_db(PREFIX . 'ATP_CLASSIFICATORS', $params, $limit, $order, $validity, $fields);
    }

    /**
     * Gauna dokumentų informaciją
     * @author Justinas Malūkas
     * @param int|array $params Dokumento ID arba parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @param string $fields [optional] Duomenų bazės laukai, kuriuos surinkti. Default: <b>null</b>
     * @return array Dokumentų informacija
     */
    public function get_document($params = null, $limit = FALSE, $order = null, $fields = null)
    {
        if ($fields === NULL) {
            $fields = '*';
        }
        if (is_array($params) === FALSE && ctype_digit((string) $params) === TRUE)
            $params = array('ID' => $params);

        return $this->get_db(PREFIX . 'ATP_DOCUMENTS', $params, $limit, $order, true, $fields, $joins = array());
    }

    /**
     * Gauna apmokėtų mokėjimų informaciją
     * @author Justinas Malūkas
     * @param array $params Parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @return array Dokumentų informacija
     */
    public function get_payments($params = null, $limit = FALSE, $order = null)
    {
        return $this->get_db(PREFIX . 'ATP_RECORD_PAYMENTS', $params, $limit, $order, false, null);
    }

    /**
     * Gauna paslaugos informaciją iš Paslaugų posistemės
     * @author Justinas Malūkas
     * @param array $params Parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @param string $fields [optional] Duomenų bazės laukai, kuriuos surinkti. Default: <b>null</b>
     * @return array Dokumentų informacija
     */
    public function get_external_service($params = null, $limit = FALSE, $order = null, $fields = null)
    {
        return $this->get_db(PREFIX . 'PASLAUGOS', $params, $limit, $order, false, $fields);
    }

    /**
     * Sukuria web-serviso objektą
     * @param string $name Web-serviso pavadinimas. Default: <b>null</b>
     * @return \atp_ws
     */
    public function get_ws($name = null)
    {
        require_once($this->core->config['GLOBAL_REAL_URL'] . 'subsystems/atp/class/Document/WebService/ws.php');
        return new \atp_ws($this->core, $name);
    }

    /**
     *
     * @author Justinas Malūkas
     * @param array $params Parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @return array
     */
    public function get_webservices_fields($params = null, $limit = FALSE, $order = null)
    {
        return $this->get_db(PREFIX . 'ATP_WEBSERVICES_FIELDS', $params, $limit, $order, false, null);
    }

    /**
     *
     * @author Justinas Malūkas
     * @param array $params Parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @return array
     */
    public function get_webservices($params = null, $limit = FALSE, $order = null)
    {
        return $this->get_db(PREFIX . 'ATP_WEBSERVICES', $params, $limit, $order, false, null);
    }

    /**
     * Gauna paieškos lauko informaciją
     * @author Justinas Malūkas
     * @param array $params [optional] Parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @param string $fields [optional] Duomenų bazės laukai, kuriuos surinkti. Default: <b>null</b>
     * @return array Paieškos lauko informacija
     */
    public function get_search_field($params = null, $limit = FALSE, $order = null, $fields = null)
    {
        return $this->get_db(PREFIX . 'ATP_SEARCH_FIELDS', $params, $limit, $order, false, $fields);
    }

    /**
     * Gauna laukus, kurie yra pririšti prie paieškos lauko
     * @param array $params [optional] Parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @param string $fields [optional] Duomenų bazės laukai, kuriuos surinkti. Default: <b>null</b>
     * @return array Laukai pririšti prie paieškos lauko
     */
    public function get_search_field_form_fields($params = null, $limit = FALSE, $order = null, $fields = null)
    {
        return $this->get_db(PREFIX . 'ATP_SEARCH_FIELDS_X_FIELDS', $params, $limit, $order, false, $fields);
    }

    /**
     * Gauna informacinius laukus, kurie yra pririšti prie paieškos lauko
     * @param array $params [optional] Parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @param string $fields [optional] Duomenų bazės laukai, kuriuos surinkti. Default: <b>null</b>
     * @return array Informaciniai laukai pririšti prie paieškos lauko
     */
    public function get_search_field_form_ifields($params = null, $limit = FALSE, $order = null, $fields = null)
    {
        return $this->get_db(PREFIX . 'ATP_SEARCH_FIELDS_X_IFIELDS', $params, $limit, $order, false, $fields);
    }

    /**
     * Gauna dokumento įrašo failų informaciją
     * @param array $params [optional] Parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @param string $fields [optional] Duomenų bazės laukai, kuriuos surinkti. Default: <b>null</b>
     * @return array Laukai pririšti prie paieškos lauko
     */
    public function get_record_files($params = null, $limit = FALSE, $order = null, $fields = null)
    {
        return $this->get_db(PREFIX . 'ATP_RECORD_FILES', $params, $limit, $order, false, $fields);
    }

    public function get_files($params = null, $limit = FALSE, $order = null, $fields = null)
    {
        return $this->get_db(PREFIX . 'ATP_FILES', $params, $limit, $order, false, $fields);
    }

    public function get_reports($params = null, $limit = FALSE, $order = null, $fields = null)
    {
        return $this->get_db(PREFIX . 'ATP_USER_REPORTS', $params, $limit, $order, false, $fields);
    }

    public function get_status($params = null, $limit = FALSE, $order = null, $fields = null, $validity = true)
    {
        return $this->get_db(PREFIX . 'ATP_STATUS', $params, $limit, $order, $validity, $fields);
    }

    public function get_status_relations($params = null, $limit = FALSE, $order = null, $fields = null)
    {
        return $this->get_db(PREFIX . 'ATP_STATUS_X_DOCUMENT', $params, $limit, $order, true, $fields);
    }

    public function get_field_relations($params = null, $limit = FALSE, $order = null, $fields = null)
    {
        return $this->get_db(PREFIX . 'ATP_TABLE_RELATIONS', $params, $limit, $order, false, $fields);
    }

    /**
     * Gauna WebPartner darbuotojų informaciją
     * @deprecated naudoti Atp\Record\Worker::getWorkerInfo()
     * @param array $params [optional] Parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @param string $fields [optional] Duomenų bazės laukai, kuriuos surinkti. Default: <b>null</b>
     * @return array Darbuotojų informacija
     */
    public function get_worker_info($params = null, $limit = FALSE, $order = null, $fields = null)
    {
        $worker = new \Atp\Record\Worker($this->core);

        $options = ['LIMIT' => $limit];
        if ($order !== NULL) {
            $options['ORDER'] = $order;
        }
        if ($fields !== NULL) {
            $options['SELECT'] = $fields;
        }

        return $worker->getWorkerInfo($params, $options);
    }

    /**
     * Gauna informaciją iš dokumento įrašų ir asmens ryšių
     * @deprecated naudoti Atp\Record\Worker::getRecordWorker()
     * @param array $params [optional] Parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @param string $fields [optional] Duomenų bazės laukai, kuriuos surinkti. Default: <b>null</b>
     * @return array
     */
    public function get_record_worker($params = null, $limit = FALSE, $order = null, $fields = null)
    {
        $worker = new \Atp\Record\Worker($this->core);

        $options = ['LIMIT' => $limit];
        if ($order !== NULL) {
            $options['ORDER'] = $order;
        }
        if ($fields !== NULL) {
            $options['SELECT'] = $fields;
        }

        return $worker->getRecordWorker($params, $options);
    }

    /**
     * Gauna dokumento įraše nurodytą automobilio numerį (t.y. lauko, kuris yra
     * priskirtas prie regitros webserviso lauko 'Valstybinis numeris', reikšmę)
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @return null|string
     */
    public function get_record_auto_nr($record)
    {
        $record = $this->get_record_relation($record);

        // Visi galimi įrašo dokumento laukai
        $fields = $this->get_fields_all(array('ITYPE' => 'DOC', 'ITEM_ID' => $record['TABLE_TO_ID']));
        foreach ($fields as &$field) {
            // Reikia tik tų laukų prie kurių prijungtas web-servisas
            if ((int) $field['WS'] === 0)
                continue;

            // Lauko web-serviso informacija
            $relations = $this->get_webservices_fields(array('FIELD_ID' => $field['ID']));

            foreach ($relations as &$relation) {
                // Jei regitros web-servisas ir laukas priskirtas regitros laukui, kurio ID = 1 (Valstybinis numeris)
                if ($relation['WS_NAME'] === 'RGT' && (int) $relation['WS_FIELD_ID'] === 1) {
//                    $value = $this->core->logic->get_extra_field_value($record['ID'], $field['ID']);
                    $value = $this->core->factory->Record()->getFieldValue($record['ID'], $field['ID']);
                    $value = trim($value);

                    if (empty($value))
                        continue;

                    return $value;
                }
            }
        }

        return null;
    }

    /**
     * Gauna dokumento įraše nurodito asmens adresą.
     * T.y. lauko, kuris yra priskirtas prie:
     * gyventojų registro webserviso lauko 'Adresas' - reikšmę
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @return null|string
     */
    public function get_record_person_name($record)
    {
        $record = $this->get_record_relation($record);
//        $document = $this->get_document($record['TABLE_TO_ID'], true);
        $names = array();

        $configuredField = $this->core->factory->Document()->getOptions($record['TABLE_TO_ID'])->person_code;
        if (empty($configuredField)) {
            return;
        }

        $code_field = $this->get_fields(array('ID' => $configuredField));

        if (empty($code_field)) {
            return;
        }

        $fields = $this->get_webservices(array(
            'PARENT_FIELD_ID' => $code_field['ID'],
            // Jei gyventojų registro web-servisas ir laukas priskirtas laukui, kurio ID = 3 (Vardas), ID = 4 (Pavardė)
            // arba
            // Jei sodros registro web-servisas ir laukas priskirtas laukui, kurio ID = 2 (Vardas), ID = 3 (Pavardė)
            '(WS_NAME = "GRT" AND WS_FIELD_ID IN(3,4)) OR (WS_NAME = "SDR" AND WS_FIELD_ID IN(2, 3))'
        ));
        foreach ($fields as &$field) {
//            $value = $this->core->logic->get_extra_field_value($record['ID'], $field['FIELD_ID']);
            $value = $this->core->factory->Record()->getFieldValue($record['ID'], $field['FIELD_ID']);
            $value = trim($value);

            if (empty($value))
                continue;

            $isName = $field['WS_NAME'] === 'GRT' && (int) $field['WS_FIELD_ID'] === 3 || $field['WS_NAME'] === 'SDR' && (int) $field['WS_FIELD_ID'] === 2;
            if ($isName) {
                $names[$field['WS_NAME']][0] = $value;
            } else {
                $names[$field['WS_NAME']][1] = $value;
            }
        }

        if (count($names)) {
            foreach ($names as $arr) {
                if (empty($arr[0]) || empty($arr[1]))
                    continue;

                return join(' ', $arr);
            }
        } else {
            $record_table = $this->get_record_table($record['ID'], 'C.' . $code_field['NAME'] . ' CODE');

            $ws = $this->get_ws('grt');
            $ws->get_fields();
            $ws->set_parameters(array('POST' => array('SEARCH_CODE' => $record_table['CODE'])));
            $ws->initialize();
            $response = $ws->get_result();
            if (empty($response) === FALSE)
                return join(' ', array($response['VARDAS'], $response['PAVARDE']));
        }

        return null;
    }

    /**
     * Gauna dokumento įraše nurodito asmens adresą (t.y. lauko, kuris yra
     * priskirtas prie gyventojų registro webserviso lauko 'Adresas', reikšmę)
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @return null|string
     */
    public function get_record_person_address($record)
    {
        $record = $this->get_record_relation($record);
//        $document = $this->get_document($record['TABLE_TO_ID'], true);
        $adresses = array();

        $configuredField = $this->core->factory->Document()->getOptions($record['TABLE_TO_ID'])->person_code;
        if (empty($configuredField)) {
            return;
        }

        $code_field = $this->get_fields(array('ID' => $configuredField));

        if (empty($code_field)) {
            return;
        }

        $fields = $this->get_webservices(array(
            'PARENT_FIELD_ID' => $code_field['ID'],
            // Jei gyventojų registro web-servisas ir laukas priskirtas laukui, kurio ID = 8, (Adresas)
            'WS_NAME = "GRT" AND WS_FIELD_ID = 8'
        ));
        foreach ($fields as &$field) {
//            $value = $this->core->logic->get_extra_field_value($record['ID'], $field['FIELD_ID']);
            $value = $this->core->factory->Record()->getFieldValue($record['ID'], $field['FIELD_ID']);
            $value = trim($value);

            if (empty($value))
                continue;

            $adresses[] = $value;
            break;
        }

        if (count($adresses)) {
            return $adresses[0];
        } else {
            $record_table = $this->get_record_table(
                $record['ID'], 'C.' . $code_field['NAME'] . ' CODE'
            );

            $ws = $this->get_ws('grt');
            $ws->get_fields();
            $ws->set_parameters(array('POST' => array('SEARCH_CODE' => $record_table['CODE'])));
            $ws->initialize();
            $response = $ws->get_result();
            if (empty($response) === FALSE)
                return trim($response['ADRESAS']);
        }

        return null;
    }

    /**
     * Gauna dokumento įraše nurodytą pažeidimo datą
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @return null|string
     */
    public function get_record_violation_date($record)
    {
        $record = $this->get_record_relation($record);

        // Pažeidimo datos laukas
//        $document = $this->get_document(array('A.ID = ' . $record['TABLE_TO_ID']), true, null, 'A.ID, A.VIOLATION_DATE FIELD');
        $configuredField = $this->core->factory->Document()->getOptions($record['TABLE_TO_ID'])->violation_date;

        if (empty($configuredField) === FALSE) {
//            $field = $this->get_fields(array('ID' => $document['FIELD']));
//
//            if ($field['ITYPE'] === 'DOC') {
//                $record_table = $this->get_record_table($record['ID']);
//                $row = $this->get_db(PREFIX . 'ATP_TBL_' . (int) $record['TABLE_TO_ID'], array(
//                    'ID' => $record_table['ID']), true, null, false, 'A.ID, A.' . $field['NAME'] . ' as VALUE');
//                $value = $row['VALUE'];
//            } else {
//                $value = $this->core->logic->get_extra_field_value($record['ID'], $field['ID']);
//            }
            $value = $this->core->factory->Record()->getFieldValue($record['ID'], $configuredField);

            if (empty($value) === FALSE) {
                $timestamp = strtotime($value);
                if ($timestamp > 0)
                    return date('Y-m-d', $timestamp);
            }
        }

        return null;
    }

    /**
     * Gauna dokumento įrašo ryšių informaciją
     * @param array $params [optional] Parametrų masyvas
     * @param boolen $limit [optional] Ar tik vienas rezultatas. Default: <b>FALSE</b>
     * @param string $order [optional] SQL query order string. Default: <b>null</b>
     * @param string $fields [optional] Duomenų bazės laukai, kuriuos surinkti. Default: <b>null</b>
     * @return array
     */
    public function get_record_relations($params = null, $limit = FALSE, $order = null, $fields = null)
    {
        $joins = array();
        if ($fields === NULL) {
            $fields = '*';
        }
        return $this->get_db(
                PREFIX . 'ATP_RECORD', $params, $limit, $order, false, $fields, $joins
        );
    }

    /**
     * Gauna dokumento įrašo ryšių informaciją
     * @param int|string|array $record ID iš `ATP_RECORD` / Dokumento įrašo numeris / `ATP_RECORD` duomenų eilutė
     * @return boolean|array
     */
    public function get_record_relation($param)
    {
        if (is_array($param) === TRUE) {
            if (isset($param['ID']))
                $arg = array('ID' => (int) $param['ID']);
        } elseif (ctype_digit((string) $param) === TRUE) {
            $arg = array('ID' => (int) $param);
        } elseif (is_string($param) === TRUE) {
            $arg = array('RECORD_ID' => $param, 'IS_LAST' => 1);
        } else {
            return false;
        }

        /*
          if (ctype_digit((string) $param) === TRUE) {
          $arg = array('ID' => (int) $param);
          } elseif (is_string($param) === TRUE) {
          $arg = array('RECORD_ID' => $param, 'IS_LAST' => 1);
          } elseif (is_array($param) === TRUE) {
          if (isset($param['ID']))
          $arg = array('ID' => (int) $param['ID']);
          //return $param;
          } else {
          return false;
          }
         */

        return $this->get_record_relations($arg, true);
    }

    /**
     * Gauna dokumento įrašo informaciją iš ATP_TBL_<i>{document_id}</i> lentelės
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param string $fields [optional] Default: 'C.*'
     * @return boolean
     */
    public function get_record_table($record, $fields = 'C.*')
    {
        $record = $this->get_record_relation($record);
        if (empty($record) === TRUE) {
            trigger_error('Record not found' . E_USER_ERROR);
            return false;
        }

        $joins = array(
            'B' => 'INNER JOIN `' . PREFIX . 'ATP_RECORD_X_RELATION` B ON B.RELATION_ID = A.ID',
            'C' => 'INNER JOIN `' . PREFIX . 'ATP_TBL_' . (int) $record['TABLE_TO_ID'] . '` C ON C.ID = B.RECORD_ID ',
            'D' => 'INNER JOIN `' . PREFIX . 'ATP_RECORD_X_WORKER` D ON D.RECORD_ID = A.ID',
            'E' => 'INNER JOIN `' . PREFIX . 'MAIN_WORKERS` E ON E.ID = D.WORKER_ID',
            'F' => 'INNER JOIN `' . PREFIX . 'MAIN_PEOPLE` F ON F.ID = E.PEOPLE_ID'
        );
        $params = array(
            'A.ID = ' . (int) $record['ID']
        );

        return $this->get_db(PREFIX . 'ATP_RECORD', $params, $limit = true, $order = null, false, $fields, $joins);
    }

    /**
     * Dokumento įrašo veika
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @return call
     */
    public function get_record_deed($record)
    {
        return $this->get_record_deed_information($record, 'DEED');
    }

    /**
     * Dokumento įrašo teisės aktas (teisės akto punktas)
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param array $data [optional] <p>Iš anksto numatyti duomenys</p>
     * @param array $data['DEED'] Veikos ID
     * @param array $data['DATE'] Pažeidimo data
     * @return call
     */
    public function get_record_act($record, $data = array())
    {
        return $this->get_record_deed_information($record, 'ACT', $data);
    }

    /**
     * Dokumento įrašo straipsnis
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param array $data [optional] <p>Iš anksto numatyti duomenys</p>
     * @param array $data['DEED'] Veikos ID
     * @param array $data['DATE'] Pažeidimo data
     * @return call
     */
    public function get_record_clause($record, $data = array())
    {
        return $this->get_record_deed_information($record, 'CLAUSE', $data);
    }

    /**
     * Dokumento įrašo veikos informacija
     * @param mixed $record Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param string $type <i>'DEED'</i>, <i>'ACT'</i> arba <i>'CLAUSE'</i>
     * @param array $data [optional] <p>Iš anksto numatyti duomenys</p>
     * @param array $data['DEED'] Veikos ID
     * @param array $data['DATE'] Pažeidimo data
     * @return boolean|array
     */
    private function get_record_deed_information($record, $type, $data = array())
    {
        $record = $this->get_record_relation($record);
        if (empty($record) === TRUE) {
            trigger_error('Record not found' . E_USER_ERROR);
            return false;
        }


        $document = $this->get_document($record['TABLE_TO_ID']);
        $configuredField = $this->core->factory->Document()->getOptions($record['TABLE_TO_ID'])->violation_date;
        // Dokumento data
        if (empty($data['DATE']) && empty($configuredField) === FALSE) {
            $date = $this->core->factory->Record()->getFieldValue($record['ID'], $configuredField);
        } else {
            $date = $data['DATE'];
        }
        if (empty($date)) {
            $date = date('Y-m-d H:i:s');
        }

        // Veikos informacija
        if (empty($data['DEED'])) {
            $deed_field = $this->get_fields(array('ITEM_ID' => $record['TABLE_TO_ID'], 'ITYPE' => 'DOC',
                'SORT' => '4', 'TYPE' => '23'), true);
//            $deed = $this->get_record_table($record, 'C.`ID`, C.`' . $deed_field['NAME'] . '` `VALUE`');
//            $deed = $deed['VALUE'];
            $deed = $this->core->factory->Record()->getFieldValue($record['ID'], $deed_field['ID']);
        } else {
            $deed = $data['DEED'];
        }
        $deed = $this->core->logic->get_deed(array('ID' => $deed));

        if ($type === 'DEED') {
            return $deed;
        }

        // Veiką praplečia
        $ddata = $this->core->logic->get_deed_extend_data(array('DEED_ID' => $deed['ID']));

        // Surenka veikos praplečiančių elementų ID
        $act_ids = array();
        $clause_ids = array();
        foreach ($ddata as $arr) {
            if ($arr['ITYPE'] === 'ACT')
                $act_ids[$arr['ID']] = $arr['ITEM_ID'];
            elseif ($arr['ITYPE'] === 'CLAUSE')
                $clause_ids[$arr['ID']] = $arr['ITEM_ID'];
        }

        // Teisės akto laukai
        if ($type === 'ACT') {
            if (count($act_ids)) {
                $data = $this->get_act(array(
                    0 => '`ID` IN(' . join(',', $act_ids) . ')',
                    1 => '"' . $date . '"' . ' > `VALID_FROM` AND "' . $date . '" <= `VALID_TILL`'
                ));
                if (count($data)) {
                    foreach ($data as &$arr) {
                        $act = $arr['ID'];
                        break; // only one
                    }
                }
            }
            if (empty($act) === FALSE)
                return $this->get_act(array('ID' => $act));
        } else
        // Straipsnio laukai
        if ($type === 'CLAUSE') {
            if (count($clause_ids)) {
                $data = $this->get_clause(array(
                    0 => '`ID` IN(' . join(',', $clause_ids) . ')',
                    1 => '"' . $date . '"' . ' > `VALID_FROM` AND "' . $date . '" <= `VALID_TILL`'
                ));
                if (count($data)) {
                    foreach ($data as &$arr) {
                        $clause = $arr['ID'];
                        break; // only one
                    }
                }
            }
            if (empty($clause) === FALSE)
                return $this->get_clause(array('ID' => $clause));
        }

        return;
    }

    /**
     * Tikrina ar <i>$date</i> atitinka bent vieną <i>$format</i> nurodytą datos formatą
     * @param string $date Data
     * @param null|string|array $format [optional]<p>Datos formatas arba formatų masyvas. <b>Default:</b> 'Y-m-d H:i:s'</p>
     * @return boolen
     */
    public function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $valid = false;

        $new_php = false;
        if (version_compare(phpversion(), '5.3') >= 0)
            $new_php = true;

        foreach ((array) $format as $form) {
            if ($new_php) {
                $d = \DateTime::createFromFormat($form, $date);
            } else {
                // TODO: palaiko tik paprastus formatus, be PHP 5.3+ tikamai neprasisuksi
                $time = strtotime($date);
                $ugly_fix = date('Y-m-d H:i:s', $time);
                $d = new \DateTime($ugly_fix);
            }
            $valid = $d && $d->format($form) == $date;
            if ($valid === TRUE)
                break;
        }

        return $valid;
    }

    /**
     * Gauna dokumento įrašų numerius, kuriuose rastas baustumas
     * @deprecated Nebenaudojamas. Naudoti \Atp\Document\Record\Punishment:getRecordsByCode()
     * @param int $person_code Juridinio / Fizinio asmens kodas
     * @param string|array $exclude_records [optional]<p>Dokumento įrašų numeriai, kuriuos rezultate praleisti</p>
     * @return null|array RECORD_RELATIONS.ID masyvas
     */
    public function get_punishment_records($person_code, $exclude_records = array())
    {
        if (preg_match('/^[0-9]+$/', $person_code) === 0)
            return;

        $data = array(
            'CODE' => $person_code,
            'EXCLUDES' => (array) $exclude_records,
            'SOURCE' => array(// Dokumentai ir būsenos, kuriose nustatomas baustumas
                // Protokolas
                \Atp\Document::DOCUMENT_PROTOCOL => array(
                    //17, // AN įvykdytas
                    //35, // Baigtas
                    18 // AN neįvykdytas
                ),
                // Byla
                \Atp\Document::DOCUMENT_CASE_OLD => array(
                    29, // Priimtas nutarimas
                    32, // Tikrinti sumokėjimą
                    33, // Sumokėta
                    37, // Byla apskųsta
                    30, // Priimti sprendimą del nutarimo
                    31, // Priimtas nutarimas išnagrinėtoje byloje
                    23, // Byla išsiųsta teismui
                    24, // Parengtas atsiliepimas į skundą
                    25, // Parengtas apeliacinis skundas
                    26, // Parengtas atsiliepimas į apeliacinį skundą
                    27, // Išieškojo antstolis
                    28 // Byla baigta (kitu pagrindu)
                ),
                // Nutarimas
                \Atp\Document::DOCUMENT_CASE => array(
                    29 // Priimtas nutarimas
                )
            )
        );

        // Pežeidėjo laukai
        $code_fields = $this->get_document(array('A.ID IN (' . join(',', array_keys($data['SOURCE'])) . ')'), false, null, 'A.ID');

        $joins = array(
            'B' => 'INNER JOIN ' . PREFIX . 'ATP_RECORD B ON B.RECORD_ID = A.RECORD_ID AND B.IS_LAST = 1'
        );

        $result = array();
        foreach ($code_fields as $document_id => &$field) {

            $configuredField = $this->core->factory->Document()->getOptions($document_id)->person_code;
            
            $field = $this->get_fields(array('ID' => $configuredField), true, null, 'ID, NAME');

            if (empty($field) === FALSE) {
                $param = array(
                    '`' . $field['NAME'] . '` = ' . (int) $data['CODE'],
                    'RECORD_LAST' => 1);
                $records = $this->get_db(PREFIX . 'ATP_TBL_' . (int) $document_id, $param, false, null, false, 'A.RECORD_ID, B.ID, B.DOC_STATUS', $joins);

                foreach ((array) $records as $key => $record) {
                    if (in_array($record['RECORD_ID'], $data['EXCLUDES']) === TRUE || in_array($record['DOC_STATUS'], $data['SOURCE'][$document_id]) === FALSE) {
                        unset($records[$key]);
                        continue;
                    }

                    $result[] = $record['ID'];
                }
            }
        }

        return $result;
    }

    /**
     * Ar dokumento įrašas importuotas iš VMSA
     * @param int $record_relation_id
     * @return boolean
     */
    function is_VMSA_record($record_relation_id)
    {
        $row = $this->core->db_next($this->core->db_query_fast('SELECT COUNT(ID) CNT FROM ' . PREFIX . 'ATP_RECORD_X_VMSA WHERE RECORD_ID = ' . (int) $record_relation_id));
        if ((int) $row['CNT'])
            return true;

        return false;
    }
}
