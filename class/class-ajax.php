<?php

namespace Atp;

class Ajax
{
    /**
     * @var \Atp\Core
     */
    private $core;

    /**
     *
     * @param \Atp\Core $core
     */
    public function __construct(\Atp\Core $core)
    {
        $this->core = $core;
    }
//    /**
//     * @deprecated nenaudojamas
//     * @return boolean/string
//     */
//    public function getFieldSelectParents()
//    {
//        trigger_error('getFieldSelectParents() is deprecated', E_USER_DEPRECATED);
//        $field_id = (int) $_POST['field_type'];
//        $field_relations = $this->core->logic->get_field_relation_list($field_id);
//
//        if ($field_relations) {
//            return json_encode($field_relations);
//        }
//
//        return false;
//    }
//    /**
//     * @deprecated nenaudojamas
//     * @return boolean/string
//     */
//    public function getFieldSelectChildren()
//    {
//        trigger_error('getFieldSelectChildren() is deprecated', E_USER_DEPRECATED);
//        $field_parent = (int) $_POST['field_parent'];
//        $field_childrens = $this->core->logic->get_field_select($field_parent);
//
//        if ($field_childrens) {
//            return json_encode($field_childrens);
//        }
//
//        return false;
//    }

    /**
     * Gražina dokumento laukų informaciją
     * @author Martynas Boika
     * @return string|boolean JSON object arba FALSE
     */
    public function get_table_fields()
    {
        $id = (int) $_REQUEST['id'];
        if (empty($id) === false) {
            $structure = $this->core->logic->get_table_fields($id);

            foreach ($structure as $key => $field) {
                $structure[$key]['SOURCE_LABEL'] = null;
                if (empty($field['SOURCE']) === false) {
                    $classificator = $this->core->logic2->get_classificator(['ID' => $field['SOURCE']]);
                    $structure[$key]['SOURCE_LABEL'] = $classificator['LABEL'];
                }
                // Klasifikatorius
                $structure[$key]['DEFAULT_LABEL'] = null;
                if (empty($field['DEFAULT_ID']) === false && $field['SORT'] == 4 && $field['TYPE'] == 18) {
                    $classificator = $this->core->logic2->get_classificator(['ID' => $field['DEFAULT_ID']]);
                    $structure[$key]['DEFAULT_LABEL'] = $classificator['LABEL'];
                }
            }
            if (empty($structure) === false) {
                return json_encode($structure);
            }
        }

        return false;
    }

    /**
     * Gražina galimų dokumento laukų tipų informaciją
     * @author Martynas Boika
     * @return string|boolean JSON objektas arba FALSE
     * @deprecated nenaudojamas
     */
    public function get_field_type()
    {
        trigger_error('get_field_type() is deprecated', E_USER_DEPRECATED);
        $field_types = $this->core->logic->get_field_type();
        if (!empty($field_types)) {
            return json_encode($field_types);
        }

        return false;
    }

    /**
     * @deprecated Nenaudojamas
     * @return boolean
     */
    public function get_field_types()
    {
        trigger_error('get_field_types() is deprecated', E_USER_DEPRECATED);
        $field_types = $this->core->logic->get_field_types();

        if ($field_types) {
            return json_encode($field_types);
        }

        return false;
    }

//    /**
//     * @deprecated nenaudojamas
//     * @return boolean/string
//     */
//    public function get_field_select()
//    {
//        trigger_error('get_field_select() is deprecated', E_USER_DEPRECATED);
//        $field_select = $this->core->logic->get_field_select();
//
//        if ($field_select) {
//            return json_encode($field_select);
//        }
//
//        return false;
//    }

    public function get_table_parent()
    {
        $table_id = (int) $_POST['table_id'];
        if (!empty($table_id)) {
            $table_parent = $this->core->logic->get_table_parent($table_id);

            if ($table_parent) {
                return json_encode($table_parent);
            }
        }

        return false;
    }
//    public function get_table_params_old()
//    {
//        $table_id = (int) $_POST['table_id'];
//        $table = $this->core->logic2->get_document($table_id);
//
//        //$options = $this->core->manage->document_fields($table_id, 'DOC', array(
//        //	'multiplier' => 3,
//        //	'prefix' => $this->core->manage->parse_document_template_variable_prefix_from_string($table['LABEL']),
//        //	'options' => true
//        //	), $tmpl);
//        //if(empty($options) === FALSE) {
//        //	$options = join(PHP_EOL, $options);
//        //} else
//        //	$options = '';
//        /*
//          if(empty($table['SERVICE_ID']) === FALSE) {
//          $service = $this->core->logic2->get_external_service(array('ID' => $table['SERVICE_ID']), true, false, 'ID, SHOWS');
//          $service = array('ID' => $service['ID'], 'NAME' => $service['SHOWS']);
//          } */
//
//        if (!empty($table)) {
//            $all_fields = array();
//            $select_hierarchy = $this->core->manage->tables_relations_parent_fields(
//                $table_id, 'DOC', $data = array(), $all_fields, $null = null
//            );
//
//            $data = array();
//            $select_hierarchy = array_reverse($select_hierarchy, true);
//            $select_hierarchy[0]['OPT'] = '-- nepasirinkta --';
//            $data['select_hierarchy'] = array_reverse($select_hierarchy, true);
//
//            $all_fields[0] = array('ID' => 0, 'LABEL' => '-- nepasirinkta --');
//            $data['all_fields'] = &$all_fields;
//
//            $this->core->manage->printTree(
//                $data['select_hierarchy'], 0, $data['all_fields'], $valid_fields = false, $selected = null, $opts
//            );
//
//            $return = $table;
//
//            $documentManager = $this->core->factory->Document();
//            $return['TERMS'] = $documentManager->GetTerms(array('DOCUMENT_ID' => $table_id));
//            $return['OPTIONS'] = $opts;
//            return json_encode($return);
//
////            return json_encode(array(
////                'PARENT_ID' => $table['PARENT_ID'],
////                'CAN_EDIT' => $table['CAN_EDIT'],
////                'CAN_END' => $table['CAN_END'],
////                'CAN_NEXT' => $table['CAN_NEXT'],
////                'CAN_IDLE' => $table['CAN_IDLE'],
////                'IDLE_DAYS' => $table['IDLE_DAYS'],
////                'CAN_TERM' => $table['CAN_TERM'],
////                'TERM_DAYS' => $table['TERM_DAYS'],
////                'VIOLATION_DATE' => $table['VIOLATION_DATE'],
////                'PERSON_CODE' => $table['PERSON_CODE'],
////                'PERSON_ADDRESS' => $table['PERSON_ADDRESS'],
////                'FILL_DATE' => $table['FILL_DATE'],
////                'AVILYS_DATE' => $table['AVILYS_DATE'],
////                'AVILYS_NUMBER' => $table['AVILYS_NUMBER'],
////                'PENALTY_SUM' => $table['PENALTY_SUM'],
////                'CLASS_TYPE' => $table['CLASS_TYPE'],
////                'EXAMINATOR' => $table['EXAMINATOR'],
////                'EXAMINATOR_CABINET' => $table['EXAMINATOR_CABINET'],
////                'EXAMINATOR_PHONE' => $table['EXAMINATOR_PHONE'],
////                'EXAMINATOR_EMAIL' => $table['EXAMINATOR_EMAIL'],
////                'EXAMINATOR_INSTITUTION_NAME' => $table['EXAMINATOR_INSTITUTION_NAME'],
////                'EXAMINATOR_UNIT' => $table['EXAMINATOR_UNIT'],
////                'EXAMINATOR_SUBUNIT' => $table['EXAMINATOR_SUBUNIT'],
////                'EXAMINATOR_INSTITUTION_ADDRESS' => $table['EXAMINATOR_INSTITUTION_ADDRESS'],
////                'EXAMINATOR_DATE' => $table['EXAMINATOR_DATE'],
////                'EXAMINATOR_START_TIME' => $table['EXAMINATOR_START_TIME'],
////                'EXAMINATOR_FINISH_TIME' => $table['EXAMINATOR_FINISH_TIME'],
////                'DECISION_DATE' => $table['DECISION_DATE'],
////                'TERMS' => $documentManager->GetTerms(array('DOCUMENT_ID' => $table_id)),
////                'OPTIONS' => $opts//,
////                //'OPTIONS' => $options//,
////                //'SERVICE' => $service
////            ));
//        }
//    }

    /**
     * @param int[] $fieldsIds
     * @return array
     */
    public function getConfiguredFields($fieldsIds = array())
    {
        $result = [];
        /* @var $repository \Atp\Repository\FieldConfigurationRepository */
        $repository = $this->core->factory->Doctrine()->getRepository('Atp\Entity\FieldConfiguration');

        foreach ($fieldsIds as $fieldId) {
            foreach ($repository->getTypesByField($fieldId) as $configurationType) {
                $result[] = [
                    'field' => $fieldId,
                    'configuration' => $configurationType->getId(),
                ];
            }
        }

        return $result;
    }

    public function get_table_params()
    {
//        $oldResult = json_decode($this->get_table_params_old(), true);
//        $oldResult['OPTIONS'] = '0';
//
//        $return = array_map(function() { return null; }, $oldResult);

        $documentManager = $this->core->factory->Document();
        $document = $documentManager->get(
            filter_input(INPUT_POST, 'table_id', FILTER_VALIDATE_INT)
        );

        if (empty($document) === false) {
            $all_fields = array();
            $select_hierarchy = $this->core->manage->tables_relations_parent_fields(
                $document->getId(), 'DOC', $data = array(), $all_fields, $null = null
            );

            $data = array();
            $select_hierarchy = array_reverse($select_hierarchy, true);
            $select_hierarchy[0]['OPT'] = '-- nepasirinkta --';
            $data['select_hierarchy'] = array_reverse($select_hierarchy, true);

            $all_fields[0] = array('ID' => 0, 'LABEL' => '-- nepasirinkta --');
            $data['all_fields'] = &$all_fields;

            $this->core->manage->printTree(
                $data['select_hierarchy'], 0, $data['all_fields'], $valid_fields = false, $selected = null, $fieldsOptions
            );


            $documentOptions = $documentManager->getOptions($document->getId());

            $fieldsIds = array_values($this->core->factory->Field($this->core)
                    ->getFieldsMapByDocument($document->getId(), 'assoc'));
            $configuredFields = $this->getConfiguredFields($fieldsIds);

            $return = [
                'document' =>
                [
                    'parent' => ($document->getParent() ? $document->getParent()->getId() : null),
                    'options' => [
                        'canEdit' => $documentOptions->can_edit,
                        'disableEdit' => $documentOptions->disable_edit,
                        'canEnd' => $documentOptions->can_end,
                        'canNext' => $documentOptions->can_next,
                        'canIdle' => $documentOptions->can_idle,
                        'idleDays' => $documentOptions->idle_days,
                        'canTerm' => $documentOptions->can_term,
                        'termDays' => $documentOptions->term_days
                    ],
                    'terms' => array_map(function($term) {
                        return [
                            'type' => (int) $term['TYPE'],
                            'field' => (int) $term['FIELD_ID'],
                            'days' => (int) $term['DAYS']
                        ];
                    }, $documentManager->GetTerms(['DOCUMENT_ID' => $document->getId()]))
                ],
                'configuredFields' => $configuredFields,
                'fieldsOptions' => $fieldsOptions
            ];

            return json_encode($return);
        }

        return false;
    }

    /**
     * Valdo straipsnių veiksmus
     * @author Justinas Malūkas
     * @return string
     */
    public function clause_action()
    {
        // Tikrina ar vartotojas turi teisę į straipsnių koregavimą.
        // Jei ne - gražiną pranešimą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_CLAUSE') < 2) {
            return json_encode(array('error' => 'Administracinės teisės pažeidimai'));
        }

        // Veiksmai
        switch ($_REQUEST['action']) {
            // Redagavimo forma
            case 'editform':
                $res = $this->core->manage->clause_editform();
                //$res = $this->core->action->clause((int) $_POST['id'], $_REQUEST['action']);
                //if($this->core->content_type === 2) {
                $ret = array(
                    'result' => (bool) $res,
                    'html' => $res);
                //} else
                //	return $res;
                break;
            // Straipsnių trinimas
            case 'delete':
                //$res = $this->core->logic->clause_delete((int) $_POST['item']);
                $res = $this->core->logic->clause_action((int) $_POST['item'], 'delete');
                $ret = array(
                    'result' => (bool) $res,
                    'ids' => $res);
                break;
            // Straipsnių laukų redagavimas
            case 'fields':
                $arr['ID'] = $_POST['item'];
                $type = 'CLAUSE';
                $res = $this->extra_fields_html($type, $arr);
                $ret = array(
                    'result' => (bool) $res,
                    'html' => $res,
                    'data' => $this->core->logic->extra_fields_info($type, $arr));
                break;
            // Straipsnių laukų saugiojimas
            case 'savefields':
                $type = 'CLAUSE';
                if (isset($_GET['item']) === true) {
                    $_POST['item'] = (int) $_GET['item'];
                }
                $res = $this->core->logic->extra_fields_save($type, $_POST);
                $ret = array(
                    'result' => (bool) $res,
                    'message' => $res);
                break;
        }

        return json_encode($ret);
    }

    function filter_by_source($sourceId, &$items)
    {
        foreach ($items as $item) {
            if ($this->filter_by_source_is_removable($sourceId, $item, $items)) {

            }
        }
    }

    function filter_by_source_is_removable($sourceId, $item, $items)
    {
        foreach ($items as $item) {
            if ($item['PARENT_ID'] === $sourceId) {

            }
        }
    }

    /**
     * Valdo klasifikatoriaus veiksmus
     * @author Justinas Malūkas
     * @return string
     */
    public function classificator_action()
    {
        // Tikrina ar vartotojas turi teisę į klasifikatorių koregavimą.
        // Jei ne - gražiną pranešimą
        if (in_array($_REQUEST['action'], ['delete', 'save', 'fields', 'fieldsinfo',
                'savefields', 'getChildrenHtml']) && $this->core->factory->User()->getLoggedPersonDutyStatus('A_CLASSIFICATION') < 2) {
            return json_encode(array('error' => 'Administracinės teisės pažeidimai'));
        }
        
        // Veiksmai
        switch ($_REQUEST['action']) {
            // Paieška
            // TODO: Horofying disastrous functionality. Need to implement nested set tree.
            case 'search':
                ini_set('memory_limit', '300M');

                $s = microtime(true);
                $sm = memory_get_usage();
                $arr = array();
                $extract = array();
                $source = null;
                foreach ($_POST as $key => $val) {
                    switch ($key) {
                        case 'name':
                            $arr['LABEL'] = $val;
                            break;
                        case 'extract':
                            $extract = $val;
                        case 'with_parents':
                            $arr['WITH_PARENTS'] = (bool) $val;
                            break;
                        case 'source':
//                            $arr['SOURCE'] = $val;
                            $source = $val;
                            break;
                    }
                }

//                $entityManager = $this->core->factory->Doctrine();
//                /* @var $repository \Atp\Repository\ClassificatorRepository */
//                $repository = $entityManager->getRepository('\Atp\Entity\Classificator');
//                //$source = 1;
//                $node = null;
//                if (empty($source) === false) {
//                    $node = $entityManager->getReference('\Atp\Entity\Classificator', $source);
//                }
//                $direct = !(empty($arr['LABEL']) || $arr['WITH_PARENTS']);
//                $queryBuilder = $repository->getChildrenQueryBuilder($node, $direct);
//                if (isset($arr['LABEL'])) {
//                    $queryBuilder->andWhere('node.label LIKE :name')
//                        ->setParameter('name', '%' . $arr['LABEL'] . '%')
//                        ->getQuery()->getResult();
//                }
//                //var_dump($queryBuilder->getQuery()->getSQL());die;
//                $res = $queryBuilder->getQuery()->getArrayResult();
//
//                if ($node) {
//                  $res = array_map(function($item) {
//                      if(is_object($item)) {
//                          return $item->getDescendant();
//                      }
//                      return $item['descendant']; }, $res);
//                }
//                var_dump($res);
//                die;


                $res = $this->core->logic->classificator_action($arr, 'search');

                if (empty($source) === false && empty($res) === false) {
                    /* @var $repository \Atp\Repository\ClassificatorRepository */
                    $repository = $this->core->factory->Doctrine()->getRepository('Atp\Entity\Classificator');
                    $children = $repository->getAllChildrenIds($source);
                    $children[] = $source;

                    foreach ($res as $key => $classificator) {
                        if (in_array((int) $classificator['ID'], $children) === false) {
                            unset($res[$key]);
                        }
                    }
                }

                // Jei neranda tėvo, tai pameta įrašą. Todėl hierarchiškai rikiuojam tik tada, kai gražinami visi rezultatai
                if (empty($res) === false && (empty($arr['LABEL']) === true || $arr['WITH_PARENTS'])) {
                    $res = $this->core->logic->sort_children_to_parent($res, 'ID', 'PARENT_ID');

                    $extract[] = 'depth';
                }

                if (empty($source) === false) {
                    foreach ($res as $key => $classificator) {
                        if ($classificator['ID'] == $source) {
                            unset($res[$key]);
                            break;
                        }
                    }
                }

                if (empty($extract) === false) {
                    array_unshift($extract, 'ID');

                    foreach ($res as $key => $classificator) {
                        $newData = [];
                        foreach ($classificator as $columnName => $value) {
                            if (in_array($columnName, $extract) === false) {
                                continue;
                            }
                            $newData[$columnName] = $value;
                        }
                        $res[$key] = $newData;
                    }
                }
                $e = microtime(true);
                $em = memory_get_usage();

                $ret = array(
                    'result' => (bool) $res,
                    'data' => $res,
                    'time' => $e - $s,
                    'memory' => $em - $sm,
                );
                break;
            // Klasifikatoriaus trinimas
            case 'delete':
                $classificatorId = filter_input(INPUT_POST, 'item', FILTER_VALIDATE_INT);

                $res = $this->core->logic->classificator_action($classificatorId, 'delete');
                $ret = array(
                    'result' => (bool) $res,
                    'ids' => $res);
                break;
            // Klasifikatoriaus išsaugojimas
            case 'save':
                $parameters = [
                    'LABEL' => filter_input(INPUT_POST, 'name'),
                    'ID' => filter_input(INPUT_POST, 'item', FILTER_VALIDATE_INT),
                    'PARENT_ID' => filter_input(INPUT_POST, 'parent', FILTER_VALIDATE_INT, ['options' => ['default' => 0]])
                ];

                if (empty($parameters['ID'])) {
                    unset($parameters['ID']);
                }

                /* @var $classificatorId false|int */
                $classificatorId = $this->core->logic->classificator_action($parameters, 'save');
                $ret = [
                    'result' => (bool) $classificatorId
                ];
                if ($classificatorId) {
                    $ret['id'] = $classificatorId;
                }
                break;
            // Klasifikatoriaus laukų redagavimas
            case 'fields':
                $arr['ID'] = $_POST['item'];
                $type = 'CLASS';
                $res = $this->extra_fields_html($type, $arr);

                $fields = $this->core->logic->extra_fields_info($type, $arr);

                foreach ($fields as $key => $field) {
                    $fields[$key]['SOURCE_LABEL'] = null;
                    if (empty($field['SOURCE']) === false) {
                        $classificator = $this->core->logic2->get_classificator(['ID' => $field['SOURCE']]);
                        $fields[$key]['SOURCE_LABEL'] = $classificator['LABEL'];
                    }
                    // Klasifikatorius
                    $fields[$key]['DEFAULT_LABEL'] = null;
                    if (empty($field['DEFAULT_ID']) === false && $field['SORT'] == 4 && $field['TYPE'] == 18) {
                        $classificator = $this->core->logic2->get_classificator(['ID' => $field['DEFAULT_ID']]);
                        $fields[$key]['DEFAULT_LABEL'] = $classificator['LABEL'];
                    }
                }

                $ret = array(
                    'result' => (bool) $res,
                    'html' => $res,
                    'data' => $fields
                );
                break;
            // Klasifikatoriaus laukų informacija
            // TODO: kolkas nereikia.
            case 'fieldsinfo':
                $arr['ID'] = $_POST['item'];
                $type = 'CLASS';
                $res = $this->core->logic->extra_fields_info($type, $arr);
                $ret = array(
                    'result' => (bool) $res,
                    'html' => $res);
                break;
            // Klasifikatoriaus laukų saugiojimas
            case 'savefields':
                $type = 'CLASS';
                if (isset($_GET['item']) === true) {
                    $_POST['item'] = (int) $_GET['item'];
                }
                $res = $this->core->logic->extra_fields_save($type, $_POST);
                $ret = array(
                    'result' => (bool) $res,
                    'message' => $res);
                break;
            case 'getChildrenHtml':
                $tableHtml = $this->core->manage->table_classifications(
                    (int) $_POST['item'], (int) $_POST['level']
                );

                $html = null;
                if (preg_match_all('/<tr(|\s+.*?)>.*?<\/tr>/s', $tableHtml, $matches)) {
                    unset($matches[0][0]);
                    $html = implode('', $matches[0]);
                }

                $ret = array(
                    'result' => (bool) $html,
                    'html' => $html
                );
                break;
        }

        return json_encode($ret);
    }

    /**
     * Dokumentų būsenų veiksmai
     * @author Justinas Malūkas
     * @return string
     */
    public function status_action()
    {
        // Tikrina ar vartotojas turi teisę į koregavimą.
        // Jei ne - gražiną pranešimą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_STATUS') < 2) {
            return json_encode(array('error' => 'Administracinės teisės pažeidimai'));
        }

        // Veiksmai
        switch ($_REQUEST['action']) {
            // Būsenos redagavimo forma
            case 'editstatus':
                $res = $this->core->action->status((int) $_POST['id'], $_REQUEST['action']);
                if ($this->core->content_type === 2) {
                    $ret = array(
                        'result' => (bool) $res,
                        'html' => $res);
                } else {
                    return $res;
                }
                break;
            // Būsenos saugojimas
            case 'savestatus':
                $arr = array(
                    'ID' => $_REQUEST['id'],
                    'LABEL' => $_REQUEST['label'],
                );
                if (isset($_REQUEST['parent'])) {
                    $arr['PARENT_ID'] = $_REQUEST['parent'];
                }
                $res = $this->core->action->status($arr, $_REQUEST['action']);

                if ($this->core->content_type === 2) {
                    $ret = array('result' => (bool) $res);
                    if (is_int($res) && isset($_REQUEST['parent'])) {
                        $ret['id'] = $res;
                    }
                } else {
                    header('Location: index.php?m=21');
                    exit;
                }
                break;
            // Būsenos trinimas
            case 'deletestatus':
                $res = $this->core->action->status((int) $_POST['id'], $_REQUEST['action']);
                $ret = array(
                    'result' => (bool) $res,
                    'ids' => $res);
                break;
            // Dokumento būsenų redagavimo forma
            case 'editrelation':
                $res = $this->core->action->status((int) $_POST['id'], $_REQUEST['action']);
                if ($this->core->content_type === 2) {
                    $ret = array(
                        'result' => (bool) $res,
                        'html' => $res);
                } else {
                    return $res;
                }
                break;
            // Dokumento būsenų saugojimas
            case 'saverelation':
                $arr = array(
                    'DOCUMENT_ID' => $_REQUEST['id'],
                    'RELATIONS' => json_decode(stripslashes($_REQUEST['relation']), true)
                );

                $res = $this->core->action->status($arr, $_REQUEST['action']);

                if ($this->core->content_type === 2) {
                    $ret = array('result' => (bool) $res);
                    if (is_int($res) && isset($_REQUEST['parent'])) {
                        $ret['id'] = $res;
                    }
                    //$ret['messages'] = $this->core->get_messages(true);
                } else {
                    header('Location: index.php?m=20');
                    exit;
                }
                break;
            // Dokumentų būsenų trinimas
            case 'deleterelation':
                die;
                $res = $this->core->action->status((int) $_POST['id'], $_REQUEST['action']);
                $ret = array(
                    'result' => (bool) $res,
                    'ids' => $res);
                break;
        }

        return json_encode($ret);
    }

    /**
     * Teisės akto veiksmai
     * @author Justinas Malūkas
     * @return string
     */
    public function act_action()
    {
        // Tikrina ar vartotojas turi teisę į klasifikatorių koregavimą.
        // Jei ne - gražiną pranešimą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_ACT') < 2) {
            return json_encode(array('error' => 'Administracinės teisės pažeidimai'));
        }

        // Veiksmai
        switch ($_REQUEST['action']) {
            // Redagavimo forma
            case 'editform':
                $res = $this->core->action->act((int) $_POST['id'], $_REQUEST['action']);
                if ($this->core->content_type === 2) {
                    $ret = array(
                        'result' => (bool) $res,
                        'html' => $res);
                } else {
                    return $res;
                }
                break;
            // Išsaugojimas
            case 'save':
                $arr = array(
                    'ID' => $_REQUEST['id'],
                    'NR' => $_REQUEST['number'],
                    'LABEL' => $_REQUEST['name'],
                    'VALID_FROM' => $_REQUEST['valid_from'],
                    'VALID_TILL' => $_REQUEST['valid_till'],
                    'CLASSIFICATOR' => $_REQUEST['act_id'],
//                    'FIELDS_VALUES' => serialize($_REQUEST['field']),
                    'VALUE_1' => $_REQUEST['field'][1],
                    'VALUE_2' => $_REQUEST['field'][2],
                    'VALUE_3' => $_REQUEST['field'][3],
                    'VALUE_4' => $_REQUEST['field'][4],
                    'VALUE_6' => $_REQUEST['field'][6]
                );

                if (isset($_REQUEST['parent'])) {
                    $arr['PARENT_ID'] = $_REQUEST['parent'];
                }

                // TODO: kodėl du $this->core->action->act() ?
                $res = $this->core->action->act($arr, $_REQUEST['action']);
                //$res = $this->core->action->act((int) $_POST['id'], $_REQUEST['action']);
                if ($this->core->content_type === 2) {
                    $ret = array('result' => (bool) $res);
                    if (is_int($res) && isset($_REQUEST['parent'])) {
                        $ret['id'] = $res;
                    }
                    //$ret['messages'] = $this->core->get_messages(true);
                } else {
                    header('Location: index.php?m=15');
                    exit;
                }
                break;
            // Trinimas
            case 'delete':
                $res = $this->core->action->act((int) $_POST['id'], $_REQUEST['action']);
                $ret = array(
                    'result' => (bool) $res,
                    'ids' => $res);
                break;
            // Laukų redagavimas
            case 'fields':
                $arr['ID'] = $_POST['item'];
                $type = 'ACT';
                $res = $this->extra_fields_html($type, $arr);
                $ret = array(
                    'result' => (bool) $res,
                    'html' => $res,
                    'data' => $this->core->logic->extra_fields_info($type, $arr));
                break;
            // Laukų saugiojimas
            case 'savefields':
                $type = 'ACT';
                if (isset($_GET['item']) === true) {
                    $_POST['item'] = (int) $_GET['item'];
                }
                $res = $this->core->logic->extra_fields_save($type, $_POST);
                $ret = array(
                    'result' => (bool) $res,
                    'message' => $res);
                break;
            // Paieška
            case 'search':
                $arr = array();
                foreach ($_POST as $key => $val) {
                    switch ($key) {
                        case 'name':
                            $arr['LABEL'] = $val;
                            break;
                    }
                }

                $res = $this->core->action->act($arr, 'search');
                // Jei neranda tėvo, tai pameta įrašą. Todėl hierarchiškai rikiuojam tik tada, kai gražinami visi rezultatai
                if (empty($arr['LABEL']) === true) {
                    $res = $this->core->logic->sort_children_to_parent($res, 'ID', 'PARENT_ID');
                }
                $ret = array(
                    'result' => (bool) $res,
                    'data' => $res);
                break;
        }

        return json_encode($ret);
    }

    /**
     * Veikos veiksmai
     * @author Justinas Malūkas
     * @return string
     */
    public function deed_action()
    {
        // Tikrina ar vartotojas turi teisę į klasifikatorių koregavimą.
        // Jei ne - gražiną pranešimą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_DEED') < 2 && in_array($_REQUEST['action'], array(
                'deedoptions', 'deedrecordfields'), true) !== true) {
            return json_encode(array('error' => 'Administracinės teisės pažeidimai'));
        }

        // Veiksmai
        switch ($_REQUEST['action']) {
            // Redagavimo forma
            case 'editform':
                $res = $this->core->logic->deed_action((int) $_POST['id'], $_REQUEST['action']);
                $ret = array(
                    'result' => (bool) $res,
                    'html' => $res);
                break;
            // Išsaugojimas
            case 'save':
                $arr = array(
                    'data' => array(
                        'ID' => $_REQUEST['id'],
                        'LABEL' => $_REQUEST['name'],
                    ),
                    'extends' => array(
                        'clauses' => explode(',', $_REQUEST['clauses']),
                        'acts' => explode(',', $_REQUEST['acts'])
                    )
                );
                if (isset($_REQUEST['parent'])) {
                    $arr['data']['PARENT_ID'] = $_REQUEST['parent'];
                }
                $res = $this->core->logic->deed_action($arr, $_REQUEST['action']);

                $ret = array('result' => (bool) $res);
                if (is_int($res) && isset($_REQUEST['parent'])) {
                    $ret['id'] = $res;
                }
                //$ret['messages'] = $this->core->get_messages(true);
                break;
            // Trinimas
            case 'delete':
                $res = $this->core->logic->deed_action((int) $_POST['id'], $_REQUEST['action']);
                $ret = array(
                    'result' => (bool) $res,
                    'ids' => $res);
                break;
            // Laukų redagavimas
            case 'fields':
                $arr['ID'] = $_POST['item'];
                $type = 'DEED';
                $res = $this->extra_fields_html($type, $arr);
                $ret = array(
                    'result' => (bool) $res,
                    'html' => $res,
                    'data' => $this->core->logic->extra_fields_info($type, $arr));
                break;
            // Laukų saugiojimas
            case 'savefields':
                $type = 'DEED';
                if (isset($_GET['item']) === true) {
                    $_POST['item'] = (int) $_GET['item'];
                }
                $res = $this->core->logic->extra_fields_save($type, $_POST);
                $ret = array(
                    'result' => (bool) $res,
                    'message' => $res);
                break;
            // Veikos pasirinkimai
            case 'deedoptions':
                $arr = array();
                if (empty($_POST['class_type']) === false) {
                    $arr['CLASS_TYPE'] = $_POST['class_type'];
                }

                $data = $this->core->logic->get_deed_search_values($arr);

                $html = '';
                foreach ($data as $arr) {
                    $html .= '<option value="' . $arr['VALUE'] . '">' . $arr['LABEL'] . '</option>';
                }

                $ret = array(
                    'result' => (bool) true,
                    'html' => $html);
                break;
            // Pagal veiką gražina laukus
            case 'deedrecordfields':
                $template_html = $this->core->manage->view_column_order(null, array(
                    'ID' => null,
                    'TYPE' => 'DEED'), $_POST['record_id']);
                $ret = array(
                    'result' => (bool) $template_html,
                    'html' => $field_info . $template_html);
                break;
        }

        return json_encode($ret);
    }

    /**
     * Webserviso laukų veiksmai
     * @author Justinas Malūkas
     * @return string
     */
    public function ws_action()
    {
        // Tikrina ar vartotojas turi teisę į klasifikatorių koregavimą.
        // Jei ne - gražiną pranešimą
        //if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_DOC') < 2) {
        //	return json_encode(array('error' => 'Administracinės teisės pažeidimai'));
        //}

		// Surenka prieš tai įvestuose įrašuose informaciją

//		$this->updateDataFromPrevieousFields($_REQUEST[

		$tmp = array();
		$tmp['item'] = $_REQUEST['item'];
		$tmp['value'] = $_REQUEST['value'];
		$tmp['record'] = $_REQUEST['record'];
		$values_arr = array();
		$fields_arr = array();

		$record_arr = $this->core->db_getItemByID(PREFIX."ATP_RECORD", $tmp['record']);
		$table_to_id = $record_arr['TABLE_TO_ID'];
		$table_record_id = $record_arr['RECORD_ID'];

		$fqry = "SELECT DISTINCT NAME FROM ".PREFIX."ATP_FIELD WHERE ITEM_ID = '".$table_to_id."' AND (LOWER(LABEL) LIKE '%adres%' OR LOWER(LABEL) LIKE '%telef%' OR LOWER(LABEL) LIKE '%el.%' OR LOWER(LABEL) LIKE '%pajam%')";
		$fcol = $this->core->db_query_fast($fqry);
		while($fres = $this->core->db_next($fcol)) {
			$fields_arr[] = $fres['NAME'];
		}
		if (empty($fields_arr[0]) === FALSE) {
			foreach ($fields_arr as $fkey => $fval) {
				$qry = "SELECT $fval ITEM_VALUE FROM ".PREFIX."ATP_TBL_".$table_to_id." WHERE ($fval IS NOT NULL AND $fval != '') AND RECORD_ID IN (
				SELECT TB.RECORD_ID FROM ".PREFIX."ATP_FIELDS_VALUES TA INNER JOIN ".PREFIX."ATP_RECORD TB ON TA.RECORD_ID = TB.ID WHERE TA.FIELD_ID IN (
				SELECT FIELD_ID FROM ".PREFIX."ATP_FIELDS_VALUES WHERE FIELD_ID IN (
				SELECT ID FROM ".PREFIX."ATP_FIELD WHERE ITEM_ID = '".$tmp['item']."'
				) AND VALUE != '' AND VALUE = '".$tmp['value']."' 
				) AND VALUE = '".$tmp['value']."') ORDER BY ID DESC";
				$col = $this->core->db_query_fast($qry);
				$res = $this->core->db_next($col);
				$values_arr[$fval] = $res['ITEM_VALUE'];
			}
			$add_js_html = '<script>';
			if (count($values_arr)) {
				foreach ($values_arr as $vkey => $vval) {
					if (empty($vval) === FALSE) {
						$qry = "UPDATE ".PREFIX."ATP_TBL_".$table_to_id." SET $vkey = '".CleanText($vval)."' WHERE ($vkey = '' OR $vkey IS NULL) AND RECORD_ID = '".$record_arr['RECORD_ID']."'";
//						$col = $this->core->db_query_fast($qry);
						$vfield = strtolower($vkey);
						$add_js_html .= "if(document.getElementsByName('$vfield')[0]) {if (document.getElementsByName('$vfield')[0].value == '') {document.getElementsByName('$vfield')[0].value='$vval';}}";
					}
				}
			}
		}
		$add_js_html .= '</script>';
		// Veiksmai
        switch ($_REQUEST['action']) {
            // Laukų redagavimas
            case 'wsform':
                $arr['ID'] = $_REQUEST['item'];
                //$arr['WS_ID'] = $_REQUEST['source'];

                $type = 'WS';
                $res = $this->extra_fields_html($type, $arr);

                //if($this->core->content_type === 2) {
                $ret = array(
                    'result' => (bool) $res,
                    'html' => $res,
                    'data' => $this->core->logic->extra_fields_info($type, $arr));
                //} else
                //	return $res;
                break;
            // Laukų saugiojimas
            case 'savefields':
                if (isset($_GET['item']) === true) {
                    $_POST['item'] = (int) $_GET['item'];
                }

                $type = 'WS';
                $res = $this->core->logic->extra_fields_save($type, $_POST);

                $ret = [
                    'result' => (bool) $res,
                    'message' => $res
                ];
                break;
            // Web-serviso vykdymas
            case 'wssearch':
                $result = null;

                $fieldId = filter_input(INPUT_POST, 'item', FILTER_VALIDATE_INT);
                $recordId = filter_input(INPUT_POST, 'record', FILTER_VALIDATE_INT);
                $makeUpdate = filter_input(INPUT_POST, 'update', FILTER_VALIDATE_BOOLEAN);
                $fieldValue = trim(filter_input(INPUT_POST, 'value'));

				if ($makeUpdate == '1') {
					$dqur = "UPDATE ".PREFIX."ATP_FIELDS_VALUES SET VALUE = '' WHERE FIELD_ID IN (SELECT FIELD_ID FROM ".PREFIX."ATP_WEBSERVICES WHERE PARENT_FIELD_ID = '".$fieldId."') AND RECORD_ID = '".$recordId."'";
					$dcol = $this->core->db_query($dqur, $dbin);
				}

                // Web-serviso laukai į kuriuos ateina informacija iš formos lauko
                $relations = $this->core->logic2->get_webservices_fields(['FIELD_ID' => $fieldId]);
                if (empty($relations) === false) {
                    $ws = $this->core->logic2->get_ws();
//                    $valueManager = new \Atp\Record\Values($this->core);
                    $recordHelper = $this->core->factory->Record();
                    
                    $data = [
                        'html' => [],
                        'values' => [],
                        'messages' => [],
                    ];


                    // Priskirti gyventojų registro gimimo datos ir regitros registravimo būsenos laukai
                    $birthdayFieldId = null;
                    $birthdayFieldValue = null;
                    $regitraStatusFieldId = null;
                    $regitraStatusFieldValue = null;
                    
                    $wsField = $ws->get_field('GRT', ['TAG' => 'GIMIMO_DATA']);
                    if (empty($wsField) === false) {
                        $wsFieldXRef = $this->core->logic2->get_webservices([
                            'WS_NAME' => 'GRT',
                            'WS_FIELD_ID' => $wsField['ID'],
                            'PARENT_FIELD_ID' => $fieldId
                            ], true);
                        if (count($wsFieldXRef)) {
                            $birthdayFieldId = (int) $wsFieldXRef['FIELD_ID'];
                        }
                    }
                    $wsField = $ws->get_field('RGT', ['TAG' => 'REG_DATA_MAIN_INFO_KTPR_REGISTRAVIMO_BUSENA']);
                    if (empty($wsField) === false) {
                        $wsFieldXRef = $this->core->logic2->get_webservices([
                            'WS_NAME' => 'RGT', 'WS_FIELD_ID' => $wsField['ID'],
                            'PARENT_FIELD_ID' => $fieldId
                            ], true);
                        if (count($wsFieldXRef)) {
                            $regitraStatusFieldId = (int) $wsFieldXRef['FIELD_ID'];
                        }
                    }


                    $owner_code_field = array(
                        'REG_DATA_PERS_INFO_A_KTPR_ASMENS_KODAS',
                        'REG_DATA_PERS_INFO_A_KTPR_IMONES_KODAS'
                    );

                    // Jei įrašas importuotas iš VMSA
                    if ($this->core->logic2->is_VMSA_record($recordId) === true) {
                        // Visi web-serviso laukai pagal tėvinį lauką
                        $fields = $this->core->logic2->get_fields(['ITYPE' => 'WS', 'ITEM_ID' => $fieldId], false, '`ORD` ASC');
                        if (count($fields) === 0) {
                            continue;
                        }

                        foreach ($fields as $field) {
                            if (empty($field) === true) {
                                continue;
                            }

//                            $value = $valueManager->getValue($recordId, $field['ID']);
                            $value = $recordHelper->getFieldValue($recordId, $field['ID']);

//                            $data['values'][$field['ID']] = null;
                            if (empty($value) === true) {
                                continue;
                            }

                            
                            // Jei gyventojų registras ir yra gimimo datos informacija, tai tikrina ar nuo datos praėjo 16 m.
                            if ($birthdayFieldId && (int) $field['ID'] === $birthdayFieldId) {
                                $birthdayFieldValue = $value;
                            } elseif ($regitraStatusFieldId && (int) $field['ID'] === $regitraStatusFieldId) {
                                $regitraStatusFieldValue = $value;
                            }


                            $data['html'][$field['ORD']] = '<div class="atp-table-fill info'
                                . (!$field['VISIBLE'] ? ' hidden' : '')
                                . '">'
                                . '<div class="atp-rec-title">' . $field['LABEL'] . ':</div>'
                                . '<span class="value">' . $value . '</span>'
                                . '</div>';
                            $data['values'][$field['ID']] = $value;
                        }
                    } else {
                        $parseDateFieldValue = function($value, $fieldData) {
                            $is_empty = false;

                            $value = str_replace('.', '-', $value);
                            if (empty($value) === true) {
                                $is_empty = true;
                            }

                            $value = strtotime($value);

                            if ($fieldData['TYPE'] == 11) {
                                $value = date('Y', $value);
                            } elseif ($fieldData['TYPE'] == 12) {
                                $value = date('Y-m-d', $value);
                            } elseif ($fieldData['TYPE'] == 13) {
                                $value = date('H:i:s', $value);
                            } elseif ($fieldData['TYPE'] == 14) {
                                $value = date('Y-m-d H:i:s', $value);
                            }

                            if ($is_empty === true) {
                                $value = '';
                            }

                            return $value;
                        };
                        

                        // Laukų struktūra į kuruos ateina informacija
                        $fields = $this->core->logic2->get_fields(['ITYPE' => 'WS', 'ITEM_ID' => $fieldId], false, '`ORD` ASC');

                        // Išvedami sistemoje išsaugoti duomenys
                        if($makeUpdate === false) {
                            // Prie lauko pririštų webserviso duomenų laukų ryšiai
                            $fieldsWsXRef = $this->core->logic2->get_webservices([
                                'PARENT_FIELD_ID' => $fieldId
                            ]);

                            foreach($fieldsWsXRef as $fieldXRef) {
                                $wsName = $fieldXRef['WS_NAME'];
                                
//                                // Nurodomas web-servisas
//                                $ws->set_webservice($wsName);

                                // Web-serviso lauko informaciją
                                $info = $ws->get_field($wsName, ['ID' => $fieldXRef['WS_FIELD_ID']]);
                                if (empty($info) === true) {
                                    continue;
                                }

                                $field = $fields[$fieldXRef['FIELD_ID']];

                                // Lauko reikšmė
//                                $value = $valueManager->getValue($recordId, $fieldXRef['FIELD_ID']);
                                $value = $recordHelper->getFieldValue($recordId, $fieldXRef['FIELD_ID']);

//                                $data['values'][$field['ID']] = null;
                                if (empty($value) === true) {
                                    continue;
                                }

                                // Reikšmė konvertuojama pagal tipą
                                // Datos ir laiko laukai
                                if ($field['SORT'] == 7) {
                                    $value = $parseDateFieldValue($value, $field);
                                }


                                // Jei gyventojų registras ir yra gimimo datos informacija, tai tikrina ar nuo datos praėjo 16 m.
                                if ($birthdayFieldId && (int) $field['ID'] === $birthdayFieldId) {
                                    $birthdayFieldValue = $value;
                                } elseif ($regitraStatusFieldId && (int) $field['ID'] === $regitraStatusFieldId) {
                                    $regitraStatusFieldValue = $value;
                                }


                                $data['html'][$field['ORD']] = '<div class="atp-table-fill info'
                                    . (!$field['VISIBLE'] ? ' hidden' : '')
                                    . (in_array($info['TAG'], $owner_code_field, true) ? ' field-owner-code' : '')
                                    . '">'
                                    . '<div class="atp-rec-title">' . $field['LABEL'] . ':</div>'
                                    . '<span class="value">' . $value . '</span>'
                                    . '</div>';
                                $data['values'][$field['ID']] = $value;
                            }
                        } else {
                            // Išvedami duomenys iš registrų (per WP registrus)
                            // Surenkami parametrai web-serviso užklausai
                            $parameters = array();

                            foreach ($relations as $relation) {
                                $info = $ws->get_param($relation['WS_NAME'], array('ID' => $relation['WS_FIELD_ID']));
                                if (empty($info['TAGS']) === false) {
                                    foreach ($info['TAGS'] as $tag) {
                                        // Jei tai gyventojų registras, asmens kodo laukas, tačiau neatitinka asmens kodo formato
                                        if (in_array($relation['WS_NAME'], array('GRT',
                                                'SDR')) && $tag === 'SEARCH_CODE' && (int) preg_match('/^[0-9]{11}$/', $fieldValue) === 0) {
                                            $expl = explode(' ', $fieldValue);
                                            foreach ($expl as $part) {
                                                if ($this->core->logic2->validateDate($part, array(
                                                        'Y-m-d', 'Y-m', 'Y')) === true) {
                                                    if ($relation['WS_NAME'] === 'GRT') {
                                                        $parameters[$relation['WS_NAME']]['SEARCH_BIRTH_DATE'] = $part;
                                                    } elseif ($relation['WS_NAME'] === 'SDR') {
                                                        $parameters[$relation['WS_NAME']]['SEARCH_BDATE'] = $part;
                                                    }
                                                } elseif (preg_match('/[0-9]/', $part, $m) === 0) {
                                                    $parameters[$relation['WS_NAME']]['SEARCH_FNAME'][] = $part;
                                                    $parameters[$relation['WS_NAME']]['SEARCH_NAME'][] = $part;
                                                }
                                            }
                                            if (count($parameters[$relation['WS_NAME']]['SEARCH_FNAME']) === 1 && count($parameters[$relation['WS_NAME']]['SEARCH_NAME']) === 1) {
                                                unset($parameters[$relation['WS_NAME']]['SEARCH_FNAME']);
                                            }
                                            continue;
                                        } else {
                                            $parameters[$relation['WS_NAME']][$tag] = $fieldValue;
                                        }
                                    }
                                }
                            }


                            foreach ($parameters as $ws_name => $arr) {
                                // Nurodomas web-servisas
                                $ws->set_webservice($ws_name);
                                // Į web-servisą paduodami parametrai
                                $ws->set_parameters($arr, 'POST');
                                // Vykdomas web-servisas
                                $ws->initialize();
                                // Web-serviso rezultatas
                                $response = $ws->get_result();
                                // Web-serviso pranešimai
                                $messages = $ws->get_messages();
								if (empty($arr['SEARCH_CODE']) === FALSE) {
									if (in_array($ws_name, array('GRT', 'JAR', 'SDR'))) {
										if ((is_numeric($arr['SEARCH_CODE']) != TRUE) OR (strlen($arr['SEARCH_CODE']) != 11 AND strlen($arr['SEARCH_CODE']) != 9)) {
											$messages[] = array('message' => ' Neatitinka įmonės arba asmens kodo standarto.', 'type' => 'error');
										}
									}
								}
								if (empty($messages) === false) {
                                    $data['messages'][$ws_name] = $messages;
                                }
                                // Praleidžia, jei nėra jokio rezultato
                                if (empty($response) === true) {
//                                    continue;
                                }

                                // Sąryšiai tarp web-serviso laukų ir dokumento laukų į kuriuos keliaus informacija
                                $ws_relations = $this->core->logic2->get_webservices([
                                    'WS_NAME' => $ws_name,
                                    'PARENT_FIELD_ID' => $fieldId
                                ]);
                                foreach ($ws_relations as $relation) {
                                    // Web-serviso lauko informaciją
                                    $info = $ws->get_field($ws_name, array('ID' => $relation['WS_FIELD_ID']));
                                    if (empty($info) === true) {
                                        continue;
                                    }

                                    // Lauko informacija, į kurį keliaus informacija
                                    $field = $fields[$relation['FIELD_ID']];
                                    if (empty($field) === true) {
                                        continue;
                                    }

                                    // Gauta lauko reikšmė
                                    $value = $response[$info['TAG']];

//                                    $data['values'][$field['ID']] = null;
                                    if (empty($value) === true) {
                                        continue;
                                    }

                                    // Reikšmė konvertuojama pagal tipą
                                    // Datos ir laiko laukai
                                    if ($field['SORT'] == 7) {
                                        $value = $parseDateFieldValue($value, $field);
                                    }

                                    
                                    // Jei gyventojų registras ir yra gimimo datos informacija, tai tikrina ar nuo datos praėjo 16 m.
                                    if ($birthdayFieldId && (int) $field['ID'] === $birthdayFieldId) {
                                        $birthdayFieldValue = $value;
                                    } elseif ($regitraStatusFieldId && (int) $field['ID'] === $regitraStatusFieldId) {
                                        $regitraStatusFieldValue = $value;
                                    }


                                    $data['html'][$field['ORD']] = '<div class="atp-table-fill info'
                                        . (!$field['VISIBLE'] ? ' hidden' : '')
                                        . (in_array($info['TAG'], $owner_code_field, true) ? ' field-owner-code' : '')
                                        . '">'
                                        . '<div class="atp-rec-title">' . $field['LABEL'] . ':</div>'
                                        . '<span class="value">' . $value . '</span>'
                                        . '</div>';
                                    $data['values'][$field['ID']] = $value;
                                }
                            }
                        }
                    }

//                    $fieldsWsXRef = $this->core->logic2->get_webservices([
//                        'PARENT_FIELD_ID' => $fieldId
//                    ]);
//
//                    foreach ($fieldsWsXRef as $fieldXRef) {
//                        $wsName = $fieldXRef['WS_NAME'];
//
//                        // Web-serviso lauko informaciją
//                        $info = $ws->get_field($wsName, [
//                            'ID' => $fieldXRef['WS_FIELD_ID']
//                        ]);
//                        if (empty($info) === true) {
//                            continue;
//                        }
//
//                        $field = $fields[$fieldXRef['FIELD_ID']];
//
//                        if (!isset($data['values'][$field['ID']])) {
//                            $data['values'][$field['ID']] = null;
//                        }
//                    }
                    
                    if ($birthdayFieldValue !== null) {
                        $timestamp = strtotime(str_replace('.', '-', $birthdayFieldValue));
                        if ($timestamp > 0) {
                            if ($this->is_older_than_16_years($birthdayFieldValue) === false) {
                                $data['messages']['GRT'][] = [
                                    'message' => 'Asmuo jaunesnis nei 16 metų',
                                    'type' => 'warning'
                                ];
                            }
                        }
                    }
                    if ($regitraStatusFieldValue !== null) {
                        if (mb_strtolower(trim($regitraStatusFieldValue), 'utf8') !== "įregistruota") {
                            $data['messages']['RGT'][] = [
                                'message' => 'Būsena yra "' . $regitraStatusFieldValue . '"',
                                'type' => 'warning'
                            ];
                        }
                    }
					// Pranešimai
                    if (count($data['messages'])) {
                        // Unique registry messages
                        foreach ($data['messages'] as $wsName => $messages) {
							 $data['messages'][$wsName] = (array) array_map('json_decode', array_unique(array_map('json_encode', $messages)), [true]);
                        }
						array_walk($data['messages'], function (&$arr, $ws_name, $ws) {
                            array_walk($arr, function (&$message, $key, $ws_name) {
                                $message = '<div class="atp-table-fill info messages ' . $ws_name . '-messages">'
                                    . '<div class="atp-rec-title">&nbsp;</div>'
                                    . '<span class="atp-message atp-message-' . $message['type'] . ' value">' . $message['message'] . '</span>'
                                    . '</div>';
                            }, $ws_name);
                            $arr = join('', $arr);
                        }, $ws);
						$result .= join('', $data['messages']);
                    }

                    // Laukų HTML'as ir jų reikšmės JS'e
                    if (count($data['values'])) {
                        $result .= join('', $data['html']);
                        foreach ($data['values'] as $wsFieldId => $value) {
                            $result .= '<input type="hidden" name="ws_field[' . $fieldId . '][' . $wsFieldId . ']" value="' . htmlspecialchars($value) . '" />';
                        }
                    }
                }

                $ret = array(
                    'result' => (bool) $result,
                    'html' => $result);
                break;
        }
		if (empty($add_js_html) === FALSE) {
			$ret['html'] .= $add_js_html;
		}
        return json_encode($ret);
    }

    private function is_older_than_16_years($date)
    {
        $now = new \DateTime();
        if ((int) $now->diff(new \DateTime($date))->format('%Y') < 16) {
            return false;
        }

        return true;
    }

    /**
     * Sugeneruoja papildomų laukų formą prie klasifikatorių ir straipsnių
     * @param string $type Tipas: classificator / clause
     * @param array $arr Duomenys
     * @return string
     */
    public function extra_fields_html($type, $arr)
    {
        $typeMap = [
            'CLASS' => 'A_CLASSIFICATION',
            'CLAUSE' => 'A_CLAUSE',
            'DEED' => 'A_DEED',
            'ACT' => 'A_ACT'
        ];
        if ($type !== 'WS' && !$this->core->factory->User()->getLoggedPersonDutyStatus($typeMap[$type])) {
            return $this->core->printForbidden();
        }


        $adminJournal = $this->core->factory->AdminJournal();
        $adminJournal->addRecord(
            $adminJournal->getVisitRecord(
                'Papildomų laukų formą'
                . ' (Formos elemento tipas "' . $type . '";'
                . ' "Formos elemento identifikatorius "' . $arr['ID'] . '").'
            )
        );

        $table_id = (int) $arr['ID'];
        $field = $this->core->logic2->get_fields(['ID' => $table_id]);

        $field_sort = $this->core->logic->get_field_sort();
        $field_type = $this->core->logic->get_field_type();
//        $classifications = $this->core->logic2->get_classificator(null, null, null, 'ID, LABEL');

        $tmpl = $this->core->factory->Template();
        $tmpl->handle = 'fields_file';
        $tmpl->set_file($tmpl->handle, 'fields.tpl');

        $handles = $tmpl->set_multiblock($tmpl->handle, [
            'default_fields_ws', // Web-serviso pasirinkimas
            'default_fields', // Web-serviso pasirinkimo konteineris
            //
            'webservice_field', // Blokas web-serviso laukui
            'webservice_block', // Blokas web-serviso lauko atitikimui
            'parent_field_types' // Blokas lauko atitikimams web-servisų laukams
        ]);


        // Formos blokas
        $form_block = $tmpl->handle . 'form';
        $tmpl->set_block($tmpl->handle, 'form', $form_block);

        $tmpl->set_var(
            $this->core->set_lng_vars(
                ['title', 'table', 'not_chosen', 'save'], [
                'action_edit_table' => 'index.php?m=1&action=edit'
                ]
            )
        );

        if (($type === 'CLASS' && $this->core->factory->User()->getLoggedPersonDutyStatus('A_CLASSIFICATION') > 1) || ($type === 'CLAUSE' && $this->core->factory->User()->getLoggedPersonDutyStatus('A_CLAUSE') > 1) || ($type === 'DEED' && $this->core->factory->User()->getLoggedPersonDutyStatus('A_DEED') > 1) || ($type === 'ACT' && $this->core->factory->User()->getLoggedPersonDutyStatus('A_ACT') > 1) || ($type === 'WS')) {
            $tmpl->set_var($this->core->set_lng_vars(['parent_table', 'not_chosen']));
            $tmpl->set_var($this->core->set_lng_vars(['save', 'chosen_table_structure',
                    'title', 'not_chosen']));

            if ($type === 'WS') {
                $ws = $this->core->logic2->get_ws();

                $default_fields_json = [];

                // Web-serviso laukai
                $ws_fields = $ws->get_webservices_fields();
                foreach ($ws_fields as $ws_name => $arr) {
                    $default_fields_json[$ws_name]['fields'] = $arr;
                }

                // Web-serviso parametrų laukai
                $ws_params = $ws->get_webservices_params();
                foreach ($ws_params as $ws_name => $arr) {
                    $default_fields_json[$ws_name]['params'] = $arr;
                }

                // Web-serviso pasirinkimas
                $info = $ws->get_infos();
                foreach ($info as &$data) {
                    $tmpl->set_var([
                        //'value' => $data['id'],
                        'value' => $data['name'],
                        'label' => htmlspecialchars($data['label'])
                    ]);
                    $tmpl->parse($handles['default_fields_ws'], 'default_fields_ws', true);
                }

                // Web-servisų masyvas JavaScript'ui
                $tmpl->set_var('default_fields_json', json_encode($default_fields_json));
                $tmpl->parse($handles['default_fields'], 'default_fields');

                $tmpl->set_var([
                    'class' => ' sample',
                    'ws_name' => '',
                    'ws_html' => ' disabled="disabled"',
                    'name' => ''
                ]);
                $tmpl->clean($handles['webservice_field']);
                $tmpl->parse($handles['webservice_block'], 'webservice_block');


                // Surenka lauko web-servisų laukus su kuriais apjungtas laukas
                $field_relations = $this->core->logic2->get_webservices_fields([
                    'FIELD_ID' => $table_id]);

                $field_relations_tmp = [];
                foreach ($field_relations as &$relation) {
                    $field_relations_tmp[$relation['WS_NAME']][] = $relation['WS_FIELD_ID'];
                }
                $field_relations = $field_relations_tmp;
                unset($field_relations_tmp);


                // Spausdina laukų ryšius
                foreach ($field_relations as $name => &$values) {
                    if (isset($ws_params[$name]) === false) {
                        continue;
                    }

                    $i = 0;
                    foreach ($ws_params[$name] as &$value) {
                        $tmpl->set_var([
                            'value' => $value['ID'],
                            'label' => $value['LABEL'],
                            'selected' => (in_array($value['ID'], $values) ? ' selected="selected"' : ''),
                        ]);
                        $tmpl->parse($handles['webservice_field'], 'webservice_field', $i);
                        $i++;
                    }

                    $tmpl->set_var([
                        'class' => '',
                        'ws_name' => $name,
                        'ws_html' => '',
                        'name' => $info[$name]['label']
                    ]);
                    $tmpl->parse($handles['webservice_block'], 'webservice_block', true);
                }

                $tmpl->parse($handles['parent_field_types'], 'parent_field_types');
            } else {
                $tmpl->set_var($handles['default_fields'], '');
                $tmpl->set_var($handles['parent_field_types'], '');
                $tmpl->clean($handles['default_fields']);
            }

            // TODO: regis nereikalingas set_var'as
            $tmpl->set_var($this->core->set_lng_vars(['not_chosen', 'save'], [
                    'action_delete_table' => 'index.php?m=1&action=delete']));
            $tmpl->parse($form_block, 'form');
        } else {
            $tmpl->clean($form_block);
        }

        $tmpl->set_var([
            'm' => $this->core->extractVariable('m'),
            'item' => $table_id,
            'sys_message' => $this->core->get_messages(),
            'table_id' => $table_id,
            'field_type' => json_encode($field_type),
            'field_sort' => json_encode($field_sort),
//            'classifications' => json_encode($classifications),
            'nr' => (empty($_POST['nr']) ? uniqid() : $_POST['nr']),
            'nr2' => uniqid(),
            'fields_edit_block' => $this->core->manage->fields_edit_form(true)
        ]);

        $tmpl->set_var('GLOBAL_SITE_URL', $this->core->config['GLOBAL_SITE_URL']);

        $tmpl->parse($tmpl->handle . '_out', $tmpl->handle);
        $output = $tmpl->get($tmpl->handle . '_out');

        return $this->core->atp_content($output);
    }

    // TODO: galima perkelt į $this->ajax->classificator_action
    /**
     * @deprecated nenaudojamas
     * @return boolean/string
     */
    public function get_classification()
    {
        trigger_error('get_classification() is deprecated', E_USER_DEPRECATED);
        $classification_id = (!empty($_POST['classification_id'])) ? $_POST['classification_id'] : null;
        $parent_id = (!empty($_POST['parent_id'])) ? $_POST['parent_id'] : null;

        $classifications = $this->core->logic->get_classification($classification_id, $parent_id);
        if (!empty($classifications)) {
            return json_encode($classifications);
        }

        return false;
    }

    /**
     * @deprecated Nebenaudojamas
     * @return boolean
     */
    public function get_source_value()
    {
        trigger_error('\Atp\Ajax::get_source_value() is deprecated', E_USER_ERROR);
        $sort_id = (!empty($_POST['sort_id'])) ? $_POST['sort_id'] : 0;
        $type_id = (!empty($_POST['type_id'])) ? $_POST['type_id'] : 0;
        $source_id = (!empty($_POST['source_id'])) ? $_POST['source_id'] : 0;

        if (!empty($sort_id) && !empty($type_id) && !empty($source_id)) {
            $field_sort = $this->core->logic->get_field_sort();
            $field_type = $this->core->logic->get_field_type();

            if ($field_sort[$sort_id]['SORT'] == 'webservice') {
                $web_services = $this->core->logic->get_web_services();

                return !empty($web_services[$source_id]) ? json_encode($web_services[$source_id]) : null;
            }
        }

        return false;
    }

//	/**
//	 * @deprecated Nebenaudojamas
//	 * @return boolean
//	 */
//	function get_webservice() {
//		trigger_error('\Atp\Ajax::get_webservice() deprecated', E_USER_FATAL);
//
//		$table_id = (int) $_POST['table_id'];
//		$webservice_id = (int) $_POST['webservice_id'];
//		$col = (!empty($_POST['col'])) ? $_POST['col'] : array();
//
//		if (!empty($table_id) && !empty($col) && !empty($webservice_id)) {
//			$new_cols = array();
//			foreach ($col as $value)
//				$new_cols[strtoupper($value['NAME'])] = $value['VALUE'];
//
//			$webservices = $this->core->logic->get_web_services();
//			$webservice = $webservices[$webservice_id];
//
//			$structure = array();
//			$webservice_search = array();
//			$table = $this->core->logic2->get_document($table_id);
//			$table_structure = $this->core->logic->get_table_fields($table_id);
//			foreach ($table_structure as $structure_chk) {
//				if (array_key_exists($structure_chk['NAME'], $new_cols) && $structure_chk['SOURCE'] == $webservice_id) {
//					$structure[$structure_chk['VALUE']] = $structure_chk['NAME'];
//					$webservice_search[$structure_chk['VALUE']] = $new_cols[$structure_chk['NAME']];
//					if ($structure_chk['OTHER_SOURCE'] && $structure_chk['VALUE_TYPE'] == 2) {
//						foreach ($table_structure as $chk_value) {
//							if ($chk_value['SOURCE'] == $structure_chk['OTHER_SOURCE'] && $chk_value['VALUE'] == $structure_chk['OTHER_VALUE']) {
//								$structure[$chk_value['VALUE']] = $chk_value['NAME'];
//								break;
//							}
//						}
//					}
//				}
//			}
//
//			if (!empty($structure)) {
//				$search = $this->core->logic->search_webservice($structure, $webservice_id, $webservice_search);
//				return json_encode($search);
//			}
//		}
//
//		return FALSE;
//	}

    public function get_clause_child()
    {
        $clause_id = (!empty($_POST['clause_id']) ? $_POST['clause_id'] : 0);

        if (!empty($clause_id)) {
            $clause = $this->core->logic2->get_clause(array('ID' => $clause_id), true);
            if ($clause['TYPE'] < 4) {
                $clause_child = $this->core->logic2->get_clause(array('PARENT_ID' => $clause['ID']));
            }

            if (!empty($clause_child)) {
                return json_encode($clause_child);
            }
        }

        return false;
    }

    public function search_department()
    {
        $arr = array();
        if ($_POST['department']) {
            $arr['SHOWS'] = $_POST['department'];
        }

        $fiels = array('B.ID', 'B.SHOWS');
        $result = $this->core->logic->search_departments($arr, $fiels);
        return json_encode($result);
    }

    public function search_user()
    {
        $arr = array();
        if ($_POST['user']) {
            $arr['SHOWS'] = $_POST['user'];
        }
        if ($_POST['department_id']) {
            $arr['STRUCTURE_ID'] = $_POST['department_id'];
        }
        if ($_POST['type']) {
            $arr['TYPE'] = $_POST['type'];
        }

        $fiels = array('A.ID', 'A.SHOWS', 'B.STRUCTURE_ID', 'C.SHOWS as DEPARTMENT_TITLE');
        $result = $this->core->logic->search_users($arr, $fiels, 'A.SHOWS ASC, C.SHOWS ASC');
        return json_encode($result);
    }

    // TODO: search_clause2 ir search_clause galima apjungti?
    public function search_clause2()
    {
        $arr = array();
        if ($_POST['clause']) {
            $arr['NAME'] = $_POST['clause'];
        }
        if ($_POST['name']) {
            $arr['NAME'] = $_POST['name'];
        }
        // Skyriaus ir straipsnio sąryšis panaikintas
        //if ($_POST['department_id'])
        //	$arr['M_DEPARTMENT_ID'] = $_POST['department_id'];

        $fiels = array('DISTINCT A.ID', 'A.PARENT_ID', 'A.NAME');
        $result = $this->core->logic->search_clauses($arr, $fiels);
        // Jei neranda tėvo, tai pameta įrašą. Todėl hierarchiškai rikiuojam tik tada, kai gražinami visi rezultatai
        if (empty($arr['NAME']) === true) {
            $result = $this->core->logic->sort_children_to_parent($result, 'ID', 'PARENT_ID');
        }

        return json_encode($result);
    }

    public function search_clause()
    {
        $arr = $_POST;
        $arr = array_filter($arr);
        $where = array();
        foreach ($arr as $key => $val) {
            switch ($key) {
                case 'name':
                    //$where[] = 'NAME REGEXP "' . addslashes($val) . '"';
                    $where[] = 'NAME LIKE("%' . addslashes($val) . '%")';
                    break;
                case 'clause':
                    //$where[] = 'CLAUSE REGEXP "' . addslashes($val) . '"';
                    $where[] = 'CLAUSE LIKE("%' . addslashes($val) . '%")';
                    break;
                case 'section':
                    $where[] = 'SECTION LIKE("%' . addslashes($val) . '%")';
                    break;
                case 'paragraph':
                    $where[] = 'PARAGRAPH LIKE("%' . addslashes($val) . '%")';
                    break;
                case 'subparagraph':
                    $where[] = 'SUBPARAGRAPH LIKE("%' . addslashes($val) . '%")';
                    break;
                case 'clause_id':
                    break;
                case 'section_id':
                    break;
                case 'paragraph_id':
                    break;
                case 'subparagraph_id':
                    break;
            }
        }
        $res = $this->core->logic->search_clause(array('ID', 'PARENT_ID', 'NAME',
            'CLAUSE',
            'SECTION', 'PARAGRAPH', 'SUBPARAGRAPH'), $where);
        // Jei neranda tėvo, tai pameta įrašą. Todėl hierarchiškai rikiuojam tik tada, kai gražinami visi rezultatai
        if (empty($where) === true && empty($res) === false) {
            $res = $this->core->logic->sort_children_to_parent($res, 'ID', 'PARENT_ID');
        }
        if (empty($res) === true) {
            $res = array();
        }
        return json_encode($res);
    }

    /**
     * @deprecated nenaudojamas
     * @return boolean/string
     */
    public function get_clause()
    {
        trigger_error('get_clause() is deprecated', E_USER_DEPRECATED);
        //$clause_id = (!empty($_POST['clause_id']) ? $_POST['clause_id'] : 0);
        //$section_id = (!empty($_POST['section_id']) ? $_POST['section_id'] : 0);
        //$paragraph_id = (!empty($_POST['paragraph_id']) ? $_POST['paragraph_id'] : 0);
        //$subparagraph_id = (!empty($_POST['subparagraph_id']) ? $_POST['subparagraph_id'] : 0);
        //if (!empty($subparagraph_id))
        if (!empty($_POST['subparagraph_id'])) {
            $id = &$_POST['subparagraph_id'];
        }
        //elseif (!empty($paragraph_id))
        elseif (!empty($_POST['paragraph_id'])) {
            $id = &$_POST['paragraph_id'];
        }
        //elseif (!empty($section_id))
        elseif (!empty($_POST['section_id'])) {
            $id = &$_POST['section_id'];
        }
        //elseif (!empty($clause_id))
        elseif (!empty($_POST['clause_id'])) {
            $id = &$_POST['clause_id'];
        } else {
            $id = 0;
        }

        if (!empty($id)) {
            $clause[$id] = $this->core->logic2->get_clause(array('ID' => $id), true);
            if (!empty($clause)) {
                return json_encode($clause);
            }
        }

        return false;
    }

    // TODO: buvo naudojama ryšiams. Kolkas nebereikia
    public function get_clause_relation()
    {
        $clause_id = (int) $_REQUEST['clause_id'];

        if (!empty($clause_id)) {
            $clause_relation = $this->core->logic->get_clause_relation(array('CLAUSE_ID' => $clause_id));
            if (!empty($clause_relation)) {
                return json_encode($clause_relation);
            }
        }

        return false;
    }

    /**
     * Dokumento ryšio laukui gauna reikšmės pagal nutylėjimą įvedimo lauką
     * @param int $field_id
     */
    public function get_document_relation_default_field()
    {
        $field_id = $_REQUEST['id'];
        $document_id = $_REQUEST['document'];
        $parent_document = $_REQUEST['parent_document'];

        $relation = $this->core->logic2->get_field_relations(array(
            'COLUMN_ID' => $field_id,
            'DOCUMENT_ID' => $document_id,
            'DOCUMENT_PARENT_ID' => $parent_document), true);

        if ($relation['DEFAULT'] === null && $relation['COLUMN_PARENT_ID'] !== null) {
            return;
        }

        $html = $this->core->manage->get_field_html(array('ID' => $field_id, 'VALUE' => $relation['DEFAULT']));
        if ($html === false) {
            return;
        }
        return $html;
    }

    /**
     * @deprecated Nebenaudojamas
     * @return type
     */
    public function record_payments_form()
    {
        trigger_error('Function \Atp\Ajax::record_payments_form() is deprecated', E_USER_DEPRECATED);
        return json_encode($this->core->manage->record_payments_form($_GET['table_id'], $_GET['record_id']));
    }

    public function search_services()
    {
        $data = $this->core->logic2->get_external_service(array(0 => '`SHOWS` LIKE "%' . $this->core->db_escape($_POST['name']) . '%"'), false, 'SHOWS DESC', 'ID, SHOWS');
        $ret = array();
        foreach ($data as &$arr) {
            $r = array('ID' => $arr['ID'], 'NAME' => $arr['SHOWS']);
            $ret[] = $r;
        }
        return json_encode($ret);
    }

    /**
     * Paieškos laukų veiksmai
     * @author Justinas Malūkas
     * @return string
     */
    public function search_fields_action()
    {
        // Tikrina ar vartotojas turi teisę į klasifikatorių koregavimą.
        // Jei ne - gražiną pranešimą
        if ($this->core->factory->User()->getLoggedPersonDutyStatus('A_TABLE') < 2) {
            return json_encode(array('error' => 'Administracinės teisės pažeidimai'));
        }

        // Veiksmai
        switch ($_REQUEST['action']) {
            // Redagavimo forma
            case 'editform':
                $res = $this->core->action->searchFields((int) $_POST['id'], $_REQUEST['action']);
                if ($this->core->content_type === 2) {
                    $ret = array(
                        'result' => (bool) $res,
                        'html' => $res);
                } else {
                    return $res;
                }
                break;
            case 'save':
                $arr = array(
                    'ID' => filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT),
                    'NAME' => filter_input(INPUT_POST, 'field_name', FILTER_SANITIZE_STRING),
                    'TYPE' => filter_input(INPUT_POST, 'field_type', FILTER_VALIDATE_INT),
                    'USE_IN_QUICK_SEARCH' => filter_input(INPUT_POST, 'quick_search', FILTER_VALIDATE_BOOLEAN),
                    'FIELDS' => json_decode(stripslashes($_REQUEST['form_fields']), true)
                );

                $res = $this->core->action->searchFields($arr, $_REQUEST['action']);

                if ($this->core->content_type === 2) {
                    $ret = array('result' => (bool) $res);
                    if (is_int($res) && isset($_REQUEST['parent'])) {
                        $ret['id'] = $res;
                    }
                    //$ret['messages'] = $this->core->get_messages(true);
                } else {
                    header('Location: index.php?m=124');
                    exit;
                }
                break;
            // Trinimas
            case 'delete':
                $res = $this->core->action->searchFields((int) $_POST['id'], $_REQUEST['action']);
                $ret = array(
                    'result' => (bool) $res,
                    'ids' => $res);
                break;
            //case 'get_fields':
            //	break;
            /*
              // Laukų redagavimas
              case 'fields':
              $arr['ID'] = $_POST['item'];
              $type = 'ACT';
              $res = $this->extra_fields_html($type, $arr);
              $ret = array(
              'result' => (bool) $res,
              'html' => $res,
              'data' => $this->core->logic->extra_fields_info($type, $arr));
              break;
              // Laukų saugiojimas
              case 'savefields':
              $type = 'ACT';
              if (isset($_GET['item']) === TRUE)
              $_POST['item'] = (int) $_GET['item'];
              $res = $this->core->logic->extra_fields_save($type, $_POST);
              $ret = array(
              'result' => (bool) $res,
              'message' => $res);
              break;
              // Paieška
              case 'search':
              $arr = array();
              foreach ($_POST as $key => $val) {
              switch ($key) {
              case 'name':
              $arr['LABEL'] = $val;
              break;
              }
              }

              $res = $this->core->action->act($arr, 'search');
              // Jei neranda tėvo, tai pameta įrašą. Todėl hierarchiškai rikiuojam tik tada, kai gražinami visi rezultatai
              if (empty($arr['LABEL']) === TRUE)
              $res = $this->core->logic->sort_children_to_parent($res, 'ID', 'PARENT_ID');
              $ret = array(
              'result' => (bool) $res,
              'data' => $res);
              break; */
        }

        return json_encode($ret);
    }

    public function get_document_person_free_time()
    {
        $data = array(
            'RESULT_COUNT' => 5
        );
        $data['RECORD'] = $this->core->logic2->get_record_relation($_POST['record_nr']);


        $parsed_date = date_parse($_POST['date']);
        if ($parsed_date['error_count'] === 0) {
            $data['DATETIME'] = $_POST['date'];

            $parsed_time = date_parse($_POST['time']);
            if ($parsed_time['error_count'] === 0) {
                $data['DATETIME'] .= ' ' . $_POST['time'];
            }
        }

        // TODO: RECORD_ID | Į $data['RECORD'] pateikti RECORD_RELATION.ID
        $result = $this->core->logic->get_document_person_free_time($data);

        // Iš rezultatų pašalina sekundes
        foreach ($result as &$res) {
            foreach ($res as &$r) {
                $r['TIME'] = date('H:i', strtotime($r['TIME']));
            }
        }

        return json_encode($result);
    }

    /**
     * Sugeneruoja baustumo pažeidimų lentelę
     * @param array $_POST
     * @param mixed $_POST['record']  Tai ką valgo <b>\Atp\Logic2::get_record_relation</b>
     * @param int $_POST['code'] Juridinio / Fizinio asmens kodas
     * @return string JSON objektas
     */
    public function get_punishment_records_list()
    {
        $result = array();

        $data = array(
            'RECORD' => $this->core->logic2->get_record_relation($_POST['record']),
            'CODE' => $_POST['code']
        );
        if (preg_match('/^[0-9]+$/', $data['CODE']) && empty($data['RECORD']) === false) {
            $punishment = new \Atp\Document\Record\Punishment($this->core);

            $clause = $this->core->logic2->get_record_clause($data['RECORD']);
            $param = array(
                // Tikrinamas dokumento įrašas
                'RECORD' => $data['RECORD']['ID'],
                // T.d.į. asmens kodas
                'CODE' => $data['CODE'],
                // T.d.į. straipsnis
                'CLAUSE' => empty($clause) ? null : $clause['ID'],
                // T.d.į. automobilio numeris
                'AUTO_NR' => $this->core->logic2->get_record_auto_nr($data['RECORD']['ID'])
            );


//            // Dokumentų įrašų numeriai, kuriuose neieškoti baustumo
//            $exclude_records = $this->core->logic->get_record_path($data['RECORD']);
//            array_walk($exclude_records, function (&$val) {
//                $val = $val['RECORD_ID'];
//            });
//            $exclude_records = array_values($exclude_records);


            $records = array();

//            // Galiojantis įrašai
//            $get_valid_records_method = $punishment->getValidRecordMethod($data['RECORD']['ID']);
//            $records[0] = $punishment->getRecordsByType($param, 'P' . $get_valid_records_method, $exclude_records);
//            foreach ($records[0] as &$record_ref) {
//                $record_ref = $this->core->logic2->get_record_relation($record_ref);
//                $exclude_records[] = $record_ref['RECORD_ID'];
//            }
//            $exclude_records = array_unique($exclude_records);
//
//            // Susiję įrašai
//            $records[1] = $punishment->getRecordsByType($param, 'P' . 'Related', $exclude_records);
//            foreach ($records[1] as &$record_ref) {
//                $record_ref = $this->core->logic2->get_record_relation($record_ref);
//                $exclude_records[] = $record_ref['RECORD_ID'];
//            }
//            $exclude_records = array_unique($exclude_records);
//
//            // Istorijos įrašai
//            $records[2] = $punishment->getRecordsByType($param, 'P' . 'History', $exclude_records);
//            foreach ($records[2] as &$record_ref) {
//                $record_ref = $this->core->logic2->get_record_relation($record_ref);
//            }

            // ATPEIR įrašai
            $records[3] = [];
            $atprMessage = null;
            $atpeirList = [];
            /* @var $atpeirList \Atpr\Kis\AtpDetailsType */
            try {
                $atpeirList = $punishment->getRecordsByType($param, 'P' . 'Atpeir');
                if (count($atpeirList)) {
                    $atprMessage = '<span style="color: red;">Pagal ATPR duomenis asmuo baustas</span>';
                    foreach ($atpeirList as $atpr) {
                        $records[3][] = $atpr;
                    }
                } else {
                    $atprMessage = '<span style="color: green;">Pagal ATPR duomenis asmuo nėra baustas</span>';
                }
            } catch (\Exception $e) {
                error_log($e);
//                $atprMessage = $e->getMessage();
                $atprMessage = 'Negalima nustatyti baustumo';
            }

            $labels = array(
                'Atpeir' => 'ATPR pažeidimai',
//                $get_valid_records_method => 'Galiojantys pažeidimai',
//                'Related' => 'Susiję pažeidimai',
//                'History' => 'Pažeidimų įstorija'
            );
            $typeRecordMap = array(
                'Atpeir' => $records[3],
//                $get_valid_records_method => $records[0],
//                'Related' => $records[1],
//                'History' => $records[2],
            );

            $typeRecordMapForSaving = $typeRecordMap;
            foreach ($typeRecordMapForSaving['Atpeir'] as &$record) {
                $record = $record->getRoik();
            }
            
            $punishment->deleteRecords($data['RECORD']['ID']);
            $punishment->saveRecords($data['RECORD']['ID'], $typeRecordMapForSaving);


            $html = '';
            $tmpl = $this->core->factory->Template();
            $documents = $this->core->logic2->get_document();

            $tmpl->handle = 'penalty_table_file';
            $tmpl->set_file($tmpl->handle, 'penalty_table.tpl');

            $handles = $tmpl->set_multiblock($tmpl->handle, array(
                'header_block',
                'row_block',
                'empty_row_block',
                'table_block',
                'atpr_block',
                'person_block',
                )
            );


            $tmpl->set_var('personData', 'a.k. ' . $data['CODE']);
            $tmpl->parse($handles['person_block'], 'person_block');
//
//            $tmpl->set_var('resultText', $atprMessage);
//            $tmpl->parse($handles['atpr_block'], 'atpr_block');
            $tmpl->clean($handles['atpr_block']);


            // Parsinamos lentelės
            $count = count($labels);

            $workerManager = new \Atp\Record\Worker($this->core);
            foreach ($labels as $type => $label) {
                if (count($typeRecordMap[$type]) === 0) {
//                    if ($type === 'Atpeir') {
                        $message = $atprMessage;
//                    } else {
//                        $message = 'Baustumo įrašų nerasta';
//                    }
                    //$tmpl->clean($handles['header_block']);
                    $tmpl->clean($handles['row_block']);
                    $tmpl->set_var('message', $message);
                    $tmpl->parse($handles['empty_row_block'], 'empty_row_block');
                } else {
//                    if ($type !== 'Atpeir') {
//                        $typeRecordMap[$type] = sort_by($typeRecordMap[$type], 'CREATE_DATE', 'asc');
//                    } else {
                        $tmpl->set_var('message', $atprMessage);
                        $tmpl->parse($handles['empty_row_block'], 'empty_row_block');
//                    }

                    $wrapAnchor = function($string, $url) {
                        return '<a href="' . $url . '" target="_blank">' . $string . '</a>';
                    };

                    $j = 0;
                    foreach ($typeRecordMap[$type] as $record) {
//                        if ($type === 'Atpeir') {
                            /* @var $record \Atpr\Kis\AtpDetailsType */
                            $arr = array(
                                'url' => 'javascript:void(0)',
                                'record_id' => '-',
                                'table' => '-',
                                'clause' => '-',
                                'person' => '-',
                                'status' => '-',
                                'reg_date' => '-', // Dokumento įrašo registracijos data
                                'pazeidimo_data' => '-', // Pažeidimo datos laukas
                                'pazeidimo_laikas' => '-', // Pažeidimo laiko laukas
                                'reg_number' => '-', // Dokumento įrašo registracijos numeris
                                'penalty' => '-' // Baudos suma
                            );

                            $arr['record_id'] = $arr['reg_number'] = $record->getROIK();
                            if ($record->getPazeidimoDataLaikas()) {
                                $arr['pazeidimo_data'] = $record->getPazeidimoDataLaikas()->format('Y-m-d');
                                $arr['pazeidimo_laikas'] = $record->getPazeidimoDataLaikas()->format('H:i');
                            }
                            if ($record->getBauda()) {
                                $arr['penalty'] = $record->getBauda()->getBaudosDydis();
                            }
                            if ($record->getStraipsniai() && $record->getStraipsniai()->getStraipsnis()) {
                                $clauses = array_map(function (\Atpr\Kis\StraipsnisType $straipsnis) {
                                    $straipsnioPavadinimas = $straipsnis->getStraipsnis()->getClassifierEntryName();
                                    $daliesPavadinimas = null;
                                    if ($straipsnis->getStraipsnioDalis()) {
                                        $daliesPavadinimas = $straipsnis->getStraipsnioDalis()->getClassifierEntryName();
                                    }
                                    return join(' ', array_filter([$straipsnioPavadinimas,
                                        $daliesPavadinimas]));
                                }, $record->getStraipsniai()->getStraipsnis());
                                $arr['clause'] = join(', ', $clauses);
                            }

                            if ($record->getPareigunasNustatesATP()) {
                                $pareigunas = $record->getPareigunasNustatesATP();
                                $arr['person'] = join(' ', array_filter([
                                    $pareigunas->getVardas(),
                                    $pareigunas->getPavarde()
                                ]));
                            }
//                        } else {
//                            $record = $this->core->logic2->get_record_relation($record);
//                            $document = &$documents[$record['TABLE_TO_ID']];
//
//                            $url = 'index.php?m=5&id=' . $record['ID'];
//
//                            $arr = array(
//                                'url' => $url,
//                                'record_id' => $wrapAnchor($record['ID'], $url),
//                                'reg_number' => $wrapAnchor('-', $url),
//                                'reg_date' => $wrapAnchor('-', $url),
//                                'pazeidimo_data' => $wrapAnchor('-', $url),
//                                'table' => $wrapAnchor($document['LABEL'], $url),
//                                'penalty' => $wrapAnchor('-', $url),
//                                'clause' => $wrapAnchor('-', $url),
//                                'person' => $wrapAnchor('-', $url),
//                                'status' => $wrapAnchor('-', $url),
//                            );
//
//
//                            // Surašęs pareigūnas
//                            $worker = $this->core->factory->Record()->getWorker($record['ID']);
//
//                            $arr['person'] = $wrapAnchor(join(' ', array_filter([$worker['FIRST_NAME'],
//                                $worker['LAST_NAME']])), $url);
//
//                            // Paskutinio dokumento įrašo kelyje būklė
//                            $last_record = $this->core->logic->record_path_last($record['ID']);
//                            $last_status = $this->core->logic2->get_status(array(
//                                'ID' => $last_record['DOC_STATUS']));
//                            if (empty($last_status) === false) {
//                                $arr['status'] = $wrapAnchor($last_status['LABEL'], $url);
//                            }
//
//
//                            // Pagal nustatytus laukus
//                            $option_fields = array(
//                                'reg_date' => 'avilys_date', // Dokumento įrašo registracijos data
//                                'pazeidimo_data' => 'violation_date', // Pažeidimo datos laukas
//                                'pazeidimo_laikas' => 'violation_time', // Pažeidimo laiko laukas
//                                'reg_number' => 'avilys_number', // Dokumento įrašo registracijos numeris
//                                'penalty' => 'penalty_sum' // Baudos suma
//                            );
//                            foreach ($option_fields as $key => &$configType) {
//
//                                $configuredFieldId = $this->core->factory->Document()->getOptions($document['ID'])->$configType;
//                                if (empty($configuredFieldId) === false) {
//                                    $data['VALUE'] = $this->core->factory->Record()->getFieldValue($record['ID'], $configuredFieldId);
//
//                                    if ($data['VALUE'] !== null && $data['VALUE'] !== "") {
//                                        $arr[$key] = $wrapAnchor($data['VALUE'], $url);
//                                    }
//                                }
//                            }
//
//                            if (isset($arr['pazeidimo_laikas'])) {
//                                $timestamp = strtotime($arr['pazeidimo_laikas']);
//                                if ($timestamp > 0) {
//                                    $arr['pazeidimo_laikas'] = $wrapAnchor(date('H:i', $timestamp), $url);
//                                }
//                            }
//
//                            $clause = $this->core->logic2->get_record_clause($record['ID']);
//                            if (empty($clause) === false) {
//                                $arr['clause'] = $wrapAnchor($clause['NAME'], $url);
//                            }
//                        }

                        $tmpl->set_var($arr);

//                        //$tmpl->set_var($handles['empty_row_block'], '');
//                        if ($type !== 'Atpeir') {
//                            $tmpl->clean($handles['empty_row_block']);
//                        }
                        $tmpl->parse($handles['row_block'], 'row_block', $j);

                        $j++;
                    }
                }

                $tmpl->parse($handles['header_block'], 'header_block');

                $tmpl->set_var('table_label', $label);
                $tmpl->parse($handles['table_block'], 'table_block', true);
            }



            $tmpl->parse($tmpl->handle . '_out', $tmpl->handle);
            $output = $tmpl->get($tmpl->handle . '_out');
            $html .= $this->core->atp_content($output, 2);
            $html = '<div id="atp-column-ws-1158" class="atp-column-ws-fields punishment" style="display: block;">' . $html . '</div>';

            $result = array('html' => $html);

            $punishment->setCheckStatus($param['RECORD'], true);
            $this->core->logic->update_record_status($param['RECORD']);
        }

        return json_encode($result);
    }

    /**
     * @deprecated
     */
    public function template_form()
    {/*
      $data = array(
      'ID' => $_GET['template'],
      'TYPE' => $_GET['type'],
      'ITEM' => $_GET['item']
      );
      $data = array_filter($data);

      $count = count($data);
      if (empty($data['ID'])) {
      return '{}';
      }

      $templateManager = new \Atp_Document_Templates_Template($this->core, $data['ID']);
      $template = $templateManager->get();

      // Pasirinktas s1lygos tipas, gražina
      if ($count === 2) {
      if (isset($data['TYPE'])) {
      // Sąlygos tipas
      switch ($data['TYPE']) {
      // Klasifikatoriaus
      case 'CLASS':
      $all_fields = array();
      $select_hierarchy = $this->core->manage->tables_relations_parent_fields($template->document_id, 'DOC', $data = array(), $all_fields, $null = null);
      $data = array();
      $select_hierarchy = array_reverse($select_hierarchy, true);
      $select_hierarchy[0]['OPT'] = '-- Klasifikatoriaus laukas --';
      $data['select_hierarchy'] = array_reverse($select_hierarchy, true);
      $all_fields[0] = array('ID' => 0, 'LABEL' => '-- Klasifikatoriaus laukas --');
      $data['all_fields'] = &$all_fields;
      $this->core->manage->printTree($data['select_hierarchy'], 0, $data['all_fields'], $valid_fields = false, $selected = null, $options);

      $matches = null;
      $returnValue = preg_match_all('#<option([^>]*)>([^</]*)</?option>#i', $options, $matches);

      $result = array();
      if ($returnValue) {
      $result['options'] = array();
      for ($i = 0; $i < $returnValue; $i++) {
      $ref = &$result['options'][$i];
      $attributes_ref = &$matches[1][$i];

      preg_match('#value=["\']?([^"\']*)#i', $attributes_ref, $m);
      $field = $this->core->logic2->get_fields(array('ID' => $m[1]));

      $ref['value'] = $m[1];

      preg_match('#disabled=["\']?([^"\']*)#i', $attributes_ref, $m);
      $ref['disabled'] = $m[1];

      $ref['label'] = $matches[2][$i];

      if ($field['TYPE'] !== '18' && $ref['value'] !== '0') {
      $ref['disabled'] = 'disabled';
      }
      if ($ref['value'] === '0') {
      $ref['value'] = '';
      }
      }
      }

      break;
      // Straipsnis
      case 'CLAUSE':
      $items = $this->core->logic2->get_clause(null, null, 'NAME', 'ID, NAME');

      $result = array();

      $i = 0;
      $result['options'][$i++] = array(
      'value' => '',
      'label' => '-- ' . 'Straipsnis' . ' --'
      );

      foreach ($items as &$item) {
      $result['options'][$i++] = array(
      'value' => $item['ID'],
      'label' => $item['NAME']
      );
      }
      break;
      // Veiksmas
      case 'ACTION':
      $obj = new \Atp_Document_Templates_Template_Condition_Action();
      $items = $obj->get_actions();

      $result = array();

      $i = 0;
      $result['options'][$i++] = array(
      'value' => '',
      'label' => '-- ' . 'Veiksmo tipas' . ' --'
      );

      foreach ($items as &$item) {
      $result['options'][$i++] = array(
      'value' => $item['ID'],
      'label' => $item['NAME']
      );
      }
      break;
      }

      echo json_encode($result);
      exit;
      }
      } else
      if ($count === 3) {
      if (isset($data['TYPE']) && isset($data['ITEM'])) {
      // Sąlygos tipas
      switch ($data['TYPE']) {
      // Klasifikatoriaus
      case 'CLASS':
      $field = $this->core->logic2->get_fields(array('ID' => $data['ITEM']));
      $items = $this->core->logic2->get_classificator(array('PARENT_ID' => $field['SOURCE']));

      $result = array();

      $i = 0;
      $result['options'][$i++] = array(
      'value' => '',
      'label' => '-- ' . 'Klasifikatorius' . ' --'
      );

      foreach ($items as &$item) {
      $result['options'][$i++] = array(
      'value' => $item['ID'],
      'label' => $item['LABEL']
      );
      }
      break;
      // Straipsnis
      case 'CLAUSE':
      $result = array();
      break;
      // Veiksmas
      case 'ACTION':
      $result = array();
      break;
      }

      echo json_encode($result);
      exit;
      }
      } */
    }

    /**
     * Egzistuojančio dokumento įrašo paprastas saugojimas
     * @param php://input Request query
     * @return string
     */
    public function save_record()
    {
        $raw_data = file_get_contents('php://input');
        parse_str($raw_data, $data);
        if ($data === null) {
            return json_encode(array('result' => false));
        }

        if (empty($data['record_id']) === true) {
            return json_encode(array('result' => false));
        }

        $record = $this->core->logic2->get_record_relation($data['record_id']);
        if (empty($record) === true) {
            trigger_error('Record not found', E_USER_ERROR);
        }

        // Jei įrašas importuotas iš VMSA, nevikdyti automatinio saugojimo
        if ($this->core->logic2->is_VMSA_record((int) $record['ID']) === true) {
            return json_encode(array('result' => false));
        }

        $privileges_data = array(
            'user_id' => $this->core->factory->User()->getLoggedUser()->person->id,
            'department_id' => $this->core->factory->User()->getLoggedPersonDuty()->department->id,
            'record_id' => $record['ID'],
            'table_id' => $record['TABLE_TO_ID']
        );
        // Vartotojas privalo turėti teisė į redagavimą
        if ($this->core->logic->privileges('table_record_edit', $privileges_data) === false) {
            return json_encode(array('result' => false));
        }

        $this->core->logic->_r_r_id = $record['ID'];
        $this->core->logic->_r_table_id = $this->core->logic->_r_table_from_id = $this->core->logic->_r_table_to_id = $record['TABLE_TO_ID'];
//        $this->core->logic->_r_record_id = $record['RECORD_ID'];
        $this->core->logic->_r_record_status = $record['STATUS'];
        $this->core->logic->_r_values = $data;
        $new_record_id = $this->core->logic->create_record('save', ['disable_messages'], $record['RECORD_ID']);

        if (ctype_digit((string) $new_record_id) === true) {
            return json_encode(array('result' => true));
        }

        return json_encode(array('result' => false));
    }

    /**
     * Gauna dokumento šablonų ID ir pavadinimą, kurie atitinka prie šablono nurodytas sąlygas
     * @return string
     */
    public function get_valid_templates()
    {
        $raw_data = file_get_contents('php://input');
        parse_str($raw_data, $data);
        if ($data === null) {
            return json_encode(array('result' => false));
        }

        $templateManager = new \Atp_Document_Templates_Template($this->core);
        $recordManager = new \Atp\Document\Record($data['record'], $this->core);
        $templates = $templateManager->GetAvailable($recordManager);

        foreach ($templates as &$template) {
            $template = array(
                'value' => $template['ID'],
                'label' => $template['LABEL']
            );
        }

        return json_encode(array('result' => true, 'options' => array_values($templates)));
    }

    /**
     *
     */
    public function get_RN_code()
    {
        $RN = new \Atp_Post_RN($this->core);
        $RN->reserve_RN_code();
        $ret = $RN->get_code();

        return json_encode($ret);
    }

    /**
     *
     */
    public function free_RN_code()
    {
        $RN = new \Atp_Post_RN($this->core);
        //$RN->reserve_RN_code();
        $ret = $RN->free_RN_code();

        return json_encode($ret);
    }
}
