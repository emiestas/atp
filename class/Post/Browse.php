<?php

/**
 * Gauti informacijai apie dokumento šabloną
 */
class Atp_Post_Browse
{
    /**
     * Šablono informacija
     * @var object
     */
    public $template;

    /**
     * puslapio htmlas
     * @var string
     */
    public $page;

    /**
     * @var \Atp\Core
     */
    private $core;

    /**
     *
     * @param \Atp\Core $db
     * @param int $id [optional]
     */
    public function __construct(\Atp\Core &$core, $id = 1)
    {
        $this->core = & $core;
        $this->preparePage($id);
    }

    /**
     * Set'ina šabloną
     * @param int $id puslapio ID
     * @return boolean
     */
    private function preparePage($id)
    {
        if (!$this->core->factory->User()->getLoggedPersonDutyStatus('A_TABLE')) {
            return $this->core->printForbidden();
        } elseif (isset($_POST['submit']) === true) {
            $imported = $this->saveData();
            $message = 'Duomenys sėkmingai importuoti, įkėlta ' . $imported . ' naujų numerių.<br><br>';
        }

        $this->set($id);

        $tmpl = $this->core->factory->Template();
        $tmpl_handler = 'container'; // $this->template->handler;
        //$tmpl->set_file($tmpl_handler, $this->template->filename);
        $tmpl->set_file($tmpl_handler, 'container.tpl');

        $obj = new $this->template->class($this->core, $this->template);
        $output2 = $obj->getHtml();

        $tmpl->set_var(array(
            'title' => $this->template->label,
            'middle' => $message . $output2
        ));
        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');
        $this->page = $output;
        return true;
    }

    /**
     * Patikrina ar dokumento įrašas tenkina set'into dokumento šabloną sąlygas
     * @param \Atp\Document\Record $recordManager Dokumento įrašo duomenys
     * @return boolen
     */
    private function saveData()
    {
        $result = true;
        $post = $_POST;
        //'import_new_rns'
        switch ($post['action']) {
            case 'import_new_rns':
                if ($_FILES["filename"]['error'] == 0 && is_file($_FILES["filename"]['tmp_name']) === true) {
                    $RN_array_file = file_get_contents($_FILES["filename"]['tmp_name']);
                    $RN_array = explode("\n", $RN_array_file);
                    $i = 0;
                    $j = 0;
                    $values = array();
                    $limit = count($RN_array);
                    $count_before = $this->core->db_next($this->core->db_query_fast('SELECT COUNT(ID) as TOTAL FROM ' . PREFIX . 'ATP_POST_RN_REGISTRY'));
                    foreach ($RN_array as &$RN) {
                        $j++;
                        $i++;
                        $values[] = "('" . $RN . "')";
                        if ($i === 10000 || $j === $limit) {
                            $this->core->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_POST_RN_REGISTRY
						(`RN`) VALUES ' . join(',' . PHP_EOL, $values));
                            $values = array();
                            $i = 0;
                        }
                    }
                    $count_after = $this->core->db_next($this->core->db_query_fast('SELECT COUNT(ID) as TOTAL FROM ' . PREFIX . 'ATP_POST_RN_REGISTRY'));
                    $result = $count_after['TOTAL'] - $count_before['TOTAL'];
                }
                break;
        }
        /*  if (empty($this->template)) {
          trigger_error('Template must be set by set(), get() or constructor', E_USER_ERROR);
          exit;
          }

          //$conditionManager = new \Atp_Document_Templates_Template_Condition($this, $this->core);
          //$conditions = $conditionManager->get_by_template();

          //$actionManager = new \Atp_Document_Templates_Template_Condition_Action($this, $this->core);

          $result = true;
          foreach ($conditions as &$condtition) {
          if(empty($condtition->action_id) === TRUE)
          continue;

          $actionManager->set($condtition->action_id);

          $parameters = $conditionManager->get_parameters($condtition->id);
          $result = $actionManager->check($parameters, $recordManager);
          if ($result === FALSE) {
          break;
          }
          }
         */
        return $result; //$result;
    }

    /**
     * Set'ina šabloną
     * @param int $id Šablono ID
     * @return boolean
     */
    private function set($id)
    {
        $available_templates = array(
            1 => array(
                'id' => 1,
                'label' => 'Paštas',
                'class' => 'Atp_Post_Browse_Form',
                'handler' => 'post_main',
                'filename' => 'post_main.tpl'
            ),
            2 => array(
                'id' => 2,
                'label' => 'RN numerio informacija',
                'class' => 'Atp_Post_Browse_Form',
                'handler' => 'post_rn_info',
                'filename' => 'post_rn_info.tpl',
                'handler' => 'post_main',
                'filename' => 'post_main.tpl'
            ),
            3 => array(
                'id' => 3,
                'label' => 'RN numerių importavimas',
                'class' => 'Atp_Post_Browse_Import',
                'handler' => 'post_rn_import',
                'filename' => 'post_rn_import.tpl'
            )
        );

        if (empty($available_templates[$id]) === false) {
            $this->template = (object) array(
                    'id' => $available_templates[$id]['id'],
                    'class' => $available_templates[$id]['class'],
                    'label' => $available_templates[$id]['label'],
                    'handler' => $available_templates[$id]['handler'],
                    'filename' => $available_templates[$id]['filename']);
            return true;
        }

        return false;
    }

    /**
     * Gauna set'intą šabloną
     * @param int $id [optional] Šablono ID
     * @return object
     */
    public function get($id = null)
    {
        if ($id !== null) {
            $this->set($id);
        }

        return $this->template;
    }

    /**
     * Gauna šablonus, kurie atitinka šablono sąlygas
     * @param \Atp\Document\Record $recordManager Dokumento įrašo duomenys
     * @return array
     */
    public function printPage()
    {
        return $this->core->atp_content($this->page); // $templates;
    }
}
