<?php

/**
 * Šablono informacijos forma (Dokumentų šablonai)
 */
class Atp_Post_Browse_Import
{
    /**
     * Šablono informacija
     * @var Atp_Document_Templates_Template_Info
     */
    private $post_template;

    /**
     * Šablono informacija
     * @var Atp_Document_Templates_Template_Info
     */
    private $html;

    /**
     *
     * @var \Atp\Core
     */
    private $core;

    /**
     *
     * @param Atp_Document_Templates_Template $template
     * @param \Atp\Core $core
     */
    public function __construct(\Atp\Core &$core, $template)
    {
        $this->post_template = &$template;
        $this->core = &$core;
        $this->prepareData();
    }

    /**
     * Klases sugeneruoto HTML'o grazinimas ()
     * @return string HTML
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * Šablono informacijos forma (Dokumentų šablonai)
     * @return string HTML
     */
    public function prepareData()
    {
        $result = '';

        $tmpl = $this->core->factory->Template();
        $tmlp_hanlder = $this->post_template->handler; //'post_main';
        $tmpl->set_file($tmlp_hanlder, $this->post_template->filename);
        $tmpl->set_var(array(
            'form' => '<form action="index.php?m=140" method="post"  enctype="multipart/form-data">
                            Duomenų failas:<br>
                            <input type="file" name="filename" ><br>
                            <input type="hidden" name="action" value="import_new_rns">
                            <input type="submit" name="submit" value="Įkelti">
                        </form>'
            )
        );
        $tmpl->parse($tmlp_hanlder . '_out', $tmlp_hanlder);
        $this->html = $tmpl->get($tmlp_hanlder . '_out');

        return true;
    }

    /**
     *
     * @return string
     */
    public function printAjax()
    {
        $rq = array(
            'template_id' => $_POST['template'],
            'condition_id' => $_POST['condition'],
            'action_id' => $_POST['action'],
            'parameters' => $_POST['parameters']
        );

        $parameters_values = array();
        if (count($rq['parameters'])) {
            foreach ($rq['parameters'] as $key => &$val) {
                $parameters_values[] = (object) array(
                        'id' => $key,
                        'type' => 'IS',
                        'value' => $val,
                );
            }
        }

        // Šablono informacija
        $template = $this->template->get();

        // Sąlygos informacija
        $conditionManager = new \Atp_Document_Templates_Template_Condition($this->template, $this->core);
        $condition = $conditionManager->get($rq['condition_id']);

        if ($condition !== false) {
            // Priskiriamas veiksmas
            $actionsManager = new \Atp_Document_Templates_Template_Condition_Action($this->template, $this->core);
            $result = $actionsManager->set($rq['action_id']);

            if ($result !== false) {
                // Veiksmo parametrai
                $parameters = $actionsManager->get_parameters();
                $actionsManager->set_values($parameters_values);


                $tmpl = $this->core->factory->Template();
                $tmpl->handle = 'documents_template_form_file';
                $tmpl->set_file($tmpl->handle, 'documents_template_form.tpl');


                // Parametro blokas
                $handles = $tmpl->set_multiblock($tmpl->handle, array('html_block'));


                // Sąlygos veiksmo parametrų laukai
                $append = false;
                foreach ($parameters as &$parameter) {
                    $no_need = false;
                    foreach ($parameters_values as $parameter_value) {
                        if ($parameter_value->id === $parameter['ID']) {
                            $no_need = true;
                            break;
                        }
                    }
                    if ($no_need === true) {
                        continue;
                    }

                    $html = $actionsManager->parse_parameter_field($parameter['ID'], $condition->id);

                    $tmpl->set_var('html', $html);
                    $tmpl->parse($handles['html_block'], 'html_block', $append);
                    $append = true;
                }

                $output = $tmpl->get($handles['html_block']);

                $output = $this->core->atp_content(trim($output));
            }
        } else {
            $new_id = $this->saveChanges($rq, $this->template);
            return json_encode(array('new_id' => $new_id));
        }

        $this->saveChanges($rq, $this->template, $conditionManager, $actionsManager);

        return json_encode(array('html' => $output));
    }

    /**
     *
     * @param array $rq
     * @param Atp_Document_Templates_Template $template
     * @param Atp_Document_Templates_Template_Condition $conditionManager [optional]
     * @param Atp_Document_Templates_Template_Condition_Action $actionsManager [optional]
     */
    private function saveChanges($rq, Atp_Document_Templates_Template &$template, Atp_Document_Templates_Template_Condition &$conditionManager = null, Atp_Document_Templates_Template_Condition_Action &$actionsManager = null)
    {

        // Šablono informacija
        $template = $template->get();

        // Jei tai nauja sąlyga, sukurti tuščią sąlygą
        if (empty($rq['condition_id']) === true) {
            $this->core->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_TMPL_CONDITION
						(`ACTION_ID`) VALUES (0)');
            $new_id = $this->core->db_last_id();
            $arg = array(
                'TMPL_ID' => $template->id,
                'CONDITION_ID' => $new_id
            );
            $this->core->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_TMPL_X_TMPL_CONDITION
						(`TMPL_ID`, `CONDITION_ID`) VALUES (:TMPL_ID, :CONDITION_ID)', $arg);

            return $new_id;
        } else {

            // Sąlygos informacija
            $condition = $conditionManager->get($rq['condition_id']);


            // Jei keičiasi veiksmas
            if ((int) $condition->action_id != (int) $rq['action_id']) {
                $arg = array(
                    'CONDITION_ID' => $condition->id,
                    'ACTION_ID' => $rq['action_id']
                );

                // Pašalinami seni parametrai
                $this->core->db_query_fast('DELETE A, B
				FROM
					' . PREFIX . 'ATP_TMPL_CONDITION_X_CONDITION_PRAM A
						LEFT JOIN ' . PREFIX . 'ATP_TMPL_CONDITION_PARAM B ON B.ID = A.PARAMETER_ID
				WHERE
					A.CONDITION_ID = :CONDITION_ID', $arg);

                // Atnaujiamas veiksmo ID
                $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_TMPL_CONDITION
				SET ACTION_ID = :ACTION_ID
				WHERE
					ID = :CONDITION_ID', $arg);
            } else {
                $arg = array(
                    'CONDITION_ID' => $condition->id
                );

                // Pašalinami seni parametrai
                $this->core->db_query_fast('DELETE A, B
				FROM
					' . PREFIX . 'ATP_TMPL_CONDITION_X_CONDITION_PRAM A
						LEFT JOIN ' . PREFIX . 'ATP_TMPL_CONDITION_PARAM B ON B.ID = A.PARAMETER_ID
				WHERE
					A.CONDITION_ID = :CONDITION_ID', array('CONDITION_ID' => $condition->id));

                // Sukuriami nauji parametrai
                $new_ids = array();
                foreach ($actionsManager->values[$rq['action_id']] as &$value) {
                    $this->core->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_TMPL_CONDITION_PARAM
						(`COMPARISON_TYPE`, `ACTION_PARAMETER_ID`, `VALUE`)
					VALUES
						(:COMPARISON_TYPE, :ACTION_PARAMETER_ID, :VALUE)', array(
                        'COMPARISON_TYPE' => $value->type,
                        'ACTION_PARAMETER_ID' => $value->id,
                        'VALUE' => $value->value
                    ));
                    $new_ids[] = $this->core->db_last_id();
                }

                // Sukuriami naujų parametrų ryšiai
                if (count($new_ids)) {
                    $values = array();
                    foreach ($new_ids as &$new_id) {
                        $values[] = '(' . (int) $condition->id . ', ' . (int) $new_id . ')';
                    }
                    $this->core->db_query_fast('INSERT INTO ' . PREFIX . 'ATP_TMPL_CONDITION_X_CONDITION_PRAM
						(`CONDITION_ID`, `PARAMETER_ID`)
					VALUES ' . join(',', $values));
                }
            }
        }
    }

    public function saveService()
    {
        $rq = array(
            'service_id' => $_POST['service']
        );

        // Šablono informacija
        $template = $this->template->get();

        $qry = 'UPDATE ' . PREFIX . 'ATP_TABLE_TEMPLATE SET SERVICE_ID = :SERVICE_ID WHERE ID = :ID';
        $this->core->db_query_fast($qry, array('ID' => $template->id, 'SERVICE_ID' => $rq['service_id']));
        return true;
    }
}
