<?php

/**
 * Gauti informacijai apie dokumento šabloną
 */
class Atp_Post_RN
{
    /**
     * RN KODAS
     * @var object
     */
    private $code;

    private $storage;

    /**
     * puslapio htmlas
     * @var string
     */
    public $data = array(
        'RN' => '',
        'NAME' => '',
        'SURNAME' => '',
        'COMPANY' => '',
        'MUNICIPALITY' => '',
        'CITY' => '',
        'STREET' => '',
        'HOUSE_NO' => '',
        'FLAT_NO' => '',
        'POST_CODE' => '',
        'ACQ_CONFIRMATION' => '',
        'DOC_ID' => '',
        'RESERVATION_DATE' => '',
        'RESERVED_FOR_STRUCTURE' => ''
    );

    /**
     * @var \Atp\Core
     */
    private $core;

    /**
     *
     * @param \Atp\Core $db
     * @param int $id [optional]
     */
    public function __construct(\Atp\Core &$core)
    {
        $this->core = & $core;

        $this->storage = $this->core->config['REAL_URL'] . 'files/tmp/barcodes/';
        if (is_dir($this->storage) === false) {
            $old_mask = umask(0);
            mkdir($this->storage, 0777, true);
            umask($old_mask);
        }

        if (empty($_POST['record']) == false) {
            $this->prepareCode();
        }
    }

    /**
     * Set'ina šabloną
     * @param int $id puslapio ID
     * @return boolean
     */
    private function prepareCode()
    {
        $record_id = (int) $_POST['record'];
        $query = 'SELECT * FROM ' . PREFIX . 'ATP_POST_RN_REGISTRY WHERE DOC_ID=' . $record_id;
        $res = $this->core->db_query_fast($query);
        $data = $this->core->db_next($res);
        if (!$data) {
            $field_values = $this->collect_record_data($record_id);
            if ((empty($field_values['PERSON_NAME']) && empty($field_values['COMPANY_NAME'])) || (empty($field_values['PERSON_ADDRESS']) && empty($field_values['COMPANY_ADDRESS']) && empty($field_values['OTHER_ADDRESS']))) {
                $this->data['POST_CODE'] = false;
                return false;
            }
            $this->data['ACQ_CONFIRMATION'] = $_POST['confirmation_needed'];
            $this->data['DOC_ID'] = $record_id;
            $this->data['RESERVED_FOR_STRUCTURE'] = $this->core->factory->User()->getLoggedPersonDuty()->department->id;
            $this->data['NAME'] = $field_values['PERSON_NAME'];
            $this->data['SURNAME'] = $field_values['PERSON_SURNAME'];
            $this->data['COMPANY'] = $field_values['COMPANY_NAME'];
            $post_code = $this->get_post_code($field_values);
        } else {
            $this->data = $data;
            $this->code = $data['RN'];
        }
        return true;
    }

    public function reserve_RN_code()
    {
        if (empty($this->code)) {
            if ($this->data['POST_CODE'] !== false && empty($this->code)) {
                $query = 'LOCK TABLES ' . PREFIX . 'ATP_POST_RN_REGISTRY READ';
                $res = $this->core->db_query_fast($query);
                $query = 'LOCK TABLES ' . PREFIX . 'ATP_POST_RN_REGISTRY WRITE';
                $res = $this->core->db_query_fast($query);
                $query = 'SELECT ID, RN FROM ' . PREFIX . 'ATP_POST_RN_REGISTRY WHERE MUNICIPALITY IS NULL AND RESERVED = 0 ORDER BY ID ASC LIMIT 1';
                $res = $this->core->db_query_fast($query);
                if ($res !== false) {
                    $ID = $this->core->db_next($res);
                    $this->data['RESERVATION_DATE'] = date('Y-m-d H:i:s');
                    $query = 'UPDATE ' . PREFIX . 'ATP_POST_RN_REGISTRY SET RESERVED = 1,'
                        . 'NAME=\'' . $this->data['NAME'] . '\','
                        . ' SURNAME=\'' . $this->data['SURNAME'] . '\','
                        . ' COMPANY=\'' . (empty($this->data['COMPANY']) === true ? '' : $this->data['COMPANY']) . '\','
                        . ' MUNICIPALITY=\'' . $this->data['MUNICIPALITY'] . '\','
                        . ' CITY=\'' . $this->data['CITY'] . '\','
                        . ' STREET=\'' . $this->data['STREET'] . '\','
                        . ' HOUSE_NO=\'' . $this->data['HOUSE_NO'] . '\','
                        . ' FLAT_NO=\'' . $this->data['FLAT_NO'] . '\','
                        . ' POST_CODE=\'' . $this->data['POST_CODE'] . '\','
                        . ' DOC_ID=\'' . $this->data['DOC_ID'] . '\','
                        . ' ACQ_CONFIRMATION=' . (int) $this->data['ACQ_CONFIRMATION'] . ','
                        . ' RESERVATION_DATE=\'' . $this->data['RESERVATION_DATE'] . '\','
                        . ' RESERVED_FOR_STRUCTURE=' . (int) $this->data['RESERVED_FOR_STRUCTURE'] . ''
                        . ' WHERE ID =' . $ID['ID'];
                    $res = $this->core->db_query_fast($query);
                    if ($res !== false) {
                        //jau rezervuotas rn, siam dokumentui
                        $query = 'UNLOCK TABLES';
                        $this->core->db_query_fast($query);
                        $this->code = $ID['RN'];
                        return true;
                    } else {
                        //nepavyko rezervuoti rn
                        $query = 'UNLOCK TABLES';
                        $this->core->db_query_fast($query);
                        return false;
                    }
                } else {
                    //nerastas laisvas RN
                    $query = 'UNLOCK TABLES';
                    $this->core->db_query_fast($query);
                    return false;
                }
                // = $RN;//$this->data;
            } elseif ($this->data['POST_CODE'] == false) {
                $this->code = false; //'Pašto kodas nerastas, negalima priskirti RN. Prašome įvesti "Kitą adresą", pagal nurodytą formuluotę.';//print_r($field_values,true);//'Klaida'; TODO grazinti ne duomenis
                return false;
            }
        }
    }

    private function get_post_code($field_values)
    {
        //ini_set('display_errors',1);
        if (empty($field_values['OTHER_ADDRESS']) === false) {
            //nurodytas "kitas adresas"
            //god knows how it will look...
            //suppose it will look like Person address from GRT (Vilnius, Tverečiaus 10-57)
            //if (trim($field_values['OTHER_ADDRESS']) === 'Vilniaus m. sav.') {
            //     return false;
            //
            $address = $field_values['OTHER_ADDRESS'];
            $address_arr = explode(',', $address);
            $this->data['CITY'] = $city = trim($address_arr[0]);
            $address = trim($address_arr[1]);
            unset($address_arr);
            $address_arr = explode(' ', $address);
            $house_flat = array_pop($address_arr);
            $street = implode(' ', $address_arr);
            unset($address_arr);
            $address_arr = explode('-', $house_flat);
            $this->data['HOUSE_NO'] = $house = $address_arr[0];
            $this->data['FLAT_NO'] = $flat = $address_arr[1];
            unset($address_arr);

            $ws = $this->core->logic2->get_ws();
            $ws->set_webservice('POST');
            // Į web-servisą paduodami parametrai
            $arr = array('SEARCH_LOCALITY' => $city, 'SEARCH_STREET' => $street,
                'SEARCH_HOUSE_NO' => $house);
            $ws->set_parameters($arr, 'POST');
            // Vykdomas web-servisas
            $ws->initialize();
            //Web-serviso rezultatas
            $response = json_decode($ws->get_result(), true);
            if (count($response['data']) > 1) {
                $this->data['POST_CODE'] = false;
                return false;
            } elseif (count($response['data']) == 1) {
                $this->data['POST_CODE'] = $response['data'][0]['POST_CODE'];
                $this->data['MUNICIPALITY'] = $response['data'][0]['MUNICIPALITY'];
                $this->data['STREET'] = $response['data'][0]['STREET'];
                return $this->data['POST_CODE'];
            } elseif (empty($response['data']) === true) {
                $this->data['POST_CODE'] = false;
                return false;
            }
            //$this->code = $this->data;
            // $ret = print_r($response,true);
            //Vilnius, Tverečiaus 10-57
            //Vilnius, P. Vileišio 9-44
        } elseif (empty($field_values['COMPANY_ADDRESS']) === false && empty($field_values['COMPANY_NAME']) === false) {
            $this->data['POST_CODE'] = false;
            return false;
            //TODO: Keist linksnius, Ieskot miesto.
        } elseif (empty($field_values['PERSON_ADDRESS']) === false && empty($field_values['PERSON_NAME']) === false && empty($field_values['PERSON_SURNAME']) === false) {
            //gavejas fizinis
            //$field_values['PERSON_ADDRESS'] = 'Vilnius, P. Vileišio 9-44';
            //isimtis vilniaus savivaldybei
            if (trim($field_values['PERSON_ADDRESS']) === 'Vilniaus m. sav.') {
                $this->data['POST_CODE'] = false;
                return false;
            }
            $address = $field_values['PERSON_ADDRESS'];
            $address_arr = explode(',', $address);
            $this->data['CITY'] = $city = trim($address_arr[0]);
            $address = trim($address_arr[1]);
            unset($address_arr);
            $address_arr = explode(' ', $address);
            $house_flat = array_pop($address_arr);
            $street = implode(' ', $address_arr);
            unset($address_arr);
            $address_arr = explode('-', $house_flat);
            $this->data['HOUSE_NO'] = $house = $address_arr[0];
            $this->data['FLAT_NO'] = $flat = $address_arr[1];
            unset($address_arr);

            $ws = $this->core->logic2->get_ws();
            $ws->set_webservice('POST');
            // Į web-servisą paduodami parametrai
            $arr = array('SEARCH_LOCALITY' => $city, 'SEARCH_STREET' => $street,
                'SEARCH_HOUSE_NO' => $house);
            $ws->set_parameters($arr, 'POST');
            // Vykdomas web-servisas
            $ws->initialize();
            //Web-serviso rezultatas
            $response = json_decode($ws->get_result(), true);
            if (count($response['data']) > 1) {
                $this->data['POST_CODE'] = false;
                return false;
            } elseif (count($response['data']) == 1) {
                $this->data['POST_CODE'] = $response['data'][0]['POST_CODE'];
                $this->data['MUNICIPALITY'] = $response['data'][0]['MUNICIPALITY'];
                $this->data['STREET'] = $response['data'][0]['STREET'];
                return $this->data['POST_CODE'];
            } elseif (empty($response['data']) === true) {
                $this->data['POST_CODE'] = false;
                return false;
            }
            //$this->code = $this->data;
            // $ret = print_r($response,true);
            //Vilnius, Tverečiaus 10-57
            //Vilnius, P. Vileišio 9-44
        } else {
            $this->data['POST_CODE'] = false;
            return false;
        }
    }

    /**
     * @param $record_id - dokumento ID
     * @return array - duomenys reikalingi RN registravimui
     */
    private function collect_record_data($record_id)
    {
        $field_values = array();

        $record = $this->core->logic2->get_record_relation($record_id);
        $document = $this->core->logic2->get_document(array('ID' => $record['TABLE_TO_ID']));
        $fields_arr = array('PERSON_ADDRESS', 'COMPANY_ADDRESS', 'OTHER_ADDRESS',
            'PERSON_NAME', 'PERSON_SURNAME', 'COMPANY_NAME');
        foreach ($fields_arr as $field_name) {
            $field_id = $this->core->factory->Document()->getOptions($document['ID'])->$field_name; //COMPANY_ADDRESS,OTHER_ADDRESS,PERSON_NAME,PERSON_SURNAME,COMPANY_NAME
//            $field = $this->core->logic2->get_fields(array('ID' => $field_id));
//            if (empty($field) === false) {
//                if ($field['ITYPE'] === 'DOC') {
//                    $record_table = $this->core->logic2->get_record_table($record['ID']);
//                    $field_value = $record_table[$field['NAME']];
//                } else {
//                    $field_value = $this->core->logic->get_extra_field_value($record['ID'], $field['ID']);
//                }
//            } else {
//                $field_value = null;
//            }

            $field_value = null;
            if($field_id) {
                $field_value = $this->core->factory->Record()->getFieldValue($record['ID'], $field_id);
            }
            
            $field_values[$field_name] = $field_value;
        }
        $field_values['ACQ_CONFIRMATION'] = $_POST['confirmation_needed'];

        return $field_values;
    }

    /**
     * patikrina pasto registre busena pagal RN
     * @return string - laisko busena
     */
    public function check_rn_state()
    {
        /*
          EVENT_CODE:
          EM*     siunčiama
         *
          EMI 	siunta įteikta
         *
          EMH 	siunta bandyta įteikti, bet nesėkmingai:
         *
         *
          emh reasons:
          10	Netikslus adresas
          18	Siunta pažeista
          11	Negalima nustatyti gavėjo buvimo vietos
          12	Gavėjas nerastas pristatymo metu
          13	Gavėjas atsisakė paimti siuntą
          14	Gavėjas paprašė pristatyti vėliau
          15	Gavėjas streikuoja
          16	Nesėkmingas pristatymas
          17	Siunta neteisingai nukreipta
          19	Draudžiamas siųsti daiktas – siunta nepristatyta
          20	Daiktų įvežimo apribojimai – siunta nepristatyta
          21	Mokamas mokestis
          22	Gavėjo nepareikalauta
          23	Gavėjas miręs
          24	Force Majeure – siunta nepristatyta
          25	Gavėjas paprašė leisti atsiimti pačiam
          26	Šventinė diena
          27	Siunta prarasta
          28	Gavėjas išsikėlęs kitur
          29	Gavėjas turi abonentinę pašto dėžutę
          30	Persiųsta gavėjo prašymu
          31	Persiųsta siuntėjo prašymu
          32	Persiųsta pristatymo pašto darbuotojo prašymu
          33	Persiųsta gavėjui atsiimti
          40	Siunta neatsiimta iš pristatymo pašto
          43	Pristatyta neteisingu adresu
          44	Persiuntimo adresas nežinomas
          46	Neatitinka muitinės reikalavimų
          99	Kita
          45	Šiam produktui nėra pristatymo paslaugos nurodytu adresu
         *
         *
          action_taken:
          A	Numatyta dar kartą bandyti pristatyti šiandien
          B	Bus bandoma pristatyti kitą darbo dieną
          C	Siunta sulaikyta, gavėjas įspėtas
          D	Susisiekta su siuntėju - laukiama atsakymo
          E	Siunta grąžinta siuntėjui
          F	Siunta persiųsta/peradresuota
          G	Siunta sulaikyta patikrai
          H	Siunta dėl jos turinio konfiskuota ar sunaikinta
          I	Susisiekta su siuntimo šalies paštu
          J	Gavėjui pasiūlyta atsiimti siuntą
          K	Sunaikinta siuntėjo pageidavimu
          L	Siunta sulaikyta gavėjo prašymu
          M	Tikrinamas gavėjo adresas


         */
        if (empty($this->code)) {
            return '';
        }
        $ws = $this->core->logic2->get_ws();
        $ws->set_webservice('POST_STATE');
        // Į web-servisą paduodami parametrai
        $arr = array('SEARCH_CODE' => $this->code);
        $ws->set_parameters($arr, 'POST');
        // Vykdomas web-servisas
        $ws->initialize();
        //Web-serviso rezultatas
        $response = json_decode($ws->get_result(), true);
        //var_dump($this->code,$ws->get_result());
        if (count($response['data']) > 1) {
            //$this->data['POST_CODE'] = false;
            return 'Klaida registruose';
        } elseif (count($response['data']) == 1 && in_array($response['data'][0]['EVENT_CODE'], array(
                'EMH', 'EMI'))) {
            if ($response['data'][0]['EVENT_CODE'] == 'EMI') {
                $person_signed = (empty($response['data'][0]['ITEM_SIGNATORY']) === false ? '. Priėmęs asmuo: ' . $response['data'][0]['ITEM_SIGNATORY'] : '');
                return 'Siunta įteikta. Data:&nbsp;' . date('Y-m-d', strtotime($response['data'][0]['EVENT_DATE'])) . $person_signed;
            } elseif ($response['data'][0]['EVENT_CODE'] == 'EMH') {
                $ret = 'Siunta neįteikta';
                if (empty($response['data'][0]['ACTION_DESCRIPTION']) === false) {
                    $ret .= '. ' . $response['data'][0]['ACTION_DESCRIPTION'];
                }
                if (empty($response['data'][0]['ACTION_DESCRIPTION']) === false) {
                    $ret .= '. ' . $response['data'][0]['REASON_DESCRIPTION'];
                }
                if ($response['data'][0]['ACTION_TAKEN'] == 'E') {
                    $ret = 'Siunta gražinta siuntėjui. Data:&nbsp;' . date('Y-m-d', strtotime($response['data'][0]['EVENT_DATE']));
                }
                return $ret;
            }
            return $response['data'][0]['REASON_DESCRIPTION'];
        } elseif (empty($response['data']) === true) {
            return 'Laiškas siunčiamas';
        }


        return 'Klaida registruose';
        /* if (count($response['data']) > 1) {
          $this->data['POST_CODE'] = false;
          return false;
          } elseif (count($response['data']) == 1) {
          $this->data['POST_CODE'] = $response['data'][0]['POST_CODE'];
          $this->data['MUNICIPALITY'] = $response['data'][0]['MUNICIPALITY'];
          $this->data['STREET'] = $response['data'][0]['STREET'];
          return $this->data['POST_CODE'];

          } elseif (empty($response['data']) === TRUE) {
          $this->data['POST_CODE'] = false;
          return false;
          } */
    }

    public function free_RN_code()
    {
        $query = 'UPDATE ' . PREFIX . 'ATP_POST_RN_REGISTRY SET RESERVED = 0,'
            . 'NAME=NULL,'
            . ' SURNAME=NULL,'
            . ' COMPANY=NULL,'
            . ' MUNICIPALITY=NULL,'
            . ' CITY=NULL,'
            . ' STREET=NULL,'
            . ' HOUSE_NO=NULL,'
            . ' FLAT_NO=NULL,'
            . ' POST_CODE=NULL,'
            . ' DOC_ID=0,'
            . ' ACQ_CONFIRMATION=NULL,'
            . ' RESERVATION_DATE=NULL,'
            . ' RESERVED_FOR_STRUCTURE=NULL'
            . ' WHERE RN =\'' . $this->code . '\'';
        $res = $this->core->db_query_fast($query);
        return $res;
    }

    public function get_code()
    {
        if (empty($this->code) === false) {
            file_put_contents($this->storage . $this->code . '.gif', file_get_contents($this->core->config['SITE_URL'] . 'class/Post/Barcode/gd.php?RN=' . $this->code));
            chmod($this->storage . $this->code . '.gif', 0777);
        }
        return $this->code;
    }

    /**
     * Funkcija surenkanti visus šia dieną skyriaus rezervuotų RN'ų duomenis
     * @param type $division_id - Skyriaus ID
     * @return type Array - Masyvas su visais lenteles duomenimis.
     */
    public function getDivisionReservedRNs($division_id)
    {
        $date_morning = date('Y-m-d') . ' 00:00:01';
        $date_evening = date('Y-m-d') . ' 23:59:59';

        $query = 'SELECT * FROM ' . PREFIX . 'ATP_POST_RN_REGISTRY WHERE RESERVED = 1
                   AND (RESERVATION_DATE >= \'' . $date_morning . '\' AND RESERVATION_DATE <= \'' . $date_evening . '\' )
                   AND RESERVED_FOR_STRUCTURE = ' . $division_id;

        $res = $this->core->db_query_fast($query);
        while ($row = $this->core->db_next($res)) {
            $ret[] = $row;
        }
        return $ret;
    }
}
