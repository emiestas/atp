<?php

namespace Atp;

use \Atp\Entity\Document as DocumentEntity;
use \Atp\Entity\FieldConfiguration;
use \Atp\Entity\FieldConfigurationType;
use \Atp\Core;
use \Doctrine\DBAL\Types\Type;

class Classificator
{
    private $atp;

    /**
     * @param Core $atp
     */
    public function __construct(Core $atp)
    {
        $this->atp = $atp;
    }

    /**
     * Delete classifactor, its children classificators, related fields and relations with deed.
     *
     * @param int $classificatorId Classificator ID.
     * @return boolean|int[] False or array of deleted classificators IDs.
     */
    public function delete($classificatorId)
    {
        if (empty($classificatorId)) {
            return false;
        }

        $entityManager = $this->atp->factory->Doctrine();

        /* @var $classificatorRepository \Atp\Repository\ClassificatorRepository */
        $classificatorRepository = $entityManager->getRepository('\Atp\Entity\Classificator');

        $node = $classificatorRepository->find($classificatorId);

        $ids = [];
        foreach ($classificatorRepository->getChildren($node, false, null, null, true) as $child) {
            $ids[] = $child->getId();

            $entityManager->remove($child);
        }

        $entityManager->flush();

        $joinedIds = join(',', $ids);
        $this->atp->logic->delete_table_structure([
            'ITYPE' => 'CLASS',
            0 => '`ITEM_ID` IN (' . $joinedIds . ')'
        ]);
        $this->atp->logic->delete_deed_extends([
            'ITYPE' => 'CLASS',
            0 => '`ITEM_ID` IN (' . $joinedIds . ')'
        ]);


        $adminJournal = $this->atp->factory->AdminJournal();
        $adminJournal->addRecord(
            $adminJournal->getDeleteRecord(
                'Klasifikatorių "' . $classificator['LABEL'] . '"'
            )
        );

        return $ids;
    }

    /**
     * Save classificator data.
     * 
     * If ID is provided and is not empty record update will be performed.
     * If ID is empty while PARENT_ID is set a new record will be created.
     * If ID is empty and PARENT_ID is not set a false will be returned.
     *
     * @param array $data
     * @param array $data['ID'] [optional]
     * @param array $data['PARENT_ID'] [optional]
     * @param array $data['LABEL']
     * @return boolean|int false or classificator ID.
     */
    public function save($data)
    {
        $adminJournal = $this->atp->factory->AdminJournal();
        $entityManager = $this->atp->factory->Doctrine();

        /* @var $repository \Atp\Repository\ClassificatorRepository */
        $repository = $entityManager->getRepository('Atp\Entity\Classificator');

        if (empty($data['ID']) === false) {
            $entity = $repository->find($data['ID']);
            $entity->setLabel($data['LABEL']);

            $journalEntry = $adminJournal->getEditRecord(
                'Klasifikatorių "' . $entity->getlabel() . '"'
            );
        } elseif (isset($data['PARENT_ID'])) {
            $parentEntity = null;
            $classificatorLevel = 0;
            if (empty($data['PARENT_ID']) === false) {
                $parentEntity = $repository->find($data['PARENT_ID']);
                $classificatorLevel = $parentEntity->getLevel() + 1;
            }

            $entity = new \Atp\Entity\Classificator();
            $entity->setLabel($data['LABEL']);
            $entity->setParent($parentEntity);
            $entity->setLevel($classificatorLevel);
            $entity->setValid(true);

            $journalEntry = $adminJournal->getCreateRecord(
                'Klasifikatorių "' . $entity->getlabel() . '"'
            );
        } else {
            return false;
        }

        $entityManager->persist($entity);
        $entityManager->flush();

        $adminJournal->addRecord($journalEntry);

        return $entity->getId();
    }
}
