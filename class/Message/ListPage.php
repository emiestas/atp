<?php

namespace Atp\Message;

class ListPage
{
    private $core;

    /**
     * @param \Atp\Core $core
     */
    public function __construct(\Atp\Core $core)
    {
        $this->core = $core;
    }

    public function render()
    {
        $workerId = $this->core->factory->User()->getLoggedPersonDuty()->id;
		$documentId = filter_input(INPUT_GET, 'documentId', FILTER_VALIDATE_INT);

        $tmpl = $this->core->factory->Template();
        $tmpl_handler = 'message_list_file';
        $tmpl->set_file($tmpl_handler, 'message_list.tpl');


        $messageManager = new \Atp\Message\Manager($this->core);
        $repository = $messageManager->getRepository();
        
        $filter = [
            'messageReceiver.receiverId' => $workerId
        ];
        if (empty($documentId) === false) {
            $filter['messageRecordDocument.id'] = $documentId;
        }
//        $totalMessages = $repository->count($filter);
        $totalMessages = $repository
            ->getBuilder($filter)
            ->select('COUNT(messageReceiver)')
            ->groupBy('receivedMessage.id')
            ->getQuery()
//            ->getSingleScalarResult();
            ->getArrayResult();
        $totalMessages = count($totalMessages);

        // Puslapiavimas
        include_once($this->core->config['REAL_URL'] . 'helper/paginator.php');
        $paginator = new \Paginator();
        $paginator->items_per_page = (!empty($_GET['ipp'])) ? $_GET['ipp'] : 20;
        $paginator->items_total = $totalMessages;
        $paginator->paginate();


        $messages = $repository
            ->getBuilder($filter)
            ->select('messageReceiver, messageRecord.id, messageRecord.number')
            ->orderBy('messageReceiver.receiveDate', 'DESC')
            ->groupBy('receivedMessage.id')
            ->setMaxResults(
                ($paginator->items_per_page === 'All' ? $paginator->items_total : $paginator->items_per_page)
            )
            ->setFirstResult($paginator->limit_start)
            ->getQuery()
            ->getResult();

        if (empty($messages) === false) {
            // Pranešimų lentelė
            $tableHtml = '';
            $tpl_arr = $this->core->getTemplate('table');

            // table
            $table = array();
            $table['table_class'] = 'message-list';
            $tableHtml .= $this->core->returnHTML($table, $tpl_arr[0]);

            // Lentelės antraštė
            $header = array('row_html' => '');
            $header['row_class'] = 'table-row table-header';
            $tableHtml .= $this->core->returnHTML($header, $tpl_arr[1]);


            $columns = array(
                //            'ID' => 'Pranešimo ID',
                'DATE' => 'Pasikeitimo data',
                'SUBJECT' => 'Pavadinimas',
                'DOCUMENT' => 'Dokumentas',
                'ACTIONS' => $this->core->lng('atp_table_actions')
            );
            foreach ($columns as $columnName => $column) {
                $cell = array(
                    'cell_html' => '',
                    'cell_class' => 'table-cell cell-' . $columnName,
                    'cell_content' => $column
                );
                $tableHtml .= $this->core->returnHTML($cell, $tpl_arr[2]);
            }
            $tableHtml .= $this->core->returnHTML($header, $tpl_arr[3]);
			foreach ($messages as $key => $pair) {
				$message = $pair[0];
                $recordId = $pair['id'];
                $recordNr = $pair['recordId'];

				$number = $pair['number'];
				$url = '';
				if (empty($number) === FALSE) {
					$url = '<a href="index.php?m=5&id='.$recordId.'">'.$number.'</a>';
				}

                $row = array(
                    'row_html' => 'id="atp-table-list-item-id-' . $messageId . '"',
                    'row_class' => 'table-row'
                );
                $tableHtml .= $this->core->returnHTML($row, $tpl_arr[1]);

                $table = array(
//                    'ID' => $message->getMessage()->getId(),
                    'DATE' => $message->getMessage()->getCreateDate()->format('Y-m-d H:i:s'),
                    'SUBJECT' => $message->getMessage()->getSubject(),
                    'DOCUMENT' => $url,
                    'ACTIONS' => ($message->isRead() ? null : '<a class="atp-html-button" href="index.php?m=23&id=' . $message->getId() . '&redirect=' . urlencode($_SERVER['REQUEST_URI']) . '">Perskaityti</a>')
                );

                foreach ($columns as $columnName => $column) {
                    $cell = array('cell_html' => '', 'cell_content' => '');
                    $cell['cell_class'] = 'table-cell cell-' . $columnName;
                    $cell['cell_content'] .= $table[$columnName];
                    $tableHtml .= $this->core->returnHTML($cell, $tpl_arr[2]);
                }

                $tableHtml .= $this->core->returnHTML($row, $tpl_arr[3]);
            }

            $tableHtml .= $this->core->returnHTML($table, $tpl_arr[4])
                . (empty($messages) ? null : $paginator->display_pages() . '<br/>' . $paginator->display_selects());
        } else {
            $tableHtml = 'Pranešimų nėra';
        }

        $tmpl->set_var(array(
            'table' => $tableHtml,
            'sys_message' => $this->core->get_messages(),
            'GLOBAL_SITE_URL' => $this->core->config['GLOBAL_SITE_URL']
        ));

        $tmpl->parse($tmpl_handler . '_out', $tmpl_handler);
        $output = $tmpl->get($tmpl_handler . '_out');

        return $this->core->atp_content($output);
    }

    /**
     * @return boolean
     * @throws \Exception If worker ID of message does not match user ID
     */
    public function markAsRead()
    {
        if (filter_has_var(INPUT_GET, 'id')) {
            $receiverMessageId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

            $entityManager = $this->core->factory->Doctrine();
            $repository = $entityManager->getRepository('Atp\Entity\MessageReceiver');
            $receiverMessage = $repository->find($receiverMessageId);

            if ($receiverMessage->getReceiverId() !== (int) $this->core->factory->User()->getLoggedPersonDuty()->id) {
                throw new \Exception(
                'Nepakanka teisių koreguoti kito darbuotojo pranešimą.', 403
                );
            }

            $messagesManger = new \Atp\Message\Manager($this->core);
            $result = $messagesManger->markAsRead($receiverMessage);

            return $result;
        }

        return false;
    }
}
