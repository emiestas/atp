<?php

namespace Atp\Message;

class Manager
{
    /**
     * @var \Atp\Core
     */
    private $core;

    /**
     * @param \Atp\Core $core
     */
    public function __construct(\Atp\Core $core)
    {
        $this->core = $core;
    }

    /**
     * @return \Atp\Repository\MessageReceiverRepository
     */
    public function getRepository()
    {
        $entityManager = $this->core->factory->Doctrine();
        return $entityManager->getRepository('Atp\Entity\MessageReceiver');
    }

    /**
     * @param \Atp\Entity\MessageReceiver|int $receiverMessage
     * @return boolean
     */
    public function markAsRead($receiverMessage)
    {
        $entityManager = $this->core->factory->Doctrine();

        if (is_int($receiverMessage) || is_numeric($receiverMessage)) {
            $repository = $this->getRepository();
            $receiverMessage = $repository->find($receiverMessage);
        }

        if ($receiverMessage instanceof \Atp\Entity\MessageReceiver) {
            // Marking message as read
            $receiverMessage->setIsRead(true);
            $receiverMessage->setReadDate(new \DateTime('now'));

            $entityManager->merge($receiverMessage);
            $entityManager->flush();

            return true;
        }

        return false;
    }
}
