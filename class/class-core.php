<?php

namespace Atp;

// TODO: gauti sekančio dokumento nagrinėtoją. Numatyti kada po tarpinės stadijojs pasibaigimo nagrinėtojas galės nagrinėti dokumentą ir šią informaciją įvesti į dokumento laukus
// TODO: WTF?
if (session_id() == '') {
    session_start();
}

class Core extends \datepicker
{
    /**
     * @var \Atp\FactoryStorage
     */
    public $factory;

    /**
     * @var \Atp\Manage
     */
    public $manage;

    /**
     * @var \Atp\Logic
     */
    public $logic;

    /**
     * @var \Atp\Logic2
     */
    public $logic2;

    /**
     * @var \Atp\Ajax
     */
    public $ajax;

    /**
     * @var \Atp\Action
     */
    public $action;

    /**
     * WS overload
     * @var \common
     */
    public $lang;

    /**
     * WS overload
     * @var \tpl
     */
    public $tpl;

    /**
     * user Prisijungusio vartotojo informacija<br>
     * user->id Vartotojo ID<br>
     * user->worker Darbuotojo informacija<br>
     * user->department->id Skyriaus ID<br>
     * user->departments Skyrių informacija, kuriems vartotojas priklauso<br>
     * user->status Statusai<br>
     * user->privileges Privilegijos<br>
     */
    public $user;

//    /**
//     * @deprecated nenaudojamas
//     */
//    public $m_people_id;

    public $_load_files;

    public $_message = array();

    public $_prefix;

    public $content_type = null;

//    //local
//    /**
//     * @deprecated nenaudojamas
//     */
//    public $_soft_id = 204;
//
//    public $_admin_id = 1;
//
//    //vilnius.lt
//    //public $_soft_id = 26;
//    //public $_admin_id = 3340;

    /**
     * Debuginimui
     * @var boolen
     */
    public $debug = false;

    public $debugQuery = false;

    public $config;

    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @param type $config
     */
    public function __construct($config)
    {
        $this->factory = new \Atp\FactoryStorage($this);

        $this->setConfig($config);

        if($_COOKIE['I'] === 'dev') {
            $this->debug = true;
        }

        if ($this->debug === true) {
            $this->debugQuery = false;

            ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
            if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) === false && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
                ini_set('display_errors', false);
            } else {
                ini_set('display_errors', true);
            }
        }
        if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) === false && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
            $this->content_type = 2;
        }

        $this->openSession();
        $this->get_messages_sess();

        // Prefix'ai
        $this->_prefix = new \stdClass();
        $this->_prefix->table = 'TBL_';
        $this->_prefix->column = 'COL_';
        // Pridėjus item kode ieškoti array_keys($this->_prefix->item), kad surasti kur reikalingi pakeitimai
        $this->_prefix->item = array(
            'DOC' => '',
            'CLASS' => 'K',
            'CLAUSE' => 'S',
            'DEED' => 'D',
            'ACT' => 'A',
            'WS' => 'W'
        );
        $this->_prefix->cl = ' str.';
        $this->_prefix->se = ' d.';
        $this->_prefix->pa = ' p.';
        $this->_prefix->su = '';


        $this->performance = $this->factory->Performance();
        $this->logic = new \Atp\Logic($this);
        $this->logic2 = new \Atp\Logic2($this);
        $this->manage = new \Atp\Manage($this);
        $this->action = new \Atp\Action($this);
        $this->ajax = new \Atp\Ajax($this);


        $this->set_load_files([
            'js' => [
                'jquery.min.js',
                'jquery-ui.min.js',
                'atp.js',
                'atp-combobox.js',
                'atp-list.js',
                'atp-picker.js',
                'atp-spinner.js',
            ]
        ]);


        if ($this->isTable(PREFIX . 'ATP_QUERY_LOG') === false) {
            $this->db_query_fast('CREATE TABLE IF NOT EXISTS `' . PREFIX . 'ATP_QUERY_LOG` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`site` int(1) NOT NULL DEFAULT "0",
					`start` float NOT NULL,
					`end` float NOT NULL,
					`time` float NOT NULL,
					`query` TEXT NOT NULL,
					`backtrace` TEXT NOT NULL,
					`request` TEXT NOT NULL,
					PRIMARY KEY (`id`)
				  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
        }
    }

    public function init()
    {
        $userManager = $this->factory->User();

        if (empty($_SESSION['WP']['PEOPLE']['ID']) === false) {
            if ($userManager->isLoggedIn() && $userManager->getLoggedUser()->person->id != $_SESSION['WP']['PEOPLE']['ID'] || $userManager->isLoggedIn() === false) {
                $userManager->logout();

                $userData = $userManager->getUserDataByPersonId($_SESSION['WP']['PEOPLE']['ID']);
                $userManager->loginById($userData->id);
            }
        }

        $username = filter_input(INPUT_POST, 'username');
        $password = filter_input(INPUT_POST, 'password');
        if ($username && $password && $userManager->isLoggedIn() === false) {
            if (!$userManager->login($username, $password)) {
                throw new \Exception('Failed to login with credentials');
            }
        }

        if ($userManager->isLoggedIn()) {
            if (isset($_POST['atp_user_department'])) {
                $userManager->setLoggedPersonDuty($_POST['atp_user_department']);
            } else if ($userManager->getLoggedPersonDuty() === false) {
                $userManager->setLoggedPersonDuty();
            }
        }
    }

    public function set_load_files($load_files)
    {
        foreach ($load_files as $type => $files) {
            foreach ($files as $file) {
                if (!in_array($file, (array) $this->_load_files[$type])) {
                    $this->_load_files[$type][] = $file;
                }
            }
        }
    }

    public function set_lng_vars($lng_vars = array(), $vars_arr = array())
    {
        foreach ($lng_vars as $var) {
            $vars_arr['lng_' . $var] = $this->lng($var);
        }

        return $vars_arr;
    }

    public function openSession()
    {
        session_destroy();
        session_name(SITE_NAME);
        session_start();
        $this->getDefaultSessionVars();
        $this->getAllLangElements();
    }

    public function checkSession()
    {
        checkSession('ATP');
    }

    public function action_manage($m = 1)
    {
        try {
            switch ($m) {
                case 1: $html = $this->manage->tables();
                    break;
                case 2: $html = $this->manage->documents_relations();
                    break;
                case 3: $html = $this->manage->classifications();
                    break;
                case 4: $html = $this->manage->table_records();
                    break;
                case 5: $html = $this->manage->table_record_edit();
                    break;
                case 6: $html = $this->manage->record_path();
                    break;
                // Vartotojų teisės (Administravimas)
                case 7: $html = $this->manage->user_administration();
                    break;
                case 8:
                    $browse = new \Atp\Document\Templates\Browse($this);
                    $html = $browse->init();
                    break;
                case 9:
                    $generator = new \Atp\Document\Templates\Generator($this);
                    $generator->Make($_REQUEST['id'], $_REQUEST['template_id']);
                    $generator->Download();
                    break;
                /* case 10: $html = $this->manage->departments();
                  break; */
                case 10: $html = $this->manage->clause_editform();
                    break;
                case 11: $html = $this->manage->clause();
                    break;
                // Vartotojų teisės: Administravimas
                case 12: $html = $this->manage->user_administration();
                    break;
                // Vartotojų teisės: Kompetencijos
                case 13: $html = $this->manage->user_competences();
                    break;
                // Vartotojų teisės: Dokumentų teisės
                case 14: $html = $this->manage->user_rights();
                    break;
                // Teisės aktų punktai (ex. Teisės aktai)
                case 15: $html = $this->manage->acts();
                    break;
                // Veikos
                case 16: $html = $this->manage->deeds();
                    break;
                // Įrašo mokėjimai
                case 17:
                    //$html = $this->ajax->record_payments_form();
                    $html = $this->manage->record_payments_form($_GET['table_id'], $_GET['id']);
                    break;
                case 18: $html = $this->manage->search();
                    break;
                case 19: $html = $this->action->report();
                    break;
                // Dokumentų būsenos
                case 20: $html = $this->manage->status();
                    break;
                // Būsenos
                case 21: $html = $this->manage->status_list();
                    break;
                // Pranešimų sąrašas
                case 22:
                    $messagesListPage = new \Atp\Message\ListPage($this);
                    $html = $messagesListPage->render();
                    break;
                // Pažymėti pranešimą kaip perskaitytą
                case 23:
                    $messagesListPage = new \Atp\Message\ListPage($this);
                    $result = $messagesListPage->markAsRead();

                    if ($result) {
                        $adminJournal = $this->factory->AdminJournal();
                        $adminJournal->addRecord(
                            $adminJournal->getSaveRecord(
                                'Pranešimas perskaitytas'
                                . ' (Pranešimas: ' . filter_has_var(INPUT_GET, 'id') . ').'
                            )
                        );


                        $messageType = 'success';
                        $messageText = 'Pranešimas perskaitytas';
                    } else {
                        $messageType = 'error';
                        $messageText = 'Pranešimo nepavyko pažymėti perskaitytu.';
                    }

                    if (!$result) {
                        throw new \Exception($messageText);
                    }

                    $this->set_message($messageType, $messageText);
                    if (filter_has_var(INPUT_GET, 'redirect')) {
                        $location = filter_input(INPUT_GET, 'redirect');
                        header('Location: ' . $location);
                    }
                    exit;
                    break;
                // Redirect to ATPR
                case 25:
                    $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

                    $entityManager = $this->factory->Doctrine();
                    $repository = $entityManager->getRepository('Atp\Entity\Atpr\Atp');
                    $queryBuilder = $repository->createQueryBuilder('atp')
                        ->select(array('atp.roik'))
                        ->innerJoin('atp.record', 'record')
                        ->where('record.id = :id')
                        ->setParameter('id', $id);

                    $atprRoik = $queryBuilder->getQuery()->getSingleScalarResult();

                    $userName = $_SESSION['username'];
                    $authentication = new \Atp\Atpr\AuthToken($this);
                    $authToken = $authentication->get($userName, $atprRoik);

//                    $url = str_replace('ozas.vrm.lt', '10.246.1.193', $authToken->getAtprIsorinisUrl());
                    $url = $authToken->getAtprIsorinisUrl();

                    header('Location: ' . $url);
                    exit;
                    break;
                case 98:
                    $result = $this->logic->delete_record((int) $_GET['table_id'], $_GET['record_id']);
                    if ($result) {

                        $adminJournal = $this->factory->AdminJournal();
                        $adminJournal->addRecord(
                            $adminJournal->getDeleteRecord(
                                'Dokumento įrašą'
                                . ' (Dokumento įrašas: ' . $_GET['record_id'] . ').'
                            )
                        );


                        $type = 'success';
                        $message = str_replace('[doc_id]', $_GET['record_id'], $this->lng('success_deleted'));
                    } else {
                        $type = 'error';
                        $message = str_replace('[doc_id]', $_GET['record_id'], $this->lng('deleted_denied'));
                    }

                    $this->set_message($type, $message);

                    header('Location: ' . $this->config['SITE_URL'] . 'index.php?m=4&id=' . (int) $_GET['table_id']);
                    exit;
                    break;
                case 99:
                    $html = $this->manage->login();
                    break;
//                // TODO: nenaudojamas
//                case 100: $html = $this->ajax->getFieldSelectParents();
//                    break;
//                // TODO: nenaudojamas
//                case 101: $html = $this->ajax->getFieldSelectChildren();
                    break;
                case 102: $html = '';
                    break;
                // TODO: nenaudojamas
                case 103: $html = $this->ajax->get_field_type();
                    break;
//                // TODO: nenaudojamas
//                case 104: $html = $this->ajax->get_field_select();
                    break;
                case 105: $html = $this->ajax->get_table_parent();
                    break;
                case 106: $html = $this->ajax->get_table_params();
                    break;
                // TODO: Nenaudojamas
                //case 107: $html = $this->ajax->get_classification();
                //	break;
                case 108:
                    trigger_error('\Atp\Ajax::get_source_value() deprecated', E_USER_FATAL);
                    $html = $this->ajax->get_source_value();
                    break;
                case 109: $html = $this->ajax->get_table_fields();
                    break;
//				// Nenaudojamas
//				case 110: $html = $this->ajax->get_webservice();
                    break;
                case 111: $html = $this->ajax->get_clause_child();
                    break;
                // TODO: Nenaudojamas
                case 112: $html = $this->ajax->get_clause();
                    break;
                // TODO: buvo naudojama ryšiams. Kolkas nebereikia
                case 113: $html = $this->ajax->get_clause_relation();
                    break;
                case 114: $html = $this->ajax->search_clause();
                    break;
                case 115: $html = $this->ajax->search_department();
                    break;
                case 116: $html = $this->ajax->search_user();
                    break;
                case 117: $html = $this->ajax->search_clause2();
                    break;
                case 118: $html = $this->ajax->classificator_action();
                    break;
                case 119: $html = $this->ajax->clause_action();
                    break;
                case 120: $html = $this->ajax->act_action();
                    break;
                case 121: $html = $this->ajax->deed_action();
                    break;
                case 122: $html = $this->ajax->search_services();
                    break;
                case 123: $html = $this->ajax->ws_action();
                    break;
                case 124: $html = $this->manage->searchFields();
                    break;
                case 125: $html = $this->ajax->search_fields_action();
                    break;
                case 126: $html = $this->ajax->status_action();
                    break;
                case 127: $html = $this->ajax->get_document_relation_default_field();
                    break;
                //// <editor-fold defaultstate="collapsed" desc="commented no longer used Register document">
//                // Register document
//                case 128:
//                    throw new \Exception('Dokumento registravimo funkcija pašalinta, naudoti elektroninio dokumento registravimo funkciją.');
//
//                    if (false) {
//                        $recordId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
//                        $templateId = filter_input(INPUT_GET, 'template_id', FILTER_VALIDATE_INT);
//
//                        $recordManager = new \Atp\Document\Record($recordId, $this);
//                        $recordData = $recordManager->record;
//
//                        $templateManager = new \Atp_Document_Templates_Template($this, $templateId);
//                        $registrationManager = new \Atp\Document\Record\Register($recordManager, $templateManager, $this);
//
//                        try {
//                            $result = $registrationManager->register();
//                            if ($result !== true) {
//                                if (empty($registrationManager->errors) === false) {
//                                    throw new \Exception(join(PHP_EOL, $registrationManager->errors));
//                                }
//
//                                throw new \Exception('Nepavyko užregistruoti dokumento');
//                            }
//
//                            $this->set_message(
//                                'success', 'Dokumentas užregistruotas. Dokumento numeris: ' . $registrationManager->document_number
//                            );
//
//
//                            $adminJournal = $this->factory->AdminJournal();
//                            $adminJournal->addRecord(
//                                $adminJournal->getCreateRecord(
//                                    'Avilio dokumentas "' . $registrationManager->document_number . '"'
//                                    . ' dokumento įrašui "' . $recordData['RECORD_ID'] . '"'
//                                )
//                            );
//                        } catch (\Exception $e) {
//                            $this->set_message('error', $e->getMessage());
//                        }
//
//                        header('Location: index.php?m=5&id=' . $recordData['ID']);
//                        exit;
//                    }
//                    break;// </editor-fold>

                case 129: $html = $this->ajax->get_document_person_free_time();
                    break;
                case 130: $html = $this->ajax->get_punishment_records_list();
                    break;
                case 136:
                    $html = $this->ajax->save_record();
                    break;
                case 137:
                    $html = $this->ajax->get_valid_templates();
                    break;
                // Register case
                case 138:
                    $recordId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
                    
                    try {
                        $avilysHelper = new \Atp\Avilys\AvilysHelper($this);
                        $avilysHelper->registerCase($recordId);
                    } catch (\Exception $e) {
                        error_log($e);
                        $this->set_message('error', $e->getMessage());
                    }
                    
                    header('Location: index.php?m=5&id=' . $recordId);
                    exit;
                    break;

                // <editor-fold defaultstate="collapsed" desc="commented old Register case">
//                // Register case
//                case 138:
//                    $recordId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
//                    $templateId = filter_input(INPUT_GET, 'template_id', FILTER_VALIDATE_INT);
//
//                    $recordManager = new \Atp\Document\Record($recordId, $this);
//                    $recordData = $recordManager->record;
//
//                    $templateManager = new \Atp_Document_Templates_Template($this, $templateId);
//                    $registrationManager = new \Atp\Document\Record\Register($recordManager, $templateManager, $this);
//
//                    try {
//                        $registrationManager->set_registration_fields = false;
//
//                        $result = $registrationManager->register();
//                        if ($result !== true) {
//                            if (empty($registrationManager->errors) === false) {
//                                throw new \Exception(join(PHP_EOL, $registrationManager->errors));
//                            }
//
//                            throw new \Exception('Nepavyko užregistruoti dokumento');
//                        }
//
//                        // TODO: Perduoti dokumentą į nutarimą, užsideda būseana "Užvesta byla"
//                        // Dokumentas perduodamas į "Bylą"
//                        $manager = new \Atp\Record\Manage($recordManager, $this);
//                        $manager->transferTo(\Atp\Document::DOCUMENT_CASE);
//
//                        $new_record = new \Atp\Document\Record($manager->result['saved_record_id'], $this);
//
//                        $registrationManager->setRegistrationFields($new_record);
//
////                        $this->set_message(
////                            'success',
////                            'Dokumentas užregistruotas. Dokumento numeris: ' . $registrationManager->document_number
////                        );
//                        $this->set_messages($manager->result['messages']);
//
//
//                        $adminJournal = $this->factory->AdminJournal();
//                        $adminJournal->addRecord(
//                            $adminJournal->getCreateRecord(
//                                'Avilio byla "' . $registrationManager->document_number . '"'
//                                . ' dokumento įrašui "' . $recordData['RECORD_ID'] . '".'
//                                . ' Naujas ATP bylos dokumento įrašas "' . $new_record->record['RECORD_ID'] . '".'
//                            )
//                        );
//
//
//                        header('Location: ' . $manager->result['redirect']);
//                        exit;
//                    } catch (\Exception $e) {
//                        $this->set_message('error', $e->getMessage());
//                    }
//
//                    header('Location: index.php?m=5&id=' . $recordData['ID']);
//                    exit;
//                    break;// </editor-fold>
//
                // Create/edit ATPR
                case 139:
                    $recordId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
                    $type = filter_input(INPUT_GET, 'type');

                    $record = new \Atp\Document\Record($recordId, $this);
                    $recordData = $record->record;

                    $atprManager = new \Atp\Atpr\Atp($this);

                    $errorMessage = null;
                    try {
                        $atpData = $atprManager->collectAtpData($recordData['ID']);

                        switch ($type) {
                            case 'create':
                                $result = $atprManager->create($atpData, $recordData['ID']);

                                try {
                                    $statusManager = new \Atp\Document\Record\Status(['RECORD' => $recordData['ID']], $this);
                                    $statusResult = $statusManager->CheckSingleStatus(64);
                                    if (empty($statusResult) === false) {
                                        $statusResult = array_shift($statusResult);
                                        $statusManager->SetRecordStatus($statusResult['RECORD'], $statusResult['STATUS']);
                                    }
                                } catch (\Exception $e) {
                                    error_log($e);
                                }
                                break;
                            case 'edit':
                                $result = $atprManager->edit($atpData);
                                break;
                            default:
                                throw new \Exception('Unknown Atpr action type');
                        }
                    } catch (\Exception $e) {
                        trigger_error($e, E_USER_WARNING);
                        $result = false;
                        $errorMessage = $e->getMessage();
                    }

                    if ($result !== false) {
						$atprManager->addDocumentsFor($result->getRoik());

                        $messageTypeText = null;
                        if ($type === 'create') {
                            $adminJournal = $this->factory->AdminJournal();
                            $adminJournal->addRecord(
                                $adminJournal->getCreateRecord(
                                    'ATPR objektas "' . $result->getRoik() . '"'
                                    . ' dokumento įrašui "' . $recordData['RECORD_ID'] . '".'
                                )
                            );

                            $messageTypeText = 'Sukurtas ATPR objektas';
                        } elseif ($type === 'edit') {
                            $adminJournal = $this->factory->AdminJournal();
                            $adminJournal->addRecord(
                                $adminJournal->getEditRecord(
                                    'ATPR objektas "' . $result->getRoik() . '"'
                                    . ' dokumento įrašui "' . $recordData['RECORD_ID'] . '".'
                                )
                            );

                            $messageTypeText = 'Atnaujintas ATPR objektas';
                        }

                        $messageText = $messageTypeText . ': '
                            . '<a href="index.php?m=25&id=' . $recordData['ID'] . '">'
                            . $result->getRoik()
                            . '</a>';
                    } else {
                        if ($type === 'create') {
                            $messageText = 'Nepavyko sukurti ATPR objekto.';
                        } elseif ($type === 'edit') {
                            $messageText = 'Nepavyko atnaujinti ATPR objekto.';
                        }

                        if (empty($errorMessage) === false) {
                            $messageText .= ' ' . $errorMessage;
                        }
                    }

                    $this->set_message(((bool) $result ? 'success' : 'error'), $messageText);

//                    header('Location: index.php?m=4&id=' . $recordData['TABLE_TO_ID']);
                    header('Location: index.php?m=5&id=' . $recordData['ID']);
                    exit;
                    break;
                case 140:
                    $id = (empty($_GET['id']) === true ? 1 : (int) $_GET['id']);
                    $post = new \Atp_Post_Browse($this, $id);
                    $html = $post->printPage();
                    break;
                case 141:
                    $html = $this->ajax->get_RN_code();
                    break;
                case 142:
                    $html = $this->ajax->free_RN_code();
                    break;
                case 143:
                    $html = $this->ajax->free_RN_code();
                    break;
                case 144:
                    $templateId = filter_input(INPUT_POST, 'template', FILTER_VALIDATE_INT);

                    $templateManager = new \Atp_Document_Templates_Template($this, $templateId);

                    $templateForm = new \Atp_Document_Templates_Template_Form($templateManager, $this);

                    $result = $templateForm->getServiceFieldsTable($templateManager->get()->service_id);

                    echo json_encode(array(
                        'result' => (bool) $result,
                        'html' => $result
                    ));
                    break;
                // Register el. document
                case 145:
                    $recordId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
                    $templateId = filter_input(INPUT_GET, 'template_id', FILTER_VALIDATE_INT);

                    try {
                        $recordHelper = $this->factory->Record();
                        $avilysHelper = new \Atp\Avilys\AvilysHelper($this);

                        $record = $recordHelper->getEntity($recordId);
                        $documentId = $record->getDocument()->getId();

						if ($documentId === \Atp\Document::DOCUMENT_PROTOCOL) {
                            $avilysHelper->createProtocol(
                                $record,
                                $templateId,
                                $recordHelper->isAnProtocol($record->getId())
                            );
                        } else if ($documentId === \Atp\Document::DOCUMENT_ACCOMPANYING) {
                            $protocol = $recordHelper->getPreviousDocumentFor(
                                $record->getId(),
                                \Atp\Document::DOCUMENT_PROTOCOL
                            );
                            $isAn = false;
                            if ($protocol) {
                                $isAn = $recordHelper->isAnProtocol($protocol->getId());
                            }

                            $avilysHelper->createAccompanying(
                                $record,
                                $templateId,
                                $isAn
                            );
                        } else if ($documentId === \Atp\Document::DOCUMENT_SUMMONS) {
                            $avilysHelper->createSummons(
                                $record,
                                $templateId
                            );
                        } else if ($documentId === \Atp\Document::DOCUMENT_REPORT) {
                            $protocol = $recordHelper->getPreviousDocumentFor(
                                $record->getId(),
                                \Atp\Document::DOCUMENT_PROTOCOL
                            );
                            $isAn = false;
                            if ($protocol) {
                                $isAn = $recordHelper->isAnProtocol($protocol->getId());
                            }
							$avilysHelper->createReport(
                                $record,
                                $templateId,
                                $isAn
                            );
                        } else {
                            $avilysHelper->registerElectronicDocument(
                                $record->getId(),
                                $templateId
                            );
                        }
                    } catch (\Exception $e) {
                        error_log($e);
                        $this->set_message('error', $e->getMessage());
                    }

                    header('Location: index.php?m=5&id=' . $recordId);
                    exit;
                    break;
                
                // <editor-fold defaultstate="collapsed" desc="commented old Register el. document">

//                // Register el. document
//                case 145:
//                    $recordId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
//                    $templateId = filter_input(INPUT_GET, 'template_id', FILTER_VALIDATE_INT);
//
//                    $recordManager = new \Atp\Document\Record($recordId, $this);
//                    $recordData = $recordManager->record;
//
//                    if ($templateId) {
//                        $templateManager = new \Atp_Document_Templates_Template($this, $templateId);
//                    } else {
//                        $templateManager = null;
//                    }
//                    $registrationManager = new \Atp\Document\Record\Register($recordManager, $templateManager, $this);
//
//                    try {
//                        $registrationManager->registerElectronic();
//
//                        try {
//                            $statusManager = new \Atp\Document\Record\Status(['RECORD' => $recordData['ID']], $this);
//                            $result = $statusManager->CheckSingleStatus(65);
//                            if (empty($result) === false) {
//                                $result = array_shift($result);
//                                $statusManager->SetRecordStatus($result['RECORD'], $result['STATUS']);
//                            }
//                        } catch (\Exception $e) {
//                            error_log($e);
//                        }
//
//                        $this->set_message(
//                            'success', 'Dokumentas užregistruotas. Dokumento numeris: ' . $registrationManager->document_number
//                        );
//
//                        $adminJournal = $this->factory->AdminJournal();
//                        $adminJournal->addRecord(
//                            $adminJournal->getCreateRecord(
//                                'Elektroninis Avilio dokumentas "' . $registrationManager->document_number . '"'
//                                . ' dokumento įrašui "' . $recordData['RECORD_ID'] . '".'
//                            )
//                        );
//                    } catch (\Exception $e) {
//                        $this->set_message('error', $e->getMessage());
//                    }
//
//                    header('Location: index.php?m=5&id=' . $recordData['ID']);
//                    exit;
//                    break;// </editor-fold>

                // Get configured fields
                case 146:
                    $fieldsIds = filter_input(INPUT_POST, 'fields', FILTER_VALIDATE_INT, FILTER_REQUIRE_ARRAY);

                    $configuredFields = $this->ajax->getConfiguredFields($fieldsIds);

                    $html = json_encode(array(
                        'result' => true,
                        'data' => $configuredFields
                    ));

                    break;
                // Get document data from ATPR
                case 147:
                    $recordId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

                    $record = new \Atp\Document\Record($recordId, $this);
                    $recordData = $record->record;

                    $atprManager = new \Atp\Atpr\Atp($this);
                    try {
                        $atprManager->updateAtp($recordData['ID']);

                        $result = true;
                    } catch (\Exception $e) {
						error_log($e);
                        $result = false;
                        $errorMessage = $e->getMessage();
                    }

                    if ($result !== false) {
                        $messageText = 'ATP atnaujintas.';
                    } else {
                        $messageText = 'Atnaujinti ATP nepavyko.';
                    }

                    $this->set_message(((bool) $result ? 'success' : 'error'), $messageText);

                    header('Location: index.php?m=5&id=' . $recordId);

                    exit;
                    break;
                // Get list of available next documents
                case 148:
                    $recordId = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);

                    $html = json_encode(array(
                        'result' => true,
                        'data' => $this->factory->Record()->getNextListHtml($recordId)
                    ));
                    break;
                // Address search
                case 149:
                    $query = trim(filter_input(INPUT_GET, 'query'));
                    $querySplit = array_filter(array_map([$this, 'db_escape'], explode(' ', $query)));
                    
                    $result = [
                        'result' => false,
                        'query' => $query,
                    ];

                    $longest = max(array_map('strlen', $querySplit));
                    
                    if (strlen(join('', $querySplit)) < 3 || $longest < 3) {
                        if ($longest < 3) {
                            $result['message'] = 'Įveskite bent vieną žodį su 3 simboliais';
                        } else {
                            $result['message'] = 'Per trumpas paieškos parametras';
                        }
                    } else {
                        $this->db_query_fast('SET NAMES UTF8');



                        $revelanceSelect = null;
                        $revelanceOrder = null;
                        $conditionType = 'OR';


                        $whereCondition = join(
                            ' ' . $conditionType . ' ',
                            array_map(
                                function($query) {
                                    $collate = null;
                                    if (strlen($query) === 1) {
                                        $query = strtoupper($query);
                                        $collate = 'COLLATE utf8_bin';
                                    }

                                    return 'ADRESAS LIKE "%' . $query . '%" ' . $collate . '';
                                },
                                $querySplit
                            )
                        );

                        if ($conditionType === 'OR') {
                            // <editor-fold defaultstate="collapsed" desc="Would be needed for revelance priority, if filtered with OR conditions">
                            $nr = 0;


                            // Revelance. By match count
                            $matchCountPriority = null;
                            foreach ($querySplit as $query) {
                                $collate = null;
                                if (strlen($query) === 1) {
                                    $query = strtoupper($query);
                                    $collate = 'COLLATE utf8_bin';
                                }
                                $matchCountPriority[] = 'IF(ADRESAS LIKE "%' . $query . '%" ' . $collate . ', 1, 0)';
                            }
                            $matchCountPriority = join(' + ', $matchCountPriority);

                            $nr++;
                            $number = '_' . $nr;
                            $revelanceSelect[] = $matchCountPriority . ' ' . $number;
                            $revelanceOrder[] = $number . ' DESC';

                            // Revelance by word position in address. Exact match, starts, anywhere
                            foreach ($querySplit as $query) {
                                $nr++;
                                $number = '_' . $nr;

                                $nr++;
                                $number = '_' . $nr;
                                $revelanceSelect[] = 'IF(ADRESAS LIKE "' . $query . '", 1, 0) ' . $number;
                                $revelanceOrder[] = $number . ' DESC';

                                $nr++;
                                $number = '_' . $nr;
                                $revelanceSelect[] = 'IF(ADRESAS LIKE "' . $query . '%", 1, 0) ' . $number;
                                $revelanceOrder[] = $number . ' DESC';

                                $nr++;
                                $number = '_' . $nr;
                                $revelanceSelect[] = 'IF(ADRESAS LIKE "%' . $query . '%", 1, 0) ' . $number;
                                $revelanceOrder[] = $number . ' DESC';
                            }
                            // </editor-fold>
                        }

                        // Default sort
                        $revelanceOrder[] = 'GATVE ASC';
                        $revelanceOrder[] = 'NAMO_NR ASC';
                        $revelanceOrder[] = 'NAMO_R ASC';

                        $revelanceSelect = join(',', $revelanceSelect);
                        $revelanceOrder = join(',', $revelanceOrder);
                        
                        $qryResult = $this->db_query_fast('
                            SELECT
                                ADRESAS
                                ' . ($revelanceSelect ? ',' . $revelanceSelect : null) . '
                            FROM ' . PREFIX . 'VAR_DATA
                            WHERE
                                ' . $whereCondition . '
                            ORDER BY ' . $revelanceOrder . '
                            LIMIT 100');
                        $count = $this->db_num_rows($qryResult);

                        $data = [];
                        if ($count) {
                            for ($i = 0; $i < $count; $i++) {
                                $row = $this->db_next($qryResult);

                                $data[] = $row['ADRESAS'];
                            }

                            $result['result'] = true;
                            $result['data'] = $data;
                        } else {
                            $result['result'] = false;
                            $result['message'] = 'Nerasta';
                        }
                    }

                    header('Content-type: application/json');
                    echo json_encode($result);
                    exit;
                case 150:
                    $page = new \Atp\Avilys\ConfigurationPage($this);
                    $page->handle();
                    exit;
                    break;
                // Documents filter
                case 151:
                    $request = filter_input_array(INPUT_POST);
                    $filter = $request['filter'];

                    $queryBuilder = $this->factory->Doctrine()
                        ->getRepository('Atp\Entity\Document')
                        ->createQueryBuilder('document');

                    if (isset($filter['label'])) {
                        $queryBuilder
                            ->andWhere('document.label LIKE :label')
                            ->setParameter('label', '%' . $filter['label'] . '%');
                    }

                    /* @var $documents \Atp\Entity\Document[] */
                    $documents = $queryBuilder->getQuery()
                        ->getResult();

                    header('Content-type: application/json');

                    echo json_encode([
                        'data' => array_map(function(\Atp\Entity\Document $document) {
                                return [
                                    'id' => $document->getId(),
                                    'label' => $document->getLabel()
                                ];
                            }, $documents)
                    ]);

                    exit;
                    break;
                // Templates filter
                case 152:
                    $request = filter_input_array(INPUT_POST);
                    $filter = $request['filter'];

                    $queryBuilder = $this->factory->Doctrine()
                        ->getRepository('Atp\Entity\Template')
                        ->createQueryBuilder('template');

                    if (isset($filter['label'])) {
                        $queryBuilder
                            ->andWhere('template.label LIKE :label')
                            ->setParameter('label', '%' . $filter['label'] . '%');
                    }
                    if (isset($filter['document'])) {
                        $queryBuilder
                            ->innerJoin('template.document', 'document')
                            ->andWhere('document.id = :documentId')
                            ->setParameter('documentId', $filter['document']);
                    }

                    /* @var $templates \Atp\Entity\Template[] */
                    $templates = $queryBuilder->getQuery()
                        ->getResult();

                    header('Content-type: application/json');

                    echo json_encode([
                        'data' => array_map(function(\Atp\Entity\Template $template) {
                                return [
                                    'id' => $template->getId(),
                                    'label' => $template->getLabel()
                                ];
                            }, $templates)
                    ]);

                    exit;
                    break;
                // Department filter
                case 153:
                    $request = filter_input_array(INPUT_POST);
                    $filter = $request['filter'];

                    $queryBuilder = $this->factory->Doctrine()
                        ->getRepository('Atp\Entity\Department')
                        ->createQueryBuilder('department');

                    if (isset($filter['label'])) {
                        $queryBuilder
                            ->andWhere('department.label LIKE :label')
                            ->setParameter('label', '%' . $filter['label'] . '%');
                    }

                    /* @var $departments \Atp\Entity\Department[] */
                    $departments = $queryBuilder->getQuery()
                        ->getResult();

                    header('Content-type: application/json');

                    echo json_encode([
                        'data' => array_map(function(\Atp\Entity\Department $department) {
                                return [
                                    'id' => $department->getId(),
                                    'label' => $department->getLabel()
                                ];
                            }, $departments)
                    ]);

                    exit;
                    break;
                default:
//                    $html = $this->manage->tables();
                    $messagesListPage = new \Atp\Message\ListPage($this);
                    $html = $messagesListPage->render();
                    break;
            }
        } catch (\Exception $e) {
            error_log($e);

            $tmpl = $this->factory->Template();

            $tmpl->handler = 'home_file';
            $tmpl->set_file($tmpl->handler, 'home.tpl');
            $arr = array('message' => $e->getMessage());
            if ($this->debug === true) {
                $arr['backtrace'] = $e->getTraceAsString();
            }
            $tmpl->set_var($arr);
            $tmpl->parse($tmpl->handler . '_out', $tmpl->handler);

            $output = $tmpl->get($tmpl->handler . '_out');
            $html = $this->atp_content($output);
        }

        echo $html;
    }

    /**
     * Sukuria pranešimą
     * @authors Martynas Boika, Justinas Malūkas
     * @param string $type Pranešimo tipas
     * @param string $message Pranešimas
     * @param array $param Nurodyti pakeitimai
     * @example <b>set_message('error', 'Klaida: [klaidos_id]', array('klaidos_id' => 11))
     * @return boolean
     */
    public function set_message($type, $message, $param = null)
    {
        if (empty($type) || empty($message)) {
            return false;
        }

        // Teksto pakeitimas
        if (empty($param) === false) {
            $keys = array();
            foreach ($param as $key => $val) {
                $keys[] = '[' . $key . ']';
            }
            $vals = array_values($param);
            $message = str_replace($keys, $vals, $message);
        }

//        if ($type === 'success') {
//            $sp = array($message);
//            $this->manage->writeATPactionLog($sp);
//        }
        $adminJournal = $this->factory->AdminJournal();
        $adminJournal->addRecord(
            $adminJournal->getMessageRecord($message)
        );

        $_SESSION['ATP']['MESSAGES'][$type][] = $message;
    }

    public function set_messages($arr)
    {
        $_SESSION['ATP']['MESSAGES'] = $arr;
    }

    public function get_messages_sess()
    {
        $this->_message = array();
        if (!empty($_SESSION['ATP']['MESSAGES'])) {
            foreach ($_SESSION['ATP']['MESSAGES'] as $type => &$messages) {
                foreach ($messages as &$message) {
                    $this->_message[$type][] = $message;
                }
            }
        }
    }

    public function unset_messages_sess()
    {
        unset($_SESSION['ATP']['MESSAGES']);
    }

    public function get_messages($array = false)
    {
        $this->get_messages_sess();
        if (!empty($this->_message)) {
            if ($array === true) {
                $res = $this->_message;
            } else {
                $tpl = $this->getTemplate('get_messages');
                $res = $tpl[0];
                foreach ($this->_message as $type => &$messages) {
                    foreach ($messages as &$message) {
                        $res .= $this->returnHTML(array(
                            'message_type' => $type,
                            'message' => $message), $tpl[1]);
                    }
                }
                $res .= $tpl[2];
            }
            $this->unset_messages_sess();

            return $res;
        }

        return '';
    }

    public function atp_header()
    {
        $header_tpl = $this->getTemplate('header');

        $header_markers = array('css' => 1, 'js' => 2);

        $header_url = array(
            'css' => STYLE_URL,
            'js' => $this->config['JS_URL'],
            'site' => $this->config['SITE_URL']);

        foreach ($header_url as &$url) {
            $parsed = parse_url($url);
            $url = $parsed['path'];
        }

        $header_html = $this->returnHTML($header_url, $header_tpl[0]);
        if (!empty($this->_load_files)) {
            foreach ($this->_load_files as $type => $files) {
                foreach ($files as $file) {
                    $header_html .= $this->returnHTML(
                        array('file' => $header_url[$type] . $file), $header_tpl[$header_markers[$type]]
                    );
                }
            }
        }

        $header_html .= $this->returnHTML(array(), $header_tpl[count($header_markers) + 1]);

        return $header_html;
    }

    public function atp_menu()
    {
        $admin_menu = array(
            array('ID' => 1, 'NAME' => 'A_TABLE', 'LABEL' => $this->lng('menu_A_TABLE'),
                'HREF' => 'index.php?m=1'),
            array('ID' => 2, 'NAME' => 'A_RELATION', 'LABEL' => $this->lng('menu_A_RELATION'),
                'HREF' => 'index.php?m=2'),
            array('ID' => 14, 'NAME' => 'A_STATUS', 'LABEL' => $this->lng('menu_A_STATUS'),
                'HREF' => 'index.php?m=20'),
            array('ID' => 5, 'NAME' => 'A_DOCUMENT', 'LABEL' => $this->lng('menu_A_DOCUMENT'),
                'HREF' => 'index.php?m=8'),
            array('ID' => 3, 'NAME' => 'A_CLASSIFICATION', 'LABEL' => $this->lng('menu_A_CLASSIFICATION'),
                'HREF' => 'index.php?m=3'),
            array('ID' => 11, 'NAME' => 'A_ACT', 'LABEL' => $this->lng('menu_A_ACT'),
                'HREF' => 'index.php?m=15'),
            array('ID' => 12, 'NAME' => 'A_DEED', 'LABEL' => $this->lng('menu_A_DEED'),
                'HREF' => 'index.php?m=16'),
            array('ID' => 4, 'NAME' => 'A_COMPETENCE', 'LABEL' => $this->lng('menu_A_COMPETENCE'),
                'HREF' => 'index.php?m=7', 'CHILDREN' => array(
                    array('ID' => 8, 'NAME' => 'A_COMPETENCE_ADMIN', 'LABEL' => $this->lng('menu_A_COMPETENCE_ADMIN'),
                        'HREF' => 'index.php?m=12'),
                    array('ID' => 9, 'NAME' => 'A_COMPETENCE_COMPETENCIES', 'LABEL' => $this->lng('menu_A_COMPETENCE_COMPETENCIES'),
                        'HREF' => 'index.php?m=13'),
                    array('ID' => 10, 'NAME' => 'A_COMPETENCE_USER_RIGHTS', 'LABEL' => $this->lng('menu_A_COMPETENCE_USER_RIGHTS'),
                        'HREF' => 'index.php?m=14')/* ,
                  array('ID' => 7, 'NAME' => 'A_DEPARTMENT', 'LABEL' => 'Skyriai', 'HREF' => 'index.php?m=10') */
                )),
            array('ID' => 6, 'NAME' => 'A_CLAUSE', 'LABEL' => $this->lng('menu_A_CLAUSE'),
                'HREF' => 'index.php?m=11'),
            array('ID' => 13, 'NAME' => 'A_TABLE', 'LABEL' => $this->lng('menu_A_SEARCH'),
                'HREF' => 'index.php?m=124'),
            array('ID' => 15, 'NAME' => 'A_TABLE', 'LABEL' => 'Pašto administravimas' /* $this->lng('menu_A_POST') */,
                'HREF' => 'index.php?m=140&id=1'),
            array('ID' => 16, 'NAME' => 'A_TABLE', 'LABEL' => 'Avilio informacija',
                'HREF' => 'index.php?m=150')
        );

        $tmpl = $this->factory->Template();
        $tmpl->handler = 'menu_file';
        $tmpl->set_file($tmpl->handler, 'menu.tpl');

        $handler = $tmpl->set_multiblock($tmpl->handler, array(
            'menu_subitem',
            'menu_item',
            'menu_group_title',
            'menu_group_title_anchor',
            'menu_group',
            'user_multiple_departments_option',
            'user_multiple_departments',
            'user_single_department',
            'user_block',
        ));


        $messageManager = new \Atp\Message\Manager($this);
        $repository = $messageManager->getRepository();

        $unreadMessagesCount = $repository->count([
            'messageReceiver.receiverId' => $this->factory->User()->getLoggedPersonDuty()->id,
            'messageReceiver.isRead' => false
        ]);

        $unreadCountList = $repository->getUnreadMessagesCountList($this->factory->User()->getLoggedPersonDuty()->id);


        // Pranešimai
        $tmpl->set_var(array(
            'menu_href' => 'index.php?m=22',
            'menu_label' => 'Pranešimai',
            'menu_extra' => '(' . $unreadMessagesCount . ')'
        ));
        $tmpl->clean($handler['menu_item']);
        $tmpl->clean($handler['menu_subitem']);
        $tmpl->parse($handler['menu_group_title_anchor'], 'menu_group_title_anchor', false);
        $tmpl->clean($handler['menu_group_title']);
        $tmpl->set_var(array(
            'class' => 'to-left menu-messages',
            'icon' => '<i class="icon icon-mail" style="float: left; border: 0;"></i>'
        ));
        $tmpl->parse($handler['menu_group'], 'menu_group', false);


        // Dokumentų sąrašas
        $tables = $this->logic2->get_document(null, false, 'PARENT_ID, LABEL');
        $tables = $this->logic->sort_children_to_parent($tables, 'ID', 'PARENT_ID');

        $i = 0;
        foreach ($tables as $table) {

            $privileges_data = array(
                'user_id' => $this->factory->User()->getLoggedUser()->person->id,
                'department_id' => $this->factory->User()->getLoggedPersonDuty()->department->id,
                'table_id' => $table['ID']
            );
            if ($this->logic->privileges('atp_menu_see_records_table_link', $privileges_data) === true) {
                $unreadMessageCount = null;
                if (empty($unreadCountList[$table['ID']]) === false) {
                    $unreadMessageCount = '<span style="float:none;">&nbsp;(<a style="float:none;" href="index.php?m=22&documentId=' . $table['ID'] . '">' . $unreadCountList[$table['ID']] . '</a>)</span>';
                }

                $tmpl->set_var(array(
                    'menu_href' => 'index.php?m=4&id=' . $table['ID'],
                    'menu_label' => $table['LABEL'],
                    'menu_extra' => $unreadMessageCount
                ));
                $tmpl->parse($handler['menu_item'], 'menu_item', $i);
            }
            $tmpl->clean($handler['menu_subitem']);

            $i++;
        }

        $tmpl->set_var(array(
            'title' => 'Formų sąrašas',
            'class' => 'to-left menu-document',
            'icon' => '<img alt="" src="{$GLOBAL_SITE_URL}images/dok_block_dokum.png">'
        ));
        $tmpl->parse($handler['menu_group_title'], 'menu_group_title');
        $tmpl->clean($handler['menu_group_title_anchor']);
        $tmpl->parse($handler['menu_group'], 'menu_group', true);


        // Administravimas
        if ($this->factory->User()->getLoggedPersonDutyStatus('VIEW_ADMIN')) {
            $i = 0;
            foreach ($admin_menu as $menu) {
                if ($this->factory->User()->getLoggedPersonDutyStatus($menu['NAME'])) {
                    if (empty($menu['CHILDREN']) === false) {
                        $submenu_html = array();
                        for ($j = 0; $j < count($menu['CHILDREN']); $j++) {
                            $tmpl->set_var(array(
                                'menu_href' => $menu['CHILDREN'][$j]['HREF'],
                                'menu_label' => $menu['CHILDREN'][$j]['LABEL'],
                                'menu_extra' => ''
                            ));
                            $tmpl->parse($handler['menu_subitem'], 'menu_subitem', $j);
                        }
                    } else {
                        $tmpl->clean($handler['menu_subitem']);
                    }

                    $tmpl->set_var(array(
                        'menu_href' => $menu['HREF'],
                        'menu_label' => $menu['LABEL'],
                        'menu_extra' => ''
                    ));
                    $tmpl->parse($handler['menu_item'], 'menu_item', $i);
                } else {
                    $tmpl->clean($handler['menu_item']);
                }

                $i++;
            }

            $tmpl->set_var(array(
                'title' => 'Administravimas',
                'class' => 'to-right menu-admin',
                'icon' => '<img alt="" src="{$GLOBAL_SITE_URL}images/dok_block_dokum.png">'
            ));
            $tmpl->parse($handler['menu_group_title'], 'menu_group_title');
            $tmpl->clean($handler['menu_group_title_anchor']);
            $tmpl->parse($handler['menu_group'], 'menu_group', true);
        }

        $duties = $this->factory->User()->getLoggedUser()->person->personDuties;

        $departments = [];
        foreach ($duties as $duty) {
            if ($duty->isTemporary) {
                continue;
            }
            $departments[] = $duty->department;
        }

        if (count($departments) == 1) {
            foreach ($departments as $department) {
                $tmpl->set_var(array(
                    'department_label' => $department->label
                ));
                $tmpl->parse($handler['user_single_department'], 'user_single_department', true);
            }
        } else {
            foreach ($departments as $department) {
                $tmpl->set_var(array(
                    'selected' => ($department->id == $this->factory->User()->getLoggedPersonDuty()->department->id ? ' selected="selected"' : ''),
                    'department_id' => $department->id,
                    'department_label' => $department->label
                ));
                $tmpl->parse($handler['user_multiple_departments_option'], 'user_multiple_departments_option', true);
            }

            $tmpl->parse($handler['user_multiple_departments'], 'user_multiple_departments');
        }

        $tmpl->set_var(array(
            'user_name' => $this->factory->User()->getLoggedUser()->firstName,
            'user_surname' => $this->factory->User()->getLoggedUser()->lastName
        ));
        $tmpl->parse($handler['user_block'], 'user_block');


        $tmpl->set_var('GLOBAL_SITE_URL', $this->config['GLOBAL_SITE_URL']);

        $tmpl->parse($tmpl->handler . '_out', $tmpl->handler);
        $output = $tmpl->get($tmpl->handler . '_out');

        return $output;
    }

    public function atp_content($html, $type = null)
    {
		if ($type === null) {
            $type = $this->content_type;
        }

        switch ($type) {
            case 1: $html = $this->atp_header() . $html . $this->atp_footer();
                break;
            case 2: $html = $html;
                break;
            //default: $html = $this->atp_header() . $this->_preparsed_menu . $html . $this->atp_footer();
            default: $html = $this->atp_header() . $this->atp_menu() . $html . $this->atp_footer();
        }

        $html = $this->parseForNewWebPartner($html);

        if ($type !== 2 && extension_loaded('tidy') && $this->debug === true) {
            $config = array(
                'indent' => true,
                'output-xhtml' => false,
                'wrap' => 200);

            $tidy = new \tidy;
            $tidy->parseString($html, $config, 'utf8');
            $tidy->cleanRepair();
            
            $html = (string) $tidy;
        }

        return $html;
    }

    public function atp_footer()
    {
        $tpl = $this->getTemplate('footer');
        $bottom_tpl = $this->returnHTML(array(), $tpl[0]);

        return $bottom_tpl;
    }

    public function getRDBWsData($method, $data, $type = 'array')
    {
        prepare_rdb();
        require_once($this->config['GLOBAL_REAL_URL'] . 'subsystems/rdb/tools/class-rdbws.php');
        $data['username'] = sha1('rdbNRTws');
        $data['password'] = sha1('T53==+oje+tsycv3Z!2-nQbwt*R==');
        $ws = new tools_ws($method, $data, $type);
        $ws->init();
        $res = $ws->get_result();
        if (isset($res['data'])) {
            return $res['data'];
        }

        return false;
    }

    // WS overload
    public function getTemplate($a, $null = null)
    {
        if (empty($this->tpl)) {
            $this->tpl = new \tpl();
        }

        $subsystem = $this->config['SUBSYSTEM'];
        if (empty($null) === false) {
            $subsystem = $null;
        }

        return $this->tpl->getTemplate($a, $subsystem);
    }

    public function getAllLangElements($null = null)
    {
        if (empty($this->lang)) {
            $this->lang = new \common();
        }
        return $this->lang->getAllLangElements($this->config['SUBSYSTEM']);
    }

    public function lng($string)
    {
        return $this->lang->getLangElement($string);
    }

    /**
     * WS overload
     * @param string $table
     * @param string[] $row
     * @param int $mode [optional]
     * @return int
     */
    public function db_quickinsert($table, $row, $mode = 0)
    {
        $type = null;
        if ($mode === \dbo::INSERT) {
            $type = 1;
        } elseif ($mode === \dbo::INSERT_REPLACE) {
            $type = 2;
        } elseif ($mode === \dbo::INSERT_IGNORE) {
            $type = 3;
        }

        return $this->db_ATP_insert($table, $row, $type);
    }

    /**
     * @param string $table
     * @param string[]|array[] $arr
     * @param int|string $mode [optional] (1 || 'INSERT'; 2 || 'REPLACE'; 3 || 'IGNORE' || 'INSERT IGNORE'). Default: 1
     * @param string|array $onDuplicate [optional]
     * @return type
     */
    public function db_ATP_insert($table, $arr, $mode = 1, $onDuplicate = null)
    {
        $qry = array(
            'col' => null,
            'rows' => [],
            'args' => [],
            'val' => null
        );

        foreach ($arr as $row) {
            if (is_array($row) === false) {
                $arr = array($arr);
                break;
            }
        }
        foreach ($arr as $i => $row) {
            $row_qry = array();
            foreach ($row as $key => $val) {
                if (strpos($key, '#') !== false) {
                    $key_2 = preg_replace('/#/', '#' . $i . '_', $key, 1);
                    $qry['args'][$key_2] = $val;
                } else {
                    $qry['args'][$i . '_' . $key] = $val;
                }

                $row_qry['val'][] = ':' . str_replace('#', '', $i . '_' . $key);
                $row_qry['col'][] = '`' . str_replace('#', '', $key) . '`';
            }
            $qry['rows'][] = '(' . join(',', $row_qry['val']) . ')';
            $qry['col'] = '(' . join(',', $row_qry['col']) . ')';
        }
        $qry['val'] = join(',', $qry['rows']);

        if (empty($qry['col'])) {
            $qry['col'] = '()';
        }
        if (empty($qry['val'])) {
            $qry['val'] = '()';
        }

        if ($mode === 1 || strtoupper($mode) === 'INSERT') {
            $action = 'INSERT';
        } elseif ($mode === 2 || strtoupper($mode) === 'REPLACE') {
            $action = 'REPLACE';
        } elseif ($mode === 3 || strtoupper($mode) === 'INSERT IGNORE' || strtoupper($mode) === 'IGNORE') {
            $action = 'INSERT IGNORE';
        }

        $sql = $this->db_query_fast($action . ' INTO ' . $table . ' ' . $qry['col'] . ' VALUES ' . $qry['val'], $qry['args'], null, null, true);

        if (empty($onDuplicate) === false) {
            $sql .= ' ON DUPLICATE KEY UPDATE ';
            if (is_array($onDuplicate)) {
                $sql .= implode(',', $onDuplicate);
            } elseif (is_string($onDuplicate)) {
                $sql .= $onDuplicate;
            }
        }

        $this->db_query_fast($sql);

        return $this->db_last_id();
    }

    // WS overload
    public function db_query_fast($query, $bind = null, $print = false, $skip_termination = false, $returnSql = false)
    {
        if (empty($this->CONN)) {
            $this->db_connect();
        }
        if ($bind !== null) {
            if (true === is_array($bind) && 0 < sizeof($bind)) {
                // BUG FIX - BEGIN
                // jei parametru keys su tokiomis paciomis pradziomis - str_replace iskerpa kito parametra ir
                // bogai suformuoja SQL
                // pvz.: pupil, pupilcount -> uzbindins PUPIL = 589 AND PUPILCOUNT = count, ...
                $parKeys = array_keys($bind);
                $parKeysStr = implode(',', $parKeys);
                $unsetArrKeys = array();
                $setNewValsArr = array();
                foreach ($bind as $k => $v) {
                    if (substr_count($parKeysStr, $k) > 1) { // dubliuotas key - atliekema fixa
                        $cnt = 0;
                        foreach ($parKeys as $k1) {
                            $cnt++;
                            if (strpos($k1, $k) !== false && (strlen($k1) > strlen($k))) {
                                $query = str_replace(':' . $k1, ':' . $cnt . $k1, $query);
                                $unsetArrKeys[] = $k1;
                                $setNewValsArr[$cnt . $k1] = $bind[$k1];
                            }
                        }
                    }
                }
                // sutvarkom parametru bindima
                if (empty($unsetArrKeys) === false) {
                    foreach ($unsetArrKeys as $k) {
                        unset($bind[$k]);
                    }
                    $bind = array_merge($bind, $setNewValsArr);
                }
                unset($parKeys, $parKeysStr, $unsetArrKeys, $setNewValsArr);
                // BUG FIX - END
                $arr = array();
                foreach ($bind as $key => $val) {
                    if (strpos($query, ':' . trim($key, '#')) === false) {
                        continue;
                    }

                    // Be konvertavimo. pvz.: #CREATE_DATE# => NOW()
                    if (preg_match('/^#[0-9A-Za-z_]+#$/', $key) === 1) {
                        $key = str_replace('#', '', $key);
                        $arr[':' . $key] = $val;
                        // TODO: jei paduodamas null, tai reikia insertinti null o ne "" ar 0
                        // Tam reikia prieš tai susigaudyti laukus, į kuriuos dedamas null, tačiau jie aprašyti kaip NOT null
                    } elseif (true === is_null($val)) {
                        $arr[':' . $key] = 'NULL';
                    } elseif (false === is_numeric($val)) {
                        $arr[':' . $key] = "'" . $this->db_escape($val) . "'";
                    } elseif (ctype_digit($val) === true) {
                        $arr[':' . $key] = $val;
                    } else {
                        $arr[':' . $key] = "'" . $this->db_escape($val) . "'";
                    }
                }

                $query = str_replace(array_keys($arr), array_values($arr), $query);
            }
        }

        if ($print === true) {
            echo $query;
        }

        if ($returnSql === true) {
            return $query;
        }

        $start = microtime(true);
        if (is_resource($this->CONN)) {
            $res = mysql_query($query);
        } elseif (is_object($this->CONN)) {
            $res = mysqli_query($this->CONN, $query);
        }
        $end = microtime(true);

        if (!$res) {
            $var_dump = debug_backtrace();
            if (is_resource($this->CONN)) {
                $ERRORMSG = mysql_error() . PHP_EOL . $query . PHP_EOL;
            } elseif (is_object($this->CONN)) {
                $ERRORMSG = mysqli_error($this->CONN) . PHP_EOL . $query . PHP_EOL;
            }
            $a = -1;
            $b = count($var_dump);
            while ($a <= $b - 2) {
                $a++;
                $ERRORMSG .= 'File:' . $var_dump[$a]['file'] . '; Line: ' . $var_dump[$a]['line'] . '; Function: ' . $var_dump[$a]['function'] . PHP_EOL;
            }
            echo '<!-- ' . $ERRORMSG . ' -->';
            $this->reportError($ERRORMSG, $_SERVER['REQUEST_URI'], $skip_termination);
        } else {
            if (strpos($query, 'LOCK TABLES') !== false) {
                $this->debugQuery = false;
            } elseif (strpos($query, 'UNLOCK TABLES') !== false) {
                $this->debugQuery = true;
            }
            if ($this->debugQuery === true) {
                if (strpos($query, 'TST_ATP_') !== false) {
                    if (strpos(substr($query, 0, 50), 'SELECT') !== false) {
                        if (strpos($query, 'TEMP_COUNT') === false) {
                            $time = round($end - $start, 4);
                            if ($time > 0.001) {
                                ob_start();
                                debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
                                $backtrace = ob_get_clean();
                                $this->db_ATP_insert(
                                    PREFIX . 'ATP_QUERY_LOG', array(
                                    'start' => round($start, 4),
                                    'end' => round($end, 4),
                                    'time' => $time,
                                    'query' => $query,
                                    'backtrace' => $backtrace,
                                    'request' => json_encode(array('request' => $_REQUEST,
                                        'server' => $_SERVER))
                                    )
                                );
                            }
                        }
                    }
                }
            }
        }

        return $res;
    }

    /**
     * Šablono spauzdinimas, kai vartotojas neturi teisės į puslapio peržiurą
     * @return string
     */
    public function printForbidden()
    {
        throw new \Exception('Administracinės teisės pažeidimai', 401);
    }

    /**
     * Get first not empty element.
     *
     * @param array $array Array to filter.
     * @param mixed[] $inIndexes [optional] Filter array by indexes.
     * @param boolean $returnIndex [optional] Filter array by indexes.
     * @return mixed
     */
    public function filterCoalesce(array $array, array $inIndexes = null, $returnIndex = false)
    {
        foreach ($array as $index => $item) {
            if (isset($inIndexes) && is_array($inIndexes) && in_array($index, $inIndexes) === false) {
                continue;
            }

            if (empty($item) === false) {
                if ($returnIndex) {
                    return $index;
                }

                return $item;
            }
        }

        return;
    }

	function fix_ROIKToCase($record) {
        try {
			if (method_exists($record, 'getId')) {
				$recordId = $record->getId();
				if (empty($record->getParent()) === FALSE) {
					$recordParentId = $record->getParent()->getId();
				}
				if ($record->getDocument()->getId() === \Atp\Document::DOCUMENT_CASE) {
					if ($record->getParent()->getDocument()->getId() === \Atp\Document::DOCUMENT_PROTOCOL) {
						$check = array();
						$check = $this->db_getItemByColumn(PREFIX."ATP_ATPR_ATP", "RECORD_ID",	$recordId);
						if (empty($check['ROIK']) == TRUE) {
							$checkParent = $this->db_getItemByColumn(PREFIX."ATP_ATPR_ATP", "RECORD_ID", $recordParentId);
							$ins = array();
							$ins = $checkParent;
							$ins['RECORD_ID'] = $recordId;
							$this->db_quickInsert(PREFIX."ATP_ATPR_ATP", $ins);
						}
					}
				}
				if ($record->getDocument()->getId() === \Atp\Document::DOCUMENT_ACCOMPANYING) {
					if ($record->getParent()->getDocument()->getId() === \Atp\Document::DOCUMENT_PROTOCOL) {
						$check = array();
						$check = $this->db_getItemByColumn(PREFIX."ATP_ATPR_ATP", "RECORD_ID",	$recordId);
						if (empty($check['ROIK']) == TRUE) {
							$checkParent = $this->db_getItemByColumn(PREFIX."ATP_ATPR_ATP", "RECORD_ID", $recordParentId);
							$ins = array();
							$ins = $checkParent;
							$ins['RECORD_ID'] = $recordId;
							$this->db_quickInsert(PREFIX."ATP_ATPR_ATP", $ins);
						}
					}
					if ($record->getParent()->getDocument()->getId() === \Atp\Document::DOCUMENT_CASE) {
						$check = array();
						$check = $this->db_getItemByColumn(PREFIX."ATP_ATPR_ATP", "RECORD_ID",	$recordId);
						if (empty($check['ROIK']) == TRUE) {
							$checkParent = $this->db_getItemByColumn(PREFIX."ATP_ATPR_ATP", "RECORD_ID", $recordParentId);
							$ins = array();
							$ins = $checkParent;
							$ins['RECORD_ID'] = $recordId;
							$this->db_quickInsert(PREFIX."ATP_ATPR_ATP", $ins);
						}
					}
				}
			}
		} catch (\Exception $e) {
		}
	}
}
