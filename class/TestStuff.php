<?php

namespace Atp;

class TestStuff
{
    private $atp;

    private $em;

    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;
        $this->em = $atp->factory->Doctrine();
    }

    public function a()
    {
        $a = new \Atp\Atpr\AuthToken($this);
        $result = $a->get('test');
        var_dump($result);
        exit;
    }

    public function b()
    {
        $repository = $this->em->getRepository('\Atp\Entity\Atpr\Atp');
        $query = $repository->createQueryBuilder('u')
            ->select('u')
            ->where('u.id = 7');
        $atp = $query->getQuery()->getSingleResult();
        foreach ($atp->getMessages() as $message) {
            var_dump($message->getMessageId());
//            foreach ($message->getWorkers() as $worker) {
//                var_dump($worker->isRead());
//            }
        }
        exit;
    }

    public function c()
    {
        $repository = $this->em->getRepository('\Atp\Entity\Atpr\Message');
        $query = $repository->createQueryBuilder('u')
            ->select('u')
            ->where('u.messageId = 124927');
        $message = $query->getQuery()->getSingleResult();
        var_dump($message->getAtp()->getROIK());
        exit;
    }

    public function d()
    {
        $repository = $this->em->getRepository('\Atp\Entity\Atpr\Atp');
        $atp = $repository->findById(7);
        foreach ($atp as $a) {
            foreach ($a->getMessages() as $m) {
                var_dump($m->getMessageId());
            }
        }
        exit;
    }

    public function e()
    {
        $query = $this->em->createQuery('
            SELECT   a
            FROM     Atp\Entity\Atpr\Atp a
            JOIN     \Atp\Entity\Atpr\Message b
            WHERE a.id = 7');
        $arr = $query->getArrayResult();
        var_dump($arr);
        die;
        foreach ($query->getResult() as $a) {
            foreach ($a->getMessages() as $m) {
                var_dump($m->getMessageId());
            }
        }
        exit;
    }

    public function f()
    {
        $query = $this->em->createQueryBuilder();
        $query->select('atpr')
            ->from('\Atp\Entity\Atpr\Atp', 'atpr');
        $query->join('atpr.messages', 'pa');
        $query->where('atpr.id = 7');
        $atp = $query->getQuery()->getResult();
        foreach ($atp as $a) {
            foreach ($a->getMessages() as $m) {
                var_dump($m->getMessageId());
            }
        }
        exit;
    }

    public function g()
    {
        $query = $this->em->createQueryBuilder();
        $query->select('message')
            ->from('\Atp\Entity\Atpr\Message', 'message');
        $query->join('message.atp', 'atp');
        $query->where('message.messageId = 124927');
        $message = $query->getQuery()->getSingleResult();
        var_dump([
            $message->getAtp() instanceof Doctrine\Common\Collections\ArrayCollection,
            $message->getAtp() instanceof Atp\Entity\Atpr\Atp
        ]);
        exit;
    }

    public function h()
    {
        $query = $this->em->createQueryBuilder();
        $query->select('atpr')
            ->from('\Atp\Entity\Atpr\Atp', 'atpr');
        $query->join('atpr.record', 'atp');
        $query->where('atpr.id = 6');
        $atp = $query->getQuery()->getSingleResult();
//        var_dump($query->getQuery()->getArrayResult());
        var_dump($atp->getRecord()->getId());
        exit;
    }

    public function h2()
    {
        $query = $this->em->createQueryBuilder();
        $query->select('atp.id')
            ->from('\Atp\Entity\Atpr\Atp', 'atpr');
        $query->join('atpr.record', 'atp');
        $query->where('atpr.id = 6');
        $atp = $query->getQuery()->getResult();
        var_dump($atp); // is array
        exit;
    }

    public function gedmoUpload()
    {
        require_once "/srv/www/idamas/webPartner2/subsystems/atp/class/Atpr/Document/FileGedmo.php";
        $em = $this->atp->factory->_doctorine_gedmo();

        $listener = new \Gedmo\Uploadable\UploadableListener();
        $listener->setDefaultPath(__DIR__ . '/../files/tmp');

        if (isset($_FILES['file'])) {
            $file = new \Atp\Entity\FileGedmo();
//                $listener->addEntityFileInfo($file, $_FILES['file']);
            $listener->addEntityFileInfo(
                $file, new \Gedmo\Uploadable\FileInfo\FileInfoArray($_FILES['file'])
            );
//            var_dump($file,$_FILES['file'],new \Gedmo\Uploadable\FileInfo\FileInfoArray($_FILES['file']));

            $em->persist($file);
            $em->flush();
        } else {
            echo '<html><body><form method="post" enctype="multipart/form-data" action="TestStuff.php"><input name="file" type="file"><input type="submit"></form></body></html>';
        }
    }

    public function i()
    {
        $roik = '15000005080';
        $fileId = '22';

        $entityManager = $this->atp->factory->Doctrine();
        $repository = $entityManager->getRepository('Atp\Entity\Atpr\Atp');
        $entity = $repository->find($roik);
        $entity->addDocument(
            $entityManager->getReference('Atp\Entity\File', $fileId)
        );
        $entityManager->persist($entity);
        $entityManager->flush($entity);
    }

    public function j()
    {
        $entityManager = $this->atp->factory->Doctrine();
        $repository = $entityManager->getRepository('Atp\Entity\Atpr\Atp');

        '
SELECT
    FILE.*,
    ATPR.ROIK
FROM
    IDM_ATP_ATPR_ATP ATPR
        INNER JOIN IDM_ATP_ATPR_ATP_RECORD_XREF ATPR_ATP ON     ##
            ATPR_ATP.ROIK = ATPR.ROIK                           #
        INNER JOIN IDM_ATP_RECORD ATP ON
            ATP.ID = ATPR_ATP.RECORD_ID
        INNER JOIN IDM_ATP_RECORD_FILES ATP_FILE ON             ##
            ATP_FILE.RECORD_ID = ATP.ID                         #
        INNER JOIN IDM_ATP_FILES FILE ON
            FILE.ID = ATP_FILE.FILE_ID
        LEFT JOIN IDM_ATP_ATPR_ATP_DOCUMENT_XREF ATPR_FILE ON   ##
            ATPR_FILE.FILE_ID = FILE.ID AND                     #
            ATPR_FILE.ROIK = ATPR.ROIK                          #
WHERE
    ATPR.ROIK = "15000005080" AND
    ATPR_FILE.ROIK IS NULL
';
        $query = $entityManager->createQueryBuilder()
//            ->select('FILE') // BAD
//            ->select('FILE.id, FILE.name, FILE.path, FILE.type, FILE.size, ATPR.roik')
//            ->select('FILE', 'ATPR')
            ->select('ATPR', 'ATP', 'ATP_FILE', 'FILE')
//            ->select('ATP_FILE')
            ->from('Atp\Entity\Atpr\Atp', 'ATPR')
            ->innerJoin('ATPR.record', 'ATP')
            ->innerJoin('ATP.files', 'ATP_FILE')
            ->innerJoin('ATP_FILE.file', 'FILE')
            ->leftJoin(
                'ATPR.documents', 'ATPR_FILE', 'WITH', 'ATPR_FILE.file = FILE.id AND ' .
                'ATPR_FILE.roik = ATPR.roik'
            )
            ->andWhere('ATPR.roik = :roik')
            ->andWhere('ATPR_FILE.roik IS NULL')
            ->setParameter('roik', '15000005080');



        $query = $entityManager->createQueryBuilder()
            ->select('FILE')
            ->from('Atp\Entity\File', 'FILE')
            ->innerJoin('FILE.records', 'FILE_RECORD')
            ->innerJoin('FILE_RECORD.record', 'ATP')
            ->innerJoin('ATP.atpr', 'ATPR')
            ->leftJoin(
                'ATPR.documents', 'ATPR_FILE', 'WITH', 'ATPR_FILE.file = FILE.id AND ' .
                'ATPR_FILE.roik = ATPR.roik'
            )
            ->andWhere('ATPR.roik = :roik')
            ->andWhere('ATPR_FILE.roik IS NULL')
            ->setParameter('roik', '15000005080');
        var_dump($query->getQuery()
                ->getSql()
//            ->getResult()
//            ->getArrayResult()
        );
        exit;

        /*
          SELECT
          i0_.ID FILE_ID,
          i2_.ID RECORD_ID,
          i3_.ROIK ATPR,
          i6_.FILE_ID ATPR_FILE
          FROM   IDM_ATP_FILES i0_
          INNER JOIN IDM_ATP_RECORD_FILES i1_
          ON i0_.ID = i1_.FILE_ID
          INNER JOIN IDM_ATP_RECORD i2_
          ON i1_.RECORD_ID = i2_.ID
          INNER JOIN IDM_ATP_ATPR_ATP_RECORD_XREF i4_
          ON i2_.ID = i4_.RECORD_ID
          INNER JOIN IDM_ATP_ATPR_ATP i3_
          ON i3_.ROIK = i4_.ROIK
          LEFT JOIN IDM_ATP_ATPR_ATP_DOCUMENT_XREF i6_
          ON i3_.ROIK = i6_.ROIK
          LEFT JOIN IDM_ATP_FILES i5_
          ON i5_.ID = i6_.FILE_ID
          WHERE  i3_.ROIK = 15000005080
         */

        $query = $entityManager->createQueryBuilder()
            ->select('FILE')
            ->from('Atp\Entity\File', 'FILE')
            ->innerJoin('FILE.records', 'FILE_RECORD')
            ->innerJoin('FILE_RECORD.record', 'ATP')
            ->innerJoin('ATP.atpr', 'ATPR')
            ->leftJoin(
                'ATPR.documents', 'ATPR_FILE', 'WITH', 'ATPR_FILE.FILE_ID = FILE.id AND ' .
                'ATPR_FILE.ROIK = ATPR.roik'
            )
            ->andWhere('ATPR.roik = :roik')
            ->andWhere('ATPR_FILE.id IS NULL')
            ->setParameter('roik', '15000005080');

        var_dump($query->getQuery()
//            ->getSql()
//            ->getResult()
                ->getArrayResult()
        );
        exit;
    }

    function k()
    {
        $entityManager = $this->atp->factory->Doctrine();

        // was RDO_INC
        $avilysDocument = $entityManager->getReference('Atp\Entity\Avilys\Document', '8232880094fb11e5b1f5a27b12a0020b');
        $avilysDocument->setDocumentCategory('test');
        $entityManager->persist($avilysDocument);
        $entityManager->flush();

        $avilysDocument->setDocumentCategory('RDO_INC');
        $entityManager->persist($avilysDocument);
        $entityManager->flush();
    }

    function l()
    {
        $internalUrl = 'http://wp.dev//subsystems/atp/index.php?m=5&id=36p5';

        $dhs = prepare_dhs();

        $urlPart = parse_url($internalUrl);

        // Detect subsystem
        $subsystemId = null;
        if (preg_match('#/+subsystems/(.*?)/#', $urlPart['path'], $matches)) {
            $subsystemName = $matches[1];
            if (empty($subsystemName) === false) {
                $result = $dhs->db_query_fast('SELECT ID
                    FROM ' . PREFIX . 'WP_SOFT
                    WHERE
                        SOFT_DATA LIKE "%subsystems/' . $subsystemName . '/index.php%" AND
                        VISIBLE = 1');
                if ($dhs->db_num_rows($result) === 1) {
                    list($subsystemId) = array_values($dhs->db_next($result));
                }
            }
        }

        if (empty($subsystemId) === false) {
            // Extract query
            parse_str($urlPart['query'], $query);

            $urlQuery = [
                'm' => 5,
                'id' => $subsystemId,
                'redirect_url' => 'index.php?' . http_build_query($query, null, '|')
            ];

            $secureStr = $urlQuery;
            array_walk(
                $secureStr, function(&$value, $key) {
                $value = '(' . $key . '|' . $value . ')';
            });


            $urlQuery['secure'] = $dhs->makeHash(implode($secureStr));
            var_dump(GLOBAL_SITE_URL);
            $url = GLOBAL_SITE_URL . 'subsystems/wp/index.php?' . http_build_query($urlQuery);

            var_dump(['$urlQuery' => $urlQuery, '$secureStr' => $secureStr, '$url' => $url]);
        }
    }

    public function m()
    {
        $entityManager = $this->atp->factory->Doctrine();
        $repository = $entityManager->getRepository('Atp\Entity\Avilys\Document');
        $repository instanceof \Atp\Repository\Avilys\DocumentRepository;

        $avilysDocument = $repository->findOneBy(['record' => 36]);
        if ($avilysDocument) {
            echo "200\r\n";
        } else {
            echo "404\r\n";
        }
    }

    public function n()
    {

        $entityManager = $this->atp->factory->Doctrine();
        $repository = $entityManager->getRepository('Atp\Entity\Avilys\Document');
        $repository instanceof \Atp\Repository\Avilys\DocumentRepository;

        $result = $repository->isByRecordId(36);

        var_dump($result);
    }

    public function o()
    {
        $entityManager = $this->atp->factory->Doctrine();
        $repository = $entityManager->getRepository('Atp\Entity\FieldConfiguration');
        $fieldId = 2473;

        $s1 = microtime(true);

        $queryBuilder = $repository->createQueryBuilder('configuration')
            ->select(array('configuration', 'type'))
            ->innerJoin('configuration.type', 'type')
            ->where('configuration.fieldId = :fieldId')
            ->setParameter('fieldId', $fieldId, \Doctrine\DBAL\Types\Type::INTEGER);
        $query = $queryBuilder->getQuery();

        $query = $entityManager->createQuery("
            SELECT
                configuration, type
            FROM
                Atp\Entity\FieldConfiguration configuration
                    INNER JOIN configuration.type type
            WHERE
                configuration.fieldId = :fieldId")
            ->setParameter('fieldId', $fieldId, \Doctrine\DBAL\Types\Type::INTEGER);

        $query->execute();
        $s2 = microtime(true);

        var_dump($query->getSQL());
        /* @var $configurations \Atp\Entity\FieldConfiguration[] */
        $configurations = $query->getResult();

        var_dump(count($configurations));
        foreach ($configurations as $configuration) {
            var_dump($configuration->getType()->getName());
        }
        $s3 = microtime(true);
        var_dumP($s2 - $s1, $s3 - $s1);
    }

    public function p()
    {
        $entityManager = $this->atp->factory->Doctrine();
        /* @var $repository \Atp\Repository\FieldConfigurationRepository */
        $repository = $entityManager->getRepository('Atp\Entity\FieldConfiguration');
        $fieldId = 2473;

        $configurations = $repository->getByField($fieldId);


        foreach ($configurations as $configuration) {
            var_dump($configuration->getType()->getName());
        }
    }

    public function r()
    {
        $string = base64_encode('o tu kurmi');

        $tempFile = fopen('php://temp', 'r+');
        fwrite($tempFile, base64_decode($string));

        rewind($tempFile);
        $metaData = stream_get_meta_data($tempFile);
        $content = stream_get_contents($tempFile);

        var_dump($metaData, $content);
    }

    public function r2()
    {
        $string = base64_encode('o tu kurmi');

        $tempFile = tmpfile();
        fwrite($tempFile, base64_decode($string));

        rewind($tempFile);
        $metaData = stream_get_meta_data($tempFile);
        $content = stream_get_contents($tempFile);

        var_dump($metaData, $content);
    }
    public function s()
    {
        $documentManager = $this->atp->factory->Document();
        $paths = $documentManager->getPathFromDocument(36);

        var_dump(array_map(function($a) { return [ $a['ID'], $a['PARENT_ID']];}, $paths));
    }
}

require(__DIR__ . '/../../../inc/mainconf.php');

ini_set('display_errors', true);
$atp = prepare_atp();
//$atp->init();
$test = new TestStuff($atp);
$test->s();
