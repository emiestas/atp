<?php

namespace Atp;

use \Doctrine;
use \Gedmo;

class FactoryStorage
{
    private $atp;

    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;
    }

    /**
     * @return \Atp\Cache
     */
    public function Cache()
    {
        if (isset($this->cache) === false) {
            $this->cache = new Cache();
        }
        return $this->cache;
    }

    /**
     * @return \Atp\User
     */
    public function User()
    {
        if (isset($this->user) === false) {
            $this->user = new \Atp\User($this->atp);
        }
        return $this->user;
    }

    /**
     * @return \Atp\Document
     */
    public function Document()
    {
        if (isset($this->document) === false) {
            $this->document = new Document($this->atp);
        }
        return $this->document;
    }

    /**
     * @return \Atp\Document\Field
     */
    public function Field()
    {
        if (isset($this->field) === false) {
            $this->field = new Document\Field($this->atp);
        }
        return $this->field;
    }

    /**
     * @param \Atp\Core $atp
     * @return \Atp\AdminJournal
     */
    public function AdminJournal()
    {
        if (isset($this->adminJournal) === false) {
            $this->adminJournal = new AdminJournal($this->atp);
        }
        return $this->adminJournal;
    }

    /**
     * @return \Atp\Record
     */
    public function Record()
    {
        if (isset($this->record) === false) {
            $this->record = new Record($this->atp);
        }
        return $this->record;
    }

    /**
     * @return \Atp\File
     */
    public function File()
    {
        if (isset($this->file) === false) {
            $this->file = new File($this->atp);
        }
        return $this->file;
    }

    /**
     * @return \Atp\Performance
     */
    public function Performance()
    {
        if (isset($this->performance) === false) {
            $this->performance = new Performance();
        }
        return $this->performance;
    }

    /**
     * @param string $templatePath [optional]
     * @return \Atp\Helper\Template
     */
    public function Template($templatePath = null)
    {
        require_once($this->atp->config['REAL_URL'] . 'helper/template.php');
        $this->template = new \Atp\Helper\Template(
            (empty($templatePath) ? $this->atp->config['TMPL_URL'] : $templatePath), 'comment'
        );

        return $this->template;
    }

    /**
     * @todo Remove $new and $setNames. Is needed for AddressHelper since TST_VAR_DATA is utf-8 table with utf-8 data. But TST client uses utf-8 tables with latin1 data.
     * @return \Doctrine\ORM\EntityManager
     */
    public function Doctrine($new = false, $setNames = null)
    {
        if ($new) {
            return $this->_doctorine_gedmo($setNames);
        }

        if (isset($this->doctrine) === false) {
            $this->doctrine = $this->_doctorine_gedmo();
        }

        return $this->doctrine;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function _doctorine_gedmo($setNames = null)
    {
        $reflector = new \ReflectionClass('\Doctrine\ORM\Mapping\Driver\DriverChain');
        \Doctrine\Common\Annotations\AnnotationRegistry::registerFile(dirname($reflector->getFileName()) . '/DoctrineAnnotations.php');

        //// WARNING: setup, assumes that autoloaders are set
        // globally used cache driver, in production use APC or memcached
        $cache = new Doctrine\Common\Cache\ArrayCache;
//        $cache = new Doctrine\Common\Cache\VoidCache();
        // standard annotation reader
        $annotationReader = new Doctrine\Common\Annotations\AnnotationReader();
        $cachedAnnotationReader = new Doctrine\Common\Annotations\CachedReader(
            $annotationReader, // use reader
            $cache // and a cache driver
        );
        // create a driver chain for metadata reading
        $driverChain = new Doctrine\ORM\Mapping\Driver\DriverChain();
        // load superclass metadata mapping only, into driver chain
        // also registers Gedmo annotations.NOTE: you can personalize it
        Gedmo\DoctrineExtensions::registerAbstractMappingIntoDriverChainORM(
            $driverChain, // our metadata driver chain, to hook into
            $cachedAnnotationReader // our cached annotation reader
        );

        // now we want to register our application entities,
        // for that we need another metadata driver used for Entity namespace
        $annotationDriver = new Doctrine\ORM\Mapping\Driver\AnnotationDriver(
            $cachedAnnotationReader, // our cached annotation reader
            array(__DIR__ . '/Entity') // paths to look in
        );

        // NOTE: driver for application Entity can be different, Yaml, Xml or whatever
        // register annotation driver for our application Entity namespace
        $driverChain->addDriver($annotationDriver, 'Atp\Entity');

        // general ORM configuration
        $config = new Doctrine\ORM\Configuration;
        $config->setProxyDir(sys_get_temp_dir());
        $config->setProxyNamespace('Proxy');
        $config->setAutoGenerateProxyClasses(true); // this can be based on production config.
        // register metadata driver
        $config->setMetadataDriverImpl($driverChain);
        // use our already initialized cache driver
        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);

        // create event manager and hook preferred extension listeners
        $evm = new Doctrine\Common\EventManager();

        // tree
        $treeListener = new Gedmo\Tree\TreeListener;
        $treeListener->setAnnotationReader($cachedAnnotationReader);
        $evm->addEventSubscriber($treeListener);
        
        // timestampable
        $timestampableListener = new Gedmo\Timestampable\TimestampableListener;
        $timestampableListener->setAnnotationReader($cachedAnnotationReader);
        $evm->addEventSubscriber($timestampableListener);

        // gedmo extension listeners, remove which are not used
        //// sluggable
        //        $sluggableListener = new Gedmo\Sluggable\SluggableListener;
        //// you should set the used annotation reader to listener, to avoid creating new one for mapping drivers
        //        $sluggableListener->setAnnotationReader($cachedAnnotationReader);
        //        $evm->addEventSubscriber($sluggableListener);

//        $classLoader = new \Doctrine\Common\ClassLoader('DoctrineExtensions', '/srv/www/idamas/webPartner2/vendor/beberlei/DoctrineExtensions/src');
//        $classLoader->register();
        //// loggable, not used in example
        //        $loggableListener = new Gedmo\Loggable\LoggableListener;
        //        $loggableListener->setAnnotationReader($cachedAnnotationReader);
        //        $evm->addEventSubscriber($loggableListener);
        //

        //// translatable
        //        $translatableListener = new Gedmo\Translatable\TranslatableListener;
        //// current translation locale should be set from session or hook later into the listener
        //// most important, before entity manager is flushed
        //        $translatableListener->setTranslatableLocale('en');
        //        $translatableListener->setDefaultLocale('en');
        //        $translatableListener->setAnnotationReader($cachedAnnotationReader);
        //        $evm->addEventSubscriber($translatableListener);
        //
    //// sortable, not used in example
        //        $sortableListener = new Gedmo\Sortable\SortableListener;
        //        $sortableListener->setAnnotationReader($cachedAnnotationReader);
        //        $evm->addEventSubscriber($sortableListener);

        // mysql set names UTF-8 if required
        if ($setNames === true || defined('SET_NAMES_UTF8') === TRUE) {
            $evm->addEventSubscriber(new Doctrine\DBAL\Event\Listeners\MysqlSessionInit());
        }

        // DBAL connection
        $connection = array(
            'driver' => 'mysqli',
            'host' => DB_HOST,
            'dbname' => DB_DB,
            'user' => DB_USER,
            'password' => DB_PASS
        );
        if ($connection['host'] === 'localhost') {
            $connection['host'] = '127.0.0.1';
        }

        // client prefix
        $tablePrefix = new DoctrineExtensions\TablePrefix(PREFIX);
        $evm->addEventListener(\Doctrine\ORM\Events::loadClassMetadata, $tablePrefix);

        // Custom functions
        $config->addCustomStringFunction('COLLATE', \Atp\DoctrineExtensions\DQL\CollateFunction::class);

        // Finally, create entity manager
        $em = Doctrine\ORM\EntityManager::create($connection, $config, $evm);

        return $em;
    }
}
