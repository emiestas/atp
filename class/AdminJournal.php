<?php

namespace Atp;

use \Atp\Entity\AdminJournalRecord;
use \WP_Log as Logger;
use \Atp\Core as AtpCore;

class AdminJournal
{
    /**
     * @var Logger
     */
    private static $logger;

    /**
     * @var AtpCore
     */
    private $atp;

    public function __construct(AtpCore $atp)
    {
        $this->atp = $atp;

        if (self::$logger === null) {
            self::$logger = new Logger();
        }
    }

    /**
     * @param AdminJournalRecord $record
     */
    public function addRecord(AdminJournalRecord $record)
    {
        self::$logger->writeAction(
            $record->getMessage(),
            $record->getPersonName(),
            $record->getSubsystemName(),
            null,
            $record->getUrl()
        );
    }

    /**
     * Get AdminJournalRecord entity with default parameters (person name, subsystem name, url) for ATP system.
     * @return AdminJournalRecord
     */
    public function getDefaultRecord()
    {
        $personName = implode(
            ' ',
            array_filter(
                [
                    $this->atp->factory->User()->getLoggedUser()->firstName,
                    $this->atp->factory->User()->getLoggedUser()->lastName
                ]
            )
        );

        return new AdminJournalRecord(
            null,
            $personName,
            $_SERVER['REQUEST_URI'],
            'ATP'
        );
    }

    public function getVisitRecord($message)
    {
        return $this->getDefaultRecord($message)
                ->setMessage('Atidarė: ' . $message);
    }

    public function getCreateRecord($message)
    {
        return $this->getDefaultRecord($message)
                ->setMessage('Sukūrė: ' . $message)
                ->setUrl(null);
    }

    public function getEditRecord($message)
    {
        return $this->getDefaultRecord($message)
                ->setMessage('Redagavo: ' . $message)
                ->setUrl(null);
    }

    public function getSaveRecord($message)
    {
        return $this->getDefaultRecord($message)
                ->setMessage('Išsaugojo: ' . $message)
                ->setUrl(null);
    }

    public function getDeleteRecord($message)
    {
        return $this->getDefaultRecord($message)
                ->setMessage('Ištrynė: ' . $message)
                ->setUrl(null);
    }

    public function getMessageRecord($message)
    {
        return $this->getDefaultRecord($message)
                ->setMessage('Pranešimas: ' . $message)
                ->setUrl(null);
    }

    /**
     *
     * @param string $url
     * @return AdminJournalRecord
     */
    public function getRecordByUrl($url)
    {
        $query = parse_url($url, PHP_URL_QUERY);

//        $message = $this->parseQueryToMessage($query);

        $record = $this->getDefaultRecord();
        $record->setMessage($message);

        return $record;
    }
//
//    /**
//     *
//     * @param array $query
//     * @return boolean|string
//     */
//    private function parseQueryToMessage(array $query)
//    {
//        if(!isset($query['m'])) {
//            return false;
//        }
//
//        switch($query['m']) {
//            case 1:
////                $a = filter_input(INPUT_GET, 'a');
////                if()
//
//                break;
//            case '':
//                break;
//            case '':
//                break;
//            case '':
//                break;
//            case '':
//                break;
//            default:
//        }
//    }
}
