<?php

namespace Atp\Address\Model;

class BuildingModel
{
    /**
     * @var int
     */
    protected $number;

    /**
     * @var char
     */
    protected $letter;

    /**
     * @var int
     */
    protected $complexNumber;

    /**
     *
     * @param int $number
     * @param char $letter
     * @param int $complexNumber
     */
    public function __construct($number, $letter = null, $complexNumber = null)
    {
        $this->number = $number;
        $this->letter = $letter;
        $this->complexNumber = $complexNumber;
    }

    /**
     * Get building number.
     * 
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Get building letter.
     *
     * @return char|null
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * Get building number in complex.
     * 
     * @return int|null
     */
    public function getComplexNumber()
    {
        return $this->complexNumber;
    }

    /**
     * Get line for address.
     *
     * @param ApartmentModel $apartment [optional]
     * @return string
     */
    public function getLine(ApartmentModel $apartment = null)
    {
        return $this->getNumber()
            . $this->getLetter
            . ($apartment ? '-' . $apartment->getNumber() : null)
            . ($this->getComplexNumber() ? ' K' . $this->getComplexNumber() : null);
    }
}
