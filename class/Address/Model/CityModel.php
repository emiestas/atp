<?php

namespace Atp\Address\Model;

class CityModel
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $name;

    /**
     * @param string $code
     * @param type $name
     */
    public function __construct($code, $name)
    {
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * Get city code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get city name.
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
