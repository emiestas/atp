<?php

namespace Atp\Address\Model;

class ApartmentModel
{
    /**
     * @var string
     */
    protected $number;

    /**
     * @param string $number
     */
    public function __construct($number)
    {
        $this->number = $number;
    }

    /**
     * Get apartment number.
     * 
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }
}
