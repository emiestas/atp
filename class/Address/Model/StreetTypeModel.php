<?php

namespace Atp\Address\Model;

class StreetTypeModel
{
    /**
     * @var string 
     */
    protected $name;

    /**
     * @var string []
     */
    protected $shortNames;

    /**
     *
     * @param string $name
     * @param string[] $shortNames
     */
    public function __construct($name, array $shortNames)
    {
        $this->name = $name;
        $this->shortNames = $shortNames;
    }

    /**
     * Get street type name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get street type short names.
     * 
     * @return string[]
     */
    public function getShortNames()
    {
        return $this->shortNames;
    }
}
