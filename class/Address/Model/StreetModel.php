<?php

namespace Atp\Address\Model;

class StreetModel
{
    /**
     * @var int
     */
    protected $code;

    /**
     * @var string
     */
    protected $name;

    /**
     * @param string $code
     * @param type $name
     */
    public function __construct($code, $name)
    {
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * Get street code.
     *
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get street name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get street name without street type.
     *
     * @return string
     */
    public function getNameMainPart()
    {
        return trim(str_replace($this->getTypesShortNames(), '', $this->name));
    }

    /**
     * Get street name for address line.
     * 
     * @return string
     */
    public function getLine()
    {
        return $this->getName();
    }

    /**
     * Get street types short names.
     *
     * @return string[]
     */
    private function getTypesShortNames()
    {
        $shortNames = [];
        foreach ($this->getTypes() as $streetType) {
            foreach ($streetType->getShortNames() as $shortName) {
                $shortNames[] = $shortName;
            }
        }

        return $shortNames;
    }

    /**
     * Get street types.
     * 
     * @return StreetTypeModel[]
     */
    private function getTypes()
    {
        return
            [
                new StreetTypeModel('gatvė', ['g.']),
                new StreetTypeModel('skersgatvis', ['skg.']),
                new StreetTypeModel('akligatvis', ['aklg.']),
                new StreetTypeModel('alėja', ['al.']),
                new StreetTypeModel('prospektas', ['pr.']),
                new StreetTypeModel('aikštė', ['a.']),
                new StreetTypeModel('skveras', ['skv.']),
                new StreetTypeModel('takas', ['tak.']),
                new StreetTypeModel('plentas', ['pl.']),
                new StreetTypeModel('kelias', ['kel.', 'kl.']),
                new StreetTypeModel('aplinkkelis', ['aplinkl.']),
                new StreetTypeModel('vieškelis', ['vieškl.']),
                new StreetTypeModel('krantinė', ['krant.'])
        ];
    }
}
