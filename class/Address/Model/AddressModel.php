<?php

namespace Atp\Address\Model;

class AddressModel
{
    /**
     * @var CityModel
     */
    protected $city;

    /**
     * @var StreetModel
     */
    protected $street;

    /**
     * @var BuildingModel
     */
    protected $building;

    /**
     * @var ApartmentModel
     */
    protected $apartment;

    /**
     * @var CoordinatesModel
     */
    protected $coordinates;

    /**
     * @param CityModel $city
     * @param StreetModel $street
     * @param BuildingModel $building
     * @param ApartmentModel $apartment [optional]
     * @param CoordinatesModel $coordinates [optional]
     */
    public function __construct(CityModel $city, StreetModel $street, BuildingModel $building, ApartmentModel $apartment = null, CoordinatesModel $coordinates = null)
    {
        $this->city = $city;
        $this->street = $street;
        $this->building = $building;
        $this->apartment = $apartment;
        $this->coordinates = $coordinates;
    }

    /**
     * Get address line.
     * 
     * @return string
     */
    public function getLine()
    {
        return $this->getCity()->getName() . ', '
            . $this->getStreet()->getLine() . ' '
            . $this->getBuilding()->getLine($this->getApartment());
    }

    /**
     * @return CityModel
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return StreetModel
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @return BuildingModel
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * @return ApartmentModel|null
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * @return CoordinatesModel|null
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }
}
