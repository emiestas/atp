<?php

namespace Atp\Address\Model;

class CoordinatesModel
{
    /**
     * @var int
     */
    protected $x;

    /**
     * @var int
     */
    protected $y;

    /**
     *
     * @param int $x
     * @param int $y
     */
    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * Get X coordinate.
     *
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Get Y coordinate.
     *
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }
}
