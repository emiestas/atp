<?php

namespace Atp\Address;

use Atp\Address\Exception\AddressException,
    Atp\Address\Model\AddressModel,
    Atp\Address\Model\ApartmentModel,
    Atp\Address\Model\BuildingModel,
    Atp\Address\Model\CityModel,
    Atp\Address\Model\CoordinatesModel,
    Atp\Address\Model\StreetModel,
    Atp\Entity\Address,
    Atp\Repository\AddressRepository;

class AddressHelper
{
    /**
     * @var AddressRepository
     */
    protected $repository;

    /**
     * @param AddressRepository $repository
     */
    public function __construct(AddressRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get street by GRT code.
     * 
     * @param int $streetCode
     * @return StreetModel
     */
    public function getStreetByGrtCode($streetCode)
    {
        /* @var $address Address */
        $address = $this->repository->findOneBy(['gat' => $streetCode]);

        return new StreetModel($address->getGat(), $address->getGatve());
    }

    /**
     * Get city by GRT code.
     * 
     * @param int $cityCode
     * @return CityModel
     */
    public function getCityByGrtCode($cityCode)
    {
        if (substr($cityCode, 0, 1) === 'G') {
            $cityCode = substr($cityCode, 1);
        }

        /* @var $address Address */
        $address = $this->repository->findOneBy(['gyvId' => $cityCode]);

        return new CityModel($address->getGyvId(), $address->getGyvPav());
    }

//    /**
//     * Get district name by GRT code.
//     * 
//     * @param int $districtCode
//     * @return string
//     */
//    public function getDistrictNameByGrtCode($districtCode)
//    {
//        /* @var $address Address */
//        $address = $this->repository->findOneBy(['seniunijanr' => $districtCode]);
//        return $address->getSeniunija();
//    }


    /**
     * Get address object by GRT data.
     *
     * @param int $cityCode GRT city id.
     * @param int $streetCode GRT street id.
     * @param string $buildingNumber [optional]
     * @param string $apartmentNumber [optional]
     * @return AddressModel
     */
    public function getAddressByGrtCodes($cityCode, $streetCode, $buildingNumber, $apartmentNumber = null) {
        $city = $this->getCityByGrtCode($cityCode);
        $street = $this->getStreetByGrtCode($streetCode);
        $building = null;
        $apartment = null;
        $coordinates = null;
        
        $buildingMatches = [];
        $regex = '/'
            . '(?<number>[0-9]+)'
            . '(?<letter>[A-Za-z]+)?'
            . '/';

        if (preg_match($regex, $buildingNumber, $buildingMatches)) {
            $building = new BuildingModel(
                $buildingMatches['number'],
                empty($buildingMatches['letter']) ? null : $buildingMatches['letter']
            );

            $address = $this->getAddress($city, $street, $building);
            if ($address) {
                $coordinates = new CoordinatesModel($address->getXKoord(), $address->getYKoord());
            }
        }

        if ($apartmentNumber) {
            $apartment = new ApartmentModel($apartmentNumber);
        }

        return new AddressModel($city, $street, $building, $apartment, $coordinates);
    }

    /**
     * Get address object by address line.
     * 
     * @param string $address
     * @return AddressModel
     * @throws AddressException
     */
    public function getAddressByString($address)
    {
        $cityMatches = [];
        $foundCity = null;
        foreach ($this->getCities() as $city) {
            if (mb_strpos($address, $city->getName()) === false) {
                continue;
            }

            $cityMatches[] = $city;
        }

        $cityMatchesCount = count($cityMatches);
        if ($cityMatchesCount === 0) {
            throw new AddressException('City not found');
        }
        if ($cityMatchesCount > 1) {
            throw new AddressException('Too many city matches found');
        }

        $city = $cityMatches[0];
        $address = str_ireplace($city->getName(), '', $address);

        $streetMatches = [];
        foreach ($this->getStreetsInCity($city) as $street) {
            if (mb_strpos($address, $street->getNameMainPart()) === false) {
                continue;
            }

            $streetMatches[] = $street;
        }

        $streetMatchesCount = count($streetMatches);
        if ($streetMatchesCount === 0) {
            throw new AddressException('Street not found');
        }
        if ($streetMatchesCount > 1) {
            throw new AddressException('Too many street matches found');
        }

        $street = $streetMatches[0];
        $address = str_ireplace($street->getName(), '', $address);

        $buildingMatches = [];
        $regex = '/'
            . '(?<number>[0-9]+)'
            . '(?<letter>[A-Za-z]+)?'
            . '(?:\s*-\s*(?<apartment>[0-9]+))?'
            . '(?:\s+[Kk](?<complex>[0-9]+))?'
            . '/';
        if (!preg_match($regex, $address, $buildingMatches)) {
            throw new AddressException('Building number not found');
        }

        $building = new BuildingModel(
            $buildingMatches['number'],
            empty($buildingMatches['letter']) ? null : $buildingMatches['letter'],
            empty($buildingMatches['complex']) ? null : $buildingMatches['complex']
        );

        $address = $this->getAddress($city, $street, $building);

        $apartment = null;
        if ($buildingMatches['apartment']) {
            $apartment = new ApartmentModel($buildingMatches['apartment']);
        }
        $coordinates = new CoordinatesModel($address->getXKoord(), $address->getYKoord());

        return new AddressModel($city, $street, $building, $apartment, $coordinates);
    }

    /**
     * Get list of available cities in addresses registry.
     * 
     * @return CityModel[]
     */
    private function getCities()
    {
        $cities = $this->repository->createQueryBuilder('address')
            ->select('address.gyvId, address.gyvPav')
            ->distinct()
            ->andWhere('address.gyvId IS NOT NULL')
            ->getQuery()
            ->getArrayResult();

        return array_map(function($city) {
            return new CityModel($city['gyvId'], $city['gyvPav']);
        }, $cities);
    }

    /**
     * Get list of available streets for city in addresses registry.
     *
     * @param CityModel $city
     * @return StreetModel[]
     */
    private function getStreetsInCity(CityModel $city)
    {
        $cities = $this->repository->createQueryBuilder('address')
            ->select('address.gat, address.gatve')
            ->where('address.gyvId = :cityCode')
            ->setParameter('cityCode', $city->getCode())
            ->distinct()
            ->getQuery()
            ->getArrayResult();

        return array_map(function($city) {
            return new StreetModel($city['gat'], $city['gatve']);
        }, $cities);
    }

    /**
     * Get address by address models in addresses registry.
     * 
     * @param CityModel $city
     * @param StreetModel $street
     * @param BuildingModel $building
     * @return Address
     */
    private function getAddress(CityModel $city, StreetModel $street, BuildingModel $building)
    {
        $queryBuilder = $this->repository->createQueryBuilder('address')
            ->select('address')
            ->andWhere('address.gyvId = :cityCode')
            ->andWhere('address.gat = :streetCode')
            ->andWhere('address.namoNr = :buildingNumber')
            ->setParameter('cityCode', $city->getCode())
            ->setParameter('streetCode', $street->getCode())
            ->setParameter('buildingNumber', $building->getNumber());

        if (!$building->getLetter()) {
            $queryBuilder->andWhere('address.namoR is NULL');
        } else {
            $queryBuilder
                ->andWhere('address.namoR = :buildingLetter')
                ->setParameter('buildingLetter', $building->getLetter());
        }
        if (!$building->getComplexNumber()) {
            $queryBuilder->andWhere('address.korpusoNr is NULL');
        } else {
            $queryBuilder
                ->andWhere('address.korpusoNr = :complexNumber')
                ->setParameter('complexNumber', $building->getComplexNumber());
        }

        $building = $queryBuilder->getQuery()
            ->getSingleResult();

        return $building;
    }
}
