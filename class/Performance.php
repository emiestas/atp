<?php

namespace Atp;

class Performance
{
    public function start($name = null, $parent_id = null)
    {
        if ($name === null) {
            $name = microtime();
        }

        $id = str_replace('.', '', microtime(true));
        $this->execution[$id] = [
            'name' => $name,
            'parent' => $parent_id,
            'start' => [
                'time' => microtime(true),
                'memory' => memory_get_usage()
            ]
        ];

        return $id;
    }

    public function finish($id)
    {
        $time = microtime(true);
        $memory = memory_get_usage();

        if (isset($this->execution[$id]) === false) {
            return;
        }

        $this->execution[$id]['end'] = [
            'time' => $time,
            'memory' => $memory
        ];

        $this->parse_exec($id);
    }

    private function parse_exec($id)
    {
        $data = $this->execution[$id];

        $this->execution[$id]['time'] = $data['end']['time'] - $data['start']['time'];

        $unit = ['b', 'kb', 'mb', 'gb', 'tb', 'pb'];
        $diff = $data['end']['memory'] - $data['start']['memory'];
        if ($diff > 0) {
            $diff = @round($diff / pow(1024, ($i = floor(log($diff, 1024)))), 2) . ' ' . $unit[$i];
        }

        $this->execution[$id]['memory'] = $diff;
    }

    public function echo_exec($id = null)
    {
        if ($id !== null) {
            $data = [$this->execution[$id]];
        } else {
            $data = $this->execution;
        }

        $str = null;
        foreach ($data as $a) {
            $str .= ' # [' . $a['start']['time'] . '] ' . $a['name'] . PHP_EOL . ' ## Time: ' . $a['time'] . ' s' . PHP_EOL . ' ## Memory: ' . $a['memory'] . PHP_EOL . PHP_EOL;
        }

        echo $str;
    }
}
