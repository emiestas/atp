<?php

namespace Atp\File;

class Record
{
    private $atp;

    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;
    }

    public function GetByRecord($record_id)
    {
        $qry = 'SELECT *
			FROM
				' . PREFIX . 'ATP_RECORD_FILES
			WHERE
				RECORD_ID = :RID';

        $param = [
            'RID' => (int) $record_id,
        ];
        $result = $this->atp->db_query_fast($qry, $param);

        $data = [];
        $count = $this->atp->db_num_rows($result);
        if ($count) {
            for ($i = 0; $i < $count; $i++) {
                $row = $this->atp->db_next($result);
                $data[] = (object) [
                        'id' => (int) $row['ID'],
                        'file_id' => (int) $row['FILE_ID'],
                        'record_id' => (int) $row['RECORD_ID'],
                        'field_id' => (int) $row['FIELD_ID']
                ];
            }
        }

        return $data;
    }

    public function Save($file_id, $record_id, $field_id)
    {
        $qry = 'SELECT ID
			FROM
				' . PREFIX . 'ATP_RECORD_FILES
			WHERE
				RECORD_ID = :RECORD_ID AND
				FIELD_ID = :FIELD_ID AND
				FILE_ID = :FILE_ID';

        $param = [
            'RECORD_ID' => (int) $record_id,
            'FIELD_ID' => (int) $field_id,
            'FILE_ID' => (int) $file_id,
        ];
        $result = $this->atp->db_query_fast($qry, $param);

        if ($this->atp->db_num_rows($result) === 0) {
            $id = $this->atp->db_ATP_insert(PREFIX . 'ATP_RECORD_FILES', [
                'FILE_ID' => $file_id,
                'RECORD_ID' => $record_id,
                'FIELD_ID' => $field_id
            ]);
        } else {
            $row = $this->atp->db_next($result);
            $id = $row['ID'];
        }

        return $id;
    }
}
