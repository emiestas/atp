<?php

namespace Atp\File;

interface IHandler
{
    public function GetRelatedFiles($param);

    public function GetFileInfo($id);
}
