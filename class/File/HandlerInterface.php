<?php

namespace ATP\File;

interface HandlerInterface {

	public function GetRelatedFiles($param);

	public function GetFileInfo($id);
}
