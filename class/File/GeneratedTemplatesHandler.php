<?php

namespace Atp\File;

use \Atp\Core;

class GeneratedTemplatesHandler extends AbstractHandler
{
    private $atp;

    private $root = 'files/documents/';

    public function __construct(\Atp\Core $atp)
    {
        parent::__construct($atp);
        $this->atp = $atp;
    }

    public function GetRelatedFiles($param)
    {
        if (empty($param) === true) {
            return false;
        }

        $data1 = $this->atp->logic2->get_db(PREFIX . 'ATP_RECORD_DOCUMENTS', $param, null, null, false, 'FILE_ID as ID');
        $data2 = $this->atp->logic2->get_db(PREFIX . 'ATP_RECORD_FILES', $param, null, null, false, 'FILE_ID as ID');
		if (count($data1) === 0 AND count($data2) === 0) {
            return;
        }
        return array_unique(array_merge(array_values(array_column($data1, 'ID')), array_values(array_column($data2, 'ID'))));
    }

    /**
     * @param int $data->id [optional] Failo ID.
     * @param int $data->record_id Dokumento įrašo ID.
     * @param int $data->template_id [optional] Dokumento šablono ID.
     *
     * @return int Sukurto įrašo ID.
     */
    public function Save($data)
    {
        $id = $this->atp->db_ATP_insert(PREFIX . 'ATP_RECORD_DOCUMENTS', [
            'FILE_ID' => $data->id,
            'RECORD_ID' => $data->record_id,
            'TEMPLATE_ID' => (isset($data->template_id) ? $data->template_id : null)
        ]);

        return $id;
    }
}
