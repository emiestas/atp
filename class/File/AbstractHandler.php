<?php

namespace Atp\File;

use \Atp\Core;

abstract class AbstractHandler implements IHandler
{
    private $atp;

    public function __construct(\Atp\Core $atp)
    {
        $this->atp = $atp;
    }

    abstract public function GetRelatedFiles($param);

    public function GetFileInfo($id)
    {
        $data = $this->atp->logic2->get_db(PREFIX . 'ATP_FILES', ['ID' => (int) $id], true, null, false);
        if (empty($data)) {
            return false;
        }

        return (object) array_change_key_case($data, CASE_LOWER);
    }
    /**
     * @param \stdClass $data
     * @param string $data->path Kelias iki failo
     * @param string $data->name Failo pavadinimas
     * @param string $data->type [optional] Failo mime-type
     * @param int $data->size [optional] Failo dydis (bytais)
     * @return int
     */
    /* public function Save(\stdClass $data) {
      if (empty($data->type)) {
      $data->type = (new \finfo(FILEINFO_MIME_TYPE))->file($data->path);
      }
      if (empty($data->size)) {
      $data->size = filesize($data->path);
      }

      $row = [
      'TYPE' => $data->type,
      'SIZE' => $data->size,
      'NAME' => $data->name,
      'PATH' => $path['atp']['file']
      ];
      $data->id = $this->atp->db_quickInsert(PREFIX . 'ATP_FILES', $row);

      return $data->id;
      } */
}
