<?php

namespace Atp\File;

use \Atp\Core;

class RecordFilesHandler extends AbstractHandler
{
    private $atp;

    private $root = 'files/uploads/';

    public function __construct(\Atp\Core $atp)
    {
        parent::__construct($atp);
        $this->atp = $atp;
    }

    public function GetRelatedFiles($param)
    {
        if (empty($param) === true) {
            return false;
        }

        $data = $this->atp->logic2->get_db(PREFIX . 'ATP_RECORD_FILES', $param, null, null, false, 'FILE_ID as ID');
        if (count($data) === 0) {
            return;
        }

        $data = array_map(function ($a) {
            return (int) $a['ID'];
        }, $data);

        return array_values($data);
    }

    /**
     * @param int $data->id [optional] Failo ID. Nurodomas tuo atveju, kai failas jau yra išsaugotas
     * @param int $data->record_id Dokumento įrašo ID
     * @param int $data->field_id Dokumento įrašo formos lauko ID
     *
     * @param #\stdClass $file [optional] Failo duomenys
     * @param #string $file->path Kelias iki failo
     * @param #string $file->name Failo pavadinimas
     * @param #string $file->type [optional] Failo mime-type
     * @param #int $file->size [optional] Failo dydis (bytais)
     *
     * @return int Įrašo ID
     * @throws \Exception
     */
    public function Save($data)
    {
        //public function Save($data, $file = null) {
        //var_dump($data);
        //if (empty($data->id)) {
        //if (isset($file->path) === FALSE && isset($file->name) === FALSE)
        //$data->id = parent::Save($file);
        //	else
        //		throw new \Exception('Nenurodyti saugomo failo duomenys');
        //}

        $id = $this->atp->db_ATP_insert(PREFIX . 'ATP_RECORD_FILES', [
            'FILE_ID' => $data->id,
            'RECORD_ID' => $data->record_id,
            'FIELD_ID' => $data->field_id
        ]);

        return $id;
    }
}
