<?php

class TROLOLOL
{
    // Teisės tikrinamos prieš kviečiant metodą
    public function create_record($type, $param_arg = null)
    {
        if (is_array($param_arg) === false) {
            $param_arg = (array) $param_arg;
        }

        if (empty($this->_r_table_id) === true || empty($this->_r_table_to_id) === true
        /* || empty($this->_r_record_id) === TRUE *//* || empty($this->_r_values) === TRUE */) {
            return false;
        }

        // Iš kur dokumentas keliauja // ?
        $this->_r_table_from_id = $this->_r_table_id;

        // ? // TODO: Kam nors naudojama? // TODO: kuriant naują dokumentą, jis prasideda tyrimo stadijoje ar toje pačioje, kaip ankstesnis?
        if (empty($this->_r_record_id)) {
            //$this->_r_record_status = 2; // tiriamo statusas+
            // Pradinis statusas
            $this->_r_record_status = $this->get_document_status($this->_r_table_id);
        } else {
            // Gauna dabartinį statusą
            $r_relations = $this->logic2->get_record_relations(array('RECORD_ID' => $this->_r_record_id,
                'IS_LAST' => 1), true);
            $this->_r_record_status = $r_relations['STATUS'];
        }

        if ($type === 'submit_path') {
            if ($this->_r_path) {
                $this->_r_table_id = $this->_r_table_to_id;
                //$this->_r_record_from_id = $this->_r_record_id;
            }
        }

        /**
         * Extract'inima:
         * $values
         * $extra_values
         * $files_fields
         * $clause
         */
        extract($this->transform_data(), EXTR_OVERWRITE);
        $record_relation = array();

        $r_action = $type;

        $status = array();
        if (in_array($type, array('finish_investigation', 'finish_examination', 'finish_idle'), true) === true) {
            $status[$type] = true;

            if (in_array($type, array('finish_investigation', 'finish_idle'), true) === true) {
                $type = 'save';
            } else {
                $type = 'save_new';
                $status['new'] = true;
            }
        }

        if ($type === 'submit_path') {
            if ($this->_r_path) {
                $from_record = $this->logic2->get_record_table($this->_r_r_id, 'C.RECORD_MAIN_ID, A.ID RID');
                $values['RECORD_MAIN_ID'] = $from_record['RECORD_MAIN_ID'];
                $type = 'save_new';
            }
        }
        if (empty($this->core->_prefix->table)) {
            $tbl_name = 'TBL_';
        } else {
            $tbl_name = $this->core->_prefix->table;
        }

        $table_name = $tbl_name . $this->_r_table_id;
        switch ($type) {
            case 'save_new':
                $status['new'] = true;
            case 'save':
                // Surenka pradinius duomenis
                if (empty($this->_r_r_id) === false && $this->_r_path !== true) {
                    $record = $this->logic2->get_record_relation($this->_r_r_id);
                    if (empty($record) === true) {
                        trigger_error('Record not found', E_USER_ERROR);
                        exit;
                    }
                    $rid = $record['ID'];
                    $doc_status = $record['DOC_STATUS'];

                    // Ankščiau išsaugoti papildomi laukai
                    $extra_data = current((array) $this->get_table_records(array(
                            'RECORD_NR' => $this->_r_r_id)));
                    foreach ($extra_data as $key => $val) {
                        // Ar nereikia pridėti "W", kad nešalintu webserviso lauko
                        if (preg_match('/[KS]COL_/i', $key) === 0 || $val === null || $val === '') {
                            unset($extra_data[$key]);
                        }
                    }

                    $data = $this->logic2->get_record_table($record);
                    if (empty($data) === true) {
                        trigger_error('Record data not found', E_USER_ERROR);
                        exit;
                    }


                    // Jei kuriamas naujas įrašas, atnaujinama sukūrimo data ir nustatoma versija
                    if ($status['new'] === true && $data !== false) {
                        unset($data['ID'], $data['CREATE_DATE']);

                        // Jei tai ne paskutinė versija, tai gauna paskutinę
                        if ($data['RECORD_LAST'] !== '1') {
                            $last_record_table = $this->logic2->get_record_table($data['RECORD_ID']);
                            $data['RECORD_VERSION'] = $last_record_table['RECORD_VERSION'];
                            unset($last_record_table);
                        }

                        $data['RECORD_VERSION'] = ++$data['RECORD_VERSION'];
                        $data['#CREATE_DATE#'] = 'NOW()';
                    }


                    // Jei pirmą kartą saugomas laikinas įrašas, atnaujinama sukūrimo data (1/2)
                    $qry = 'SELECT ID
						FROM ' . PREFIX . 'ATP_RECORD_UNSAVED
						WHERE
							RECORD_ID = :RECORD_ID';
                    $result = $this->core->db_query_fast($qry, array('RECORD_ID' => $record['ID']));
                    $count = $this->core->db_num_rows($result);
                    if ($count) {
                        // Dokumento lentelėje
                        unset($data['CREATE_DATE']);
                        $data['#CREATE_DATE#'] = 'NOW()';
                    }


                    unset($record);
                } else {
                    $extra_data = array();
                    $data = array(
                        'RECORD_MAIN_ID' => 0,
                        'RECORD_VERSION' => 1,
                        'RECORD_LAST' => 1,
                        '#CREATE_DATE#' => 'NOW()'
                    );
                }

                // Sujungia pradinius ir naujus duomenis
                if (isset($status['finish_idle']) && $status['finish_idle'] === true) {
                    $values = $data;
                    $extra_values = $extra_data;
                } else {
                    $values = array_merge((array) $data, $values);
                }

                // Failų saugojimas (1/2 dalis)
                // Failai iš formos laukų
                if (empty($files_fields) === false) {
                    foreach ($files_fields as $field_id => &$files) {
                        foreach ($files['name'] as $key => &$name) {
                            // Klaida
                            if ($files['error'][$key] !== 0) {
                                // neprisegtas joks failas
                                if ($files['error'][$key] == 4) {
                                    continue;
                                }

                                if (in_array('disable_messages', $param_arg) === false) {
                                    $this->core->set_message('error', 'Nepavyko įkelti failo: ' . $name);
                                }
                                continue;
                            }

                            // Įkelia failą
                            $info = $this->action->uploadFile(array(
                                'name' => $files['name'][$key],
                                'type' => $files['type'][$key],
                                'size' => $files['size'][$key],
                                'error' => $files['error'][$key],
                                'tmp_name' => $files['tmp_name'][$key],
                            ));

                            // Sukuria failo įrašą
                            if ($info) {
                                $file_id = $this->action->saveFile(array(
                                    'TYPE' => $info['type'],
                                    'SIZE' => $info['size'],
                                    'NAME' => $info['filename'],
                                    'PATH' => $info['path']
                                ));
                                $this->_r_files[$field_id][] = $file_id;

                                // Inforamcija apie formos lauką
                                $field = $this->logic2->get_fields(array('ID' => $field_id));

                                // Nurodoma, kad laukas turi failų
                                $pos = strpos($field['NAME'], $this->core->_prefix->column);
                                if ($pos === false) {
                                    continue;
                                }

                                $reference = &$values;
                                if ($pos > 0) {
                                    $reference = &$extra_values;
                                }

                                $reference[$field['NAME']] = 1;
                            }
                        }
                    }
                }
                // Sujungia pradinius papildomus ir naujus duomenis
                $extra_values = array_merge((array) $extra_data, (array) $extra_values);

                if ((isset($status['finish_investigation']) && $status['finish_investigation'] === true) || (isset($status['finish_idle']) && $status['finish_idle'] === true)) {
                    $exp_record_id = explode('p', $this->_r_record_id);
                    $values['ID'] = $exp_record_id[1];
                    $status['new'] = false;
                    $rid = $this->_r_r_id;
                    $qry = 'SELECT MAX(RECORD_VERSION) AS version
                                            FROM ' . PREFIX . 'ATP_' . $table_name . '
                                            WHERE
                                                    RECORD_ID = :RECORD_ID';
                    $result = $this->core->db_query_fast($qry, array('RECORD_ID' => $this->_r_record_id));
                    if ($this->core->db_num_rows($result) === 1) {
                        $row = $this->core->db_next($result);
                        $values['RECORD_VERSION'] = $row['version'];
                    } else {
                        $values['RECORD_VERSION'] = 1;
                    }
                } elseif ($this->_r_table_to_id == 36 && $r_action === 'submit_path') {
                    $qry = 'SELECT MAX(RECORD_VERSION) AS version
                                            FROM ' . PREFIX . 'ATP_' . $table_name . '
                                            WHERE
                                                RECORD_MAIN_ID = :RECORD_MAIN_ID';
                    $result = $this->core->db_query_fast($qry, array('RECORD_MAIN_ID' => $values['RECORD_MAIN_ID']));
                    if ($this->core->db_num_rows($result) === 1) {
                        $row = $this->core->db_next($result);
                        $values['RECORD_VERSION'] = $row['version'];
                    }
                    $values['RECORD_VERSION'] ++;
                }

                // Išsaugo dokumento įrašo duomenis (1/2 dalis)
                $new_id = $this->core->db_ATP_insert(PREFIX . 'ATP_' . $table_name, $values, 'replace');

                $extra_rows = array();
                if (empty($extra_values) === false) {
                    // TODO: $extra_rows, kai tenkina $status['finish_idle'] === TRUE
                    foreach ($extra_values as $name => $val) {
                        $data = array();
                        $data['ID'] = null;
                        $data['FIELD_ID'] = $this->get_extra_field_id_by_name($name);
                        $data['TABLE_ID'] = $this->_r_table_id;
                        $data['RECORD_ID'] = $new_id;
                        $data['VALUE'] = $val;
                        $extra_rows[] = $data;
                    }
                }

                // Web-servisu gauti laukai
                if (empty($this->_r_values['ws_field']) === false) {
                    // Jei magic_quotes_gpc įšjungtas lokaliai, bet įjungtas globaliai, tai visteik naudoja globalų nustatymą
                    $php_ini = ini_get_all();
                    if (isset($php_ini['magic_quotes_gpc']) === true && in_array(strtolower($php_ini['magic_quotes_gpc']['global_value']), array(
                            'on', '1')) === true) {
                        $ws_field = stripslashes($this->_r_values['ws_field']);
                    } else {
                        $ws_field = $this->_r_values['ws_field'];
                    }
                    $ws_field = json_decode($ws_field, true);

                    /*
                      Formatas:
                      [ 0: { 1948: { 1955: '348646348', 1956: 'VALDAS', 1957: 'PUČKIS' } },
                      1: { 1949: { 1958: '13246687', 1959: 'UAB "Plytos"' } } ]
                     */
                    foreach ($ws_field as $field) {
                        $field = current($field);
                        foreach ($field as $key => $val) {
                            $data = array();
                            $data['ID'] = null;
                            $data['FIELD_ID'] = $key;
                            $data['TABLE_ID'] = $this->_r_table_id;
                            $data['RECORD_ID'] = $new_id;
                            $data['VALUE'] = $val;
                            $extra_rows[] = $data;
                        }
                    }
                }

                // Išsaugo dokumento įrašo duomenis (2/2 dalis)
                if (count($extra_rows) > 0 && $status['finish_investigation'] !== true && $status['finish_idle'] !== true) {
                    $this->core->db_ATP_insert(PREFIX . 'ATP_FIELDS_VALUES', $extra_rows, 'replace');
                }

                // Surenka duomenis sąryšio sukūrimui
                if (empty($this->_r_record_id) === true || $status['new'] === true || $status['finish_investigation'] === true || $status['finish_idle'] === true) {
                    //$uprivileges_data = array('table_id' => $this->_r_table_id, 'user_id' => $this->core->factory->User()->getLoggedUser()->person->id, 'department_id' => $this->core->factory->User()->getLoggedPersonDuty()->department->id);
                    $arr = array(
                        'TABLE_FROM_ID' => $this->_r_table_from_id,
                        'TABLE_TO_ID' => $this->_r_table_to_id,
                        'IS_LAST' => 1,
                        '#CREATE_DATE#' => 'NOW()',
                        'DOC_STATUS' => isset($this->_r_values['document_status']) ? $this->_r_values['document_status'] : null);

                    if (isset($status['new']) && $status['new'] === true && $this->_r_path !== true) {
                        // jei nauja versija
                        $arr['STATUS'] = $r_relations['STATUS'];
                        $arr['RECORD_ID'] = $r_relations['RECORD_ID'];
                    } else {
                        // jei nauja versijai, kai yra perduodama į kitą dokumentą
                        unset($arr['DOC_STATUS'], $record_relation['DOC_STATUS']);
                        $arr['STATUS'] = $this->get_document_status($this->_r_table_id);

                        $arr['RECORD_ID'] = $this->_r_table_id . 'p' . $new_id;
                    }

                    // Baigus protokolo ar nutarimo tyrima, prideti nauja irasa i tvarkarasty,
                    // jei nebuvo apmoketa
                    $idle_days = $this->get_document_status_idle_days($this->_r_table_id);
                    if ($status['finish_investigation'] === true && ($this->_r_table_id == 36 || $this->_r_table_id == 57) && $idle_days !== false && (bool) $_POST['paid'] !== true) {
                        $sch = prepare_schedule();
                        $events = $sch->logic->getEventsBySoft(array('ITEM' => $this->_r_record_id,
                            'ITEM_TYPE' => 'expire'));
                        if (count($events) === 0) {
                            $worker = $this->logic2->get_record_worker(array(
                                'F.RECORD_ID = "' . $this->_r_record_id . '"',
                                'A.TYPE = "' . 'invest' . '"'
                                ), true
                            );

                            $prefix = parse_url($this->core->config['GLOBAL_SITE_URL'], PHP_URL_PATH);
                            $url = $prefix . 'subsystems/atp/index.php?m=5&id=' . $this->_r_record_id;
                            $current_document = $this->logic2->get_document(array(
                                'ID' => $this->_r_table_id));

                            $from = date('Y-m-d', time() + $idle_days * (24 * 60 * 60));

                            $event_id = $sch->logic->saveEvent(array(
                                'FROM' => $from,
                                'TILL' => $from,
                                'NAME' => 'ATP: Kontroliuoti AN',
                                'DETAILS' => 'ATP: Kontroliuoti AN.<br/>'
                                . 'Dokumentas: ' . $current_document['NAME'] . '<br/>'
                                . 'Adresas: <a href="' . $url . '">' . $this->_r_record_id . '</a>'
                                ), $worker['PEOPLE_ID'], array(
                                'SOFT' => 'atp',
                                'TYPE' => 'expire',
                                'ITEM' => $this->_r_record_id
                                ), $this->core->factory->User()->getLoggedUser()->person->id
                            );
                        }
                    }

                    // Pakeičia tipą į nagrinėjimą
                    if ((isset($status['finish_investigation']) && $status['finish_investigation'] === true) || (isset($status['finish_idle']) && $status['finish_idle'] === true)) {
                        //						unset($arr['DOC_STATUS'], $record_relation['DOC_STATUS']);
                        // $t duomenys yra $this->logic->table
                        //$t = $this->logic2->get_document($this->_r_table_id);
                        if (empty($this->manage->table['CAN_IDLE']) === false && $status['finish_idle'] !== true) {
                            $arr['STATUS'] = 6;
                        } else {
                            // Nagrinėjimo statusas
                            $arr['STATUS'] = 3;

                            // Jei tyrėjas neturi teisės nagrinėti, suranda laisviausią nagrinėtoją ir jam priskiria dokumento įrašą
                            if ($status['finish_idle'] !== true) {
                            } else {

                                // Jei bauda apmokėta - įrašui priskiriamas finished (Baigtas) statusas, pašalinamas nagrinėtojas ir įrašas niekur nesiunčiamas
                                if ((bool) $_POST['paid'] === true) {
                                    $arr['STATUS'] = 5;

                                    $workers = $this->logic2->get_record_worker(array(
                                        'A.RECORD_ID' => $rid));
                                    foreach ($workers as &$worker) {
                                        if (in_array($worker['TYPE'], array('exam',
                                                'next'), true) === true) {
                                            $this->core->db_query_fast('DELETE FROM ' . PREFIX . 'ATP_RECORD_X_WORKER WHERE ID = :ID', $worker);
                                        }
                                    }
                                } else {
                                }

                                //$arr['IS_PAID'] = (bool) $_POST['paid'];
                            }
                        }
                    }
                    // Pakeičia tipą į baigta // TODO: persųsti, kam?
                    elseif (isset($status['finish_examination']) && $status['finish_examination'] === true) {
                        unset($arr['DOC_STATUS'], $record_relation['DOC_STATUS']);
                        $arr['STATUS'] = 4;
                    }

                    $arr = array_merge($record_relation, $arr);
                }


                // Jei perduodama į kitą dokumentą
                if ($this->_r_path === true) {
                    unset($arr['DOC_STATUS']);
                    $arr['RECORD_FROM_ID'] = $from_record['RID'];
                    $this->_r_record_id = $arr['RECORD_ID'];


                    // Atnaujina RECORD_ID
                    $args = array(
                        'ID' => $new_id,
                        'RECORD_ID' => $this->_r_record_id);
                    $this->core->db_query_fast('UPDATE
							' . PREFIX . 'ATP_' . $table_name . '
						SET
							RECORD_ID = :RECORD_ID
 						WHERE ID = :ID', $args);


                    // Sukuria sąryšių įrašą
                    $new_r_id = $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD', $arr, 'replace');
                    $this->_r_r_id = $new_r_id;

                    if (in_array('disable_messages', $param_arg) === false) {
                        $this->core->set_message('success', 'Iš ' . $_POST['record_id'] . ' sukurtas naujas dokumentas: "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $this->_r_record_id . '</a>"');
                    }
                    $redirect_url = $this->core->config['SITE_URL'] . 'index.php?m=5&id=' . $this->_r_r_id;
                } else {
                    // Jei buvo įrašyti nauji duomenys
                    if (empty($this->_r_record_id) === true) {
                        $this->_r_record_id = $arr['RECORD_ID'];

                        // Atnaujina RECORD_ID
                        $args = array(
                            'ID' => $new_id,
                            'RECORD_ID' => $this->_r_record_id
                        );
                        $this->core->db_query_fast('UPDATE
							' . PREFIX . 'ATP_' . $table_name . '
						SET
							RECORD_ID = :RECORD_ID,
							RECORD_MAIN_ID = :RECORD_ID
						WHERE ID = :ID', $args);

                        $arr['DOC_STATUS'] = isset($this->_r_values['document_status']) ? $this->_r_values['document_status'] : null;
                        // Sukuria sąryšių įrašą
                        $new_r_id = $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD', $arr, 'replace');
                        $this->_r_r_id = $new_r_id;

                        if (in_array('disable_messages', $param_arg) === false) {
                            $this->core->set_message('success', 'Sukurtas naujas dokumentas: <a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $this->_r_record_id . '</a>');
                        }
                    }
                    // Jei saugoma, kaip nauja versija
                    elseif ($status['new'] === true) {
                        // Senus įrašus nurodo kaip nebe paskutinius
                        $args = array(
                            'RECORD_ID' => $this->_r_record_id,
                            'ID' => $new_id
                        );
                        $this->core->db_query_fast('UPDATE
							' . PREFIX . 'ATP_' . $table_name . '
						SET RECORD_LAST = 0
						WHERE
							RECORD_ID = :RECORD_ID AND
							RECORD_LAST = 1 AND
							ID != :ID', $args);
                        $this->core->db_query_fast('UPDATE
							' . PREFIX . 'ATP_RECORD
						SET IS_LAST = 0
						WHERE
							RECORD_ID = :RECORD_ID AND
							IS_LAST = 1', $args);

                        $arr['RECORD_FROM_ID'] = $rid;

                        if (empty($this->_r_values['document_status'])) {
                            $arr['DOC_STATUS'] = $doc_status;
                        } else {
                            $arr['DOC_STATUS'] = $this->_r_values['document_status'];
                        }

                        $this->_r_record_id = $arr['RECORD_ID'];

                        // Sukuria sąryšių įrašą
                        $new_r_id = $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD', $arr, 'replace');
                        $this->_r_r_id = $new_r_id;

                        if ($status['finish_investigation'] === true || $status['finish_idle'] === true) {
                            if ($arr['STATUS'] === 6) {
                                if (in_array('disable_messages', $param_arg) === false) {
                                    $this->core->set_message('success', 'Dokumentas "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $this->_r_record_id . '</a>" perkeltas į tarpinę būseną');
                                }
                            } elseif ((bool) $_POST['paid'] === true && $arr['STATUS'] === 5) {
                                if (in_array('disable_messages', $param_arg) === false) {
                                    $this->core->set_message('success', 'Dokumento "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $this->_r_record_id . '</a>" nagrinėjimas baigtas');
                                }
                            } else {
                                if (in_array('disable_messages', $param_arg) === false) {
                                    $this->core->set_message('success', 'Dokumentas "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $this->_r_record_id . '</a>" perduotas nagrinėjimui');
                                }
                            }
                        } elseif ($status['finish_examination'] === true) {
                            if (in_array('disable_messages', $param_arg) === false) {
                                $this->core->set_message('success', 'Dokumento "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $this->_r_record_id . '</a>" nagrinėjimas baigtas');
                            }
                        } else {
                            if (in_array('disable_messages', $param_arg) === false) {
                                $this->core->set_message('success', 'Išsaugota nauja dokumento "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $this->_r_record_id . '</a>" versija');
                            }
                        }

                        $redirect_url = $this->core->config['SITE_URL'] . 'index.php?m=5&record_id=' . $this->_r_r_id;
                    } else {
                        $param = array();

                        // Jei pirmą kartą saugomas laikinas įrašas, atnaujinama sukūrimo data (2/2)
                        $qry = 'SELECT ID
						FROM ' . PREFIX . 'ATP_RECORD_UNSAVED
						WHERE
							RECORD_ID = :RECORD_ID';
                        $result = $this->core->db_query_fast($qry, array('RECORD_ID' => $rid));
                        $count = $this->core->db_num_rows($result);
                        if ($count) {
                            $param['CREATE_DATE'] = date('Y-m-d H:i:s');
                        }

                        if (empty($this->_r_values['document_status'])) {
                            $param['DOC_STATUS'] = $doc_status;
                        } else {
                            $param['DOC_STATUS'] = $this->_r_values['document_status'];
                        }

                        if ($status['finish_investigation'] === true || $status['finish_idle'] === true) {
                            $param['STATUS'] = $arr['STATUS'];
                            $redirect_url = $this->core->config['SITE_URL'] . 'index.php?m=5&record_id=' . $this->_r_r_id;
                        }

                        $this->core->db_quickupdate(PREFIX . 'ATP_RECORD', $param, $rid, 'ID');
                        if (in_array('disable_messages', $param_arg) === false) {
                            $this->core->set_message('success', 'Dokumentas "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_r_id . '">' . $this->_r_record_id . '</a>" atnaujintas');
                        }
                    }
                }

                // Nes registruojant dokumentą per webservisus, nebuna automatiškai sukurto tuščio dokumento
                $r_relations = $this->logic2->get_record_relations(array($this->_r_r_id), true);


                // TODO: naudoti \Atp\Document\Record\Payment::SetRecordPaymentOption() // Ar papildomas veiksmas nieko negadins? (t.y. versijavimas)
                // Apmokėjimo atnaujinimas // Jei pažymėta, kad baigtas
                if ((int) $arr['STATUS'] === 5) {
                    //$this->core->db_ATP_insert(PREFIX . 'ATP_RECORD_PAID', array('RECORD_ID' => $this->_r_r_id, 'STATUS' => (int) $arr['IS_PAID']), 2);
                    $payment_status = 1;
                    if ((bool) $_POST['paid'] === true) {
                        $payment_status = 2;
                    }
                    $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD_PAID', array(
                        'RECORD_ID' => $this->_r_r_id, 'PAYMENT_STATUS' => $payment_status), 2);
                }


                // Failų saugojimas (2/2 dalis)
                // Sukuria išsaugotų failų ryšius su dokumento įrašu
                if (isset($this->_r_files) && count($this->_r_files)) {
                    /* if (empty($new_r_id) === TRUE) {
                      $record = $this->logic2->get_record_relation($this->_r_r_id);
                      $record_id = $record['ID'];
                      } else {
                      $record_id = $new_r_id;
                      } */

                    $record_id = $this->_r_r_id;
                    foreach ($this->_r_files as $field_id => &$ids) {
                        foreach ($ids as &$id) {
                            $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD_FILES', array(
                                'FILE_ID' => $id,
                                'RECORD_ID' => $record_id,
                                'FIELD_ID' => $field_id
                            ));
                        }
                    }
                }

                /**
                 * Sukuria ryšį tarp dokumento įrašo duomenų ATP_TBL_[0-9]+ ir ATP_RECORD_RELATION
                 */
                $this->core->db_query_fast(
                    'INSERT INTO ' . PREFIX . 'ATP_RECORD_X_RELATION (`DOCUMENT_ID`, `RECORD_ID`, `RELATION_ID`) VALUES (:DOCUMENT_ID, :RECORD_ID, :RELATION_ID)', array(
                    'DOCUMENT_ID' => $this->_r_table_id, 'RECORD_ID' => $new_id,
                    'RELATION_ID' => $this->_r_r_id));

                if (empty($this->_r_r_id) === false/* && in_array($arr['STATUS'], array(1,2,3)) */) {
                    $current_workers = $this->create_record_add_worker(array(
                        'new_r_id' => $this->_r_r_id,
                        'status' => $status,
                        'clause' => $clause,
                        'r_relations' => $r_relations,
                        'r_path' => $this->_r_path,
                        'table_from_id' => $this->_r_table_from_id,
                        'table_id' => $this->_r_table_id));
                } elseif (empty($this->_r_r_id) && empty($this->_r_record_id) === false) {
                    die('This wasn\'t supposed to happen');
                    $record = $this->logic2->get_record_relation($this->_r_record_id);
                    $current_workers = $this->create_record_add_worker(array(
                        'new_r_id' => $record['ID'],
                        'status' => $record['STATUS'],
                        'clause' => $clause,
                        'r_relations' => $r_relations,
                        'r_path' => $this->_r_path,
                        'table_from_id' => $this->_r_table_from_id,
                        'table_id' => $this->_r_table_id));
                }


                if ($this->_r_table_id == '57') {
                    $current_record = $this->logic2->get_record_relation($this->_r_r_id);
                    if (empty($current_record['RECORD_FROM_ID']) === false) {
                        $previous_record = $this->logic2->get_record_relation($current_record['RECORD_FROM_ID']);

                        if ((int) $previous_record['TABLE_TO_ID'] !== (int) $current_record['TABLE_TO_ID']) {
                            $current_person = $this->logic2->get_record_worker(array(
                                'A.RECORD_ID = ' . (int) $current_record['ID']), true, null, 'A.ID, A.WORKER_ID, A.TYPE');

                            $previous_person = $this->logic2->get_record_worker(array(
                                'A.TYPE IN ("exam","next")',
                                'A.RECORD_ID = ' . (int) $previous_record['ID']), true, null, 'A.ID, A.WORKER_ID, A.TYPE');

                            if ((int) $current_person['WORKER_ID'] !== (int) $previous_person['WORKER_ID']) {
                                $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD_X_WORKER SET WORKER_ID = ' . (int) $previous_person['WORKER_ID'] . ' WHERE ID = ' . $current_person['ID']);
                            }
                        }
                    }
                }

                if (empty($this->_r_r_id) === false) {
                    $qry = 'DELETE
						FROM ' . PREFIX . 'ATP_RECORD_UNSAVED
						WHERE
							WORKER_ID = :WORKER_ID AND
							#RECORD_NR = :RECORD_NR
							RECORD_ID = :RECORD_ID';
                    $qry_arg = array(
                        'WORKER_ID' => $this->core->factory->User()->getLoggedPersonDuty()->id,
                        //'RECORD_NR' => $this->_r_record_id,
                        'RECORD_ID' => $this->_r_r_id);
                    $this->core->db_query_fast($qry, $qry_arg);
                }


                // Lentelės įrašą tiesiogiai suriša su įrašo "sąryšio" lentelės įrašu
                if (empty($this->_r_r_id) === false) {
                    /*
                     * Automatinis laukų užpildymas
                     */
                    $current_document = $this->logic2->get_document(array('ID' => $this->_r_table_id));
                    // Visų dokumento įrašo laukų reikšmės
                    $record_values = $this->get_fields_values_by_type_and_id2(array(
                        'RECORD' => $this->_r_r_id));

                    // Tesiama tik tada, kai nagrinėtojo vardo ir pavardės laukas nėra užpildytas
                    $set_examinator_fields = false;
                    //if ($this->logic2->is_VMSA_record($this->_r_r_id) === FALSE) { // šitoje vietoje dokumento irasas dar negali būti pažymetas kai VMSA dokumentas
                    $examinator_field_id = $current_document['EXAMINATOR'];
                    if (empty($examinator_field_id) === false) {
                        $field = $this->logic2->get_fields(array('ID' => $examinator_field_id));
                        // Praleidžia, jei dokumentas neturi tokio lauko
                        if (array_key_exists($field['NAME'], $record_values) === true) {
                            // Praleidžia, jei dokumento laukas jau yra užpildytas
                            $record_values[$field['NAME']] = trim($record_values[$field['NAME']]);
                            if (empty($record_values[$field['NAME']]) === true) {
                                $set_examinator_fields = true;
                            }
                        }
                    }

                    if ($set_examinator_fields === true) {
                        // Numatytas nagrinėtojas
                        $examinator = $this->logic2->get_record_worker(array(
                            'A.TYPE IN("exam", "next")',
                            'A.RECORD_ID = ' . (int) $this->_r_r_id), true, null, 'A.ID, A.WORKER_ID');

                        if (empty($examinator) === false) {
                            // Surenka nagrinėtojo informaciją
                            $fields = 'A.ID,B.FIRST_NAME, B.LAST_NAME,
								A.ROOM, B.PHONE_MOBILE, B.PHONE_FULL, B.EMAIL,
								A.STRUCTURE_ID, C.PARENT_ID as "STRUCTURE_PARENT_ID", C.SHOWS AS "STRUCTURE_NAME",
								C.TYPE_ID as "STRUCTURE_TYPE_ID"';
                            $examinator = $this->logic2->get_worker_info(array('A.ID = ' . (int) $examinator['WORKER_ID']), true, null, $fields);

                            // Suformuoja dokumento įrašo laukų reikšmes
                            $fields_data = array();

                            // Kai tyrėjas priskirtas poskyriui, nustatomas poskyris ir skyrius
                            if ($examinator['STRUCTURE_TYPE_ID'] === '8') {
                                $fields_data['EXAMINATOR_SUBUNIT'] = $examinator['STRUCTURE_NAME'];

                                $parent_structure = $this->logic2->get_db(PREFIX . 'MAIN_STRUCTURE', array(
                                    'ID' => $examinator['STRUCTURE_PARENT_ID']), true, null, false, 'A.ID, A.SHOWS, A.TYPE_ID');
                                if (empty($parent_structure) === false && $parent_structure['TYPE_ID'] === '7') {
                                    $fields_data['EXAMINATOR_UNIT'] = $parent_structure['SHOWS'];
                                }
                            } else {
                                // Kai tyrėjas priskirtas skyriui, nustatomas tik skyrius
                                if ($examinator['STRUCTURE_TYPE_ID'] === '7') {
                                    $fields_data['EXAMINATOR_UNIT'] = $examinator['STRUCTURE_NAME'];
                                }
                            }

                            // Institucijos informacija
                            $institution = $this->get_parent_structure($examinator['STRUCTURE_PARENT_ID'], 12);
                            if (empty($institution) === false) {
                                $institution = $this->logic2->get_db(PREFIX . 'MAIN_STRUCTURE', array(
                                    'ID' => $institution), true, null, false, 'A.ID, A.SHOWS, A.ADDRESS');
                                // Institucijos pavadinimas
                                $fields_data['EXAMINATOR_INSTITUTION_NAME'] = $institution['SHOWS'];
                                // Institucijos adresas
                                $fields_data['EXAMINATOR_INSTITUTION_ADDRESS'] = $institution['ADDRESS'];
                            }
                            // Nagrinėtojo vardas ir pavardė
                            $fields_data['EXAMINATOR'] = join(' ', array($examinator['FIRST_NAME'],
                                $examinator['LAST_NAME']));
                            // Kabinetas
                            $fields_data['EXAMINATOR_CABINET'] = $examinator['ROOM'];
                            // Telefonas
                            if (empty($examinator['PHONE_FULL']) === false) {
                                $phone = $examinator['PHONE_FULL'];
                            } elseif (empty($examinator['PHONE_MOBILE']) === false) {
                                $phone = $examinator['PHONE_MOBILE'];
                            }
                            $fields_data['EXAMINATOR_PHONE'] = $phone;
                            // El. paštas
                            $fields_data['EXAMINATOR_EMAIL'] = $examinator['EMAIL'];

                            // Pradiniai nagrinėjimo laikai
                            $examinator_free_slot = $this->get_document_person_free_time(array(
                                'RECORD' => $this->_r_r_id,
                                'DATETIME' => date('Y-m-d H:i:s'),
                                'RESULT_COUNT' => 1
                            ));
                            $fields_data['EXAMINATOR_DATE'] = $examinator_free_slot[0]['FROM']['DATE'];
                            $fields_data['EXAMINATOR_START_TIME'] = $examinator_free_slot[0]['FROM']['TIME'];
                            $fields_data['EXAMINATOR_FINISH_TIME'] = $examinator_free_slot[0]['TILL']['TIME'];

                            // Automatiškai pildomi laukai
                            $update_data = array();
                            $fields_arr = array(
                                'EXAMINATOR',
                                'EXAMINATOR_CABINET',
                                'EXAMINATOR_PHONE',
                                'EXAMINATOR_EMAIL',
                                'EXAMINATOR_INSTITUTION_NAME',
                                'EXAMINATOR_UNIT',
                                'EXAMINATOR_SUBUNIT',
                                'EXAMINATOR_INSTITUTION_ADDRESS',
                                'EXAMINATOR_DATE',
                                'EXAMINATOR_START_TIME',
                                'EXAMINATOR_FINISH_TIME'
                            );
                            foreach ($fields_arr as $field_name) {
                                $field_id = $current_document[$field_name];
                                if (empty($field_id)) {
                                    continue;
                                }

                                $field = $this->logic2->get_fields(array('ID' => $field_id));
                                // Praleidžia, jei dokumentas neturi tokio lauko
                                if (array_key_exists($field['NAME'], $record_values) === false) {
                                    continue;
                                }

                                // Nustato, kur nauji duomenys keliaus
                                if ($field['ITYPE'] === 'DOC') {
                                    $update_data['DOC'][$field['NAME']] = $fields_data[$field_name];
                                } else {
                                    $update_data['EXTRA'][$field['NAME']] = array(
                                        'ID' => null,
                                        'FIELD_ID' => $field_id,
                                        'TABLE_ID' => $this->_r_table_id,
                                        'RECORD_ID' => $new_id,
                                        'VALUE' => $fields_data[$field_name]
                                    );
                                }
                            }

                            if (empty($update_data['DOC']) === false) {
                                //foreach ($update_data['DOC'] as $row) {
                                $this->core->db_quickupdate(PREFIX . 'ATP_TBL_' . $this->_r_table_id, $update_data['DOC'], $new_id, 'ID');
                                //}
                            }

                            if (empty($update_data['EXTRA']) === false) {
                                $this->core->db_ATP_insert(PREFIX . 'ATP_FIELDS_VALUES', $update_data['EXTRA'], 'replace');
                            }
                        }
                    }


                    /**
                     * Sukuria pranešimus: dokumento termino pabaiga, ATP priminimai.
                     * ATP dokumento pabaiga.
                     * ATP priminimas.
                     */
                    $qry = 'SELECT COUNT(ID) CNT
						FROM `' . PREFIX . 'ATP_RECORD`
						WHERE `RECORD_ID` = :RECORD_ID';
                    $result = $this->core->db_query_fast($qry, array('RECORD_ID' => $this->_r_record_id));
                    $count = $this->core->db_next($result);
                    $count = $count['CNT'];

                    if ($count === '1') {
                        $worker = $this->logic2->get_worker_info(array('A.ID = ' . (int) $current_workers['main']), true);
                        $term = $this->get_record_term($this->_r_r_id, null, false);

                        //$prefix = trim(end(explode($_SERVER['HTTP_HOST'], $this->core->config['GLOBAL_SITE_URL'], 2)), '/');
                        $prefix = parse_url($this->core->config['GLOBAL_SITE_URL'], PHP_URL_PATH);
                        $url = $prefix . 'subsystems/atp/index.php?m=5&id=' . $this->_r_record_id;

                        $current_document = $this->logic2->get_document(array('ID' => $this->_r_table_id));
                        $sch = prepare_schedule();

                        if (empty($term) === false) {
                            $events = $sch->logic->getEventsBySoft(array('ITEM' => $this->_r_record_id,
                                'ITEM_TYPE' => 'expire'));
                            if (count($events) === 0) {
                                $event_id = $sch->logic->saveEvent(array(
                                    'FROM' => $term,
                                    'TILL' => $term,
                                    'NAME' => 'ATP dokumento pabaiga',
                                    'DETAILS' => 'ATP termino pabaiga.<br/>'
                                    . 'Dokumentas: ' . $current_document['NAME'] . '<br/>'
                                    . 'Adresas: <a href="' . $url . '">' . $this->_r_record_id . '</a>'
                                    ), $worker['PEOPLE_ID'], array(
                                    'SOFT' => 'atp',
                                    'TYPE' => 'expire',
                                    'ITEM' => $this->_r_record_id // TODO: RECORD_ID | ar čia reikia paduot RR.ID?
                                    ), $this->core->factory->User()->getLoggedUser()->person->id);
                            }
                        }

                        $holidays = $sch->logic->getHolidays($worker['PEOPLE_ID']);
                        $terms = $this->get_record_other_terms($this->_r_record_id, (array) $holidays);

                        if ($terms !== false) {
                            $events = $sch->logic->getEventsBySoft(array('ITEM' => $this->_r_record_id,
                                'ITEM_TYPE' => 'remind'));
                            if (count($events) === 0) {
                                foreach ($terms as &$term) {
                                    $event_id = $sch->logic->saveEvent(array(
                                        'FROM' => $term,
                                        'TILL' => $term,
                                        'NAME' => 'ATP priminimas',
                                        'DETAILS' => 'ATP priminimas.<br/>'
                                        . 'Dokumentas: ' . $current_document['NAME'] . '<br/>'
                                        . 'Adresas: <a href="' . $url . '">' . $this->_r_record_id . '</a>'
                                        ), $worker['PEOPLE_ID'], array(
                                        'SOFT' => 'atp',
                                        'TYPE' => 'remind',
                                        'ITEM' => $this->_r_record_id // TODO: RECORD_ID | ar čia reikia paduot RR.ID?
                                        ), $this->core->factory->User()->getLoggedUser()->person->id);
                                }
                            }
                        }
                    }
                }


                // TODO: tikrinti ne pagal prisijugusį vartotoją, o pagal senus įrašo duomenis
                // TODO: papildomai tikrinit pagal skyrių
                // Jei dokumento tyrimas / pagrinėjimas perduotas kitam vartotojui, praneša kam jis buvo perduotas
                #if (in_array($arr['STATUS'], array(2, 3)) === TRUE && $arr['M_PEOPLE_ID'] != $this->core->factory->User()->getLoggedUser()->person->id) {
                if (in_array($arr['STATUS'], array(2, 3)) === true && $current_workers['main'] != $this->core->factory->User()->getLoggedPersonDuty()->id) {
                    $worker = $this->logic2->get_worker_info(array('A.ID = ' . (int) $current_workers['main']), true);
                    if (in_array('disable_messages', $param_arg) === false) {
                        $this->core->set_message('success', 'Dokumentas perduotas į "' . $worker['STRUCTURE_NAME'] . '" skyrių<br>' .
                            'Atsakingas asmuo: ' . $worker['FIRST_NAME'] . ' ' . $worker['LAST_NAME']);
                    }
                    $redirect_url = $this->core->config['SITE_URL'] . 'index.php?m=4&id=' . $this->_r_table_id;
                }
                #if (in_array($arr['STATUS'], array(6)) === TRUE && $arr['M_PEOPLE_ID_2'] != $this->core->factory->User()->getLoggedUser()->person->id && $arr['M_PEOPLE_ID_2'] !== NULL) {
                if (in_array($arr['STATUS'], array(6)) === true && $current_workers['main'] != $this->core->factory->User()->getLoggedPersonDuty()->id) {
                    $worker = $this->logic2->get_worker_info(array('A.ID = ' . (int) $current_workers['main']), true);
                    //$dep = $this->get_m_department($arr['M_DEPARTMENT_ID_2']);
                    if (in_array('disable_messages', $param_arg) === false) {
                        $this->core->set_message('success', 'Dokumentas perduotas į "' . $worker['STRUCTURE_NAME'] . '" skyrių<br>' .
                            'Atsakingas asmuo: ' . $worker['FIRST_NAME'] . ' ' . $worker['LAST_NAME']);
                    }
                    $redirect_url = $this->core->config['SITE_URL'] . 'index.php?m=4&id=' . $this->_r_table_id;
                }
                break;
            case 'finish_proccess':
                if (empty($this->_r_r_id) === false) {
                    /* $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD
                      SET
                      STATUS = 5 #,
                      #DOC_STATUS = NULL
                      WHERE
                      RECORD_ID = :record_id AND
                      IS_LAST = 1
                      LIMIT 1', array('record_id' => $this->_r_record_id)); */
                    $this->core->db_query_fast('UPDATE ' . PREFIX . 'ATP_RECORD
						SET
							STATUS = 5
						WHERE
							ID = :ID
						LIMIT 1', array('ID' => $this->_r_r_id));


                    $sch = prepare_schedule();
                    $events = $sch->logic->getEventsBySoft(array('SOFT' => 'atp',
                        'ITEM' => $this->_r_record_id));
                    foreach ($events as &$event) {
                        $sch->logic->disableEvent($event['ID']);
                    }

                    if (in_array('disable_messages', $param_arg) === false) {
                        $this->core->set_message('success', 'Dokumento "<a href="' . $this->core->config['SITE_URL'] . '/index.php?m=5&id=' . $this->_r_record_id . '">' . $this->_r_record_id . '</a>" procesas baigtas');
                    }
                } else {
                    if (in_array('disable_messages', $param_arg) === false) {
                        $this->core->set_message('error', 'Dokumentas nerastas');
                    }
                }
                break;
        }

        $this->update_record_status($this->_r_r_id);

        if (empty($redirect_url) === true) {
            $redirect_url = $this->core->config['SITE_URL'] . 'index.php?m=4&id=' . $this->_r_table_id;
        }

        $this->create_record_redirect_url = $redirect_url;

        //var_dumP($this->core->_message, $this->core->get_messages_sess());die;
        return $this->_r_r_id;
    }

    public function transform_data()
    {
        $values = array();
        $extra_values = array();
        $files_fields = array();
        $record_relation = array();
        $clause_id = null;

        if (!empty($this->_r_r_id)) {
            $record = $this->logic2->get_record_relation($this->_r_r_id);
            //$table_record = $this->get_table_record($this->_r_r_id);
            //$table_record = $this->logic2->get_record_table($record);
            $form_values = $this->get_fields_values_by_type_and_id2(array('RECORD' => $record));
            //if (in_array($record['STATUS'], array(2, 3)))
            $this->_r_record_status = $record['STATUS'];
        }

        $table_sturcture = $this->logic2->get_fields_all(array('ITEM_ID' => $this->_r_table_id,
            'ITYPE' => 'DOC'));

        foreach ($table_sturcture as $structure) {
            // ??? // Jei dabartinis dokumento statusas nelygus lauko statusui // jei lauko statusas nelygus 1 // - tai praleidža tą lauką
            //if ($this->_r_path !== TRUE && (int) $this->_r_record_status !== (int) $structure['COMPETENCE'] && $structure['COMPETENCE'] != '1') {
            if ($this->_r_path !== true && (int) $this->_r_record_status !== (int) $structure['COMPETENCE'] && $structure['COMPETENCE'] != '1' && (int) $this->_r_record_status !== 1) {
                //if (($this->_r_path !== TRUE && (int) $this->_r_record_status !== (int) $structure['COMPETENCE'] && (int) $this->_r_record_status !== 1) /*|| isset($_POST['finish_idle']) === FALSE*/) {
                //if (($this->_r_path !== TRUE
                //		&& ((int) $this->_r_record_status !== (int) $structure['COMPETENCE'] && $structure['COMPETENCE'] != '1') )
                //		|| in_array($this->_r_record_status, array(2, 3)) === FALSE) {

                if ($structure['ITYPE'] == 'DOC') {
                    $values[$structure['NAME']] = $form_values[$structure['NAME']];
                } else {
                    $extra_values[$structure['NAME']] = $form_values[$structure['NAME']];
                }
                continue;
            }

            if (isset($this->_r_values[strtolower($structure['NAME'])])) {
                $value = $this->_r_values[strtolower($structure['NAME'])];
            } else {
                $value = null;
            }

            if (in_array($structure['TYPE'], array(1, 2, 3, 4, 5, 15, 16)) === true) {
                $value = (int) $value;
            } elseif (in_array($structure['TYPE'], array(6)) === true) {
                $value = (float) str_replace(',', '.', $value);
            } elseif (in_array($structure['TYPE'], array(7, 8, 9, 10, 18)) === true) {
                if ($structure['SORT'] == 5) {
                    $value = (array) $value;
                } elseif ($structure['TYPE'] == 18) {
                    $value = (int) $value;
                } else {
                    $value = (string) htmlspecialchars($value, ENT_QUOTES);
                }
            } elseif (in_array($structure['TYPE'], array(11, 12, 13, 14)) === true) {
                // Metai
                if ($structure['TYPE'] == 11) {
                    $pattern = '#^(19|20)([0-9]{2})$#';
                    $default = 'Y';
                } else {
                    // Data
                    if ($structure['TYPE'] == 12) {
                        $pattern = '#^(19|20)([0-9]{2})[-./](0[1-9]|1[012])[-./](0[1-9]|[12][0-9]|3[01])$#';
                        $default = 'Y-m-d';
                    } else {
                        // Laikas
                        if ($structure['TYPE'] == 13) {
                            //$pattern = '#^([01]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$#';
                            $pattern = '#^([01]?[0-9]|2[0-3]):([0-5][0-9])$#';
                            //$default = 'H:i:s';
                            $default = 'H:i';
                        } else {
                            // Data ir laikas
                            if ($structure['TYPE'] == 14) {
                                //$pattern = '#^(19|20)([0-9]{2})[-./](0[1-9]|1[012])[-./](0[1-9]|[12][0-9]|3[01]) ([01]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$#';
                                $pattern = '#^(19|20)([0-9]{2})[-./](0[1-9]|1[012])[-./](0[1-9]|[12][0-9]|3[01]) ([01]?[0-9]|2[0-3]):([0-5][0-9]))$#';
                                //$default = 'Y-m-d H:i:s';
                                $default = 'Y-m-d H:i';
                            }
                        }
                    }
                }


                $is_empty = empty($value);
                if (!preg_match($pattern, $value)) {
                    if ($is_empty === true) {
                        $value = '';
                    } else {
                        $value = date($default, strtotime($value));
                    }
                }
            } else {
                // Failas
                if ($structure['SORT'] == 10 && empty($_FILES) === false) {
                    // Nauji failai
                    if (isset($_FILES[strtolower($structure['NAME'])]) === true) {
                        $files_fields[$structure['ID']] = $_FILES[strtolower($structure['NAME'])];
                    }
                } else {
                    // Straipsnis
                    if ($structure['SORT'] == 6 && empty($value) === false) {
                        $clause_id = $value;
                    }
                }
            }

            if ($structure['SORT'] == 5 || $structure['SORT'] == 8 && empty($value) === false) {
                if ($structure['SORT'] == 8) {
                    $value = array($value);
                }
                $value = json_encode($value);
            }

            /**
             * @TODO: [done] Neaišku kam to reikia. Turi būti saugomi viski įjungti, matomi formos laukai.
             * Buvo reikalinga, kad nesaugotu tuščių papildomų laukų reikšmių.

              //if(empty($value) === TRUE)
              if ($value === NULL || $value === '')
              continue;
             */
            /**
             * @TODO: [done] Taip pat netinka. Saugant tą pačią versiją turi perrašyti esamus išsaugotų laukų reikšmes

              // Tuščias laukas. Jei dokumento laukas, tai nurodoma NULL reikšmė, jei papildomas laukas - išvis praleidžiamas.
              if ($value === NULL || $value === '') {
              if ($structure['ITYPE'] !== 'DOC') {
              continue;
              }
              $value = NULL;
              } */
            // Jei reikšmė tuščia
            if ($value === null || $value === '') {
                $value = null;
            }


            if ($structure['ITYPE'] == 'DOC') {
                $values[$structure['NAME']] = $value;
            } else {
                $extra_values[$structure['NAME']] = $value;
            }
        }

        return array(
            'values' => $values,
            'extra_values' => $extra_values,
            'files_fields' => $files_fields,
            'clause' => $clause_id
        );
    }

    /**
     * Priskiria darbuotojus prie dokumento įrašo
     * @param array $param
     * @param int $param[new_r_id] Naujo dokumento įrašo ATP_RECORD.ID
     * @param array $param[status] Indeksai: new, finish_investigation, finish_idle, finish_examination
     * @param int $param[clause] Straipsnio ID. $clause iš $this->transform_data()
     * @param array $param[r_relations] Ankstesnio dokumento įrašo duomenys iš ATP_RECORD
     * @param boolen $param[r_path] Ar dokumento įrašo duomenys keliauja į kitą dokumentą
     * @param int $param[table_from_id] Dokumento ID iš kurio kuriamas naujas dokumento įrašas
     * @param int $param[table_id] Naujo dokumento įrašo dokumento ID
     * @return array $current_workers
     */
    private function create_record_add_worker($param)
    {
        /*
         *
         */

        extract($param);

        // Saugomas dokumentas
        $record = $this->logic2->get_record_relation($new_r_id);
        // Saugomo dokumento straipsnis
        $clause = $this->get_record_clause($record);

        // Ankstesnis dokumentas
        $prev_record = $this->logic2->get_record_relation($record['RECORD_FROM_ID']);

        $qry = 'SELECT A.ID CNT
			FROM ' . PREFIX . 'ATP_RECORD_UNSAVED A
				INNER JOIN ' . PREFIX . 'ATP_RECORD B
					ON
						B.ID = A.RECORD_ID AND
						B.TABLE_TO_ID = :DOCUMENT_ID AND
						B.ID = :ID AND
						B.IS_LAST = 1
			WHERE
				A.WORKER_ID = :WORKER_ID';
        $qry_arg = array(
            'ID' => $record['ID'],
            'WORKER_ID' => $this->core->factory->User()->getLoggedPersonDuty()->id,
            'DOCUMENT_ID' => $record['TABLE_TO_ID']
        );
        $result = $this->core->db_query_fast($qry, $qry_arg);
        $count = $this->core->db_num_rows($result);
        //$reset = false;
        //if ($count) {
        //	$reset = true;
        //}

        $worker_tmp = array(
            'RECORD_ID' => $record['ID'],
            'WORKER_ID' => null,
            'TYPE' => null
        );

        $current_document = $this->logic2->get_document(array('ID' => $table_id));

        $workers = array();
        $current_workers = array();

        // Pagal dokumento tipą
        switch ($current_document['STATUS']) {
            // Nesvarbu
            case '1':
                if (empty($status['finish_investigation']) === true && empty($status['finish_idle']) === true) {
                    $this->core->db_quickupdate(PREFIX . 'ATP_RECORD', array('STATUS' => 2), $record['RECORD_ID'], 'ID');
                }
            //break;
            // Tiriamas
            case '2':
                /* if(isset($prev_record['ID']) && $r_path !== TRUE) {
                  $old_workers = $this->logic2->get_record_worker(array('A.RECORD_ID = ' . (int) $prev_record['ID']));
                  } else {
                  $old_workers = $this->logic2->get_record_worker(array('A.RECORD_ID = ' . $record['ID']));
                  } */


                /**
                 * Jei nevyksta perdavimas į kitą dokumentą ir tai yra nauja dokumento versija
                 * (kuriama nauja versija<del>, baigiamas tyrimas arba tarpinė būsena</del>),
                 * tai priskiriami buvusios versijos darbuotojai
                 */
                if ((
                    (isset($status['new']) && $status['new'] === true)
                    /* || (isset($status['finish_investigation']) && $status['finish_investigation'] === TRUE)
                      || (isset($status['finish_idle']) && $status['finish_idle'] === TRUE) */
                    ) && $r_path !== true) {
                    $prev_workers = $this->logic2->get_record_worker(array('A.RECORD_ID = ' . (int) $prev_record['ID']));
                    /* if($status['new'] === TRUE) {
                      $record_to_get_workers_from = $prev_record['ID'];
                      } else {
                      $record_to_get_workers_from = $record['ID'];
                      }
                      $workers_to_check = $this->logic2->get_record_worker(array('A.RECORD_ID = ' . (int) $record_to_get_workers_from)); */

                    foreach ($prev_workers as $worker) {
                        //foreach ($workers_to_check as $worker) {

                        $tmp = $worker_tmp;

                        $tmp['WORKER_ID'] = $worker['WORKER_ID'];
                        $tmp['TYPE'] = $worker['TYPE'];

                        // Jei priskirtas numatytasis nagrinėtojas ir baigiama tarpinė būsena, tai jis numatomas kaip nagrinėtojas ir pagrindinis dokumento darbuotojas
                        if ($tmp['TYPE'] === 'next' && isset($status['finish_idle']) && $status['finish_idle'] === true) {
                            $tmp['TYPE'] = 'exam';
                            $current_workers['main'] = $tmp['WORKER_ID'];
                        } else {
                            // Kitaip tyrėjas yra pagrindinis dokumento darbuotojas
                            if ($tmp['TYPE'] === 'invest') {
                                $current_workers['main'] = $tmp['WORKER_ID'];
                            }
                        }

                        $workers[] = $tmp;
                    }
                } else {
                    // Vykdoma saugant be naujos versijos
                    // Jei dokumentas keliauja (į tirimą), tai "next" tipo darbuotojas neliečiamas
                    /**
                     * Jei sugoma be naujos versijos
                     * (išsugoti<ins>, baigiamas tyrimas arba tarpinė būsena, baigiamas nagrinėjimas(?)</ins>),
                     * tai "next" tipo darbuotojas neliečiamas
                     */
                    $tmp = $worker_tmp;

                    // Paprastam saugojimui surenka esamą informacija
                    $record_workers = $this->logic2->get_record_worker(array('A.RECORD_ID = ' . $record['ID']));
                    if (count($record_workers)) {
                        foreach ($record_workers as $rw) {
                            $tmp = $worker_tmp;

                            $tmp['WORKER_ID'] = $rw['WORKER_ID'];
                            $tmp['TYPE'] = $rw['TYPE'];

                            if ($tmp['TYPE'] === 'invest') {
                                $current_workers['main'] = $tmp['WORKER_ID'];
                            } elseif ($tmp['TYPE'] === 'next') {
                                if ($status['finish_idle'] === true) {
                                    $tmp['TYPE'] = 'exam';
                                    $current_workers['exam'] = $tmp['WORKER_ID'];
                                } else {
                                    $current_workers['next'] = $tmp['WORKER_ID'];
                                }
                            }

                            $workers[] = $tmp;
                        }
                    } else {
                        // Jei nera ankščiau priskirtų darbuotojų
                        // Jei registruojantis asmuo nėra tyrėjas, surasti laisviausią tyrėją ir jam priski dokumento įrašą
                        // Tikrina registruojančio darbuotojo tyrimo ir nagrinėjimo teises
                        $can_investigate = $this->can_current_worker_receive_record('INVESTIGATE_RECORD', $record, $clause);
                        if ((int) $current_document['STATUS'] === 1) {
                            $can_examinate = $this->can_current_worker_receive_record('EXAMINATE_RECORD', $record, $clause);
                        } else {
                            $can_examinate = false;
                        }


                        $tmp['WORKER_ID'] = null;
                        // Ieško galinčio tirti
                        if ($can_investigate === false && $can_examinate === false) {
                            $worker = $this->get_free_investigator($current_document['ID'], $clause);

                            if (empty($worker['ID']) === false) {
                                $tmp['WORKER_ID'] = $worker['WORKER_ID'];
                                $tmp['TYPE'] = 'invest';
                            }
                        } else {
                            // Registruojantis vartotojas gali dirbti su dokumentu
                            //$worker = $this->logic2->get_worker_info(array('A.ID = ' . (int) $this->core->factory->User()->getLoggedPersonDuty()->id), true);
                            //if (empty($worker['ID']) === FALSE)
                            //	$tmp['WORKER_ID'] = $worker['ID'];
                            $tmp['WORKER_ID'] = $this->core->factory->User()->getLoggedPersonDuty()->id;
                            if ($can_investigate === false && $can_examinate === true) {
                                $tmp['TYPE'] = 'exam';
                            } else {
                                $tmp['TYPE'] = 'invest';
                            }
                        }

                        $current_workers['main'] = $tmp['WORKER_ID'];

                        $workers[] = $tmp;


                        //-- BULL BEGIN --// Neįmanoma tiksliai numatyti koks bus sekantis dokumentas
                        // Jei sekantis dokumentas yra nagrainėjimo dokumentas, tai suranda galintį nagrinėti
                        $next_document = $this->logic2->get_document(array('PARENT_ID' => $current_document['ID']), true, null, 'ID, STATUS');

                        if ($next_document['STATUS'] === '3') {
                            $tmp = $worker_tmp;

                            #// Jei registruojantis asmuo nėra nagrinėtojas, surasti laisviausią nagrinėtoją ir jam priski dokumento įrašą
                            #if ($this->check_privileges('EXAMINATE_RECORD', $check_arr) === FALSE) {
                            #} else {
                            $worker = $this->get_free_examinator($next_document['ID']);
                            if (empty($worker) === false) {
                                $tmp['WORKER_ID'] = $worker['WORKER_ID'];
                            }
                            #}
                            $tmp['TYPE'] = 'next';
                            $workers[] = $tmp;
                            $current_workers['next'] = $tmp['WORKER_ID'];
                        }
                        //-- BULL END --//
                    }
                }


                //-- BULL BEGIN --// nebepriskirinėjo numatytų nagrinėtojų, tai jei nerastas numatytas nagrinėtojas arba nagrinėjantis asmuo - priskiria
                $next_is_set = false;
                foreach ($workers as $worker) {
                    if ($worker['TYPE'] === 'next' || $worker['TYPE'] === 'exam') {
                        $next_is_set = true;
                        break;
                    }
                }
                if ($next_is_set === false) {
                    if ($current_document['ID'] == 36) {
                        $next_document = $this->logic2->get_document(array('ID' => 57), true, null, 'ID, STATUS');
                    } else {
                        $next_document = $this->logic2->get_document(array('PARENT_ID' => $current_document['ID']), true, null, 'ID, STATUS');
                    }

                    if ($next_document['STATUS'] === '3' || $next_document['STATUS'] === '1') {
                        $tmp = $worker_tmp;

                        #// Jei registruojantis asmuo nėra nagrinėtojas, surasti laisviausią nagrinėtoją ir jam priski dokumento įrašą
                        #if ($this->check_privileges('EXAMINATE_RECORD', $check_arr) === FALSE) {
                        #} else {
                        $worker = $this->get_free_examinator($next_document['ID']);
                        if (empty($worker) === false) {
                            $tmp['WORKER_ID'] = $worker['WORKER_ID'];
                        }
                        #}
                        $tmp['TYPE'] = 'next';
                        $workers[] = $tmp;
                        $current_workers['next'] = $tmp['WORKER_ID'];
                    }
                }
                //-- BULL END --//





                break;
            // Nagrinėjamas
            case '3':
                // Paprastam saugojimui surenka esamą informacija
                $record_workers = $this->logic2->get_record_worker(array('A.RECORD_ID = ' . $record['ID']));
                if (count($record_workers)) {
                    foreach ($record_workers as $rw) {
                        $tmp = $worker_tmp;
                        $tmp['WORKER_ID'] = $rw['WORKER_ID'];
                        $tmp['TYPE'] = $rw['TYPE'];
                        $workers[] = $tmp;

                        if ($tmp['TYPE'] === 'invest') {
                            $current_workers['main'] = $tmp['WORKER_ID'];
                        } elseif ($tmp['TYPE'] === 'next') {
                            $current_workers['next'] = $tmp['WORKER_ID'];
                        }
                    }
                } else {
                    // Jei tai yra nauja dokumento versija, tai priskiriami buvusios versijos darbuotojai
                    if ($status['new'] === true && $r_path !== true) {
                        $prev_workers = $this->logic2->get_record_worker(array('A.RECORD_ID = ' . (int) $prev_record['ID']));
                        foreach ($prev_workers as $worker) {
                            $tmp = $worker_tmp;
                            $tmp['WORKER_ID'] = $worker['WORKER_ID'];
                            $tmp['TYPE'] = $worker['TYPE'];
                            if ($tmp['TYPE'] === 'exam') {
                                $current_workers['main'] = $tmp['WORKER_ID'];
                            }
                            $workers[] = $tmp;
                        }
                    } else {
                        // Jei tai naujas dokumentas arba keliauja (į tirimą), tai "next" tipo darbuotojas neliečiamas

                        $tmp = $worker_tmp;

                        $prev_document = $this->logic2->get_document(array('ID' => $table_from_id), true, null, 'ID, STATUS');

                        if ($prev_document['STATUS'] === '2') {
                            $worker = $this->logic2->get_record_worker(array(
                                'A.RECORD_ID = ' . (int) $r_relations['ID'],
                                'A.TYPE = "next"'), true);
                            if (empty($worker) === false) {
                                $tmp['WORKER_ID'] = $worker['WORKER_ID'];
                            }
                        }
                        if (empty($tmp['WORKER_ID']) === true) {
                            // Jei registruojantis asmuo nėra nagrinėtojas, surasti laisviausią nagrinėtoją ir jam priski dokumento įrašą
                            if ($this->can_current_worker_receive_record('EXAMINATE_RECORD', $record, $clause) === false) {
                                $worker = $this->get_free_examinator($current_document['ID'], $clause);
                                if (empty($worker['ID']) === false) {
                                    $tmp['WORKER_ID'] = $worker['WORKER_ID'];
                                }
                            } else {
                                $worker = $this->logic2->get_worker_info(array('A.PEOPLE_ID = ' . (int) $this->core->factory->User()->getLoggedUser()->person->id,
                                    'STRUCTURE_ID' => (int) $this->core->factory->User()->getLoggedPersonDuty()->department->id), true);
                                if (empty($worker['ID']) === false) {
                                    $tmp['WORKER_ID'] = $worker['ID'];
                                }
                            }
                        }

                        $tmp['TYPE'] = 'exam';
                        $workers[] = $tmp;
                        $current_workers['main'] = $tmp['WORKER_ID'];
                    }
                }
                break;
        }

        $this->core->db_query_fast('DELETE FROM ' . PREFIX . 'ATP_RECORD_X_WORKER WHERE RECORD_ID = :RECORD_ID', array(
            'RECORD_ID' => $record['ID']));
        $this->core->db_ATP_insert(PREFIX . 'ATP_RECORD_X_WORKER', $workers);

        return $current_workers;
    }
}
