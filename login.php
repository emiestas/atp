<?php
require __DIR__ . '/../../inc/mainconf.php';
?><!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=Content-Type content="text/html; charset=utf-8">
        <title>webPartner</title>
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link rel="shortcut icon" href="<?= GLOBAL_SITE_URL ?>favicon.ico" type="image/x-icon"/>
        <script language="javascript" type="text/javascript" src="<?= GLOBAL_SITE_URL ?>js/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?= GLOBAL_SITE_URL ?>styles/new/css/wp/login-responsive.css"/>
    </head>
    <body class="top-menu-body">
        <div id="main">
            <div id="login-box">
                <div id="login-logo">
                    <img src="<?= GLOBAL_SITE_URL ?>styles/new/images/logo_webpartner.png" alt="WebPartner"/>
                </div>
                <div id="login-form_block">
                    <div id="login-icon_name">
                        <img src="<?= GLOBAL_SITE_URL ?>styles/new/images/loginSpyn.png" alt="" height="14px" width="12px" style="display:block"/><b>Prisijungimas</b>
                    </div>
                    <div id="login-form">
                        <form target="_top" name="loginForm" action="<?= GLOBAL_SITE_URL ?>subsystems/atp/index.php?m=login" method="POST">
                            <input type="hidden" name="redirect_url" value="<?= GLOBAL_SITE_URL ?>subsystems/atp/index.php?m=4&id=67">
                            <p id="false-login"></p>
                            <div class="username_field">
                                <p class="name">
                                    Vardas:
                                </p>
                                <input class="field" value="" type="text" name="username"/>
                            </div>
                            <div class="password_field">
                                <p class="pass">
                                    Slaptažodis:
                                </p>
                                <input class="field" value="" type="password" name="password"/>
                            </div>
                            <div class="out_field">
                                <p class="rem">
                                    <input id="remember" type="checkbox" name="remember" value="1"/>
                                    <label for="remember">Prisiminti</label>
                                </p>
                            </div>
                            <div class="submit_and_remember">
                                <input type="Submit" value="Prisijungti"/>
                                <!--<input type="button" value="Pamiršau slaptažodį" onclick="location.href=('<?= GLOBAL_SITE_URL ?>subsystems/atp/index.php?m=21');"/>-->
                            </div>
                        </form>
                    </div>
                </div>
                <div id="login-copyright">
                    © WebPartner.lt Sprendimas: <a href="http://www.idamas.lt" target="_blank">Idamas</a>.
                </div>
            </div>
        </div>
        <script>
            var onResize;
            (onResize = function () {
                var bodyHeight = $('.top-menu-body').height();
                var loginHeight = $('#main').height();
                var paddingTop = (bodyHeight - loginHeight) / 2;
                $('#main').css('padding', paddingTop + 'px 0px 0px 0px');
            })();
            $(window).resize(onResize);
        </script>
    </body>
</html>