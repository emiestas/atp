<?php 
function xlogwrite($text, $clear = false){
	if (!__DEVELOPER__){
		return;
	}
	
	$text = (is_array($text) ?  print_r($text, true) : $text);
	$trace = debug_backtrace()[0];
	//print_r($trace);
	/* VARIABLE */
	$vLine = file($trace['file']);
    $fLine = $vLine[ $trace['line'] - 1 ];
	
	preg_match( "/\w+\([']?([\$a-s]+)[\,|'\)]/", $fLine, $match );
	$variable = $match[1];
	/* */
	//echo $variable;
	$myfile = fopen(__ROOT__ . 'debug.txt', (($clear === true) ? "w" : "a"));
	fwrite($myfile, '--------- ' . str_replace(__ROOT__, '', $trace['file']) .':'. $trace['line'] . ' ---- time: ' . number_format(microtime(),4) . PHP_EOL);
	if (substr($variable,0,1) == '$'){
		fwrite($myfile, $variable . ' = ' .$text . PHP_EOL);
	} else {
		fwrite($myfile, $text . PHP_EOL);
	}
	fclose($myfile);
}
//specialiai dubliuota, neverclear;
function xlogwriteu($text, $clear = false){
	static $called = false;
	
	$text = (is_array($text) ?  print_r($text, true) : $text);
	$trace = debug_backtrace()[0];
	//print_r($trace);
	/* VARIABLE */
	$vLine = file($trace['file']);
    $fLine = $vLine[ $trace['line'] - 1 ];
	
	preg_match( "/\w+\([']?([\$a-s]+)[\,|'\)]/", $fLine, $match );
	$variable = $match[1];
	/* */
	//echo $variable;
	$myfile = fopen(__ROOT__ . 'debug_'. (__USERID__ == false ? 'false' : __USERID__) .'.txt', "a"); //apend always, userid
	if ($called == false){
		fwrite($myfile, '=====================' . date("Y-m-d H:i:s") . '=====================' . PHP_EOL);
		
		$called = true;
	}
	
	fwrite($myfile, '--------- ' . str_replace(__ROOT__, '', $trace['file']) .':'. $trace['line'] . ' ---- time: ' . number_format(microtime(),4) . PHP_EOL);
	if (substr($variable,0,1) == '$'){
		fwrite($myfile, $variable . ' = ' .$text . PHP_EOL);
	} else {
		fwrite($myfile, $text . PHP_EOL);
	}
	fclose($myfile);
}

function xlogwritem($text, $clear = false){
	if (!__MASTER__){
		return;
	}
	
	$myfile = fopen(__ROOT__ . 'debugm.txt', (($clear === true) ? "w" : "a"));
	fwrite($myfile, $text . PHP_EOL);
	fclose($myfile);
	die('irasyta');
}


$memo = array();

function xlogmemo($text, $output = false){
	global $memo;
	//xlog(__ROOT__);
	$trace = debug_backtrace()[0];
	
	$vLine = file($trace['file']);
    $fLine = $vLine[ $trace['line'] - 1 ];
	
	preg_match( "/\w+\([']?([\$a-s]+)[\,|'\)]/", $fLine, $match );
	$variable = $match[1];
	
	$file = str_replace(__ROOT__, '', $trace['file']) .':'. $trace['line'];
	
	if (substr($variable,0,1) == '$'){
		array_push($memo, array(
			$variable => $text,
			'file' => $file
		));
	} else {
		array_push($memo, array(
			'data' => $text,
			'file' => $file
		));
	}
	
	if ($output === true){
		xlog($memo);
	}
}



?>