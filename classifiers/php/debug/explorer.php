<?php 
function localexplorer_from_path($path, $line = 0, $onlyHref = false){
	if (is_dir($path)){
		$path = rtrim($path,'/');
	}
	
	$name = str_replace(__ROOT__, '', $path);
	
	if (empty($name)){
		$name = $path;
	}
	
	$path = str_replace(array('www','/'),array('www-data','\\'), realpath($path));
	if(defined('__LOCALEXPLORER__')){
		$href = "localexplorer:\\\\". __LOCALEXPLORER__ ."$path";
	} else {
		$href = "localexplorer:\\\\192.168.2.18$path";
	}

	if ($onlyHref){
		return $href;
	}
	return "<a href=\"$href\">" . $name . (($line === 0) ? '' : ':' . $line) . '</a>';
}

function localexplorer_from_referer(){
	return '';
}

function localexplorer_scandir($dir){
	return;
}

?>