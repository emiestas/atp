<?php
function xlog($param, $die = true){
	if(!__DEVELOPER__){
		return;
	}

	if (__JSON__){
		print_r($param);
		echo chr(10) . chr(10) . chr(10) .  $trace['path']  . ':' . $trace['line'];
	} else {
		echo '<pre>';
		print_r($param);
		echo '</pre>';
	}
	
	if ($die){
		http_response_code(207);
		die();
	}
}
function xlogf($param){
	if(!__DEVELOPER__){
		return;
	}

	if (__JSON__){
		print_r($param);
		echo chr(10);
	} else {
		echo '<pre>';
		print_r($param);
		echo '</pre>';
	}

	http_response_code(207);
}

function xlogd($param){
	echo '<pre>';
	print_r($param);
	echo '</pre>';
	http_response_code(207);
	die();
}

function xlog_query($param, $die = true){
	if(!__DEVELOPER__){
		return;
	}
	
	$from = array(' FROM ', ' UNION ', ' WHERE ', ' GROUP BY ', ' ORDER BY ');
	$to = array(chr(10).'FROM ',chr(10).'UNION '.chr(10),chr(10).'WHERE ',chr(10).'GROUP BY ', chr(10).'ORDER BY ');
	
	echo '<pre>';
	print_r(str_replace($from, $to, $param));
	echo '</pre>';
	
	if ($die){
		http_response_code(207);
		die();
	}
}
function xlogm($param, $die = true){
	if(!__MASTER__){
		return;
	}

	if (__JSON__){
		print_r($param);
		echo chr(10) . chr(10) . chr(10) .  $trace['path']  . ':' . $trace['line'];
	} else {
		echo '<pre>';
		print_r($param);
		echo '</pre>';
	}
	
	if ($die){
		http_response_code(207);
		die();
	}
}
function masterdie(){
	if (!defined('__MASTER__')){
		die('NOT DEFINED MASTERDIE');
	}
	
	if (!__MASTER__){
		die('VYSKTA ATNAUJINIMO DARBAI, PRASOME PALUKETI. JEI SI PRANESIMA MATOTE PER ILGAI - KREIPKITES I VYR. PROGRAMUOTOJA');
	}
}
function developerdie(){
	if (!__DEVELOPER__){
		die('VYSKTA ATNAUJINIMO DARBAI, PRASOME PALUKETI. JEI SI PRANESIMA MATOTE PER ILGAI - KREIPKITES I PROGRAMUOTOJUS');
	}

	if (!defined('__DEVELOPER__')){
		die('NOT DEFINED DEVELOPER');
	}
}
?>