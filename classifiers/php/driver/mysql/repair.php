<?php
function mssql_row_repair_numeric($row, $meta){
	return $row;
	foreach ($meta as $key => $value){
		switch($value['Type']){
			case SQLSRV_SQLTYPE_DECIMAL:
				$temp = (float)$row[$key];
				$decimals = strlen(substr(strrchr($temp, "."), 1));

				if ($value['Precision'] == 19){ //money
					$row[$key] = number_format($temp, $decimals, '.', '');
				} else {
					$row[$key] = number_format($temp, $decimals, '.', '');
				}
				
				break;
			case SQLSRV_SQLTYPE_VARCHAR:
			case SQLSRV_SQLTYPE_NVARCHAR:
				$name = $value['Name'] ?: '';
				if (__JSON__ === false && $row[$key][0] !== '<' && stripos($name, 'html') === false){
					$row[$key] = htmlspecialchars($row[$key]);
				}
				break;
			default:
				continue;
		}
	}
	
	return $row;
}

function mssql_row_repair_assoc($row, $meta){
	return $row;
	foreach ($meta as $key => $value){
		switch($value['Type']){
			case SQLSRV_SQLTYPE_DECIMAL:
				$temp = (float)$row[$value['Name']];
				$decimals = strlen(substr(strrchr($temp, "."), 1));

				if ($value['Precision'] == 19){ //money
					$row[$value['Name']] = number_format($temp, $decimals, '.', ''); //
				} else {
					$row[$value['Name']] = number_format($temp, $decimals, '.', '');
				}
				
				break;
			case SQLSRV_SQLTYPE_VARCHAR:
			case SQLSRV_SQLTYPE_NVARCHAR:
				$name = $value['Name'] ?: '';
				//xlogf(substr(trim($row[$value[Name]]),0,1));
				if (__JSON__ === false && substr(trim($row[$value['Name']]),0,1) !== '<' && substr(trim($row[$value['Name']]),0,4) !== '&lt;' && stripos($name, 'html') === false){
					$row[$value['Name']] = htmlspecialchars($row[$value['Name']]); //str_replace('&quot;','"',
				}
				break;
			default:
				continue;
		}
	}
	return $row;
}
?>