<?php

function mssql_error($query, $params = array()){
	global $conn;
	
	$trace =  debug_backtrace();

	$errors = mysqli_error($conn);

	$error = $errors;//mssql_parse_sql_error($errors[0]['code'], $errors[0]['message'], $query, $params);

	$is_raiserror = false;//in_array($errors[0]['code'], array(547,50000)); //triggerrije raiserror, arba delete reference constraint

	$address = '<br />Klaidos adresas: ' . $_SERVER['REQUEST_URI']. '<br />Refereris: '.  $_SERVER['HTTP_REFERER'];

	if (__PRODUCTION__ === true && __DEVELOPER__ === false && $is_raiserror == false){
		$number = mssql_error_lexita($error, $address, null, $query, $params); //$browser
	}
	
	$json_data = array(
        "success" => false,
        "msg" 	  => $error,
		"code" => 2,
		"erroruser" => $is_raiserror,
        "errornr" => $number
    );

	if (__DEVELOPER__ || __CLI__){
		$json_data["query"] = mssql_get_raw_query($query, $params);
	}

	if (__JSON__){
		if (__DEVELOPER__ && $is_raiserror === false){
			$temp = debug_backtrace();
			foreach ($temp as $file){
				if (strpos($file['file'],'/php/driver') == false){
					$trace = $file;
					break;
				}
			}
			$json_data['file'] = str_replace(__ROOT__, '',$trace['file']) . ':' . $trace['line'];
			if(strpos($file['file'],'/php/json') == false){
				$json_data['msg'] = $json_data['msg'] . '<br/>' . localexplorer_from_path($trace['file'], $trace['line']) . '<br/>' . localexplorer_from_referer();
			} else {
				if (__MASTER__){
					$json_data['msg'] = $json_data['msg'] . '<br/>' . localexplorer_from_referer();
				}
			}
		}
		die(json_encode($json_data));
	} else if (defined('__WEBSERVICE__')){
		throw new Exception($error);
		return;
	} else if (!__DEVELOPER__){
		echo '<font color="red"><b>' . $error . '</b></font>';
		xlog_query($json_data['query']);
	} else {

		$temp = debug_backtrace();
		foreach ($temp as $file){
			if (strpos($file['file'],'/php/driver') == false){
				$trace = $file;
				break;
			}
		}
		
		//xlogs($trace);
		
		$json_data['file'] = str_replace(__ROOT__, '',$trace['file']) . ':' . $trace['line'];
		$json_data['msg'] = $json_data['msg'] . '<br/>' . $query .'<br/>' . localexplorer_from_path($trace['file'], $trace['line']) . '<br/>' . localexplorer_from_referer();
		$error = $json_data['msg'];
		die('<pre>' . print_r($json_data) . '</pre>');
	}
	die('Klaida SQL: ' . $query. 'klaidos tekstas: '.  "\n\r" . $row['msg']);
}

function mssql_error_lexita($error, $addres, $browser, $query, $params){
	return $ErrorNr;
}

function mssql_parse_sql_error($number, $message, $query, $params){
	return '';
}

?>