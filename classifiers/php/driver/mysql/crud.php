<?php
function mssql_insert($postas, $json = true){
	$postas = mssql_magic_post($postas);
	
	$changed = array_filter(array_map('trim', mssql_unset_reserved($postas)),'strlen'); //tusciu elementu neinsertiname strlen -> jei nepaduodi sio parametro ismeta reiksmes su 0, trim -> trimina visas reikšmes
	
	if (!empty($postas['fk_key'])){
		$changed[$postas['fk_key']] = $postas['fk_val'];
	}
	
	if ($postas['pk_val'] != -1){
		$changed[$postas['pk_key']] = $postas['pk_val'];
		$query = mssql_get_raw_query('INSERT INTO ' . $postas['table'] . ' (' . implode(',', array_keys($changed)) . ') values (' .  trim(str_repeat('?,', count(
	$changed)),',') . ')', array_values($changed));
		$scope = $postas['pk_val'];
	}
	
	
	$query = mssql_get_raw_query('INSERT INTO ' . $postas['table'] . ' (' . implode(',', array_keys($changed)) . ') values (' .  trim(str_repeat('?,', count(
	$changed)),',') . ')', array_values($changed));

	//xlog($query);
	
	$stmt = query($query); // . '; SELECT SCOPE_IDENTITY() AS id'

	$scope = mssql_get_scope_identity($stmt);

	//pk gali negrizti jei triggeris su set nocount on
	if (!$scope && $postas['pk_val'] == -1){
		xlog($query);
		//sqlsrv_free_stmt($stmt);
		xlogd('JEI NERA SCOPE_IDENTITY SAKYTI MASTER ARBA LENTELES PK NERA AUTO_INCREMENT');
	}
	
	//sqlsrv_free_stmt($stmt);
	
	mssql_postaction($postas['postaction'], $scope);
	mssql_insert_documents($postas['documents'], $postas['pk_key'], $scope);
	
	log_actions('insert', $postas['table'], $scope, $query, null, $changed, true); //perduodamas visas masyvas
	
	$result = array(
        "success" => true,
        "pk"      => $scope
    );

	if (__DEVELOPER__){
		$result['query'] = $query;
	}
	
	if ($json){
	    die(json_encode($result));
	}
	
	return $scope;
}
//done
function mssql_update($postas, $json = true){
	$postas = mssql_magic_post($postas);
	$changed = mssql_unset_reserved($postas);
	
	if (count($changed) === 0){
		die('niekas nebuvo pakeista');
	}
	
	$query = 'SELECT ' . implode(',', array_keys($changed)) . ' FROM ' . $postas['table'] . ' WHERE ' . $postas['pk_key'] . "='" . $postas['pk_val']."'" ;
	
	$old = mssql_row($query); //nusiskaitome senas reikšmes
	
	if ($old === false){
		if ($json == false){
			return;
		}
		$json_data = array(
			"success" => false,
			"msg" 	  => 'Klaida updeitinat, greičiausiai įrašas neegzistuoja',
			"postas"  => $postas
		);
		
		if (__DEVELOPER__){
			$json_data['query'] = $query;
		}
		
		die(json_encode($json_data));
	}
	
	$changed = array_diff_assoc($changed, $old); /*   array_diff xbugas nepraeina kai vienetas      */

	if (count($changed) == 0){
		if ($json === false){
			return;
		}
		die(json_encode(array(
			'success' => true,
			'pk' => $postas['pk_val'],
			'msg' => 'Niekas nebuvo pakeista'
		)));
	}
	
	$up = mssql_get_raw_query(implode(' = ?, ', array_keys($changed)) . ' = ?', array_values($changed));
	
	$sql = "UPDATE {$postas['table']} SET {$up} WHERE {$postas['pk_key']} = '{$postas['pk_val']}'"; 
	
	
	//'UPDATE ' . $postas['table'] . ' SET ' . $up . ' WHERE ' . $postas['pk_key'] . ' = '" . $postas['pk_val'] . "'";
	//pritaiko where salyga
	if (isset($postas['fk_key']) && isset($postas['fk_val'])){
		$sql .= ' AND ' . $postas['fk_key'] . ' = ' . $postas['fk_val'];
	}
	
	$stmt = query($sql);
	
	//$result = mysqli_stmt_get_result($stmt);
	
	mssql_postaction($postas['postaction'], $postas['pk_val']);
	
	//mysqli_free_result($result);

	log_actions('update', $postas['table'], $postas['pk_val'], $sql, $old, $changed, true);

	$json_data = array(
		"success"	=> true
	);
	
	if (__DEVELOPER__){
		$json_data['query'] = $sql;
	}
	
	if ($json){
		die(json_encode($json_data));
	}
	
	return $json_data;
}

function mssql_delete($postas, $json = true){
	$postas = mssql_magic_post($postas);
	
    $query = "DELETE FROM " . $postas['table'] . " WHERE  " . $postas['pk_key'] . " = '" . $postas['pk_val'] . "'";
   
	$stmt = query($query);
	//sqlsrv_free_stmt($stmt);

	log_actions('delete', $postas['table'], $postas['pk_val'], $query, array(), array(), true);

	$json_data = array(
        "success" => true
    );
	
	if (__DEVELOPER__){
		$json_data['query'] = $query;
	}

	if ($json){
	    die(json_encode($json_data));
	}
	return $json_data;
}

function mssql_insert_documents($documents, $refKey, $refVal, $ClientId  = null, $die = false, $extra = null){
	if (!$documents){
		return;
	}
	
	require_once(__HELPER__ . 'files.php');
	
	$files = explode(';', $documents);
	$result = array();
	
	foreach ($files as $path){
		$filepath = __ROOT__ . $path;
		if (!is_file($filepath)){
			print_r('nera failo');
			die();
		}
		$file = get_file_attributes($filepath);
		$insert = array(
			'table'  => 'Documents',
			'pk_key' => 'DocumentId',
			'pk_val' => -1,
			'Name'   => $file['basename'],
			'Date'   => date('Y-m-d H:i:s'),
			'Path'   => $path,
			'Size'   => $file['size'],
			'Ext'    => $file['ext'],
			'UserId' => __USERID__,
			'refKey' => $refKey,
			'refVal' => $refVal
		);


		// ZANAS -- 2017-11-01

		if(is_array($extra) && count($extra)){
			$insert = array_merge($insert, $extra);
		}
		
		// ----- -- 2017-11-01
		
		$result = mssql_insert($insert, false);
	}
	if ($die === true){
		return $result;
	}
}

function mssql_insert_if_not_exists($postas, $json = true){
	$exists = mssql_row_column("SELECT COUNT(*) FROM $postas[table] WHERE $postas[pk_key] = '$postas[pk_val]'");
	
	if ($exists == 1){
		return;
	}
	
	$postas[$postas['pk_key']] = $postas['pk_val'];
	
	return mssql_insert($postas, $json);
}

function mssql_crud($query, $json = true){
	if (!is_string($query)){
	    die(json_encode(array('success' => false, 'msg'=>'query TURI BŪTI string tipo!')));
	}
	
	global $conn;
	
	mysqli_begin_transaction($conn);
	
	$stmt = query($query);
	
	$results = mysqli_stmt_get_result($stmt);
	
	
	$res_array = array();
	if ($results){
		while( $row = mysqli_fetch_array($results, MYSQLI_NUM) ) {
			$res_array[] = $row;//mssql_row_repair($row, $meta, false);
		}
		
		mysqli_free_result($results);
	}
	
	
	mysqli_commit($conn);
	
	$json_data = array(
		"success" => true,
		"data" => $results[0],
		"pk" => $scope
	);
	
	if (__DEVELOPER__){
		$json_data['query'] = $query;
	}
	
	log_actions('crud', 'crud', '-1', $query, array(), array(), array());

	$json_data = array(
		"success" => true,
		"data" => $results[0],
		"pk" => $scope
	);
	
	if (__DEVELOPER__){
		$json_data['query'] = $query;
	}
	
	log_actions('crud', 'crud', '-1', $query, array(), array(), true);
	
	if ($json){
		die(json_encode($json_data));
	}
	return $json_data;
}

function mssql_crud_admin($query, $key = ''){
	$stmt = sqlsrv_query(__CONN__, $query, $params);

	if ($stmt === false){
		mssql_error();
	}

	while (sqlsrv_next_result($stmt)){
		$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);
		if ($row){
			$results[] = $row;
		}
	}
	sqlsrv_free_stmt($stmt);

	return $results;
}

function mssql_crud_raw($query){
	//$query = str_replace(';', '; ', $query);
	global $conn;
	$result = mysqli_multi_query($conn, $query);
	if($result == false){
		$error = mysqli_error ($conn);
		echo $query;
		die($error);
	}
	
	while(mysqli_more_results($conn)){
		mysqli_next_result($conn);
	}
	
	return;
}

?>