<?php

function mssql_magic_post($post){
	return str_replace(
		array("\'", '\"', "'", "?","\\0"), //\0 preg_replace atpazista kaip ?, tada nukencia mssql raw xbug
		array("'", '"', "''", "&#63;",'&#92;&#48;'),
		$post
	);
}

function mssql_unset_reserved($postas){
	if (($key = array_search('NULL', $postas)) !== false){ //Trinam 'NULL' reikšmes //array_search(strtolower($search), array_map('strtolower', $array)); <--xbug, greiciausiai turetu buti sitas, o kaip insert, update?
		unset($postas[$key]);
	}

	$mssql_reserved = array('pk_key', 'pk_val','fk_key' ,'fk_val', 'table','action','postaction','documents', 'undefined');//Donatas 2018-01-05: pridejau 'undefined'
	return array_diff_key($postas,array_flip($mssql_reserved));
}

function check_login(){
	if (__USERID__ !== false || defined('__SKIPLOGIN__') === true || __URL__ == 'web'){
		return;
	}
	
	$json_data = array(
		"success"	=> false,
		"msg" 		=> 'Reikia prisijungti'
	);
	
	if (__DEVELOPER__){
		$json_data['db'] = __SOLUTION__;
	}
	
	if (isset($_COOKIE["au"])){
		require_once(__PHP__ . 'login/autologin_' . __URL__ . '.php');
		die();
	}
	
	if (__JSON__ === false){
		require_once(__PHP__ . 'login/screen.php');
		die();
	}
	
	die(json_encode($json_data));
}

function mssql_prevent_injection($query){
	$bad = array(
		'--', 'drop', 'sys.', 'delete', 'truncate', ' or ',
		'exec', 'sp_', 'master', 'dbo.', 'alter', 'create',
		'database', 'table', 'db_name', 'scope', 'insert',
		'update', '/*', '*/','!', ';','/0', 'char(','char ','union',
		'MD5', 'having','version','shell', 'declare',' out ', 'xp_',
		'.dll', 'sysprocesses', '..', 'sys', 'exists', 'xtype', '@',
		'schema', 'ascii', '0x', 'delay', 'waitfor', 'sleep',
		' top ','bulk','/', 'collate',' ALL ', '()', 'server', 'into', 'USER', 'password','.php'
	);

	foreach($bad as $b){
		if (stripos($query, $b) !== false){
			if (__DEVELOPER__){
				xlogf('_galima injekcija - susitvarkykite query_');
				xlogf($query);
				xlog($b);
			}
			$json_data = array( 'success' => false);
			die(json_encode($json_data, JSON_UNESCAPED_UNICODE));
		}
	}
}

function mssql_get_scope_identity($stmt){
	global $conn;
	return mysqli_insert_id($conn);
}

function mssql_get_raw_query($query, $params = array()){
	//xlogm($params);
	
	if (count($params) === 0){
		return $query;
	}

    $keys = array();
	$values = $params;

    foreach ($params as $key => $value){
		if (is_string($key)){
            $keys[] = '/:'.$key.'/';
        } else {
            $keys[] = '/[?]/';
        }
		
		if (is_numeric($value)){
			$temp = ltrim($value, '0');
			if (strlen($temp) != strlen($value) && strlen($temp) > 0){
				$values[$key] = "'" . $value . "'";
			} else {
				$values[$key] = $value;
			}

			/*
			if (ctype_digit($value) && strpos($value, '0') === 0){ //fix 000077
				$values[$key] = "'" . $value . "'";
			} else {
				$values[$key] = $value;
			}
			*/
			continue; 
		}
		
        if (is_string($value)){
			if($value == 'GETDATE()'){
				$values[$key] = $value;
			} else if ($value){
				$values[$key] = "'" . $value . "'";
			} else {
				$values[$key] = 'null';
			}
			continue;
		}
		
        if (is_array($value)){
            $values[$key] = "'" . implode("','", $value) . "'";
			continue;
		}
		
        if (is_null($value)){
            $values[$key] = 'null';
		}
    }

    $query = str_replace(array('&#63;', '&#92;&#48;'), array('?', '\\0'), preg_replace($keys, $values, $query, 1));
	//xlogm($query);
    return $query;
}

function prepare_paging($start, $length){
	return "limit  {$start}, {$length}";
}