<?php
//done
function mssql_row($query){
	$stmt = query($query);
	$result = mysqli_stmt_get_result($stmt);
	if ($result === false){
		mssql_error($query);
	}
	$rows = mysqli_num_rows($result);
	if ($rows > 1){
		mysqli_free_result($result);
		die('Klaida: gražina daugiau nei vieną eilutę:' .$query);
	} else if ($rows == 0){
		mysqli_free_result($result);
		return false;
	}
	
	$meta = mysqli_stmt_result_metadata($stmt);
	$row = mysqli_fetch_assoc($result);
	mysqli_free_result($result);
	return mssql_row_repair_assoc($row, $meta);
}
//don't need a fix
function mssql_row_column($query){
	$array = mssql_row($query);
	if(!$array || empty($array)) {
		return $array;
	}
	
	return reset($array);
}
//done
function mssql_fetch($query){
	$stmt = query($query);
	$meta = mysqli_stmt_result_metadata($stmt);
	$result = mysqli_stmt_get_result($stmt);
	if (!isset($result)){
		mssql_error($query);
	}
	$res_array = array();
	while( $row = mysqli_fetch_array($result, MYSQLI_ASSOC) ) {
		$res_array[] = mssql_row_repair_assoc($row, $meta);
	}
	mysqli_free_result($result);
	return $res_array;
}
//done
function mssql_fetch_vals($query){
	$stmt = query($query); //xlog(sqlsrv_field_metadata($stmt));
	$meta = mysqli_stmt_result_metadata($stmt);
	
	$result = mysqli_stmt_get_result($stmt);

	$res_array = array();
	while( $row = mysqli_fetch_array($result, MYSQLI_NUM ) ) {
		$res_array[] = mssql_row_repair_numeric($row, $meta);
	}
	mysqli_free_result($result);
	return $res_array;
}
//done
function mssql_fetch_column($query, $sort = null){
	$stmt = query($query); //xlog(sqlsrv_field_metadata($stmt));
	$meta = mysqli_stmt_result_metadata($stmt);
	
	$result = mysqli_stmt_get_result($stmt);

	$res_array = array();
	while( $row = mysqli_fetch_array($result, MYSQLI_NUM ) ) {
		$res_array[] = array_values(mssql_row_repair_numeric($row, $meta))[0];
	}
	mysqli_free_result($result);
	
	if ($sort === null){
		return $res_array;
	} elseif ($sort === true){
		asort($res_array);
		return $res_array;
	} else {
		arsort($res_array);
		return $res_array;
	}
	
	return $res_array;
}
//done
function mssql_fetch_key($query, $key, $single = true){
	$stmt = query($query);
	$meta = mysqli_stmt_result_metadata($stmt);
	$result = mysqli_stmt_get_result($stmt);
	
	if (!$result){
		mssql_error($query);
	}
	
	
	$res_array = array();
	if (mysqli_stmt_field_count($stmt) == 2){
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			$index = $row[$key];
			unset($row[$key]);
			$res_array[$index] = reset(mssql_row_repair_assoc($row, $meta));
		}
	} else {
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			if ($single === true){
				$res_array[$row[$key]] = mssql_row_repair_assoc($row, $meta);
			} else {
				$res_array[$row[$key]][] = mssql_row_repair_assoc($row, $meta);
			}
		}
	}
	
	
	
	mysqli_free_result($result);
	return $res_array;
}

function mssql_exists($postas, $only = true){ //false = return pk
	$changed = array_map('trim', mssql_unset_reserved($postas));
	
	$changed = array_map(function($k, $v){
		return "{$k} = '{$v}'";
	}, array_keys($changed), $changed);
	
	if (count($changed) == 0){
		if (!empty($postas['fk_key'])){
			$changed[] = $postas['fk_key'] . " = '" . $postas['fk_val'] . "'";
		} else {
			$changed[] = $postas['pk_key'] . " = '" . $postas['pk_val'] . "'";
		}
	}

	if ($only){
		$query = "SELECT TOP 1 FROM {$postas['table']} WHERE " . implode(' AND ', $changed);
		
		$stmt = query($query);
		$rows = mysqli_num_rows($stmt);
		sqlsrv_free_stmt($stmt);
		return (($rows > 0) ? true : false);
	}
	
	$query = "SELECT {$postas['pk_key']} FROM {$postas['table']} WHERE " . implode(' AND ', $changed) . ' LIMIT 1';
	//xlog($query);
	$row = mssql_row_column($query);
	//xlog($row);
	return (!empty($row) ? $row : false);
}
//don't need a fix
function mssql_not_exists($postas){
	return !mssql_exists($postas);
}

function mssql_exists2($query){
	$stmt = query($query);
	$result = mysqli_stmt_get_result($stmt);
	if ($result === false){
		mssql_error($query);
	}
	$rows = mysqli_num_rows($result);
	if ($rows >= 1){
		mysqli_free_result($result);
		return true;
	} else if ($rows == 0){
		mysqli_free_result($result);
		return false;
	}
	
	return false;
}

?>