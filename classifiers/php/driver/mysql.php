<?php
$conn = mysqli_connect($connection['server'], $connection['username'], $connection['password'], $connection['dbname']);

if(mysqli_connect_errno()){
	die('Negaliu prisijungti prie ' . (__DEVELOPER__ ? __SOLUTION__ : 'DB'));
}

$conn->set_charset("utf8");

function query($query){
	global $conn;
	$stmt = mysqli_prepare($conn, $query);
	if ($stmt === false){
		mssql_error($query);
	}
	$r = mysqli_stmt_execute($stmt);
	if ($r === false){
		mssql_error($query);
	}
	return $stmt;
}

require_once(__PHP__ . 'driver/mysql/helper.php');
require_once(__PHP__ . 'driver/mysql/error.php');
require_once(__PHP__ . 'driver/mysql/repair.php');
require_once(__PHP__ . 'driver/mysql/select.php');
require_once(__PHP__ . 'driver/mysql/log.php');
require_once(__PHP__ . 'driver/mysql/postaction.php');
require_once(__PHP__ . 'driver/mysql/crud.php');

function mssql_connection_close(){
	global $conn;
	mysqli_close($conn);
}
?>