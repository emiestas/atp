<?php

function get_upload_path($folder, $name){
	if (is_array($name)){
		$name = $name->basename;
	}
	
	$path = get_upload_folder('uploads', $folder);
	
	return (object)array(
		'full' => $path . $name,
		'short' => str_replace(__ROOT__, '/', $path) . $name
	);
}

function get_upload_folder($uploads, $folder){
	if($uploads == 'blanks'){
		$tree = __UPLOADS__ . 'blanks/' . $folder;
	}else{
		$tree = __UPLOADS__ . $folder . '/' . (string)date("Y/m/d");
	}

	return create_dir_tree($tree);
}

function create_dir_tree($tree){
	if (!is_dir($tree)){
		@mkdir($tree, 0777, true);
	}

	return $tree . '/';
}

function get_file_attributes($file){
	if (!$file){
		die(json_encode(array(
			'success' => false,
			'msg' =>'Nepaduodas failas'
		)));
	}

	if (is_array($file)){
		$info = pathinfo($file['name']);
		get_upload_badextensions($info['filename'], $info['extension']);

		//$mimetype = get_extension_from_mimetype('audio/mpeg'); //mime_content_type($file['tmp_name']);
		$mimetype = mime_content_type($file['tmp_name']);
		return array (
			'name' => get_alias($info['filename']),
			'ext' => $info['extension'],
			'size' => $file['size'],
			'tmp' => $file['tmp_name'],
			'basename' => $info['basename'],
			'mimetype' => $mimetype,
			'type' => explode('/', $mimetype)[0]
		);
	} else if (is_file($file)){
		$info = pathinfo($file);
		//xlog($info);
		$mimetype = mime_content_type($file);

		return array (
			'name' => get_alias($info['filename']),
			'ext' => $info['extension'],
			'basename' => $info['basename'],
			'size' => filesize($file),
			'mimetype' => $mimetype,
			'type' => explode('/', $mimetype)[0],
			'dir' => str_replace(__ROOT__, '/', $info['dirname']) .'/',
			'download' => str_replace(__ROOT__, '/', $info['dirname']) .'/' . $info['basename'],
			'path' => $file,
			'pathDir' => dirname($file) . '/'
		);
	} else {
		$info = pathinfo($file);

		return array (
			'name' => get_alias($info['filename']),
			'ext' => $info['extension'],
			'basename' => $info['basename']
		);
	}
}

function get_upload_file_unique($tree, $file){
	$alias = get_alias($file['name'], true);
	$filename = $tree . $alias . '.' . $file['ext'];

	if (is_file($filename)){
		$i = 1;
		while (is_file($filename)){
			$filename = $tree . $alias . "_{$i}." . $file['ext'];
			$i++;
		}
	}
	
	return $filename;
}

$allowedfileextensions = array();

function get_upload_badextensions($name, $ext){
	$bad = array('php', 'exe', 'html', 'js', 'tpl');

	if(!in_array($ext, $bad)){
		return;
	}

	die(json_encode(array(
		'success' => false,
		'name' => $name,
		'msg' => 'Bandėte įkelti uždraustą dokumentą, operacija nutraukta'
	)));
}

function get_file_path($fullpath){
	return str_replace(__ROOT__, '/', $fullpath);
}

function get_alias($text, $upload = false) {
	$from = array('Ё', 'Й', 'Ц', 'У', 'К', 'Е', 'Н', 'Г', 'Ш', 'Щ', 'З', 'Х', 'Ъ', 'Ф', 'Ы', 'В', 'А', 'П', 'Р', 'О', 'Л', 'Д', 'Ж', 'Э', 'Я', 'Ч', 'С', 'М', 'И', 'Т', 'Ь', 'Б', 'Ю');
    $to = array('ё', 'й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ', 'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э', 'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю');
    $text = str_replace($from, $to, $text);
    $from = array('ё', 'й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ', 'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э', 'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю');
    $to = array('io', 'i', 'c', 'u', 'k', 'e', 'n', 'g', 's', 's', 'z', 'ch', '', 'f', 'i', 'v', 'a', 'p', 'r', 'o', 'l', 'd', 'z', 'e', 'ja', 'c', 's', 'm', 'i', 't', '', 'b', 'ju');
    $text = str_replace($from, $to, $text);

    $from = array('Ą', 'Č', 'Ę', 'Ė', 'Į', 'Š', 'Ų', 'Ū', 'Ž', ' ');
    $to = array('a', 'c', 'e', 'e', 'i', 's', 'u', 'u', 'z', '-');
    $text = str_replace($from, $to, $text);

    $from = array('ą', 'č', 'ę', 'ė', 'į', 'š', 'ų', 'ū', 'ž', ' ');
    $to = array('a', 'c', 'e', 'e', 'i', 's', 'u', 'u', 'z', '-');
    $text = str_replace($from, $to, $text);

    $from = array(': ', ';', ',', '+', '„', '“', "\"");
    $to   = array('-', '-', '-', '', '', '', '');
    $text = str_replace($from, $to, $text);

    if (!$upload){
        $text = preg_replace("/[^a-zA-Z0-9]+/", "-", $text);
    }

    if (substr($text, strlen($text) - 1, strlen($text)) == '-') {
        $text = substr($text, 0, strlen($text) - 1);
    }

    return strtolower($text);
}

function rename_file_alias($path){
	$file = pathinfo($path);
	$alias = get_alias($file['filename'],true);
	$newpath = $file['dirname'] . '/' . $alias . '.' . $file['extension'];
	//xlog($newpath);
	rename($path, $newpath);
	
	return get_file_attributes($newpath);
}

function get_filesize($file){
  $size = filesize($file);
  $units = array( 'b', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb');
  $power = $size > 0 ? floor(log($size, 1024)) : 0;
  return number_format($size / pow(1024, $power), 2, '.', ',') . $units[$power];
}

function create_zip($files, $zip_name = '') {
	$zip_name = rtrim($zip_name, '.zip') . '.zip';
	$zip = new ZipArchive;
	$zip_dir  = __UPLOADS__ . 'zips/';
	$zip_path = $zip_dir.$zip_name;

	if ( !file_exists ($zip_dir) ) {
		mkdir ($zip_dir);
	}

	$res = $zip->open($zip_path, ZIPARCHIVE::CREATE) or die('Negaliu sukurti zip');

	if($res == 1){
		foreach($files as $file) {
			$zip->addFile($file['path'], $file['basename']) or die('Negaliu pridėti failo: ' . $file['path']);
		}

		$zip->close();
		return get_file_attributes($zip_path);
	} 
}

function unzip($filepath){
	if (is_array($filepath)){
		$filepath = $filepath['path'];
	}
	
	
	$zip = new ZipArchive;
	$zip->open($filepath);
	$zip->extractTo(__DIR_TEMP__ . 'zip');
	$zip->close();

	$files = array();

	$dir = scandir(__DIR_TEMP__ . 'zip/');

	foreach ($dir as $file){
		if ($file == '.' OR $file == '..'){
			continue;
		}
		$file = __DIR_TEMP__ . 'zip/' . $file;

		$att = get_file_attributes($file);
		$att['path'] = $file;
		$att['pathDir'] = __DIR_TEMP__ . 'zip/';
		array_push($files, $att);
	}

	return $files;
}

function downloadFile($url, $folder = __DIR_TEMP__){
	if($folder == __DIR_TEMP__){
		$folder = __DIR_TEMP__ . 'temp/';
		if (!is_dir($folder)){
			@mkdir($folder, 0777, true);
		}
	} else {
		$folder = get_upload_folder(__UPLOADS__, $folder);
	}
	$file = get_file_attributes($url);
	$path = $folder . $file['basename'];
	file_put_contents($path, fopen($url, 'r'));

	$file = get_file_attributes($path);
	//$file['path'] = $path;
	//$file['pathDir'] = $folder;
	return $file;
}

function get_extension_from_mimetype($mimetype){

	$types = array(
		'hqx'	=>	array('application/mac-binhex40', 'application/mac-binhex', 'application/x-binhex40', 'application/x-mac-binhex40'),
		'cpt'	=>	'application/mac-compactpro',
		'csv'	=>	array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain'),
		'bin'	=>	array('application/macbinary', 'application/mac-binary', 'application/octet-stream', 'application/x-binary', 'application/x-macbinary'),
		'dms'	=>	'application/octet-stream',
		'lha'	=>	'application/octet-stream',
		'lzh'	=>	'application/octet-stream',
		'exe'	=>	array('application/octet-stream', 'application/x-msdownload'),
		'class'	=>	'application/octet-stream',
		'psd'	=>	array('application/x-photoshop', 'image/vnd.adobe.photoshop'),
		'so'	=>	'application/octet-stream',
		'sea'	=>	'application/octet-stream',
		'dll'	=>	'application/octet-stream',
		'oda'	=>	'application/oda',
		'pdf'	=>	array('application/pdf', 'application/force-download', 'application/x-download', 'binary/octet-stream'),
		'ai'	=>	array('application/pdf', 'application/postscript'),
		'eps'	=>	'application/postscript',
		'ps'	=>	'application/postscript',
		'smi'	=>	'application/smil',
		'smil'	=>	'application/smil',
		'mif'	=>	'application/vnd.mif',
		'xls'	=>	array('application/vnd.ms-excel', 'application/msexcel', 'application/x-msexcel', 'application/x-ms-excel', 'application/x-excel', 'application/x-dos_ms_excel', 'application/xls', 'application/x-xls', 'application/excel', 'application/download', 'application/vnd.ms-office', 'application/msword'),
		'ppt'	=>	array('application/powerpoint', 'application/vnd.ms-powerpoint', 'application/vnd.ms-office', 'application/msword'),
		'pptx'	=> 	array('application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/x-zip', 'application/zip'),
		'wbxml'	=>	'application/wbxml',
		'wmlc'	=>	'application/wmlc',
		'dcr'	=>	'application/x-director',
		'dir'	=>	'application/x-director',
		'dxr'	=>	'application/x-director',
		'dvi'	=>	'application/x-dvi',
		'gtar'	=>	'application/x-gtar',
		'gz'	=>	'application/x-gzip',
		'gzip'  =>	'application/x-gzip',
		'php'	=>	array('application/x-httpd-php', 'application/php', 'application/x-php', 'text/php', 'text/x-php', 'application/x-httpd-php-source'),
		'php4'	=>	'application/x-httpd-php',
		'php3'	=>	'application/x-httpd-php',
		'phtml'	=>	'application/x-httpd-php',
		'phps'	=>	'application/x-httpd-php-source',
		'js'	=>	array('application/x-javascript', 'text/plain'),
		'swf'	=>	'application/x-shockwave-flash',
		'sit'	=>	'application/x-stuffit',
		'tar'	=>	'application/x-tar',
		'tgz'	=>	array('application/x-tar', 'application/x-gzip-compressed'),
		'z'	=>	'application/x-compress',
		'xhtml'	=>	'application/xhtml+xml',
		'xht'	=>	'application/xhtml+xml',
		'zip'	=>	array('application/x-zip', 'application/zip', 'application/x-zip-compressed', 'application/s-compressed', 'multipart/x-zip'),
		'rar'	=>	array('application/x-rar', 'application/rar', 'application/x-rar-compressed'),
		'mid'	=>	'audio/midi',
		'midi'	=>	'audio/midi',
		'mpga'	=>	'audio/mpeg',
		'mp2'	=>	'audio/mpeg',
		'mp3'	=>	array('audio/mpeg', 'audio/mpg', 'audio/mpeg3', 'audio/mp3'),
		'aif'	=>	array('audio/x-aiff', 'audio/aiff'),
		'aiff'	=>	array('audio/x-aiff', 'audio/aiff'),
		'aifc'	=>	'audio/x-aiff',
		'ram'	=>	'audio/x-pn-realaudio',
		'rm'	=>	'audio/x-pn-realaudio',
		'rpm'	=>	'audio/x-pn-realaudio-plugin',
		'ra'	=>	'audio/x-realaudio',
		'rv'	=>	'video/vnd.rn-realvideo',
		'wav'	=>	array('audio/x-wav', 'audio/wave', 'audio/wav'),
		'bmp'	=>	array('image/bmp', 'image/x-bmp', 'image/x-bitmap', 'image/x-xbitmap', 'image/x-win-bitmap', 'image/x-windows-bmp', 'image/ms-bmp', 'image/x-ms-bmp', 'application/bmp', 'application/x-bmp', 'application/x-win-bitmap'),
		'gif'	=>	'image/gif',
		'jpg'	=>	array('image/jpeg', 'image/pjpeg'),
		'jp2'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
		'j2k'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
		'jpf'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
		'jpg2'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
		'jpx'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
		'jpm'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
		'mj2'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
		'mjp2'	=>	array('image/jp2', 'video/mj2', 'image/jpx', 'image/jpm'),
		'png'	=>	array('image/png',  'image/x-png'),
		'tiff'	=>	'image/tiff',
		'tif'	=>	'image/tiff',
		'css'	=>	array('text/css', 'text/plain'),
		'html'	=>	array('text/html', 'text/plain'),
		'htm'	=>	array('text/html', 'text/plain'),
		'shtml'	=>	array('text/html', 'text/plain'),
		'txt'	=>	'text/plain',
		'text'	=>	'text/plain',
		'log'	=>	array('text/plain', 'text/x-log'),
		'rtx'	=>	'text/richtext',
		'rtf'	=>	'text/rtf',
		'xml'	=>	array('application/xml', 'text/xml', 'text/plain'),
		'xsl'	=>	array('application/xml', 'text/xsl', 'text/xml'),
		'mpeg'	=>	'video/mpeg',
		'mpg'	=>	'video/mpeg',
		'mpe'	=>	'video/mpeg',
		'qt'	=>	'video/quicktime',
		'mov'	=>	'video/quicktime',
		'avi'	=>	array('video/x-msvideo', 'video/msvideo', 'video/avi', 'application/x-troff-msvideo'),
		'movie'	=>	'video/x-sgi-movie',
		'doc'	=>	array('application/msword', 'application/vnd.ms-office'),
		'docx'	=>	array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/zip', 'application/msword', 'application/x-zip'),
		'dot'	=>	array('application/msword', 'application/vnd.ms-office'),
		'dotx'	=>	array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/zip', 'application/msword'),
		'xlsx'	=>	array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/zip', 'application/vnd.ms-excel', 'application/msword', 'application/x-zip'),
		'word'	=>	array('application/msword', 'application/octet-stream'),
		'xl'	=>	'application/excel',
		'eml'	=>	'message/rfc822',
		'json'  =>	array('application/json', 'text/json'),
		'pem'   =>	array('application/x-x509-user-cert', 'application/x-pem-file', 'application/octet-stream'),
		'p10'   =>	array('application/x-pkcs10', 'application/pkcs10'),
		'p12'   =>	'application/x-pkcs12',
		'p7a'   =>	'application/x-pkcs7-signature',
		'p7c'   =>	array('application/pkcs7-mime', 'application/x-pkcs7-mime'),
		'p7m'   =>	array('application/pkcs7-mime', 'application/x-pkcs7-mime'),
		'p7r'   =>	'application/x-pkcs7-certreqresp',
		'p7s'   =>	'application/pkcs7-signature',
		'crt'   =>	array('application/x-x509-ca-cert', 'application/x-x509-user-cert', 'application/pkix-cert'),
		'crl'   =>	array('application/pkix-crl', 'application/pkcs-crl'),
		'der'   =>	'application/x-x509-ca-cert',
		'kdb'   =>	'application/octet-stream',
		'pgp'   =>	'application/pgp',
		'gpg'   =>	'application/gpg-keys',
		'sst'   =>	'application/octet-stream',
		'csr'   =>	'application/octet-stream',
		'rsa'   =>	'application/x-pkcs7',
		'cer'   =>	array('application/pkix-cert', 'application/x-x509-ca-cert'),
		'3g2'   =>	'video/3gpp2',
		'3gp'   =>	array('video/3gp', 'video/3gpp'),
		'mp4'   =>	'video/mp4',
		'm4a'   =>	'audio/x-m4a',
		'f4v'   =>	array('video/mp4', 'video/x-f4v'),
		'flv'	=>	'video/x-flv',
		'webm'	=>	'video/webm',
		'aac'   =>	'audio/aac', //pakeista is audio/x-acc
		'm4u'   =>	'application/vnd.mpegurl',
		'm3u'   =>	'text/plain',
		'xspf'  =>	'application/xspf+xml',
		'vlc'   =>	'application/videolan',
		'wmv'   =>	array('video/x-ms-wmv', 'video/x-ms-asf'),
		'au'    =>	'audio/x-au',
		'ac3'   =>	'audio/ac3',
		'flac'  =>	'audio/x-flac',
		'ogg'   =>	array('audio/ogg', 'video/ogg', 'application/ogg'),
		'kmz'	=>	array('application/vnd.google-earth.kmz', 'application/zip', 'application/x-zip'),
		'kml'	=>	array('application/vnd.google-earth.kml+xml', 'application/xml', 'text/xml'),
		'ics'	=>	'text/calendar',
		'ical'	=>	'text/calendar',
		'zsh'	=>	'text/x-scriptzsh',
		'7zip'	=>	array('application/x-compressed', 'application/x-zip-compressed', 'application/zip', 'multipart/x-zip'),
		'cdr'	=>	array('application/cdr', 'application/coreldraw', 'application/x-cdr', 'application/x-coreldraw', 'image/cdr', 'image/x-cdr', 'zz-application/zz-winassoc-cdr'),
		'wma'	=>	array('audio/x-ms-wma', 'video/x-ms-asf'),
		'jar'	=>	array('application/java-archive', 'application/x-java-application', 'application/x-jar', 'application/x-compressed'),
		'svg'	=>	array('image/svg+xml', 'application/xml', 'text/xml'),
		'vcf'	=>	'text/x-vcard',
		'srt'	=>	array('text/srt', 'text/plain'),
		'vtt'	=>	array('text/vtt', 'text/plain'),
		'ico'	=>	array('image/x-icon', 'image/x-ico', 'image/vnd.microsoft.icon'),
		'odc'	=>	'application/vnd.oasis.opendocument.chart',
		'otc'	=>	'application/vnd.oasis.opendocument.chart-template',
		'odf'	=>	'application/vnd.oasis.opendocument.formula',
		'otf'	=>	'application/vnd.oasis.opendocument.formula-template',
		'odg'	=>	'application/vnd.oasis.opendocument.graphics',
		'otg'	=>	'application/vnd.oasis.opendocument.graphics-template',
		'odi'	=>	'application/vnd.oasis.opendocument.image',
		'oti'	=>	'application/vnd.oasis.opendocument.image-template',
		'odp'	=>	'application/vnd.oasis.opendocument.presentation',
		'otp'	=>	'application/vnd.oasis.opendocument.presentation-template',
		'ods'	=>	'application/vnd.oasis.opendocument.spreadsheet',
		'ots'	=>	'application/vnd.oasis.opendocument.spreadsheet-template',
		'odt'	=>	'application/vnd.oasis.opendocument.text',
		'odm'	=>	'application/vnd.oasis.opendocument.text-master',
		'ott'	=>	'application/vnd.oasis.opendocument.text-template',
		'oth'	=>	'application/vnd.oasis.opendocument.text-web'
	);

	//$types_reversed = array_reverse($types);

	//return $types_reversed[$mimetype];

	//ieskome reikiamo extensiono
    foreach ($types as $key => $val) {
		//tikrinam ar val yra masyvas, jeigu taip einam per jo elementus ir ieskom reiksmes.
		if(is_array($val)){
			foreach ($val as $inner_key => $inner_val) {
				if ($inner_val == $mimetype) {
					return $key;
				}
			}
		} else { //jeigu ne masyvas, tai tiesiog ieskome paciame $val
			if ($val == $mimetype) {
				return $key;
			}

		}
    }
    return null;

}

function safe_temp_unlink($path){
	if (stripos($path, 'templates_c') === false){
		die('Ne temp');
	}

	if (!is_file($path)){
		die('Ne failas');
	}

	unlink($path);
}

function resizeImage($imagePath, $width, $height, $filterType, $blur, $bestFit, $cropZoom) {
	$force_width = $force_height = false;

    if (substr($width, 0, 1) == 'f'){
		$force_width = true;
		$width = str_replace('f', '', $width);
	}

	if (substr($height, 0, 1) == 'f'){
		$force_height = true;
		$height = str_replace('f', '', $height);
	}



	//The blur factor where &gt; 1 is blurry, &lt; 1 is sharp.
    $imagick = new \Imagick(realpath($imagePath));




	if($force_width && $force_height){
		$cropZoom = true;
	}

    if ($cropZoom) {

		/*
			$cropZoom sprendimas:
			http://php.net/manual/en/imagick.cropimage.php#119086
		*/

		$w = $imagick->getImageWidth();
		$h = $imagick->getImageHeight();
		$new_w = $width;
		$new_h = $height;

		$focus = 'center';

		if ($w > $h) {
			$resize_w = $w * $new_h / $h;
			$resize_h = $new_h;
		}
		else {
			$resize_w = $new_w;
			$resize_h = $h * $new_w / $w;
		}

		$imagick->resizeImage($resize_w, $resize_h, $filterType, $blur);
		$imagick->setImageCompressionQuality(100);

		switch ($focus) {
			case 'northwest':
				$imagick->cropImage($new_w, $new_h, 0, 0);
				break;

			case 'center':
				$imagick->cropImage($new_w, $new_h, ($resize_w - $new_w) / 2, ($resize_h - $new_h) / 2);
				break;

			case 'northeast':
				$imagick->cropImage($new_w, $new_h, $resize_w - $new_w, 0);
				break;

			case 'southwest':
				$imagick->cropImage($new_w, $new_h, 0, $resize_h - $new_h);
				break;

			case 'southeast':
				$imagick->cropImage($new_w, $new_h, $resize_w - $new_w, $resize_h - $new_h);
				break;
		}
		/*
			$newWidth = $cropWidth / 2;
			$newHeight = $cropHeight / 2;

			$imagick->cropimage(
				$newWidth,
				$newHeight,
				($cropWidth - $newWidth) / 2,
				($cropHeight - $newHeight) / 2
			);

			$imagick->scaleimage(
				$imagick->getImageWidth() * 2,
				$imagick->getImageHeight() * 2
			);
		*/
    } else {
	    $imagick->resizeImage($width, $height, $filterType, $blur, $bestFit);
	}

	return $imagick;
}
?>