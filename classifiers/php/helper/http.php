<?php

function prevent_confirm_form_resubmission(){
	header("Cache-Control: no cache");
	session_cache_limiter("private_no_expire");
}

function http_auto_refresh(){
	echo '<meta http-equiv="refresh" content="10">';
}

function http_response_close(){
	ob_start();
	echo "ok";
	header("Content-Length: ". ob_get_length());//send length header
	header("Connection: close");//or redirect to some url: header('Location: http://www.google.com');
	ob_end_flush();
	flush();
	fastcgi_finish_request();
}

function http_get_web($url, $ssl = false){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_REFERER, '*.google.com');
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 10);
	if ($ssl === true){
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	}
	//redirect follow Moved permanently
	curl_setopt($ch, CURLOPT_POSTREDIR, 3);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

	$data = curl_exec($ch);
	curl_close($ch);

	return $data;
}

function http_get_url_data($url, $post){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_REFERER, '*.google.com');
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 10);

    $data = curl_exec($ch);
    curl_close($ch);

	// reikės tikrinimo dėl curl_error()

    return $data;
}

function http_get_file($url, $die = true){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_REFERER, '*.google.com');
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 10);

	$data = curl_exec($ch);
	$info = curl_getinfo($ch);
	$response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);

	if ($response_code != 200 && $die === true){
		die('Nerastas failas :' . $url);
	} else if($response_code != 200){ // 17-11-09 - pridetas else if kad nemirtu be nieko kai die=false
		return false;
	}

	$header_size = $info['header_size'];
	$header = substr($data, 0, $header_size);
	$body = substr($data, $header_size);

	return $body;
}

?>