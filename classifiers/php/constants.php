<?php
define('__JSON__', (strpos($_SERVER['HTTP_ACCEPT'],'application/json') !== false));
define('__AJAX__', (isset($_SERVER['HTTP_X_REQUESTED_WITH'])));

define('__PRODUCTION__', false);
define('__SOLUTION__', $connection['dbname']);

define('__ROOT__', $_SERVER['DOCUMENT_ROOT'] . '/lexita/');
define('__DIR_TEMP__', __ROOT__ . 'temp/');
define('__PHP__', __ROOT__ . 'php/');
require(__PHP__ . 'admins.php');
require_once(__PHP__ . 'debug.php');
define('__HELPER__', __PHP__ . 'helper/');
define('__PLUGINS__', __PHP__ . 'plugins/');
define('__USERID__', 1);
require_once(__PHP__ . 'driver/' . __DRIVER__ . '.php');