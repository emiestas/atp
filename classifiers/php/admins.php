<?php
$developers_ip_array = array('127.0.0.1');
$masters_ip_array = array('127.0.0.1');

define('__DEVELOPER__', in_array($_SERVER['REMOTE_ADDR'], array_merge($developers_ip_array,$masters_ip_array)));
define('__MASTER__', in_array($_SERVER['REMOTE_ADDR'], $masters_ip_array));
?>