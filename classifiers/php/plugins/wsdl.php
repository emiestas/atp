<?php
class wsdl {
	public $debug_level = 0;
	private $soap;
	private $config = array(
		"exceptions" => true,
		"soap_version" => SOAP_1_1,
		"connection_timeout" => -1,
		'trace' => true,
		'cache_wsdl' => WSDL_CACHE_NONE,
		'keep_alive' => false,
		'encoding' => 'UTF-8'
	);
	
	public function __construct($url, $config = array(), $auth = array()){
		ini_set("default_socket_timeout", $this->config['connection_timeout']);
		ini_set("soap.wsdl_cache_enabled", WSDL_CACHE_NONE);
		$soap = new SoapClient($url, array_merge($this->config, $config));
		$this->soap = $soap;
		
		if (isset($auth['method'])){
			if ($auth['method'] == 'WSSE:PLAIN'){
				$this->wsse_plain($auth);
			}
		}
	}
	
	public function request($command, $params, $xml = false, $filename = ''){
		$soap = $this->soap;
		//xlog($soap);
		try {

			//$result = $soap->{$command}($params);
			$result = $soap->__soapCall($command, $params);
			
			if ($this->debug_level === 1){
				$this->debug($soap, $result);
			}
		
			if ($xml === true){
				if ($filename !== ''){
					file_put_contents(__ROOT__ . $filename . '.xml', $soap->__getLastResponse());
					return $filename . '.xml';
				}
				
				return $soap->__getLastResponse();
			}
		
			return $result;
		} catch (\Exception $e){
			$this->error($e);
		}
		
		return null;
	}
	
	private function error($error){
		$text = <<<A
Code: <b>{$error->faultcode}</b>
Text: <b>{$error->faultstring}</b>
A;

		xlog($text);
	}
	
	private function debug($soap, $result){
		echo "====== REQUEST HEADERS =====" . PHP_EOL;
		xlog($soap->__getLastRequestHeaders(), false);
		echo "========= REQUEST ==========" . PHP_EOL;
		echo "<pre>" . htmlentities($soap->__getLastRequest()) . "</pre>\n";
		echo "========= RESPONSE =========" . PHP_EOL;
		xlog($result);
	}
	
	public function help($list = false, $params = false){
		$soap = $this->soap;
		
		$help = array();
		
		$methods = $this->help_functions();
		if ($list){
			$i = 0;
			echo "<pre>";
			foreach ($methods as $method){
				$i++;
				if ($params){
					echo str_pad($i, 2,"0",STR_PAD_LEFT) . '. ' . $method['request'] . (" [". (empty($method['params']) ? 'Nėra parametrų]' : ']')) . "<br/>";
				} else {
					echo str_pad($i, 2,"0",STR_PAD_LEFT) . '. ' . $method['request'] . "<br/>";
				}
			}
			echo "</pre>";
			die();
		}
		
		xlog($methods);
		//xlog($this->help_types($functions, $soap->__getTypes()));
	}
	
	private function help_functions(){
		$functions =  $this->soap->__getFunctions();
		$func = array();
		
		foreach ($functions as $function){
			$a = explode(' ', $function, 2);
			$r = explode('(', $a[1], 2);
			$e = explode(')', $r[1], 2);
			
			array_push($func, array(
				'request' => $r[0],
				'response' => $a[0],
				'params' => $e[1]
			));
			
		}
		
		return $func;
	}
	
	private function help_types($req, $types){
		//sudetingas reikalas
		$param1 = 'IssamusProgramosIsrasasPagalJarKodaRequest';
		$test = array();
		foreach ($types as $type){
			$type = ltrim($type, 'struct');
			$g = explode(' ', $type, 2);
			
			$l = $g[0];
			$attributtes = rtrim(ltrim($g[1],'{'),'}');
			
			array_push($test, array(
				'a' => $type,
				'b' => $g,
				'c' => $l,
				'd' => $attributtes
			));
		}
		
		xlog($test);
		
		return $types;
	}

	private function wsse_plain($auth){
		$username = $auth['username'];
		$password = $auth['password'];
		
		$nonce = mt_rand();
		$timestamp = gmdate('Y-m-d\TH:i:s\Z');
		$base64 = base64_encode(pack('H*', $nonce));
		
			$xml = <<<A
		<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
			<wsse:UsernameToken>
				<wsse:Username>{$username}</wsse:Username>
				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">{$password}</wsse:Password>
				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">)){$base64}</wsse:Nonce>
				<wsu:Created xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">{$timestamp}</wsu:Created>
			</wsse:UsernameToken>
		</wsse:Security>
A;
		$header = new \SoapHeader('http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
			'Security',
			new \SoapVar($xml, XSD_ANYXML), 
			true
		);
		
		$this->soap->__setSoapHeaders(array($header));
	}
}

class wsdl_reader {
	public $xml;
	
	public function __construct($url){
		if (is_file($url)){
			$this->xml = simplexml_load_file($url);
			return;
		} else {
			$this->xml = simplexml_load_string($url);
			return;
		}
	}
	
	public function getXml(){
		return $this->xml;
	}
	
	public function getNodes($namespace, $selector){
		return $this->xml
			->children('http://schemas.xmlsoap.org/soap/envelope/')
			->Body
			->children($namespace)
			->xpath($selector);
	}
	
	public function pretty($content = ''){
		$doc = new DomDocument('1.0');
		$doc->preserveWhiteSpace = false;
		$doc->formatOutput = true;
		if ($content === ''){
			$doc->loadXML($this->xml->asXml());
		} else {
			@$doc->loadHTML($content); //error reporting off
		}
		
		$xml_string = $doc->saveXML();
		xlog(htmlentities($xml_string));
	}
	
	public function help(){
		$xml = $this->xml;

		$c = $xml
			->children('http://schemas.xmlsoap.org/soap/envelope/')
			->Body
			->children('http://webservice.aikos2.asseco.lt/')
			->xpath('pilnasProgramosIsrasas/pilnosProgramos/pilnaPrograma');
			
		return $c;
	}
}