<?php
$_SERVER['DOCUMENT_ROOT'] = '/home/edarbuotojas/pvstest';

require_once(__DIR__ . '/param.php');

$query = "SELECT * FROM mato WHERE updated = 0 AND yra = 0 ORDER BY Size ASC, ID ASC LIMIT 100";

$rows = mssql_fetch($query);

foreach ($rows as $row){
	$cols = explode(',', $row['cols']);
	
	$exists = false;
	$table  = $row['Tbl'];
	foreach ($cols as $col){ //`{$col}` LIKE '%š%' COLLATE utf8_bin OR
		$exists = mssql_exists2("SELECT `{$col}` FROM {$table} WHERE `{$col}` LIKE '%ų%' COLLATE utf8_bin OR `{$col}` LIKE '%ą%' COLLATE utf8_bin OR `{$col}` LIKE '%ū%' COLLATE utf8_bin OR `{$col}` LIKE '%į%' COLLATE utf8_bin OR `{$col}` LIKE '%ę%' COLLATE utf8_bin OR `{$col}` LIKE '%ė%' COLLATE utf8_bin LIMIT 1");
		if ($exists === true){
			break;
		}
	}
	
	if ($exists === true){
		mssql_update(array(
			'table' => 'mato',
			'pk_key' => 'ID',
			'pk_val' => $row['ID'],
			'yra' => 1
		), false);
		continue;
	}
	
	mssql_crud_raw($row['DOSQl']);
	mssql_update(array(
		'table' => 'mato',
		'pk_key' => 'ID',
		'pk_val' => $row['ID'],
		'updated' => 1
	), false);
}