SET GLOBAL group_concat_max_len=65000;
SELECT CONCAT('UPDATE `',TABLE_NAME,'` SET ',GROUP_CONCAT('`',COLUMN_NAME,'`=convert(cast(convert( `',COLUMN_NAME,'` using  latin1 ) as binary) using utf8)' SEPARATOR ','),'; UPDATE `mato` SET `Lenta`=''',TABLE_NAME,''';')
FROM information_schema.columns 
WHERE table_schema = 'webpartner' 

AND DATA_TYPE IN ('varchar', 'tinytext', 'text', 'longtext','mediumtext', 'char')
AND TABLE_NAME 
IN (SELECT TABLE_NAME FROM information_schema.TABLES 
WHERE table_schema = 'webpartner'
AND TABLE_TYPE='BASE TABLE' AND TABLE_COLLATION='utf8_unicode_ci')
GROUP BY TABLE_NAME
ORDER BY TABLE_NAME ASC

