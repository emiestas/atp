<?php

//1. patikrinam ar yra toks klasifikatoriaus tipas
function ATP_ATPR_CLASSIFIER_TYPE($type){
	
	$record = array(
		'table' => 'TST_ATP_ATPR_CLASSIFIER_TYPE',
		'pk_key' => 'ID',
		'pk_val' => -1,
		'NAME' => $type
	);

	$pk = mssql_exists($record, false);
	
	if ($pk === false){
		$pk = mssql_insert($record, false);
	}
	echo "{$type} {$pk}<br/>";
	
	ATP_ATPR_CLASSIFIER_GET($type, $pk);
	
	return $pk;
}
//2. Gaunam duomenis iš soap
function ATP_ATPR_CLASSIFIER_GET($classifier, $typeid){
	global $soap;
	$result = $soap->request('GetClassifierItemsWithProperties', array(
		'ClassifiersTypes' => array(
			'ClassifierType' => $classifier
		)
	));
	
	$entries = $result->ClassifierEntry;
	//xlog($entries);
	if (count($entries) === 0){
		die("Negautas registras {$classifier} {$typeid}");
	}
	//xlog($entries);
	if (isset($_GET['test'])){
		xlog($entries);
	}
	
	query("DELETE FROM TST_ATP_ATPR_CLASSIFIER WHERE TYPE_ID = {$typeid}");
	query("DELETE FROM TST_ATP_CLASSIFICATORS WHERE TYPE_ID = {$typeid}");
	foreach ($entries as $entry){
		$parent = mssql_row_column("SELECT ID FROM TST_ATP_CLASSIFICATORS WHERE LABEL = '{$classifier}' AND VALID = 1 AND LEVEL = 0");
		if (empty($parent)){
			$parent = mssql_insert(array(
				'table' => 'TST_ATP_CLASSIFICATORS',
				'pk_key' => 'ID',
				'pk_val' => -1,
				'LABEL' => $classifier,
				'VALID' => 1,
				'LEVEL' => 0,
				'TYPE_ID' => $typeid
			), false);
		}
		ATP_ATPR_CLASSIFIER($classifier, $typeid, $entry, $parent);
	}
}
//3. Tvarkom klasifikaltorius
function ATP_ATPR_CLASSIFIER($classifier, $typeid, $entry, $parent, $deep = 1){ //$parent = parentClassificatorId
	$CLASSIFICATOR_ID = ATP_ATPR_CLASSIFICATOR($classifier, $typeid, $entry, $parent, $deep);
	//xlog($CLASSIFICATOR_ID);
	/*$record = array(
		'table' => 'TST_ATP_ATPR_CLASSIFIER',
		'pk_key' => 'EXTERNAL_ID',
		'pk_val' => $entry->ExternalId
	);*/
	
	$record = array(
		'table' => 'TST_ATP_ATPR_CLASSIFIER',
		'pk_key' => 'EXTERNAL_ID',
		'pk_val' => -1,
		'CLASSIFICATOR_ID' => $CLASSIFICATOR_ID
	);
	
	$pk = mssql_exists($record, false);
	
	if ($pk === false){
		$record = array(
			'table' => 'TST_ATP_ATPR_CLASSIFIER',
			'pk_key' => 'EXTERNAL_ID',
			'pk_val' => $entry->ExternalId
		);
		
		$pk = mssql_exists($record, false);
		
		if ($pk == false){
			$record = array_merge($record, array(
				'pk_val' => $entry->ExternalId,
				'PARENT_EXTERNAL_ID' => $entry->ParentExternalId,
				'TYPE_ID' => $typeid,
				'CODE' => $entry->Code,
				'SHORT_NAME' => $entry->ShortName,
				'NAME' => $entry->Name,
				'CLASSIFICATOR_ID' => $CLASSIFICATOR_ID
			));
			
			$pk = mssql_insert($record, false);
		}
	} else if ($pk != $entry->ExternalId){
		$record = array(
			'table' => 'TST_ATP_ATPR_CLASSIFIER',
			'pk_key' => 'CLASSIFICATOR_ID',
			'pk_val' => $CLASSIFICATOR_ID,
			'EXTERNAL_ID' => $entry->ExternalId,
			'PARENT_EXTERNAL_ID' => $entry->ParentExternalId,
			'CODE' => $entry->Code,
			'SHORT_NAME' => $entry->ShortName,
			'NAME' => $entry->Name
		);
		//xbug laikinai praleidziu
		
		return;
		
		
		//xlog($entry);
		//xlog("{$pk} != {$entry->ExternalId} | {$CLASSIFICATOR_ID} | {$entry->Name} | {$entry->ShortName}");
		//mssql_update($record, false); //neatnaujinu nes nesamone
		
	}
	
	if (is_array($entry->child)){
		foreach ($entry->child as $child){
			ATP_ATPR_CLASSIFIER($classifier, $typeid, $child, $CLASSIFICATOR_ID, $deep + 1); //rekursija
		}
	}
	
	ATP_ATPR_CLASSIFIER_PROPERTY($entry->ExternalId, $entry->properties, $typeid);
}
//4.
function ATP_ATPR_CLASSIFIER_PROPERTY($externalId, $properties, $typeid){
	if (empty($properties->key)){
		return;
	}
	
	$key = array(
		'table' => 'TST_ATP_ATPR_CLASSIFIER_PROPERTY_KEY',
		'pk_key' => 'ID',
		'pk_val' => -1,
		'NAME' => $properties->key
	);
	
	$id = mssql_exists($key, false);
	if ($id === false){
		$id = mssql_insert($key, false);
	}

	$record = array(
		'table' => 'TST_ATP_ATPR_CLASSIFIER_PROPERTY',
		'pk_key' => 'ID',
		'pk_val' => -1,
		'CLASSIFIER_ID' => $externalId,
		'KEY_ID' => $id,
		'VALUE' => $properties->value
	);

	$pk = mssql_exists($record, false);
	
	if ($pk === false){
		//xlogwriteu($record);
		$pk = mssql_insert($record, false);
	}
	
	return $pk;
}
//5.
function ATP_ATPR_CLASSIFICATOR($classifier, $typeid, $entry, $parent, $deep){
	$query = "SELECT ID FROM TST_ATP_CLASSIFICATORS WHERE TYPE_ID = {$typeid} AND LABEL = '" . str_replace("'","''", ($entry->Name ? $entry->Name : $entry->ShortName)) . "' AND VALID = 1 AND LEVEL = {$deep} AND PARENT_ID = {$parent} LIMIT 1 ";

	$id = mssql_row_column($query);
	
	if (!empty($id)){
		return $id;
	}
	
	return mssql_insert(array(
		'table' => 'TST_ATP_CLASSIFICATORS',
		'pk_key' => 'ID',
		'pk_val' => -1,
		'PARENT_ID' => $parent,
		'LABEL' => ($entry->Name ? $entry->Name : $entry->ShortName),
		'VALID' => 1,
		'LEVEL' => $deep,
		'TYPE_ID' => $typeid
	), false);
}

?>