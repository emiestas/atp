<?php

if (php_sapi_name() == 'cli'){
	$_SERVER['DOCUMENT_ROOT'] = '/home/edarbuotojas/pvstest';
}

require_once($_SERVER['DOCUMENT_ROOT'] . '/lexita/param.php');
require_once(__PLUGINS__ . 'wsdl.php');
require_once(__DIR__ . '/VISI_KITI_HELPER.php');

$classifiers = array(
	'ATP_TIKSLI_VIETA',
	'VALSTYBE',
	'POLICIJOS_ISTAIGA',
	'LYTIS',
	//'TP_MARKE',
	'TP_KATEGORIJA',
	'PAZEIDIMA_PADARES_ASMUO',
	'TEISENOS_PRIEMONE',
	'MATAVIMO_MATAS',
	'SPRENDIMO_RUSIS',
	'ATP_PADARYMO_IRANKIS',
	'LENGVINANTI_APLINKYBE',
	'PROCESINIO_SPRENDIMO_TIPAS',
	'SUNKINANTI_APLINKYBE',
	'APSKUNDUSI_SALIS',
	'VALIUTA',
	'DOKUMENTO_GAVEJAS',
	'PREKES_PAVADINIMAS',
	//'ATP_KODEKSAS',
	'ATP_NUSTATYMO_PRIEMONE',
	'TEISES_AKTO_TIPAS',
	'NUOBAUDOS_RUSIS',
	'TA_PANAIKINIMO_PRIEZASTIS',
	//'AN_KODEKSAS',
	'ORGANIZACIJA',
	'PROCESINIO_DOKUMENTO_TIPAS'//,
	//'TARIC_KODAS'
);

foreach ($classifiers as $classifier){
	sleep(2);
	$soap = new wsdl($wsdl['url'], array(), $wsdl);
	
	ATP_ATPR_CLASSIFIER_TYPE($classifier);
}
?>