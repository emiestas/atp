<?php

//1. patikrinam ar yra toks klasifikatoriaus tipas
function ATP_ATPR_CLASSIFIER_TYPE($type){
	
	$record = array(
		'table' => 'TST_ATP_ATPR_CLASSIFIER_TYPE',
		'pk_key' => 'ID',
		'pk_val' => -1,
		'NAME' => $type
	);

	$pk = mssql_exists($record, false);
	
	if ($pk === false){
		$pk = mssql_insert($record, false);
	}
	echo "{$type} {$pk}<br/>";
	
	ATP_ATPR_CLASSIFIER_GET($type, $pk);
	
	return $pk;
}
//2. Gaunam duomenis iš soap
function ATP_ATPR_CLASSIFIER_GET($classifier, $typeid){
	global $soap;
	$result = $soap->request('GetClassifierItemsWithProperties', array(
		'ClassifiersTypes' => array(
			'ClassifierType' => $classifier
		)
	));
	
	$entries = $result->ClassifierEntry;
	//xlog($entries);
	if (count($entries) === 0){
		die("Negautas registras {$classifier} {$typeid}");
	}
	//xlog($entries);
	if (isset($_GET['test'])){
		xlog($entries);
	}
	
	query("DELETE FROM TST_ATP_ATPR_CLASSIFIER WHERE TYPE_ID = {$typeid}");
	query("DELETE FROM TST_ATP_CLASSIFICATORS WHERE TYPE_ID = {$typeid}");
	$parent = mssql_row_column("SELECT ID FROM TST_ATP_CLASSIFICATORS WHERE TYPE_ID = {$typeid} AND LABEL = '{$classifier}' AND VALID = 1 AND LEVEL = 0");
	if (empty($parent)){
		$parent = mssql_insert(array(
			'table' => 'TST_ATP_CLASSIFICATORS',
			'pk_key' => 'ID',
			'pk_val' => -1,
			'LABEL' => $classifier,
			'VALID' => 1,
			'LEVEL' => 0,
			'TYPE_ID' => $typeid
		), false);
	}
	
	foreach ($entries as $entry){
		ATP_ATPR_CLASSIFIER($classifier, $typeid, $entry, $parent);
	}
}
//3. Tvarkom klasifikaltorius
function ATP_ATPR_CLASSIFIER($classifier, $typeid, $entry, $parent, $deep = 1){ //$parent = parentClassificatorId
	$CLASSIFICATOR_ID = ATP_ATPR_CLASSIFICATOR($classifier, $typeid, $entry, $parent, $deep);
	
	$clauseId = TST_ATP_CLAUSES($entry, $CLASSIFICATOR_ID, $deep);
	
	$record = array(
		'table' => 'TST_ATP_ATPR_CLASSIFIER',
		'pk_key' => 'EXTERNAL_ID',
		'pk_val' => -1,
		'CLASSIFICATOR_ID' => $CLASSIFICATOR_ID
	);
	
	$pk = mssql_exists($record, false);
	
	if ($pk === false){
		$record = array(
			'table' => 'TST_ATP_ATPR_CLASSIFIER',
			'pk_key' => 'EXTERNAL_ID',
			'pk_val' => $entry->ExternalId
		);
		
		$pk = mssql_exists($record, false);
		
		if ($pk == false){
			$record = array_merge($record, array(
				'pk_val' => $entry->ExternalId,
				'PARENT_EXTERNAL_ID' => $entry->ParentExternalId,
				'TYPE_ID' => $typeid,
				'CODE' => $entry->Code,
				'SHORT_NAME' => $entry->ShortName,
				'NAME' => $entry->Name,
				'CLASSIFICATOR_ID' => $CLASSIFICATOR_ID,
				'CLAUSE_ID' => $clauseId
			));
			
			$pk = mssql_insert($record, false);
		}
	} else if ($pk != $entry->ExternalId){
		//query("DELETE FROM TST_ATP_ATPR_CLASSIFIER WHERE EXTERNAL_ID = {$entry->ExternalId}");
		/*
		$record = array(
			'table' => 'TST_ATP_ATPR_CLASSIFIER',
			'pk_key' => 'CLASSIFICATOR_ID',
			'pk_val' => $CLASSIFICATOR_ID,
			'EXTERNAL_ID' => $entry->ExternalId,
			'CLAUSE_ID' => $clauseId
		);
		
		$pk = mssql_insert($record, false);
		*/
		//mssql_exists($record, false);
		//kolkas skip, nezinau kaip spresti
		return;
	}
	
	if (is_array($entry->child)){
		foreach ($entry->child as $child){
			ATP_ATPR_CLASSIFIER($classifier, $typeid, $child, $CLASSIFICATOR_ID, $deep + 1); //rekursija
		}
	}
	
	ATP_ATPR_CLASSIFIER_PROPERTY($entry->ExternalId, $entry->properties, $typeid);
	
}
//4.
function ATP_ATPR_CLASSIFIER_PROPERTY($externalId, $properties, $typeid){
	if (!is_array($properties)){
		if(!isset($properties->key)){
			return;
		}
		
		return ATP_ATPR_CLASSIFIER_PROPERTY_CHILD($externalId, $property, $typeid);
	}
	
	if (count($properties) == 0){
		return;
	}
	
	foreach ($properties as $property){
		ATP_ATPR_CLASSIFIER_PROPERTY_CHILD($externalId, $property, $typeid);
	}
}

function ATP_ATPR_CLASSIFIER_PROPERTY_CHILD($externalId, $properties, $typeid){
	$key = array(
		'table' => 'TST_ATP_ATPR_CLASSIFIER_PROPERTY_KEY',
		'pk_key' => 'ID',
		'pk_val' => -1,
		'NAME' => $properties->key
	);
	
	$id = mssql_exists($key, false);
	if ($id === false){
		$id = mssql_insert($key, false);
	}

	$record = array(
		'table' => 'TST_ATP_ATPR_CLASSIFIER_PROPERTY',
		'pk_key' => 'ID',
		'pk_val' => -1,
		'CLASSIFIER_ID' => $externalId,
		'KEY_ID' => $id,
		'VALUE' => $properties->value
	);

	$pk = mssql_exists($record, false);
	
	if ($pk === false){
		//xlogwriteu($record);
		$pk = mssql_insert($record, false);
	}
	
	return $pk;
}


//5.
function ATP_ATPR_CLASSIFICATOR($classifier, $typeid, $entry, $parent, $deep){
	$label =  str_replace('.', ':', $entry->ShortName) . ' ' . str_replace("'","''", $entry->Name);
	//xlog($label);
	$query = "SELECT ID FROM TST_ATP_CLASSIFICATORS WHERE TYPE_ID = {$typeid} AND LABEL = '{$label}' AND VALID = 1 AND LEVEL = {$deep} AND PARENT_ID = {$parent} LIMIT 1 ";

	$id = mssql_row_column($query);
	
	if (!empty($id)){
		return $id;
	}
	
	return mssql_insert(array(
		'table' => 'TST_ATP_CLASSIFICATORS',
		'pk_key' => 'ID',
		'pk_val' => -1,
		'PARENT_ID' => $parent,
		'LABEL' => $label,
		'VALID' => 1,
		'LEVEL' => $deep,
		'TYPE_ID' => $typeid
	), false);
}

function TST_ATP_CLAUSES($entry, $CLASSIFICATOR_ID, $deep){
	$properties = $entry->properties;
	//xlog($properties);
	$insert = array(
		'table' => 'TST_ATP_CLAUSES',
		'pk_key' => 'ID',
		'pk_val' => -1,
		'VALID' => 1
	);
	
	if ($deep == 1){
		$insert['TYPE'] = 0;//$deep;
		
		if (!is_array($properties)){
			$insert['CLAUSE'] = $properties->value;
		} else {
			
			foreach ($properties as $property){
				switch ($property->key){
					case 'ATPK_straipsnis':
					case 'Straipsnis':
						$insert['CLAUSE'] = $property->value;
						break;
					default:
						break;
				}
			}
		}
		
		$pk = mssql_exists($insert, false);
		
		if ($pk === false){
			
			$insert['NAME'] = "{$insert['CLAUSE']} str";
			$insert['LABEL'] = $entry->Name;
			
			if (empty($insert['CLAUSE'])){
				xlog($insert);
			}
			
			
			return mssql_insert($insert, false);
		}
		
		return $pk;
	
	}
	
	
	
	$props = array();
	
	if (!is_array($properties)){
		return;
	}
	
	
	foreach ($properties as $property){
		switch ($property->key){
			case 'BaudaNuo':
			case 'BaudaVadovamsNuo':
			case 'BaudaNuoEUR':
				$props['PENALTY_MIN'] = $property->value;
				break;
			case 'BaudaIki':
			case 'BaudaVadovamsIki':
			case 'BaudaIkiEUR':
				$props['PENALTY_MAX'] = $property->value;
				break;
			case 'Istaiga':
				break;
			case 'ATPK_straipsnis':
			case 'Straipsnis':
				$props['CLAUSE'] = $property->value;
				break;
			case 'StraipsnioDalis':
				$props['SECTION'] = $property->value;
				break;
			case 'ArbaNuobauda':
				$props['NOTICE_LABEL'] = $property->value;
				$props['NOTICE'] = 1;
				break;
			case 'KitaNuobauda':
				$props['NOTICE_LABEL'] = $property->value;
				$props['NOTICE'] = 1;
				break;
			default:
				break;
		}
	}
	
	$insert['CLAUSE'] = $props['CLAUSE'];
	$insert['TYPE'] = 0;//$deep;
	if ($deep == 2){
		$insert['SECTION'] = $props['SECTION'];
	}
	
	$pk = mssql_exists($insert, false);
	if ($pk === false){
		$insert = array_merge($insert, $props);
		if ($deep == 2){
			$insert['NAME'] = "{$props['CLAUSE']} str {$props['SECTION']} d";
			$insert['PARENT_ID'] = mssql_row_column("SELECT ID FROM TST_ATP_CLAUSES WHERE TYPE = 0 AND CLAUSE = '{$props['CLAUSE']}' AND SECTION = ''");
		} else {
			$insert['NAME'] = "{$props['CLAUSE']} str";
		}
		$insert['LABEL'] = $entry->Name;
		//xlog($insert);
		return mssql_insert($insert, false);
	}
	
	//kitaip turėčiau tikrinti update
}

?>