<?php

ob_start();
$start = microtime(true);
$startm = memory_get_usage(true);

try {
    require __DIR__ . '/../../inc/mainconf.php';
    ini_set('max_execution_time', 100);
    ini_set('display_errors', true);

    $atp = prepare_atp();
    $atp->init();

    if (empty($_POST['atp_act']) === false && $_POST['atp_act'] == 'open') {
        $atp->manage->writeATPactionLog('Atsidarė posistemę');
    }

    if (empty($_SESSION['WP']['REDIRECT_URL']) === false) {
        header('Location: ' . str_replace('|', '&', $_SESSION['WP']['REDIRECT_URL']));
        unset($_SESSION['WP']['REDIRECT_URL']);
        exit;
    }

    $atp->action_manage(filter_input(INPUT_GET, 'm'));
} catch (\Exception $e) {
    echo $e->getMessage();
    exit;
}

$end = microtime(true);
$endm = memory_get_usage(true);
$content = ob_get_clean();

if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) && (in_array($_SERVER['REMOTE_ADDR'], ['195.14.183.214',
        '127.0.0.1']) || $_COOKIE['I'] === 'dev')) {

    $convert = function ($size) {
        $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    };

    $extraHtml = '<div style="position: fixed; bottom: 20px; left: 0; z-index: 99999;">Loadtime: ' . ($end - $start) . '<br/>Memory: ' . $convert($endm - $startm) . '</div>';

    $regexp = '/<body.*?>/';
    if (preg_match($regexp, $content, $matches)) {
        $content = preg_replace($regexp, $matches[0] . $extraHtml, $content, 1);
    } else {
        $content .= $extraHtml;
    }
}

echo $content;
